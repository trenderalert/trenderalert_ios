//
//  ImageTrendCollectionViewCell.swift
//  TrenderAlert
//
//  Created by OSP LABS on 03/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ImageTrendCollectionViewCell: UICollectionViewCell {

    @IBOutlet var videoView: PlayerView!
    @IBOutlet weak var trendImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
