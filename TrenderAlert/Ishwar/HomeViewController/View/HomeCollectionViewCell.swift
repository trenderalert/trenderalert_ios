//
//  HomeCollectionViewCell.swift
//  TrenderAlert
//
//  Created by OSP LABS on 14/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var trendImageView: UIImageView!
}
