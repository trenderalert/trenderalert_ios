//
//  TrendTableViewCell.swift
//  TrenderAlert
//
//  Created by OSP LABS on 03/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import DropDown
import ActiveLabel

class TrendTableViewCell: UITableViewCell, ASAutoPlayVideoLayerContainer {
    
    // MARK: - Outlets
    
    @IBOutlet var btnShareTrend: UIButton!
    @IBOutlet var imgVideoIcon: UIImageView!
    @IBOutlet var cnstrntVideoWidth: NSLayoutConstraint!
    @IBOutlet var btnFullScrProfileImg: UIButton!
    var profileNameTap = UITapGestureRecognizer()
//    @IBOutlet weak var profileNameTap: UITapGestureRecognizer!
    @IBOutlet var txtAddComment: UITextField!
    @IBOutlet var addCommentButton: UIButton!
    @IBOutlet var shareDescLabel: UILabel!
    @IBOutlet var cnstrntShareDescHeight: NSLayoutConstraint!
    
    @IBOutlet weak var cnstrTrendHeaderViewHeight: NSLayoutConstraint!
    @IBOutlet weak var cnstrTrendStatusViewHeight: NSLayoutConstraint!
    @IBOutlet weak var cnstrTrendViewCommentViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var shareCountButton: UIButton!
    @IBOutlet var viewsCountButton: UIButton!
    @IBOutlet var otherUserProfileButton: UIButton!
    @IBOutlet var followStatusButton: UIButton!
    @IBOutlet var likeCountButton: UIButton!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var trendHeaderView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet var btnLearnMore: UIButton!
    @IBOutlet weak var profileNameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var trendMoreButton: UIButton!
    @IBOutlet weak var statusImageView: UIImageView!
    @IBOutlet weak var trendImageCollectionView: UICollectionView!
    @IBOutlet weak var trendStatusView: UIView!
    @IBOutlet weak var trendCommentsView: UIView!
    @IBOutlet weak var trendCommentsImageView: UIImageView!
    @IBOutlet weak var commentsCountLabel: UILabel!
    @IBOutlet weak var trendLikesView: UIView!
    @IBOutlet weak var trendLikesImageView: UIImageView!
    @IBOutlet weak var likesCountLabel: UILabel!
    @IBOutlet weak var trendShareStatusView: UIView!
    @IBOutlet weak var shareStatusImageView: UIImageView!
    @IBOutlet weak var shareCountLabel: UILabel!
    @IBOutlet weak var trendViewsView: UIView!
    @IBOutlet weak var trendViewsImageView: UIImageView!
    @IBOutlet weak var viewsCountLabel: UILabel!
    @IBOutlet weak var trendImagesPageControl: UIPageControl!
    @IBOutlet weak var trendViewCommentsView: UIView!
    @IBOutlet weak var followStatusImageView: UIImageView!
    @IBOutlet weak var trendPostTitleLabel: UILabel!
    @IBOutlet weak var postCommentLabel: UILabel!
    @IBOutlet weak var viewAllCommentsButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var trendAddedLabel: UILabel!
    @IBOutlet weak var trendExpiryLabel: UILabel!
    @IBOutlet weak var trendCurrentImageLabel: UILabel!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet var viewVideo: UIView!
    var trendDropDown = DropDown()
//    var playerController: ASVideoPlayerController?
    var videoLayer: AVPlayerLayer = AVPlayerLayer()
    var videoURL: String? {
        didSet {
            if let videoURL = videoURL {
                ASVideoPlayerController.sharedVideoPlayer.setupVideoFor(url: videoURL)
            }
            videoLayer.isHidden = videoURL == nil
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        videoLayer.backgroundColor = UIColor.clear.cgColor
        videoLayer.videoGravity = AVLayerVideoGravity.resize
//        videoLayer.frame = trendImageCollectionView.frame
        trendImageCollectionView.layer.addSublayer(videoLayer)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        let horizontalMargin: CGFloat = 20
//        let width: CGFloat = self.frame.size.width
//        let height: CGFloat = (width * 1).rounded(.up)
//        videoLayer.frame = CGRect(x: 0, y: -82, width: width , height: height)
//        videoLayer.frame = viewVideo.frame
//
//        videoLayer.frame.origin.y = 0
    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func visibleVideoHeight() -> CGFloat {
        let videoFrameInParentSuperView: CGRect? = self.superview?.superview?.convert(trendImageCollectionView.frame, from: trendImageCollectionView)
        guard let videoFrame = videoFrameInParentSuperView,
            let superViewFrame = superview?.frame else {
                return 0
        }
        let visibleVideoFrame = videoFrame.intersection(superViewFrame)
//        videoLayer.frame = trendImageCollectionView.frame

        return visibleVideoFrame.size.height
    }
    
    func selectedLang(trendData:TrendListData,index:Int)
    {
        self.trendExpiryLabel.text = "\(MySingleton.shared.selectedLangData.expires_in) \(trendData.data[index].RemainingExpiryTime ?? "")"
        self.trendAddedLabel.text = "\(trendData.data[index].TimeAgo ?? "") \(MySingleton.shared.selectedLangData.ago)"
        self.txtAddComment.attributedPlaceholder = NSAttributedString(string: "\(MySingleton.shared.selectedLangData.write_a_comment)",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.baseColor])
    }
    
    
    func trendMoreButtonAction(index:Int,trendData:TrendListData)
    {
        
        var arrayOptions = [MySingleton.shared.selectedLangData.share, MySingleton.shared.selectedLangData.share_external, MySingleton.shared.selectedLangData.favorite, MySingleton.shared.selectedLangData.turn_off_post_notifications, MySingleton.shared.selectedLangData.report, MySingleton.shared.selectedLangData.save_to_folder]
        if trendData.data[index].IsTrendFavourite
        {
            arrayOptions[2] = MySingleton.shared.selectedLangData.unfavorite
        }
        else
        {
            arrayOptions[2] = MySingleton.shared.selectedLangData.favorite
        }
        
        if trendData.data[index].IsPushNotificationEnabled
        {
            arrayOptions[3] = MySingleton.shared.selectedLangData.turn_off_post_notifications
        }
        else
        {
            arrayOptions[3] = MySingleton.shared.selectedLangData.turn_on_post_notifications
        }
        
        if trendData.data[index].AreYouFollowingTrender != 1 && trendData.data[index].AreYouFollowingTrender != 0 && trendData.data[index].AreYouFollowingTrender != 2{
            // your trend
            arrayOptions.append(MySingleton.shared.selectedLangData.edit_trend)
            arrayOptions.append(MySingleton.shared.selectedLangData.delete_trend)
        }
        
        trendDropDown.dataSource = arrayOptions
        trendDropDown.cellHeight = 35.0
        trendDropDown.show()
        //  }
    }
    
}
