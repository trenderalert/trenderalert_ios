//
//  TrendListData.swift
//  TrenderAlert
//
//  Created by OSP LABS on 06/02/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class TrendListData: NSObject, Codable {
    var data = [TrendList]()
    
    enum CodingKeys: String, CodingKey {
        case data
    }
}

class TrendListImageData: NSObject, Codable {
    var TrendId = 0
    var TrendImage = String()
    var VideoUrl : String?
    var Media:String?
    var ImageHeight : Int?
    var ImageWidth : Int?
    var TrendImageId : Int?
    
    
    enum CodingKeys: String, CodingKey {
        case TrendId
        case TrendImage
        case VideoUrl
        case Media
        case ImageHeight
        case ImageWidth
        case TrendImageId
    }
}

class SharedTrend: NSObject, Codable {
    var UserProfileId = 0
    var Description : String?
    var UserProfileFirstName = String()
    var UserProfileLastName = String()
    var UserProfileImage : String?
    
    
    enum CodingKeys: String, CodingKey {
        case UserProfileId
        case Description
        case UserProfileFirstName
        case UserProfileLastName
        case UserProfileImage
    }
}

class TaggedTrenders: NSObject, Codable {
    var TrendId = Int()
    var FollowerProfileImageUrl : String?
    var UserFollowerId = Int()
    var UserProfileFirstName = String()
    var UserProfileLastName = String()
    
    
    enum CodingKeys: String, CodingKey {
        case TrendId
        case FollowerProfileImageUrl
        case UserFollowerId
        case UserProfileFirstName
        case UserProfileLastName
    }
}

class TaggedFriends: NSObject, Codable {
    var ProfileImage : String?
    var UserProfileID = Int()
    var Firstname = String()
    var Lastname = String()
    
    enum CodingKeys: String, CodingKey {
        case Firstname
        case Lastname
        case ProfileImage
        case UserProfileID
    }
}

class TrendList: NSObject, Codable {
    var AreYouFollowingTrender = Int()
    var CommentCount:String?
    var ContactEmail:String?
    var Description : String?
    var EnableEdit = Bool()
    var EnableIsTrenderOnline = Bool()
    var EnableTrendFollowing = Bool()
    var EnableTrendLike = Bool()
    var EventDate = String()
    var ExpiryDate = String()
    var ExpiryDuration = 0
    var FolderPath : String?
    var IsContactByAudio = Bool()
    var IsContactByAudioInSetting = Bool()
    var IsContactByText = Bool()
    var IsContactByTextInSetting = Bool()
    var IsTrendActive = Bool()
    var IsTrendDelete = Bool()
    var IsTrendFavourite = Bool()
    var IsTrendFollowing = Bool()
    var IsTrendLike = Bool()
    var IsTrendPublic = Bool()
    var IsTrenderOnline = Bool()
    var IsVideoLink = Bool()
    //    var MediaType = String()
    var RemainingExpiryTime:String?
    var StartDate = String()
    var Title : String?
    var TrendFollowerCount = 0
    var TrendId = 0
    var TrendImage : String?
    var TrendLatitude = 0.0
    var TrendLikesCount = 0
    var TrendLocation : String?
    var TrendLongitude = 0.0
    var TrendShareId = 0
    //    var TrendThumbnail : String?
    //    var TrendVideo : String?
    var TrendViewCount:String?
    var UserProfileFirstName:String?
    var UserProfileId = 0
    var UserProfileImage : String?
    var UserProfileLastName:String?
    var ImageList : [TrendListImageData]?
    var SharedTrendInfo : SharedTrend?
    var TimeAgo:String? = ""
    var IsCustomAd = false
    var IsGoogleAd = false
    var AdUrl:String?
    var TrendShareCount = 0
    var TaggedTrenders : [TaggedTrenders]?
    var IsPushNotificationEnabled = Bool()
    var TaggedFriends : [TaggedFriends]?
    var TrendTypeId = Int()
    var IsTrendShared = Bool()
    var TrendOwner:String?
    var TrendOwnerId:Int?
    var SharedTrendDescription:String?
   
    enum CodingKeys: String, CodingKey {
        case AreYouFollowingTrender
        case CommentCount
        case ContactEmail
        case Description
        case EnableEdit
        case EnableIsTrenderOnline
        case EnableTrendFollowing
        case EnableTrendLike
        case EventDate
        case ExpiryDate
        case ExpiryDuration
        case FolderPath
        case IsContactByAudio
        case IsContactByAudioInSetting
        case IsContactByText
        case IsContactByTextInSetting
        case IsTrendActive
        case IsTrendDelete
        case IsTrendFavourite
        case IsTrendFollowing
        case IsTrendLike
        case IsTrendPublic
        case IsTrenderOnline
        case IsVideoLink
        //        case MediaType
        case RemainingExpiryTime
        case StartDate
        case Title
        case TrendFollowerCount
        case TrendId
        case TrendImage
        case TrendLatitude
        case TrendLikesCount
        case TrendLocation
        case TrendLongitude
        case TrendShareId = "TrendShareId"
        //        case TrendThumbnail
        //        case TrendVideo
        case TrendViewCount
        case UserProfileFirstName
        case UserProfileId
        case UserProfileImage
        case UserProfileLastName
        case ImageList
        case SharedTrendInfo
        case TimeAgo = "TimeAgo"
        case IsCustomAd = "IsCustomAd"
        case IsGoogleAd = "IsGoogleAd"
        case AdUrl = "AdUrl"
        case TrendShareCount
        case TaggedTrenders
        case IsPushNotificationEnabled
        case TaggedFriends
        case TrendTypeId
        case IsTrendShared
        case TrendOwner = "TrendOwner"
        case TrendOwnerId
        case SharedTrendDescription
    }
}

class ChatListData: NSObject, Codable {
    var data = [ChatDataWithUser]()
    
    enum CodingKeys: String, CodingKey {
        case data
    }
}

class ChatDataWithUser: NSObject, Codable {
    var ChatDetails = [ChatData]()
    
    enum CodingKeys: String, CodingKey {
        case ChatDetails
    }
}

class ChatData: NSObject, Codable {
    var FromUserProfileId = 0
    var ToUserProfileId = 0
    var ChatText : String?
    var MediaType = String()
    var MediaPath : String?
    var ThumbnailUrl : String?
    var Duration : String?
    var CreatedDate = String()
    var Id = Int()
    var IsDeleted = Bool()
    
    enum CodingKeys: String, CodingKey {
        case FromUserProfileId
        case ToUserProfileId
        case ChatText
        case MediaType
        case MediaPath
        case ThumbnailUrl
        case Duration
        case CreatedDate
        case Id
        case IsDeleted
    }
}

