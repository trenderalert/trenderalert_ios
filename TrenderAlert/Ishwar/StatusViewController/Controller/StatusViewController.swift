//
//  StatusViewController.swift
//  TrenderAlert
//
//  Created by Admin on 05/02/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import SwiftOnoneSupport
import AGEmojiKeyboard
import UIFontComplete
import IQKeyboardManagerSwift


protocol StatusData {
    func fetchStatusData(status : [String:Any], image : UIImage)
}

class StatusViewController: UIViewController,AGEmojiKeyboardViewDelegate, AGEmojiKeyboardViewDataSource,UITextViewDelegate {
    
    @IBOutlet var viewOptions: UIView!
//    @IBOutlet var statusView: UIView!
    @IBOutlet var cnstShareBottom: NSLayoutConstraint!
    @IBOutlet var viewColor: UIView!
    @IBOutlet var cnstColorHeight: NSLayoutConstraint!
    @IBOutlet var colorsCollection: UICollectionView!
    @IBOutlet var imgBackground: UIImageView!
    @IBOutlet var txtViewStatus: UITextView!
    var emojiImages = [#imageLiteral(resourceName: "time_clk"),
                       #imageLiteral(resourceName: "emoji_clk"),
                       #imageLiteral(resourceName: "bell_clk"),
                       #imageLiteral(resourceName: "flower_clk"),
                       #imageLiteral(resourceName: "car_clk"),
                       #imageLiteral(resourceName: "Recent_clk")]
    var emojiSelectedIndex = -1
    var emojiNotSelectedIndex = -1
    var fontStyle = [UIFont(font: .americanTypewriterCondensedBold, size: 30),UIFont(font: .arialBoldItalicMT, size: 30),UIFont(font: .cochinItalic, size: 30),UIFont(font: .gillSansSemiBoldItalic, size: 30),UIFont(font: .timesNewRomanPSBoldMT, size: 30),UIFont(font: .timesNewRomanPSItalicMT, size: 30)]
    var colorList = [AnyObject]()
    var colorCode = String()
    var font = Int()
    let picker = UIImagePickerController()
    var txtData = String()
    var flagStatus = Int()
    var delegate : StatusData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtViewStatus.delegate  = self
        self.picker.delegate = self
//        self.cnstColorHeight.constant = 0
        IQKeyboardManager.shared.enable = true
        getColorList()
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Hex to rgb color
    
    func colorWithHexString(_ hex: String) -> UIColor {
        
        var cString = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substring(from: 1)
        }
        
        if (cString.characters.count != 6) {
            return UIColor.gray
        }
        
        let rString = (cString as NSString).substring(to: 2)
        let gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
        let bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
    
    // MARK: - Get color list
    func getColorList(){
        
        WebServices().callUserService(service: UserServices.getColors, urlParameter: "", parameters: nil, isLazyLoading: true, isHeader: true, CallMethod: .get, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
//            if(serviceResponse["Status"] as! Bool == true)
//            {
            DispatchQueue.main.async{
                self.colorList = serviceResponse["data"] as! [AnyObject]
                self.colorsCollection.delegate = self
                self.colorsCollection.dataSource = self
                self.colorsCollection.reloadData()
                self.colorsCollection.isHidden = true
            }
           // self.cnstColorHeight.constant = 0
//            }
//            else
//            {
//                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
//            }
        })
    }

    @IBAction func btn_emojiKeyboardAction(_ sender: UIButton)
    {
        var agEmojiKeyboardView = AGEmojiKeyboardView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216), dataSource: self)
        agEmojiKeyboardView?.autoresizingMask = .flexibleHeight
        if sender.currentImage == #imageLiteral(resourceName: "Smile")
        {
        self.txtData = self.txtViewStatus.text
        agEmojiKeyboardView?.delegate = self
        agEmojiKeyboardView?.segmentsBar.backgroundColor = UIColor.clear
        sender.setImage(#imageLiteral(resourceName: "keyboard"), for: .normal)
        self.txtViewStatus.inputView = agEmojiKeyboardView
        self.txtViewStatus.becomeFirstResponder()
        self.txtViewStatus.text = self.txtData
        }
        else
        {
            self.txtData = self.txtViewStatus.text
            self.txtViewStatus.inputView = nil
            self.txtViewStatus.inputView = UIView()
            sender.setImage(#imageLiteral(resourceName: "Smile"), for: .normal)
            self.txtViewStatus.keyboardType = UIKeyboardType.default
            self.txtViewStatus.becomeFirstResponder()
            self.txtViewStatus.text = self.txtData
        }
    }
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if sender.currentImage != #imageLiteral(resourceName: "Smile")
//        {
//             self.txtViewStatus.keyboardType = UIKeyboardType.default
//            self.txtViewStatus.text = self.txtData
//        }
//        return true
//    }
    
    @IBAction func btn_FOntAction(_ sender: UIButton)
    {
        if self.font < self.fontStyle.count
        {
           self.txtViewStatus.font = self.fontStyle[self.font]
           self.font += 1
        }
        else
        {
            self.font = 0
            self.txtViewStatus.font = self.fontStyle[self.font]
            self.font += 1
        }
    }
    
    @IBAction func btn_ColorAction(_ sender: UIButton)
    {
         self.imgBackground.backgroundColor = UIColor.clear
        if self.colorsCollection.isHidden == true
        {
            self.colorsCollection.isHidden = false
        }
        else
        {
            self.colorsCollection.isHidden = true
        }
    }
    
    @IBAction func btn_SelectPicture(_ sender: UIButton)
    {
        self.view.endEditing(true)
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select your option", message: "", preferredStyle: .actionSheet)
        
        let cameraActionButton: UIAlertAction = UIAlertAction(title: "Camera", style: .default) { void in
            print("Camera")
            self.openCamera()
        }
        actionSheetControllerIOS8.addAction(cameraActionButton)
        
        let galleryActionButton: UIAlertAction = UIAlertAction(title: "Gallery", style: .default) { void in
            print("Gallery")
            self.openGallary()
        }
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .default) { void in
            print("Cancel")
            
        }
                actionSheetControllerIOS8.addAction(galleryActionButton)
        actionSheetControllerIOS8.addAction(cancelActionButton)
        actionSheetControllerIOS8.popoverPresentationController?.sourceView = self.imgBackground
        actionSheetControllerIOS8.popoverPresentationController?.sourceRect = self.imgBackground.bounds
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    
    @IBAction func btn_SubmitStatus(_ sender: UIButton)
    {
        // Begin context
        UIGraphicsBeginImageContextWithOptions(self.viewColor.bounds.size, false, UIScreen.main.scale)

        // Draw view in that context
        self.viewColor.drawHierarchy(in: self.viewColor.bounds, afterScreenUpdates: true)

        // And finally, get image
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
//
//        let imagesnap = self.viewColor.snapshot(of: CGRect(x: 0, y: 0, width: self.viewColor.frame.width, height: self.view.frame.height - self.viewOptions.frame.height), afterScreenUpdates: true)
//
//      //  print(image!)
       
        var statusInfo = [String:Any]()
        let imageData = ["FileExt":"jpg","contenttype":"image/jpg","ImageData":"data:image/jpg;base64,\(encodeToBase64String(image: image!))"]
        
        if self.imgBackground.image != nil
        {
            let imageStatus = ["FileExt":"jpg","contenttype":"image/jpg","ImageData":"data:image/jpg;base64,\(encodeToBase64String(image: self.imgBackground.image!))"]
            statusInfo = ["StatusText":self.txtViewStatus.text!,"BGColorCode":"","BGImage":imageStatus,"ConstructedStatusImage":imageData] as [String : Any]
        }
        else
        {
            statusInfo = ["StatusText":self.txtViewStatus.text!,"BGColorCode":self.colorCode,"BGImage":"","ConstructedStatusImage":imageData] as [String : Any]
        }
        
//        print(statusInfo)
        delegate?.fetchStatusData(status: statusInfo, image: image!)
        
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    
    func encodeToBase64String(image: UIImage) -> String
    {
        let imageData = image.jpegData(compressionQuality: 0.5)
        let imag_str:String = imageData!.base64EncodedString(options:[])
        //print(imag_str)
        return imag_str
    }
    
    // MARK: - Emoji Delegate
    func emojiKeyBoardView(_ emojiKeyBoardView: AGEmojiKeyboardView!, didUseEmoji emoji: String!) {
        self.txtViewStatus.text = self.txtViewStatus.text + emoji
        print(emoji)
    }
    
    func emojiKeyBoardViewDidPressBackSpace(_ emojiKeyBoardView: AGEmojiKeyboardView!) {
        self.txtViewStatus.text = String(self.txtViewStatus.text.dropLast())
    }
    
    // MARK: - Emoji Data Source
    func emojiKeyboardView(_ emojiKeyboardView: AGEmojiKeyboardView!, imageForSelectedCategory category: AGEmojiKeyboardViewCategoryImage) -> UIImage! {
        if emojiSelectedIndex <= 4 {
            emojiSelectedIndex = emojiSelectedIndex + 1
        }
        return emojiImages[emojiSelectedIndex]
    }
    
    func emojiKeyboardView(_ emojiKeyboardView: AGEmojiKeyboardView!, imageForNonSelectedCategory category: AGEmojiKeyboardViewCategoryImage) -> UIImage! {
        if emojiNotSelectedIndex <= 4 {
            emojiNotSelectedIndex = emojiNotSelectedIndex + 1
        }
        return emojiImages[emojiNotSelectedIndex]
    }
    
    func backSpaceButtonImage(for emojiKeyboardView: AGEmojiKeyboardView!) -> UIImage! {
        return #imageLiteral(resourceName: "backspace_clk")
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n"{
            textView.resignFirstResponder()
        }
        let textFieldText: NSString = (textView.text ?? "") as NSString
        let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: text)
       // newAnswers[textView.tag] = txtAfterUpdate
        return true
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
            textView.text = ""
            textView.textColor = UIColor.white
    }
    
}
extension StatusViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    /*************************
     Method Name:  openGallary()
     Parameter: nil
     return type: nil
     Desc: This function opens the gallery.
     *************************/
    func openGallary()
    {
        picker.allowsEditing = false
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        present(picker, animated: true, completion: nil)
    }
    
    /*************************
     Method Name:  openCamera()
     Parameter: nil
     return type: nil
     Desc: This function opens the Camera.
     *************************/
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            present(picker, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    /*************************
     Method Name:  imagePickerControllerDidCancel()
     Parameter: UIImagePickerController
     return type: nil
     Desc: This function is called if user taps cancel button.
     *************************/
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    /*************************
     Method Name:  imagePickerController()
     Parameter: UIImagePickerController, [String : Any]
     return type: nil
     Desc: This function is called when user selects image from gallery.
     *************************/
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        print(chosenImage)
        self.viewColor.backgroundColor = UIColor.clear
        self.colorCode = ""
        self.flagStatus = 1
        self.imgBackground.backgroundColor = UIColor.lightGray
        self.imgBackground.image = chosenImage
        
        dismiss(animated:true, completion: nil)
    }
}

extension StatusViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.colorList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ColorsCVC", for: indexPath) as! ColorsCVC
         cell.viewColor.giveBorderColor(color: UIColor.black)
        if indexPath.row == 0
        {
            let colorRGB = colorWithHexString(self.colorList[indexPath.row]["ColorCode"] as! String)
            self.viewColor.backgroundColor = colorRGB
            self.colorCode = self.colorList[indexPath.row]["ColorCode"] as! String
            cell.viewColor.backgroundColor = colorRGB
        }
        else
        {
           let colorRGB = colorWithHexString(self.colorList[indexPath.row]["ColorCode"] as! String)
           cell.viewColor.backgroundColor = colorRGB
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.imgBackground.image = nil
        let colorRGB = colorWithHexString(self.colorList[indexPath.row]["ColorCode"] as! String)
        self.viewColor.backgroundColor = colorRGB
        self.colorCode = self.colorList[indexPath.row]["ColorCode"] as! String
       // self.cnstColorHeight.constant = 0
        self.flagStatus = 0
    }
    
}

extension UIView {
    
    /// Create image snapshot of view.
    ///
    /// - Parameters:
    ///   - rect: The coordinates (in the view's own coordinate space) to be captured. If omitted, the entire `bounds` will be captured.
    ///   - afterScreenUpdates: A Boolean value that indicates whether the snapshot should be rendered after recent changes have been incorporated. Specify the value false if you want to render a snapshot in the view hierarchy’s current state, which might not include recent changes. Defaults to `true`.
    ///
    /// - Returns: The `UIImage` snapshot.
    
    func snapshot(of rect: CGRect? = nil, afterScreenUpdates: Bool = true) -> UIImage {
        return UIGraphicsImageRenderer(bounds: rect ?? bounds).image { _ in
            drawHierarchy(in: bounds, afterScreenUpdates: true)
        }
    }
}
