//
//  Utilities.swift
//  TrenderAlert
//
//  Created by OSP LABS on 03/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class Utilities: NSObject {

    static let shared = Utilities()
    
    static let groupPrivacyDetails = [[MySingleton.shared.selectedLangData.Public,MySingleton.shared.selectedLangData.anyone_can_search_the_group_amp_what_they_post,1],[MySingleton.shared.selectedLangData.closed,MySingleton.shared.selectedLangData.secret_group_info,2],[MySingleton.shared.selectedLangData.secret,MySingleton.shared.selectedLangData.only_member_can_search_for_group_see_members_and_there_post,3]]
    
    static func getImageHeight(image: UIImage) -> CGFloat {
        let imageView = UIImageView()
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        imageView.sizeToFit()
//        print("Image Height with Scale Aspect Fit = \(imageView.frame.size.height)")
        return imageView.frame.size.height
    }
    
    static func getImageWidth(image: UIImage) -> CGFloat {
        let imageView = UIImageView()
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        imageView.sizeToFit()
//        print("Image Width with Scale Aspect Fit = \(imageView.frame.size.width)")
        return imageView.frame.size.width
    }
    
    static func setNavigationBar(viewController: UIViewController) {
        let logo = UIImage(named: "logo_navigation")
        let imageView = UIImageView(image:logo)
        viewController.navigationItem.titleView = imageView
        viewController.navigationController?.navigationBar.barTintColor = .black
        viewController.navigationController?.navigationBar.tintColor = .white
    }
    
    
    static func setNavigationBarWithTitle(viewController: UIViewController,title:String){
        
        viewController.navigationItem.title = title
        let label = UILabel()
        label.attributedText = NSAttributedString(string: title, attributes: [NSAttributedString.Key.font:UIFont(name: "OpenSans-Bold", size: UIFont.systemFontSize) ?? 15,NSAttributedString.Key.foregroundColor:UIColor.white])
        viewController.navigationItem.titleView = label
        viewController.navigationController?.navigationBar.barTintColor = .black
        viewController.navigationController?.navigationBar.tintColor = .white
    }
    
    func encodeToBase64String(image: UIImage) -> String
    {
        let imageData = image.jpegData(compressionQuality: 0.5)
        let imag_str:String = imageData!.base64EncodedString(options:[])
        //print(imag_str)
        return imag_str
    }
    
    class settingSwitch:UISwitch{
        var row:Int?
        var section:Int?
    }
}
