//
//  TrenderAertVC.swift
//  TrenderAlert
//
//  Created by OSP LABS on 27/12/18.
//  Copyright © 2018 Single. All rights reserved.
//

import UIKit

class TrenderAlertVC: UIAlertController {
    
    static let shared = TrenderAlertVC(title: "TrenderAlert", message: nil, preferredStyle: .alert)
    var actionAfterDismiss: (()->Void)!
    var appdelegate : AppDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appdelegate = UIApplication.shared.delegate as? AppDelegate
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            guard self.actionAfterDismiss != nil else {
                return
            }
            self.actionAfterDismiss()
        }
        addAction(okAction)
    }
    
    func presentAlertController(title: String = "TrenderAlert", message: String, completionHandler: (()->Void)?) {
        self.title = title
        self.message = message
        actionAfterDismiss = completionHandler
        UIApplication.shared.keyWindow?.rootViewController?.present(self, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
