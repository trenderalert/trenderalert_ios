//
//  Validation.swift
//  TrenderAlert
//
//  Created by HPL on 05/06/17.
//  Copyright © 2017 HPL. All rights reserved.
//

import UIKit

class Validation: NSObject
{
    var viewsToReturn = [UITextField]()
    var whitespaceSet = CharacterSet.whitespaces
    
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func matchesForRegexInText(regex: String, text: String) -> [String] {
        do {
            let regex = try NSRegularExpression(pattern: regex, options: [])
            let nsString = text as NSString
            let results = regex.matches(in: text, options: [], range: NSMakeRange(0, nsString.length))
            return results.map { nsString.substring(with: $0.range)}
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            return []
        }
    }
    
    
    func validationForSocialSignUp(viewList : [UIView]) -> Array<Any>
    {
        var viewsToReturn = [UIView]()
        if((viewList[0]as! FormFields).txtFld_UserValue.text?.isEmpty)!
        {
            (viewList[0]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter First Name",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[0])
        }
        else if ((viewList[0]as! FormFields).txtFld_UserValue.text?.trimmingCharacters(in: whitespaceSet).isEmpty)!
        {
            (viewList[0]as! FormFields).txtFld_UserValue.text = ""
            (viewList[0] as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter First Name",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[0])
        }
        if(((viewList[0]as! FormFields).txtFld_UserValue.text?.characters.count)! > 20)
        {
            (viewList[0]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "You can enter maximum 20 characters.",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[0])
        }
        
        if((viewList[1]as! FormFields).txtFld_UserValue.text?.isEmpty)!
        {
            (viewList[1]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Last Name",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[1])
        }
        else if ((viewList[1]as! FormFields).txtFld_UserValue.text?.trimmingCharacters(in: whitespaceSet).isEmpty)!
        {
            (viewList[1]as! FormFields).txtFld_UserValue.text = ""
            (viewList[1] as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Last Name",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[1])
        }
        if(((viewList[1]as! FormFields).txtFld_UserValue.text?.characters.count)! > 20)
        {
            (viewList[1]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "You can enter maximum 20 characters.",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[1])
        }
        
        if((viewList[2]as! FormFields).txtFld_UserValue.text?.isEmpty)!
        {
            (viewList[2]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Please Select Birth Date",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[2])
        }
        
        if((viewList[3]as! FormFields).txtFld_UserValue.text?.isEmpty)!
        {
            (viewList[3]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Location",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[3])
        }
        else if ((viewList[3]as! FormFields).txtFld_UserValue.text?.trimmingCharacters(in: whitespaceSet).isEmpty)!
        {
            (viewList[3]as! FormFields).txtFld_UserValue.text = ""
            (viewList[3] as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Location",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[3])
        }
        if((viewList[4]as! FormFields).txtFld_UserValue.text?.isEmpty)!
        {
            (viewList[4]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Mobile No.",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[4])
        }
        else if ((viewList[4]as! FormFields).txtFld_UserValue.text?.trimmingCharacters(in: whitespaceSet).isEmpty)!
        {
            (viewList[4]as! FormFields).txtFld_UserValue.text = ""
            (viewList[4] as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Mobile No.",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[4])
        }
        else if(((viewList[4]as! FormFields).text?.characters.count)! > 15) //|| (((viewList[4]as! FormFields).txtFld_UserValue.text?.characters.count)! < 15)
        {
            (viewList[4]as! FormFields).txtFld_UserValue.text = ""
            (viewList[4]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter 15 Digit Contact Number",attributes: [NSAttributedString.Key.foregroundColor : UIColor.red])
            viewsToReturn.append(viewList[4])
        }
        
        if((viewList[5]as! FormFields).txtFld_UserValue.text?.isEmpty)!
        {
            (viewList[5]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Email Address",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[5])
        }
        else if (!(isValidEmail(testStr: (viewList[5]as! FormFields).txtFld_UserValue.text!)))
        {
            (viewList[5]as! FormFields).txtFld_UserValue.text = ""
            (viewList[5]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Valid Email Address",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[5])
        }
        else if ((viewList[5]as! FormFields).txtFld_UserValue.text?.trimmingCharacters(in: whitespaceSet).isEmpty)!
        {
            (viewList[5]as! FormFields).txtFld_UserValue.text = ""
            (viewList[5] as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Valid Email Address",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[5])
        }
//        if(((viewList[5]as! FormFields).txtFld_UserValue.text?.characters.count)! > 20)
//        {
//            (viewList[5]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "You can enter maximum 20 characters.",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
//            viewsToReturn.append(viewList[5])
//        }
         if ((viewList[6]as! FormFields).txtFld_UserValue.text?.trimmingCharacters(in: whitespaceSet).isEmpty)!
        {
            (viewList[6]as! FormFields).txtFld_UserValue.text = ""
            (viewList[6]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Valid Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[6])
        }
        else if ((viewList[6]as! FormFields).txtFld_UserValue.text?.isEmpty)!
        {
            (viewList[6]as! FormFields).txtFld_UserValue.text = ""
            (viewList[6] as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[6])
        }
         if ((viewList[7]as! FormFields).txtFld_UserValue.text?.isEmpty)!
        {
            (viewList[7]as! FormFields).txtFld_UserValue.text = ""
            (viewList[7]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Confirm Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[7])
        }
        else if ((viewList[7]as! FormFields).txtFld_UserValue.text?.trimmingCharacters(in: whitespaceSet).isEmpty)!
        {
            (viewList[7]as! FormFields).txtFld_UserValue.text = ""
            (viewList[7] as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Valid Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[7])
        }
        
       return  viewsToReturn
    }
    
    func validationForCreateAd(viewList : [UIView]) -> Array<Any>
    {
        var viewsToReturn = [UIView]()
        if((viewList[0]as! FormFields).txtFld_UserValue.text?.isEmpty)!
        {
            (viewList[0]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter title",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[0])
        }
        else if ((viewList[0]as! FormFields).txtFld_UserValue.text?.trimmingCharacters(in: whitespaceSet).isEmpty)!
        {
            (viewList[0]as! FormFields).txtFld_UserValue.text = ""
            (viewList[0] as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter title",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[0])
        }
        if(((viewList[0]as! FormFields).txtFld_UserValue.text?.characters.count)! > 20)
        {
            (viewList[0]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "You can enter maximum 20 characters.",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[0])
        }
        
        if((viewList[1]as! FormFields).txtFld_UserValue.text?.isEmpty)!
        {
            (viewList[1]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter URL",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[1])
        }
        else if ((viewList[1]as! FormFields).txtFld_UserValue.text?.trimmingCharacters(in: whitespaceSet).isEmpty)!
        {
            (viewList[1]as! FormFields).txtFld_UserValue.text = ""
            (viewList[1] as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter URL",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[1])
        }
        
        if((viewList[3]as! FormFields).txtFld_UserValue.text?.isEmpty)!
        {
            (viewList[3]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[3])
        }
        else if ((viewList[3]as! FormFields).txtFld_UserValue.text?.trimmingCharacters(in: whitespaceSet).isEmpty)!
        {
            (viewList[3]as! FormFields).txtFld_UserValue.text = "Enter Location"
            (viewList[3] as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Location",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[3])
        }
        
        
        if((viewList[2]as! FormFields).txtFld_UserValue.text?.isEmpty)!
        {
            (viewList[2]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter duration of advertise",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[2])
        }
        else if ((viewList[2]as! FormFields).txtFld_UserValue.text?.trimmingCharacters(in: whitespaceSet).isEmpty)!
        {
            (viewList[2]as! FormFields).txtFld_UserValue.text = ""
            (viewList[2] as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter duration of advertise",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[2])
        }
        
        return  viewsToReturn
    }
    
    
    func validationForEdiProfile(viewList : [UIView]) -> Array<Any>
    {
        var viewsToReturn = [UIView]()
        if((viewList[0]as! FormFieldsEditProfile).txtFld_UserValue.text?.isEmpty)!
        {
            (viewList[0]as! FormFieldsEditProfile).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter First Name",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[0])
        }
        else if ((viewList[0]as! FormFieldsEditProfile).txtFld_UserValue.text?.trimmingCharacters(in: whitespaceSet).isEmpty)!
        {
            (viewList[0]as! FormFieldsEditProfile).txtFld_UserValue.text = ""
            (viewList[0] as! FormFieldsEditProfile).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter First Name",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[0])
        }
        
        if((viewList[1]as! FormFieldsEditProfile).txtFld_UserValue.text?.isEmpty)!
        {
            (viewList[1]as! FormFieldsEditProfile).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Last Name",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[1])
        }
        else if ((viewList[1]as! FormFieldsEditProfile).txtFld_UserValue.text?.trimmingCharacters(in: whitespaceSet).isEmpty)!
        {
            (viewList[1]as! FormFieldsEditProfile).txtFld_UserValue.text = ""
            (viewList[1] as! FormFieldsEditProfile).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Last Name",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[1])
        }
        
        if((viewList[2]as! FormFieldsEditProfile).txtFld_UserValue.text?.isEmpty)!
        {
            (viewList[2]as! FormFieldsEditProfile).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Please Select Birth Date",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[2])
        }
        
        if((viewList[3]as! FormFieldsEditProfile).txtFld_UserValue.text?.isEmpty)!
        {
            (viewList[3]as! FormFieldsEditProfile).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Location",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[3])
        }
        else if ((viewList[3]as! FormFieldsEditProfile).txtFld_UserValue.text?.trimmingCharacters(in: whitespaceSet).isEmpty)!
        {
            (viewList[3]as! FormFieldsEditProfile).txtFld_UserValue.text = ""
            (viewList[3] as! FormFieldsEditProfile).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Location",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[3])
        }
        if((viewList[4]as! FormFieldsEditProfile).txtFld_UserValue.text?.isEmpty)!
        {
            (viewList[4]as! FormFieldsEditProfile).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Mobile No.",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[4])
        }
        
//        else if(((viewList[4]as! FormFields).txtFld_UserValue.text?.characters.count)! > 15)
//                {
//                    (viewList[4]as! FormFields).txtFld_UserValue.text = ""
//                    (viewList[4]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Max length should not exceed 15.",attributes: [NSAttributedString.Key.foregroundColor : UIColor.red])
//                    viewsToReturn.append(viewList[4])
//                }
        
        
        
        
//        else if ((viewList[4]as! FormFieldsEditProfile).txtFld_UserValue.text?.trimmingCharacters(in: whitespaceSet).isEmpty)!
//        {
//            (viewList[4]as! FormFieldsEditProfile).txtFld_UserValue.text = ""
//            (viewList[4] as! FormFieldsEditProfile).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Mobile No.",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
//            viewsToReturn.append(viewList[4])
//        }
//        else if(((viewList[4]as! FormFieldsEditProfile).text?.characters.count)! > 10) || (((viewList[4]as! FormFieldsEditProfile).txtFld_UserValue.text?.characters.count)! < 10)
//        {
//            (viewList[4]as! FormFieldsEditProfile).txtFld_UserValue.text = ""
//            (viewList[4]as! FormFieldsEditProfile).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter 10 Digit Contact Number",attributes: [NSAttributedString.Key.foregroundColor : UIColor.red])
//            viewsToReturn.append(viewList[4])
//        }
        
        return  viewsToReturn
    }
    
    func validationForLogin(viewList : [UIView]) -> Array<Any>
    {
        var viewsToReturn = [UIView]()
        if((viewList[0]as! FormFields).txtFld_UserValue.text?.isEmpty)!
        {
            (viewList[0]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Please enter valid email address or Mobile No.",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[0])
        }
        
        else if ((viewList[0]as! FormFields).txtFld_UserValue.text?.trimmingCharacters(in: whitespaceSet).isEmpty)!
        {
            (viewList[0]as! FormFields).txtFld_UserValue.text = ""
            (viewList[0] as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter email ID or Mobile No.",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[0])
        }
        
        if((viewList[1]as! FormFields).txtFld_UserValue.text?.isEmpty)!
        {
            (viewList[1]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[1])
        }
        else if ((viewList[1]as! FormFields).txtFld_UserValue.text?.trimmingCharacters(in: whitespaceSet).isEmpty)!
        {
            (viewList[1]as! FormFields).txtFld_UserValue.text = ""
            (viewList[1] as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[1])
        }
        else if(((viewList[1]as! FormFields).txtFld_UserValue.text?.characters.count)! < 6)
        {
            (viewList[1]as! FormFields).txtFld_UserValue.text = ""
            (viewList[1]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Password too small",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[1])
        
        }
        
        return  viewsToReturn
        
    }
    
    func validationForChangePassword(viewList : [UIView]) -> Array<Any>
    {
        var viewsToReturn = [UIView]()
    
    if((viewList[0]as! FormFields).txtFld_UserValue.text?.isEmpty)!
    {
      (viewList[0]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Old Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
      viewsToReturn.append(viewList[0])
    }
    else if ((viewList[0]as! FormFields).txtFld_UserValue.text?.trimmingCharacters(in: whitespaceSet).isEmpty)!
    {
      (viewList[0]as! FormFields).txtFld_UserValue.text = ""
      (viewList[0] as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Old Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
      viewsToReturn.append(viewList[0])
    }
        
        if((viewList[1]as! FormFields).txtFld_UserValue.text?.isEmpty)!
        {
            (viewList[1]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter New Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[1])
        }
        else if ((viewList[1]as! FormFields).txtFld_UserValue.text?.trimmingCharacters(in: whitespaceSet).isEmpty)!
        {
            (viewList[1]as! FormFields).txtFld_UserValue.text = ""
            (viewList[1] as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter New Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[1])
        }
        
        if((viewList[2]as! FormFields).txtFld_UserValue.text?.isEmpty)!
        {
            (viewList[2]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Confirm Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[2])
        }
        else if ((viewList[2]as! FormFields).txtFld_UserValue.text?.trimmingCharacters(in: whitespaceSet).isEmpty)!
        {
            (viewList[2]as! FormFields).txtFld_UserValue.text = ""
            (viewList[2] as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Confirm Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[2])
        }
        return viewsToReturn
    }

    func validationForResetPassword(viewList : [UIView]) -> Array<Any>
    {
        var viewsToReturn = [UIView]()
        
        if((viewList[0]as! FormFields).txtFld_UserValue.text?.isEmpty)!
        {
            (viewList[0]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter New Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[0])
        }
        else if ((viewList[0]as! FormFields).txtFld_UserValue.text?.trimmingCharacters(in: whitespaceSet).isEmpty)!
        {
            (viewList[0]as! FormFields).txtFld_UserValue.text = ""
            (viewList[0] as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter New Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[0])
        }
        
     
        
        if((viewList[1]as! FormFields).txtFld_UserValue.text?.isEmpty)!
        {
            (viewList[1]as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Confirm Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[1])
        }
        else if ((viewList[1]as! FormFields).txtFld_UserValue.text?.trimmingCharacters(in: whitespaceSet).isEmpty)!
        {
            (viewList[1]as! FormFields).txtFld_UserValue.text = ""
            (viewList[1] as! FormFields).txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Confirm Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            viewsToReturn.append(viewList[1])
        }
        return viewsToReturn
    }

}
