//
//  WebServices.swift
//  TrenderAlert
//
//  Created by Admin on 31/05/17.
//  Copyright © 2017 HPL. All rights reserved.
//

import UIKit
import Alamofire
import Foundation
import MBProgressHUD 

enum UserServices : String {
    case NormalLogin = "token"
    case SignUp = "api/ApiAccount/Register"
    case verifyOtp = "api/apiAccount/VerifyOTP"
    case ForgotPassword = "api/apiAccount/ForgotPassword"
    case setUserName = "api/profile/SetUserName"
    case changePasssword = "api/apiAccount/PasswordChange"
    case resendOtp = "api/apiAccount/ResendOTP"
    case updateEditBioInfo = "api/Profile/AddUserBio"
    case getUserInfo = "api/profile/GetUserInformation"
    case deleteTrendTags = "api/alert/RemoveTagLocation"
    case getTrendTags = "api/Tags/Get"
    case setTrendTags = "api/Tags/Save"
    case deleteTrendAlerts = "api/alert/Delete"
    case setTrendAlerts = "api/alert/SaveTagLocation"
    case getTrendAlerts = "api/alert/Get"
    case getFollowerList = "api/profile/LoginUserFollowerList"
    case getAllTrends = "api/trend/GetAllTrends"
    case likeTrend = "api/Trend/LikeTrend/"
    case dislikeTrend = "api/Trend/UnlikeTrend/"
    case setTrend = "api/trend/SetTrend"
    case getColors = "api/ApiAccount/GetColors"
    case isFirstTime = "api/profile/IsLoginFirstTime"
    case uploadProfileImage = "api/profile/UploadProfile"
    case uploadCoverPic = "api/profile/UploadCoverPic"
    case createFolder = "api/trend/CreateFolder"
    case getFriendList = "api/Friend/ListOfFriends"
    case getFriendRequestList = "api/Friend/ListofFriendRequest"
    case getFriendSuggestionList = "api/Friend/FriendSuggestions"
    case acceptFriendRequest = "api/friend/Accept"
    case declineFriendRequest = "api/friend/Reject"
    case sendFriendRequest = "api/friend/SendFriendRequest"
    case removeFriendSuggestion = "api/friend/RemoveSuggestion"
    case markTrendFavourite = "api/trend/MakeTrendAsFavourite/"
    case markTrendUnfavourite = "api/trend/MakeTrendAsUnFavourite/"
    case getLikes = "api/trend/GetLikes"
    case followUser = "api/profile/FollowUser/"
    case unfollowUser = "api/profile/UnFollowUser/"
    case getMyTrends = "api/trend/GetMyTrends"
    case getGroupListing = "api/group/GroupListing"
    case getGroupSuggestion = "api/group/GetGroupSuggestionList"
    case createGroup = "api/group/SaveGroupDetails"
    case groupDetails = "api/group/GetGroupDetails"
    case getGroupMemberList = "api/group/ListofGroupUsers"
    case getGroupAdminList = "api/group/ListofAdmins"
    case makeAdmin = "api/group/MakeAdmin"
    case removeMemberFromGroup = "api/group/RemoveMember"
    case getFriendListForGroup = "api/group/UsersToAddInGroup"
    case addGroupMembers = "api/group/AddGroupMembers"
    case muteUnmuteMember = "api/group/MuteUnmuteGroupMember"
    case getAllNotifications = "api/profile/UserNotifications"
    case makeUserPremium = "api/Profile/AddUserReceipt"
    case getSettingInfo = "api/alertsettings/Get"
    case saveSettingInfo = "api/alertsettings/Save"
    case editBio = "api/Profile/EditUserBio"
    case saveToFolder = "api/trend/SaveToFolder"
    case trendView = "api/trend/TrendView/"
    case getViews = "api/trend/GetTrendViewList"
    case shareTrend = "api/share/sharetrend"
    case getAllCommentsByTrend = "api/Comment/GetCommentsbyTrends"
    case addComment = "api/comment/ManageComments"
    case addReply = "api/Comment/ManageReplies"
    case getAllReply = "api/comment/GetReplies"
    case likeUnLikeComment = "api/Comment/LikeComment"
    case likeUnLikeReply = "api/Comment/LikeReply"
    case getAllGIF = "api/Admin/GIFListing"
    case deleteComment = "api/Comment/DeleteComment"
    case deleteRply = "api/Comment/DeleteReply"
    case addAudioComment = "api/comment/AddCommentAudio"
    case myFavouriteTrend = "api/trend/GetMyFavouriteTrends"
    case getGroupDiscussion = "api/group/GetAllTrendsGroup"
    case unfriend = "api/friend/Unfriend"
    case getTrenders = "api/profile/GetUsersFollowsYou"
    case getTrendSetters = "api/profile/GetUsersYouFollow"
    case getOtherUserTrends = "api/trend/GetOtherUserTrends"
    case followUnfollowTrender = "api/profile/FollowUnfollowUser/"
    case blockUnblockUser = "api/friend/BlockUnblockUser"
    case getTrendsForGridView = "api/trend/GetTrendsForGridview"
    case getAllContest = "api/MonthlyContest/GetMonthlyContest"
    case getContestDetails = "api/MonthlyContest/GetDetails"
    case getContestWinner = "api/monthlycontest/GetContestWinner"
    case participationContest = "api/monthlycontest/Participate"
    case getHottestTrends = "api/trend/GetHottestTrends"
    case getTrendById = "api/trend/GetTrendById"
    case getTagByName = "api/Tags/GetTagByName"
    case getTagByPreference = "api/trend/GetTrendByTagGrid"
    case searchPlaceForTrend = "api/trend/SearchPlacesForTrend"
    case cancelFriendRequest = "api/friend/CancelRequest/"
    case likeUnlikeTrend = "api/trend/LikeUnlikeTrend/"
    case getChat = "api/chat/getchat"
    case deleteTrend = "api/trend/DeleteTrend/"
    case getPeopleList = "api/profile/UserList"
    case hottestTrendsGrid = "api/trend/GetHottestTrendsGrid"
    case trenderTravel = "api/trend/search"
    case getBlockListUser = "api/friend/ListofBlockedUsers"
    case getMyTrendsGrid = "api/trend/GetMyTrendsGrid"
    case favouriteTrendGrid = "api/trend/GetMyFavouriteTrendsGrid"
    case otherUserTrendGrid = "api/trend/GetOtherUserTrendsGrid"
    case getGroupGrid = "api/group/GetAllTrendsGroupGrid"
    case tagByPreferenceList = "api/trend/GetTrendByTagPreferenceList"
    case SearchPlacesForTrendList = "api/trend/SearchPlacesForTrendList"
    case sharedTrendDetails = "api/share/GetSharedTrendDetails"
    case getMonthlyContestTrendList = "api/monthlycontest/GetMonthlyContestTrendList"
    case getTrendDetailsByTrendId = "api/trend/GetTrendDetailsByTrendId"
    case deleteImage = "api/trend/DeleteTrendImage"
    case deleteVideo = "api/trend/DeleteTrendVideo"
    case deleteStatus = "api/trend/DeleteTrendTextStatus"
    case clearChat = "api/chat/Clearchats"
    case requestToJoinGroup = "api/group/RequestJoinGroup"
    case getAdvertiseList = "api/Admin/GetAllAdvertisement"
    case getBuisnessTrendList = "api/BusinessAccount/GetAllTrends"
    case setTrendPushNotificationStatus = "/api/trend/SetTrendPushNotificationStatus/"
    case deleteChat = "api/chat/Deletechats"
    case acceptFollowRequest = "api/Profile/AcceptFollowRequest"
    case declineFollowRequest = "api/Profile/RejectFollowRequest"
    case groupInvite = "api/group/UserResponseGroupInvitation"
    case groupJoinRequest = "api/group/RespondJoinGroup"
    case getInteractionGraph = "api/BusinessAccount/InteractionsGraph"
    case getDiscoveryGraph = "api/BusinessAccount/DiscoveryGraph"
    case setInteractions = "api/BusinessAccount/SetInteractions"
    case audienceGraph = "api/BusinessAccount/GetAudienceGraph"
    case getMessageNotification = "api/profile/GetMessageNotifications"
    case readNotification = "api/profile/ReadNotification"
    case leaveGroup = "api/group/LeaveGroup"
    case remainingGroupMemberCount = "/api/group/GetUserCount"
    case saveLanguage = "api/Profile/SaveLanguage/"
}

typealias serviceCompletionHandler = (_ serviceResponse : [String : AnyObject]?) -> Void

typealias serviceCompletionHandlerArray = (_ serviceResponse : AnyObject?) -> Void

class WebServices: NSObject {
    
   static let baseURL : String = "https://trenderalert.azurewebsites.net/"

//   static let baseURL : String = "http://172.16.3.11:9091/"
    
    //gayatri local
//    static let baseURL : String = "http://172.16.2.50:92/"
    
    
    func callUserService1(service : UserServices, urlParameter: String, parameters : [String:AnyObject]?, isLazyLoading : Bool ,isHeader : Bool, CallMethod : HTTPMethod,actionAfterServiceResponse completionHandler: @escaping ((_ serviceResponse: [String:AnyObject], _ data: Data)-> Void))
    {
        if (NetworkReachabilityManager()!.isReachable) {
            if isLazyLoading == false
            {
               self.showHUD()
            }
            var TokenValue = String()
            if(isHeader == true)
            {
                TokenValue = "Bearer " + MySingleton.shared.loginObject.access_token

                print(TokenValue)
            }

           Alamofire.request("\(type(of: self).baseURL)\(service.rawValue)\(urlParameter)", method: CallMethod, parameters: parameters, encoding: URLEncoding.default, headers: ["Authorization": TokenValue]).responseJSON
                { response in
                    print(response.request as Any)  // original URL request
                    print(response.response as Any) // URL response
                    print(response.data as Any)     // server data
                    print(response.result)   // result of response serializatio
                    switch response.result {
                    case .success:
                        DispatchQueue.main.async {
                            self.dismissHUD()
                        }

                        if response.response?.statusCode != 200 {
                            if response.response?.statusCode == 401
                            {
                                UserDefaults.standard.set(nil, forKey: "access_token")
                                let appDelegatge = UIApplication.shared.delegate as! AppDelegate
                                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                                let loginNavigationController = mainStoryBoard.instantiateViewController(withIdentifier: "navigationController") as! UINavigationController
                                appDelegatge.window!.rootViewController = loginNavigationController
                                appDelegatge.window!.makeKeyAndVisible()
                                return
                            }
                            if service == .NormalLogin
                            {
                                if let error = (response.result.value as! [String:AnyObject])["error_description"] as? String
                                {
                                    TrenderAlertVC.shared.presentAlertController(message: error, completionHandler: nil)
                                }
                                return
                            }
                            TrenderAlertVC.shared.presentAlertController(message: "An error has occurred.", completionHandler: nil)
                        }
                        else
                        {
                            if(response.result.value is [String:AnyObject])
                            {
                                if let JSON  = response.result.value as? [String:AnyObject]{
                                    print(JSON)
                                    completionHandler(JSON, response.data!)
                            }
                            }
                        }
                    case .failure(let error):
                        DispatchQueue.main.async {
                            self.dismissHUD()
                        print(error)
                        TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                        }
                    }
            }
        }
        else
        {
            DispatchQueue.main.async {
                TrenderAlertVC.shared.presentAlertController(message: "Please check your internet connection", completionHandler: nil)
            }
        }
    }
    
    func callUserService(service : UserServices, urlParameter: String, parameters : [String:AnyObject]?, isLazyLoading : Bool ,isHeader : Bool, CallMethod : HTTPMethod,actionAfterServiceResponse completionHandler: @escaping ((_ serviceResponse: [String:AnyObject], _ data: Data)-> Void))
    {
        print(parameters)
        var request = NSMutableURLRequest()
        if (NetworkReachabilityManager()!.isReachable) {
            if isLazyLoading == false
            {
                DispatchQueue.main.async {
                    self.showHUD()
                }
            }
                var urlStr: String = "\(WebServices.baseURL)\(service.rawValue)\(urlParameter)"
                urlStr = urlStr.replacingOccurrences(of: " ", with: "&")
            print(urlStr)
                request.httpMethod = CallMethod.rawValue
                if CallMethod.rawValue == "POST"
                {
                        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                    
//                    request.httpBody = try! JSONSerialization.data(withJSONObject: parameters!, options: .prettyPrinted)
                    do
                    {
                        if parameters != nil
                        {
                            let jsonData = try JSONSerialization.data(withJSONObject: parameters!, options: JSONSerialization.WritingOptions.prettyPrinted)
                            request.httpBody = jsonData
                        }
                    }
                    catch let error as NSError
                    {
                        DispatchQueue.main.async {
                            self.dismissHUD()
                            TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                        }
                    }
                }

                if(isHeader == true)
                {
                    request.addValue("Bearer " + MySingleton.shared.loginObject.access_token, forHTTPHeaderField: "Authorization")
                }
                request.url = NSURL(string:urlStr)! as URL

                let dataTask = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
                    DispatchQueue.main.async {
                        self.dismissHUD()
                    }
                    if error != nil
                    {
                        DispatchQueue.main.async {
                            TrenderAlertVC.shared.presentAlertController(message: error!.localizedDescription, completionHandler: nil)
                        }
                    }
                    if (response as? HTTPURLResponse)?.statusCode != 200
                    {
                        if (response as? HTTPURLResponse)?.statusCode == 401
                        {
                            DispatchQueue.main.async {
                            UserDefaults.standard.set(nil, forKey: "access_token")
                            let appDelegatge = UIApplication.shared.delegate as! AppDelegate
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let loginNavigationController = mainStoryBoard.instantiateViewController(withIdentifier: "navigationController") as! UINavigationController
                            appDelegatge.window!.rootViewController = loginNavigationController
                            appDelegatge.window!.makeKeyAndVisible()
                            return
                            }
                        }
                        else if (response as? HTTPURLResponse)?.statusCode == 400
                        {
                        
                        if service == .NormalLogin
                        {
                            do
                            {
                                let responseData = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                                print(responseData)
                            if let error = responseData["error_description"] as? String
                            {
                                TrenderAlertVC.shared.presentAlertController(message: error, completionHandler: nil)
                            }
                            }catch let error as NSError
                            {
                               print("error")
                            }
                            return
                        }
                        else{
                            DispatchQueue.main.async {
                                print("Status code = \(String(describing: (response as? HTTPURLResponse)?.statusCode))")
                                TrenderAlertVC.shared.presentAlertController(message: "An error has occurred.", completionHandler: nil)
                            }
                            }
                        }
                        else{
                                DispatchQueue.main.async {
                                print("Status code = \(String(describing: (response as? HTTPURLResponse)?.statusCode))")
                            TrenderAlertVC.shared.presentAlertController(message: "An error has occurred.", completionHandler: nil)
                            }
                        }
                        
                    }
                    else
                    {
                        do
                        {
                            let responseData = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments) as! [String:AnyObject]
                            print(responseData)
                            if(responseData["Status"] as! Bool == true)
                            {
                                completionHandler(responseData, data!)
                            }
                            else
                            {
                                DispatchQueue.main.async {
                                TrenderAlertVC.shared.presentAlertController(message: responseData["Message"] as! String, completionHandler: nil)
                                }
                            }
                            
                        }
                        catch let error as NSError
                        {
                            DispatchQueue.main.async {
//                                self.dismissHUD()
                                TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                            }
                        }
                    }
                })
                dataTask.resume()
        }
        else
        {
            DispatchQueue.main.async {
                TrenderAlertVC.shared.presentAlertController(message: "Please check your internet connection", completionHandler: nil)
            }
        }
    }
    
    func decodeDataToClass<classType: Codable>(data: Data, decodeClass: classType.Type) -> classType? {
        do {
            let classObject = try JSONDecoder().decode(decodeClass.self, from: data)
            return classObject
        }
        catch let error {
            print("Codable Error --- \(error)")
        }
        return nil
    }
    
    
    

    func callUserServiceArrayID(service : String,parameters : [String:AnyObject]?, isHeader : Bool, CallMethod : HTTPMethod ,actionAfterServiceResponse completionHandler: @escaping ((_ serviceResponseArray: AnyObject)-> Void))
    {
        if (NetworkReachabilityManager()!.isReachable) {
            self.showHUD()
            var TokenValue = String()
            if(isHeader == true)
            {
                TokenValue = UserDefaults.standard.object(forKey: "access_token")! as! String
            }
            
            print(TokenValue)
            Alamofire.request("\(type(of: self).baseURL)\(service)", method: CallMethod, parameters: parameters, encoding: URLEncoding.default, headers: ["Authorization": TokenValue]).responseJSON
                { response in
                    DispatchQueue.main.async {
                        self.dismissHUD()
                    }
                    print(response.request as Any)  // original URL request
                    print(response.response as Any) // URL response
                    print(response.data as Any)     // server data
                    print(response.result)   // result of response serializatio
                    switch response.result {
                    case .success:
                        if response.response?.statusCode == 401
                        {
                            UserDefaults.standard.set(nil, forKey: "access_token")
                            let appDelegatge = UIApplication.shared.delegate as! AppDelegate
                            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                            let loginNavigationController = mainStoryBoard.instantiateViewController(withIdentifier: "navigationController") as! UINavigationController
                            appDelegatge.window!.rootViewController = loginNavigationController
                            appDelegatge.window!.makeKeyAndVisible()
                        }
                        else
                        {
                            if let JSON  = response.result.value
                            {
                                DispatchQueue.main.async {
                                    self.dismissHUD()
                                }
                                print(JSON)
                                
                                completionHandler(JSON as AnyObject)
                                
                            }
                        }
                    case .failure(let error):
                        DispatchQueue.main.async {
                            self.dismissHUD()
                        }
                        print(error)
                        TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                    }
                    
            }
        }else{
            TrenderAlertVC.shared.presentAlertController(message: "Please check your internet connection", completionHandler: nil)
            
        }
    }

    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
    func showHUD()
    {
        _ = MBProgressHUD.showAdded(to: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
    }
    
    func dismissHUD()
    {
        _ = MBProgressHUD.hide(for: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
    }

//    class TaggedFriends: NSObject, Codable {
//        var ProfileImage : String?
//        var UserProfileID = Int()
//        var Firstname = String()
//        var Lastname = String()
//
//        enum CodingKeys: String, CodingKey {
//            case Firstname
//            case Lastname
//            case ProfileImage
//            case UserProfileID
//        }
//    }
    
//    let params = ["SortColumn":SortColumn,"SortDirection":SortDirection,"Search":"","Skip":Skip,"Take":Take] as [String:Any]
//
//    WebServices().callUserService(service: .getAllTrends, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: lazyLoading, isHeader: true, CallMethod: .post) { (serviceResponse, responseData) in
//    print("Get All Trends --- \(serviceResponse)")
//    if(serviceResponse["Status"] as! Bool == true)
//    {
//
//    DispatchQueue.main.async {
//    let temp = WebServices().decodeDataToClass(data: responseData, decodeClass: TrendListData.self)!
//    self.trendData.data += temp.data
//
//    if self.trendData.data.count == 0
//    {
    
   
}
