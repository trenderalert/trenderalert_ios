//
//  GetAddress.swift
//  TrenderAlert
//
//  Created by Admin on 14/06/17.
//  Copyright © 2017 HPL. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import CoreLocation
import MBProgressHUD

typealias googleAddress = (_ locationData:[[String:String]]) -> Void

typealias AddressComplitionHandler = (_ coordinate:CLLocationCoordinate2D,_ zipcode:String?) -> Void

class GetAddress: NSObject {
    func getAddress(location_str:String,block:@escaping googleAddress) -> Void
    {
//        GMSServices.provideAPIKey("AIzaSyBzISI2Gp2VHusOdWSDmfiVYnuZadsZZNs")
//        GMSPlacesClient.provideAPIKey("AIzaSyBzISI2Gp2VHusOdWSDmfiVYnuZadsZZNs")

        GMSPlacesClient.provideAPIKey("AIzaSyDOqh8kBUF2_tbQF_uc2HiRPCGFC77_vE4")
        
        let filter = GMSAutocompleteFilter()
        filter.type = GMSPlacesAutocompleteTypeFilter.city
        
        GMSPlacesClient.shared().autocompleteQuery(location_str, bounds: nil, filter: filter) { (results, error) in
            guard error == nil else {
                print("Autocomplete error \(error)")
                return
            }
            var location_arr = [[String:String]]()
            print(results!)
            for result in results!
            {
                var location_dict = [String:String]()
                location_dict["city"] = result.attributedPrimaryText.string
                location_dict["address"] = result.attributedFullText.string
                //                location_dict.setObject(result.attributedPrimaryText.string, forKey: "city" as NSCopying)
                //                location_dict.setObject(result.attributedFullText.string, forKey : "address" as NSCopying)
                print(location_dict)
                location_arr.insert(location_dict, at: location_arr.count)
            }
            if location_arr.count != 0
            {
                block(location_arr)
            }
        }
    }
    
    func getLocationFromAddressString(addressStr: String,block:@escaping AddressComplitionHandler) -> Void {
        showHUD()
        CLGeocoder().geocodeAddressString(addressStr, completionHandler: {(placemarks, error) -> Void in
            if((error) != nil){
                print("Error", error!)
            }
            
            if let placemark = placemarks?.first {
                if let coordinate = placemark.location?.coordinate
                {
                    block(coordinate, placemark.postalCode)
                }
            }
            DispatchQueue.main.async {
                self.dismissHUD()        }
        })
    }
    
    func getAddressFromLocation(_ location: CLLocation,_ completionHandler: @escaping ((_ placemark: String)-> Void))
    {
        //        self.showHUD()
        CLGeocoder().reverseGeocodeLocation(location) { (placemark, error) in
            print(placemark!)
            //            DispatchQueue.main.async {
            //                self.dismissHUD()
            //            }
            completionHandler(("\(placemark!.first!.addressDictionary!["State"] as? String ?? placemark!.first!.addressDictionary!["City"] as! String) \(placemark!.first!.addressDictionary!["Country"] as! String)"))
            
        }
    }
    
    func getAddressFromLocationSetTrendCity(_ location: CLLocation,_ completionHandler: @escaping ((_ placemark: String)-> Void))
    {
        //        self.showHUD()
        CLGeocoder().reverseGeocodeLocation(location) { (placemark, error) in
            print(placemark!)
        
            completionHandler("\(placemark!.first!.addressDictionary!["City"] as! String)")
            
        }
    }
    
    func getAddressFromLocationSetTrendCountry(_ location: CLLocation,_ completionHandler: @escaping ((_ placemark: String)-> Void))
    {
        //        self.showHUD()
        CLGeocoder().reverseGeocodeLocation(location) { (placemark, error) in
            print(placemark!)
            
            completionHandler("\(placemark!.first!.addressDictionary!["Country"] as! String)")
            
        }
    }
    
    func getAddressFromLocationTravel(_ location: CLLocation,_ completionHandler: @escaping ((_ placemark: String)-> Void))
    {
        //        self.showHUD()
        CLGeocoder().reverseGeocodeLocation(location) { (placemark, error) in
            print(placemark ?? "")
            if placemark != nil
            {
                if placemark!.first!.addressDictionary!["City"] != nil
                {
                     completionHandler("\(placemark!.first!.addressDictionary!["City"] as! String)")
                }
                else
                {
                    completionHandler("\(placemark!.first!.addressDictionary!["State"] as! String)")
                }
               
            }
            else
            {
                print("placemark nil")
                print(error?.localizedDescription)
                 completionHandler("")
            }
        }
    }
    
    func showHUD()
    {
        _ = MBProgressHUD.showAdded(to: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
    }
    func dismissHUD()
    {
        _ = MBProgressHUD.hide(for: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
    }
    
//    func googleMapsiOSSDKReverseGeocoding(_ location: CLLocation) {
//        let aGMSGeocoder: GMSGeocoder = GMSGeocoder()
//        aGMSGeocoder.reverseGeocodeCoordinate(CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)) {
//            ( gmsReverseGeocodeResponse: GMSReverseGeocodeResponse!, let error: NSError!) -> Void in
//            
//            let gmsAddress: GMSAddress = gmsReverseGeocodeResponse.firstResult()
//            print("\ncoordinate.latitude=\(gmsAddress.coordinate.latitude)")
//            print("coordinate.longitude=\(gmsAddress.coordinate.longitude)")
//            print("thoroughfare=\(gmsAddress.thoroughfare)")
//            print("locality=\(gmsAddress.locality)")
//            print("subLocality=\(gmsAddress.subLocality)")
//            print("administrativeArea=\(gmsAddress.administrativeArea)")
//            print("postalCode=\(gmsAddress.postalCode)")
//            print("country=\(gmsAddress.country)")
//            print("lines=\(gmsAddress.lines)")
//            } as! GMSReverseGeocodeCallback as! GMSReverseGeocodeCallback as! GMSReverseGeocodeCallback
//    }

}
