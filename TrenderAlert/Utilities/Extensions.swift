//
//  Extensions.swift
//  Axess
//
//  Created by HPL on 18/09/18.
//  Copyright © 2018 HPL. All rights reserved.
//

import UIKit

class Extensions: NSObject {
    

}

extension UIView {
    func giveBorderColor(color : UIColor)
    {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = 1.0
    }
    
    func makeCornerRadius(radius : CGFloat)
    {
        DispatchQueue.main.async {
            self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
        self.layoutIfNeeded()
        }
    }
    
    public func round() {
        let width = bounds.width < bounds.height ? bounds.width : bounds.height
        let mask = CAShapeLayer()
            //mask.path = UIBezierPath(ovalInRect: CGRect(bounds.midX - width / 2, bounds.midY - width / 2, width, width)).cgPath
        mask.path = UIBezierPath(ovalIn: CGRect(x: bounds.midX - width / 2, y: bounds.midY - width / 2, width: width, height: width)).cgPath
        self.layer.mask = mask
    }

    
    func addGradientTo(startColor colorOne: UIColor, endColor colorTwo: UIColor, isVertical verticalFlag: Bool) {
        let gradient = CAGradientLayer()
        gradient.frame = bounds
        gradient.colors = [colorOne.cgColor, colorTwo.cgColor]
        
        if !verticalFlag
        {
            gradient.startPoint = CGPoint.zero
            gradient.endPoint = CGPoint(x: 1, y: 0)
        }
        
        layer.insertSublayer(gradient, at: 0)
    }
    
    @IBInspectable var borderWidth: Double {
        get {
            return Double(self.layer.borderWidth)
        }
        set {
            self.layer.borderWidth = CGFloat(newValue)
        }
    }
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        }
        set {
            self.layer.borderColor = newValue?.cgColor
        }
    }
    
    }

extension UIColor {
    class var baseColor: UIColor
    {
        get
        {
            return UIColor(red: 254.0/255.0, green: 194.0/255.0, blue: 16.0/255.0, alpha: 1.0 )//yellow color
        }
    }
    
    class var friendsSBGrayColor:UIColor{
        get{
            return UIColor(red: 243.0/255, green: 243.0/255, blue: 243.0/255, alpha: 1.0)
        }
    }
    
    class var friendSBGrayFontColor:UIColor{
        get{
            return UIColor(red: 139.0/255, green: 139.0/255, blue: 139.0/255, alpha: 1.0)
        }
    }
    
    class var tagColor:UIColor{
        get{
            return UIColor(red: 39.0/255, green: 171.0/255, blue: 250.0/255, alpha: 1.0)
        }
    }
}

extension UITextField{
    
    func leftPadding(paddingSize:Int){
        self.leftView = UIView(frame: CGRect(x: 0, y: 0, width: paddingSize, height: Int(self.frame.height)))
        self.leftViewMode = .always
    }
    
    func rightPadding(paddingSize:Int){
        self.leftView = UIView(frame: CGRect(x: 0, y: 0, width: paddingSize, height: Int(self.frame.height)))
        self.rightViewMode = .always
    }
    
    func setBottomBorder(borderColor:CGColor = UIColor.gray.cgColor,borderHeight:Double = 1.0,borderWidth:Double = 0.0,backgroundColor:CGColor = UIColor.white.cgColor) {
        self.borderStyle = .none
        self.layer.backgroundColor = backgroundColor
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = borderColor
        self.layer.shadowOffset = CGSize(width: borderWidth, height: borderHeight)
        self.layer.shadowOpacity = 1.0
        self.layer.shadowRadius = 0.0
    }
}

extension UITableViewCell {
    /// Search up the view hierarchy of the table view cell to find the containing table view
    var tableView: UITableView? {
        get {
            var table: UIView? = superview
            while !(table is UITableView) && table != nil {
                table = table?.superview
            }
            
            return table as? UITableView
        }
    }
}

extension UIDatePicker {
func set16YearValidation() {
    let currentDate: Date = Date()
    var calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
    calendar.timeZone = TimeZone(identifier: "UTC")!
    var components: DateComponents = DateComponents()
    components.calendar = calendar
    components.year = -16
    let maxDate: Date = calendar.date(byAdding: components, to: currentDate)!
    components.year = -150
    let minDate: Date = calendar.date(byAdding: components, to: currentDate)!
    self.minimumDate = minDate
    self.maximumDate = maxDate
}
}

extension UITextView{
    
    func numberOfLines() -> Int{
        if let fontUnwrapped = self.font{
            return Int(self.contentSize.height / fontUnwrapped.lineHeight)
        }
        return 0
    }
    
}
extension Date {
    
    func timeAgoSinceDate() -> String {
        
        // From Time
        let fromDate = self
        
        // To Time
        let toDate = Date()
        
        // Estimation
        // Year
        if let interval = Calendar.current.dateComponents([.year], from: fromDate, to: toDate).year, interval > 0  {
            
            return interval == 1 ? "\(interval)" + " " + "year ago" : "\(interval)" + " " + "years ago"
        }
        
        // Month
        if let interval = Calendar.current.dateComponents([.month], from: fromDate, to: toDate).month, interval > 0  {
            
            return interval == 1 ? "\(interval)" + " " + "month ago" : "\(interval)" + " " + "months ago"
        }
        
        // Day
        if let interval = Calendar.current.dateComponents([.day], from: fromDate, to: toDate).day, interval > 0  {
            
            return interval == 1 ? "\(interval)" + " " + "day ago" : "\(interval)" + " " + "days ago"
        }
        
        // Hours
        if let interval = Calendar.current.dateComponents([.hour], from: fromDate, to: toDate).hour, interval > 0 {
            
            return interval == 1 ? "\(interval)" + " " + "hour ago" : "\(interval)" + " " + "hours ago"
        }
        
        // Minute
        if let interval = Calendar.current.dateComponents([.minute], from: fromDate, to: toDate).minute, interval > 0 {
            
            return interval == 1 ? "\(interval)" + " " + "minute ago" : "\(interval)" + " " + "minutes ago"
        }
        return "a moment ago"
    }
}

extension String{
    func localizableString(loc:String)->String{
        let path = Bundle.main.path(forResource: loc, ofType: "lproj")
        let bundle = Bundle(path: path!)
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
}
