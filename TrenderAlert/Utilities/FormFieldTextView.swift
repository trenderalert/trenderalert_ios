//
//  FormFieldTextView.swift
//  TrenderAlert
//
//  Created by HPL on 30/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class FormFieldTextView:UIView , UITextViewDelegate {
        
    @IBOutlet weak var img_IconPic: UIImageView!
        
    @IBOutlet weak var lbl_PlaceHolderTxt: UILabel!
    
    @IBOutlet weak var txtView_UserValue: UITextView!
    @IBOutlet weak var imgIconHeight: NSLayoutConstraint!
    
    
    var view: UIView!
    
    var textViewPlaceholder:String?{
        didSet{
            lbl_PlaceHolderTxt.text = textViewPlaceholder
        }
    }
    
    
    @IBInspectable var image : UIImage? {
        get {
            return img_IconPic.image
        }
        set(image) {
            img_IconPic.image = image
        }
    }
        

    
        @IBInspectable var text : String? {
            get {
                return lbl_PlaceHolderTxt.text
            }
            set(text) {
                lbl_PlaceHolderTxt.text = text
            }
        }
        
//        override func draw(_ rect: CGRect)
//        {
//
//
//
//        }
        func xibSetup() {
            view = loadViewFromNib()
            
            // use bounds not frame or it'll be offset
            view.frame = bounds
            
            // Make the view stretch with containing view
            view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
            // Adding custom subview on top of our view (over any custom drawing > see note below)
            
            let padding = txtView_UserValue.textContainer.lineFragmentPadding
            txtView_UserValue.textContainerInset = UIEdgeInsets(top: 0, left: -padding, bottom: padding, right: -padding)
            addSubview(view)
            
            lbl_PlaceHolderTxt.isHidden = true
            txtView_UserValue.delegate = self
            NotificationCenter.default.addObserver(
                self,
                selector: #selector(textViewTextChanged(sender:)),
                name: UITextView.textDidChangeNotification,
                object: nil
            )
        }
        
        func loadViewFromNib() -> UIView {
            
            //        let bundle = Bundle(for: type(of: self))
            let bundle = Bundle(for: self.classForCoder)
            //        let className = NSStringFromClass(self.classForCoder)
            let nib = UINib(nibName: "FormFieldTextView", bundle: bundle)
            let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
            
            return view
        }
        
        override init(frame: CGRect) {
            // 1. setup any properties here
            
            // 2. call super.init(frame:)
            super.init(frame: frame)
            
            // 3. Setup view from .xib file
            xibSetup()
        }
        
        required init?(coder aDecoder: NSCoder) {
            // 1. setup any properties here
            
            // 2. call super.init(coder:)
            super.init(coder: aDecoder)
            
            // 3. Setup view from .xib file
            xibSetup()
        }
    

    
        @objc func textViewTextChanged(sender : UITextView)
        {
//            if(txtView_UserValue.text?.isEmpty)!
//            {
//                lbl_PlaceHolderTxt.isHidden = true
//            }
//            else
//            {
//                lbl_PlaceHolderTxt.isHidden = false
//            }

         }
    
    
}

