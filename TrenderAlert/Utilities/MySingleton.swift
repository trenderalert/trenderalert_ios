//
//  MySingleton.swift
//  TrenderAlert
//
//  Created by Admin on 08/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class MySingleton: NSObject {
    static let shared = MySingleton()
    
    var username = String()
    var isForgotPassword = Bool()
    var loginObject = Login()
    var usersArray = [Login]()
    var isFirstTimeSideMenu = true
    var selectedLangData = LanguagesModel()
    var selectedLang = String()
}
