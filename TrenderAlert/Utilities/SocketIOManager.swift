//
//  SocketIoManager.swift
//  TrenderAlert
//
//  Created by Admin on 20/03/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import SocketIO

enum Event: String {
    //   case typing = "typing"
    case message = "sendcomment"
    case connectComment = "commentconnect"
    case connectRoom = "myroom"
    case receivedComment = "commentdata"
    case userLogin = "userlogin"
    case userConnected = "userconnected"
    case userDisconnected = "userdisconnected"
    case chatconnection = "chatconnection"
    case chatinitiate = "chatinitiate"
    case sendmessage = "sendmessage"
    case getmessage = "getmessage"
    case userStatus = "userstatus"
 
}

@objc protocol SocketIOManagerDelegate: class {
   
    @objc optional func connectRoom(_ data: [String:Any])
    @objc optional func userConnected(_ data: [String:Any], isOnline: Bool)
    @objc optional func messageReceived(_ data: [String:Any])
//    func messageReceived(_ data: CommentList)
}

@objc protocol SocketIOManagerDelegateReply: class {
 
    @objc optional func messageReceivedReply(_ data: [String:Any])
    //    func messageReceived(_ data: CommentList)
}

@objc protocol SocketIOManagerDelegateChat: class {
    
    @objc optional func messageReceivedChat(_ data: [String:Any])
    //    func messageReceived(_ data: CommentList)
}

@objc protocol SocketIOManagerDelegateUserStatus: class {
    
    @objc optional func userStatus(_ data: [String:Any])
}

class SocketIoManager : NSObject {
    
    static let sharedInstance = SocketIoManager()
//     var socket = SocketManager(socketURL: URL(string: "http://172.16.3.11:3100")!, config: [])
    var socket = SocketManager(socketURL: URL(string: "https://trenderalertsocket.azurewebsites.net")!, config: [])
    
//    var socket : SocketIOClient = SocketIOClient(socketURL: URL(string: "http://dev.techadox.com:8890")!)
    weak var socketIOManagerDelegate: SocketIOManagerDelegate!
    weak var socketIOManagerDelegateReply: SocketIOManagerDelegateReply!
    weak var socketIOManagerDelegateChat: SocketIOManagerDelegateChat!
    weak var socketIOManagerDelegateUserStatus: SocketIOManagerDelegateUserStatus!

    func establishConnection() {
        socket.connect()
        print("connection established....")
        socket.defaultSocket.onAny { (socketEvent) in
            print(socketEvent.event)
            
            switch Event(rawValue: socketEvent.event) {
            case .message?:
                if (self.socketIOManagerDelegate != nil)
                {
                    print("MessageReceived \(socketEvent.items?.first as! [String:Any])")
//                    self.socketIOManagerDelegate.messageReceived(socketEvent.items?.first as! CommentList)
                }
            case .connectComment?:
                if (self.socketIOManagerDelegate != nil)
                {
                    print("RoomConnect \(socketEvent.items?.first as! [String:Any])")
                //    self.socketIOManagerDelegate.connectRoom(socketEvent.items?.first as! [String:Any])
                }
            case .connectRoom?:
                if (self.socketIOManagerDelegate != nil)
                {
                    print("RoomReceived \(socketEvent.items?.first as! [String:Any])")
                    self.socketIOManagerDelegate.connectRoom!(socketEvent.items?.first as! [String:Any])
                    //    NotificationCenter.default.post(name: NSNotification.Name("RoomReceived"), object: nil)
                }
            case .receivedComment?:
                if (self.socketIOManagerDelegateReply != nil)
                {
                    print("Received Comment \(socketEvent.items?.first as! [String:Any])")
                    if (socketEvent.items?.first as! [String:Any])["IsReply"] as? Bool == true
                    {
                        self.socketIOManagerDelegateReply.messageReceivedReply!(socketEvent.items?.first as! [String:Any])
                    }
                    else
                    {
                        self.socketIOManagerDelegate.messageReceived!(socketEvent.items?.first as! [String:Any])
                    }
                    //    NotificationCenter.default.post(name: NSNotification.Name("RoomReceived"), object: nil)
                }
            case .userConnected?:
                print("User Connected \(socketEvent.items?.first as! [String:Any])")
//                self.socketIOManagerDelegate.userConnected!(socketEvent.items?.first as! [String:Any], isOnline: true)
            case .userDisconnected?:
                print("User Disconnected \(socketEvent.items?.first as! [String:Any])")
//                self.socketIOManagerDelegate.userConnected!(socketEvent.items?.first as! [String:Any], isOnline: false)

            case .chatconnection?:
                print("Chat Connected \(socketEvent.items?.first as! [String:Any])")
                
            case .getmessage?:
                print("Chat Message \(socketEvent.items?.first as! [String:Any])")
                if self.socketIOManagerDelegateChat != nil
                {
                    self.socketIOManagerDelegateChat.messageReceivedChat!(socketEvent.items?.first as! [String:Any])
                }
                
            case .userStatus?:
                print("User Status \(socketEvent.items?.first as! [String:Any])")
                if self.socketIOManagerDelegateUserStatus != nil
                {
                    self.socketIOManagerDelegateUserStatus.userStatus!(socketEvent.items?.first as! [String:Any])
                }
                
            default:
                print("Done")
            }
        }
    }
    
    func closeConnection() {
        socket.disconnect()
    }
  
    func sendDataToEvent(_ socketEvent: Event,data dict: [String:Any]) {
        socket.defaultSocket.emit(socketEvent.rawValue, dict)
        print(socketEvent.rawValue)
    }
    
}
