//
//  FullScreenViewController.swift
//  TrenderAlert
//
//  Created by HPL on 14/03/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

protocol TrendViewDelegate {
    func IncreaseTrendViewCount(Index: Int)
}

class FullScreenViewController: UIViewController {

    @IBOutlet var fullScreenPageControl: UIPageControl!
    @IBOutlet var fullScreenCollection: UICollectionView!
    var pageIndex = Int()
    var imageList = [TrendListImageData]()
    var isZooming = false
    var originalImageCenter:CGPoint?
    var viewVideo = PlayerView()
    var delegate : TrendViewDelegate?
    var cellIndex = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()

         fullScreenCollection.register(UINib(nibName: "FullScreenCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FullScreenCollectionViewCell")
         fullScreenPageControl.numberOfPages = imageList.count
//        if pageIndex != 0{
//            fullScreenCollection.scrollToItem(at: IndexPath(item: pageIndex, section: 0) , at: .centeredHorizontally, animated: false)
//        }
//        NotificationCenter.default.addObserver(self,
//                                               selector: #selector(self.playerItemDidReachEnd(_:)),
//                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
//                                               object: self.viewVideo)
        setupNavaigationbar()
//        updateTrendViewCount()
        self.fullScreenCollection.performBatchUpdates(nil, completion: {
            (result) in
            // ready
            print("loaded")
            if self.pageIndex != 0{
                self.fullScreenCollection.scrollToItem(at: IndexPath(item: self.pageIndex, section: 0) , at: .centeredHorizontally, animated: false)
            }
        })

    }
    
    func setupNavaigationbar(){
        Utilities.setNavigationBarWithTitle(viewController: self, title: "")
        let leftButton = UIBarButtonItem(image: UIImage(named: "img_BackArrow"), style: .plain, target: self, action: #selector(goBack))
        navigationItem.leftBarButtonItem = leftButton
    }
    
    @objc func goBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func updateTrendViewCount(){
        if imageList[0].Media == "VIDEO" && imageList[0].VideoUrl != nil {
            WebServices().callUserService(service: .trendView, urlParameter: "\(imageList[0].TrendId)", parameters: nil, isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
                print(serviceResponse)
                if (serviceResponse["Message"]as! String == "View count for this trend updated."){
                    self.delegate?.IncreaseTrendViewCount(Index: self.cellIndex)
                }
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.viewVideo)
    }
    
//    @objc func playerItemDidReachEnd(_ sender: NSNotification) {
//
//        guard let playerItem = sender.object as? AVPlayerItem else {
//            return
//        }
//        guard viewVideo.playerLayer.player != nil else
//        {
//            return
//        }
//        playerItem.seek(to: CMTime.zero)
//        guard viewVideo.playerLayer.player?.currentItem == playerItem else {
//            return
//        }
//        viewVideo.playerLayer.player?.play()
//        viewVideo.playerLayer.player?.volume = 0.0
//    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
}

extension FullScreenViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = "FullScreenCollectionViewCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! FullScreenCollectionViewCell
        cell.trendImageView.sd_setImage(with: URL(string: imageList[indexPath.item].TrendImage), placeholderImage: #imageLiteral(resourceName: "favourite"))
        
//        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(pinch(_:)))
        cell.trendImageView.tag = indexPath.item
        
        if imageList[indexPath.item].Media == "VIDEO" && imageList[indexPath.item].VideoUrl != nil {
//            let videoLayer: AVPlayerLayer = AVPlayerLayer()
//            videoLayer.frame = CGRect(x: 0, y: 0, width: cell.trendImageView.frame.size.width, height: cell.trendImageView.frame.size.height)
//            videoLayer.backgroundColor = UIColor.clear.cgColor
//            videoLayer.videoGravity = AVLayerVideoGravity.resize
//            cell.trendImageView.layer.addSublayer(videoLayer)
//            ASVideoPlayerController.sharedVideoPlayer.setupVideoFor(url: imageList[indexPath.item].VideoUrl!)
//            pausePlayeVideos()
            cell.videoView.isHidden = true
            cell.btnPlayVideo.isHidden = false
            cell.btnPlayVideo.tag = indexPath.item
            cell.btnPlayVideo.addTarget(self, action: #selector(btnPlayVideoAction(_:)), for: .touchUpInside)
//            cell.trendImageView.isUserInteractionEnabled = false
//            cell.trendImageView.removeGestureRecognizer(pinch)
        }
        else
        {
            cell.videoView.isHidden = true
            cell.btnPlayVideo.isHidden = true
//            cell.trendImageView.isUserInteractionEnabled = true
//            cell.trendImageView.addGestureRecognizer(pinch)
            
        }
        
        
//        let pan = UIPanGestureRecognizer(target: self, action: #selector(pan(_:)))
//        cell.trendImageView.addGestureRecognizer(pan)
        
        return cell
    }
    
    @objc func btnPlayVideoAction(_ sender: UIButton)
    {
        let videoURL = URL(string: imageList[sender.tag].VideoUrl!)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
            self.updateTrendViewCount()
        }
    }
   
    
//    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        if let videoCell = cell as? ASAutoPlayVideoLayerContainer, let _ = videoCell.videoURL {
//            ASVideoPlayerController.sharedVideoPlayer.removeLayerFor(cell: videoCell)
//        }
//    }
    
  
    
//    @objc func pan(_ sender: UIPanGestureRecognizer) {
//        if self.isZooming && sender.state == .began {
//            self.originalImageCenter = sender.view?.center
//        }
//    }
    
//    @objc func pinch(_ sender: UIPinchGestureRecognizer) {
//
//        if sender.state == .began {
//            let currentScale = sender.view!.frame.size.width / sender.view!.bounds.size.width
//            let newScale = currentScale*sender.scale
//            if newScale > 1 {
//                self.isZooming = true
//            }
//        } else if sender.state == .changed {
//            guard let view = sender.view else {return}
//            let pinchCenter = CGPoint(x: sender.location(in: view).x - view.bounds.midX,
//                                      y: sender.location(in: view).y - view.bounds.midY)
//            let transform = view.transform.translatedBy(x: pinchCenter.x, y: pinchCenter.y)
//                .scaledBy(x: sender.scale, y: sender.scale)
//                .translatedBy(x: -pinchCenter.x, y: -pinchCenter.y)
//            let currentScale = sender.view!.frame.size.width / sender.view!.bounds.size.width
//            var newScale = currentScale*sender.scale
//            if newScale < 1 {
//                newScale = 1
//                let transform = CGAffineTransform(scaleX: newScale, y: newScale)
//                sender.view!.transform = transform
//                sender.scale = 1
//            }else {
//                view.transform = transform
//                sender.scale = 1
//            }
//        } else if sender.state == .ended || sender.state == .failed || sender.state == .cancelled {
////            originalImageCenter = self.view.center
////            guard let center = originalImageCenter else {return}
////            UIView.animate(withDuration: 0.3, animations: {
////                sender.view!.transform = CGAffineTransform.identity
////                sender.view!.center = center
////                self.fullScreenCollection.reloadItems(at: [IndexPath(item: sender.view!.tag, section: 0)])
////            }, completion: { _ in
//                self.isZooming = false
//
//            let transform = CGAffineTransform(scaleX: 1, y: 1)
//            sender.view!.transform = transform
//            sender.scale = 1
////            })
//        }
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        originalImageCenter = CGPoint(x: collectionView.frame.origin.x, y: collectionView.frame.origin.y)
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    
//    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        fullScreenPageControl.currentPage = indexPath.item
//        if imageList[indexPath.item].Media == "VIDEO" && imageList[indexPath.item].VideoUrl != nil{
//            (cell as! FullScreenCollectionViewCell).videoView.frame = (cell as! FullScreenCollectionViewCell).trendImageView.frame
//            (cell as! FullScreenCollectionViewCell).videoView.playerURl = imageList[indexPath.item].VideoUrl
//
//            (cell as! FullScreenCollectionViewCell).videoView.playerLayer.videoGravity = AVLayerVideoGravity.resizeAspect
//
//            (cell as! FullScreenCollectionViewCell).videoView.playVideo()
//            (cell as! FullScreenCollectionViewCell).videoView.playerLayer.player?.volume = 0.0
//        }
//        else
//        {
//
//        }
//    }
//
//    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        if imageList[indexPath.item].Media == "VIDEO" && imageList[indexPath.item].VideoUrl != nil {
//            (cell as! FullScreenCollectionViewCell).videoView.stopVideo()
//            viewVideo = PlayerView()
//        }
//    }
}
