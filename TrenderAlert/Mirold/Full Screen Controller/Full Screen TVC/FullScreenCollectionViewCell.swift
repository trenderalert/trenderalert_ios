//
//  FullScreenCollectionViewCell.swift
//  TrenderAlert
//
//  Created by HPL on 02/04/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class FullScreenCollectionViewCell: UICollectionViewCell, UIScrollViewDelegate {

    @IBOutlet var btnPlayVideo: UIButton!
    @IBOutlet var scroll: UIScrollView!
    @IBOutlet var trendImageView: UIImageView!
    @IBOutlet var videoView: PlayerView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        scroll.minimumZoomScale = 1.0
        scroll.maximumZoomScale = 10.0
        if btnPlayVideo.isHidden{
            scroll.delegate = self
        }
        else
        {
            scroll.delegate = nil
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
         if btnPlayVideo.isHidden{
            return trendImageView
        }
        return nil
    }

}
