//
//  PopUpViewController.swift
//  ALIST
//
//  Created by ospmac on 05/09/17.
//  Copyright © 2017 heypayless. All rights reserved.
//

import UIKit

//protocol MaintainStateForPresentationDelegate {
//    var refreshPresentationState: UIViewController! { get set }
//}

class PopUpViewController: UIViewController {
    
    var viewControllerToDisplay: UIViewController!
    var heightForPopUp: CGFloat?
    var dismissingView = UIView()
    var requiredNavController: UINavigationController!
    var refreshPresentationState: UIViewController!
    
    init(_ viewController: UIViewController,withHeight height: CGFloat?) {
        super.init(nibName: nil, bundle: nil)
        viewControllerToDisplay = viewController
        heightForPopUp = height
        modalPresentationStyle = .overCurrentContext
//        modalTransitionStyle = .crossDissolve
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = UIColor.clear
        
//        dismissingView = UIView(frame: )
        dismissingView.frame = view.frame
        let tapToDimissPopUp = UITapGestureRecognizer(target: self, action: #selector(tapToDimissPopUp(_:)))
        dismissingView.addGestureRecognizer(tapToDimissPopUp)
        dismissingView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        dismissingView.isOpaque = false
        view.addSubview(dismissingView)
        
        self.addChild(viewControllerToDisplay)
       
        self.view.addSubview(viewControllerToDisplay.view)
//        self.view.addSubview(viewControllerToDisplay.view)
        viewControllerToDisplay.view.frame.size = CGSize(width: view.frame.width, height: heightForPopUp ?? view.frame.height - 113)
        viewControllerToDisplay.view.frame.origin = CGPoint(x: 0, y: view.frame.midY - (heightForPopUp ?? view.frame.height - 113)/2)
//        viewControllerToDisplay.view.center = view.centerCGRect.miCGRect.midYdY
        viewControllerToDisplay.didMove(toParent: self)
        
        viewControllerToDisplay.view.layer.cornerRadius = 10
        viewControllerToDisplay.view.layer.masksToBounds = true
    }
    

    func dismissPopUp(animated flag: Bool, completion: (() -> Void)? = nil) {
        guard refreshPresentationState == nil else {
            super.dismiss(animated: flag) {
                self.requiredNavController.present(self.refreshPresentationState, animated: true, completion: completion)
            }
            return
        }
        super.dismiss(animated: flag, completion: completion)
    }
    
    @objc func tapToDimissPopUp(_ sender: UITapGestureRecognizer) {
        self.dismissPopUp(animated: true, completion: nil)
    }

}
