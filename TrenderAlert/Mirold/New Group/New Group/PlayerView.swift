//
//  PlayerView.swift
//  TrenderAlert
//
//  Created by Admin on 12/06/18.
//  Copyright © 2018 HPL. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class PlayerView: UIView {

    var playerURl :String?
    {
        willSet(newplayerURl)
        {
            print("About to set totalSteps to \(newplayerURl)")
            let avPlayer = AVPlayer(url: URL(string: newplayerURl!)!)
            self.playerLayer.player = avPlayer
         }
    }
    
    func playVideo()
    {
      self.playerLayer.player?.play()
    }

    func stopVideo()
    {
        self.playerLayer.player?.pause()
    }
 
    override static var layerClass: AnyClass {
        return AVPlayerLayer.self
    }
    
    var playerLayer: AVPlayerLayer {
      //  self.playerLayer.player?.volume = 0.0
        return layer as! AVPlayerLayer
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
