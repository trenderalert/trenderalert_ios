//
//  ShareInternalViewController.swift
//  TrenderAlert
//
//  Created by HPL on 02/04/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import AFViewShaker
import GrowingTextView
import DropDown

protocol friendShareSelectedDelegate {
    func controller(arraySelectedFriends: [TagFriendModel],indexShare: Int, trendId: Int, description: String)
}

protocol FriendsPopupDelegate {
    func friendsPopup(indexShare: Int, trendId: Int, description: String)
}

class ShareInternalViewController: UIViewController {

    @IBOutlet weak var btnShare: UIButton!
    @IBOutlet var btnShareNow: UIButton!
    @IBOutlet var txtDescription: GrowingTextView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var profilePicImg: UIImageView!
    @IBOutlet var viewBottom: UIView!
    var arrayPublicPrivate = [MySingleton.shared.selectedLangData.Public,MySingleton.shared.selectedLangData.Private,MySingleton.shared.selectedLangData.friends]
    @IBOutlet var viewPublic: UIView!
    @IBOutlet var lblPublicPrivate: UILabel!
    var txtdiscription = String()
    var publicPrivateDropdown = DropDown()
    var trendId = Int()
    var index = Int()
    var delegate : friendShareSelectedDelegate?
    var friendsPopupDelegate : FriendsPopupDelegate?
    var selectedShareFriends = [AnyObject]()
    override func viewDidLoad() {
        super.viewDidLoad()
        if index == 2
        {
            self.lblPublicPrivate.text = MySingleton.shared.selectedLangData.friends
            self.txtDescription.text = self.txtdiscription
        }
        else{
            self.lblPublicPrivate.text = MySingleton.shared.selectedLangData.Public
        }
        viewBottom.makeCornerRadius(radius: 10.0)
        btnShareNow.makeCornerRadius(radius: 6.0)
        lblName.text = MySingleton.shared.loginObject.userName
        profilePicImg.sd_setImage(with: URL(string: MySingleton.shared.loginObject.ProfilePic ?? ""), placeholderImage: #imageLiteral(resourceName: "img_selectProfilePic"))
        publicPrivateDropdown.anchorView = viewPublic
        publicPrivateDropdown.bottomOffset = CGPoint(x: 0, y:(publicPrivateDropdown.anchorView?.plainView.bounds.height)!)
        publicPrivateDropdown.topOffset = CGPoint(x: 0, y:-(publicPrivateDropdown.anchorView?.plainView.bounds.height)!)
        dropdownPublicPrivateSelection()
        self.btnShare.setTitle(MySingleton.shared.selectedLangData.share_now, for: .normal)
        self.txtDescription.placeholder = MySingleton.shared.selectedLangData.say_something_about_this
    }
    
    override func viewDidLayoutSubviews() {
        profilePicImg.makeCornerRadius(radius: profilePicImg.frame.size.height/2)
        viewPublic.makeCornerRadius(radius: 3.0)
        viewPublic.giveBorderColor(color: UIColor.init(red: 179.0/255.0, green: 179.0/255.0, blue: 179.0/255.0, alpha: 1.0))
        viewPublic.layer.borderWidth = 2.0
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func dropdownPublicPrivateSelection()
    {
        publicPrivateDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            self.lblPublicPrivate.text! = item
            
            if index == 1
            {
                self.index = 1
            }
            
            if item == MySingleton.shared.selectedLangData.friends
            {
                self.dismiss(animated: true, completion: {
                    self.friendsPopupDelegate?.friendsPopup(indexShare: index, trendId: self.trendId, description: self.txtDescription.text)
                })
            }
            
        }
    }
    
    @IBAction func viewPublicTapAction(_ sender: Any) {
        publicPrivateDropdown.dataSource = arrayPublicPrivate
        publicPrivateDropdown.cellHeight = 40.0
        publicPrivateDropdown.show()
    }
  
    @IBAction func shareNowButtonAction(_ sender: Any) {
        if txtDescription.text.isEmpty
        {
            var shaker = AFViewShaker(view: txtDescription)
            shaker?.shake()
        }
        else
        {
            let param = ["TrendId": trendId,"Description": txtDescription.text!,"TrendTypeId":Int(index+1),"TaggedFriends":selectedShareFriends] as [String : Any]
            WebServices().callUserService(service: UserServices.shareTrend, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
                print(serviceResponse)
                if serviceResponse["Status"]as! Bool{
                    self.dismiss(animated: true, completion: {
                     //   self.delegate?.controller(arraySelectedFriends: self.selectedShareFriends, indexShare: self.index, trendId: self.trendId, description: self.txtDescription)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getAllTrends"), object: self, userInfo: nil)
                    })
                }
            })
        }
    }
    
}

extension ShareInternalViewController : friendSelectedDelegate {
    
    func controller(arraySelectedFriends: [TagFriendModel], index: Int, trendId: Int, description: String) {
        print("select friends")
    }
   
}
