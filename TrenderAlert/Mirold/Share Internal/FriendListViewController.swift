//
//  FriendListViewController.swift
//  TrenderAlert
//
//  Created by Riken Shah on 09/07/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

protocol friendSelectedDelegate {
    func controller(arraySelectedFriends: [TagFriendModel],index: Int, trendId: Int,description: String)
}

class FriendListViewController: UIViewController {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var lblSelectAll: UILabel!
    @IBOutlet weak var tblFriends: UITableView!
    @IBOutlet weak var viewCorner: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSelectAllCheck: UIButton!
    var arrayTagTrendersToSelect = [TagFriendModel]()
    var delegate : friendSelectedDelegate?
//    var selectedFriendsId = [AnyObject]()
    var isFriendApiRequestInProgress = false
    var totalFriendCount = 1
    var noOfFriends = 0
    var trendId = Int()
    var index = Int()
    var isSelected = [Bool]()
    var txtdescprition = String()
    var editId = [Int]()
    var arrarEditSelected = [AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblFriends.register(UINib.init(nibName: "TagTrendersTableViewCell", bundle: nil), forCellReuseIdentifier: "TagTrendersTableViewCell")
        self.tblFriends.tableFooterView = UIView()
        getFriendList(Skip: 0, Take: 500, lazyLoading: false)
        self.lblTitle.text = MySingleton.shared.selectedLangData.friends
        self.lblSelectAll.text = MySingleton.shared.selectedLangData.select_all
        self.btnSave.setTitle(MySingleton.shared.selectedLangData.save, for: .normal)
        self.btnCancel.setTitle(MySingleton.shared.selectedLangData.cancel, for: .normal)
        self.lblNoData.text = MySingleton.shared.selectedLangData.no_friends_found
        // Do any additional setup after loading the view.
    }
    
    override func viewDidLayoutSubviews() {
        btnSave.makeCornerRadius(radius: 5)
        btnCancel.makeCornerRadius(radius: 5)
        viewCorner.makeCornerRadius(radius: 10)
    }
    
    
    func getFriendList(Skip: Int, Take: Int, lazyLoading: Bool,SearchString:String = ""){
        
        
        self.isFriendApiRequestInProgress = true
        
        var param = ["Skip":Skip,"Take":Take,"Search":""] as [String : Any]
        
        
        WebServices().callUserService(service: UserServices.getFriendList, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
            if serviceResponse["Status"]as! Bool
            {
                if serviceResponse["data"]is [AnyObject]
                {
                    DispatchQueue.main.async {
                        
                        var items = TagFriendModel()
                        if (serviceResponse["data"]as! [AnyObject]).count != 0
                        {
                        for i in 0...(serviceResponse["data"]as! [AnyObject]).count-1
                        {
                            items.id = ((serviceResponse["data"]as! [AnyObject])[i]as! [String:AnyObject])["UserProfileID"]as! Int
                            items.FirstName = ((serviceResponse["data"]as! [AnyObject])[i]as! [String:AnyObject])["Firstname"]as! String
                            items.LastName = ((serviceResponse["data"]as! [AnyObject])[i]as! [String:AnyObject])["Lastname"]as! String
                            if ((serviceResponse["data"]as! [AnyObject])[i]as! [String:AnyObject])["ProfileImage"] is String
                            {
                            items.ProfilePic = ((serviceResponse["data"]as! [AnyObject])[i]as! [String:AnyObject])["ProfileImage"]as! String
                            }
                            else
                            {
                                items.ProfilePic = ""
                            }
                            if self.editId.count != 0
                            {
                                if self.editId.contains(((serviceResponse["data"]as! [AnyObject])[i]as! [String:AnyObject])["UserProfileID"]as! Int)
                                {
                                    items.isSelected = true
                                }
                                else
                                {
                                    items.isSelected = false
                                }
                            }
                            else
                            {
                                items.isSelected = false
                            }
                            self.arrayTagTrendersToSelect.append(items)
                            print(self.arrayTagTrendersToSelect)
                        }
                        self.tblFriends.delegate = self
                        self.tblFriends.dataSource = self
                        self.tblFriends.reloadData()
                    }
                    }
                }
            }
        })
    }
    

    @IBAction func btn_SubmitAction(_ sender: UIButton) {
           self.dismiss(animated: true) {
            self.delegate?.controller(arraySelectedFriends: self.arrayTagTrendersToSelect, index: self.index, trendId: self.trendId, description: self.txtdescprition)
        }
    }
    
    @IBAction func btn_CancelAction(_ sender: UIButton) {
         self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_SelectAll(_ sender: UIButton) {
        if arrayTagTrendersToSelect.count != 0
        {
            if btnSelectAllCheck.currentBackgroundImage == #imageLiteral(resourceName: "radioOffYellow")
            {
                for i in 0...arrayTagTrendersToSelect.count-1
                {
                    arrayTagTrendersToSelect[i].isSelected = true
                }
                btnSelectAllCheck.setBackgroundImage(#imageLiteral(resourceName: "check mark"), for: .normal)
                tblFriends.reloadData()
            }
            else
            {
                for i in 0...arrayTagTrendersToSelect.count-1
                {
                    arrayTagTrendersToSelect[i].isSelected = false
                }
                btnSelectAllCheck.setBackgroundImage(#imageLiteral(resourceName: "radioOffYellow"), for: .normal)
                tblFriends.reloadData()
            }
        }
    }
}

extension FriendListViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrayTagTrendersToSelect.count == 0
        {
            btnSelectAllCheck.isHidden = true
            lblSelectAll.isHidden = true
            lblNoData.isHidden = false
        }
        else
        {
            lblNoData.isHidden = true
        }
        return arrayTagTrendersToSelect.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "TagTrendersTableViewCell", for: indexPath)as! TagTrendersTableViewCell
        cell.lblFullName.text = "\(self.arrayTagTrendersToSelect[indexPath.row].FirstName ) \(self.arrayTagTrendersToSelect[indexPath.row].LastName)"
     
        if arrayTagTrendersToSelect[indexPath.row].isSelected
        {
            cell.btnCheck.setBackgroundImage(#imageLiteral(resourceName: "check mark"), for: .normal)
        }
        else
        {
            cell.btnCheck.setBackgroundImage(#imageLiteral(resourceName: "radioOffYellow"), for: .normal)
        }
        
        cell.btnCheck.tag = indexPath.row
        cell.btnCheck.addTarget(self, action: #selector(btnCheckAction(_:)), for: .touchUpInside)
        return cell
    }
    
    @objc func btnCheckAction(_ sender:UIButton)
    {
        let cell = tblFriends.cellForRow(at: IndexPath(row: sender.tag, section: 0))as? TagTrendersTableViewCell

        if cell?.btnCheck.currentBackgroundImage == #imageLiteral(resourceName: "radioOffYellow")
        {
            cell?.btnCheck.setBackgroundImage(#imageLiteral(resourceName: "check mark"), for: .normal)
            arrayTagTrendersToSelect[sender.tag].isSelected = true
        }
        else
        {
            cell?.btnCheck.setBackgroundImage(#imageLiteral(resourceName: "radioOffYellow"), for: .normal)
            arrayTagTrendersToSelect[sender.tag].isSelected = false
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }
    
}
