//
//  SelectFolderViewController.swift
//  TrenderAlert
//
//  Created by HPL on 25/03/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

protocol CreateFolderPopupDelegate {
    func createFolderPopup()
}

class SelectFolderViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet var viewCorner: UIView!
    @IBOutlet var lblNoData: UILabel!
    @IBOutlet var tableSelectFolder: UITableView!
    var foldersArray = [AnyObject]()
    var trendId = Int()
    var shareTrendId = Int()
    var createFolderPopupDelegate : CreateFolderPopupDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableSelectFolder.register(UINib.init(nibName: "SelectFolderTableViewCell", bundle: nil), forCellReuseIdentifier: "SelectFolderTableViewCell")
        tableSelectFolder.tableFooterView = UIView()
        viewCorner.makeCornerRadius(radius: 10)
        self.lblTitle.text = MySingleton.shared.selectedLangData.please_select_folder
        self.lblNoData.text = MySingleton.shared.selectedLangData.no_data_found
        getUserInfo()
    }
    
    override func viewDidLayoutSubviews() {
//        viewCorner.makeCornerRadius(radius: 10.0)
    }

    @IBAction func btnCloseAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func getUserInfo(){
        WebServices().callUserService(service: UserServices.getUserInfo, urlParameter: "", parameters: ["UserID":MySingleton.shared.loginObject.UserID as AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
            DispatchQueue.main.async {

            if (((serviceResponse["data"]as! [AnyObject])[0]as! [String:Any])["Folders"]as! [AnyObject]).count != 0
            {
                self.foldersArray = ((serviceResponse["data"]as! [AnyObject])[0]as! [String:Any])["Folders"] as! [AnyObject]
                
                
                self.foldersArray.insert(["FolderId": 0, "FolderName": MySingleton.shared.selectedLangData.create_new_folder] as AnyObject, at: 0)
                self.lblNoData.isHidden = true
                self.tableSelectFolder.delegate = self
                self.tableSelectFolder.dataSource = self
                self.tableSelectFolder.reloadData()
            }
            else{
                self.lblNoData.isHidden = true
                self.foldersArray.insert(["FolderId": 0, "FolderName": MySingleton.shared.selectedLangData.create_new_folder] as AnyObject, at: 0)
                self.tableSelectFolder.delegate = self
                self.tableSelectFolder.dataSource = self
                self.tableSelectFolder.reloadData()
            }
        }
        })
    }

}

extension SelectFolderViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.foldersArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectFolderTableViewCell", for: indexPath)as! SelectFolderTableViewCell
        cell.lblTitle.text = (foldersArray[indexPath.row]as! [String:Any])["FolderName"]as? String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        if indexPath.row == 0
        {
            DispatchQueue.main.async {
                self.dismiss(animated: true) {
                    self.createFolderPopupDelegate?.createFolderPopup()

                }
            }
        }
        else{
        
            WebServices().callUserService(service: UserServices.saveToFolder, urlParameter: "", parameters: ["FolderID": (foldersArray[indexPath.row]as! [String:Any])["FolderId"]as! Int as AnyObject, "TrendID": trendId as AnyObject,"TrendShareId":self.shareTrendId as AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
                print(serviceResponse)
                if serviceResponse["Status"]as! Bool == true
                {
                    self.dismiss(animated: true, completion: nil)
                }
                else
                {
                    DispatchQueue.main.async {
                        TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"]as! String, completionHandler: nil)
                    }
                }
                
                
            })
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }
}

extension SelectFolderViewController: CreateFolderDelegate
{
    func controller() {
        getUserInfo()
    }
    
}
