//
//  SelectFolderTableViewCell.swift
//  TrenderAlert
//
//  Created by HPL on 25/03/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class SelectFolderTableViewCell: UITableViewCell {
    
    @IBOutlet var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
