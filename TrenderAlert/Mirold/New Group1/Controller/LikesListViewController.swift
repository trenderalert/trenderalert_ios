//
//  LikesListViewController.swift
//  TrenderAlert
//
//  Created by HPL on 13/03/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class LikesListViewController: UIViewController {

    @IBOutlet var lblNoData: UILabel!
    var trendData = [String:Any]()
    @IBOutlet var viewCorner: UIView!
    @IBOutlet var tableLikesList: UITableView!
    var arrayLikesList = [AnyObject]()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableLikesList.register(UINib.init(nibName: "LikesListTableViewCell", bundle: nil), forCellReuseIdentifier: "LikesListTableViewCell")
        tableLikesList.tableFooterView = UIView()
        setupNavaigationbar()
        let title = UILabel()
        title.font = UIFont.boldSystemFont(ofSize: 20)
        title.textColor = UIColor.white
        if trendData["Type"]as! String == "LikesCount"{
            title.text = "People who liked"
            getLikesData(isLazyLoading: false)
        }
        else if trendData["Type"]as! String == "ShareCount"
        {
            title.text = "People who shared"
            getShareData(isLazyLoading: false)
        }
        else{
            title.text = "People who viewed"
            getViewsData(isLazyLoading: false)
        }
        self.navigationItem.titleView = title
        
    }
    
    func setupNavaigationbar(){
        Utilities.setNavigationBarWithTitle(viewController: self, title: "")
        let leftButton = UIBarButtonItem(image: UIImage(named: "img_BackArrow"), style: .plain, target: self, action: #selector(goBack))
        navigationItem.leftBarButtonItem = leftButton
    }
    
    @objc func goBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func getShareData(isLazyLoading : Bool){
        let param = ["TrendId": trendData["TrendId"] as AnyObject] as [String : Any]
        WebServices().callUserService(service: UserServices.sharedTrendDetails, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: isLazyLoading, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
            DispatchQueue.main.async {

            self.arrayLikesList = (serviceResponse["data"]as! [AnyObject])
            if self.arrayLikesList.count == 0
            {
                self.lblNoData.isHidden = false
            }
            else
            {
                self.lblNoData.isHidden = true
            }
                self.tableLikesList.delegate = self
                self.tableLikesList.dataSource = self
                self.tableLikesList.reloadData()
            }
            
        })
    }
    
    func getViewsData(isLazyLoading : Bool){
        let param = ["TrendId": trendData["TrendId"] as AnyObject,"PageIndex": 0] as [String : Any]
        WebServices().callUserService(service: UserServices.getViews, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: isLazyLoading, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
            DispatchQueue.main.async {

            self.arrayLikesList = (serviceResponse["data"]as! [AnyObject])
            if self.arrayLikesList.count == 0
            {
                self.lblNoData.isHidden = false
            }
            else
            {
                self.lblNoData.isHidden = true
            }
                self.tableLikesList.delegate = self
                self.tableLikesList.dataSource = self
                self.tableLikesList.reloadData()
            }
        })
    }
    
    func getLikesData(isLazyLoading : Bool){
        let param = ["TrendId": trendData["TrendId"] as AnyObject,"PageIndex": 0] as [String : Any]
        WebServices().callUserService(service: UserServices.getLikes, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: isLazyLoading, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            DispatchQueue.main.async {
            print(serviceResponse)
            self.arrayLikesList = (serviceResponse["data"]as! [AnyObject])
            if self.arrayLikesList.count == 0
            {
                self.lblNoData.isHidden = false
            }
            else
            {
                self.lblNoData.isHidden = true
            }
                self.tableLikesList.delegate = self
                self.tableLikesList.dataSource = self
                self.tableLikesList.reloadData()
            }
            
        })
    }
    
    override func viewDidLayoutSubviews() {
        
    }

}

extension LikesListViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayLikesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LikesListTableViewCell", for: indexPath)as! LikesListTableViewCell
        cell.lblName.text = (arrayLikesList[indexPath.row]as! [String:AnyObject])["UserFullName"]as? String
        let friendNameGesture = UITapGestureRecognizer(target: self, action: #selector(showUserProfile(_:)))
        cell.lblName.tag = indexPath.row
        cell.lblName.addGestureRecognizer(friendNameGesture)
        cell.lblName.isUserInteractionEnabled = true
        
        if (arrayLikesList[indexPath.row]as! [String:AnyObject])["UserProfileImage"]is String
        {
            cell.btnProfilePic.sd_setImage(with: URL(string: (arrayLikesList[indexPath.row]as! [String:AnyObject])["UserProfileImage"]as! String), placeholderImage: #imageLiteral(resourceName: "img_selectProfilePic"))
        }
        else
        {
            cell.btnProfilePic.image = #imageLiteral(resourceName: "img_selectProfilePic")
        }
        cell.btnProfilePic.makeCornerRadius(radius: cell.btnProfilePic.frame.size.height/2)
//        let imageTapGesture = UITapGestureRecognizer(target: self, action: #selector(showZoomedImage))
//         cell.friendImage.tag = indexPath.row
//        cell.friendImage.addGestureRecognizer(imageTapGesture)
//        cell.friendImage.isUserInteractionEnabled = true
        tableView.rowHeight = 50.0
        return cell
    }
    
     @objc func showUserProfile(_ sender:UITapGestureRecognizer){
        viewProfile(profileId: ((arrayLikesList[(sender.view?.tag)!]as! [String:AnyObject])["UserProfileId"]as! Int))
    }
    
    func viewProfile(profileId:Int){
        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
        let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
        
        profileVC.userId = profileId
        
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
}
