//
//  LikesListTableViewCell.swift
//  TrenderAlert
//
//  Created by HPL on 14/03/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class LikesListTableViewCell: UITableViewCell {

    @IBOutlet var lblName: UILabel!
    @IBOutlet var btnProfilePic: UIImageView!
    @IBOutlet var btnFollow: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
