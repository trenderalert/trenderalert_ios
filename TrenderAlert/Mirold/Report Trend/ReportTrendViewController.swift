//
//  ReportTrendViewController.swift
//  TrenderAlert
//
//  Created by HPL on 28/03/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import TagListView
import MessageUI

class ReportTrendViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet var btnSave: UIButton!
    @IBOutlet var tagView: TagListView!
    var tagTitles = [MySingleton.shared.selectedLangData.nudity, MySingleton.shared.selectedLangData.violence, MySingleton.shared.selectedLangData.harassment, MySingleton.shared.selectedLangData.suicide_or_self_injury, MySingleton.shared.selectedLangData.false_news, MySingleton.shared.selectedLangData.spam, MySingleton.shared.selectedLangData.unauthorised_sales, MySingleton.shared.selectedLangData.hate_speech, MySingleton.shared.selectedLangData.terrorism, MySingleton.shared.selectedLangData.incorrect_voting_information]
    
    var selectedTags = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnSave.makeCornerRadius(radius: 6.0)
        tagView.addTags(tagTitles)
        tagView.delegate = self
        tagView.textFont = UIFont.systemFont(ofSize: 14)
        tagView.cornerRadius = 15
        self.lblTitle.text = MySingleton.shared.selectedLangData.reporting_trend
    }
    
    
    @IBAction func cancelButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func saveButton(_ sender: Any) {
        if selectedTags != ""
        {
            let emailTitle = selectedTags
            let messageBody = ""
            //trenderalertme@gmail.com
            let toRecipents = ["report@trenderalert.com"]
            let mc: MFMailComposeViewController = MFMailComposeViewController()
            mc.mailComposeDelegate = self
            mc.setSubject(emailTitle)
            mc.setMessageBody(messageBody, isHTML: false)
            mc.setToRecipients(toRecipents)
            
            self.present(mc, animated: true, completion: nil)
        }
    }
}

extension ReportTrendViewController: TagListViewDelegate{
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) -> Void{
        print(title)
        selectedTags = title
        tagView.textColor = UIColor.white
        if sender.selectedTags().count != 0
        {
            let tagsCount = (sender.selectedTags().count)
            for i in 0...tagsCount - 1{
                if (sender.selectedTags()[i] != tagView)
                {
                    sender.selectedTags()[i].textColor = UIColor.darkGray
                    sender.selectedTags()[i].isSelected = false
                }
                
            }
        }
        
        tagView.isSelected = true

        
    }
}

extension ReportTrendViewController: MFMailComposeViewControllerDelegate
{

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result {
        case MFMailComposeResult.cancelled:
            print("Mail cancelled")
        case MFMailComposeResult.saved:
            print("Mail saved")
        case MFMailComposeResult.sent:
            print("Mail sent")
            self.dismiss(animated: true, completion: nil)
        case MFMailComposeResult.failed:
            print("Mail sent failure: \(error!.localizedDescription)")
        default:
            break
        }
//        self.dismiss(animated: true, completion: nil)
        controller.dismiss(animated: true, completion: nil)
    }
}
