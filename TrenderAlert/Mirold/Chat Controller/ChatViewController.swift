//
//  ChatViewController.swift
//  TrenderAlert
//
//  Created by HPL on 09/04/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import GrowingTextView
import CropViewController
import MBProgressHUD
import AVFoundation
import AVKit
import Alamofire
import MobileCoreServices
import AGEmojiKeyboard
import DropDown

class ChatViewController: UIViewController {

    @IBOutlet var btnMicrophone: UIButton!
    @IBOutlet var tableChat: UITableView!
    @IBOutlet var viewMessage: UIView!
    @IBOutlet var txtMessage: GrowingTextView!
    var menuDropdown = DropDown()
    var ToUserId = Int()
    var customNav : UIView!
    var backBtn = UIButton()
    var menuBtn = UIButton()
    var navProfilePic = UIImageView()
    var navFullName = UILabel()
    var navLastSeen = UILabel()
    var navDeleteMsgView = UIView()
    var deleteMsgBtn = UIButton()
    var backBtnDeleteMsgView = UIButton()
    let picker = UIImagePickerController()
    var messages = ChatListData()
    var fullName = ""
    var lastSeen = ""
    var profilePic = UIImage()
    var updateTimerRecording = Timer()
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var audioFilename : URL!
    var duration = String()
    var audioPlayIndex = Int()
    var updateTimerForRecorded = Timer()
    var videoUrl : URL!
    var agEmojiKeyboardView = AGEmojiKeyboardView()
    var txtData = String()
//    var longPressed = false
    var emojiImages = [#imageLiteral(resourceName: "time_clk"),
                       #imageLiteral(resourceName: "emoji_clk"),
                       #imageLiteral(resourceName: "bell_clk"),
                       #imageLiteral(resourceName: "flower_clk"),
                       #imageLiteral(resourceName: "car_clk"),
                       #imageLiteral(resourceName: "Recent_clk")]
    var messagesToDelete = [Int]()
    var emojiSelectedIndex = -1
    var emojiNotSelectedIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.picker.delegate = self
        self.tableChat.register(UINib.init(nibName: "OtherChatTableViewCell", bundle: nil), forCellReuseIdentifier: "OtherChatTableViewCell")
        self.tableChat.register(UINib.init(nibName: "MyChatTableViewCell", bundle: nil), forCellReuseIdentifier: "MyChatTableViewCell")
        self.tableChat.register(UINib.init(nibName: "MyChatImageTableViewCell", bundle: nil), forCellReuseIdentifier: "MyChatImageTableViewCell")
        self.tableChat.register(UINib.init(nibName: "OthersChatImageTableViewCell", bundle: nil), forCellReuseIdentifier: "OthersChatImageTableViewCell")
        self.tableChat.register(UINib.init(nibName: "OthersChatAudioTableViewCell", bundle: nil), forCellReuseIdentifier: "OthersChatAudioTableViewCell")
        self.tableChat.register(UINib.init(nibName: "MyChatAudioTableViewCell", bundle: nil), forCellReuseIdentifier: "MyChatAudioTableViewCell")
        self.tableChat.register(UINib.init(nibName: "OtherChatDeleteTableViewCell", bundle: nil), forCellReuseIdentifier: "OtherChatDeleteTableViewCell")
        self.tableChat.register(UINib.init(nibName: "MyChatDeletedMessageTableViewCell", bundle: nil), forCellReuseIdentifier: "MyChatDeletedMessageTableViewCell")
        
        
        self.txtMessage.delegate = self
        
        tableChat.tableFooterView = UIView()
        self.tableChat.estimatedRowHeight = 20.0
        self.tableChat.rowHeight = UITableView.automaticDimension
        
//        setupCustomNavigationBar()
        print(MySingleton.shared.loginObject.AspNetUserId)
        print(["fromUserProfileId": MySingleton.shared.loginObject.UserID, "toUserProfileId": ToUserId, "userId": MySingleton.shared.loginObject.AspNetUserId])
        SocketIoManager.sharedInstance.sendDataToEvent(.chatinitiate, data: ["fromUserProfileId": MySingleton.shared.loginObject.UserID, "toUserProfileId": ToUserId, "userId": MySingleton.shared.loginObject.AspNetUserId])
        SocketIoManager.sharedInstance.socketIOManagerDelegateChat = self
        SocketIoManager.sharedInstance.socketIOManagerDelegateUserStatus = self
        getChatData()
        
        let longPressGesture:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(ChatViewController.handleLongPress(_:)))
        longPressGesture.minimumPressDuration = 0.5 // 0.5 second press
        self.tableChat.addGestureRecognizer(longPressGesture)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupCustomNavigationBar()
    }
    
    @objc func handleLongPress(_ longPressGestureRecognizer: UILongPressGestureRecognizer) {
        
        
        if longPressGestureRecognizer.state == UIGestureRecognizer.State.began {
            navDeleteMsgView.isHidden = false
            
            let touchPoint = longPressGestureRecognizer.location(in: self.tableChat)
            if let indexPath = tableChat.indexPathForRow(at: touchPoint) {
                print(touchPoint)
                print(indexPath.row)
                
                if (!(messages.data[0].ChatDetails[indexPath.row].IsDeleted))
                {
                    
                if messages.data[0].ChatDetails[indexPath.row].MediaType == "TEXT"
                {
                    if messages.data[0].ChatDetails[indexPath.row].FromUserProfileId == Int(MySingleton.shared.loginObject.UserID)
                    {
                        let cell = self.tableChat.cellForRow(at: indexPath)as! MyChatTableViewCell
                        if cell.contentView.backgroundColor == UIColor.lightGray
                        {
                            cell.contentView.backgroundColor = UIColor.clear
                            print(messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)!)
                            let index = messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)
                            messagesToDelete.remove(at: index!)
                        }
                        else
                        {
                            cell.contentView.backgroundColor = UIColor.lightGray
                            messagesToDelete.append(messages.data[0].ChatDetails[indexPath.row].Id)
                        }
                    }
                    else
                    {
                        let cell = self.tableChat.cellForRow(at: indexPath)as! OtherChatTableViewCell
                        if cell.contentView.backgroundColor == UIColor.lightGray
                        {
                            cell.contentView.backgroundColor = UIColor.clear
                            print(messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)!)
                            
                            let index = messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)
                            messagesToDelete.remove(at: index!)
                        }
                        else
                        {
                            cell.contentView.backgroundColor = UIColor.lightGray
                            messagesToDelete.append(messages.data[0].ChatDetails[indexPath.row].Id)
                        }
                    }
                    
                }
                else if messages.data[0].ChatDetails[indexPath.row].MediaType == "IMAGE"
                {
                    if messages.data[0].ChatDetails[indexPath.row].FromUserProfileId == Int(MySingleton.shared.loginObject.UserID)
                    {
                        let cell = self.tableChat.cellForRow(at: indexPath)as! MyChatImageTableViewCell
                        if cell.contentView.backgroundColor == UIColor.lightGray
                        {
                            cell.contentView.backgroundColor = UIColor.clear
                            print(messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)!)
                            let index = messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)
                            messagesToDelete.remove(at: index!)
                        }
                        else
                        {
                            cell.contentView.backgroundColor = UIColor.lightGray
                            messagesToDelete.append(messages.data[0].ChatDetails[indexPath.row].Id)
                        }
                    }
                    else
                    {
                        let cell = self.tableChat.cellForRow(at: indexPath)as! OthersChatImageTableViewCell
                        if cell.contentView.backgroundColor == UIColor.lightGray
                        {
                            cell.contentView.backgroundColor = UIColor.clear
                            print(messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)!)
                            let index = messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)
                            messagesToDelete.remove(at: index!)
                        }
                        else
                        {
                            cell.contentView.backgroundColor = UIColor.lightGray
                            messagesToDelete.append(messages.data[0].ChatDetails[indexPath.row].Id)
                        }
                    }
                }
                else if messages.data[0].ChatDetails[indexPath.row].MediaType == "AUDIO"
                {
                    if messages.data[0].ChatDetails[indexPath.row].FromUserProfileId == Int(MySingleton.shared.loginObject.UserID)
                    {
                        let cell = self.tableChat.cellForRow(at: indexPath)as! MyChatAudioTableViewCell
                        if cell.contentView.backgroundColor == UIColor.lightGray
                        {
                            cell.contentView.backgroundColor = UIColor.clear
                            print(messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)!)
                            let index = messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)
                            messagesToDelete.remove(at: index!)
                        }
                        else
                        {
                            cell.contentView.backgroundColor = UIColor.lightGray
                            messagesToDelete.append(messages.data[0].ChatDetails[indexPath.row].Id)
                        }
                    }
                    else
                    {
                        let cell = self.tableChat.cellForRow(at: indexPath)as! OthersChatAudioTableViewCell
                        if cell.contentView.backgroundColor == UIColor.lightGray
                        {
                            cell.contentView.backgroundColor = UIColor.clear
                            print(messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)!)
                            let index = messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)
                            messagesToDelete.remove(at: index!)
                        }
                        else
                        {
                            cell.contentView.backgroundColor = UIColor.lightGray
                            messagesToDelete.append(messages.data[0].ChatDetails[indexPath.row].Id)
                        }
                    }
                }
                else
                {
                    if messages.data[0].ChatDetails[indexPath.row].FromUserProfileId == Int(MySingleton.shared.loginObject.UserID)
                    {
                        let cell = self.tableChat.cellForRow(at: indexPath)as! MyChatImageTableViewCell
                        if cell.contentView.backgroundColor == UIColor.lightGray
                        {
                            cell.contentView.backgroundColor = UIColor.clear
                            print(messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)!)
                            let index = messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)
                            messagesToDelete.remove(at: index!)
                        }
                        else
                        {
                            cell.contentView.backgroundColor = UIColor.lightGray
                            messagesToDelete.append(messages.data[0].ChatDetails[indexPath.row].Id)
                        }
                    }
                    else
                    {
                        let cell = self.tableChat.cellForRow(at: indexPath)as! OthersChatImageTableViewCell
                        if cell.contentView.backgroundColor == UIColor.lightGray
                        {
                            cell.contentView.backgroundColor = UIColor.clear
                            print(messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)!)
                            let index = messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)
                            messagesToDelete.remove(at: index!)
                        }
                        else
                        {
                            cell.contentView.backgroundColor = UIColor.lightGray
                            messagesToDelete.append(messages.data[0].ChatDetails[indexPath.row].Id)
                        }
                    }
                }
                
                }
                print(messagesToDelete)

                if messagesToDelete.count == 0
                {
                    navDeleteMsgView.isHidden = true
                }
            }
        }
    }
    
    func setupCustomNavigationBar()
    {
        customNav = UIView.fromNib()
        customNav.frame = CGRect(x: 0.0, y: 0.0, width: self.view.frame.size.width, height: 44.0)
        backBtn = customNav.viewWithTag(1)as! UIButton
        backBtn.addTarget(self, action: #selector(btnBackAction(_:)), for: .touchUpInside)
        menuBtn = customNav.viewWithTag(7)as! UIButton
        menuBtn.addTarget(self, action: #selector(btnMenuAction(_:)), for: .touchUpInside)
        navProfilePic = customNav.viewWithTag(2)as! UIImageView
        navProfilePic.makeCornerRadius(radius: navProfilePic.frame.size.height/2)
        navFullName = customNav.viewWithTag(3)as! UILabel
        navLastSeen = customNav.viewWithTag(4)as! UILabel
        navDeleteMsgView = customNav.viewWithTag(8)as! UIView
        deleteMsgBtn = customNav.viewWithTag(9)as! UIButton
        deleteMsgBtn.addTarget(self, action: #selector(btnDeleteMsgAction(_:)), for: .touchUpInside)
        backBtnDeleteMsgView = customNav.viewWithTag(10)as! UIButton
        backBtnDeleteMsgView.addTarget(self, action: #selector(btnDeleteMsgBackAction(_:)), for: .touchUpInside)
        navDeleteMsgView.isHidden = true
        //        self.navigationItem.titleView = customNav
        //        let button1 = UIBarButtonItem(image: UIImage(named: "sendBlack"), style: .plain, target: self, action: #selector(btnBackAction(_:)))
        
        menuDropdown.anchorView = menuBtn
        menuDropdown.bottomOffset = CGPoint(x: 0, y:(menuDropdown.anchorView?.plainView.bounds.height)!)
        menuDropdown.topOffset = CGPoint(x: 10, y:-(menuDropdown.anchorView?.plainView.bounds.height)!)
        dropdownMenuSelection()
        self.navigationItem.leftBarButtonItem = nil
        self.navigationController?.navigationBar.addSubview(customNav)
        
        if fullName != ""
        {
            self.navFullName.text = fullName
            self.navProfilePic.image = profilePic
            self.navLastSeen.text = lastSeen
        }
    }
    
    override func viewDidLayoutSubviews() {
        viewMessage.makeCornerRadius(radius: 25)
        viewMessage.giveBorderColor(color: UIColor.darkGray)
    }

    override func viewWillDisappear(_ animated: Bool) {
//        self.navigationController?.isNavigationBarHidden = false
//        SocketIoManager.sharedInstance.socketIOManagerDelegateChat = nil
        customNav.removeFromSuperview()
    }
    
    
    @IBAction func btn_EmojiKeyboardAction(_ sender: UIButton)
    {
        self.view.endEditing(true)
        agEmojiKeyboardView = AGEmojiKeyboardView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216), dataSource: self)
        agEmojiKeyboardView.autoresizingMask = .flexibleHeight
        agEmojiKeyboardView.delegate = self
        self.txtMessage.inputView = agEmojiKeyboardView
        self.txtMessage.becomeFirstResponder()
        self.txtMessage.text = self.txtData
    }
    
    
    func profileAction()  {
        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
        let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
        profileVC.userId = ToUserId
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    func getChatData(){
        WebServices().callUserService(service: UserServices.getChat, urlParameter: "", parameters: ["FromUserProfileId":MySingleton.shared.loginObject.UserID as AnyObject, "ToUserProfileId":ToUserId as AnyObject, "Skip":0 as AnyObject, "Take":0 as AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
            DispatchQueue.main.async {
                if ((serviceResponse["data"]as! [AnyObject])[0]as! [String:AnyObject])["UserProfileInfo"] is [String:AnyObject]
                {
                    self.navFullName.text = "\((((serviceResponse["data"]as! [AnyObject])[0]as! [String:AnyObject])["UserProfileInfo"]as! [String:AnyObject])["UserProfileFirstName"]as! String) \((((serviceResponse["data"]as! [AnyObject])[0]as! [String:AnyObject])["UserProfileInfo"]as! [String:AnyObject])["UserProfileLastName"]as! String)"
                    self.fullName = self.navFullName.text!
                    if((((serviceResponse["data"]as! [AnyObject])[0]as! [String:AnyObject])["UserProfileInfo"]as! [String:AnyObject])["UserProfileImage"]is String)
                    {
                        self.navProfilePic.sd_setImage(with: URL(string: ((((serviceResponse["data"]as! [AnyObject])[0]as! [String:AnyObject])["UserProfileInfo"]as! [String:AnyObject])["UserProfileImage"]as! String)), placeholderImage: #imageLiteral(resourceName: "img_selectProfilePic"))
                        self.navProfilePic.sd_setImage(with: URL(string: ((((serviceResponse["data"]as! [AnyObject])[0]as! [String:AnyObject])["UserProfileInfo"]as! [String:AnyObject])["UserProfileImage"]as! String)), placeholderImage: #imageLiteral(resourceName: "img_selectProfilePic"), options: [], progress: nil, completed: { (image, error, cache, url) in
                            if image != nil{
                                self.profilePic = image!
                            }
                        })
                    }
                    else
                    {
                        self.navProfilePic.image = #imageLiteral(resourceName: "img_selectProfilePic")
                        self.profilePic = #imageLiteral(resourceName: "img_selectProfilePic")
                    }
                    
                    if ((((serviceResponse["data"]as! [AnyObject])[0]as! [String:AnyObject])["UserProfileInfo"]as! [String:AnyObject])["IsOnline"]as! Bool)
                    {
                        self.navLastSeen.text = "Active Now"
                        self.lastSeen = "Active Now"
                    }
                    else
                    {
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
                        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
                        if let date = dateFormatter.date(from: ((((serviceResponse["data"]as! [AnyObject])[0]as! [String:AnyObject])["UserProfileInfo"]as! [String:AnyObject])["LastSeen"]as! String))
                        {
                            self.navLastSeen.text = "Last seen \(date.timeAgoSinceDate())"
                            self.lastSeen = "Last seen \(date.timeAgoSinceDate())"
                        }
                        else
                        {
                            self.navLastSeen.text = ""
                            self.lastSeen = ""
                        }
                    }
                    
                    self.messages = WebServices().decodeDataToClass(data: serviceData, decodeClass: ChatListData.self)!
                    
                    self.tableChat.dataSource = self
                    self.tableChat.delegate = self
                    self.tableChat.reloadData()
                    
                    guard self.messages.data[0].ChatDetails.count > 0 else {
                        return
                    }
                    
                    self.tableChat.performBatchUpdates(nil, completion: {
                        (result) in
                        // ready
                        print("loaded")
                        self.tableChat.scrollToRow(at: IndexPath(row: self.messages.data[0].ChatDetails.count - 1, section: 0), at: .bottom, animated: false)
                    })
                    
                }
            }
        })
    }
    
    @IBAction func btnGalleryAction(_ sender: Any) {
        picker.allowsEditing = false
        picker.sourceType = UIImagePickerController.SourceType.savedPhotosAlbum
        picker.mediaTypes = [kUTTypeMovie as String, kUTTypeImage as String]
        present(picker, animated: true, completion: nil)
    }
    
    @IBAction func btnCameraAction(_ sender: Any) {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            picker.mediaTypes = [kUTTypeMovie as String, kUTTypeImage as String]
            present(picker, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            
            present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnDeleteMsgAction(_ sender: UIButton)
    {
        DeleteMsgApi()
    }
    
    func DeleteMsgApi()
    {
        WebServices().callUserService(service: UserServices.deleteChat, urlParameter: "", parameters: ["IdArray": messagesToDelete as AnyObject, "ToUserProfileId":ToUserId as AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            DispatchQueue.main.async {
            print(serviceResponse)
            self.navDeleteMsgView.isHidden = true
            self.messagesToDelete.removeAll()
            self.getChatData()
            }
//            for i in 0...self.messagesToDelete.count-1
//            {
////                let index = messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)
//                let index = self.messagesToDelete
//
//                messagesToDelete.remove(at: index!)
//            }
//
//            messages.data[0].ChatDetails[indexPath.row].Id
            
        })
    }
    
    @objc func btnDeleteMsgBackAction(_ sender: UIButton)
    {
        navDeleteMsgView.isHidden = true
//        longPressed = false
        messagesToDelete.removeAll()
        tableChat.reloadData()
    }
    
    @objc func btnMenuAction(_ sender: UIButton)
    {
        menuDropdown.dataSource = ["Clear Chat", "Block"]
        menuDropdown.cellHeight = 40.0
        menuDropdown.show()
    }
    
    func dropdownMenuSelection()
    {
        menuDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            switch index {
            case 0:
                self.clearChat()
                
            case 1:
                print("Block user")
                self.blockUser()
                
            default:
                break
            }
            
        }
    }
    
    func blockUser()
    {
        let param:[String:String] = ["UserID": String(ToUserId)]
        
        WebServices().callUserService(service: .blockUnblockUser, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    DispatchQueue.main.async {
                        self.navigationController?.popViewController(animated: true)
                    }

                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func clearChat()
    {
        WebServices().callUserService(service: UserServices.clearChat, urlParameter: "", parameters: ["ToUserProfileId":ToUserId as AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
            if serviceResponse["Status"]as! Bool
            {
                DispatchQueue.main.async {
                    self.messages.data[0].ChatDetails.removeAll()
                    self.tableChat.reloadData()
                }
            }
            
        })
    }
    
    @IBAction func btnMicrophoneAction(_ sender: Any) {
        if self.audioRecorder == nil {
            self.startRecording()
            self.btnMicrophone.setImage(#imageLiteral(resourceName: "record"), for: .normal)
        }
        else {
            self.finishRecording(success: true)
            self.btnMicrophone.setImage(#imageLiteral(resourceName: "UnRecord"), for: .normal)
        }
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        
        return documentsDirectory
    }
    
    func startRecording() {
        
        self.txtMessage.text = ""
        self.txtMessage.placeholder = ""
        self.txtMessage.placeholderColor = UIColor.red
        self.audioFilename = nil
        self.updateTimerRecording.invalidate()
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        //  self.loadRecordingUI()
                    } else {
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }
        
        
        self.audioFilename = getDocumentsDirectory().appendingPathComponent("recording.mp4")
        
        do {
            audioRecorder = try AVAudioRecorder(url: self.audioFilename, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record()
            print("Recording Started")
            
            DispatchQueue.main.async {
                self.txtMessage.placeholder = "00:00"
                self.txtMessage.placeholder = self.txtMessage.placeholder! + " (Tap to stop)"
                self.updateTimerRecording = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(self.updateTimeForRecording(_:)), userInfo: nil, repeats: true)
            }
            
        } catch {
            //    finishRecording(success: false)
            self.btnMicrophone.setImage(#imageLiteral(resourceName: "UnRecord"), for: .normal)
            print("Recording Stoped")
        }
    }
    
    @objc func updateTimeForRecording(_ sender: Timer) {
        
        //   print(audioRecorder)
        if audioRecorder == nil {
            updateTimerRecording.invalidate()
            return
        }
        
        let currentTime = Int(self.audioRecorder.currentTime)
        let minutes = currentTime/60
        let seconds = currentTime - minutes / 60
        print("Current time \(currentTime)")
        self.txtMessage.placeholder = NSString(format: "%02d:%02d", minutes,seconds) as String
        self.duration = NSString(format: "%02d:%02d", minutes,seconds) as String
        self.txtMessage.placeholder = self.txtMessage.placeholder! + " (Tap to stop)"
//        print("Timer Update \(recordingTimerlabel.text!)")
        
    }
    
    func finishRecording(success: Bool) {
        self.btnMicrophone.setImage(#imageLiteral(resourceName: "UnRecord"), for: .normal)
        self.txtMessage.placeholder = "Type your message..."
        self.txtMessage.placeholderColor = UIColor.darkGray
        self.audioPlayIndex = 0
        self.audioRecorder?.stop()
        self.updateTimerRecording.invalidate()
        
        print("Finished Recording")
        print(self.duration)
        
        if self.duration == ""
        {
            return
        }
        
        
        let alertController = UIAlertController(title: "TrenderAlert", message: "Are you sure you want to upload the audio?" , preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (action) in
            self.audioRecorder = nil
            let dict = ["Duration": "0"] as [String : Any]
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
                self.UploadAudio(parameter: jsonData as NSData)

            } catch {
                print(error.localizedDescription)
            }
            
            
        })
        alertController.addAction(okAction)
        let cancelAction = UIAlertAction(title: "CANCEL", style: UIAlertAction.Style.default, handler: { (action) in
            self.audioRecorder?.stop()
            self.updateTimerRecording.invalidate()
            self.audioRecorder = nil
            self.txtMessage.placeholder = "Type your message..."
            self.txtMessage.placeholderColor = UIColor.darkGray
            //  self.txtCommentView.text = ""
            self.btnMicrophone.setImage(#imageLiteral(resourceName: "UnRecord"), for: .normal)
        })
        alertController.addAction(cancelAction)
        UIApplication.shared.keyWindow!.rootViewController!.present(alertController, animated: true, completion: nil)
        
        self.btnMicrophone.setImage(#imageLiteral(resourceName: "UnRecord"), for: .normal)
        //        recordPlayButton.isEnabled = true
        
    }
    
    func UploadAudio(parameter:NSData?)-> Void{
        
        let boundary = "Boundary-\(UUID().uuidString)"
        DispatchQueue.main.async {
            MBProgressHUD.showAdded(to: self.view, animated: true)
        }
        let headers: HTTPHeaders = [
            "Authorization": "Bearer " + MySingleton.shared.loginObject.access_token,
            "content-type": "multipart/form-data; boundary= \(boundary)",
            "cache-control": "no-cache"
        ]
        
        var request = URLRequest(url: NSURL.init(string: "\(WebServices.baseURL)api/chat/UploadAudio")! as URL)
        
        request.allHTTPHeaderFields = headers
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(self.audioFilename!, withName: "fileUpload")
            let data = (self.duration).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
            print(data)
            let dataToUserId = (String(self.ToUserId)).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
            print(dataToUserId)
            multipartFormData.append(dataToUserId,  withName: "ToUserProfileId")
            let audioDuration = (self.duration).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
            print(audioDuration)
            multipartFormData.append(audioDuration,  withName: "Duration")
        }, with: request, encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _,_):
                upload.responseJSON { response in
                    
                    DispatchQueue.main.async {
                        MBProgressHUD.hide(for: self.view, animated: true)
                    }
                    if(response.result.value is [String:AnyObject])
                    {
                        if let JSON  = response.result.value as? [String:AnyObject]{
                            print(JSON)
                            if JSON["Status"] as! Bool == true
                            {
                                DispatchQueue.main.async {
                                    self.view.endEditing(true)
                                    SocketIoManager.sharedInstance.sendDataToEvent(.sendmessage, data: ["fromUserProfileId": MySingleton.shared.loginObject.UserID, "toUserProfileId": self.ToUserId, "userId": MySingleton.shared.loginObject.AspNetUserId, "ParentMediaId": JSON["data"]as! String])
                                }
                            }
                            else
                            {
                                TrenderAlertVC.shared.presentAlertController(message: "Something went wrong while uploading audio.", completionHandler: nil)
                            }
                            
                        }
                    }
                    
                    self.audioPlayIndex = 0
                    self.updateTimerForRecorded.invalidate()
                    self.duration = ""
                    self.txtMessage.placeholder = "Type your message..."
                    self.txtMessage.placeholderColor = UIColor.darkGray
                    self.audioRecorder?.stop()
                    self.updateTimerRecording.invalidate()
                    self.audioRecorder = nil
                }
            case .failure(let encodingError):
                // hide progressbas here
                debugPrint("SUCCESS Failure: \(encodingError)")
            }
        })
        
    }
    
    @IBAction func btnSendAction(_ sender: Any) {
        if !(txtMessage.text.isEmpty)
        {
            self.view.endEditing(true)
            SocketIoManager.sharedInstance.sendDataToEvent(.sendmessage, data: ["fromUserProfileId": MySingleton.shared.loginObject.UserID, "toUserProfileId": ToUserId, "userId": MySingleton.shared.loginObject.AspNetUserId, "message": txtMessage.text!])
            
//            let latestMessage = ChatData()
//            latestMessage.ChatText = txtMessage.text!
//            latestMessage.MediaType = "TEXT"
//            latestMessage.FromUserProfileId = Int(MySingleton.shared.loginObject.UserID)!
//            latestMessage.ToUserProfileId = ToUserId
//            messages.data[0].ChatDetails.append(latestMessage)
//            self.tableChat.dataSource = self
//            self.tableChat.delegate = self
//            self.tableChat.reloadData()
            txtMessage.text = ""
//            guard self.messages.data[0].ChatDetails.count > 0 else {
//                return
//            }
//
//            self.tableChat.performBatchUpdates(nil, completion: {
//                (result) in
//                // ready
//                print("loaded")
//                self.tableChat.scrollToRow(at: IndexPath(row: self.messages.data[0].ChatDetails.count - 1, section: 0), at: .bottom, animated: false)
//            })
        }
    }
}

extension ChatViewController :  AGEmojiKeyboardViewDelegate, AGEmojiKeyboardViewDataSource, UITextViewDelegate
{
    // MARK: - Emoji Delegate
    func emojiKeyBoardView(_ emojiKeyBoardView: AGEmojiKeyboardView!, didUseEmoji emoji: String!) {
        self.txtMessage.text = self.txtMessage.text + emoji
    }
    
    func emojiKeyBoardViewDidPressBackSpace(_ emojiKeyBoardView: AGEmojiKeyboardView!) {
        self.txtMessage.text = String(self.txtMessage.text.dropLast())
    }
    
    // MARK: - Emoji Data Source
    func emojiKeyboardView(_ emojiKeyboardView: AGEmojiKeyboardView!, imageForSelectedCategory category: AGEmojiKeyboardViewCategoryImage) -> UIImage! {
        if emojiSelectedIndex <= 4 {
            emojiSelectedIndex = emojiSelectedIndex + 1
        }
        return emojiImages[emojiSelectedIndex]
    }
    
    func emojiKeyboardView(_ emojiKeyboardView: AGEmojiKeyboardView!, imageForNonSelectedCategory category: AGEmojiKeyboardViewCategoryImage) -> UIImage! {
        if emojiNotSelectedIndex <= 4 {
            emojiNotSelectedIndex = emojiNotSelectedIndex + 1
        }
        return emojiImages[emojiNotSelectedIndex]
    }
    
    func backSpaceButtonImage(for emojiKeyboardView: AGEmojiKeyboardView!) -> UIImage! {
        return #imageLiteral(resourceName: "backspace_clk")
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        if self.txtMessage.inputView == agEmojiKeyboardView
        {
            self.txtData = self.txtMessage.text
            // self.txtCommentView.inputView = nil
            self.txtMessage.inputView = UIView()
            // self.txtCommentView.keyboardType = UIKeyboardType.default
            self.txtMessage.keyboardType = UIKeyboardType.default
            self.txtMessage.keyboardAppearance = .default
            self.txtMessage.becomeFirstResponder()
            self.txtMessage.text = self.txtData
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
       
        if self.txtMessage.inputView == agEmojiKeyboardView
        {
            self.txtData = self.txtMessage.text
            self.txtMessage.inputView = nil
            //   self.txtCommentView.inputView = UIView()
            //   self.txtCommentView.keyboardType = UIKeyboardType.default
            //self.txtCommentView.keyboardAppearance = .default
            self.txtMessage.becomeFirstResponder()
            self.txtMessage.text = self.txtData
        }
        
    }
}

extension ChatViewController : AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    
    
    
//    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool)
//    {
//        DispatchQueue.main.async {
//            let cell_indexPath = NSIndexPath(row: self.audioPlayIndex, section: 0)
//            let tableViewCell = self.tblComment.cellForRow(at: cell_indexPath as IndexPath) as? CommentsTableViewCell
//            tableViewCell?.btnPlayPause.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
//            let cellindexPath = NSIndexPath(row: self.audioPlayIndex, section: 0)
//            let tableViewCellOther = self.tblComment.cellForRow(at: cellindexPath as IndexPath) as? OthersCommentTableViewCell
//            tableViewCellOther?.btnComentAudioPlay.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
//        }
//        // self.audioRecorderPlayer?.stop()
//        //  self.audioRecorder?.stop()
//        self.updateTimerRecording.invalidate()
//
//    }
    
}

extension ChatViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.data[0].ChatDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         if (messages.data[0].ChatDetails[indexPath.row].IsDeleted)
         {
            if messages.data[0].ChatDetails[indexPath.row].FromUserProfileId == Int(MySingleton.shared.loginObject.UserID)
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MyChatDeletedMessageTableViewCell", for: indexPath)as! MyChatDeletedMessageTableViewCell
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "OtherChatDeleteTableViewCell", for: indexPath)as! OtherChatDeleteTableViewCell
                return cell
            }
        }
        else
         {
        if messages.data[0].ChatDetails[indexPath.row].MediaType == "TEXT"
        {
            if messages.data[0].ChatDetails[indexPath.row].FromUserProfileId == Int(MySingleton.shared.loginObject.UserID)
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MyChatTableViewCell", for: indexPath)as! MyChatTableViewCell
                cell.lblMyMsg.text = messages.data[0].ChatDetails[indexPath.row].ChatText
                cell.lblTime.text =  updateTime(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)
                if indexPath.row == 0
                {
                    cell.lblDate.text = updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)
                }
                else
                {
                    if ((updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)) == (updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row-1].CreatedDate)))
                    {
                        cell.lblDate.text = ""
                    }
                    else
                    {
                        cell.lblDate.text = updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)
                    }
                }
                if messagesToDelete.contains(messages.data[0].ChatDetails[indexPath.row].Id)
                {
                    cell.contentView.backgroundColor = UIColor.lightGray
                }
                else
                {
                    cell.contentView.backgroundColor = UIColor.clear
                }
                
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "OtherChatTableViewCell", for: indexPath)as! OtherChatTableViewCell
                cell.lblOtherMsg.text = messages.data[0].ChatDetails[indexPath.row].ChatText
                cell.lblTime.text = updateTime(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)
                if indexPath.row == 0
                {
                    cell.lblDate.text = updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)
                }
                else
                {
                    if ((updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)) == (updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row-1].CreatedDate)))
                    {
                        cell.lblDate.text = ""
                    }
                    else
                    {
                        cell.lblDate.text = updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)
                    }
                }
                cell.imgProfilePic.image = navProfilePic.image
                cell.imgProfilePic.makeCornerRadius(radius: cell.imgProfilePic.frame.size.height/2)
                
                if messagesToDelete.contains(messages.data[0].ChatDetails[indexPath.row].Id)
                {
                    cell.contentView.backgroundColor = UIColor.lightGray
                }
                else
                {
                    cell.contentView.backgroundColor = UIColor.clear
                }
                
                return cell
            }
        }
        else if messages.data[0].ChatDetails[indexPath.row].MediaType == "IMAGE"
        {
            if messages.data[0].ChatDetails[indexPath.row].FromUserProfileId == Int(MySingleton.shared.loginObject.UserID)
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MyChatImageTableViewCell", for: indexPath)as! MyChatImageTableViewCell
                cell.btnPlayVideo.isHidden = true
                let imageTapGesture = UITapGestureRecognizer(target: self, action: #selector(showZoomedImage))
                cell.lblTime.text = updateTime(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)
                cell.imgMedia.tag = indexPath.row
                cell.imgMedia.addGestureRecognizer(imageTapGesture)
                cell.imgMedia.isUserInteractionEnabled = true
                if indexPath.row == 0
                {
                    cell.lblDate.text = updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)
                }
                else
                {
                    if ((updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)) == (updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row-1].CreatedDate)))
                    {
                        cell.lblDate.text = ""
                    }
                    else
                    {
                        cell.lblDate.text = updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)
                    }
                }
                cell.imgMedia.sd_setImage(with: URL(string: messages.data[0].ChatDetails[indexPath.row].MediaPath!), placeholderImage: #imageLiteral(resourceName: "Img_LogoNly"))
                do {
                    DispatchQueue.global(qos: .background).async {
                        let data = try? Data(contentsOf: URL(string: self.messages.data[0].ChatDetails[indexPath.row].MediaPath!)!)
                        if data != nil {
                            DispatchQueue.main.async {
                     
                                var calculatedWidth = CGFloat()
                                calculatedWidth = (Utilities.getImageWidth(image: UIImage(data: data!)!))
                                var calculatedHeight = CGFloat()
                                calculatedHeight = (Utilities.getImageHeight(image: UIImage(data: data!)!))
                                
                                let updatedwidth = (calculatedWidth * 150)/calculatedHeight
                                
                                print(updatedwidth)
                                cell.cnstrntImageWidth.constant = updatedwidth
                            }
                        }
                    }
                } catch {
                    print("Unable to load data: \(error.localizedDescription)")
                }
                
                if messagesToDelete.contains(messages.data[0].ChatDetails[indexPath.row].Id)
                {
                    cell.contentView.backgroundColor = UIColor.lightGray
                }
                else
                {
                    cell.contentView.backgroundColor = UIColor.clear
                }
                
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "OthersChatImageTableViewCell", for: indexPath)as! OthersChatImageTableViewCell
                let imageTapGesture = UITapGestureRecognizer(target: self, action: #selector(showZoomedImage))
                cell.lblTime.text = updateTime(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)
                cell.imageMedia.tag = indexPath.row
                cell.imageMedia.addGestureRecognizer(imageTapGesture)
                cell.imageMedia.isUserInteractionEnabled = true
                cell.btnPlayVideo.isHidden = true
                if indexPath.row == 0
                {
                    cell.lblDate.text = updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)
                }
                else
                {
                    if ((updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)) == (updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row-1].CreatedDate)))
                    {
                        cell.lblDate.text = ""
                    }
                    else
                    {
                        cell.lblDate.text = updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)
                    }
                }
                cell.imageMedia.sd_setImage(with: URL(string: messages.data[0].ChatDetails[indexPath.row].MediaPath!), placeholderImage: #imageLiteral(resourceName: "img_selectProfilePic"))
                
                do {
                    DispatchQueue.global(qos: .background).async {
                        let data = try? Data(contentsOf: URL(string: self.messages.data[0].ChatDetails[indexPath.row].MediaPath!)!)
                        if data != nil {
                            DispatchQueue.main.async {
                                
                                var calculatedWidth = CGFloat()
                                calculatedWidth = (Utilities.getImageWidth(image: UIImage(data: data!)!))
                                var calculatedHeight = CGFloat()
                                calculatedHeight = (Utilities.getImageHeight(image: UIImage(data: data!)!))
                                
                                let updatedwidth = (calculatedWidth * 150)/calculatedHeight
                                
                                print(updatedwidth)
                                cell.cnstrntImageWidth.constant = updatedwidth
                            }
                        }
                    }
                } catch {
                    print("Unable to load data: \(error.localizedDescription)")
                }
                
                cell.imgProfilePic.image = navProfilePic.image
                cell.imgProfilePic.makeCornerRadius(radius: cell.imgProfilePic.frame.size.height/2)
                
                if messagesToDelete.contains(messages.data[0].ChatDetails[indexPath.row].Id)
                {
                    cell.contentView.backgroundColor = UIColor.lightGray
                }
                else
                {
                    cell.contentView.backgroundColor = UIColor.clear
                }
                return cell
            }
        }
        else if messages.data[0].ChatDetails[indexPath.row].MediaType == "AUDIO"
        {
            if messages.data[0].ChatDetails[indexPath.row].FromUserProfileId == Int(MySingleton.shared.loginObject.UserID)
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MyChatAudioTableViewCell", for: indexPath)as! MyChatAudioTableViewCell
                cell.lblAudio.text = messages.data[0].ChatDetails[indexPath.row].Duration!
                cell.sliderAudio.setThumbImage(UIImage(named:"img_SilderThumb"), for: .highlighted)
                cell.lblTime.text = updateTime(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)
                cell.sliderAudio.tag = indexPath.row
                if indexPath.row == 0
                {
                    cell.lblDate.text = updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)
                }
                else
                {
                    if ((updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)) == (updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row-1].CreatedDate)))
                    {
                        cell.lblDate.text = ""
                    }
                    else
                    {
                        cell.lblDate.text = updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)
                    }
                }
                cell.sliderAudio.addTarget(self, action: #selector(self.sliderActionForValueChangedMy(_:)), for: .valueChanged)
                cell.btnPlayAudio.tag = indexPath.row
                cell.btnPlayAudio.addTarget(self, action: #selector(self.playPauseActionReply(_:)), for: .touchUpInside)
                
                if messagesToDelete.contains(messages.data[0].ChatDetails[indexPath.row].Id)
                {
                    cell.contentView.backgroundColor = UIColor.lightGray
                }
                else
                {
                    cell.contentView.backgroundColor = UIColor.clear
                }
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "OthersChatAudioTableViewCell", for: indexPath)as! OthersChatAudioTableViewCell
                cell.lblAudio.text = messages.data[0].ChatDetails[indexPath.row].Duration!
                cell.sliderAudio.setThumbImage(UIImage(named:"img_SilderThumb"), for: .highlighted)
                cell.lblTime.text = updateTime(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)
                cell.imgProfilePic.image = navProfilePic.image
                cell.imgProfilePic.makeCornerRadius(radius: cell.imgProfilePic.frame.size.height/2)
                if indexPath.row == 0
                {
                    cell.lblDate.text = updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)
                }
                else
                {
                    if ((updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)) == (updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row-1].CreatedDate)))
                    {
                        cell.lblDate.text = ""
                    }
                    else
                    {
                        cell.lblDate.text = updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)
                    }
                }
                cell.sliderAudio.tag = indexPath.row
                cell.sliderAudio.addTarget(self, action: #selector(self.sliderActionForValueChangedOther(_:)), for: .valueChanged)
                cell.btnPlayAudio.tag = indexPath.row
                cell.btnPlayAudio.addTarget(self, action: #selector(self.playPauseActionReply(_:)), for: .touchUpInside)
                
                if messagesToDelete.contains(messages.data[0].ChatDetails[indexPath.row].Id)
                {
                    cell.contentView.backgroundColor = UIColor.lightGray
                }
                else
                {
                    cell.contentView.backgroundColor = UIColor.clear
                }
                return cell
            }
        }
        else
        {
            if messages.data[0].ChatDetails[indexPath.row].FromUserProfileId == Int(MySingleton.shared.loginObject.UserID)
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MyChatImageTableViewCell", for: indexPath)as! MyChatImageTableViewCell
                cell.btnPlayVideo.isHidden = false
                cell.btnPlayVideo.tag = indexPath.row
                cell.btnPlayVideo.addTarget(self, action: #selector(btnPlayVideoAction(_:)), for: .touchUpInside)
                if indexPath.row == 0
                {
                    cell.lblDate.text = updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)
                }
                else
                {
                    if ((updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)) == (updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row-1].CreatedDate)))
                    {
                        cell.lblDate.text = ""
                    }
                    else
                    {
                        cell.lblDate.text = updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)
                    }
                }
                cell.lblTime.text = updateTime(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)
                cell.imgMedia.sd_setImage(with: URL(string: messages.data[0].ChatDetails[indexPath.row].ThumbnailUrl!), placeholderImage: #imageLiteral(resourceName: "Img_LogoNly"))
                do {
                    DispatchQueue.global(qos: .background).async {
                        let data = try? Data(contentsOf: URL(string: self.messages.data[0].ChatDetails[indexPath.row].ThumbnailUrl!)!)
                        if data != nil {
                            DispatchQueue.main.async {
                                
                                var calculatedWidth = CGFloat()
                                calculatedWidth = (Utilities.getImageWidth(image: UIImage(data: data!)!))
                                var calculatedHeight = CGFloat()
                                calculatedHeight = (Utilities.getImageHeight(image: UIImage(data: data!)!))
                                
                                let updatedwidth = (calculatedWidth * 150)/calculatedHeight
                                
                                print(updatedwidth)
                                cell.cnstrntImageWidth.constant = updatedwidth
                            }
                        }
                    }
                } catch {
                    print("Unable to load data: \(error.localizedDescription)")
                }
                
                if messagesToDelete.contains(messages.data[0].ChatDetails[indexPath.row].Id)
                {
                    cell.contentView.backgroundColor = UIColor.lightGray
                }
                else
                {
                    cell.contentView.backgroundColor = UIColor.clear
                }
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "OthersChatImageTableViewCell", for: indexPath)as! OthersChatImageTableViewCell
                cell.btnPlayVideo.tag = indexPath.row
                cell.btnPlayVideo.addTarget(self, action: #selector(btnPlayVideoAction(_:)), for: .touchUpInside)
                cell.btnPlayVideo.isHidden = false
                cell.imgProfilePic.image = navProfilePic.image
                cell.imgProfilePic.makeCornerRadius(radius: cell.imgProfilePic.frame.size.height/2)
                if indexPath.row == 0
                {
                    cell.lblDate.text = updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)
                }
                else
                {
                    if ((updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)) == (updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row-1].CreatedDate)))
                    {
                        cell.lblDate.text = ""
                    }
                    else
                    {
                        cell.lblDate.text = updateDate(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)
                    }
                }
                cell.lblTime.text = updateTime(dateStr: messages.data[0].ChatDetails[indexPath.row].CreatedDate)
                cell.imageMedia.sd_setImage(with: URL(string: messages.data[0].ChatDetails[indexPath.row].ThumbnailUrl!), placeholderImage: #imageLiteral(resourceName: "img_selectProfilePic"))
                
                do {
                    DispatchQueue.global(qos: .background).async {
                        let data = try? Data(contentsOf: URL(string: self.messages.data[0].ChatDetails[indexPath.row].ThumbnailUrl!)!)
                        if data != nil {
                            DispatchQueue.main.async {
                                
                                var calculatedWidth = CGFloat()
                                calculatedWidth = (Utilities.getImageWidth(image: UIImage(data: data!)!))
                                var calculatedHeight = CGFloat()
                                calculatedHeight = (Utilities.getImageHeight(image: UIImage(data: data!)!))
                                
                                let updatedwidth = (calculatedWidth * 150)/calculatedHeight
                                
                                print(updatedwidth)
                                cell.cnstrntImageWidth.constant = updatedwidth
                            }
                        }
                    }
                } catch {
                    print("Unable to load data: \(error.localizedDescription)")
                }
                
                if messagesToDelete.contains(messages.data[0].ChatDetails[indexPath.row].Id)
                {
                    cell.contentView.backgroundColor = UIColor.lightGray
                }
                else
                {
                    cell.contentView.backgroundColor = UIColor.clear
                }
                return cell
            }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if messagesToDelete.count == 0
        {
            return
        }
        if (!(messages.data[0].ChatDetails[indexPath.row].IsDeleted))
        {
            
            if messages.data[0].ChatDetails[indexPath.row].MediaType == "TEXT"
            {
                if messages.data[0].ChatDetails[indexPath.row].FromUserProfileId == Int(MySingleton.shared.loginObject.UserID)
                {
                    let cell = self.tableChat.cellForRow(at: indexPath)as! MyChatTableViewCell
                    if cell.contentView.backgroundColor == UIColor.lightGray
                    {
                        cell.contentView.backgroundColor = UIColor.clear
                        print(messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)!)
                        let index = messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)
                        messagesToDelete.remove(at: index!)
                    }
                    else
                    {
                        cell.contentView.backgroundColor = UIColor.lightGray
                        messagesToDelete.append(messages.data[0].ChatDetails[indexPath.row].Id)
                    }
                }
                else
                {
                    let cell = self.tableChat.cellForRow(at: indexPath)as! OtherChatTableViewCell
                    if cell.contentView.backgroundColor == UIColor.lightGray
                    {
                        cell.contentView.backgroundColor = UIColor.clear
                        print(messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)!)
                        
                        let index = messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)
                        messagesToDelete.remove(at: index!)
                    }
                    else
                    {
                        cell.contentView.backgroundColor = UIColor.lightGray
                        messagesToDelete.append(messages.data[0].ChatDetails[indexPath.row].Id)
                    }
                }
                
            }
            else if messages.data[0].ChatDetails[indexPath.row].MediaType == "IMAGE"
            {
                if messages.data[0].ChatDetails[indexPath.row].FromUserProfileId == Int(MySingleton.shared.loginObject.UserID)
                {
                    let cell = self.tableChat.cellForRow(at: indexPath)as! MyChatImageTableViewCell
                    if cell.contentView.backgroundColor == UIColor.lightGray
                    {
                        cell.contentView.backgroundColor = UIColor.clear
                        print(messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)!)
                        let index = messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)
                        messagesToDelete.remove(at: index!)
                    }
                    else
                    {
                        cell.contentView.backgroundColor = UIColor.lightGray
                        messagesToDelete.append(messages.data[0].ChatDetails[indexPath.row].Id)
                    }
                }
                else
                {
                    let cell = self.tableChat.cellForRow(at: indexPath)as! OthersChatImageTableViewCell
                    if cell.contentView.backgroundColor == UIColor.lightGray
                    {
                        cell.contentView.backgroundColor = UIColor.clear
                        print(messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)!)
                        let index = messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)
                        messagesToDelete.remove(at: index!)
                    }
                    else
                    {
                        cell.contentView.backgroundColor = UIColor.lightGray
                        messagesToDelete.append(messages.data[0].ChatDetails[indexPath.row].Id)
                    }
                }
            }
            else if messages.data[0].ChatDetails[indexPath.row].MediaType == "AUDIO"
            {
                if messages.data[0].ChatDetails[indexPath.row].FromUserProfileId == Int(MySingleton.shared.loginObject.UserID)
                {
                    let cell = self.tableChat.cellForRow(at: indexPath)as! MyChatAudioTableViewCell
                    if cell.contentView.backgroundColor == UIColor.lightGray
                    {
                        cell.contentView.backgroundColor = UIColor.clear
                        print(messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)!)
                        let index = messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)
                        messagesToDelete.remove(at: index!)
                    }
                    else
                    {
                        cell.contentView.backgroundColor = UIColor.lightGray
                        messagesToDelete.append(messages.data[0].ChatDetails[indexPath.row].Id)
                    }
                }
                else
                {
                    let cell = self.tableChat.cellForRow(at: indexPath)as! OthersChatAudioTableViewCell
                    if cell.contentView.backgroundColor == UIColor.lightGray
                    {
                        cell.contentView.backgroundColor = UIColor.clear
                        print(messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)!)
                        let index = messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)
                        messagesToDelete.remove(at: index!)
                    }
                    else
                    {
                        cell.contentView.backgroundColor = UIColor.lightGray
                        messagesToDelete.append(messages.data[0].ChatDetails[indexPath.row].Id)
                    }
                }
            }
            else
            {
                if messages.data[0].ChatDetails[indexPath.row].FromUserProfileId == Int(MySingleton.shared.loginObject.UserID)
                {
                    let cell = self.tableChat.cellForRow(at: indexPath)as! MyChatImageTableViewCell
                    if cell.contentView.backgroundColor == UIColor.lightGray
                    {
                        cell.contentView.backgroundColor = UIColor.clear
                        print(messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)!)
                        let index = messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)
                        messagesToDelete.remove(at: index!)
                    }
                    else
                    {
                        cell.contentView.backgroundColor = UIColor.lightGray
                        messagesToDelete.append(messages.data[0].ChatDetails[indexPath.row].Id)
                    }
                }
                else
                {
                    let cell = self.tableChat.cellForRow(at: indexPath)as! OthersChatImageTableViewCell
                    if cell.contentView.backgroundColor == UIColor.lightGray
                    {
                        cell.contentView.backgroundColor = UIColor.clear
                        print(messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)!)
                        let index = messagesToDelete.lastIndex(of: messages.data[0].ChatDetails[indexPath.row].Id)
                        messagesToDelete.remove(at: index!)
                    }
                    else
                    {
                        cell.contentView.backgroundColor = UIColor.lightGray
                        messagesToDelete.append(messages.data[0].ChatDetails[indexPath.row].Id)
                    }
                }
            }
            
        }
        if messagesToDelete.count == 0
        {
            navDeleteMsgView.isHidden = true
        }
    }
    
    func updateDate(dateStr : String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        //        dateFormatter.timeZone = TimeZone.current
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        if let date = dateFormatter.date(from: dateStr)
        {
            print(date)
            dateFormatter.dateFormat = "MM/dd/yyyy"
            dateFormatter.timeZone = TimeZone.current
            
            let strDate = dateFormatter.string(from: date)
            print(strDate)
            let calendar = Calendar.current
            if calendar.isDateInToday(date)
            {
                return "Today"
            }
            else if calendar.isDateInYesterday(date)
            {
                return "Yesterday"
            }
            return strDate
        }
        else
        {
            return ""
        }
    }
    
    func updateTime(dateStr : String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
//        dateFormatter.timeZone = TimeZone.current
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        if let date = dateFormatter.date(from: dateStr)
        {
            print(date)
            dateFormatter.dateFormat = "h:mm a"
            dateFormatter.timeZone = TimeZone.current
            
            let strTime = dateFormatter.string(from: date)
            print(dateStr)
            print(strTime)
            return strTime
        }
        else
        {
            return ""
        }
    }
    
    @objc func showZoomedImage(_ sender:UITapGestureRecognizer){
        
        let zoomImageView = ZoomImageVC(nibName: "ZoomImageVC", bundle: nil)
        zoomImageView.imageUrl =  self.messages.data[0].ChatDetails[(sender.view?.tag)!].MediaPath!
        self.navigationController?.pushViewController(zoomImageView, animated: true)
    }
    
    @objc func btnPlayVideoAction(_ sender : UIButton)
    {
        let videoURL = URL(string: self.messages.data[0].ChatDetails[sender.tag].MediaPath!)
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    @objc func sliderActionForValueChangedOther(_ sender: UISlider) {
        let cell_indexPath = NSIndexPath(row: sender.tag, section: 0)
        let tableViewCell = self.tableChat.cellForRow(at: cell_indexPath as IndexPath) as? OthersChatAudioTableViewCell
        
        if tableViewCell?.audioPlayer.currentItem != nil
        {
            let currentTime = Int(Float(CMTimeGetSeconds((tableViewCell?.audioPlayer.currentItem!.currentTime())!)))
            let duration = Int(Float(CMTimeGetSeconds((tableViewCell?.audioPlayer.currentItem!.asset.duration)!)))
            
            let minutes = Int64(sender.value)/60
            let seconds = Int64(sender.value) - minutes / 60
            print("Current time \(currentTime)")
            let targetTime:CMTime = CMTimeMake(value: seconds, timescale: 1)
            tableViewCell?.audioPlayer.seek(to: targetTime)
            
            tableViewCell?.sliderAudio.value = Float(CMTimeGetSeconds(targetTime))
            tableViewCell?.lblAudio.text = NSString(format: "%02d:%02d", minutes,seconds) as String
            //
            print("Timer Update for added recording \(tableViewCell?.lblAudio.text)")
            
            if currentTime == duration
            {
                print("Timer Update to 0")
                tableViewCell?.audioPlayer.pause()
                tableViewCell?.sliderAudio.value = 0.0
                tableViewCell?.btnPlayAudio.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
                self.updateTimerForRecorded.invalidate()
            }
        }
        else
        {
            DispatchQueue.main.async {
                //   self.downloadFileFromURL(url: URL(string:self.CommentData.data[sender.tag].CommentAudio!)!,tableView: "comment")
                self.play(url: URL(string:self.messages.data[0].ChatDetails[sender.tag].MediaPath!)!, tableView: "OtherChat")
            }
            self.audioPlayIndex = sender.tag
            
        }
    }
    
    @objc func sliderActionForValueChangedMy(_ sender: UISlider) {
        let cell_indexPath = NSIndexPath(row: sender.tag, section: 0)
        let tableViewCell = self.tableChat.cellForRow(at: cell_indexPath as IndexPath) as? MyChatAudioTableViewCell
        
        if tableViewCell?.audioPlayer.currentItem != nil
        {
            let currentTime = Int(Float(CMTimeGetSeconds((tableViewCell?.audioPlayer.currentItem!.currentTime())!)))
            let duration = Int(Float(CMTimeGetSeconds((tableViewCell?.audioPlayer.currentItem!.asset.duration)!)))
            
            let minutes = Int64(sender.value)/60
            let seconds = Int64(sender.value) - minutes / 60
            print("Current time \(currentTime)")
            let targetTime:CMTime = CMTimeMake(value: seconds, timescale: 1)
            tableViewCell?.audioPlayer.seek(to: targetTime)
            
            tableViewCell?.sliderAudio.value = Float(CMTimeGetSeconds(targetTime))
            tableViewCell?.lblAudio.text = NSString(format: "%02d:%02d", minutes,seconds) as String
            //
            print("Timer Update for added recording \(tableViewCell?.lblAudio.text)")
            
            if currentTime == duration
            {
                print("Timer Update to 0")
                tableViewCell?.audioPlayer.pause()
                tableViewCell?.sliderAudio.value = 0.0
                tableViewCell?.btnPlayAudio.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
                self.updateTimerForRecorded.invalidate()
            }
        }
        else
        {
                DispatchQueue.main.async {
                    //   self.downloadFileFromURL(url: URL(string:self.CommentData.data[sender.tag].CommentAudio!)!,tableView: "comment")
                    self.play(url: URL(string:self.messages.data[0].ChatDetails[sender.tag].MediaPath!)!, tableView: "MyChat")
                }
                self.audioPlayIndex = sender.tag
            
        }
    }
    
    @objc func playPauseActionReply(_ sender: UIButton)
    {
        if messages.data[0].ChatDetails[sender.tag].FromUserProfileId == Int(MySingleton.shared.loginObject.UserID)
        {
            // my chat
        let cell_indexPath = NSIndexPath(row: sender.tag, section: 0)
        let tableViewCell = self.tableChat.cellForRow(at: cell_indexPath as IndexPath) as! MyChatAudioTableViewCell
        print(tableViewCell.audioPlayer.timeControlStatus.rawValue)
        tableViewCell.recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try tableViewCell.recordingSession.setCategory(.playAndRecord, mode: .default)
            try tableViewCell.recordingSession.setActive(true)
            tableViewCell.recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        //  self.loadRecordingUI()
                    } else {
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }
        if  tableViewCell.audioPlayer.timeControlStatus == AVPlayer.TimeControlStatus.paused
        {
            print("play")
            if tableViewCell.sliderAudio.value > 0.0 && self.audioPlayIndex == sender.tag
            {
                tableViewCell.audioPlayer.play()
                tableViewCell.btnPlayAudio.setImage(#imageLiteral(resourceName: "img_pauseAudio"), for: .normal)
                self.updateTimerForRecorded = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(ChatViewController.updateTimeForRecordingSlider), userInfo: nil, repeats: true)
                
            }
            else
            {
                self.updateTimerForRecorded.invalidate()
                if messages.data[0].ChatDetails.count != 0
                {
                    for i in 0...self.messages.data[0].ChatDetails.count-1
                    {
                        if messages.data[0].ChatDetails[i].FromUserProfileId == Int(MySingleton.shared.loginObject.UserID)
                        {
                        if (messages.data[0].ChatDetails[i].MediaPath != nil) && messages.data[0].ChatDetails[i].MediaType == "AUDIO"
                        {
                            if messages.data[0].ChatDetails[i].FromUserProfileId == Int(MySingleton.shared.loginObject.UserID)
                            {
                                let cell_indexPath = NSIndexPath(row: i, section: 0)
                                let tableViewCell = self.tableChat.cellForRow(at: cell_indexPath as IndexPath) as? MyChatAudioTableViewCell
    //                            tableViewCell.lblCommentTime.text = self.CommentData.data[i].Duration
                                tableViewCell?.btnPlayAudio.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
                                tableViewCell?.sliderAudio.value = 0
                                tableViewCell?.audioPlayer.pause()
                                self.updateTimerForRecorded.invalidate() 
                            }
                            else
                            {
                                let cell_indexPath = NSIndexPath(row: i, section: 0)
                                let tableViewCell = self.tableChat.cellForRow(at: cell_indexPath as IndexPath) as? OthersChatAudioTableViewCell
                                //                            tableViewCell.lblCommentTime.text = self.CommentData.data[i].Duration
                                tableViewCell?.btnPlayAudio.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
                                tableViewCell?.sliderAudio.value = 0
                                tableViewCell?.audioPlayer.pause()
                                self.updateTimerForRecorded.invalidate()
                            }
                        }
                            
                        }
                        else
                        {
                            if (messages.data[0].ChatDetails[i].MediaPath != nil) && messages.data[0].ChatDetails[i].MediaType == "AUDIO"
                            {
                                if messages.data[0].ChatDetails[i].FromUserProfileId == Int(MySingleton.shared.loginObject.UserID)
                                {
                                    let cell_indexPath = NSIndexPath(row: i, section: 0)
                                    let tableViewCell = self.tableChat.cellForRow(at: cell_indexPath as IndexPath) as? MyChatAudioTableViewCell
                                    //                            tableViewCell.lblCommentTime.text = self.CommentData.data[i].Duration
                                    tableViewCell?.btnPlayAudio.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
                                    tableViewCell?.sliderAudio.value = 0
                                    tableViewCell?.audioPlayer.pause()
                                    self.updateTimerForRecorded.invalidate()
                                }
                                else
                                {
                                    let cell_indexPath = NSIndexPath(row: i, section: 0)
                                    let tableViewCell = self.tableChat.cellForRow(at: cell_indexPath as IndexPath) as? OthersChatAudioTableViewCell
                                    //                            tableViewCell.lblCommentTime.text = self.CommentData.data[i].Duration
                                    tableViewCell?.btnPlayAudio.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
                                    tableViewCell?.sliderAudio.value = 0
                                    tableViewCell?.audioPlayer.pause()
                                    self.updateTimerForRecorded.invalidate()
                                }
                            }
                        }
                    }
                    DispatchQueue.main.async {
                        //   self.downloadFileFromURL(url: URL(string:self.CommentData.data[sender.tag].CommentAudio!)!,tableView: "comment")
                        self.play(url: URL(string:self.messages.data[0].ChatDetails[sender.tag].MediaPath!)!, tableView: "MyChat")
                    }
                    self.audioPlayIndex = sender.tag      
                    
                }
                
            }
        }
        else if tableViewCell.audioPlayer.timeControlStatus == AVPlayer.TimeControlStatus.playing
        {
            print("pause")
            tableViewCell.audioPlayer.pause()
            tableViewCell.btnPlayAudio.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
            
        }
        }
        else
        {
            //others chat
            let cell_indexPath = NSIndexPath(row: sender.tag, section: 0)
            let tableViewCell = self.tableChat.cellForRow(at: cell_indexPath as IndexPath) as! OthersChatAudioTableViewCell
            print(tableViewCell.audioPlayer.timeControlStatus.rawValue)
            tableViewCell.recordingSession = AVAudioSession.sharedInstance()
            
            do {
                try tableViewCell.recordingSession.setCategory(.playAndRecord, mode: .default)
                try tableViewCell.recordingSession.setActive(true)
                tableViewCell.recordingSession.requestRecordPermission() { [unowned self] allowed in
                    DispatchQueue.main.async {
                        if allowed {
                            //  self.loadRecordingUI()
                        } else {
                            // failed to record!
                        }
                    }
                }
            } catch {
                // failed to record!
            }
            if  tableViewCell.audioPlayer.timeControlStatus == AVPlayer.TimeControlStatus.paused
            {
                print("play")
                if tableViewCell.sliderAudio.value > 0.0 && self.audioPlayIndex == sender.tag
                {
                    tableViewCell.audioPlayer.play()
                    tableViewCell.btnPlayAudio.setImage(#imageLiteral(resourceName: "img_pauseAudio"), for: .normal)
                    self.updateTimerForRecorded = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(ChatViewController.updateTimeForRecordingSliderOther), userInfo: nil, repeats: true)
                }
                else
                {
                    self.updateTimerForRecorded.invalidate()
                    if messages.data[0].ChatDetails.count != 0
                    {
                        for i in 0...self.messages.data[0].ChatDetails.count-1
                        {
                            if messages.data[0].ChatDetails[i].FromUserProfileId == Int(MySingleton.shared.loginObject.UserID)
                            {
                                if (messages.data[0].ChatDetails[i].MediaPath != nil) && messages.data[0].ChatDetails[i].MediaType == "AUDIO"
                                {
                                    if messages.data[0].ChatDetails[i].FromUserProfileId == Int(MySingleton.shared.loginObject.UserID)
                                    {
                                        let cell_indexPath = NSIndexPath(row: i, section: 0)
                                        let tableViewCell = self.tableChat.cellForRow(at: cell_indexPath as IndexPath) as? MyChatAudioTableViewCell
                                        //                            tableViewCell.lblCommentTime.text = self.CommentData.data[i].Duration
                                        tableViewCell?.btnPlayAudio.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
                                        tableViewCell?.sliderAudio.value = 0
                                        tableViewCell?.audioPlayer.pause()
                                        self.updateTimerForRecorded.invalidate()
                                    }
                                    else
                                    {
                                        let cell_indexPath = NSIndexPath(row: i, section: 0)
                                        let tableViewCell = self.tableChat.cellForRow(at: cell_indexPath as IndexPath) as? OthersChatAudioTableViewCell
                                        //                            tableViewCell.lblCommentTime.text = self.CommentData.data[i].Duration
                                        tableViewCell?.btnPlayAudio.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
                                        tableViewCell?.sliderAudio.value = 0
                                        tableViewCell?.audioPlayer.pause()
                                        self.updateTimerForRecorded.invalidate()
                                    }
                                }
                            }
                            else
                            {
                                if (messages.data[0].ChatDetails[i].MediaPath != nil) && messages.data[0].ChatDetails[i].MediaType == "AUDIO"
                                {
                                    if messages.data[0].ChatDetails[i].FromUserProfileId == Int(MySingleton.shared.loginObject.UserID)
                                    {
                                        let cell_indexPath = NSIndexPath(row: i, section: 0)
                                        let tableViewCell = self.tableChat.cellForRow(at: cell_indexPath as IndexPath) as? MyChatAudioTableViewCell
                                        //                            tableViewCell.lblCommentTime.text = self.CommentData.data[i].Duration
                                        tableViewCell?.btnPlayAudio.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
                                        tableViewCell?.sliderAudio.value = 0
                                        tableViewCell?.audioPlayer.pause()
                                        self.updateTimerForRecorded.invalidate()
                                    }
                                    else
                                    {
                                        let cell_indexPath = NSIndexPath(row: i, section: 0)
                                        let tableViewCell = self.tableChat.cellForRow(at: cell_indexPath as IndexPath) as? OthersChatAudioTableViewCell
                                        //                            tableViewCell.lblCommentTime.text = self.CommentData.data[i].Duration
                                        tableViewCell?.btnPlayAudio.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
                                        tableViewCell?.sliderAudio.value = 0
                                        tableViewCell?.audioPlayer.pause()
                                        self.updateTimerForRecorded.invalidate()
                                    }
                                }
                            }
                        }
                        DispatchQueue.main.async {
                            //   self.downloadFileFromURL(url: URL(string:self.CommentData.data[sender.tag].CommentAudio!)!,tableView: "comment")
                            self.play(url: URL(string:self.messages.data[0].ChatDetails[sender.tag].MediaPath!)!, tableView: "OtherChat")
                        }
                        self.audioPlayIndex = sender.tag
                        
                    }
                    
                }
            }
            else if tableViewCell.audioPlayer.timeControlStatus == AVPlayer.TimeControlStatus.playing
            {
                print("pause")
                tableViewCell.audioPlayer.pause()
                tableViewCell.btnPlayAudio.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
                
            }
        }
        
    }
    
    func play(url:URL,tableView:String)
    {
        if tableView == "MyChat"
        {
            DispatchQueue.main.async {
            let cell_indexPath = NSIndexPath(row: self.audioPlayIndex, section: 0)
            let tableViewCell = self.tableChat.cellForRow(at: cell_indexPath as IndexPath) as! MyChatAudioTableViewCell
            if tableViewCell.btnPlayAudio.currentImage == #imageLiteral(resourceName: "img_PlayAudio")
            {
                print("playing \(url)")
                do {
                
                
                tableViewCell.audioPlayer = try AVPlayer(url: url)
                tableViewCell.audioPlayer.volume = 1.0
                tableViewCell.audioPlayer.play()
                
                tableViewCell.sliderAudio.maximumValue = Float(CMTimeGetSeconds(tableViewCell.audioPlayer.currentItem!.asset.duration))
                tableViewCell.btnPlayAudio.setImage(#imageLiteral(resourceName: "img_pauseAudio"), for: .normal)
                tableViewCell.sliderAudio.value = 0.0
                // self.audioPlayIndex = cell_indexPath.row
                self.updateTimerForRecorded = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(ChatViewController.updateTimeForRecordingSlider), userInfo: nil, repeats: true)
                
                
                } catch let error as NSError {
                print(error.localizedDescription)
                } catch {
                print("AVAudioPlayer init failed")
                }
                //  self.audioRecorderPlayer.play()
                
            
            }
            else
            {
                DispatchQueue.main.async {
                let cell_indexPath = NSIndexPath(row: self.audioPlayIndex, section: 0)
                let tableViewCell = self.tableChat.cellForRow(at: cell_indexPath as IndexPath) as! MyChatAudioTableViewCell
                tableViewCell.audioPlayer.pause()
                tableViewCell.btnPlayAudio.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
                }
            }
            }
        }
        else
        {
            DispatchQueue.main.async {
                let cell_indexPath = NSIndexPath(row: self.audioPlayIndex, section: 0)
                let tableViewCell = self.tableChat.cellForRow(at: cell_indexPath as IndexPath) as! OthersChatAudioTableViewCell
                if tableViewCell.btnPlayAudio.currentImage == #imageLiteral(resourceName: "img_PlayAudio")
                {
                    print("playing \(url)")
                    do {
                        
                        tableViewCell.audioPlayer = try AVPlayer(url: url)
                        tableViewCell.audioPlayer.volume = 1.0
                        tableViewCell.audioPlayer.play()
                        
                        tableViewCell.sliderAudio.maximumValue = Float(CMTimeGetSeconds(tableViewCell.audioPlayer.currentItem!.asset.duration))
                        tableViewCell.btnPlayAudio.setImage(#imageLiteral(resourceName: "img_pauseAudio"), for: .normal)
                        tableViewCell.sliderAudio.value = 0.0
                        // self.audioPlayIndex = cell_indexPath.row
                        self.updateTimerForRecorded = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(ChatViewController.updateTimeForRecordingSliderOther), userInfo: nil, repeats: true)
                        
                        
                    } catch let error as NSError {
                        print(error.localizedDescription)
                    } catch {
                        print("AVAudioPlayer init failed")
                    }
                    //  self.audioRecorderPlayer.play()
                    
                    
                }
                else
                {
                    DispatchQueue.main.async {
                        let cell_indexPath = NSIndexPath(row: self.audioPlayIndex, section: 0)
                        let tableViewCell = self.tableChat.cellForRow(at: cell_indexPath as IndexPath) as! OthersChatAudioTableViewCell
                        tableViewCell.audioPlayer.pause()
                        tableViewCell.btnPlayAudio.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
                    }
                }
            }
        }
    }
    
    @objc func updateTimeForRecordingSliderOther() {
        let cell_indexPath = NSIndexPath(row: self.audioPlayIndex, section: 0)
        let tableViewCell = self.tableChat.cellForRow(at: cell_indexPath as IndexPath) as? OthersChatAudioTableViewCell
        if tableViewCell?.audioPlayer.currentItem != nil
        {
            let currentTime = Int(Float(CMTimeGetSeconds((tableViewCell?.audioPlayer.currentItem!.currentTime())!)))
            let duration = Int(Float(CMTimeGetSeconds((tableViewCell?.audioPlayer.currentItem!.asset.duration)!)))
            let total = currentTime - duration
            let totalString = String(total)
            
            let minutes = currentTime/60
            let seconds = currentTime - minutes / 60
            print("Current time \(currentTime)")
            tableViewCell?.sliderAudio.value = Float(CMTimeGetSeconds((tableViewCell?.audioPlayer.currentItem!.currentTime())!))
            tableViewCell?.lblAudio.text = NSString(format: "%02d:%02d", minutes,seconds) as String
            //
            print("Timer Update for added recording \(String(describing: tableViewCell?.lblAudio.text))")
            
            if currentTime == duration
            {
                print("Timer Update to 0")
                tableViewCell?.audioPlayer.pause()
                tableViewCell?.sliderAudio.value = 0.0
                tableViewCell?.btnPlayAudio.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
                self.updateTimerForRecorded.invalidate()
            }
        }
    }
    
    @objc func updateTimeForRecordingSlider() {
        let cell_indexPath = NSIndexPath(row: self.audioPlayIndex, section: 0)
        let tableViewCell = self.tableChat.cellForRow(at: cell_indexPath as IndexPath) as? MyChatAudioTableViewCell
        if tableViewCell?.audioPlayer.currentItem != nil
        {
            let currentTime = Int(Float(CMTimeGetSeconds((tableViewCell?.audioPlayer.currentItem!.currentTime())!)))
            let duration = Int(Float(CMTimeGetSeconds((tableViewCell?.audioPlayer.currentItem!.asset.duration)!)))
            let total = currentTime - duration
            let totalString = String(total)
            
            let minutes = currentTime/60
            let seconds = currentTime - minutes / 60
            print("Current time \(currentTime)")
            tableViewCell?.sliderAudio.value = Float(CMTimeGetSeconds((tableViewCell?.audioPlayer.currentItem!.currentTime())!))
            tableViewCell?.lblAudio.text = NSString(format: "%02d:%02d", minutes,seconds) as String
            //
            print("Timer Update for added recording \(String(describing: tableViewCell?.lblAudio.text))")
            
            if currentTime == duration
            {
                print("Timer Update to 0")
                tableViewCell?.audioPlayer.pause()
                tableViewCell?.sliderAudio.value = 0.0
                tableViewCell?.btnPlayAudio.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
                self.updateTimerForRecorded.invalidate()
            }
        }
    }
    
    func getAspectRatioAccordingToiPhones(cellImageFrame:CGSize,downloadedImage: UIImage)->CGFloat {
        let widthOffset = downloadedImage.size.width - cellImageFrame.width
        let widthOffsetPercentage = (widthOffset*100)/downloadedImage.size.width
        let heightOffset = (widthOffsetPercentage * downloadedImage.size.height)/100
        let effectiveHeight = downloadedImage.size.height - heightOffset
        return(effectiveHeight)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed("ChatNavigation", owner: nil, options: nil)![0] as! T
    }
}

extension ChatViewController : SocketIOManagerDelegateChat
{
    
    func messageReceivedChat(_ data: [String : Any]) {
        
        if ToUserId == Int(data["ToUserProfileId"]as! String)! || ToUserId == Int(data["FromUserProfileId"]as! String)!
        {
            let latestMessage = ChatData()
            if data["ChatText"]is String
            {
                latestMessage.ChatText = data["ChatText"]as! String
            }
            else{
                latestMessage.ChatText = ""
            }
            if (data["MediaType"]as! String == "IMAGE" || data["MediaType"]as! String == "AUDIO" || data["MediaType"]as! String == "VIDEO")
            {
                latestMessage.MediaPath = data["MediaPath"]as? String
            }
            if (data["ThumbnailUrl"]is String)
            {
                latestMessage.ThumbnailUrl = data["ThumbnailUrl"]as! String
            }
            if (data["Duration"]is String)
            {
                latestMessage.Duration = data["Duration"]as! String
            }
            if (data["CreatedDate"]is String)
            {
                latestMessage.CreatedDate = data["CreatedDate"]as! String
            }
            latestMessage.IsDeleted = false
            if data["Id"]is Int
            {
                latestMessage.Id = data["Id"]as! Int
            }
            else
            {
                latestMessage.Id = Int(data["Id"]as! String)!
            }
            latestMessage.MediaType = data["MediaType"]as! String
            latestMessage.FromUserProfileId = Int(data["FromUserProfileId"]as! String)!
            latestMessage.ToUserProfileId = Int(data["ToUserProfileId"]as! String)!
            messages.data[0].ChatDetails.append(latestMessage)
            self.tableChat.dataSource = self
            self.tableChat.delegate = self
            self.tableChat.reloadData()
            guard self.messages.data[0].ChatDetails.count > 0 else {
                return
            }
            
            self.tableChat.performBatchUpdates(nil, completion: {
                (result) in
                // ready
                print("loaded")
                self.tableChat.scrollToRow(at: IndexPath(row: self.messages.data[0].ChatDetails.count - 1, section: 0), at: .bottom, animated: false)
            })
        }
    }
}

extension ChatViewController: SocketIOManagerDelegateUserStatus
{
    func userStatus(_ data: [String : Any]) {
        print(data)
        if data["userProfileId"]is Int
        {
            if (data["userProfileId"]as! Int == self.ToUserId && data["isOnline"]as! Bool == true)
            {
                self.navLastSeen.text = "Active Now"
                self.lastSeen = "Active Now"
            }
        }
        else
        {
            if (data["userProfileId"]as! String == String(self.ToUserId) && data["isOnline"]as! Bool == true)
            {
                self.navLastSeen.text = "Active Now"
                self.lastSeen = "Active Now"
            }
        }
    }
}

extension ChatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate,CropViewControllerDelegate
{
    /*************************
     Method Name:  imagePickerControllerDidCancel()
     Parameter: UIImagePickerController
     return type: nil
     Desc: This function is called if user taps cancel button.
     *************************/
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    /*************************
     Method Name:  imagePickerController()
     Parameter: UIImagePickerController, [String : Any]
     return type: nil
     Desc: This function is called when user selects image from gallery.
     *************************/
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
       if let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
       {
        
            dismiss(animated:true, completion: nil)
            let cropViewController = CropViewController(image: chosenImage)
            cropViewController.delegate = self
            present(cropViewController, animated: true, completion: nil)
        }
        else
       {
            if((info[UIImagePickerController.InfoKey.mediaType] as! String) == "public.movie")
            {
                print("video selected")
                let mediaURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL
                
                let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".mov")
                compressVideo(inputURL: mediaURL as! URL, outputURL: compressedURL) { (exportSession) in
                    guard let session = exportSession else {
                        return
                    }
                    
                    switch session.status {
                    case .unknown:
                        break
                    case .waiting:
                        break
                    case .exporting:
                        break
                    case .completed:
                        //                        guard let compressedData = NSData(contentsOf: compressedURL) else {
                        //                            return
                        //                        }
                        self.encodeVideo(videoURLNew: compressedURL as NSURL)
                        
                    case .failed:
                        break
                    case .cancelled:
                        break
                    }
                }
            }
        }
    }
    
    func deleteFile(filePath:NSURL) {
        guard FileManager.default.fileExists(atPath: filePath.path!) else {
            return
        }
        
        do {
            try FileManager.default.removeItem(atPath: filePath.path!)
        }catch{
            fatalError("Unable to delete file: \(error) : \(#function).")
        }
    }
    
    func encodeVideo(videoURLNew: NSURL)
    {
        let avAsset = AVURLAsset(url: videoURLNew as URL, options: nil)
        
        let exportSession = AVAssetExportSession(asset: avAsset, presetName: AVAssetExportPresetPassthrough)
        
        let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let myDocumentPath = NSURL(fileURLWithPath: documentsDirectory).appendingPathComponent("temp.mp4")?.absoluteString
        
        let documentsDirectory2 = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0] as NSURL
        
        let filePath = documentsDirectory2.appendingPathComponent("rendered-Video.mp4")
        deleteFile(filePath: filePath! as NSURL)
        
        if FileManager.default.fileExists(atPath: myDocumentPath!) {
            do {
                try FileManager.default.removeItem(atPath: myDocumentPath!)
            }
            catch let error {
                print(error)
            }
        }
        exportSession?.outputURL = filePath! as URL
        //set the output file format if you want to make it in other file format (ex .3gp)
        exportSession?.outputFileType = AVFileType.mp4
        exportSession?.shouldOptimizeForNetworkUse = true
        
        let start = CMTimeMakeWithSeconds(0.0, preferredTimescale: 0)
        let range = CMTimeRangeMake(start: start, duration: avAsset.duration)
        exportSession?.timeRange = range
        
        exportSession?.exportAsynchronously(completionHandler: {() -> Void in
            switch exportSession?.status {
            case .failed?:
                print("%@",exportSession?.error!)
            case .cancelled?:
                print("Export canceled")
            case .completed?:
                //Video conversion finished
                print("Successful!")
                print(exportSession?.outputURL!)
                self.videoUrl = (exportSession?.outputURL!)!
            default:
                break
            }
            do {
                
                let assets = AVURLAsset(url: self.videoUrl , options: nil)
                let imgGenerator = AVAssetImageGenerator(asset: assets)
                imgGenerator.appliesPreferredTrackTransform = true
                let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
                // !! check the error before proceeding
                let uiImage = UIImage(cgImage: cgImage)
                
                print(uiImage)
                DispatchQueue.main.async {
                    self.dismiss(animated: true) {
//                        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
//                        let filePath = "\(paths[0])/MyImageName.jpg"
                        
                        // Save image.
//                        UIImagePNGRepresentation(uiImage)?.writeToFile(filePath, atomically: true)
//                        let localFile = NSURL(fileURLWithPath: filePath)
//                        uiImage.pngData()?.write(to: localFile as URL)
                        
                        let fileURL = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false).appendingPathComponent("MyImageName.png")
                        
                        do {
                            try uiImage.pngData()!.write(to: fileURL, options: .atomic)
                        } catch {
                            print(error)
                        }
                        
                        self.uploadVideo(thumbImg: fileURL)
                    }
                }
                
            }
            catch let error {
                print(error.localizedDescription)
                DispatchQueue.main.async {
                    self.dismiss(animated:true, completion: nil)
                }
            }
        })
    }
    
    func uploadVideo(thumbImg : URL)
    {
        if self.videoUrl != nil
        {
            DispatchQueue.main.async {
                MBProgressHUD.showAdded(to: self.view, animated: true)
            }
            let boundary = "Boundary-\(UUID().uuidString)"
            let TokenValue = "Bearer " + MySingleton.shared.loginObject.access_token
            print(TokenValue)
            let headers: HTTPHeaders = ["Authorization": TokenValue,
                                        "content-type": "multipart/form-data; boundary= \(boundary)",
                                        "cache-control": "no-cache"
                                        ]
    
            var request = URLRequest(url: NSURL.init(string: "\(WebServices.baseURL)api/chat/UploadVideo")! as URL)
            request.allHTTPHeaderFields = headers
            request.httpMethod = HTTPMethod.post.rawValue
            request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
            Alamofire.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(self.videoUrl , withName: "video")
                multipartFormData.append(thumbImg, withName: "thumbnail")
//                multipartFormData.append(thumbImg.pngData()!, withName: "thumbnail")
                let dataToUserId = (String(self.ToUserId)).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
                print(dataToUserId)
                multipartFormData.append(dataToUserId,  withName: "ToUserProfileId")
            }, with: request, encodingCompletion: {
                encodingResult in
                switch encodingResult {
                case .success(let upload, _,_):
                    upload.responseJSON { response in
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
                        debugPrint("SUCCESS RESPONSE: \(response)")
                            self.videoUrl = nil
                        
                        if(response.result.value is [String:AnyObject])
                        {
                            if let JSON  = response.result.value as? [String:AnyObject]{
                                print(JSON)
                                if JSON["Status"] as! Bool == true
                                {
                                    DispatchQueue.main.async {
                                        self.view.endEditing(true)
                                        SocketIoManager.sharedInstance.sendDataToEvent(.sendmessage, data: ["fromUserProfileId": MySingleton.shared.loginObject.UserID, "toUserProfileId": self.ToUserId, "userId": MySingleton.shared.loginObject.AspNetUserId, "ParentMediaId": JSON["data"]as! String])
                                    }
                                }
                                else
                                {
                                    TrenderAlertVC.shared.presentAlertController(message: "Something went wrong while uploading audio.", completionHandler: nil)
                                }
                                
                            }
                        }
                        
                        }
                case .failure(let encodingError):
                        // hide progressbas here
                        DispatchQueue.main.async {
                        MBProgressHUD.hide(for: self.view, animated: true)
                        print("ERROR RESPONSE: \(encodingError)")
                        TrenderAlertVC.shared.presentAlertController(message: encodingError.localizedDescription, completionHandler: nil)
                        }
                    
                        }
                })
        }
    }
    
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) else {
            handler(nil)
            
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mov
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
        print(image)
//        self.view.endEditing(true)
//        SocketIoManager.sharedInstance.sendDataToEvent(.sendmessage, data: ["fromUserProfileId": MySingleton.shared.loginObject.UserID, "toUserProfileId": ToUserId, "userId": MySingleton.shared.loginObject.AspNetUserId, "message": txtMessage.text!])
        dismiss(animated: true) {
            self.uploadImage(image)
        }
        
    }
    
    func uploadImage(_ image : UIImage){
        var params = ["ToUserProfileId" : ToUserId] as [String : Any]
        var imageList = [AnyObject]()
        let imageData = ["FileExt":"jpg","contenttype":"image/jpg","ImageData":"data:image/jpg;base64,\(self.encodeToBase64String(image: image))"]
        imageList.append(imageData as AnyObject)
        params["ImageList"] = ["Images": imageList]
        callApi(requestData: params, url: "\(WebServices.baseURL)api/chat/UploadChatImage")
    }
    
    func encodeToBase64String(image: UIImage) -> String
    {
        let imageData = image.jpegData(compressionQuality: 0.5)
        let imag_str:String = imageData!.base64EncodedString(options:[])
        return imag_str
    }
    
    func callApi(requestData: Dictionary<String, Any>, url: String)
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        var urlStr: String = url
        
        urlStr = urlStr.replacingOccurrences(of: " ", with: "&")
        let request :NSMutableURLRequest = NSMutableURLRequest(url:NSURL(string:urlStr)! as URL)
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        if requestData.count != 0
        {
            do
            {
                let jsonData = try JSONSerialization.data(withJSONObject: requestData, options: JSONSerialization.WritingOptions.prettyPrinted)
                request.httpBody = jsonData
            }
            catch let error as NSError
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                }
            }
        }
        let TokenValue = "Bearer " + MySingleton.shared.loginObject.access_token
        print(TokenValue)
        request.setValue(TokenValue, forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        let task = URLSession.shared.dataTask(with: request as URLRequest){ data,response,error in
            
            if error != nil
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error!.localizedDescription, completionHandler: nil)
                }
                
            }
            do
            {
                if (response as? HTTPURLResponse) != nil
                {
                    //////////////////// JSON RESPONSE /////////////////
                    
                    let jsonString = String(data: data!, encoding: .utf8)
                    print(jsonString!)
                    
                    //////////////////// JSON RESPONSE /////////////////
                    
                    if data != nil
                    {
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        
                        print(jsonData)
                        
                        if(jsonData["Status"] as! Bool == true)
                        {
                            DispatchQueue.main.async {
                            self.view.endEditing(true)
                            SocketIoManager.sharedInstance.sendDataToEvent(.sendmessage, data: ["fromUserProfileId": MySingleton.shared.loginObject.UserID, "toUserProfileId": self.ToUserId, "userId": MySingleton.shared.loginObject.AspNetUserId, "ParentMediaId": jsonData["data"]as! String])
                            }
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                MBProgressHUD.hide(for: self.view, animated: true)
                                TrenderAlertVC.shared.presentAlertController(message: jsonData["Message"] as! String, completionHandler: nil)
                            }
                        }
                        
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true)
                            TrenderAlertVC.shared.presentAlertController(message: error!.localizedDescription, completionHandler: nil)
                        }
                    }
                    
                }
            }
            catch let error as NSError
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                }
            }
        }
        task.resume()
    }
}

