//
//  MyChatDeletedMessageTableViewCell.swift
//  TrenderAlert
//
//  Created by HPL on 30/07/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class MyChatDeletedMessageTableViewCell: UITableViewCell {

    @IBOutlet var viewMsg: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewMsg.makeCornerRadius(radius: 10.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
