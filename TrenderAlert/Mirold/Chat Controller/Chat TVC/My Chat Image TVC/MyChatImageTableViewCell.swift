//
//  MyChatImageTableViewCell.swift
//  TrenderAlert
//
//  Created by HPL on 03/05/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class MyChatImageTableViewCell: UITableViewCell {

    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var btnPlayVideo: UIButton!
    @IBOutlet var cnstrntImageWidth: NSLayoutConstraint!
   // @IBOutlet var imgMedia: FLAnimatedImageView!
    @IBOutlet var imgMedia: UIImageView!
    
    internal var aspectConstraint : NSLayoutConstraint? {
        didSet {
            if oldValue != nil {
                imgMedia.removeConstraint(oldValue!)
            }
            if aspectConstraint != nil {
                imgMedia.addConstraint(aspectConstraint!)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        aspectConstraint = nil
    }

    func setCustomRatio(width : CGFloat, height : CGFloat) {
        let aspect = width / height
        
        let constraint = NSLayoutConstraint(item: imgMedia, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: imgMedia, attribute: NSLayoutConstraint.Attribute.height, multiplier: (aspect), constant: 0.0)
        constraint.priority = UILayoutPriority(rawValue: 999)
        
        aspectConstraint = constraint
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
