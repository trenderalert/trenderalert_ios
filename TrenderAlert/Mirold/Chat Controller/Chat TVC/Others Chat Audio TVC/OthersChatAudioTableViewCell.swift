//
//  OthersChatTableViewCell.swift
//  TrenderAlert
//
//  Created by HPL on 14/05/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import AVFoundation

class OthersChatAudioTableViewCell: UITableViewCell {

    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var viewAudio: UIView!
    @IBOutlet var lblAudio: UILabel!
    @IBOutlet var sliderAudio: UISlider!
    @IBOutlet var btnPlayAudio: UIButton!
    @IBOutlet var imgProfilePic: UIImageView!
    var audioPlayer = AVPlayer()
    var recordingSession: AVAudioSession!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewAudio.makeCornerRadius(radius: 10.0)
        sliderAudio.setThumbImage(UIImage(named:"img_SilderThumb"), for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
