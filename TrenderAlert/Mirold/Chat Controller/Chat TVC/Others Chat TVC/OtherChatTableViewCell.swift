//
//  OtherChatTableViewCell.swift
//  TrenderAlert
//
//  Created by HPL on 15/04/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class OtherChatTableViewCell: UITableViewCell {

    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var lblOtherMsg: UILabel!
    @IBOutlet var viewOtherMsg: UIView!
    @IBOutlet var imgProfilePic: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewOtherMsg.makeCornerRadius(radius: 10.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
