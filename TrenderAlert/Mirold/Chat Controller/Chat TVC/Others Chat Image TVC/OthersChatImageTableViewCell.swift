//
//  OthersChatImageTableViewCell.swift
//  TrenderAlert
//
//  Created by HPL on 03/05/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class OthersChatImageTableViewCell: UITableViewCell {

    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var btnPlayVideo: UIButton!
    @IBOutlet var cnstrntImageWidth: NSLayoutConstraint!
//    @IBOutlet var imageMedia: FLAnimatedImageView!
    @IBOutlet var imageMedia: UIImageView!
    @IBOutlet var imgProfilePic: UIImageView!
    
    internal var aspectConstraint : NSLayoutConstraint? {
        didSet {
            if oldValue != nil {
                imageMedia.removeConstraint(oldValue!)
            }
            if aspectConstraint != nil {
                imageMedia.addConstraint(aspectConstraint!)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        aspectConstraint = nil
    }
    
    func setCustomRatio(width : CGFloat, height : CGFloat) {
        let aspect = width / height

        let constraint = NSLayoutConstraint(item: imageMedia, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: imageMedia, attribute: NSLayoutConstraint.Attribute.height, multiplier: (aspect), constant: 0.0)
        constraint.priority = UILayoutPriority(rawValue: 999)

        aspectConstraint = constraint

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
