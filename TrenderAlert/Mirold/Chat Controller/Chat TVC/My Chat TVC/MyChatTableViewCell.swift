//
//  MyChatTableViewCell.swift
//  TrenderAlert
//
//  Created by HPL on 15/04/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class MyChatTableViewCell: UITableViewCell {

    @IBOutlet var lblDate: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var viewMyMsg: UIView!
    @IBOutlet var lblMyMsg: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewMyMsg.makeCornerRadius(radius: 10.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
