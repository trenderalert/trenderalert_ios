//
//  TagTrendersPopUpViewController.swift
//  TrenderAlert
//
//  Created by HPL on 05/02/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

protocol TagTrenderDelegate {
    func controller(arraySelectedTrender: [TagTrenderModel])
}

class TagTrendersPopUpViewController: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet var viewCorner: UIView!
    @IBOutlet var lblSelectall: UILabel!
    @IBOutlet var btnSave: UIButton!
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var tableTagTrenders: UITableView!
    @IBOutlet var btnSelectAllCheck: UIButton!

    var arrayTagTrendersToSelect = [TagTrenderModel]()
    var delegate : TagTrenderDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        tableTagTrenders.register(UINib(nibName: "TagTrendersTableViewCell", bundle: nil), forCellWithReuseIdentifier: "TagTrendersTableViewCell")
        self.tableTagTrenders.register(UINib.init(nibName: "TagTrendersTableViewCell", bundle: nil), forCellReuseIdentifier: "TagTrendersTableViewCell")
        tableTagTrenders.tableFooterView = UIView()
        self.lblTitle.text = MySingleton.shared.selectedLangData.tag_trenders
        self.lblSelectall.text = MySingleton.shared.selectedLangData.select_all
        self.btnSave.setTitle(MySingleton.shared.selectedLangData.save, for: .normal)
        self.btnCancel.setTitle(MySingleton.shared.selectedLangData.cancel, for: .normal)
        self.lblNoData.text = MySingleton.shared.selectedLangData.add_friends_to_tag
    }
    
    override func viewDidLayoutSubviews() {
        btnSave.makeCornerRadius(radius: 5)
        btnCancel.makeCornerRadius(radius: 5)
        viewCorner.makeCornerRadius(radius: 10)
    }
    
    @IBAction func btnCancelAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnSaveAction(_ sender: Any) {
        self.dismiss(animated: true) {
            self.delegate?.controller(arraySelectedTrender: self.arrayTagTrendersToSelect)
        }
    }
    
    @IBAction func btnSelectAllAction(_ sender: Any) {
        if arrayTagTrendersToSelect.count == 0
        {
            return
        }
        if btnSelectAllCheck.currentBackgroundImage == #imageLiteral(resourceName: "radioOffYellow")
        {
            for i in 0...arrayTagTrendersToSelect.count-1
            {
                arrayTagTrendersToSelect[i].isSelected = true
            }
            btnSelectAllCheck.setBackgroundImage(#imageLiteral(resourceName: "check mark"), for: .normal)
            tableTagTrenders.reloadData()
        }
        else
        {
            for i in 0...arrayTagTrendersToSelect.count-1
            {
                arrayTagTrendersToSelect[i].isSelected = false
            }
            btnSelectAllCheck.setBackgroundImage(#imageLiteral(resourceName: "radioOffYellow"), for: .normal)
            tableTagTrenders.reloadData()
        }
    }
    
    
}

extension TagTrendersPopUpViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrayTagTrendersToSelect.count == 0
        {
            btnSelectAllCheck.isHidden = true
            lblSelectall.isHidden = true
            lblNoData.isHidden = false
        }
        else
        {
            lblNoData.isHidden = true
        }
        return arrayTagTrendersToSelect.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "TagTrendersTableViewCell", for: indexPath)as! TagTrendersTableViewCell
        cell.lblFullName.text = "\(arrayTagTrendersToSelect[indexPath.row].FirstName) \(arrayTagTrendersToSelect[indexPath.row].LastName)"
        if arrayTagTrendersToSelect[indexPath.row].isSelected
        {
            cell.btnCheck.setBackgroundImage(#imageLiteral(resourceName: "check mark"), for: .normal)
        }
        else
        {
            cell.btnCheck.setBackgroundImage(#imageLiteral(resourceName: "radioOffYellow"), for: .normal)
        }
        
        cell.btnCheck.tag = indexPath.row
        cell.btnCheck.addTarget(self, action: #selector(btnCheckAction(_:)), for: .touchUpInside)
        return cell
    }
    
    @objc func btnCheckAction(_ sender:UIButton)
    {
        let cell = tableTagTrenders.cellForRow(at: IndexPath(row: sender.tag, section: 0))as? TagTrendersTableViewCell
        if cell?.btnCheck.currentBackgroundImage == #imageLiteral(resourceName: "radioOffYellow")
        {
            cell?.btnCheck.setBackgroundImage(#imageLiteral(resourceName: "check mark"), for: .normal)
            arrayTagTrendersToSelect[sender.tag].isSelected = true
        }
        else
        {
            cell?.btnCheck.setBackgroundImage(#imageLiteral(resourceName: "radioOffYellow"), for: .normal)
            arrayTagTrendersToSelect[sender.tag].isSelected = false
            btnSelectAllCheck.setBackgroundImage(#imageLiteral(resourceName: "radioOffYellow"), for: .normal)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }
    
}

