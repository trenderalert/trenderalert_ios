//
//  TagTrendersTableViewCell.swift
//  TrenderAlert
//
//  Created by OSP LABS on 05/02/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class TagTrendersTableViewCell: UITableViewCell {

    @IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var imgProfilePic: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
