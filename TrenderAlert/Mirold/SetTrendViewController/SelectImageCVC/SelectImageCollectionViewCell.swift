//
//  SelectImageCollectionViewCell.swift
//  TrenderAlert
//
//  Created by HPL on 28/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class SelectImageCollectionViewCell: UICollectionViewCell {

    var RemoveCell : (() -> Void)?;
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet var btnClose: UIButton!
    @IBOutlet var image: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func btnCloseAction(_ sender: Any) {
        if RemoveCell != nil{
            RemoveCell!();
        }
    }
}
