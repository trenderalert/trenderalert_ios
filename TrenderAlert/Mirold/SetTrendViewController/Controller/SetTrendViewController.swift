//
//  SetTrendViewController.swift
//  TrenderAlert
//
//  Created by OSP LABS on 03/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import AVFoundation
import MobileCoreServices
import DropDown
import AFViewShaker
import MBProgressHUD
import BSImagePicker
import Photos
import Alamofire
import CropViewController
import GooglePlaces
import GrowingTextView

protocol BuisnessAccountDelegate {
    func BuisnessAccount(reload: String)
}

class SetTrendViewController: UIViewController {
    
    @IBOutlet weak var lblDy: UILabel!
    @IBOutlet weak var lblHr: UILabel!
    @IBOutlet weak var lblExpireIn: UILabel!
    @IBOutlet weak var lblAddNew: UILabel!
    @IBOutlet weak var viewSeparater: UIView!
    @IBOutlet weak var viewExpiry: UIView!
    @IBOutlet var btnGetPremium: UIButton!
    @IBOutlet var switchAudio: UISwitch!
    @IBOutlet var switchText: UISwitch!
//    @IBOutlet var txtDescription: UITextView!
    
    @IBOutlet weak var btnMenu: UIBarButtonItem!
    @IBOutlet var txtDescription: GrowingTextView!
    @IBOutlet var viewEmail: FormFields!
    @IBOutlet var viewLocation: FormFields!
    @IBOutlet var viewTagTrender: FormFields!
    var str_latitude = String()
    var str_longitude = String()
    @IBOutlet var lblFolder: UILabel!
    @IBOutlet var lblPublicPrivate: UILabel!
    @IBOutlet var cnstrntAudioIconWidth: NSLayoutConstraint!
    @IBOutlet var viewAddNew: UIView!
    @IBOutlet var lblSliderMaximumValue: UILabel!
    @IBOutlet var lblSliderMinimumValue: UILabel!
    @IBOutlet var imgDaysSwitch: UIImageView!
    @IBOutlet var imgHoursSwitch: UIImageView!
    @IBOutlet var lblSliderValue: UILabel!
    @IBOutlet var viewTitle: FormFields!
    @IBOutlet var slider: UISlider!
    @IBOutlet var viewDescription: UIView!
//    @IBOutlet var cnstrntCollectionHeight: NSLayoutConstraint!
    @IBOutlet var collectionImage: UICollectionView!
    @IBOutlet var viewFolder: UIView!
    @IBOutlet var viewPublic: UIView!
    var videoURL = [URL]()
    @IBOutlet var btnPlus: UIButton!
    @IBOutlet var imgCalendarIcon: UIImageView!
    @IBOutlet var cnstrntImgCalendarWeight: NSLayoutConstraint!
    var photoArray = [PHAsset]()
    let picker = UIImagePickerController()
    var imgCollection = [UIImage]()
    var imgCollectionMediaType = [String]()
    var isPremiumUser = false
    var arrayPublicPrivate = [MySingleton.shared.selectedLangData.Public,MySingleton.shared.selectedLangData.Private,MySingleton.shared.selectedLangData.friends]
    var arrayFolders = [MySingleton.shared.selectedLangData.create_new_folder]
    var publicPrivateDropdown = DropDown()
    var locationDropdown = DropDown()
    var folderDropdown = DropDown()
    var arrayTagTrender = [TagTrenderModel]()
    var location_arr = [String]()
    var statusData = [Any]()
    var arraySelectedTagTrendersId = [AnyObject]()
    var arraySelectedTagFriendsId = [AnyObject]()
    var mediaTypeId = 0
    private var croppingStyle = CropViewCroppingStyle.default
    private var croppedRect = CGRect.zero
    private var croppedAngle = 0
    var cropIndexPath = IndexPath()
    var locationManager = CLLocationManager()
    var arrayOnlyImagesSelected = [UIImage]()
    var isEdit = Bool()
    var contestId = 0
    var isForContest = false
    var isBackButtonReq = false
    var isGroup = Bool()
    var groupParams:GroupDetail?
    var editTrendParam = TrendList()
    var imageIdArray = [Int]()
    var editId = [Int]()
    var editFriendId = [Int]()
    var trendTypeId = 1
    var selectedFriendsArray = [AnyObject]()
    var isBusinessAccount = 0
    var city = String()
    var country = String()
    var delegate : BuisnessAccountDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        picker.delegate = self
        collectionImage.register(UINib(nibName: "SelectImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SelectImageCollectionViewCell")
        initialSettings()
        getUserInfo()
        viewTitle.img_IconPic.removeFromSuperview()
        viewTitle.lbl_PlaceHolderTxt.removeFromSuperview()
        viewTagTrender.lbl_PlaceHolderTxt.removeFromSuperview()
        viewEmail.lbl_PlaceHolderTxt.removeFromSuperview()
        viewLocation.lbl_PlaceHolderTxt.removeFromSuperview()
    }
    
    override func viewDidLayoutSubviews() {
        btnPlus.makeCornerRadius(radius: 25.0)
        viewPublic.makeCornerRadius(radius: 3.0)
        viewPublic.giveBorderColor(color: UIColor.init(red: 179.0/255.0, green: 179.0/255.0, blue: 179.0/255.0, alpha: 1.0))
//        viewPublic.layer.borderWidth = 2.0
        viewAddNew.makeCornerRadius(radius: 10.0)
        viewAddNew.giveBorderColor(color: UIColor.init(red: 179.0/255.0, green: 179.0/255.0, blue: 179.0/255.0, alpha: 1.0))
        viewFolder.makeCornerRadius(radius: 3.0)
        viewFolder.giveBorderColor(color: UIColor.init(red: 179.0/255.0, green: 179.0/255.0, blue: 179.0/255.0, alpha: 1.0))
        btnGetPremium.makeCornerRadius(radius: 3.0)
        btnGetPremium.giveBorderColor(color: UIColor.init(red: 179.0/255.0, green: 179.0/255.0, blue: 179.0/255.0, alpha: 1.0))
        viewDescription.makeCornerRadius(radius: 5.0)
        viewDescription.giveBorderColor(color: UIColor.init(red: 179.0/255.0, green: 179.0/255.0, blue: 179.0/255.0, alpha: 1.0))
        slider.setThumbImage(#imageLiteral(resourceName: "img_SilderThumb"), for: .normal)
        slider.setThumbImage(#imageLiteral(resourceName: "img_SilderThumb"), for: .highlighted)
        cnstrntImgCalendarWeight.constant = viewEmail.img_IconPic.frame.size.width
        cnstrntAudioIconWidth.constant = viewEmail.img_IconPic.frame.size.width
        viewTitle.viewSeperator.isHidden = true
        viewTitle.txtFld_UserValue.autocapitalizationType = UITextAutocapitalizationType.sentences
        viewTagTrender.viewSeperator.isHidden = true
        viewEmail.viewSeperator.isHidden = true
        viewLocation.viewSeperator.isHidden = true
//        switchAudio.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
//        switchText.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func editTrendDetails()
    {
        DispatchQueue.main.async{
        print(self.editTrendParam)
            if self.editTrendParam.TrendLocation != nil {
                if self.editTrendParam.TrendLocation != ""{
                   
        self.str_latitude = String(self.editTrendParam.TrendLatitude)
        self.str_longitude = String(self.editTrendParam.TrendLongitude)
            GetAddress().getAddressFromLocationSetTrendCity(CLLocation(latitude: self.editTrendParam.TrendLatitude, longitude: self.editTrendParam.TrendLongitude)) { (address) in
                self.city = address
            }
            GetAddress().getAddressFromLocationSetTrendCountry(CLLocation(latitude: self.editTrendParam.TrendLatitude, longitude: self.editTrendParam.TrendLongitude)) { (address) in
                self.country = address
            }
        self.viewLocation.txtFld_UserValue.text = self.editTrendParam.TrendLocation
                }
                else{
                self.locationManager.delegate = self
                if CLLocationManager.authorizationStatus() == .authorizedWhenInUse
                {
                    self.locationManager.startUpdatingLocation()
                }
                else
                {
                    self.locationManager.requestWhenInUseAuthorization()
                    
                }
            }
            }
            else
            {
                self.locationManager.delegate = self
                if CLLocationManager.authorizationStatus() == .authorizedWhenInUse
                {
                    self.locationManager.startUpdatingLocation()
                }
                else
                {
                    self.locationManager.requestWhenInUseAuthorization()
                    
                }
            }
        if self.editTrendParam.TaggedTrenders != nil
        {
            if self.editTrendParam.TaggedTrenders!.count != 0
            {
                self.arraySelectedTagTrendersId = [AnyObject]()
                for i in 0...self.editTrendParam.TaggedTrenders!.count-1
                {
                    let dict = ["TrenderId": self.editTrendParam.TaggedTrenders![i].UserFollowerId]
                    self.arraySelectedTagTrendersId.append(dict as AnyObject)
                    self.editId.append(self.editTrendParam.TaggedTrenders![i].UserFollowerId)
                    
                }
                self.viewTagTrender.txtFld_UserValue.text = "\(self.arraySelectedTagTrendersId.count) \(MySingleton.shared.selectedLangData.tag_trenders)"
            }
        }
        
        if self.editTrendParam.TrendTypeId == 3
        {
            if self.editTrendParam.TaggedFriends!.count != 0
            {
                self.lblPublicPrivate.text = MySingleton.shared.selectedLangData.friends
                self.trendTypeId = 3
                self.arraySelectedTagFriendsId = [AnyObject]()
                for i in 0...self.editTrendParam.TaggedFriends!.count-1
                {
//                    let dict = ["FriendId": self.editTrendParam.TaggedFriends![i].UserProfileID]
//                    arraySelectedTagFriendsId.append(dict as AnyObject)
                    let dict = ["FriendId": self.editTrendParam.TaggedFriends![i].UserProfileID]
                    self.selectedFriendsArray.append(dict as AnyObject)
                    self.editFriendId.append(self.editTrendParam.TaggedFriends![i].UserProfileID)
                }
             //   viewTagTrender.txtFld_UserValue.text = "\(arraySelectedTagTrendersId.count) Trenders Tagged"
            }
            else
            {
                if self.editTrendParam.TrendTypeId == 1
                {
                    self.lblPublicPrivate.text = MySingleton.shared.selectedLangData.Public
                    self.trendTypeId = 1
                }
                else
                {
                    self.lblPublicPrivate.text = MySingleton.shared.selectedLangData.Private
                    self.trendTypeId = 2
                }
            }
        }
        else
        {
            if self.editTrendParam.TrendTypeId == 1
            {
                self.lblPublicPrivate.text = MySingleton.shared.selectedLangData.Public
                self.trendTypeId = 1
            }
            else
            {
                self.lblPublicPrivate.text = MySingleton.shared.selectedLangData.Private
                self.trendTypeId = 2
            }
        }
        
        if self.editTrendParam.FolderPath != nil
        {
          self.lblFolder.text = self.editTrendParam.FolderPath
        }
       
       
        self.viewTitle.txtFld_UserValue.text = self.editTrendParam.Title
        self.txtDescription.text = self.editTrendParam.Description
        if self.editTrendParam.ExpiryDuration <= 72
        {
//            self.slider.maximumValue = Float(self.editTrendParam.ExpiryDuration)
            self.lblSliderValue.text = "\(self.editTrendParam.ExpiryDuration) \(MySingleton.shared.selectedLangData.hours)"
            self.lblSliderMinimumValue.text = "1 \(MySingleton.shared.selectedLangData.hour)"
            self.lblSliderMaximumValue.text = "72 \(MySingleton.shared.selectedLangData.hours)"
            self.slider.maximumValue = 72
            self.slider.value = Float(self.editTrendParam.ExpiryDuration)
            self.slider.minimumValue = 1
            self.imgHoursSwitch.image = #imageLiteral(resourceName: "radio_On")
            self.imgDaysSwitch.image = #imageLiteral(resourceName: "radio_Off")
        }
        else
        {
            let expiry = self.editTrendParam.ExpiryDuration / 24
            
            self.lblSliderValue.text = "\(expiry) \(MySingleton.shared.selectedLangData.days)"
            self.lblSliderMinimumValue.text = "4 \(MySingleton.shared.selectedLangData.days)"
            self.lblSliderMaximumValue.text = "90 \(MySingleton.shared.selectedLangData.days)"
            self.slider.maximumValue = 90
            self.slider.minimumValue = 4
            self.slider.value = Float(expiry)
            self.imgHoursSwitch.image = #imageLiteral(resourceName: "radio_Off")
            self.imgDaysSwitch.image = #imageLiteral(resourceName: "radio_On")
        }
        self.viewEmail.txtFld_UserValue.text = self.editTrendParam.ContactEmail
        self.viewLocation.txtFld_UserValue.text = self.editTrendParam.TrendLocation
//        self.viewTagTrender.txtFld_UserValue.text = "\(self.arrayTagTrender.count) Trenders Taged"
        if self.editTrendParam.ImageList?.count != 0
        {
            for i in 0...(self.editTrendParam.ImageList?.count)!-1
            {
                let catPictureURL = URL(string: self.editTrendParam.ImageList![i].TrendImage)!
                let session = URLSession(configuration: .default)
                
                // Define a download task. The download task will download the contents of the URL as a Data object and then you can do what you wish with that data.
                let downloadPicTask = session.dataTask(with: catPictureURL) { (data, response, error) in
                    // The download has finished.
                    if let e = error {
                        print("Error downloading cat picture: \(e)")
                    } else {
                        // No errors found.
                        // It would be weird if we didn't have a response, so check for that too.
                        if let res = response as? HTTPURLResponse {
                            print("Downloaded cat picture with response code \(res.statusCode)")
                            if let imageData = data {
                                // Finally convert that Data into an image and do what you wish with it.
                                let image = UIImage(data: imageData)
                                
                                
                                // Do something with your image.
                            } else {
                                print("Couldn't get image: Image is nil")
                            }
                        } else {
                            print("Couldn't get response code for some reason")
                        }
                    }
                }
                
                
                
                
                
                let image = UIImageView()
                
                image.sd_setImage(with: URL(string: self.editTrendParam.ImageList![i].TrendImage), placeholderImage: #imageLiteral(resourceName: "logo_navigation"))
             //   mediaTypeId = 1
                if self.editTrendParam.ImageList![i].Media == "IMAGE"
                {
                self.imageIdArray.append(self.editTrendParam.ImageList![i].TrendImageId!)
                    self.imgCollection.append(image.image!)
                    self.imgCollectionMediaType.append("IMAGE")
                    self.arrayOnlyImagesSelected.append(image.image!)
                }
                else if self.editTrendParam.ImageList![i].Media == "VIDEO"
                {
                    self.imgCollection.append(image.image!)
                    self.imgCollectionMediaType.append("VIDEO")
                    self.arrayOnlyImagesSelected.append(image.image!)
                }
                else
                {
                self.imageIdArray.append(self.editTrendParam.ImageList![i].TrendImageId ?? 0)
                    self.imgCollection.append(image.image!)
                //    imgCollectionMediaType.append("STATUS")
                }
            }
            self.collectionImage.delegate = self
            self.collectionImage.dataSource = self
            self.collectionImage.reloadData()
        }
      }
    }
    
    func getUserInfo(){
        WebServices().callUserService(service: UserServices.getUserInfo, urlParameter: "", parameters: ["UserID":MySingleton.shared.loginObject.UserID as AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
                    print(serviceResponse)
            DispatchQueue.main.async {
                DispatchQueue.main.async {
           
            if (((serviceResponse["data"]as! [AnyObject])[0]as! [String:Any])["Folders"]as! [AnyObject]).count != 0
            {
                self.arrayFolders = [MySingleton.shared.selectedLangData.create_new_folder]
//                self.arrayFolders = ((serviceResponse["data"]as! [AnyObject])[0]as! [String:Any])["Folders"] as! [String]
                for i in 0...(((serviceResponse["data"]as! [AnyObject])[0]as! [String:Any])["Folders"] as! [AnyObject]).count-1
                {
                    self.arrayFolders.append(((((serviceResponse["data"]as! [AnyObject])[0]as! [String:Any])["Folders"] as! [AnyObject])[i]as! [String:Any])["FolderName"]as! String)

                }
//                self.viewEmail.txtFld_UserValue.text = ((serviceResponse["data"]as! [AnyObject])[0]as! [String:Any])["EmailId"] as? String
            }
            self.viewEmail.txtFld_UserValue.text = ((serviceResponse["data"]as! [AnyObject])[0]as! [String:Any])["EmailId"] as? String
            }
            }
            self.getTrenderData()
            
        })
    }
    
    @IBAction func didTapMenu(_ sender: UIBarButtonItem) {
        if self.btnMenu.title == "BACK"
        {
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
            if self.menuContainerViewController != nil {
                self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
            }
        }
    }
    
    func getTrenderData(){
        WebServices().callUserService(service: UserServices.getFollowerList, urlParameter: "", parameters: nil, isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
            if serviceResponse["Status"]as! Bool
            {
                if serviceResponse["Data"]is [AnyObject]
                {
                    DispatchQueue.main.async {
                        if (serviceResponse["Data"]as! [AnyObject]).count != 0{
                    var items = TagTrenderModel()
                    for i in 0...(serviceResponse["Data"]as! [AnyObject]).count-1
                    {
                        items.id = ((serviceResponse["Data"]as! [AnyObject])[i]as! [String:AnyObject])["UserFollowerId"]as! Int
                        items.FirstName = ((serviceResponse["Data"]as! [AnyObject])[i]as! [String:AnyObject])["UserProfileFirstName"]as! String
                        items.LastName = ((serviceResponse["Data"]as! [AnyObject])[i]as! [String:AnyObject])["UserProfileLastName"]as! String
                        items.ProfilePic = ((serviceResponse["Data"]as! [AnyObject])[i]as! [String:AnyObject])["FollowerProfileImageUrl"]as! String
                        if self.editId.count != 0
                        {
                            if self.editId.contains(((serviceResponse["Data"]as! [AnyObject])[i]as! [String:AnyObject])["UserFollowerId"]as! Int)
                            {
                                items.isSelected = true
                            }
                            else
                            {
                                items.isSelected = false
                            }
                        }
                        else
                        {
                            items.isSelected = false
                        }
                        self.arrayTagTrender.append(items)
                    }
                        }
                        else
                        {
                            
                        }
                }
            }
            }
        })
    }
    
    func initialSettings()
    {
        self.lblExpireIn.text = MySingleton.shared.selectedLangData.expires_in
//        let attributedStringColor = [NSAttributedString.Key.foregroundColor : UIColor.black];
//        let attributedString = NSMutableAttributedString(string: MySingleton.shared.selectedLangData.expires_in, attributes: attributedStringColor)
//        let attrs1 = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 12), NSAttributedString.Key.foregroundColor : UIColor.black]
//        let attributedString2 = NSMutableAttributedString(string: MySingleton.shared.selectedLangData.expires_in, attributes: attrs1)
//        attributedString.append(attributedString2)
//        self.lblExpireIn.attributedText = attributedString
        
        self.lblHr.text = MySingleton.shared.selectedLangData.hours
        self.lblDy.text = MySingleton.shared.selectedLangData.days
        self.lblAddNew.text = MySingleton.shared.selectedLangData.Add_new
        self.txtDescription.placeholder = MySingleton.shared.selectedLangData.Description
        self.viewTitle.txtFld_UserValue.placeholder = MySingleton.shared.selectedLangData.title
        self.viewTagTrender.txtFld_UserValue.text = MySingleton.shared.selectedLangData.tag_trenders
        self.lblSliderMinimumValue.text = "1 \(MySingleton.shared.selectedLangData.hour)"
        self.lblSliderMaximumValue.text = "72 \(MySingleton.shared.selectedLangData.hours)"
        if MySingleton.shared.loginObject.IsPremiumUser == "True"{
            isPremiumUser = true
        }
        else
        {
            isPremiumUser = false
        }
        
        if self.isForContest == true || (isGroup) || (isEdit) || (isBusinessAccount == 1)
        {
            self.btnMenu.image = nil
            self.btnMenu.title = "BACK"
            self.hidesBottomBarWhenPushed = true
            locationManager.delegate = self
            if CLLocationManager.authorizationStatus() == .authorizedWhenInUse
            {
                locationManager.startUpdatingLocation()
            }
            else
            {
                locationManager.requestWhenInUseAuthorization()
                
            }
            if isEdit
            {
               editTrendDetails()
            }
            else
            {
                self.imgHoursSwitch.image = #imageLiteral(resourceName: "radio_On")
                self.imgDaysSwitch.image = #imageLiteral(resourceName: "radio_Off")
            }
        }
        else
        {
            self.lblPublicPrivate.text = MySingleton.shared.selectedLangData.Public
            self.lblFolder.text = MySingleton.shared.selectedLangData.folder
            self.lblSliderValue.text = "72 \(MySingleton.shared.selectedLangData.hours)"
//            self.lblFolder.text = MySingleton.shared.selectedLangData.Public
            locationManager.delegate = self
            if CLLocationManager.authorizationStatus() == .authorizedWhenInUse
            {
                locationManager.startUpdatingLocation()
            }
            else
            {
                locationManager.requestWhenInUseAuthorization()
                
            }
            self.hidesBottomBarWhenPushed = false
        }
        
        if self.isForContest == true
        {
            self.viewPublic.removeFromSuperview()
            self.viewFolder.removeFromSuperview()
            self.viewExpiry.removeFromSuperview()
            self.viewSeparater.isHidden = true
        }
        else
        {
        //public private dropdown
            self.viewSeparater.isHidden = false
        publicPrivateDropdown.anchorView = viewPublic
        publicPrivateDropdown.bottomOffset = CGPoint(x: 0, y:(publicPrivateDropdown.anchorView?.plainView.bounds.height)!)
        publicPrivateDropdown.topOffset = CGPoint(x: 0, y:-(publicPrivateDropdown.anchorView?.plainView.bounds.height)!)
        dropdownPublicPrivateSelection()
        
        // folder dropdown
        folderDropdown.anchorView = viewFolder
        folderDropdown.bottomOffset = CGPoint(x: 0, y:(publicPrivateDropdown.anchorView?.plainView.bounds.height)!)
        folderDropdown.topOffset = CGPoint(x: 0, y:-(folderDropdown.anchorView?.plainView.bounds.height)!)
        dropdownfolderSelection()
        }
        // Location dropdown
        locationDropdown.anchorView = viewLocation
        locationDropdown.bottomOffset = CGPoint(x: 0, y:(locationDropdown.anchorView?.plainView.bounds.height)!)
        locationDropdown.topOffset = CGPoint(x: 0, y:-(locationDropdown.anchorView?.plainView.bounds.height)!)
        dropdownLocationSelection()
        
        self.viewLocation.txtFld_UserValue.addTarget(self, action: #selector(self.textFieldTextChanged(_:)), for: .editingChanged)
        self.viewLocation.txtFld_UserValue.delegate = self
        
        Utilities.setNavigationBar(viewController: self)
        txtDescription.delegate = self
//        txtDescription.text = "Description..."
//        txtDescription.textColor = UIColor.lightGray
        
       
    }
    
    func dropdownLocationSelection()
    {
        locationDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.viewLocation.txtFld_UserValue.text! = item
            GetAddress().getLocationFromAddressString(addressStr: self.viewLocation.txtFld_UserValue.text!) { (clLocationCoordinate2D,zipcode)  in
                self.str_latitude = String(clLocationCoordinate2D.latitude)
                self.str_longitude = String(clLocationCoordinate2D.longitude)
                
                GetAddress().getAddressFromLocationSetTrendCity(CLLocation(latitude: Double((self.str_latitude as NSString).doubleValue), longitude: Double((self.str_longitude as NSString).doubleValue)) ) { (address) in
                    self.city = address
                    print(self.city)
                }
                
                GetAddress().getAddressFromLocationSetTrendCountry(CLLocation(latitude: Double((self.str_latitude as NSString).doubleValue), longitude: Double((self.str_longitude as NSString).doubleValue))) { (address) in
                    self.country = address
                    print(self.country)
                }
            }
            

        }
    }
    
    func dropdownPublicPrivateSelection()
    {
        publicPrivateDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            print(self.slider.value)
            if item != MySingleton.shared.selectedLangData.friends
            {
                self.lblPublicPrivate.text! = item
            }
            
            if item == MySingleton.shared.selectedLangData.Public
            {
                self.trendTypeId = 1
            }
            else if item == MySingleton.shared.selectedLangData.Private
            {
                self.trendTypeId = 2
            }
            else
            {
                self.trendTypeId = 3
                let destination = self.storyboard!.instantiateViewController(withIdentifier: "FriendListViewController") as! FriendListViewController
                destination.delegate = self
                destination.editId = self.editFriendId
                let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height)
                self.navigationController?.present(pop, animated: true, completion: nil)
            }
        }
    }
    
    func dropdownfolderSelection()
    {
        folderDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            if index == 0
            {
                let storyboard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
                let nextViewController = storyboard.instantiateViewController(withIdentifier: "NewFolderPoupVC") as! NewFolderPoupVC
                nextViewController.delegate = self
                let pop = PopUpViewController(nextViewController,withHeight: self.view.frame.size.height)
                self.navigationController?.present(pop, animated: true, completion: nil)
//                self.present(nextViewController, animated: true, completion: nil)
//                self.navigationController?.pushViewController(nextViewController, animated: true)
            }
            else
            {
                self.lblFolder.text! = item
            }
           
        }
    }
    
    @IBAction func viewFolderTapAction(_ sender: Any) {
        folderDropdown.dataSource = arrayFolders
        folderDropdown.cellHeight = 40.0
        folderDropdown.show()
    }
    
    @IBAction func viewPublicTapAction(_ sender: Any) {
        publicPrivateDropdown.dataSource = arrayPublicPrivate
        publicPrivateDropdown.cellHeight = 40.0
        publicPrivateDropdown.show()
    }
    
    @IBAction func btnTagTrenderAction(_ sender: Any) {
        if arrayTagTrender.count != 0
        {
            let destination = self.storyboard?.instantiateViewController(withIdentifier: "TagTrendersPopUpViewController") as! TagTrendersPopUpViewController
            destination.arrayTagTrendersToSelect = arrayTagTrender
            destination.delegate = self
//        let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height * 0.75)
            let screenHt = self.view.frame.size.height * 0.75
            let popupHt = CGFloat(172+(40*arrayTagTrender.count))
            var height = CGFloat()
            if screenHt > popupHt {
                height = popupHt
            }
            else
            {
                height = screenHt
            }
            let pop = PopUpViewController(destination,withHeight: height)
            self.navigationController?.present(pop, animated: true, completion: nil)
        }
        else
        {
            TrenderAlertVC.shared.presentAlertController(message: "No Trenders Found", completionHandler: nil)
        }
    }
    
    
    @IBAction func btnHoursAction(_ sender: Any) {
        if imgHoursSwitch.image == #imageLiteral(resourceName: "radio_Off"){
            slider.maximumValue = 72
            slider.minimumValue = 1
            slider.value = 36
            lblSliderMinimumValue.text = "1 \(MySingleton.shared.selectedLangData.hour)"
            lblSliderMaximumValue.text = "72 \(MySingleton.shared.selectedLangData.hours)"
            lblSliderValue.text = "72 \(MySingleton.shared.selectedLangData.hours)"
            imgHoursSwitch.image = #imageLiteral(resourceName: "radio_On")
            imgDaysSwitch.image = #imageLiteral(resourceName: "radio_Off")
        }
        
    }
    
    @IBAction func btnDaysAction(_ sender: Any) {
        if self.isPremiumUser{
            if imgDaysSwitch.image == #imageLiteral(resourceName: "radio_Off"){
                slider.maximumValue = 90
                slider.minimumValue = 4
                slider.value = 15
                lblSliderMinimumValue.text = "4 \(MySingleton.shared.selectedLangData.days)"
                lblSliderMaximumValue.text = "90 \(MySingleton.shared.selectedLangData.days)"
                lblSliderValue.text = "15 \(MySingleton.shared.selectedLangData.days)"
                imgDaysSwitch.image = #imageLiteral(resourceName: "radio_On")
                imgHoursSwitch.image = #imageLiteral(resourceName: "radio_Off")
            }
        }
        else {
            DispatchQueue.main.async {
                TrenderAlertVC.shared.presentAlertController(message: "To extend expiry time up to 90 days get premium package!", completionHandler: {
                    MySingleton.shared.isFirstTimeSideMenu = false
                    let storyboard = UIStoryboard(name: "NotificationSB", bundle: nil)
                    let paymentVC = storyboard.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
                    paymentVC.isBackEnabled = true
//                    self.present(paymentVC, animated: true, completion: nil)
                    self.navigationController?.pushViewController(paymentVC, animated: true)
//                    let tabBar = self.menuContainerViewController.centerViewController as! UITabBarController
//                    let navigationController = tabBar.selectedViewController as! UINavigationController
//                    navigationController.viewControllers = [paymentVC]
                })
            }
        }
        
    }
    
    func validationForSetTrend() -> [UIView]
    {
        var viewsToShake = [UIView]()
        if viewTitle.txtFld_UserValue.text!.isEmpty
        {
            viewsToShake.append(viewTitle)
        }
        if txtDescription.text.isEmpty
        {
            viewsToShake.append(viewDescription)
        }
        if viewLocation.txtFld_UserValue.text!.isEmpty
        {
            viewsToShake.append(viewLocation)
        }
        return viewsToShake
    }
    
    @IBAction func btnSaveAction(_ sender: Any) {

        if self.selectedFriendsArray.count == 0
        {
//           self.lblPublicPrivate.text = "Public"
            self.trendTypeId = 1
        }
        
            if imgCollection.count == 0
            {
                DispatchQueue.main.async {
                    TrenderAlertVC.shared.presentAlertController(message: "Please select media!", completionHandler: nil)
                }
                return
            }
        
        if !(viewEmail.txtFld_UserValue.text!.isEmpty)
        {
            if !Validation().isValidEmail(testStr: viewEmail.txtFld_UserValue.text!){
                DispatchQueue.main.async {
                    TrenderAlertVC.shared.presentAlertController(message: "Enter valid email id.", completionHandler: nil)
                }
                return
            }
        }

            var IsTrendPublic = false
            if lblPublicPrivate.text == MySingleton.shared.selectedLangData.Public
            {
                IsTrendPublic = true
                self.trendTypeId = 1
            }
            else if lblPublicPrivate.text == MySingleton.shared.selectedLangData.Private
            {
                 self.trendTypeId = 2
            }
            else
            {
                self.trendTypeId = 3
            }


            var expiryDuration = String()
            if imgDaysSwitch.image != #imageLiteral(resourceName: "radio_On"){
                if (lblSliderValue.text!).contains(MySingleton.shared.selectedLangData.hours)
                {
                    expiryDuration = (lblSliderValue.text!).replacingOccurrences(of: " \(MySingleton.shared.selectedLangData.hours)", with: "")
                }
                else
                {
                    expiryDuration = (lblSliderValue.text!).replacingOccurrences(of: " \(MySingleton.shared.selectedLangData.hour)", with: "")
                }
            }
            else
            {
                expiryDuration = (lblSliderValue.text!).replacingOccurrences(of: " \(MySingleton.shared.selectedLangData.days)", with: "")
                expiryDuration = String(Int(expiryDuration)!*24)
            }
            
            print(expiryDuration)
        var params = ["Title": viewTitle.txtFld_UserValue.text!, "ExpiryDuration": Int(expiryDuration)!, "MediaType": "IMAGE", "Description": txtDescription.text!, "ContactEmail": viewEmail.txtFld_UserValue.text!, "IsContactByAudio": true, "IsContactByText": true,"IsTrendPublic": IsTrendPublic,"IsTrendActive": true, "IsTrendDelete": false,"IsTrenderOnline": true, "isChangeImage": true, "isChangeTrendThumbnail": false, "isChangeVideo": false, "TrendLocation": viewLocation.txtFld_UserValue.text!, "TrendLatitude": str_latitude, "TrendLongitude": str_longitude, "TaggedTrenders": arraySelectedTagTrendersId,"IsForContest":self.isForContest,"ContestId":self.contestId,"IsTrendForGroup":self.groupParams?.isTrendForGroup ?? false,"GroupId":self.groupParams?.groupId ?? 0,"TrendTypeId":self.trendTypeId,"TaggedFriends":self.selectedFriendsArray,"isBusinessAccount":self.isBusinessAccount,"TrendCity":self.city,"TrendCountry":self.country] as [String : Any]
            if lblFolder.text != MySingleton.shared.selectedLangData.folder
            {
                params["FolderPath"] = lblFolder.text!
            }
            
            if statusData.count != 0
            {
                params["isChangeImage"] = false
                params["MediaType"] = "TEXT"
                params["TextInformation"] = statusData
            }
        
        if arrayOnlyImagesSelected.count != 0
        {
            var imageList = [AnyObject]()
            for i in 0...self.arrayOnlyImagesSelected.count-1
            {
                let imageData = ["FileExt":"jpg","Height": Int(self.arrayOnlyImagesSelected[i].size.height),"Width": Int(self.arrayOnlyImagesSelected[i].size.width),"contenttype":"image/jpg","ImageData":"data:image/jpg;base64,\(self.encodeToBase64String(image: self.arrayOnlyImagesSelected[i]))"] as [String : Any]
                imageList.append(imageData as AnyObject)
            }
            params["ImageList"] = ["Images": imageList]
        }
        
            
            if self.videoURL.count != 0
            {
                params["isChangeImage"] = false
                params["isChangeTrendThumbnail"] = true
                params["MediaType"] = "VIDEO"
                var imageList = [AnyObject]()
                
                for i in 0...self.imgCollectionMediaType.count-1
                {
                    if self.imgCollectionMediaType[i] == "VIDEO"
                    {
                        let imageData = ["FileExt":"jpg","Height": Int(self.imgCollection[i].size.height),"Width": Int(self.imgCollection[i].size.width),"contenttype":"image/jpg","ImageData":"data:image/jpg;base64,\(self.encodeToBase64String(image: self.imgCollection[i]))"] as [String : Any]
                        imageList.append(imageData as AnyObject)
                    }
                }
                
                params["ThumbnailList"] = ["Images": imageList]
            }
        
        //MARK - Shrunkita changes start
        
        if isEdit
        {
            if arrayOnlyImagesSelected.count == 0
            {
                params["isChangeImage"] = false
                params["isChangeTrendThumbnail"] = false
            }
            params["TrendId"] = self.editTrendParam.TrendId
        }
        else
        {
            params["TrendId"] = 0
        }
        
        //MARK - Shrunkita changes End
            
            callApi(requestData: params, url: "\(WebServices.baseURL)api/trend/CreateTrend")
            
            
//        }
        
    }
    
    func callApi(requestData: Dictionary<String, Any>, url: String)
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        //        var urlStr: String = "http://172.16.3.12:96/api/apiAccount/Register"
//        var urlStr: String = "\(WebServices.baseURL)api/trend/UploadTrendImage"
        
        var urlStr: String = url
        
        urlStr = urlStr.replacingOccurrences(of: " ", with: "&")
        let request :NSMutableURLRequest = NSMutableURLRequest(url:NSURL(string:urlStr)! as URL)
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        if requestData.count != 0
        {
            do
            {
                let jsonData = try JSONSerialization.data(withJSONObject: requestData, options: JSONSerialization.WritingOptions.prettyPrinted)
                request.httpBody = jsonData
            }
            catch let error as NSError
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                }
            }
        }
        let TokenValue = "Bearer " + MySingleton.shared.loginObject.access_token
        print(TokenValue)
        request.setValue(TokenValue, forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        

        let task = URLSession.shared.dataTask(with: request as URLRequest){ data,response,error in
            
            if error != nil
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error!.localizedDescription, completionHandler: nil)
                }
                
            }
            do
            {
                if (response as? HTTPURLResponse) != nil
                {
                    //////////////////// JSON RESPONSE /////////////////
                    
                    let jsonString = String(data: data!, encoding: .utf8)
                    print(jsonString!)
                    
                    //////////////////// JSON RESPONSE /////////////////
                    
                    if data != nil
                    {
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        
                        print(jsonData)
                        
                        
                        if(jsonData["Status"] as! Bool == true)
                        {

                                DispatchQueue.main.async {

                                    if self.videoURL.count != 0
                                    {
                                        self.UploadVideo(trendId: jsonData["data"] as! String)
                                    }
                                    else
                                    {
                                        self.statusData = [Any]()
                                        self.imgCollection = [UIImage]()
                                        self.imgCollectionMediaType = [String]()
                                        self.arrayOnlyImagesSelected = [UIImage]()
                                        self.lblPublicPrivate.text = MySingleton.shared.selectedLangData.Public
                                        self.lblFolder.text = MySingleton.shared.selectedLangData.folder
                                        self.mediaTypeId = 0
                                        self.txtDescription.text = ""
                                        self.viewTitle.txtFld_UserValue.text = ""
                                        self.arraySelectedTagTrendersId = [AnyObject]()
                                        self.viewEmail.txtFld_UserValue.text = ""
                                        self.viewLocation.txtFld_UserValue.text = ""
                                        self.viewTagTrender.txtFld_UserValue.text = ""
                                        self.videoURL = [URL]()
                                        self.collectionImage.reloadData()
                                        self.arrayTagTrender = [TagTrenderModel]()
                                        self.arraySelectedTagTrendersId = [AnyObject]()
                                        
                                        if self.isForContest == true
                                        {
                                            self.contestId = 0
                                            self.isForContest = false
                                            for controller in self.navigationController!.viewControllers as Array {
                                                if controller.isKind(of: ContestTrendsVC.self) {
                                                    self.navigationController!.popToViewController(controller, animated: true)
                                                    break
                                                }
                                            }
                                            //self.navigationController?.popViewController(animated: true)
                                        }
                                        else
                                        {
                                            if self.isGroup == true || (self.isBusinessAccount == 1)
                                            {
                                                if self.isBusinessAccount == 1
                                                {
                                                    self.delegate?.BuisnessAccount(reload: "Load")
                                                }
                                                self.navigationController?.popViewController(animated: true)
                                                
                                            }
                                            else if self.isEdit
                                            {
                                                 NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getAllTrends"), object: self, userInfo: nil)
                                                self.navigationController?.popViewController(animated: true)
                                            }
                                            else
                                            {
                                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getAllTrends"), object: self, userInfo: nil)
                                                (self.tabBarController!).selectedIndex = 0
                                            }
                                        }
                                    }
                                }
//                            }
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                MBProgressHUD.hide(for: self.view, animated: true)
                                TrenderAlertVC.shared.presentAlertController(message: jsonData["Message"] as! String, completionHandler: nil)
                            }
                        }
                        
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true)
                            TrenderAlertVC.shared.presentAlertController(message: error!.localizedDescription, completionHandler: nil)
                        }
                    }
                    
                }
            }
            catch let error as NSError
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                }
            }
        }
        task.resume()
    }
    
    func UploadVideo(trendId : String){
        if self.videoURL.count != 0
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let boundary = "Boundary-\(UUID().uuidString)"
            let TokenValue = "Bearer " + MySingleton.shared.loginObject.access_token
            print(TokenValue)
            let headers: HTTPHeaders = ["Authorization": TokenValue,
                "content-type": "multipart/form-data; boundary= \(boundary)",
                "cache-control": "no-cache"
            ]
            
            //  var request = URLRequest(url: NSURL.init(string: "http://172.16.3.4:81/api/trend/UploadTrendVideo/\(trendID)")! as URL)
            var request = URLRequest(url: NSURL.init(string: "\(WebServices.baseURL)api/trend/UploadTrendVideo/\(trendId)")! as URL)
            request.allHTTPHeaderFields = headers
            request.httpMethod = HTTPMethod.post.rawValue
            request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
            // request.httpBody = self.vdata as Data
            
            // print(self.vdata as Data)
            
            Alamofire.upload(multipartFormData: { multipartFormData in
//                for i in 0...self.videoURL.count-1
//                {
                    multipartFormData.append(self.videoURL[0] , withName: "fileUpload")
//                }
            }, with: request, encodingCompletion: {
                encodingResult in
                switch encodingResult {
                case .success(let upload, _,_):
                    upload.responseJSON { response in
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
                        debugPrint("SUCCESS RESPONSE: \(response)")
//                        if self.isForContest == true
//                        {
//                            for controller in self.navigationController!.viewControllers as Array {
//                                if controller.isKind(of: ContestTrendsVC.self) {
//                                    self.navigationController!.popToViewController(controller, animated: true)
//                                    break
//                                }
//                            }
//                          //  self.navigationController?.popViewController(animated: true)
//                        }
                       
                        self.statusData = [Any]()
                        self.imgCollection = [UIImage]()
                        self.imgCollectionMediaType = [String]()
                        self.arrayOnlyImagesSelected = [UIImage]()
                        self.lblPublicPrivate.text = MySingleton.shared.selectedLangData.Public
                        self.lblFolder.text = MySingleton.shared.selectedLangData.folder
                        self.mediaTypeId = 0
                        self.txtDescription.text = ""
                        self.viewTitle.txtFld_UserValue.text = ""
                        self.arraySelectedTagTrendersId = [AnyObject]()
                        self.viewEmail.txtFld_UserValue.text = ""
                        self.viewLocation.txtFld_UserValue.text = ""
                        self.viewTagTrender.txtFld_UserValue.text = ""
                        self.videoURL = [URL]()
                        self.collectionImage.reloadData()
                        if self.isForContest == true
                        {
                            self.contestId = 0
                            self.isForContest = false
                            for controller in self.navigationController!.viewControllers as Array {
                                if controller.isKind(of: ContestTrendsVC.self) {
                                    self.navigationController!.popToViewController(controller, animated: true)
                                    break
                                }
                            }
                            //self.navigationController?.popViewController(animated: true)
                        }
                        else
                        {
                            if self.isGroup == true || (self.isBusinessAccount == 1) || (self.isEdit)
                            {
                                if self.isBusinessAccount == 1
                                {
                                    self.delegate?.BuisnessAccount(reload: "Load")
                                }

                                self.navigationController?.popViewController(animated: true)
                                
                            }
                            else
                            {
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getAllTrends"), object: self, userInfo: nil)
                                (self.tabBarController!).selectedIndex = 0
                            }
                        }
                    }
                case .failure(let encodingError):
                    // hide progressbas here
                    DispatchQueue.main.async {
                        MBProgressHUD.hide(for: self.view, animated: true)
                        print("ERROR RESPONSE: \(encodingError)")
                        TrenderAlertVC.shared.presentAlertController(message: encodingError.localizedDescription, completionHandler: nil)
                    }
                    
                }
            })
        }
        else
        {
            
        }
    }
    
    func encodeToBase64String(image: UIImage) -> String
    {
        let imageData = image.jpegData(compressionQuality: 0.5)
        let imag_str:String = imageData!.base64EncodedString(options:[])
        return imag_str
    }
    
    @IBAction func sliderActionForValueChanged(_ sender: Any) {
        if imgHoursSwitch.image == #imageLiteral(resourceName: "radio_On") {
            if Int((sender as! UISlider).value) == 1
            {
                lblSliderValue.text = "\(Int((sender as! UISlider).value)) \(MySingleton.shared.selectedLangData.hour)"
            }
            else
            {
                lblSliderValue.text = "\(Int((sender as! UISlider).value)) \(MySingleton.shared.selectedLangData.hours)"
            }
        }
        else
        {
            lblSliderValue.text = "\(Int((sender as! UISlider).value)) \(MySingleton.shared.selectedLangData.days)"
        }
    }
    
    
    func OptionForCameraOrGallery(imgStatus: Bool)
    {
        let msg = MySingleton.shared.selectedLangData.please_select_your_option
        
        let alertController = UIAlertController(title: msg, message: "", preferredStyle: UIAlertController.Style.alert)
        let noAction = UIAlertAction(title: MySingleton.shared.selectedLangData.camera, style: UIAlertAction.Style.default, handler: {(alert: UIAlertAction!) in
            self.openCamera(imgStatus: imgStatus)
            
        })
        let yesAction = UIAlertAction(title: MySingleton.shared.selectedLangData.gallery, style: UIAlertAction.Style.default, handler: {(alert: UIAlertAction!) in
            
            self.openGallary(imgStatus: imgStatus)
        })
        
        let cancelAction = UIAlertAction(title: MySingleton.shared.selectedLangData.cancel, style: UIAlertAction.Style.default, handler: nil)
        
        
        alertController.addAction(noAction)
        alertController.addAction(yesAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func btnPlusAction(_ sender: Any) {
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: MySingleton.shared.selectedLangData.confirmation, message: MySingleton.shared.selectedLangData.please_select_your_option, preferredStyle: .actionSheet)
        
        let cameraActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.image, style: .default) { void in
            let imgStatusCount = self.arrayOnlyImagesSelected.count + self.statusData.count
            if self.isPremiumUser{
                if imgStatusCount >= 15
                {
                    DispatchQueue.main.async {
                        TrenderAlertVC.shared.presentAlertController(message: MySingleton.shared.selectedLangData.You_can_select_only_15_images_or_status, completionHandler: nil)
                    }
                }
                else
                {
                    self.OptionForCameraOrGallery(imgStatus: true)
                }
            }
            else{
                if imgStatusCount >= 1
                {
                    DispatchQueue.main.async {
                        TrenderAlertVC.shared.presentAlertController(message: "\(MySingleton.shared.selectedLangData.You_can_select_only_15_images_or_status)\(MySingleton.shared.selectedLangData.To_upload_more_get_premium_package)", completionHandler: {
                            MySingleton.shared.isFirstTimeSideMenu = false
                            let storyboard = UIStoryboard(name: "NotificationSB", bundle: nil)
                            let paymentVC = storyboard.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
                            let tabBar = self.menuContainerViewController.centerViewController as! UITabBarController
                            let navigationController = tabBar.selectedViewController as! UINavigationController
                            navigationController.viewControllers = [paymentVC]
                        //    self.menuContainerViewController.menuState = MFSideMenuStateClosed
                        })
                    }
                }
                else
                {
                    self.OptionForCameraOrGallery(imgStatus: true)
                }
            }
            
        }
        
        let galleryActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.video, style: .default) { void in
            if self.videoURL.count == 0
            {
                self.OptionForCameraOrGallery(imgStatus: false)
            }else
            {
                DispatchQueue.main.async {
                    TrenderAlertVC.shared.presentAlertController(message: MySingleton.shared.selectedLangData.You_can_select_only_1_video, completionHandler: nil)
                }
            }
        }
        let StatusActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.status, style: .default) { void in
            
             let imgStatusCount = self.arrayOnlyImagesSelected.count + self.statusData.count
            
            if self.isPremiumUser{
                if imgStatusCount >= 15
                {
                    DispatchQueue.main.async {
                        TrenderAlertVC.shared.presentAlertController(message: MySingleton.shared.selectedLangData.You_can_select_only_15_images_or_status, completionHandler: nil)
                    }
                }
                else
                {
                    let destination = self.storyboard?.instantiateViewController(withIdentifier: "StatusViewController") as! StatusViewController
                    destination.delegate = self
                    self.navigationController?.pushViewController(destination, animated: true)
                }
            }
            else{
                if imgStatusCount >= 1
                {
                    DispatchQueue.main.async {
                        TrenderAlertVC.shared.presentAlertController(message: "\(MySingleton.shared.selectedLangData.You_can_select_only_15_images_or_status)\(MySingleton.shared.selectedLangData.To_upload_more_get_premium_package)", completionHandler: nil)
                    }
                }
                else
                {
                    let destination = self.storyboard?.instantiateViewController(withIdentifier: "StatusViewController") as! StatusViewController
                    destination.delegate = self
                    self.navigationController?.pushViewController(destination, animated: true)
                }
            }
            
        }
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.cancel, style: .cancel) { void in
        }
//        if mediaTypeId == 0
//        {
            actionSheetControllerIOS8.addAction(StatusActionButton)
            actionSheetControllerIOS8.addAction(cameraActionButton)
            actionSheetControllerIOS8.addAction(galleryActionButton)
//        }
//        else if mediaTypeId == 1
//        {
//            actionSheetControllerIOS8.addAction(cameraActionButton)
//        }
//        else if mediaTypeId == 2
//        {
//            actionSheetControllerIOS8.addAction(galleryActionButton)
//        }
//        else if mediaTypeId == 3
//        {
//            actionSheetControllerIOS8.addAction(StatusActionButton)
//        }
        actionSheetControllerIOS8.addAction(cancelActionButton)
        actionSheetControllerIOS8.popoverPresentationController?.sourceView = self.view
        actionSheetControllerIOS8.popoverPresentationController?.sourceRect = self.view.bounds
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
//    func uploadVideo()
    
}

extension SetTrendViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    /*************************
     Method Name:  openGallary()
     Parameter: nil
     return type: nil
     Desc: This function opens the gallery.
     *************************/
    func openGallary(imgStatus: Bool)
    {
        if imgStatus
        {
            if isPremiumUser
            {
                let vc = BSImagePickerViewController()
                let imgStatusCount = self.arrayOnlyImagesSelected.count + self.statusData.count
                vc.maxNumberOfSelections = 15 - imgStatusCount
                
                bs_presentImagePickerController(vc, animated: true,
                                                select: { (asset: PHAsset) -> Void in
                                                    print("Selected: \(asset)")
                }, deselect: { (asset: PHAsset) -> Void in
                    print("Deselected: \(asset)")
                }, cancel: { (assets: [PHAsset]) -> Void in
                    print("Cancel: \(assets)")
                }, finish: { (assets: [PHAsset]) -> Void in
                    self.photoArray = assets
                    for i in 0...self.photoArray.count-1
                    {
                        let requestOptions = PHImageRequestOptions()
                        requestOptions.resizeMode = PHImageRequestOptionsResizeMode.exact
                        requestOptions.deliveryMode = PHImageRequestOptionsDeliveryMode.highQualityFormat
                        requestOptions.isSynchronous = true
                        PHImageManager.default().requestImage(for: self.photoArray[i], targetSize: PHImageManagerMaximumSize, contentMode: PHImageContentMode.default, options: requestOptions, resultHandler: { (pickedImage, info) in
                           
                            self.imgCollection.append(pickedImage!)
                            self.imgCollectionMediaType.append("IMAGE")
                            self.arrayOnlyImagesSelected.append(pickedImage!)
                            self.imageIdArray.append(0)
//                                self.photoLibArray.append(["FileExt":"jpeg","contenttype":"image/jpg", "ImageData":"data:image/jpg;base64,\(self.encodeToBase64String(image: pickedImage!))"])
//                                self.imgArray.append(0)
                        })
                    }
                    DispatchQueue.main.async {
                        self.mediaTypeId = 1
                        self.collectionImage.delegate = self
                        self.collectionImage.dataSource = self
                        self.collectionImage.reloadData()
                    }
                    print(self.imgCollection.count)
                }, completion: nil)
            }
            else
            {
                picker.allowsEditing = false
                picker.sourceType = UIImagePickerController.SourceType.photoLibrary
                picker.mediaTypes = [kUTTypeImage as String]
                present(picker, animated: true, completion: nil)
            }
        }
        else
        {
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerController.SourceType.savedPhotosAlbum
            //                picker.delegate = self
            picker.mediaTypes = [kUTTypeMovie as String]
            present(picker, animated: true, completion: nil)
        }
        
    }
    
    /*************************
     Method Name:  openCamera()
     Parameter: nil
     return type: nil
     Desc: This function opens the Camera.
     *************************/
    func openCamera(imgStatus: Bool)
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            if imgStatus
            {
                picker.allowsEditing = false
                picker.sourceType = UIImagePickerController.SourceType.camera
                picker.mediaTypes = [kUTTypeImage as String]
                picker.cameraCaptureMode = .photo
                present(picker, animated: true, completion: nil)
            }
            else
            {
                picker.allowsEditing = false
                print("video")
                picker.sourceType = .camera
                picker.mediaTypes = [kUTTypeMovie as String]
                picker.cameraCaptureMode = .video
                present(picker, animated: true, completion: nil)
            }
        }
        else
        {
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    /*************************
     Method Name:  imagePickerController()
     Parameter: UIImagePickerController, [String : Any]
     return type: nil
     Desc: This function is called when user selects image from gallery.
     *************************/
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
//        if statusData.count != 0
//        {
//            statusData = [Any]()
//            imgCollection = [UIImage]()
//        }
        
        if let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
           mediaTypeId = 1
            print(chosenImage)
            imgCollection.append(chosenImage)
            imgCollectionMediaType.append("IMAGE")
            arrayOnlyImagesSelected.append(chosenImage)
            self.imageIdArray.append(0)

//            self.cnstrntCollectionHeight.constant = 180
            dismiss(animated: true) {
                
                self.collectionImage.delegate = self
                self.collectionImage.dataSource = self
                self.collectionImage.reloadData()
            }
        }
        else{
            if((info[UIImagePickerController.InfoKey.mediaType] as! String) == "public.movie")
            {
                mediaTypeId = 2
                let mediaURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL
                
                let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".mov")
                compressVideo(inputURL: mediaURL as! URL, outputURL: compressedURL) { (exportSession) in
                    guard let session = exportSession else {
                        return
                    }
                    
                    switch session.status {
                    case .unknown:
                        break
                    case .waiting:
                        break
                    case .exporting:
                        break
                    case .completed:
//                        guard let compressedData = NSData(contentsOf: compressedURL) else {
//                            return
//                        }
                        self.encodeVideo(videoURLNew: compressedURL as NSURL)
                       
                    case .failed:
                        break
                    case .cancelled:
                        break
                    }
                }
                
//                self.encodeVideo(videoURLNew: mediaURL! as NSURL)
                
            }
        }
    }
    
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) else {
            handler(nil)
            
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mov
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
    
    func encodeVideo(videoURLNew: NSURL)
    {
        let avAsset = AVURLAsset(url: videoURLNew as URL, options: nil)
        
        let exportSession = AVAssetExportSession(asset: avAsset, presetName: AVAssetExportPresetPassthrough)
        
        let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let myDocumentPath = NSURL(fileURLWithPath: documentsDirectory).appendingPathComponent("temp.mp4")?.absoluteString
        
        let documentsDirectory2 = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0] as NSURL
        
        let filePath = documentsDirectory2.appendingPathComponent("rendered-Video.mp4")
        deleteFile(filePath: filePath! as NSURL)
        
        if FileManager.default.fileExists(atPath: myDocumentPath!) {
            do {
                try FileManager.default.removeItem(atPath: myDocumentPath!)
            }
            catch let error {
                print(error)
            }
        }
        exportSession?.outputURL = filePath! as URL
        //set the output file format if you want to make it in other file format (ex .3gp)
        exportSession?.outputFileType = AVFileType.mp4
        exportSession?.shouldOptimizeForNetworkUse = true
        
        let start = CMTimeMakeWithSeconds(0.0, preferredTimescale: 0)
        let range = CMTimeRangeMake(start: start, duration: avAsset.duration)
        exportSession?.timeRange = range
        
        exportSession?.exportAsynchronously(completionHandler: {() -> Void in
            switch exportSession?.status {
            case .failed?:
                print("%@",exportSession?.error!)
            case .cancelled?:
                print("Export canceled")
            case .completed?:
                //Video conversion finished
                print("Successful!")
                print(exportSession?.outputURL!)
                self.videoURL.append(((exportSession?.outputURL!)!))
            default:
                break
            }
            do {
                
                let assets = AVURLAsset(url: self.videoURL[self.videoURL.count-1] , options: nil)
                let imgGenerator = AVAssetImageGenerator(asset: assets)
                imgGenerator.appliesPreferredTrackTransform = true
                let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
                // !! check the error before proceeding
                let uiImage = UIImage(cgImage: cgImage)
                
                self.imgCollection.append(uiImage)
                self.imgCollectionMediaType.append("VIDEO")
                DispatchQueue.main.async {
                    self.dismiss(animated: true) {
                    self.collectionImage.delegate = self
                    self.collectionImage.dataSource = self
                    self.collectionImage.reloadData()
                    }
                }
                
                  }
                catch let error {
                    print(error.localizedDescription)
                    DispatchQueue.main.async {
                       self.dismiss(animated:true, completion: nil)
                       }
                  }
        })
    }
    
    func deleteFile(filePath:NSURL) {
        guard FileManager.default.fileExists(atPath: filePath.path!) else {
            return
        }
        
        do {
            try FileManager.default.removeItem(atPath: filePath.path!)
        }catch{
            fatalError("Unable to delete file: \(error) : \(#function).")
        }
    }
    
//    func fixOrientation(img:UIImage) -> UIImage {
//        if (img.imageOrientation == UIImage.Orientation.up) {
//            return img;
//        }
//        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale);
//        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
//        img.draw(in: rect)
//        let normalizedImage : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
//        UIGraphicsEndImageContext();
//        return normalizedImage;
//    }
}

extension SetTrendViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgCollection.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectImageCollectionViewCell", for: indexPath) as! SelectImageCollectionViewCell
        cell.image.makeCornerRadius(radius: 10.0)
            cell.image.image = self.imgCollection[indexPath.row]
            cell.indicator.isHidden = true
        cell.RemoveCell = {
            collectionView.performBatchUpdates({
                if self.imgCollectionMediaType[indexPath.row] == "IMAGE"
                {
                    for i in 0...self.arrayOnlyImagesSelected.count-1
                    {
                        if self.imgCollection[indexPath.row] == self.arrayOnlyImagesSelected[i]
                        {
                            self.arrayOnlyImagesSelected.remove(at: i)
//                            if self.isEdit
//                            {
//                                self.deleteImage(index: i)
//                            }
                            break
                        }
                    }
                }
                else if self.imgCollectionMediaType[indexPath.row] == "STATUS"
                {
                    var index = 0
                    for i in 0...indexPath.row
                    {
                        if self.imgCollectionMediaType[i] == "STATUS"
                        {
                            index = index+1
                        }
                    }
                    self.statusData.remove(at: index-1)
                    
                }
                else if self.imgCollectionMediaType[indexPath.row] == "VIDEO"
                {
                    var index = 0
                    for i in 0...indexPath.row
                    {
                        if self.imgCollectionMediaType[i] == "VIDEO"
                        {
                            index = index+1
                        }
                    }
                    self.videoURL.remove(at: index-1)
                }
                self.imgCollection.remove(at: indexPath.row)
                self.imgCollectionMediaType.remove(at: indexPath.row)
                if self.imgCollection.count == 0
                {
                    
                    self.mediaTypeId = 0
                    self.videoURL = [URL]()
                }
                collectionView.deleteItems(at: [indexPath]);
            }, completion: { (bool) in
                collectionView.reloadItems(at: collectionView.indexPathsForVisibleItems);
            })
        }
        
        // Edit changes
//        if isEdit{
//            cell.image.sd_setImage(with: URL(string: self.editTrendParam.ImageList![indexPath.row].TrendImage), placeholderImage: #imageLiteral(resourceName: "logo_navigation"))
//            //   mediaTypeId = 1
//            if self.editTrendParam.ImageList![indexPath.row].Media == "IMAGE"
//            {
//                self.imageIdArray.append(self.editTrendParam.ImageList![indexPath.row].TrendImageId!)
//                self.imgCollection.append(cell.image.image!)
//                self.imgCollectionMediaType.append("IMAGE")
//                self.arrayOnlyImagesSelected.append(cell.image.image!)
//            }
//            else if self.editTrendParam.ImageList![indexPath.row].Media == "VIDEO"
//            {
//                self.imgCollection.append(cell.image.image!)
//                self.imgCollectionMediaType.append("VIDEO")
//                self.arrayOnlyImagesSelected.append(cell.image.image!)
//            }
//            else
//            {
//                self.imageIdArray.append(self.editTrendParam.ImageList![indexPath.row].TrendImageId!)
//                self.imgCollection.append(cell.image.image!)
//                //    imgCollectionMediaType.append("STATUS")
//            }
//
//        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         return CGSize(width:collectionView.frame.height, height:collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if imgCollectionMediaType[indexPath.row] == "IMAGE"
        {
            let cell = collectionView.cellForItem(at: indexPath)as! SelectImageCollectionViewCell
            let cropController = CropViewController(croppingStyle: croppingStyle, image: cell.image.image!)
            cropController.delegate = self
            cropIndexPath = indexPath
            self.present(cropController, animated: true, completion: nil)
        }
    }
    
    func deleteImage(index : Int)
    {
        let param = [
            "TrendId": self.editTrendParam.TrendId,
            "ImageId": self.imageIdArray[index]
        ]
        WebServices().callUserService(service: .deleteImage, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse,serviceData) in
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    print(serviceResponse["Status"]!)
                }
            }
        })
    }
    
    func deleteVideo()
    {
        WebServices().callUserService(service: .deleteVideo, urlParameter: "\(self.editTrendParam.TrendId)", parameters: nil, isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse,serviceData) in
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    print(serviceResponse["Status"]!)
                }
            }
        })
    }
    
    func deleteStatus(index : Int)
    {
        WebServices().callUserService(service: .deleteStatus, urlParameter: "\(self.editTrendParam)", parameters: nil, isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse,serviceData) in
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    print(serviceResponse["Status"]!)
                }
            }
        })
    }
}

extension SetTrendViewController: CropViewControllerDelegate
{
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.croppedRect = cropRect
        self.croppedAngle = angle
//        updateImageViewWithImage(image, fromCropViewController: cropViewController)
        cropViewController.dismiss(animated: true) {
            let cell = self.collectionImage.cellForItem(at: self.cropIndexPath)as! SelectImageCollectionViewCell
            
            for i in 0...self.arrayOnlyImagesSelected.count-1
            {
                if self.imgCollection[self.cropIndexPath.row] == self.arrayOnlyImagesSelected[i]
                {
                    self.arrayOnlyImagesSelected[i] = image
                    break
                }
            }
            cell.image.image = image
            self.imgCollection[self.cropIndexPath.row] = image
        }
    }
}

extension SetTrendViewController: TagTrenderDelegate{
    func controller(arraySelectedTrender: [TagTrenderModel]) {
        arraySelectedTagTrendersId = [AnyObject]()
        arrayTagTrender = arraySelectedTrender
        if arraySelectedTrender.count != 0
        {
            for i in 0...arraySelectedTrender.count-1
            {
                if(arraySelectedTrender[i].isSelected)
                {
                    let dict = ["TrenderId": arraySelectedTrender[i].id]
                    arraySelectedTagTrendersId.append(dict as AnyObject)
                }
            }
            viewTagTrender.txtFld_UserValue.text = "\(arraySelectedTagTrendersId.count) \(MySingleton.shared.selectedLangData.tag_trenders)"
        }
    }    
}

extension SetTrendViewController: UITextFieldDelegate{
    
    /*************************
     Method Name:  textFieldTextChanged()
     Parameter: UITextField
     return type: nil
     Desc: This function cis called when text Field Text is Changed.
     *************************/
    @objc func textFieldTextChanged(_ sender : UITextField)
    {
        location_arr = [String]()
        if(viewLocation.txtFld_UserValue.isEditing)
        {
            if  viewLocation.txtFld_UserValue.text!.isEmpty
            {
                self.locationDropdown.hide()
            }
            else
            {
                let getAddress = GetAddress()
                getAddress.getAddress(location_str: viewLocation.txtFld_UserValue.text!) { (locationData) in
                    // self.location_arr = locationData
                    for location in locationData
                    {
                        self.location_arr.append(location["address"]!)
                    }
                    print(self.location_arr)
                    self.locationDropdown.dataSource = self.location_arr
                    
                    if !self.viewLocation.txtFld_UserValue.text!.isEmpty
                    {
                        self.locationDropdown.show()
                    }
                }
            }
        }else if (viewLocation.txtFld_UserValue.isEditing)
        {
            sender.text = sender.text?.lowercased()
        }
    }
}

extension SetTrendViewController: UITextViewDelegate
{
//    func textViewDidBeginEditing(_ textView: UITextView) {
//        if(textView == txtDescription)
//        {
//            if txtDescription.text == "Description..."
//            {
//                if txtDescription.textColor == UIColor.lightGray{
//                    txtDescription.text = nil
//                    txtDescription.textColor = UIColor.black
//                }
//            }
//        }
//    }
    
//    func textViewDidEndEditing(_ textView: UITextView) {
//        if(textView == txtDescription)
//        {
//            if (txtDescription.text?.isEmpty)!
//            {
//                txtDescription.text = "Description..."
//                txtDescription.textColor = UIColor.lightGray
//            }
//        }
//    }
}
extension SetTrendViewController: StatusData
{
    func fetchStatusData(status: [String : Any], image: UIImage) {
        mediaTypeId = 3
        statusData.append(status as AnyObject)
//        imgCollection = [UIImage]()
        imgCollection.append(image)
        imgCollectionMediaType.append("STATUS")
        collectionImage.delegate = self
        collectionImage.dataSource = self
        collectionImage.reloadData()
    }
    
    
}

extension SetTrendViewController: CreateFolderDelegate, friendSelectedDelegate
{
    func controller(arraySelectedFriends: [TagFriendModel], index: Int, trendId: Int, description: String) {
       // self.selectedFriendsArray = arraySelectedFriends //as [AnyObject]
        if arraySelectedFriends.count != 0
        {
            for i in 0...arraySelectedFriends.count-1
            {
                if(arraySelectedFriends[i].isSelected)
                {
                    let dict = ["FriendId": arraySelectedFriends[i].id]
                    selectedFriendsArray.append(dict as AnyObject)
                }
            }
        }
        if arraySelectedFriends.count == 0
        {
            lblPublicPrivate.text = MySingleton.shared.selectedLangData.Public
            self.trendTypeId = 1
        }
        else
        {
            lblPublicPrivate.text = MySingleton.shared.selectedLangData.friends
            self.trendTypeId = 3
        }
    }
    
    func controller() {
        getUserInfo()
    }
}

extension SetTrendViewController: CLLocationManagerDelegate
{
    /*************************
     Method Name:  locationManager()
     Parameter: CLLocationManager, CLAuthorizationStatus
     return type: nil
     Desc: This function cis called when didChangeAuthorization.
     *************************/
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            manager.startUpdatingLocation()
            DispatchQueue.main.async {
                MBProgressHUD.showAdded(to: self.view, animated: true)
            }
            
        }
        else
        {
            //            TrenderAlertVC.shared.presentAlertController(message: "Please go to settings and enable location services.", completionHandler: nil)
        }
    }
    
    /*************************
     Method Name:  locationManager()
     Parameter: CLLocationManager, CLLocation
     return type: nil
     Desc: This function cis called when didUpdateLocations.
     *************************/
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        manager.stopMonitoringSignificantLocationChanges()
        manager.stopUpdatingLocation()
        str_latitude = (String(locations.last!.coordinate.latitude))
        str_longitude = (String(locations.last!.coordinate.longitude))
        GetAddress().getAddressFromLocation(locations.last!) { (address) in
            self.viewLocation.txtFld_UserValue.text! = address
        }
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: self.view, animated: true)
        }
        GetAddress().getAddressFromLocationSetTrendCity(locations.last!) { (address) in
            self.city = address
        }
        GetAddress().getAddressFromLocationSetTrendCountry(locations.last!) { (address) in
            self.country = address
        }
    }
}

//extension SetTrendViewController: UITextViewDelegate {
//    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
//        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
//        let numberOfChars = newText.count
//        return numberOfChars < 10    // 10 Limit Value
//    }
//}
