//
//  IAPService.swift
//  TrenderAlert
//
//  Created by HPL on 12/03/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import StoreKit
import MBProgressHUD


extension Notification.Name {
    
    static let purchaseNotAllowed   = Notification.Name("purchaseNotAllowed")
    static let userPremiumSuccess   = Notification.Name("userPremiumSuccess") // Api response
    static let userPremiumFailed    = Notification.Name("userPremiumFailed")  // Api response
    static let transactionSuccess   = Notification.Name("transactionSuccess")
    static let transactionFail      = Notification.Name("transactionFail")
    static let transactionInProgress = Notification.Name("transactionInProgress")
    static let productRequest = Notification.Name("productRequest")
    static let productRequestResp = Notification.Name("productRequestResp")
    static let purchasing = Notification.Name(rawValue: "purchasing")

}

class IAPService: NSObject{
    private override init(){}
    static let shared = IAPService()
    
    var products = [SKProduct]()
    let paymentQueue = SKPaymentQueue.default()
    
    func canMakePurchases() -> Bool {  return SKPaymentQueue.canMakePayments()  }
    
    
    func getProducts(){
        
        let products:Set = [IAPProduct.trenderAlertPremiumConsumable.rawValue,
                            IAPProduct.trenderAlertPremiumNonConsumable.rawValue]
        let request = SKProductsRequest(productIdentifiers: products)
        request.delegate = self
        request.start()
        paymentQueue.add(self)
    }
    
    func purchase(product:IAPProduct){
        if self.canMakePurchases() {
            guard let productToPurchase = products.filter({$0.productIdentifier == product.rawValue}).first else{
                return
            }
            let payment = SKPayment(product: productToPurchase)
            paymentQueue.add(payment)
        }else{
            NotificationCenter.default.post(name: .purchaseNotAllowed, object: "You're not allowed to make purchase on this device.")
        }
    }
    
    func restorePurchases(){
        print("restoring purchases...")
        paymentQueue.restoreCompletedTransactions()
    }
}

extension IAPService:SKProductsRequestDelegate{
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        NotificationCenter.default.post(name:.productRequestResp , object: "productRequest response received.")

        self.products = response.products
        for product in response.products{
            print(product.localizedTitle)
        }
    }
}

extension IAPService:SKPaymentTransactionObserver{
    
    func makeUserPremium(receipt:String){
        
        let param = ["ReceiptID":receipt]
        print("data ",param)
        WebServices().callUserService(service: UserServices.makeUserPremium, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                   
                    MySingleton.shared.loginObject.IsPremiumUser = "True"
                    var usersArray = [Data]()
                    if UserDefaults.standard.object(forKey: "usersArray") != nil {
                        usersArray = UserDefaults.standard.object(forKey: "usersArray") as! [Data]
                    }
                    
                    for (index,user) in usersArray.enumerated(){
                        let userObject = WebServices().decodeDataToClass(data: user, decodeClass: Login.self)
                        
                        if MySingleton.shared.loginObject.UserID == userObject?.UserID{
                            userObject?.IsPremiumUser = "True"
                            do {
                                let classObject = try JSONEncoder().encode(userObject)
                                usersArray[index] = classObject
                            }catch let error {
                                print("Codable Error --- \(error)")
                            }
                        }
                    }
                   
                    
                    
                    UserDefaults.standard.set(usersArray, forKey: "usersArray")
                    UserDefaults.standard.synchronize()
                    NotificationCenter.default.post(name:.userPremiumSuccess , object: receipt)
                }else{
                    NotificationCenter.default.post(name:.userPremiumFailed , object: serviceResponse["Message"])
                }
            }else{
                NotificationCenter.default.post(name:.userPremiumFailed , object: serviceResponse["Message"])
            }
        }
    }
    
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
        for transaction in transactions{
            print(transaction.transactionState.status(),transaction.payment.productIdentifier)
            switch transaction.transactionState{
            case .purchasing:
                NotificationCenter.default.post(name:.purchasing , object: "Purchasing in progress")
                break
            case .purchased:
                NotificationCenter.default.post(name:.transactionSuccess , object: "Transaction success.")
                queue.finishTransaction(transaction)
//                print("Purchase Transaction Id:",transaction.transactionIdentifier!)
                
                if (transaction.original?.transactionIdentifier) != nil{
                    print("1: ",(transaction.original?.transactionIdentifier)!)
                    makeUserPremium(receipt: (transaction.original?.transactionIdentifier)!)
                }else if transactions.first?.transactionIdentifier != nil{
                    print("2: ",(transactions.first?.transactionIdentifier)!)
                    makeUserPremium(receipt: (transactions.first?.transactionIdentifier)!)
                   //1000000527578038
                }
               
            case .restored:
                NotificationCenter.default.post(name:.transactionSuccess , object: "Transaction sucess.")
                queue.finishTransaction(transaction)
//                print("Restored Transaction Id:",transaction.original?.transactionIdentifier!)

//                makeUserPremium(receipt: transaction.transactionIdentifier!)
                makeUserPremium(receipt: (transaction.original?.transactionIdentifier)!)
            case .failed:
                NotificationCenter.default.post(name:.transactionFail , object: "Transaction failed.")
                queue.finishTransaction(transaction)
            case .deferred:
                NotificationCenter.default.post(name:.transactionFail , object: "Transaction failed.")
                queue.finishTransaction(transaction)
            }
        }
    }
    
}

extension SKPaymentTransactionState{
    func status()->String{
        switch self {
        case .deferred: return "deferred"
        case .failed:   return "failed"
        case .purchased:    return "purchased"
        case .purchasing:   return "purchasing"
        case .restored: return "restored"
        }
    }
}

/*
 IN VC viewDidLoad
 IAPService.shared.getProducts()
 
 on Button click
 IAPService.shared.purchase(product:.trenderAlertPremiumConsumable)
 
 */
