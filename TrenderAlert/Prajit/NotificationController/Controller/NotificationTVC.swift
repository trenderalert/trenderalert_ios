//
//  NotificationTVC.swift
//  TrenderAlert
//
//  Created by HPL on 09/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class NotificationTVC: UITableViewCell {

    @IBOutlet weak var personImage: UIImageView!
    @IBOutlet weak var personNameLbl: UILabel!
    @IBOutlet weak var personActionLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    
    static let reuseIdentifier = "NotificationTVC"
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(){
        DispatchQueue.main.async{
            self.personImage.makeCornerRadius(radius: self.personImage.frame.height/2)
        }
    }
 }
