//
//  RequestApprovalTVC.swift
//  TrenderAlert
//
//  Created by Riken Shah on 10/07/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class RequestApprovalTVC: UITableViewCell {

    @IBOutlet weak var btnDecline: UIButton!
    @IBOutlet weak var btnAccept: UIButton!
    @IBOutlet weak var viewAccept: UIView!
    @IBOutlet weak var lblExpiry: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    var profileNameTap = UITapGestureRecognizer()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        imgProfile.makeCornerRadius(radius: imgProfile.frame.width/2)
//        btnAccept.layer.backgroundColor = UIColor.baseColor.cgColor
        btnAccept.makeCornerRadius(radius: 2)
        
        btnDecline.layer.borderWidth = 1
        btnDecline.layer.borderColor = UIColor.friendsSBGrayColor.cgColor
        btnDecline.makeCornerRadius(radius: 2)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(){
        DispatchQueue.main.async{
            self.imgProfile.makeCornerRadius(radius: self.imgProfile.frame.height/2)
        }
    }
    
}
