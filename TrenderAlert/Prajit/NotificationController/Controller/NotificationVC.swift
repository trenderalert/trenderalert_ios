//
//  NotificationVC.swift
//  TrenderAlert
//
//  Created by HPL on 09/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import SDWebImage

class NotificationVC: UIViewController {
    
    @IBOutlet weak var allBtn: UIButton!
    @IBOutlet weak var msgBtn: UIButton!
    
    @IBOutlet weak var notificationTbl: UITableView!
    
    let noOfRecordPerRequest = 5
    var totalNotificationCount = 1
//    var totalMessageNotificationCount = 1
    var isNotificationRequestInProgress = false
    
    var notificationList = NotificationList()
    var messageNotificationList = NotificationMessagesList()
    
    let fontSize  = (UIDevice.current.userInterfaceIdiom == .pad) ? 18 : 13
    
    //View
    let notificationView = 1
    let messageView = 2
    var currentView = 1
    
    
    lazy var refreshControl:UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = UIColor.red
        return refreshControl
    }()
    
    var noOfNotifications = 0{
        didSet{
            if currentView == notificationView{
                DispatchQueue.main.async {
                    self.notificationTbl.reloadData()
                }
            }
        }
    }
    
    var noOfMsgNotifications = 0{
        didSet{
            if currentView == messageView{
                DispatchQueue.main.async {
                    self.notificationTbl.reloadData()
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableview()
        showAllNotifications()
        
        setupNavaigationbar()
    }
    
    func setupNavaigationbar(){
      Utilities.setNavigationBarWithTitle(viewController: self, title: "Notifications")
        let leftButton = UIBarButtonItem(image: UIImage(named: "img_BackArrow"), style: .plain, target: self, action: #selector(goBack))
        navigationItem.leftBarButtonItem = leftButton
    }
    
    @objc func goBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        switch currentView{
        case notificationView:
            self.notificationList.data = []
            totalNotificationCount = 1
            getNotificationList(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: true)
        case messageView:
            self.messageNotificationList.data = []
            totalNotificationCount = 1
            getMessagesList(Skip: 0, Take: 20, lazyLoading: true)
            
            return
        default:
            self.notificationList.data = []
            totalNotificationCount = 1
            getNotificationList(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: true)
        }
        self.notificationTbl.reloadData()
        refreshControl.endRefreshing()
    }
    
    func getNotificationList(Skip: Int, Take: Int, lazyLoading: Bool){
        
        if (notificationList.data.count >=  totalNotificationCount) || isNotificationRequestInProgress{
            return
        }
        
        self.isNotificationRequestInProgress = true
        
        let param = ["Skip":Skip,"Take":Take] as [String : Any]
        
        WebServices().callUserService(service: UserServices.getAllNotifications, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: lazyLoading, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    DispatchQueue.main.async {
                    let temp = WebServices().decodeDataToClass(data:serviceData, decodeClass: NotificationList.self)!
                    self.notificationList.data += temp.data
                    self.totalNotificationCount = serviceResponse["TotalRecords"] as! Int
                    self.noOfNotifications += 1
                    self.isNotificationRequestInProgress = false
                    self.allBtn.setTitle("All (\(serviceResponse["TotalRecords"] as! Int))", for: .normal)
                    }	
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func getMessagesList(Skip: Int, Take: Int, lazyLoading: Bool){
        if (messageNotificationList.data.count >=  totalNotificationCount) || isNotificationRequestInProgress{
            return
        }
        
        self.isNotificationRequestInProgress = true
        
        let param = ["Skip":Skip,"Take":Take] as [String : Any]
        
        WebServices().callUserService(service: UserServices.getMessageNotification, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: lazyLoading, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    DispatchQueue.main.async {
                        let temp = WebServices().decodeDataToClass(data:serviceData, decodeClass: NotificationMessagesList.self)!
                        self.messageNotificationList.data += temp.data
                        self.totalNotificationCount = serviceResponse["TotalRecords"] as! Int
                        self.noOfMsgNotifications += 1
                        self.isNotificationRequestInProgress = false
                        self.msgBtn.setTitle("Messages (\(serviceResponse["TotalRecords"] as! Int))", for: .normal)
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
        
    }
    
    @IBAction func toggleNotification(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            showAllNotifications()
        case 2:
            showMessages()
        default:
            showAllNotifications()
        }
        self.notificationTbl.reloadData()
    }
    
    func setupTableview(){
        notificationTbl.delegate = self
        notificationTbl.dataSource = self
        
        notificationTbl.register(UINib(nibName: NotificationTVC.reuseIdentifier, bundle: nil), forCellReuseIdentifier: NotificationTVC.reuseIdentifier)
        notificationTbl.register(UINib(nibName: "RequestApprovalTVC", bundle: nil), forCellReuseIdentifier: "RequestApprovalTVC")
        notificationTbl.estimatedRowHeight = 100
        notificationTbl.rowHeight = UITableView.automaticDimension
        notificationTbl.addSubview(self.refreshControl)
    }
    
    func showAllNotifications(){
        allBtn.layer.backgroundColor = UIColor.baseColor.cgColor
        allBtn.setTitleColor(UIColor.black, for: .normal)
        msgBtn.layer.backgroundColor = UIColor.white.cgColor
        msgBtn.setTitleColor(UIColor.friendSBGrayFontColor, for: .normal)
        currentView = 1
        self.notificationList.data = []
        totalNotificationCount = 1
        getNotificationList(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: false)
    }
    
    func showMessages(){
        msgBtn.layer.backgroundColor = UIColor.baseColor.cgColor
        msgBtn.setTitleColor(UIColor.black, for: .normal)
        allBtn.layer.backgroundColor = UIColor.white.cgColor
        allBtn.setTitleColor(UIColor.friendSBGrayFontColor, for: .normal)
        currentView = 2
        self.messageNotificationList.data = []
        totalNotificationCount = 1
        getMessagesList(Skip: 0, Take: 20, lazyLoading: true)
//        notificationTbl.reloadData()
    }
}

extension NotificationVC:UITableViewDelegate,UITableViewDataSource {
    
    @objc func showZoomedImage(_ sender:UITapGestureRecognizer){
        let imageUrl = notificationList.data[(sender.view?.tag)!].ProfileImage ?? ""
        
        let zoomImageView = ZoomImageVC(nibName: "ZoomImageVC", bundle: nil)
        zoomImageView.imageUrl = imageUrl
        self.navigationController?.pushViewController(zoomImageView, animated: true)
    }
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch currentView{
        case notificationView:
            return notificationList.data.count
            
        case messageView:
            return messageNotificationList.data.count
            
        default:
            return 0
        }
    }
    
    func declineFriendRequest(reqId: Int, reqFrom: Int, index: Int){
        
        let params = ["RequestID":reqId,"RequestFrom":reqFrom]
        
        WebServices().callUserService(service: UserServices.declineFriendRequest, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    DispatchQueue.main.async {
//                        self.getNotificationList(Skip: 0, Take: self.noOfRecordPerRequest, lazyLoading: true)
                        self.markReadApi(notifId: self.notificationList.data[index].NotificationID!, notifType: self.notificationList.data[index].NotificationType!)
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func declineGroupRequest(groupID: Int, index: Int){
        
        let params = ["GroupID":groupID,"GroupjoinResponse":3]
        
        WebServices().callUserService(service: UserServices.groupInvite, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    DispatchQueue.main.async {
//                        self.notificationList.data = []
//                        self.totalNotificationCount = 1
//                        self.getNotificationList(Skip: 0, Take: self.noOfRecordPerRequest, lazyLoading: true)
                        self.markReadApi(notifId: self.notificationList.data[index].NotificationID!, notifType: self.notificationList.data[index].NotificationType!)
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func declineGroupJoinRequest(groupID: Int, index: Int){
        
        let params = ["GroupID":groupID,"GroupjoinResponse":3,"userProfileID": notificationList.data[index].UserProfileID]
        
        WebServices().callUserService(service: UserServices.groupJoinRequest, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    DispatchQueue.main.async {
//                        self.notificationList.data = []
//                        self.totalNotificationCount = 1
//                        self.getNotificationList(Skip: 0, Take: self.noOfRecordPerRequest, lazyLoading: true)
                        self.markReadApi(notifId: self.notificationList.data[index].NotificationID!, notifType: self.notificationList.data[index].NotificationType!)
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func declineFollowRequest(reqId: Int, reqFrom: Int, index: Int){
        
        let params = ["RequestID":reqId,"RequestFrom":reqFrom]
        
        WebServices().callUserService(service: UserServices.declineFollowRequest, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    DispatchQueue.main.async {
//                        self.getNotificationList(Skip: 0, Take: self.noOfRecordPerRequest, lazyLoading: true)
                        self.markReadApi(notifId: self.notificationList.data[index].NotificationID!, notifType: self.notificationList.data[index].NotificationType!)
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    @objc func btnDeclineAction(_ sender : UIButton)
    {
        if notificationList.data[sender.tag].NotificationType == "SEND_FRIEND_REQUEST"
        {
            declineFriendRequest(reqId: notificationList.data[sender.tag].FriendRequestID!, reqFrom: notificationList.data[sender.tag].UserProfileID!, index: sender.tag)
        }
        else if notificationList.data[sender.tag].NotificationType == "FOLLOW_USER"
        {
            declineFollowRequest(reqId: notificationList.data[sender.tag].FollowID!, reqFrom: notificationList.data[sender.tag].UserProfileID!, index: sender.tag)
        }
        else if notificationList.data[sender.tag].NotificationType == "GROUP_INVITATION"
        {
            declineGroupRequest(groupID: notificationList.data[sender.tag].GroupID!, index: sender.tag)
        }
        else if notificationList.data[sender.tag].NotificationType == "USER_GROUP_JOIN_REQUEST"
        {
            declineGroupJoinRequest(groupID: notificationList.data[sender.tag].GroupID!, index: sender.tag)
        }
    }
    
    func accepFollowRequest(reqId: Int, reqFrom: Int, index: Int){
        
        let params = ["RequestID":reqId,"RequestFrom":reqFrom]
        
        WebServices().callUserService(service: UserServices.acceptFollowRequest, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in

            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){

                    DispatchQueue.main.async {
//                        self.getNotificationList(Skip: 0, Take: self.noOfRecordPerRequest, lazyLoading: true)
                        self.markReadApi(notifId: self.notificationList.data[index].NotificationID!, notifType: self.notificationList.data[index].NotificationType!)
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func acceptFriendRequest(reqId: Int, reqFrom: Int, index: Int){
        
        let params = ["RequestID":reqId,"RequestFrom":reqFrom]
        
        WebServices().callUserService(service: UserServices.acceptFriendRequest, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    
                    DispatchQueue.main.async {
//                        self.getNotificationList(Skip: 0, Take: self.noOfRecordPerRequest, lazyLoading: true)
                        self.markReadApi(notifId: self.notificationList.data[index].NotificationID!, notifType: self.notificationList.data[index].NotificationType!)
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func acceptGroupRequest(groupID: Int, index: Int){
        
        let params = ["GroupID":groupID,"GroupjoinResponse":2]
        
        WebServices().callUserService(service: UserServices.groupInvite, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    DispatchQueue.main.async {
                        
//                        self.notificationList.data = []
//                        self.totalNotificationCount = 1
//                        self.getNotificationList(Skip: 0, Take: self.noOfRecordPerRequest, lazyLoading: true)
                        self.markReadApi(notifId: self.notificationList.data[index].NotificationID!, notifType: self.notificationList.data[index].NotificationType!)
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func acceptGroupJoinRequest(groupID: Int, index: Int){
        
        let params = ["GroupID":groupID,"GroupjoinResponse":2,"userProfileID":notificationList.data[index].UserProfileID]
        
        WebServices().callUserService(service: UserServices.groupJoinRequest, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    DispatchQueue.main.async {
                        
//                        self.notificationList.data = []
//                        self.totalNotificationCount = 1
//                        self.getNotificationList(Skip: 0, Take: self.noOfRecordPerRequest, lazyLoading: true)
                        self.markReadApi(notifId: self.notificationList.data[index].NotificationID!, notifType: self.notificationList.data[index].NotificationType!)
                
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func markReadApi(notifId: Int, notifType: String){
        let params = ["NotificationID":notifId,"NotificationType":notifType] as [String : Any]
        WebServices().callUserService(service: UserServices.readNotification, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    self.notificationList.data = []
                    self.totalNotificationCount = 1
                    self.getNotificationList(Skip: 0, Take: self.noOfRecordPerRequest, lazyLoading: true)
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    @objc func btnAcceptAction(_ sender : UIButton)
    {
        if notificationList.data[sender.tag].NotificationType == "SEND_FRIEND_REQUEST"
        {
            acceptFriendRequest(reqId: notificationList.data[sender.tag].FriendRequestID!, reqFrom: notificationList.data[sender.tag].UserProfileID!, index: sender.tag)
        }
        else if notificationList.data[sender.tag].NotificationType == "FOLLOW_USER"
        {
            accepFollowRequest(reqId: notificationList.data[sender.tag].FollowID!, reqFrom: notificationList.data[sender.tag].UserProfileID!, index: sender.tag)
        }
        else if notificationList.data[sender.tag].NotificationType == "GROUP_INVITATION"
        {
//            accepFollowRequest(reqId: notificationList.data[sender.tag].FriendRequestID!, reqFrom: notificationList.data[sender.tag].UserProfileID!)
            acceptGroupRequest(groupID: notificationList.data[sender.tag].GroupID!, index: sender.tag)
        }
        else if notificationList.data[sender.tag].NotificationType == "USER_GROUP_JOIN_REQUEST"
        {
            acceptGroupJoinRequest(groupID: notificationList.data[sender.tag].GroupID!, index: sender.tag)
        }
    }
    
    @objc func tapActionForLabel(_ recognizer : UITapGestureRecognizer)
    {
         if recognizer.state == UIGestureRecognizer.State.ended {
            print(recognizer.view!.tag)
            if currentView == notificationView{
                if notificationList.data[recognizer.view!.tag].NotificationType == "GROUP_INVITATION"{
                    print(notificationList.data[recognizer.view!.tag].GroupName!)
                    let storyboard = UIStoryboard(name: "GroupSB", bundle: nil)
                    let groupDetailsVC = storyboard.instantiateViewController(withIdentifier: "GroupDetailsVC") as! GroupDetailsVC
                    groupDetailsVC.groupId = notificationList.data[recognizer.view!.tag].GroupID!
                    groupDetailsVC.groupPrivacy = notificationList.data[recognizer.view!.tag].GroupPrivacy!
                    groupDetailsVC.groupName = notificationList.data[recognizer.view!.tag].GroupName!
                    self.navigationController?.pushViewController(groupDetailsVC, animated: true)
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch currentView{
        case notificationView:
            if notificationList.data[indexPath.row].NotificationType == "SEND_FRIEND_REQUEST" || notificationList.data[indexPath.row].NotificationType == "FOLLOW_USER" || notificationList.data[indexPath.row].NotificationType == "GROUP_INVITATION" || notificationList.data[indexPath.row].NotificationType ==  "USER_GROUP_JOIN_REQUEST"
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "RequestApprovalTVC", for: indexPath) as! RequestApprovalTVC
                cell.imgProfile.sd_setImage(with: URL(string: notificationList.data[indexPath.row].ProfileImage ?? ""), placeholderImage: UIImage(named: "img_selectProfilePic"), options: [.continueInBackground], completed: nil)
                cell.imgProfile.tag = indexPath.row
                let imageTapGesture = UITapGestureRecognizer(target: self, action: #selector(showZoomedImage))
                cell.imgProfile.addGestureRecognizer(imageTapGesture)
                cell.imgProfile.isUserInteractionEnabled = true
                
                cell.btnDecline.tag = indexPath.row
                cell.btnDecline.addTarget(self, action: #selector(btnDeclineAction(_:)), for: .touchUpInside)
                
                cell.btnAccept.tag = indexPath.row
                cell.btnAccept.addTarget(self, action: #selector(btnAcceptAction(_:)), for: .touchUpInside)
                
                let personName = NSAttributedString(string: "\(notificationList.data[indexPath.row].Firstname ?? "") \(notificationList.data[indexPath.row].Lastname ?? "")", attributes: [NSAttributedString.Key.font:UIFont(name: "OpenSans-Semibold", size: CGFloat(fontSize))!])
                
                let personAction = NSAttributedString(string: " \(notificationList.data[indexPath.row].NotificationText ?? "")", attributes: [NSAttributedString.Key.font:UIFont(name: "OpenSans-Light", size: CGFloat(fontSize))!])
                
                
                cell.lblName.tag = indexPath.row
                cell.profileNameTap.numberOfTapsRequired = 1
                cell.lblName.isUserInteractionEnabled = true
                cell.profileNameTap = UITapGestureRecognizer(target: self, action: #selector(tapActionForLabel(_:)))
                cell.lblName.addGestureRecognizer(cell.profileNameTap)
                
                let combination = NSMutableAttributedString()
                combination.append(personName)
                combination.append(personAction)
                if notificationList.data[indexPath.row].NotificationType == "GROUP_INVITATION"
                {
                    let groupName = NSAttributedString(string: " \(notificationList.data[indexPath.row].GroupName ?? "")", attributes: [NSAttributedString.Key.font:UIFont(name: "OpenSans-Semibold", size: CGFloat(fontSize))!])
                    combination.append(groupName)
                }
                
                DispatchQueue.main.async{
                    cell.lblName.attributedText = combination
                }
                cell.setup()
                cell.lblExpiry.text = notificationList.data[indexPath.row].TimeAgo ?? ""
                if notificationList.data[indexPath.row].isRead == false
                {
                    cell.contentView.backgroundColor = UIColor.baseColor
                }
                else
                {
                    cell.contentView.backgroundColor = UIColor.white
                }
                
                return cell
            }
            else
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: NotificationTVC.reuseIdentifier, for: indexPath) as! NotificationTVC
                cell.personImage.sd_setImage(with: URL(string: notificationList.data[indexPath.row].ProfileImage ?? ""), placeholderImage: UIImage(named: "img_selectProfilePic"), options: [.continueInBackground], completed: nil)
                
                let personName = NSAttributedString(string: "\(notificationList.data[indexPath.row].Firstname ?? "") \(notificationList.data[indexPath.row].Lastname ?? "")", attributes: [NSAttributedString.Key.font:UIFont(name: "OpenSans-Semibold", size: CGFloat(fontSize))!])
                
                let personAction = NSAttributedString(string: "\(notificationList.data[indexPath.row].NotificationText ?? "")", attributes: [NSAttributedString.Key.font:UIFont(name: "OpenSans-Light", size: CGFloat(fontSize))!])

                let combination = NSMutableAttributedString()
                combination.append(personName)
                combination.append(personAction)
                DispatchQueue.main.async{
                    cell.personNameLbl.attributedText = combination
                }
                
                cell.timeLbl.text = notificationList.data[indexPath.row].TimeAgo ?? ""
                cell.personImage.tag = indexPath.row
                let imageTapGesture = UITapGestureRecognizer(target: self, action: #selector(showZoomedImage))
                cell.personImage.addGestureRecognizer(imageTapGesture)
                cell.personImage.isUserInteractionEnabled = true
                cell.setup()
                
                if notificationList.data[indexPath.row].isRead == false
                {
                    cell.contentView.backgroundColor = UIColor.baseColor
                }
                else
                {
                    cell.contentView.backgroundColor = UIColor.white
                }
                return cell
            }
        case messageView:
            let cell = tableView.dequeueReusableCell(withIdentifier: NotificationTVC.reuseIdentifier, for: indexPath) as! NotificationTVC
            
            cell.personImage.sd_setImage(with: URL(string: messageNotificationList.data[indexPath.row].ProfileImage ?? ""), placeholderImage: UIImage(named: "img_selectProfilePic"), options: [.continueInBackground], completed: nil)
            cell.timeLbl.text = messageNotificationList.data[indexPath.row].TimeAgo ?? ""
            let personName = NSAttributedString(string: "\(messageNotificationList.data[indexPath.row].Firstname ?? "") \(messageNotificationList.data[indexPath.row].Lastname ?? "")", attributes: [NSAttributedString.Key.font:UIFont(name: "OpenSans-Semibold", size: CGFloat(fontSize))!])
            
            var media = ""
            if messageNotificationList.data[indexPath.row].MediaType != nil
            {
                media = " sent a \(String(describing: messageNotificationList.data[indexPath.row].MediaType!))"
            }
            else{
                media = " sent a text"
            }
            
            
            let personAction = NSAttributedString(string: media, attributes: [NSAttributedString.Key.font:UIFont(name: "OpenSans-Light", size: CGFloat(fontSize))!])
            
            let combination = NSMutableAttributedString()
            combination.append(personName)
            combination.append(personAction)
            DispatchQueue.main.async{
                cell.personNameLbl.attributedText = combination
            }
            
            cell.timeLbl.text = "1 hour ago"
            cell.personImage.tag = indexPath.row
            let imageTapGesture = UITapGestureRecognizer(target: self, action: #selector(showZoomedImage))
            cell.personImage.addGestureRecognizer(imageTapGesture)
            cell.personImage.isUserInteractionEnabled = true
            cell.setup()
            cell.contentView.backgroundColor = UIColor.white
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: NotificationTVC.reuseIdentifier, for: indexPath) as! NotificationTVC
            return cell
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch currentView{
        case notificationView:
            if indexPath.row == (notificationList.data.count - 1){
                getNotificationList(Skip: notificationList.data.count, Take: noOfRecordPerRequest, lazyLoading: true)
            }
        case messageView:
            if indexPath.row == (messageNotificationList.data.count - 1){
                getMessagesList(Skip: messageNotificationList.data.count, Take: noOfRecordPerRequest, lazyLoading: true)
            }
            return
        default:
            if indexPath.row == (notificationList.data.count - 1){
                getNotificationList(Skip: notificationList.data.count, Take: noOfRecordPerRequest, lazyLoading: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//            let cell = tableView.cellForRow(at: indexPath)
//            cell?.contentView.backgroundColor = UIColor.baseColor
        if currentView == messageView
        {
            let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
            let nextViewController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
            nextViewController.ToUserId = self.messageNotificationList.data[indexPath.row].UserProfileID!
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
        else
        {
            if notificationList.data[indexPath.row].isRead == false && notificationList.data[indexPath.row].NotificationType != nil && notificationList.data[indexPath.row].NotificationType! != "SEND_FRIEND_REQUEST" && notificationList.data[indexPath.row].NotificationType != "FOLLOW_USER" && notificationList.data[indexPath.row].NotificationType! != "GROUP_INVITATION" && notificationList.data[indexPath.row].NotificationType! != "USER_GROUP_JOIN_REQUEST"
            {
            let params = ["NotificationID":notificationList.data[indexPath.row].NotificationID!,"NotificationType":notificationList.data[indexPath.row].NotificationType!] as [String : Any]
            WebServices().callUserService(service: UserServices.readNotification, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
                
                DispatchQueue.main.async {
                    self.notificationList.data[indexPath.row].isRead = true
                    let cell = tableView.cellForRow(at: indexPath)
                    cell?.contentView.backgroundColor = UIColor.clear
                if serviceResponse["Status"] != nil{
                    if (serviceResponse["Status"] as! Bool == true){
                            switch self.notificationList.data[indexPath.row].NotificationType{
                            case "TREND_LIKE_DISLIKE":
                                print("Trend liked")
                                let storyboard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
                                let destination = storyboard.instantiateViewController(withIdentifier: "TravelTrendViewController") as! TravelTrendViewController
                                destination.trendId = self.notificationList.data[indexPath.row].TrendId!
                                self.navigationController?.pushViewController(destination, animated: true)
                                
                            case "COMMENT_ADD":
                                print("Trend Comment")
                                let storyboard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
                                let destination = storyboard.instantiateViewController(withIdentifier: "TravelTrendViewController") as! TravelTrendViewController
                                destination.trendId = self.notificationList.data[indexPath.row].TrendId!
                                self.navigationController?.pushViewController(destination, animated: true)
                                
                            case "TREND_COMMENT_REPLY":
                                print("Trend Comment reply")
                                let storyboard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
                                let destination = storyboard.instantiateViewController(withIdentifier: "TravelTrendViewController") as! TravelTrendViewController
                                destination.trendId = self.notificationList.data[indexPath.row].TrendId!
                                self.navigationController?.pushViewController(destination, animated: true)
                                
                            case "TREND_LIKE_COMMENT":
                                print("Trend Comment like")
                                let storyboard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
                                let destination = storyboard.instantiateViewController(withIdentifier: "TravelTrendViewController") as! TravelTrendViewController
                                destination.trendId = self.notificationList.data[indexPath.row].TrendId!
                                self.navigationController?.pushViewController(destination, animated: true)
                                
                            case "TREND_AUDIO_COMMENT":
                                print("Trend Comment audio")
                                let storyboard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
                                let destination = storyboard.instantiateViewController(withIdentifier: "TravelTrendViewController") as! TravelTrendViewController
                                destination.trendId = self.notificationList.data[indexPath.row].TrendId!
                                self.navigationController?.pushViewController(destination, animated: true)
                                
                            case "SHARE_TREND":
                                print("Trend share")
                                let storyboard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
                                let destination = storyboard.instantiateViewController(withIdentifier: "TravelTrendViewController") as! TravelTrendViewController
                                destination.trendId = self.notificationList.data[indexPath.row].TrendId!
                                self.navigationController?.pushViewController(destination, animated: true)
                                
                            case "TREND_SHARED_TAG_FRIENDS":
                                print("Trend share")
                                let storyboard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
                                let destination = storyboard.instantiateViewController(withIdentifier: "TravelTrendViewController") as! TravelTrendViewController
                                destination.trendId = self.notificationList.data[indexPath.row].TrendId!
                                self.navigationController?.pushViewController(destination, animated: true)
                                
                            case "TAG_TREND_TRENDERS":
                                print("Tag Trenders")
                                let storyboard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
                                let destination = storyboard.instantiateViewController(withIdentifier: "TravelTrendViewController") as! TravelTrendViewController
                                destination.trendId = self.notificationList.data[indexPath.row].TrendId!
                                self.navigationController?.pushViewController(destination, animated: true)
                                
                            case "CONTEST_CREATE":
                                let storyboard = UIStoryboard(name: "MonthlyContestSB", bundle: nil)
                                let paymentVC = storyboard.instantiateViewController(withIdentifier: "MonthlyContestVC") as! MonthlyContestVC
                                self.navigationController?.pushViewController(paymentVC, animated: true)
                                
                                //            case "SEND_FRIEND_REQUEST":
                                //                print("Friend Request")
                                //                let storyboard = UIStoryboard(name: "NotificationSB", bundle: nil)
                                //                let destination = storyboard.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
                                //                viewCntrler?.pushViewController(destination, animated: true)
                                //                //            self.performSegue(withIdentifier: "DashboardToNotification", sender: nil)
                                //
                                //            case "FOLLOW_USER":
                                //                print("Follow Request")
                                //                let storyboard = UIStoryboard(name: "NotificationSB", bundle: nil)
                                //                let destination = storyboard.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
                                //                viewCntrler?.pushViewController(destination, animated: true)
                                //
                                //            case "GROUP_INVITATION":
                                //                print("Group Invite")
                                //                let storyboard = UIStoryboard(name: "NotificationSB", bundle: nil)
                                //                let destination = storyboard.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
                                //                viewCntrler?.pushViewController(destination, animated: true)
                                
                            default:
                                break
                            }
                    }else{
                        TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
              }
            }

            }
            else
            {
                switch self.notificationList.data[indexPath.row].NotificationType{
                case "TREND_LIKE_DISLIKE":
                    print("Trend liked")
                    let storyboard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
                    let destination = storyboard.instantiateViewController(withIdentifier: "TravelTrendViewController") as! TravelTrendViewController
                    destination.trendId = self.notificationList.data[indexPath.row].TrendId!
                    self.navigationController?.pushViewController(destination, animated: true)
                    
                case "COMMENT_ADD":
                    print("Trend Comment")
                    let storyboard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
                    let destination = storyboard.instantiateViewController(withIdentifier: "TravelTrendViewController") as! TravelTrendViewController
                    destination.trendId = self.notificationList.data[indexPath.row].TrendId!
                    self.navigationController?.pushViewController(destination, animated: true)
                    
                case "TREND_COMMENT_REPLY":
                    print("Trend Comment reply")
                    let storyboard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
                    let destination = storyboard.instantiateViewController(withIdentifier: "TravelTrendViewController") as! TravelTrendViewController
                    destination.trendId = self.notificationList.data[indexPath.row].TrendId!
                    self.navigationController?.pushViewController(destination, animated: true)
                    
                case "TREND_LIKE_COMMENT":
                    print("Trend Comment like")
                    let storyboard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
                    let destination = storyboard.instantiateViewController(withIdentifier: "TravelTrendViewController") as! TravelTrendViewController
                    destination.trendId = self.notificationList.data[indexPath.row].TrendId!
                    self.navigationController?.pushViewController(destination, animated: true)
                    
                case "TREND_AUDIO_COMMENT":
                    print("Trend Comment audio")
                    let storyboard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
                    let destination = storyboard.instantiateViewController(withIdentifier: "TravelTrendViewController") as! TravelTrendViewController
                    destination.trendId = self.notificationList.data[indexPath.row].TrendId!
                    self.navigationController?.pushViewController(destination, animated: true)
                    
                case "SHARE_TREND":
                    print("Trend share")
                    let storyboard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
                    let destination = storyboard.instantiateViewController(withIdentifier: "TravelTrendViewController") as! TravelTrendViewController
                    destination.trendId = self.notificationList.data[indexPath.row].TrendId!
                    self.navigationController?.pushViewController(destination, animated: true)
                    
                case "TREND_SHARED_TAG_FRIENDS":
                    print("Trend share")
                    let storyboard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
                    let destination = storyboard.instantiateViewController(withIdentifier: "TravelTrendViewController") as! TravelTrendViewController
                    destination.trendId = self.notificationList.data[indexPath.row].TrendId!
                    self.navigationController?.pushViewController(destination, animated: true)
                    
                case "TAG_TREND_TRENDERS":
                    print("Tag Trenders")
                    let storyboard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
                    let destination = storyboard.instantiateViewController(withIdentifier: "TravelTrendViewController") as! TravelTrendViewController
                    destination.trendId = self.notificationList.data[indexPath.row].TrendId!
                    self.navigationController?.pushViewController(destination, animated: true)
                    
                case "CONTEST_CREATE":
                    let storyboard = UIStoryboard(name: "MonthlyContestSB", bundle: nil)
                    let paymentVC = storyboard.instantiateViewController(withIdentifier: "MonthlyContestVC") as! MonthlyContestVC
                    self.navigationController?.pushViewController(paymentVC, animated: true)
                    
                default:
                    break
                }
            }
       }
    }
}
