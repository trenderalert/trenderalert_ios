//
//  NotificationModel.swift
//  TrenderAlert
//
//  Created by HPL on 11/03/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation


class NotificationModel:NSObject,Codable{

    var NotificationID:Int?
    var UserProfileID:Int?
    var Firstname:String?
    var Lastname:String?
    var NotificationText:String?
    var ProfileImage:String?
    var TimeAgo:String?
    var GroupID: Int?
    var NotificationType : String?
    var TrendId : Int?
    var FriendRequestID : Int?
    var FollowID : Int?
    var isRead = false
    var GroupName:String?
    var GroupPrivacy: Int?
    
    enum CodingKeys:String,CodingKey{
        case NotificationID = "NotificationID"
        case UserProfileID = "UserProfileID"
        case Firstname = "Firstname"
        case Lastname = "Lastname"
        case NotificationText = "NotificationText"
        case ProfileImage = "ProfileImage"
        case TimeAgo = "TimeAgo"
        case GroupID = "GroupID"
        case NotificationType = "NotificationType"
        case TrendId = "TrendId"
        case FriendRequestID = "FriendRequestID"
        case FollowID = "FollowID"
        case isRead = "isRead"
        case GroupName = "GroupName"
        case GroupPrivacy = "GroupPrivacy"
  }
    
}

class NotificationList:NSObject,Codable{
    var data = [NotificationModel]()
    
    enum CodingKeys:String,CodingKey{
        case data = "data"
    }
}


class NotificationMessageModel:NSObject,Codable{
    
    var Firstname:String?
    var Lastname:String?
    var MediaPath:String?
    var MediaType:String?
    var ProfileImage:String?
    var TimeAgo: String?
    var UserProfileID : Int?
    
    enum CodingKeys:String,CodingKey{
        case Firstname = "Firstname"
        case Lastname = "Lastname"
        case MediaPath = "MediaPath"
        case MediaType = "MediaType"
        case ProfileImage = "ProfileImage"
        case TimeAgo = "TimeAgo"
        case UserProfileID = "UserProfileID"
    }
}

class NotificationMessagesList:NSObject,Codable{
    var data = [NotificationMessageModel]()
    
    enum CodingKeys:String,CodingKey{
        case data = "data"
    }
}
