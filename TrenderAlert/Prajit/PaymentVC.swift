//
//  PaymentVC.swift
//  TrenderAlert
//
//  Created by HPL on 13/03/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import MBProgressHUD
import ActiveLabel

class PaymentVC: UIViewController {

    @IBOutlet weak var goPremiumBtn: UIButton!
    
    @IBOutlet weak var premiumUserBenefitView: UIView!
    @IBOutlet weak var paymentDetailsView: UIView!
    
    @IBOutlet weak var purchaseDateLbl: UILabel!
    @IBOutlet weak var transactionIdLbl: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTagPreference: UILabel!
    @IBOutlet weak var lblImgStatus: UILabel!
    @IBOutlet weak var lblUploadVdo: UILabel!
    @IBOutlet weak var lblContest: UILabel!
    
    @IBOutlet weak var lblFriendList: UILabel!
    @IBOutlet weak var lblExpireyDays: UILabel!
    
    
    var isBackEnabled : Bool!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblTitle.text = MySingleton.shared.selectedLangData.Premium_Package
        self.lblTagPreference.text = MySingleton.shared.selectedLangData.trend_alert_tag_prefrences_upto_5_tags
        self.lblImgStatus.text = MySingleton.shared.selectedLangData.upload_upto_15_images_status
        self.lblUploadVdo.text = MySingleton.shared.selectedLangData.upload_1_video_with_max_size_256mb
        self.lblExpireyDays.text = MySingleton.shared.selectedLangData.trend_last_upto_90_days
        self.lblFriendList.text = MySingleton.shared.selectedLangData.access_to_your_friend_list_and_groups
        self.lblContest.text = MySingleton.shared.selectedLangData.Ability_to_enter_monthly_contest
        self.goPremiumBtn.setTitle(MySingleton.shared.selectedLangData.get_premium, for: .normal)
        
        if MySingleton.shared.loginObject.IsPremiumUser == "True"{
//            UserDefaults.standard.setValue("TestReceipt", forKey: "PaymentReceipt")
//            UserDefaults.standard.setValue("TestDate", forKey: "PaymentDate")
//            self.showPaymentDetails(transactionId: "", purchaseDate: "")
            self.showPaymentDetails(transactionId: MySingleton.shared.loginObject.ReceiptID, purchaseDate: MySingleton.shared.loginObject.ReceiptDate)
        }else{
            NotificationCenter.default.addObserver(self, selector: #selector(purchaseNotAllowed), name: .purchaseNotAllowed, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(userPremiumSuccess), name: .userPremiumSuccess, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(userPremiumFailed), name: .userPremiumFailed, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(hideLoader), name: .transactionSuccess, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(transactionFailed), name: .transactionFail, object: nil)
            
            NotificationCenter.default.addObserver(self, selector: #selector(hideLoader), name: .productRequestResp, object: nil)
            
            NotificationCenter.default.addObserver(self, selector: #selector(showLoader), name: .purchasing, object: nil)
            
            goPremiumBtn.makeCornerRadius(radius: 6)
            MBProgressHUD.showAdded(to: view, animated: true)
            IAPService.shared.getProducts()
        }
        setupNaviagationbar()
       
        
        // Do any additional setup after loading the view.
//        setupResponseLabel()
//        setupResponseLabel2()
     }
    
    func setupNaviagationbar(){
        
        let leftBarButton = UIButton()
        if isBackEnabled == true
        {
//            leftBarButton.setTitle("BACK", for: .normal)
            leftBarButton.setImage(UIImage(named: "img_BackArrow"), for: .normal)
            leftBarButton.addTarget(self, action: #selector(didTapBack(_:)), for: .touchUpInside)
        }
        else
        {
            leftBarButton.setImage(#imageLiteral(resourceName: "img_MenuBarBtn"), for: .normal)
            leftBarButton.addTarget(self, action: #selector(didTapMenu), for: .touchUpInside)
        }
        let item1 = UIBarButtonItem()
        item1.customView = leftBarButton
        self.navigationItem.leftBarButtonItems = [item1]
        
        Utilities.setNavigationBarWithTitle(viewController: self, title: MySingleton.shared.selectedLangData.Payment_Details)
    }
    
    @objc func didTapBack(_ sender: UIBarButtonItem){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func didTapMenu(_ sender: UIBarButtonItem) {
        if self.menuContainerViewController != nil {
            self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
        }
    }
    

    @objc func showLoader(){
        MBProgressHUD.showAdded(to: view, animated: true)
//        MBProgressHUD.hide(for: view, animated: true)
    }
    
    @objc func hideLoader(){
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    @objc func transactionFailed(){
        self.goPremiumBtn.isEnabled = true
        self.hideLoader()
//        let alertController = UIAlertController(title: "Transaction failed", message: "You can try again later.", preferredStyle:UIAlertController.Style.alert)
//
//        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel)
//        { action -> Void in
//            // Put your code here
//        })
//        self.present(alertController, animated: true, completion: nil)
    }
    
    
    @objc func purchaseNotAllowed(){
        self.goPremiumBtn.isEnabled = true
        self.hideLoader()
        
        let alertController = UIAlertController(title: "Purchase not allowed.", message: "You're not allowed to make purchase on this device.", preferredStyle:UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel)
        { action -> Void in
            // Put your code here
        })
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func userPremiumFailed(_ notification:Notification){
        self.goPremiumBtn.isEnabled = true
        self.hideLoader()
        
        let alertController = UIAlertController(title: "Transaction Details", message: notification.object as! String, preferredStyle:UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel)
        { action -> Void in
            // Put your code here
        })
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func userPremiumSuccess(_ notification:Notification){
        DispatchQueue.main.async {
        self.goPremiumBtn.isEnabled = true
        self.hideLoader()
        let date = Date()
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "MM/dd/yyyy"
        let result = dateformatter.string(from: date)
        print("Notification Data : ",notification.object as! String)
        UserDefaults.standard.setValue(notification.object as! String, forKey: "PaymentReceipt")
        UserDefaults.standard.setValue(result, forKey: "PaymentDate")
             self.showPaymentDetails(transactionId: notification.object as! String, purchaseDate: result)
        }
       
    }
    
    func showPaymentDetails(transactionId:String,purchaseDate:String){
        var transactionIdInFunc = transactionId
        var purchaseDateInFunc  = purchaseDate
        
        if transactionIdInFunc == ""{
            if UserDefaults.standard.value(forKey: "PaymentReceipt") != nil{
                transactionIdInFunc = UserDefaults.standard.value(forKey: "PaymentReceipt") as! String
            }
        }
        
        if purchaseDateInFunc == ""{
            if UserDefaults.standard.value(forKey: "PaymentDate") != nil{
                purchaseDateInFunc = UserDefaults.standard.value(forKey: "PaymentDate") as! String
            }
        }
        
        self.transactionIdLbl.text = "\(MySingleton.shared.selectedLangData.Transaction_ID) : \(transactionIdInFunc)"
        self.purchaseDateLbl.text = "\(MySingleton.shared.selectedLangData.Purchase_Date) : \(purchaseDateInFunc)"
        
        
        
        if MySingleton.shared.loginObject.IsPremiumUser == "True"{
            self.goPremiumBtn.isHidden = true
            self.paymentDetailsView.isHidden = false
        }else{
            self.goPremiumBtn.isHidden = false
        }
//        self.premiumUserBenefitView.isHidden = true
//        self.paymentDetailsView.isHidden = false
    }
    
    
    @IBAction func goPremium(_ sender: UIButton) {
        self.goPremiumBtn.isEnabled = false
//        IAPService.shared.purchase(product: .trenderAlertPremiumConsumable)
        IAPService.shared.purchase(product: .trenderAlertPremiumNonConsumable) // for live
    }
    
    
    @IBAction func consumablePayment(_ sender: UIButton) {
      IAPService.shared.purchase(product: .trenderAlertPremiumConsumable)
    }
    
    @IBAction func nonConsumablePayment(_ sender: UIButton) {
        IAPService.shared.purchase(product: .trenderAlertPremiumNonConsumable)
    }
    
    @IBAction func resetPayment(_ sender: UIButton) {
        IAPService.shared.restorePurchases()
    }
}


extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
        // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: CGSize.zero)
        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        
        // Configure layoutManager and textStorage
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // Configure textContainer
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        let labelSize = label.bounds.size
        textContainer.size = labelSize
        
        // Find the tapped character location and compare it to the specified range
        let locationOfTouchInLabel = self.location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        
        let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        return NSLocationInRange(indexOfCharacter, targetRange)
    }
    
}

extension Range where Bound == String.Index {
    var nsRange:NSRange {
        return NSRange(location: self.lowerBound.encodedOffset,
                       length: self.upperBound.encodedOffset -
                        self.lowerBound.encodedOffset)
    }
}
