//
//  InviteContactsVC.swift
//  TrenderAlert
//
//  Created by HPL on 15/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class InviteContactsVC: UIViewController {

    
    @IBOutlet weak var lblJoin: UILabel!
    @IBOutlet weak var lblInvite: UILabel!
    @IBOutlet weak var lblInfo: UILabel!
    @IBOutlet weak var centerImage: UIImageView!
    @IBOutlet weak var inviteContactsTable: UITableView!
    
    let buttons = [MySingleton.shared.selectedLangData.Get_Started]
    
    override func viewDidLoad() {
        super.viewDidLoad()
       setupTableView()
        setupNavigationBar()
        self.lblInvite.text = MySingleton.shared.selectedLangData.invite_friends
        self.lblJoin.text = MySingleton.shared.selectedLangData.invite_join
        self.lblInfo.text = MySingleton.shared.selectedLangData.invite_contact_info
     }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         centerImage.makeCornerRadius(radius: centerImage.frame.width/2)
    }
    
    func setupTableView(){
        inviteContactsTable.register(UINib(nibName: InviteContactsTVC.reuseIdentifier, bundle: nil), forCellReuseIdentifier: InviteContactsTVC.reuseIdentifier)
        inviteContactsTable.delegate = self
        inviteContactsTable.dataSource = self
    }
    
    func setupNavigationBar(){
        
        let leftBarButton = UIButton()
        leftBarButton.setImage(#imageLiteral(resourceName: "img_MenuBarBtn"), for: .normal)
        leftBarButton.addTarget(self, action: #selector(didTapMenu), for: .touchUpInside)
        let item1 = UIBarButtonItem()
        item1.customView = leftBarButton
        self.navigationItem.leftBarButtonItems = [item1]
        
        
        
        //        Utilities.setNavigationBar(viewController:self)
        Utilities.setNavigationBarWithTitle(viewController: self, title: MySingleton.shared.selectedLangData.invite_contacts)
        
        //        let leftButton = UIBarButtonItem(image: UIImage(named: "img_BackArrow"), style: .plain, target: self, action: #selector(goBack))
        
    }
    
    @objc func didTapMenu(_ sender: UIBarButtonItem) {
        if self.menuContainerViewController != nil {
            self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
        }
    }
    
    @objc func btnOptionButtonAction(_ sender : UIButton)
    {
        if sender.tag == 0
        {
            let someText:String = "You have been invited to Trender Alert, please install the app from link below"
            let objectsToShare:URL = URL(string: "http://www.trenderalert.com")!
            let sharedObjects:[AnyObject] = [someText as AnyObject, objectsToShare as AnyObject]
            let activityViewController = UIActivityViewController(activityItems : sharedObjects, applicationActivities: nil)
            activityViewController.popoverPresentationController?.sourceView = self.view
            
            activityViewController.excludedActivityTypes = nil
            
            self.present(activityViewController, animated: true, completion: nil)
        }
    }

}

extension InviteContactsVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return buttons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: InviteContactsTVC.reuseIdentifier, for: indexPath) as! InviteContactsTVC
        cell.optionButton.setTitle(buttons[indexPath.row], for: .normal)
        cell.optionButton.tag = indexPath.row
        cell.optionButton.addTarget(self, action: #selector(btnOptionButtonAction(_:)), for: .touchUpInside)
        if buttons[indexPath.row] == buttons.first{
            cell.optionButton.setTitleColor(UIColor.baseColor, for: .normal)
        }
        return cell
    }
    
   
}
