//
//  InviteContactListTVC.swift
//  TrenderAlert
//
//  Created by HPL on 15/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class InviteContactListTVC: UITableViewCell {

    @IBOutlet weak var contactImage: UIImageView!
    @IBOutlet weak var contactNameLbl: UILabel!
    @IBOutlet weak var contactNumberLbl: UILabel!
    @IBOutlet weak var inviteBtn: UIButton!
    
    static let reuseIdentifier = "InviteContactListTVC"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(){
        DispatchQueue.main.async{
            self.contactImage.makeCornerRadius(radius: self.contactImage.frame.height/2)
        }
    }
    
}
