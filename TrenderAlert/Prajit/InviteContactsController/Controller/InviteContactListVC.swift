//
//  InviteContactListVC.swift
//  TrenderAlert
//
//  Created by HPL on 15/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class InviteContactListVC: UIViewController {

    @IBOutlet weak var inviteContactListTable: UITableView!
    
    let contactDetails = [["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"],
                          ["contest_img_2-1","Mike Velera","465-454-548"]]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
    }
    
    func setupTableView(){
        inviteContactListTable.dataSource = self
        inviteContactListTable.delegate = self
        
        inviteContactListTable.register(UINib(nibName: InviteContactListTVC.reuseIdentifier, bundle: nil), forCellReuseIdentifier: InviteContactListTVC.reuseIdentifier)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension InviteContactListVC:UITableViewDataSource,UITableViewDelegate{
    
    @objc func showZoomedImage(_ sender:UITapGestureRecognizer){
        var imageUrl = ""
        let zoomImageView = ZoomImageVC(nibName: "ZoomImageVC", bundle: nil)
        zoomImageView.imageUrl = imageUrl
        zoomImageView.defaultImageName = "contest_img_2"
        self.navigationController?.pushViewController(zoomImageView, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: InviteContactListTVC.reuseIdentifier, for: indexPath) as! InviteContactListTVC
        cell.contactImage.image = UIImage(named: contactDetails[indexPath.row][0])
        cell.contactNameLbl.text = contactDetails[indexPath.row][1]
        cell.contactNumberLbl.text  = contactDetails[indexPath.row][2]
        cell.contactImage.tag = indexPath.row
        let imageTapGesture = UITapGestureRecognizer(target: self, action: #selector(showZoomedImage))
        cell.contactImage.isUserInteractionEnabled = true
        cell.addGestureRecognizer(imageTapGesture)
        cell.setup()
        return cell
    }
}
