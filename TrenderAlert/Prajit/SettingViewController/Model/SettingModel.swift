//
//  SettingModel.swift
//  TrenderAlert
//
//  Created by HPL on 15/03/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

class SettingModel:NSObject,Codable{
    
    var AlertSettingId:Int?
    var GetNotificationFromEveryone:Bool = false
    var OnlyPeopleYouFollow:Bool = false
    var OnlyPeopleFollowingYou:Bool = false
    var LikesYourTrends:Bool = false
    var FollowsYourTrend:Bool = false
    var CommentsOnYourTrend:Bool = false
    var AudioChat:Bool = false
    var TextChat:Bool = false
    var ReplyOnYourComment:Bool = false
    var LikeYourComment:Bool = false
    var LikeYourReply:Bool = false
    
    
    enum CodingKeys:String,CodingKey{
        case AlertSettingId = "AlertSettingId"
        case GetNotificationFromEveryone = "GetNotificationFromEveryone"
        case OnlyPeopleYouFollow = "OnlyPeopleYouFollow"
        case OnlyPeopleFollowingYou = "OnlyPeopleFollowingYou"
        case LikesYourTrends = "LikesYourTrends"
        case FollowsYourTrend = "FollowsYourTrend"
        case CommentsOnYourTrend = "CommentsOnYourTrend"
        case AudioChat = "AudioChat"
        case TextChat = "TextChat"
        case ReplyOnYourComment = "ReplyOnYourComment"
        case LikeYourComment = "LikeYourComment"
        case LikeYourReply = "LikeYourReply"
    }
}

class Setting:NSObject,Codable{
    var data = [SettingModel]()
    
    enum CodingKeys:String,CodingKey{
        case data = "data"
    }
}
