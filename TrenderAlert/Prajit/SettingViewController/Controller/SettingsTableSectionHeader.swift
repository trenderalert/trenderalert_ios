//
//  SettingsTableHeader.swift
//  TrenderAlert
//
//  Created by HPL on 17/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class SettingsTableSectionHeader: UITableViewHeaderFooterView {
    
    @IBOutlet weak var settingsHeaderLbl: UILabel!
    
    
    
    static let reuseIdentifier = "SettingsTableSectionHeader"
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
