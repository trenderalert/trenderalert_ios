//
//  SaveSettingsTVC.swift
//  TrenderAlert
//
//  Created by HPL on 17/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class SaveSettingsTVC: UITableViewCell {

    static let reuseIdentifier = "SaveSettingsTVC"
    @IBOutlet weak var saveSettingBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
