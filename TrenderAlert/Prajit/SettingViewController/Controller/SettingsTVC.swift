//
//  SettingsTVC.swift
//  TrenderAlert
//
//  Created by HPL on 17/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class AlertSettingSwitch:UISwitch{
    var row:Int?
    var section:Int?
}

class SettingsTVC: UITableViewCell {

    
    @IBOutlet weak var settingTextLbl: UILabel!
//    @IBOutlet weak var settingSwitch: UISwitch!
    @IBOutlet weak var settingSwitch: AlertSettingSwitch!
    
    
    
    static let reuseIdentifier = "SettingsTVC"
    let fontSize = (UIDevice.current.userInterfaceIdiom == .pad) ? 18 : 13
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupSettingOption(data:settingStorage){
        
        if data.switchRequired{
            settingSwitch.isHidden = false
        }else{
            settingSwitch.isHidden = true
        }
        
        if data.switchState{
            self.settingSwitch.isOn = true
        }else{
           self.settingSwitch.isOn = false
        }
        
        if data.isSecondaryOpt{
            
//            settingTextLbl.font = UIFont(name: "OpenSans-Semibold", size: CGFloat(20))
            settingTextLbl.attributedText = NSAttributedString(string: data.text, attributes: [NSAttributedString.Key.font:UIFont(name:"Open Sans Light", size: CGFloat(fontSize)) ?? UIFont.systemFont(ofSize: CGFloat(fontSize)),NSAttributedString.Key.foregroundColor:UIColor.darkGray])
            
            //,NSAttributedString.Key.foregroundColor:UIColor.gray
        }else{
            settingTextLbl.text = data.text
        }
        
    }
    
    
}
