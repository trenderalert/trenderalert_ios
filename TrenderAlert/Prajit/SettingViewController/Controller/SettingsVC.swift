//
//  SettingsVC.swift
//  TrenderAlert
//
//  Created by HPL on 17/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit


class settingStorage{
    
    var text:String = ""
    var switchRequired:Bool = true
    var isSecondaryOpt = false
    var switchState = false
    var key:String = ""
    
    init(text:String,switchState:Bool,isSecondaryOpt:Bool,switchRequired:Bool,key:SettingModel.CodingKeys) {
        self.text = text
        self.switchRequired = switchState
        self.isSecondaryOpt = isSecondaryOpt
        self.switchState = switchState
        self.switchRequired = switchRequired
        self.key = key.rawValue
    }
}


class SettingsVC: UIViewController {

    @IBOutlet weak var settingsTable: UITableView!
    
    
    let settingSections = ["When on Trender Alert:","Get a heads up /notify when someone:","Send a message:"]
    
    var getNotificationFromEveryone = true
    
//    var settingOptions = [[["text":"Get notification from everyone","switchRequired":true,"isSecondaryOpt":false],["text":"Only people you follow","switchRequired":true,"isSecondaryOpt":false],
//        ["text":"Only people following you","switchRequired":true,"isSecondaryOpt":false]],
//        [["text":"Likes your trends","switchRequired":true,"isSecondaryOpt":false],["text":"Follows your trends","switchRequired":true,"isSecondaryOpt":false],["text":"Comments on your trends","switchRequired":true,"isSecondaryOpt":false],["text":"Reply to your comment","switchRequired":true,"isSecondaryOpt":false]],[["text":"Turn on","switchRequired":false,"isSecondaryOpt":false],["text":"Audio Chat","switchRequired":true,"isSecondaryOpt":true],["text":"Text Chat","switchRequired":true,"isSecondaryOpt":true]]]
    
    var settingOptions = [[settingStorage(text: "Get notification from everyone", switchState: true, isSecondaryOpt: false,switchRequired:true,key:SettingModel.CodingKeys.GetNotificationFromEveryone),
        settingStorage(text: "Only people you follow", switchState: true, isSecondaryOpt: false,switchRequired:true,key:SettingModel.CodingKeys.OnlyPeopleYouFollow),
        settingStorage(text: "Only people following you", switchState: true, isSecondaryOpt: false,switchRequired:true,key:SettingModel.CodingKeys.OnlyPeopleFollowingYou)
        ],
        [settingStorage(text: "Likes your trends", switchState: true, isSecondaryOpt: false,switchRequired:true,key:SettingModel.CodingKeys.LikesYourTrends),
         settingStorage(text: "Follows your trends", switchState: true, isSecondaryOpt: false,switchRequired:true,key:SettingModel.CodingKeys.FollowsYourTrend),
         settingStorage(text: "Comments on your trends", switchState: true, isSecondaryOpt: false,switchRequired:true,key:SettingModel.CodingKeys.CommentsOnYourTrend),
         settingStorage(text: "Like your comment", switchState: true, isSecondaryOpt: false,switchRequired:true,key:SettingModel.CodingKeys.LikeYourComment),
         settingStorage(text: "Like your reply", switchState: true, isSecondaryOpt: false,switchRequired:true,key:SettingModel.CodingKeys.LikeYourReply),
         settingStorage(text: "Reply to your comment", switchState: true, isSecondaryOpt: false,switchRequired:true,key:SettingModel.CodingKeys.ReplyOnYourComment)
         ],
        [settingStorage(text: "Turn on", switchState: true, isSecondaryOpt: false,switchRequired:false,key:SettingModel.CodingKeys.AudioChat),
         settingStorage(text: "Audio Chat", switchState: true, isSecondaryOpt: true,switchRequired:true,key:SettingModel.CodingKeys.AudioChat),
         settingStorage(text: "Text Chat", switchState: true, isSecondaryOpt: true,switchRequired:true,key:SettingModel.CodingKeys.TextChat)
         ]
        ]
    
    var SettingApiResp = Setting()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationbar()
        setupTableView()
        getSetting()
    }
    
    func setupNavigationbar(){
        let leftBarButton = UIButton()
        leftBarButton.setImage(#imageLiteral(resourceName: "img_MenuBarBtn"), for: .normal)
        leftBarButton.addTarget(self, action: #selector(didTapMenu), for: .touchUpInside)
        let item1 = UIBarButtonItem()
        item1.customView = leftBarButton
        self.navigationItem.leftBarButtonItems = [item1]
        Utilities.setNavigationBarWithTitle(viewController: self, title: "Settings")
    }
    
    @objc func didTapMenu(_ sender: UIBarButtonItem) {
        if self.menuContainerViewController != nil {
            self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
        }
    }
    
    func setupTableView(){
        settingsTable.dataSource = self
        settingsTable.delegate = self
        
        settingsTable.estimatedSectionHeaderHeight = 50
        settingsTable.estimatedRowHeight = 50
        
        settingsTable.tableFooterView = UIView()
        
        settingsTable.register(UINib(nibName: SettingsTVC.reuseIdentifier, bundle: nil), forCellReuseIdentifier: SettingsTVC.reuseIdentifier)
        settingsTable.register(UINib(nibName: SettingsTableSectionHeader.reuseIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: SettingsTableSectionHeader.reuseIdentifier)
        
        settingsTable.register(UINib(nibName: SaveSettingsTVC.reuseIdentifier, bundle: nil), forCellReuseIdentifier: SaveSettingsTVC.reuseIdentifier)
    }
    
    func getSetting(){
        WebServices().callUserService(service: UserServices.getSettingInfo, urlParameter: "", parameters: [:], isLazyLoading: false, isHeader: true, CallMethod: .get){ (serviceResponse, serviceData) in
            if serviceResponse["Status"] != nil,(serviceResponse["Status"] as! Bool == true)
            {
                DispatchQueue.main.async {
               
                self.SettingApiResp = WebServices().decodeDataToClass(data: serviceData, decodeClass: Setting.self)!
                
                self.settingOptions[0][0].switchState = self.SettingApiResp.data[0].GetNotificationFromEveryone
                self.settingOptions[0][1].switchState = self.SettingApiResp.data[0].OnlyPeopleYouFollow
                self.settingOptions[0][2].switchState = self.SettingApiResp.data[0].OnlyPeopleFollowingYou
                self.settingOptions[1][0].switchState = self.SettingApiResp.data[0].LikesYourTrends
                self.settingOptions[1][1].switchState = self.SettingApiResp.data[0].FollowsYourTrend
                self.settingOptions[1][2].switchState = self.SettingApiResp.data[0].CommentsOnYourTrend
                self.settingOptions[1][3].switchState = self.SettingApiResp.data[0].LikeYourComment
                self.settingOptions[1][4].switchState = self.SettingApiResp.data[0].LikeYourReply
                self.settingOptions[1][5].switchState = self.SettingApiResp.data[0].ReplyOnYourComment
//                self.settingOptions[2][0].switchState = self.SettingApiResp.data[0].GetNotificationFromEveryone // Turn on
                self.settingOptions[2][1].switchState = self.SettingApiResp.data[0].AudioChat
                self.settingOptions[2][2].switchState = self.SettingApiResp.data[0].TextChat
                self.settingsTable.reloadData()
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
}



extension SettingsVC:UITableViewDataSource,UITableViewDelegate{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return settingSections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (section == (settingOptions.count - 1)) ? (settingOptions[section].count + 1) : settingOptions[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.section == (settingOptions.count - 1)), (indexPath.row == ((settingOptions[indexPath.section] ).count)){ // Last Row
            let cell = tableView.dequeueReusableCell(withIdentifier: SaveSettingsTVC.reuseIdentifier, for: indexPath) as! SaveSettingsTVC
            cell.saveSettingBtn.addTarget(self, action: #selector(saveSettingOptions), for: .touchUpInside)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: SettingsTVC.reuseIdentifier, for: indexPath) as! SettingsTVC
//            cell.settingSwitch.tag = indexPath.row
            cell.settingSwitch.row = indexPath.row
            cell.settingSwitch.section = indexPath.section
            cell.setupSettingOption(data: settingOptions[indexPath.section][indexPath.row])
            cell.settingSwitch.addTarget(self, action: #selector(switchValueChanged(_:)), for: .valueChanged)
            return cell
        }
    }
    
    @objc func saveSettingOptions(){
        var param:[String:String] = [:]
        
        for settingSection in self.settingOptions{
            for setting in settingSection{
                if setting.switchRequired{
                    param[setting.key] = (setting.switchState) ? "true" : "false"
                }
            }
        }
        
        print("Parameters : ",param)
        
        WebServices().callUserService(service: UserServices.saveSettingInfo, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){ (serviceResponse, serviceData) in
            if serviceResponse["Status"] != nil,(serviceResponse["Status"] as! Bool == true)
            {

            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }

        }
    }
    
    @objc func switchValueChanged(_ sender:AlertSettingSwitch){
//        print("row: ",sender.row)
//        print("section:",sender.section)
//        print(sender.isOn)
        self.settingOptions[sender.section!][sender.row!].switchState = sender.isOn
        self.settingsTable.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: SettingsTableSectionHeader.reuseIdentifier) as! SettingsTableSectionHeader
        header.settingsHeaderLbl.text = settingSections[section]
        return header
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
