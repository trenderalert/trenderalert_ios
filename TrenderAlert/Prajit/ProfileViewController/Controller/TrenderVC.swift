//
//  TrenderVC.swift
//  TrenderAlert
//
//  Created by HPL on 28/03/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import SDWebImage

class TrenderVC: UIViewController {

    @IBOutlet weak var trenderTable: UITableView!
    @IBOutlet weak var noDataLbl: UILabel!
    
    var headerSearchViewHeight = (UIDevice.current.userInterfaceIdiom == .pad) ? 60.0 : 40.0
    
    let noOfRecordPerRequest = 5
    var totalUserCount = 1
    var searchText = ""
    var trenderListObj = TrenderModel()
    var friendViewRef:FriendsViewHeader?
    var isTrenderApiRequestInProgress = false
    var isTrender:Bool! // Trender = User's following you, Trend Setter = User's you following
    var OtherUserProfileId = 0
    var navigatedFromMyProfile = false
    
    lazy var refreshControl:UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = UIColor.red
        return refreshControl
    }()
    
    var noOfTrenders = 0{
        didSet{
            DispatchQueue.main.async {
                self.trenderTable.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationbar()
        setupTableView()
        self.noDataLbl.text = MySingleton.shared.selectedLangData.no_data_available
        getTrendersList(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: true)
    }
    
    func setupTableView(){
        trenderTable.delegate = self
        trenderTable.dataSource = self
        trenderTable.register(UINib(nibName: RequestTVC.resuseIdentifier, bundle: nil), forCellReuseIdentifier: RequestTVC.resuseIdentifier)
        
        let header = UINib.init(nibName: "FriendsViewHeader", bundle: nil)
        trenderTable.register(header, forHeaderFooterViewReuseIdentifier: "FriendsViewHeader")
        trenderTable.tableFooterView = UIView()
        self.trenderTable.addSubview(self.refreshControl)
    }
    
    func setupNavigationbar(){
        
        let title = isTrender ? MySingleton.shared.selectedLangData.trender : MySingleton.shared.selectedLangData.trend_setter
        
        Utilities.setNavigationBarWithTitle(viewController: self, title: title)
            let leftButton = UIBarButtonItem(image: UIImage(named: "img_BackArrow"), style: .plain, target: self, action: #selector(goBack))
            navigationItem.leftBarButtonItem = leftButton
    }
    
    @objc func goBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.trenderListObj.data = []
        totalUserCount = 1
        getTrendersList(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: true)
        
        self.trenderTable.reloadData()
        refreshControl.endRefreshing()
    }
    
    func getTrendersList(Skip: Int, Take: Int, lazyLoading: Bool,SearchString:String = ""){
        
        if (trenderListObj.data.count >=  totalUserCount) || isTrenderApiRequestInProgress{
            return
        }
        
        self.isTrenderApiRequestInProgress = true
        
        let param = ["Skip":Skip,"Take":Take,"OtherUserProfileId":self.OtherUserProfileId,"Search":searchText] as [String : Any]
        
        WebServices().callUserService(service: (isTrender ? .getTrenders : .getTrendSetters), urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: lazyLoading, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    let temp = WebServices().decodeDataToClass(data:serviceData, decodeClass: TrenderModel.self)!
                    self.trenderListObj.data += temp.data
                    self.totalUserCount = serviceResponse["TotalRecords"] as! Int
                    self.noOfTrenders += 1
                    self.isTrenderApiRequestInProgress = false
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }

}


extension TrenderVC:UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{
    @objc func showZoomedImage(_ sender:UITapGestureRecognizer){
        
        var imageUrl = ""
        
        imageUrl = trenderListObj.data[(sender.view?.tag)!].ProfileImage ?? ""
        
        let zoomImageView = ZoomImageVC(nibName: "ZoomImageVC", bundle: nil)
        zoomImageView.imageUrl = imageUrl
        self.navigationController?.pushViewController(zoomImageView, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.trenderListObj.data.count > 0{
            noDataLbl.isHidden = true
        }else{
            noDataLbl.isHidden = false
        }
        return self.trenderListObj.data.count
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
            if indexPath.row == (trenderListObj.data.count - 1){
                getTrendersList(Skip: trenderListObj.data.count, Take: noOfRecordPerRequest, lazyLoading: true)
            }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       let cell = tableView.dequeueReusableCell(withIdentifier: RequestTVC.resuseIdentifier, for: indexPath) as! RequestTVC
            cell.friendImg.sd_setImage(with: URL(string: self.trenderListObj.data[indexPath.row].ProfileImage ?? ""), placeholderImage: UIImage(named: "img_selectProfilePic"), options: [.continueInBackground], completed: nil)
            cell.friendNameLbl.text = "\(self.trenderListObj.data[indexPath.row].Firstname ?? "") \(self.trenderListObj.data[indexPath.row].Lastname ?? "")"
            cell.friendCityLbl.text = (self.trenderListObj.data[indexPath.row].Location ?? "").replacingOccurrences(of: "null,", with: " ").replacingOccurrences(of: "null", with: " ")
        
            cell.acceptBtn.tag = indexPath.row
            cell.declineBtn.tag = indexPath.row
        
        
        
        
        
        if isTrender{
            var acceptBtnTitle = String()
            if self.trenderListObj.data[indexPath.row].IsFollowRequested
            {
                acceptBtnTitle = MySingleton.shared.selectedLangData.Requested
                cell.acceptBtn.isEnabled = false
            }
            else{
                acceptBtnTitle = (self.trenderListObj.data[indexPath.row].AreYouFollowing) ? MySingleton.shared.selectedLangData.Unfollow.uppercased() : MySingleton.shared.selectedLangData.FOLLOW
                cell.acceptBtn.isEnabled = true
            }
            let blockBtnTitle  = (self.trenderListObj.data[indexPath.row].HaveYouBlocked) ? MySingleton.shared.selectedLangData.unblock.uppercased() : MySingleton.shared.selectedLangData.block.uppercased()
            if "\(OtherUserProfileId)" != MySingleton.shared.loginObject.UserID{ // Other users trenders
//                !self.trenderListObj.data[indexPath.row].AreYouFollowing,
                if ("\(trenderListObj.data[indexPath.row].UserProfileID ?? 0)" != MySingleton.shared.loginObject.UserID){
                    cell.acceptBtn.setTitle(acceptBtnTitle, for: .normal)
                    cell.acceptBtn.addTarget(self, action: #selector(followUnfollowUser(_:)), for: .touchUpInside)
                    cell.declineBtn.isHidden = true
                }else{
                    cell.hideAcceptDeclineBtn()
                }
            }else{
                showAllOptionsForTrenderOrTrendSetter(cell: cell, acceptBtnTitle: acceptBtnTitle, blockBtnTitle: blockBtnTitle)
            }
            
        }else{
            let acceptBtnTitle = (self.trenderListObj.data[indexPath.row].AreYouFollowing) ? MySingleton.shared.selectedLangData.Unfollow.uppercased() : MySingleton.shared.selectedLangData.FOLLOW
            let blockBtnTitle  = (self.trenderListObj.data[indexPath.row].HaveYouBlocked) ? MySingleton.shared.selectedLangData.unblock.uppercased() : MySingleton.shared.selectedLangData.block.uppercased()
            if ("\(OtherUserProfileId)" != MySingleton.shared.loginObject.UserID){ // Other users trend setters
                if ("\(trenderListObj.data[indexPath.row].UserProfileID ?? 0)" != MySingleton.shared.loginObject.UserID){
                    cell.acceptBtn.setTitle(acceptBtnTitle, for: .normal)
                    cell.acceptBtn.addTarget(self, action: #selector(followUnfollowUser(_:)), for: .touchUpInside)
                    cell.declineBtn.isHidden = true
                }else{
                    cell.hideAcceptDeclineBtn()
                }
            }else{
                showAllOptionsForTrenderOrTrendSetter(cell: cell, acceptBtnTitle: acceptBtnTitle, blockBtnTitle: blockBtnTitle)
            }
        }
        
        if !navigatedFromMyProfile{
            cell.declineBtn.isHidden = true
        }
            cell.declineBtn.backgroundColor = UIColor.baseColor
        
            let imageTapGesture = UITapGestureRecognizer(target: self, action: #selector(showZoomedImage))
            cell.friendImg.tag = indexPath.row
            cell.friendImg.addGestureRecognizer(imageTapGesture)
            cell.friendImg.isUserInteractionEnabled = true
        
            DispatchQueue.main.async {
                cell.setup()
            }
            return cell
    }
    
    func showAllOptionsForTrenderOrTrendSetter(cell:RequestTVC,acceptBtnTitle:String,blockBtnTitle:String){
        cell.acceptBtn.setTitle(acceptBtnTitle, for: .normal)
        cell.declineBtn.setTitle(blockBtnTitle, for: .normal)
        
        cell.acceptBtn.addTarget(self, action: #selector(followUnfollowUser(_:)), for: .touchUpInside)
        cell.declineBtn.addTarget(self, action: #selector(blockUnblock(_:)), for: .touchUpInside)
    }
    
    @objc func followUnfollowUser(_ sender:UIButton){
       
        let userId = "\(trenderListObj.data[sender.tag].UserProfileID ?? 0)"
        
        let param:[String:String] = [:]
        
        WebServices().callUserService(service: .followUnfollowTrender, urlParameter: userId, parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    DispatchQueue.main.async{
                    if self.isTrender{ // follow you
                        if sender.currentTitle == MySingleton.shared.selectedLangData.FOLLOW
                        {
                            self.trenderListObj.data[sender.tag].IsFollowRequested = true
                        }
                        self.trenderListObj.data[sender.tag].AreYouFollowing = !self.trenderListObj.data[sender.tag].AreYouFollowing
                    }else{ // you follow
                        if self.navigatedFromMyProfile{ 
                            self.trenderListObj.data.remove(at: sender.tag)
                            self.totalUserCount -= 1
                        }else{
                            self.trenderListObj.data[sender.tag].AreYouFollowing = !self.trenderListObj.data[sender.tag].AreYouFollowing

                        }
                    }
                        self.trenderTable.reloadData()
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    @objc func blockUnblock(_ sender:UIButton){
       
        
        let userId = "\(trenderListObj.data[sender.tag].UserProfileID ?? 0)"
        
        let param:[String:String] = ["UserID":userId]
        
        WebServices().callUserService(service: .blockUnblockUser, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    DispatchQueue.main.async{
                    self.trenderListObj.data[sender.tag].HaveYouBlocked = !self.trenderListObj.data[sender.tag].HaveYouBlocked
                    
                        self.trenderTable.reloadData()
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "FriendsViewHeader") as! FriendsViewHeader
        header.searchUTF.delegate = self
        friendViewRef = header
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return CGFloat(headerSearchViewHeight)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        //        if updatedString?.trimmingCharacters(in: .whitespaces) == ""{
        //            return true
        //        }
        
        self.trenderListObj.data = []
        self.noOfTrenders = 0
        self.totalUserCount = 1
        searchText = updatedString!
        getTrendersList(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: true,SearchString: updatedString!)
        return true
    }
}
