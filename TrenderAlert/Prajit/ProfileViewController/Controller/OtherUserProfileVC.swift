//
//  OtherUserProfileVC.swift
//  TrenderAlert
//
//  Created by HPL on 03/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import ReadMoreTextView
import SDWebImage

class OtherUserProfileVC: UIViewController {

    @IBOutlet weak var otherUserProfileDetailsTblView: UITableView!
    //    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
   
    @IBOutlet weak var otherUserImage: UIImageView!
    @IBOutlet weak var otherUserCoverImage: UIImageView!
    
  
    @IBOutlet weak var aboutMeTxtView: ReadMoreTextView!
    @IBOutlet weak var coverImageHeight: NSLayoutConstraint!
    @IBOutlet weak var userProfileNameLbl: UILabel!
    
    @IBOutlet weak var friendUnfriendRequestLbl: UILabel!
    
    @IBOutlet weak var friendUnfriendRequestBtn: UIButton!
    @IBOutlet weak var profileOptionsStackView: UIStackView!
    @IBOutlet weak var friendOptionViewStack: UIView!
    @IBOutlet weak var friendUnfriendOptionLbl: UILabel!
    @IBOutlet weak var friendUnfriendOptionImage: UIButton!
    
    var isOtherUserPremium = Bool()
    var tempView:UIView?
    
    var otherUserProfileDetails = [["education","Education",""],["interest","Interest",""],["web","Links and websites",""]]
    
    
    let picker = UIImagePickerController()
    
    let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select your option", message: "", preferredStyle: .actionSheet)
    
    var aboutMeText = ""{
        didSet{
            setExpandableText()
        }
    }
    
    var userInfoObj = UserInfo()
    var userId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupNavigationbar()
        zoomImage()
        setupTableView()
        setExpandableText()
        hideFriendOption()
        getOtherUserProfileInfo()
    }
    
    func hideFriendOption(){
        tempView = friendOptionViewStack
         self.profileOptionsStackView.removeArrangedSubview(friendOptionViewStack)
         friendOptionViewStack.removeFromSuperview()
    }
    
    func showFriendOptions(){
        if "\(self.userInfoObj.data[0].UserProfileId ?? 0)" != "\(MySingleton.shared.loginObject.UserID)"{
            self.profileOptionsStackView.addArrangedSubview(tempView!)
            
            if self.userInfoObj.data[0].IsFriend{ // Show unfriend option
                showUnfriendOption()
            }else if (!self.userInfoObj.data[0].IsFriend),((self.userInfoObj.data[0].FriendRequestID == 0)){ //Show Friend Request Option
                showFriendRequestOption()
            }else{ // Cancel Friend Request
                if (self.userInfoObj.data[0].FriendReqStatus == 0){
                    showCancelRequestOption()
                }else{
                    showFriendRequestOption()
                }
            }
        }
     }
    
    func showCancelRequestOption(){
        DispatchQueue.main.async {
            self.friendUnfriendOptionLbl.text = "Cancel Request"
            self.friendUnfriendOptionImage.setImage(UIImage(named: "Cancel_friend_request"), for: .normal)
        }
        
    }
    func showFriendRequestOption(){
        DispatchQueue.main.async {
            self.friendUnfriendOptionLbl.text = "Friend"
            self.friendUnfriendOptionImage.setImage(UIImage(named: "Friend_request"), for: .normal)
        }
    }
    func showUnfriendOption(){
        DispatchQueue.main.async {
            self.friendUnfriendOptionLbl.text = "UnFriend"
            self.friendUnfriendOptionImage.setImage(UIImage(named: "Unfriend"), for: .normal)
        }
    }
    
    
    func zoomImage(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showZoomedImage))
        otherUserCoverImage.addGestureRecognizer(tapGesture)
        otherUserCoverImage.isUserInteractionEnabled = true
        
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(showOtherUserProfileZoom))
        otherUserImage.addGestureRecognizer(tapGesture2)
        otherUserImage.isUserInteractionEnabled = true
        
    }
    @objc func showZoomedImage(){
        let zoomImageView = ZoomImageVC(nibName: "ZoomImageVC", bundle: nil)
        zoomImageView.imageUrl = userInfoObj.data[0].CoverPic ?? ""
        self.navigationController?.pushViewController(zoomImageView, animated: true)
    }
    
    @objc func showOtherUserProfileZoom(){
        let zoomImageView = ZoomImageVC(nibName: "ZoomImageVC", bundle: nil)
        zoomImageView.imageUrl = userInfoObj.data[0].UserProfileImage ?? ""
        self.navigationController?.pushViewController(zoomImageView, animated: true)
    }
    
    func setupNavigationbar(){
        Utilities.setNavigationBarWithTitle(viewController: self, title: "Profile")
        let leftButton = UIBarButtonItem(image: UIImage(named: "img_BackArrow"), style: .plain, target: self, action: #selector(goBack))
        navigationItem.leftBarButtonItem = leftButton
    }
    
    override func viewDidLayoutSubviews() {
        coverImageHeight.constant = UIScreen.main.bounds.height * 0.18
        self.otherUserImage.makeCornerRadius(radius: self.otherUserImage.frame.width/2)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func setExpandableText(){
        
        aboutMeTxtView.shouldTrim = true
        aboutMeTxtView.isScrollEnabled = false
        aboutMeTxtView.maximumNumberOfLines = 2
        aboutMeTxtView.readMoreTextPadding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        let attributedString = NSMutableAttributedString(string:aboutMeText)
        
        let range = (attributedString.string as NSString).range(of: aboutMeText)
        
    attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black , range:range)
        
        
        var readMoreAttrString:NSMutableAttributedString?
        var readLessAttrString:NSMutableAttributedString?
        
        var readMoreAdditionalTextAttrString:NSMutableAttributedString?
        let readMoreAddtionalText = "..."
        
        
        let readMoreText = "Read more"
        let readLessText = "  Read less"
        let readMoreLessFont = "OpenSans-Semibold"
        let aboutMeTextFont = "OpenSans-Light"
        
        let minFontSize:CGFloat = 13.0
        let maxFontSize:CGFloat = 18.0
        
        
        if UIDevice.current.userInterfaceIdiom == .phone{
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: aboutMeTextFont, size: minFontSize) ?? UIFont.systemFont(ofSize: minFontSize), range: range)
            
            readMoreAttrString = NSMutableAttributedString(string: readMoreText, attributes: [NSAttributedString.Key.font:UIFont(name: readMoreLessFont, size: minFontSize) ?? UIFont.systemFont(ofSize: minFontSize),NSAttributedString.Key.foregroundColor:UIColor.baseColor])
            
            readMoreAdditionalTextAttrString = NSMutableAttributedString(string: readMoreAddtionalText, attributes: [NSAttributedString.Key.font:UIFont(name: readMoreLessFont, size: minFontSize) ?? UIFont.systemFont(ofSize: minFontSize),NSAttributedString.Key.foregroundColor:UIColor.black])
            
            
            readLessAttrString = NSMutableAttributedString(string: readLessText, attributes: [NSAttributedString.Key.font:UIFont(name: readMoreLessFont, size: minFontSize) ?? UIFont.systemFont(ofSize: minFontSize),NSAttributedString.Key.foregroundColor:UIColor.baseColor])
            
            
        }else{
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: aboutMeTextFont, size: maxFontSize) ?? UIFont.systemFont(ofSize: maxFontSize), range: range)
            
            readMoreAttrString = NSMutableAttributedString(string: readMoreText, attributes: [NSAttributedString.Key.font:UIFont(name: readMoreLessFont, size: maxFontSize) ?? UIFont.systemFont(ofSize: maxFontSize),NSAttributedString.Key.foregroundColor:UIColor.baseColor])
            
            readMoreAdditionalTextAttrString = NSMutableAttributedString(string: readMoreAddtionalText, attributes: [NSAttributedString.Key.font:UIFont(name: readMoreLessFont, size: maxFontSize) ?? UIFont.systemFont(ofSize: minFontSize),NSAttributedString.Key.foregroundColor:UIColor.black])
            
            
            readLessAttrString = NSMutableAttributedString(string: readLessText, attributes: [NSAttributedString.Key.font:UIFont(name: readMoreLessFont, size: maxFontSize) ?? UIFont.systemFont(ofSize: maxFontSize),NSAttributedString.Key.foregroundColor:UIColor.baseColor])
        }
        
        readMoreAdditionalTextAttrString?.append(readMoreAttrString!)
        
        aboutMeTxtView.attributedText = NSAttributedString(attributedString: attributedString)
        aboutMeTxtView.attributedReadMoreText = NSAttributedString(attributedString: readMoreAdditionalTextAttrString!)
        aboutMeTxtView.attributedReadLessText = NSAttributedString(attributedString: readLessAttrString!)
    }
    
    func getOtherUserProfileInfo(){
        
        if self.userId <= 0{
            return
        }
        
        let parameters = ["UserID":self.userId]
        print("param ",parameters)
        WebServices().callUserService(service: UserServices.getUserInfo, urlParameter: "", parameters: parameters as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){ (serviceResponse, serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    DispatchQueue.main.async {
                    self.isOtherUserPremium = ((serviceResponse["data"]as! [AnyObject])[0] as! [String:Any])["IsPremiumUser"]as! Bool
                    self.userInfoObj = WebServices().decodeDataToClass(data: serviceData, decodeClass: UserInfo.self)!
                    self.userProfileNameLbl.text = "\(self.userInfoObj.data[0].FirstName ?? "") \(self.userInfoObj.data[0].LastName ?? "")"
                    self.otherUserCoverImage.sd_setImage(with: URL(string: self.userInfoObj.data[0].CoverPic ?? ""), placeholderImage: UIImage(named: "Img_Blur"), options: [.continueInBackground], completed: nil)
                    self.otherUserImage.sd_setImage(with: URL(string: self.userInfoObj.data[0].UserProfileImage ?? ""), placeholderImage: UIImage(named: "img_selectProfilePic"), options: [.continueInBackground], completed: nil)
                    self.otherUserProfileDetails[0][2] = self.userInfoObj.data[0].Education ?? ""
                    self.otherUserProfileDetails[1][2] = self.userInfoObj.data[0].Interest ?? ""
                    self.otherUserProfileDetails[2][2] = self.userInfoObj.data[0].Links ?? ""
                    self.aboutMeText = self.userInfoObj.data[0].About ?? ""
                       self.otherUserProfileDetailsTblView.reloadData()
                       self.showFriendOptions()
                    }
                }else{
                    
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    @IBAction func goBack(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupTableView(){
        otherUserProfileDetailsTblView.dataSource = self
        otherUserProfileDetailsTblView.delegate = self
        otherUserProfileDetailsTblView.register(UINib(nibName: UserProfileTVC.reuseIdentifier, bundle: nil), forCellReuseIdentifier: UserProfileTVC.reuseIdentifier)
        
    }
    
    @IBAction func changeProfileImage(_ sender: UIButton) {
        self.present(actionSheetControllerIOS8,animated: true,completion: nil)
    }
    
    @IBAction func showFriends(_ sender: UIButton) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "FriendsSB", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "FriendsVC") as! FriendsVC
        
//        if "\(self.userInfoObj.data[0].UserProfileId ?? 0)" == MySingleton.shared.loginObject.UserID{
//            nextViewController.OtherUserProfileID = 0
//        }else{
            nextViewController.OtherUserProfileID = self.userInfoObj.data[0].UserProfileId ?? 0
//        }
        
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
    @IBAction func goToTrenderSceen(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
        let trenderVC = storyboard.instantiateViewController(withIdentifier: "TrenderVC") as! TrenderVC
        trenderVC.OtherUserProfileId = userInfoObj.data[0].UserProfileId ?? 0
        trenderVC.isTrender = true
        self.navigationController?.pushViewController(trenderVC, animated: true)
    }
    
    @IBAction func goToTrendSetterScreen(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
        let trenderVC = storyboard.instantiateViewController(withIdentifier: "TrenderVC") as! TrenderVC
        trenderVC.OtherUserProfileId = userInfoObj.data[0].UserProfileId ?? 0
        trenderVC.isTrender = false
        self.navigationController?.pushViewController(trenderVC, animated: true)
    }
    
    @IBAction func friendUnfriendCancelFriendReqeust(_ sender: UIButton) {
        print("coming to friend unfriend")
        if "\(self.userInfoObj.data[0].UserProfileId ?? 0)" != "\(MySingleton.shared.loginObject.UserID)"{
            self.profileOptionsStackView.addArrangedSubview(tempView!)
            
            if self.userInfoObj.data[0].IsFriend{ // Show unfriend option
                self.unfriend(userId: self.userInfoObj.data[0].UserProfileId!)
            }else if (!self.userInfoObj.data[0].IsFriend),((self.userInfoObj.data[0].FriendRequestID == 0)){ //Show Friend Request Option
                self.sendFriendRequest(userId: self.userInfoObj.data[0].UserProfileId!)
            }else{ // Cancel Friend Request
                
                if (self.userInfoObj.data[0].FriendReqStatus == 0),(self.userInfoObj.data[0].FriendRequestID != 0){
                    self.cancelFriendRequest(requestId: self.userInfoObj.data[0].FriendRequestID)
                }else{
                   self.sendFriendRequest(userId: self.userInfoObj.data[0].UserProfileId!)
                }
                
            }
        }
    }
    
    func unfriend(userId:Int){
        
        let param = ["UserID":userId]
        
        WebServices().callUserService(service: UserServices.unfriend, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    self.showFriendRequestOption()
                    self.userInfoObj.data[0].IsFriend = false
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func sendFriendRequest(userId: Int) {
        
        if (MySingleton.shared.loginObject.IsPremiumUser == "False") {
            //            let showPremiumAlert = UIAlertController(title: "Non Premium User", message: "Non premium user cannot send friend request to another user", preferredStyle: .alert)
            //            let okAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            //            showPremiumAlert.addAction(okAction)
            //            self.present(showPremiumAlert, animated: true, completion: nil)
            
            DispatchQueue.main.async {
                TrenderAlertVC.shared.presentAlertController(message: "Non premium user cannot send friend request", completionHandler: {
                    MySingleton.shared.isFirstTimeSideMenu = false
                    let storyboard = UIStoryboard(name: "NotificationSB", bundle: nil)
                    let paymentVC = storyboard.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
                    paymentVC.isBackEnabled = true
                    self.navigationController?.pushViewController(paymentVC, animated: true)
                    //                    let tabBar = self.menuContainerViewController.centerViewController as! UITabBarController
                    //                    let navigationController = tabBar.selectedViewController as! UINavigationController
                    //                    navigationController.viewControllers = [paymentVC]
                    //                    //    self.menuContainerViewController.menuState = MFSideMenuStateClosed
                })
            }
            
            return
        }
        
        if self.isOtherUserPremium == false{
            DispatchQueue.main.async {
                TrenderAlertVC.shared.presentAlertController(message: "This user is non premium user", completionHandler: {
                })
            }
            return
        }
        
        let params = ["UserID":userId]
        
        WebServices().callUserService(service: UserServices.sendFriendRequest, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    self.showCancelRequestOption()
                    self.userInfoObj.data[0].FriendRequestID = Int((serviceResponse["data"] as! NSString).intValue)
                    self.userInfoObj.data[0].FriendReqStatus = 0
                }else{
                   TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    
    func cancelFriendRequest(requestId:Int){
        // Cancel friend
        let params:[String:Any] = [:]
        
        WebServices().callUserService(service: UserServices.cancelFriendRequest, urlParameter: "\(requestId)", parameters: params as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    self.showFriendRequestOption()
                    self.userInfoObj.data[0].IsFriend = false
                    self.userInfoObj.data[0].FriendReqStatus = 0
                    self.userInfoObj.data[0].FriendRequestID = 0
                }else{
                   TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    @IBAction func goToTrendsScreen(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
        let favouriteTrendsVC = storyBoard.instantiateViewController(withIdentifier: "MyFavouriteTrendsVC") as! MyFavouriteTrendsVC
        favouriteTrendsVC.OtherUserProfileID = self.userInfoObj.data[0].UserProfileId
        self.navigationController?.pushViewController(favouriteTrendsVC, animated: true)
    }
   
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}



extension OtherUserProfileVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return otherUserProfileDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UserProfileTVC.reuseIdentifier, for: indexPath) as! UserProfileTVC
        cell.detailSpecificImage.image = UIImage(named: otherUserProfileDetails[indexPath.row][0])
        cell.fieldNameLbl.text = otherUserProfileDetails[indexPath.row][1]
        cell.fieldDataLbl.text = otherUserProfileDetails[indexPath.row][2]
        return cell
    }
}



