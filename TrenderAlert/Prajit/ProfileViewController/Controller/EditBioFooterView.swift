//
//  EditBioFooterView.swift
//  TrenderAlert
//
//  Created by HPL on 04/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit


protocol EditBioFooterViewDelegate: class {
    func saveEditBio()
}

class EditBioFooterView: UITableViewHeaderFooterView {

  
    weak var delegate: EditBioFooterViewDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    @IBAction func saveEditBioDetails(_ sender: UIButton) {
        delegate?.saveEditBio()
    }
    
}
