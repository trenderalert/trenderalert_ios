//
//  BlockListUserVC.swift
//  TrenderAlert
//
//  Created by HPL on 30/05/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import SDWebImage

class BlockListUserVC: UIViewController {

    @IBOutlet weak var searchBlockListUserTFT: UITextField!
    @IBOutlet weak var blockListUserTable: UITableView!
    @IBOutlet weak var noDataLbl: UILabel!
    
    var blockListObj = BlockList()
    var noOfRecordsPerRequest = 10
    var totalBlockUserCount = 1
    var searchText = ""
    var isBlockListRequestInProgress = false
    
    var noOfBlockUser = 0{
        didSet{
            DispatchQueue.main.async {
                self.blockListUserTable.reloadData()
            }
        }
    }
    
    lazy var refreshControl:UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = UIColor.red
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl){
        self.blockListObj.data = []
        self.totalBlockUserCount = 1
        getBlocklistUser(Skip: 0, Take: self.noOfRecordsPerRequest, lazyLoading: false)
        self.blockListUserTable.reloadData()
        refreshControl.endRefreshing()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationbar()
        initialConfig()
         getBlocklistUser(Skip: 0, Take: self.noOfRecordsPerRequest, lazyLoading: false)
    }
    
    func initialConfig(){
        self.searchBlockListUserTFT.placeholder = MySingleton.shared.selectedLangData.search_by_name
        self.searchBlockListUserTFT.leftPadding(paddingSize: 15)
        self.searchBlockListUserTFT.delegate = self
        
        self.blockListUserTable.delegate = self
        self.blockListUserTable.dataSource = self
        
//        self.blockListUserTable.register(UINib(nibName: "InviteContactListTVC", bundle: nil), forCellReuseIdentifier: "InviteContactListTVC")
        self.blockListUserTable.register(UINib(nibName: "GroupMembersTVC", bundle: nil), forCellReuseIdentifier: "GroupMembersTVC")
    }
    
   func setupNavigationbar(){
    Utilities.setNavigationBarWithTitle(viewController: self, title: MySingleton.shared.selectedLangData.blocklist)
    let leftButton = UIBarButtonItem(image: UIImage(named: "img_BackArrow"), style: .plain, target: self, action: #selector(goBack))
    navigationItem.leftBarButtonItem = leftButton
   }
    
    @objc func goBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func getBlocklistUser(Skip:Int,Take:Int,lazyLoading:Bool,SearchString:String = ""){
        
        if (self.blockListObj.data.count >= totalBlockUserCount) || isBlockListRequestInProgress{
            return
        }
        self.isBlockListRequestInProgress = true
        let param = ["Skip":Skip,"Take":Take,"Search":searchText] as [String:Any]
        
        WebServices().callUserService(service: UserServices.getBlockListUser, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: lazyLoading, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            if (serviceResponse["Status"] != nil),(serviceResponse["Status"] as! Bool == true){
                let temp = WebServices().decodeDataToClass(data:serviceData, decodeClass: BlockList.self)!
                self.blockListObj.data += temp.data
                self.totalBlockUserCount = serviceResponse["TotalRecords"] as! Int
                self.noOfBlockUser += 1
                self.isBlockListRequestInProgress = false
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
                
        }
    }
    
}

extension BlockListUserVC:UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource{
    
    @objc func showZoomedImage(_ sender:UITapGestureRecognizer){
        let imageUrl = self.blockListObj.data[(sender.view?.tag)!].ProfileImage ?? ""
        let zoomImageView = ZoomImageVC(nibName: "ZoomImageVC", bundle: nil)
        zoomImageView.imageUrl = imageUrl
        self.navigationController?.pushViewController(zoomImageView, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.blockListObj.data.isEmpty && !isBlockListRequestInProgress{
            self.noDataLbl.isHidden = false
        }else{
            self.noDataLbl.isHidden = true
        }
        return self.blockListObj.data.count
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (blockListObj.data.count - 1){
            getBlocklistUser(Skip: blockListObj.data.count, Take: noOfRecordsPerRequest, lazyLoading: true)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GroupMembersTVC", for: indexPath) as! GroupMembersTVC
            cell.memberImage.sd_setImage(with: URL(string: self.blockListObj.data[indexPath.row].ProfileImage ?? ""), placeholderImage:  UIImage(named: "img_selectProfilePic"), options: [.continueInBackground], completed: nil)
            cell.memberNameLbl.text = "\(self.blockListObj.data[indexPath.row].Username ?? "")"
            cell.memberMoreOptionBtn.setBackgroundImage(nil, for: .normal)
            cell.memberMoreOptionBtn.setTitle(MySingleton.shared.selectedLangData.unblock, for: .normal)
            cell.memberMoreOptionBtn.backgroundColor = UIColor.baseColor
            cell.memberMoreOptionBtn.layer.cornerRadius = 3
            cell.memberMoreOptionBtn.layer.borderWidth = 1
            cell.memberImage.layer.cornerRadius = cell.memberImage.frame.height/2;
            cell.memberMoreOptionBtn.layer.borderColor = UIColor.gray.cgColor
            cell.memberMoreOptionBtn.tag = indexPath.row
            let imageTapGesture = UITapGestureRecognizer(target: self, action: #selector(showZoomedImage))
            cell.memberImage.addGestureRecognizer(imageTapGesture)
            cell.memberMoreOptionBtn.addTarget(self, action: #selector(unblockUser(_:)), for: .touchUpInside)

            return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let profileId = self.blockListObj.data[indexPath.row].BlockedUserId
        self.viewProfile(profileId: profileId ?? 0)
    }
    
    func viewProfile(profileId:Int){
        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
        let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
        profileVC.userId = profileId
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    @objc func unblockUser(_ sender:UIButton){
        
        let confirmationAlert = UIAlertController(title: "TrenderAlert", message: MySingleton.shared.selectedLangData.are_you_sure_you_want_to_block, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: MySingleton.shared.selectedLangData.yes, style: .destructive) { (yesAction) in
            let userIdToUnblock = "\(self.blockListObj.data[sender.tag].BlockedUserId ?? 0)"
            let param:[String:String] = ["UserID":userIdToUnblock]
            
            WebServices().callUserService(service: .blockUnblockUser, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
                if (serviceResponse["Status"] != nil),(serviceResponse["Status"] as! Bool == true){
                    DispatchQueue.main.async {
                    self.blockListObj.data.remove(at: sender.tag)
                    self.totalBlockUserCount -= 1
                        self.blockListUserTable.reloadData()
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }
        }
        let noAction = UIAlertAction(title: MySingleton.shared.selectedLangData.no, style: .cancel) { (noAction) in
            
        }
        confirmationAlert.addAction(yesAction)
        confirmationAlert.addAction(noAction)
        self.present(confirmationAlert, animated: true, completion: nil)
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
          let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        self.blockListObj.data = []
        self.noOfBlockUser = 0
        self.totalBlockUserCount = 1
        self.searchText = updatedString!
        getBlocklistUser(Skip: 0, Take: noOfRecordsPerRequest, lazyLoading: true,SearchString: updatedString!)
        return true
    }
}
