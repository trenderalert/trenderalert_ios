//
//  UserProfileTVC.swift
//  TrenderAlert
//
//  Created by HPL on 03/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class UserProfileTVC: UITableViewCell {

    static let reuseIdentifier = "UserProfileTVC"
    
    @IBOutlet weak var detailSpecificImage: UIImageView!
    @IBOutlet weak var fieldNameLbl: UILabel!
    @IBOutlet weak var fieldDataLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
