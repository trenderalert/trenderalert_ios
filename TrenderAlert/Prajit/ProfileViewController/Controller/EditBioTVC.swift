//
//  EditBioTVC.swift
//  TrenderAlert
//
//  Created by HPL on 04/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class EditBioTVC: UITableViewCell {

    @IBOutlet weak var iconImage: UIImageView!
    
    @IBOutlet weak var questionLbl: UILabel!
    @IBOutlet weak var answerTxtView: UITextView!
    
    
    
    static let reuseIdentifier = "EditBioTVC"
    var textViewPlaceholderText = ""
    var isError = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        answerTxtView.isScrollEnabled = false
        answerTxtView.delegate = self
        let padding = answerTxtView.textContainer.lineFragmentPadding
        answerTxtView.textContainerInset = UIEdgeInsets(top: 0, left: -padding, bottom: 4, right: -padding)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
   
    
    func setupTextView(text:String){
        
        answerTxtView.returnKeyType = .done
        answerTxtView.text = (text == "") ? textViewPlaceholderText : text

        
        if UIDevice.current.userInterfaceIdiom == .pad{
            answerTxtView.font = UIFont(name: "OpenSans-Semibold", size: 19)
        }else{
            answerTxtView.font = UIFont(name: "OpenSans-Semibold", size: 13)
        }
        
        if text == "",isError{
            answerTxtView.textColor = UIColor.red
        }else if answerTxtView.text == EditBioVC.textViewPlaceholderText{
            answerTxtView.textColor = UIColor.lightGray
        }else{
            answerTxtView.textColor = UIColor.black
        }
        
    }
    
    var textString:String{
        get{
            return answerTxtView.text
        }set{
            answerTxtView.text = newValue
            textViewDidChange(answerTxtView)
        }
    }
    
}

extension EditBioTVC:UITextViewDelegate{
    func textViewDidChange(_ textView:UITextView){
        let size = textView.bounds.size
        let newSize = textView.sizeThatFits(CGSize(width: size.width, height: CGFloat.greatestFiniteMagnitude))
        
        if size.height != newSize.height{
            UIView.setAnimationsEnabled(false)
            tableView?.beginUpdates()
            tableView?.endUpdates()
            UIView.setAnimationsEnabled(true)
            
            if let thisIndexPath = tableView?.indexPath(for: self){
                tableView?.scrollToRow(at: thisIndexPath, at: .bottom, animated: false)
            }
        }
    }
}


