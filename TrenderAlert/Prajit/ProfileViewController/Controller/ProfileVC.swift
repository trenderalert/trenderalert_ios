//
//  ProfileVC.swift
//  TrenderAlert
//
//  Created by HPL on 02/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

///api/profile/GetUsersYouFollow
///api/profile/GetUsersFollowsYou



import UIKit
import ReadMoreTextView
import SDWebImage
import MBProgressHUD
import CropViewController

class ProfileVC: UIViewController{
   
    @IBOutlet weak var lblFavorite: UILabel!
    @IBOutlet weak var lblMyTrends: UILabel!
    @IBOutlet weak var lblTrendSetter: UILabel!
    @IBOutlet weak var lblTrender: UILabel!
    @IBOutlet weak var lblFriends: UILabel!
    @IBOutlet weak var lblEdit: UILabel!
    @IBOutlet weak var userProfileInfoTblView: UITableView!
    @IBOutlet weak var editProfileImgBtn: UIButton!
    @IBOutlet weak var userProfileImg: UIImageView!
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var userProfileIconSmallImg: UIImageView!
    @IBOutlet weak var aboutMeTxtView: ReadMoreTextView!
    
    @IBOutlet weak var profileImageView: UIView!
    @IBOutlet weak var coverImageHeight: NSLayoutConstraint!
    @IBOutlet weak var coverPicEditBtnView: UIView!
    @IBOutlet weak var userProfileNameLbl: UILabel!
    @IBOutlet weak var blockListBtn: UIButton!
    
    
    var userProfileDetails = [["education",MySingleton.shared.selectedLangData.education,""],["interest",MySingleton.shared.selectedLangData.interest,""],["web",MySingleton.shared.selectedLangData.links_and_websites,""]]
    
    let picker = UIImagePickerController()
    let coverPicker = UIImagePickerController()
    
    let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: MySingleton.shared.selectedLangData.please_select_your_option, message: "", preferredStyle: .actionSheet)
    
    var aboutMeText = ""{
        didSet{
             setExpandableText()
        }
    }
    
    
    var footerViewHeight = (UIDevice.current.userInterfaceIdiom == .pad) ? 80.0 : 60.0
    
    var userInfoObj = UserInfo()
    
    var changeProfileImage = false
    var reloadGetInfo = true
    var getInfoUserApiResult = false
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if reloadGetInfo{
          getProfileInfo()
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblEdit.text = MySingleton.shared.selectedLangData.edit
        self.lblFriends.text = MySingleton.shared.selectedLangData.friends
        self.lblTrender.text = MySingleton.shared.selectedLangData.trender
        self.lblTrendSetter.text = MySingleton.shared.selectedLangData.trend_setter
        self.lblMyTrends.text = MySingleton.shared.selectedLangData.my_trends
        self.lblFavorite.text = MySingleton.shared.selectedLangData.favorite
        self.blockListBtn.setTitle(MySingleton.shared.selectedLangData.blocklist, for: .normal)
        setExpandableText()
        // Do any additional setup after loading the view.
        setupNavigationbar()
        setupTableView()
        setupCameraActionSheet()
    }
    

    override func viewDidLayoutSubviews() {
        self.blockListBtn.layer.cornerRadius = 3
        coverImageHeight.constant = UIScreen.main.bounds.height * 0.18
        self.userProfileImg.makeCornerRadius(radius: self.userProfileImg.frame.width/2)
        
        coverPicEditBtnView.clipsToBounds = true
        coverPicEditBtnView.layer.cornerRadius = 7
        coverPicEditBtnView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
    }
    
    func getProfileInfo(){
        
        let parameters = ["UserID":MySingleton.shared.loginObject.UserID]
        
        WebServices().callUserService(service: UserServices.getUserInfo, urlParameter: "", parameters: parameters as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){ (serviceResponse, serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    DispatchQueue.main.async {
                    self.userInfoObj = WebServices().decodeDataToClass(data: serviceData, decodeClass: UserInfo.self)!
                    self.userProfileDetails[0][2] = self.userInfoObj.data[0].Education ?? ""
                    self.userProfileDetails[1][2] = self.userInfoObj.data[0].Interest ?? ""
                    self.userProfileDetails[2][2] = self.userInfoObj.data[0].Links ?? ""
                    
//                    self.userProfileNameLbl.text = self.userInfoObj.data[0].UserName ?? ""
                    self.userProfileNameLbl.text = "\(self.userInfoObj.data[0].FirstName ?? "") \(self.userInfoObj.data[0].LastName ?? "")"
                    
                    self.aboutMeText = self.userInfoObj.data[0].About ?? ""
                    self.getInfoUserApiResult = true
                    
                    self.coverImage.sd_setImage(with: URL(string: self.userInfoObj.data[0].CoverPic ?? ""), placeholderImage: UIImage(named: "Img_Blur"), options: [.continueInBackground], completed: nil)
                    self.userProfileImg.sd_setImage(with: URL(string: self.userInfoObj.data[0].UserProfileImage ?? ""), placeholderImage: UIImage(named: "img_selectProfilePic"), options: [.continueInBackground], completed: nil)
                    
                      self.userProfileInfoTblView.reloadData()
                    }
                }else{
                    self.getInfoUserApiResult = false
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                self.getInfoUserApiResult = false
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func setExpandableText(){
        
        aboutMeTxtView.shouldTrim = true
        aboutMeTxtView.isScrollEnabled = false
        aboutMeTxtView.maximumNumberOfLines = 2
        aboutMeTxtView.readMoreTextPadding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        let attributedString = NSMutableAttributedString(string:aboutMeText)
       
        let range = (attributedString.string as NSString).range(of: aboutMeText)
        
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black , range:range )
       
        
        var readMoreAttrString:NSMutableAttributedString?
        var readLessAttrString:NSMutableAttributedString?
        
        var readMoreAdditionalTextAttrString:NSMutableAttributedString?
        let readMoreAddtionalText = "..."
        
        
        let readMoreText = " \(MySingleton.shared.selectedLangData.read_more) "
        let readLessText = " \(MySingleton.shared.selectedLangData.read_less) "
        let readMoreLessFont = "OpenSans-Semibold"
        let aboutMeTextFont = "OpenSans-Light"
        
        let minFontSize:CGFloat = 13.0
        let maxFontSize:CGFloat = 18.0
        
        
        if UIDevice.current.userInterfaceIdiom == .phone{
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: aboutMeTextFont, size: minFontSize) ?? UIFont.systemFont(ofSize: minFontSize), range: range)
            
             readMoreAttrString = NSMutableAttributedString(string: readMoreText, attributes: [NSAttributedString.Key.font:UIFont(name: readMoreLessFont, size: minFontSize) ?? UIFont.systemFont(ofSize: minFontSize),NSAttributedString.Key.foregroundColor:UIColor.baseColor])
            
            readMoreAdditionalTextAttrString = NSMutableAttributedString(string: readMoreAddtionalText, attributes: [NSAttributedString.Key.font:UIFont(name: readMoreLessFont, size: minFontSize) ?? UIFont.systemFont(ofSize: minFontSize),NSAttributedString.Key.foregroundColor:UIColor.black])
            
            
            readLessAttrString = NSMutableAttributedString(string: readLessText, attributes: [NSAttributedString.Key.font:UIFont(name: readMoreLessFont, size: minFontSize) ?? UIFont.systemFont(ofSize: minFontSize),NSAttributedString.Key.foregroundColor:UIColor.baseColor])
            
            
        }else{
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont(name: aboutMeTextFont, size: maxFontSize) ?? UIFont.systemFont(ofSize: maxFontSize), range: range)
            
             readMoreAttrString = NSMutableAttributedString(string: readMoreText, attributes: [NSAttributedString.Key.font:UIFont(name: readMoreLessFont, size: maxFontSize) ?? UIFont.systemFont(ofSize: maxFontSize),NSAttributedString.Key.foregroundColor:UIColor.baseColor])
            
            readMoreAdditionalTextAttrString = NSMutableAttributedString(string: readMoreAddtionalText, attributes: [NSAttributedString.Key.font:UIFont(name: readMoreLessFont, size: maxFontSize) ?? UIFont.systemFont(ofSize: minFontSize),NSAttributedString.Key.foregroundColor:UIColor.black])
            
            
             readLessAttrString = NSMutableAttributedString(string: readLessText, attributes: [NSAttributedString.Key.font:UIFont(name: readMoreLessFont, size: maxFontSize) ?? UIFont.systemFont(ofSize: maxFontSize),NSAttributedString.Key.foregroundColor:UIColor.baseColor])
        }
        
        readMoreAdditionalTextAttrString?.append(readMoreAttrString!)
        
        aboutMeTxtView.attributedText = NSAttributedString(attributedString: attributedString)
        aboutMeTxtView.attributedReadMoreText = NSAttributedString(attributedString: readMoreAdditionalTextAttrString!)
        aboutMeTxtView.attributedReadLessText = NSAttributedString(attributedString: readLessAttrString!)
    
    }
    
    
    func setupCameraActionSheet(){
      self.userProfileIconSmallImg.makeCornerRadius(radius: self.userProfileIconSmallImg.frame.width/2)
        
        let cameraActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.camera, style: .default) { void in
            print("Camera")
            self.openCamera()
        }
        let galleryActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.gallery, style: .default) { void in
            print("Gallery")
            self.openGallary()
        }
        actionSheetControllerIOS8.addAction(UIAlertAction(title: MySingleton.shared.selectedLangData.cancel, style: UIAlertAction.Style.cancel, handler: { (action) -> Void in
            
        }))
        actionSheetControllerIOS8.addAction(cameraActionButton)
        actionSheetControllerIOS8.addAction(galleryActionButton)
    }

    func setupNavigationbar(){
        let leftBarButton = UIButton()
        leftBarButton.setImage(#imageLiteral(resourceName: "img_MenuBarBtn"), for: .normal)
        leftBarButton.addTarget(self, action: #selector(didTapMenu), for: .touchUpInside)
        let item1 = UIBarButtonItem()
        item1.customView = leftBarButton
        self.navigationItem.leftBarButtonItems = [item1]
        
        Utilities.setNavigationBarWithTitle(viewController: self, title: MySingleton.shared.selectedLangData.profile)
    }
    
    @objc func didTapMenu(_ sender: UIBarButtonItem) {
        if self.menuContainerViewController != nil {
            self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
        }
        
    }
    
    func setupTableView(){
        self.userProfileInfoTblView.delegate = self
        self.userProfileInfoTblView.dataSource = self
        
        userProfileInfoTblView.register(UINib(nibName: UserProfileTVC.reuseIdentifier, bundle: nil), forCellReuseIdentifier: UserProfileTVC.reuseIdentifier)
        userProfileInfoTblView.register(UINib(nibName: EditBioBtbTVC.reuseIdentifier, bundle: nil), forCellReuseIdentifier: EditBioBtbTVC.reuseIdentifier)
        userProfileInfoTblView.register(UINib(nibName: ProfileViewFooter.reuseIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: ProfileViewFooter.reuseIdentifier)
    }
    
    @IBAction func changeProfileImage(_ sender: UIButton) {
        picker.delegate = self
        self.changeProfileImage = true
        
        if let presenter = actionSheetControllerIOS8.popoverPresentationController {
            presenter.sourceView = sender;
            presenter.sourceRect = sender.bounds;
        }
        
        self.present(actionSheetControllerIOS8,animated: true,completion: nil)
    }
    
    @IBAction func changeCoverPic(_ sender: UIButton) {
//        coverPicker.delegate = self
        picker.delegate = self
        self.changeProfileImage = false
        
        if let presenter = actionSheetControllerIOS8.popoverPresentationController {
            presenter.sourceView = sender;
            presenter.sourceRect = sender.bounds;
        }
        
        self.present(actionSheetControllerIOS8,animated: true,completion: nil)
    }
    
    @IBAction func goToFriendScreen(_ sender: UIButton) {
        if MySingleton.shared.loginObject.IsPremiumUser == "True"{
            self.reloadGetInfo = false
            let storyBoard : UIStoryboard = UIStoryboard(name: "FriendsSB", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "FriendsVC") as! FriendsVC
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }else{
            TrenderAlertVC.shared.presentAlertController(message: MySingleton.shared.selectedLangData.this_feature_is_for_premium_members_only, completionHandler: nil)
        }
    }
    
    @IBAction func goToMyTrendsScreen(_ sender: UIButton) {
        self.reloadGetInfo = true
        let storyboard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
        let nextViewController = storyboard.instantiateViewController(withIdentifier: "MyTrendsVC") as! MyTrendsVC
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func goToMyFavouriteTrendsScreen(_ sender: UIButton) {
        self.reloadGetInfo = false
        let storyBoard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
        let favouriteTrendsVC = storyBoard.instantiateViewController(withIdentifier: "MyFavouriteTrendsVC") as! MyFavouriteTrendsVC
        self.navigationController?.pushViewController(favouriteTrendsVC, animated: true)
    }
    
    @IBAction func goToTrenderSceen(_ sender: UIButton) {
        self.reloadGetInfo = false
        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
        let trenderVC = storyboard.instantiateViewController(withIdentifier: "TrenderVC") as! TrenderVC
        trenderVC.OtherUserProfileId = userInfoObj.data[0].UserProfileId ?? 0
        trenderVC.isTrender = true
        trenderVC.navigatedFromMyProfile = true
        self.navigationController?.pushViewController(trenderVC, animated: true)
    }
    
    @IBAction func goToTrendSetterScreen(_ sender: UIButton) {
        self.reloadGetInfo = false
        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
        let trenderVC = storyboard.instantiateViewController(withIdentifier: "TrenderVC") as! TrenderVC
        trenderVC.OtherUserProfileId = userInfoObj.data[0].UserProfileId ?? 0
        trenderVC.isTrender = false
        trenderVC.navigatedFromMyProfile = true
        self.navigationController?.pushViewController(trenderVC, animated: true)
    }
    
    @IBAction func showBlockList(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
        let blockListVC = storyboard.instantiateViewController(withIdentifier: "BlockListUserVC") as! BlockListUserVC
        self.navigationController?.pushViewController(blockListVC, animated: true)
    }
    
}


extension ProfileVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.userProfileDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if indexPath.row < userProfileDetails.count{
            let cell = tableView.dequeueReusableCell(withIdentifier: UserProfileTVC.reuseIdentifier, for: indexPath) as! UserProfileTVC
            cell.detailSpecificImage.image = UIImage(named: userProfileDetails[indexPath.row][0])
            cell.fieldNameLbl.text = userProfileDetails[indexPath.row][1]
            cell.fieldDataLbl.text = userProfileDetails[indexPath.row][2]
            return cell
//        }else{
//            let cell = tableView.dequeueReusableCell(withIdentifier: EditBioBtbTVC.reuseIdentifier, for: indexPath) as! EditBioBtbTVC
//            return cell
//        }
     }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if userProfileDetails.count == indexPath.row{
//            return  tableView.frame.height * 0.4
//        }
//        return UITableView.automaticDimension
//    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: ProfileViewFooter.reuseIdentifier) as! ProfileViewFooter
        footer.editBioBtn.setTitle(MySingleton.shared.selectedLangData.edit_bio, for: .normal)
        footer.delegate = self
        return footer
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat(footerViewHeight)
    }
    
 }



extension ProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate,ProfileViewFooterDelegate,CropViewControllerDelegate
{
    
    func goToEditBioPage() {
        if getInfoUserApiResult{
            self.reloadGetInfo = true
            let storyBoard : UIStoryboard = UIStoryboard(name: "ProfileSB", bundle:nil)
            let nextViewController = storyBoard.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
            nextViewController.userInfoObj = self.userInfoObj
            self.navigationController?.pushViewController(nextViewController, animated: true)
        }
    }
    
    
    /*************************
     Method Name:  openGallary()
     Parameter: nil
     return type: nil
     Desc: This function opens the gallery.
     *************************/
    func openGallary()
    {
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerController.SourceType.photoLibrary
//            picker.modalPresentationStyle = .popover
            present(picker, animated: true, completion: nil)
    }
    
    /*************************
     Method Name:  openCamera()
     Parameter: nil
     return type: nil
     Desc: This function opens the Camera.
     *************************/
    func openCamera()
    {
    if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            present(picker, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    /*************************
     Method Name:  imagePickerControllerDidCancel()
     Parameter: UIImagePickerController
     return type: nil
     Desc: This function is called if user taps cancel button.
     *************************/
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        reloadGetInfo = false
        dismiss(animated: true, completion: nil)
    }
    
    /*************************
     Method Name:  imagePickerController()
     Parameter: UIImagePickerController, [String : Any]
     return type: nil
     Desc: This function is called when user selects image from gallery.
     *************************/
     func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        let chosenImage = info[.originalImage] as! UIImage
        reloadGetInfo = false
        
//        if changeProfileImage{
//            self.userProfileImg.image = chosenImage
//        }else{
//            self.coverImage.image = chosenImage
//        }
       
        
        let image: UIImage = chosenImage
        //Load an image
        dismiss(animated:true, completion: nil)
        let cropViewController = CropViewController(image: image)
        cropViewController.delegate = self
        present(cropViewController, animated: true, completion: nil)
        
        
//        self.reloadGetInfo = false
//        dismiss(animated:true, completion: nil)
//        uploadImage(chosenImage)
    }
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        // 'image' is the newly cropped version of the original image
                if changeProfileImage{
                    self.userProfileImg.image = image
                }else{
                    self.coverImage.image = image
                }
         dismiss(animated:true, completion: nil)
         uploadImage(image)
    }
    
    func uploadImage(_ imageToUpload:UIImage){
        var parameters = [String:AnyObject]()
        
        let imageData = ["FileExt":"jpg","contenttype":"image/jpeg","ImageData":"data:image/jpg;base64,\(Utilities.shared.encodeToBase64String(image: imageToUpload))"]
        
        let imageArray:[AnyObject] = [imageData as AnyObject]
        
        if changeProfileImage{
            parameters = ["ProfileImage":["Images":imageArray]] as [String:Any] as [String : AnyObject]
            callApi(requestData: parameters, url: "\(WebServices.baseURL)\(UserServices.uploadProfileImage.rawValue)")
        }else{
            parameters = ["CoverImage":["Images":imageArray]] as [String:Any] as [String : AnyObject]
            callApi(requestData: parameters, url: "\(WebServices.baseURL)\(UserServices.uploadCoverPic.rawValue)")
        }
    }
    
    
    
    func callApi(requestData: Dictionary<String, Any>,url:String)
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
 
        var urlStr:String = url
        
        urlStr = urlStr.replacingOccurrences(of: " ", with: "&")
        let request :NSMutableURLRequest = NSMutableURLRequest(url:NSURL(string:urlStr)! as URL)
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        var TokenValue = String()
        TokenValue = "Bearer " + MySingleton.shared.loginObject.access_token
        print(TokenValue)
        request.addValue(TokenValue, forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        if requestData.count != 0
        {
            do
            {
                let jsonData = try JSONSerialization.data(withJSONObject: requestData, options: JSONSerialization.WritingOptions.prettyPrinted)
                request.httpBody = jsonData
            }
            catch let error as NSError
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                }
            }
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest){ data,response,error in
            
            if error != nil
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error!.localizedDescription, completionHandler: nil)
                }
                
            }
            do
            {
                if (response as? HTTPURLResponse) != nil
                {
                    
                    //////////////////// JSON RESPONSE /////////////////
                    
                    
                    let jsonString = String(data: data!, encoding: .utf8)
                    print(jsonString!)
                    
                    //////////////////// JSON RESPONSE /////////////////
                    
                    
                    if data != nil
                    {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        
                        print(jsonData)
                        
                        if(jsonData["Status"] as! Bool == true)
                        {
                            
                            DispatchQueue.main.async {
                                MBProgressHUD.hide(for: self.view, animated: true)
//                                TrenderAlertVC.shared.presentAlertController(message: jsonData["Message"] as! String, completionHandler: nil)
                            }
                            
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                MBProgressHUD.hide(for: self.view, animated: true)
                                TrenderAlertVC.shared.presentAlertController(message: jsonData["Message"] as! String, completionHandler: nil)
                            }
                        }
                        
                        
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true)
                            TrenderAlertVC.shared.presentAlertController(message: error!.localizedDescription, completionHandler: nil)
                        }
                    }
                    
                }
            }
            catch let error as NSError
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                }
            }
        }
        
        task.resume()
    }
}


