//
//  ProfileViewFooter.swift
//  TrenderAlert
//
//  Created by HPL on 11/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

protocol ProfileViewFooterDelegate:class{
    func goToEditBioPage()
}

class ProfileViewFooter: UITableViewHeaderFooterView {

    @IBOutlet weak var editBioBtn: UIButton!
    static let reuseIdentifier = "ProfileViewFooter"
    weak var delegate:ProfileViewFooterDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        editBioBtn.addTarget(self, action: #selector(printString), for: .touchUpInside)
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @objc func printString(){
        delegate?.goToEditBioPage()
    }
    @IBAction func goToEdiBio(_ sender: UIButton) {
        
        
    }
    
}
