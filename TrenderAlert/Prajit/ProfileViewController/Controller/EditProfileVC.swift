//
//  EditProfileVC.swift
//  TrenderAlert
//
//  Created by HPL on 30/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import AFViewShaker
import GooglePlaces
import DropDown
import CountryPickerView


protocol EditProfileVCDelegate: class {
    func profileUpdated();
}

class EditProfileVC: UIViewController {
    
    @IBOutlet weak var lblFemale: UILabel!
    @IBOutlet weak var lblMale: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet var mobileNo: CountryPickerView!
    @IBOutlet var imgFemaleRadio: UIImageView!
    @IBOutlet var imgMaleRadio: UIImageView!
    //    @IBOutlet var txtConfirmPass: FormFields!
    //    @IBOutlet var txtPassword: FormFields!
    //    @IBOutlet var txtEmail: FormFields!
    @IBOutlet var txtLocation: FormFieldsEditProfile!
    @IBOutlet var txtMobileNo: FormFieldsEditProfile!
    //     @IBOutlet var txtMobileNo: FormFields!
    @IBOutlet var btnCancelPic: UIButton!
    @IBOutlet var btnSelectProfilePic: UIButton!
    @IBOutlet var firstName: FormFieldsEditProfile!
    @IBOutlet var lastName: FormFieldsEditProfile!
    @IBOutlet var birthDate: FormFieldsEditProfile!
    
    @IBOutlet weak var datePickerView: UIView!
    @IBOutlet weak var dobPicker: UIDatePicker!
    
    @IBOutlet weak var aboutMe: FormFieldTextView!
    @IBOutlet weak var education: FormFieldTextView!
    @IBOutlet weak var interest: FormFieldTextView!
    @IBOutlet weak var linksAndWebsite: FormFieldTextView!
    
    @IBOutlet weak var aboutMeHeight: NSLayoutConstraint!
    @IBOutlet weak var educationHeight: NSLayoutConstraint!
    @IBOutlet weak var interestHeight: NSLayoutConstraint!
    @IBOutlet weak var linksAndWebsiteHeight: NSLayoutConstraint!
    
    @IBOutlet weak var genderImageHeight: NSLayoutConstraint!
    
    
    let picker = UIImagePickerController()
    var location_arr = [String]()
    var str_latitude = String()
    var str_longitude = String()
    var gender = "F"
    var locationDropdown = DropDown()
    var signUpObject = SignUp()
    
    let aboutMePlaceholder = MySingleton.shared.selectedLangData.about_me
    let educationPlaceholder = MySingleton.shared.selectedLangData.what_is_your_education
    let interestPlaceholder = MySingleton.shared.selectedLangData.what_is_your_interest
    let linksAndWebsitePlaceholder = MySingleton.shared.selectedLangData.what_is_your_links_and_websites
    var changeFormTextViewHeight = true
    var countryCode = String()
    var userInfoObj = UserInfo()
    
    var ipadDevice = (UIDevice.current.userInterfaceIdiom == .pad) ? true : false
    var additionalHeight = 30.0
    
    weak var profileUpdateDelegate: EditProfileVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSettings()
        self.mobileNo.dataSource = self
        self.mobileNo.delegate = self
        
        txtMobileNo.img_IconPic.removeFromSuperview()
        if userInfoObj.data[0].CountryCode == nil{
            countryCode = self.mobileNo.selectedCountry.phoneCode
        }else{
            self.countryCode = userInfoObj.data[0].CountryCode!
            //            self.mobileNo.setCountryByPhoneCode(userInfoObj.data[0].CountryCode!)
            let code = self.countryCode.components(separatedBy: "+")
            self.mobileNo.setCountryByCode(code[0])
        }
        
        getProfileInfo()
        setupNavigationbar()
        //        let logo = UIImage(named: "nav_editProfile")
        //        let imageView = UIImageView(image:logo)
        //        self.navigationItem.titleView = imageView
        //        self.navigationController?.navigationBar.barStyle = .black
        //        self.navigationController?.navigationBar.tintColor = .white
    }
    
    func setupNavigationbar(){
        Utilities.setNavigationBarWithTitle(viewController: self, title: MySingleton.shared.selectedLangData.edit_bio)
        let leftButton = UIBarButtonItem(image: UIImage(named: "img_BackArrow"), style: .plain, target: self, action: #selector(goBack))
        navigationItem.leftBarButtonItem = leftButton
    }
    
    @objc func goBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if ipadDevice{
            self.changeTextViewHeight(self.interest.txtView_UserValue, self.interest, self.interestHeight,self.userInfoObj.data[0].Interest ?? "",additionalHeight: 28.0,initialHeight: true)
            
            self.changeTextViewHeight(self.aboutMe.txtView_UserValue, self.aboutMe, self.aboutMeHeight,self.userInfoObj.data[0].About ?? "",additionalHeight: 28.0,initialHeight: true)
            
            self.changeTextViewHeight(self.education.txtView_UserValue, self.education, self.educationHeight,self.userInfoObj.data[0].Education ?? "",additionalHeight: 28.0,initialHeight: true)
            
            self.changeTextViewHeight(self.linksAndWebsite.txtView_UserValue, self.linksAndWebsite, self.linksAndWebsiteHeight,self.userInfoObj.data[0].Links ?? "",additionalHeight: 28.0,initialHeight: true)
            
        }
        
    }
    
    override func viewWillLayoutSubviews()
    {
        //         self.btnSelectProfilePic.makeCornerRadius(radius: self.btnSelectProfilePic.frame.size.height/2)
        self.aboutMe.imgIconHeight.constant = self.txtLocation.img_IconPic.frame.height
        self.education.imgIconHeight.constant = self.txtLocation.img_IconPic.frame.height
        self.interest.imgIconHeight.constant = self.txtLocation.img_IconPic.frame.height
        self.linksAndWebsite.imgIconHeight.constant = self.txtLocation.img_IconPic.frame.height
        
        self.genderImageHeight.constant = self.txtLocation.img_IconPic.frame.height
        
        if changeFormTextViewHeight{
            aboutMeHeight.constant = txtMobileNo.frame.height
            educationHeight.constant = txtMobileNo.frame.height
            interestHeight.constant = txtMobileNo.frame.height
            linksAndWebsiteHeight.constant = txtMobileNo.frame.height
        }
    }
    
    func initialSettings()
    {
        self.picker.delegate = self
        self.txtMobileNo.txtFld_UserValue.keyboardType = .numberPad
        self.txtMobileNo.txtFld_UserValue.delegate = self
        self.firstName.txtFld_UserValue.delegate = self
        self.lastName.txtFld_UserValue.delegate = self
        
        self.self.txtMobileNo.txtFld_UserValue.placeholder = MySingleton.shared.selectedLangData.mobileNo
        self.self.firstName.txtFld_UserValue.placeholder = MySingleton.shared.selectedLangData.first_name
        self.self.lastName.txtFld_UserValue.placeholder = MySingleton.shared.selectedLangData.last_name
        self.self.birthDate.txtFld_UserValue.placeholder = MySingleton.shared.selectedLangData.birth_date
        self.self.txtLocation.txtFld_UserValue.placeholder = MySingleton.shared.selectedLangData.location
//        self.self.txtMobileNo.lbl_PlaceHolderTxt.text = MySingleton.shared.selectedLangData.mobileNo
        self.self.firstName.lbl_PlaceHolderTxt.text = MySingleton.shared.selectedLangData.first_name
        self.self.lastName.lbl_PlaceHolderTxt.text = MySingleton.shared.selectedLangData.last_name
        self.self.birthDate.lbl_PlaceHolderTxt.text = MySingleton.shared.selectedLangData.birth_date
        self.self.txtLocation.lbl_PlaceHolderTxt.text = MySingleton.shared.selectedLangData.location
        self.lblGender.text = MySingleton.shared.selectedLangData.gender
        self.lblMale.text = MySingleton.shared.selectedLangData.male
        self.lblFemale.text = MySingleton.shared.selectedLangData.female
        
        self.datePickerView.isHidden = true
        self.dobPicker.backgroundColor = .white
        self.dobPicker.set16YearValidation()
        self.dobPicker.datePickerMode = UIDatePicker.Mode.date
        self.birthDate.lbl_PlaceHolderTxt.isHidden = false
        
        self.txtLocation.txtFld_UserValue.addTarget(self, action: #selector(self.textFieldTextChanged(_:)), for: .editingChanged)
        self.txtLocation.txtFld_UserValue.delegate = self
        
        self.firstName.txtFld_UserValue.delegate = self
        
        //Mark: Profile Details field
        self.aboutMe.txtView_UserValue.delegate = self
        self.aboutMe.textViewPlaceholder = aboutMePlaceholder
        self.aboutMe.txtView_UserValue.text = aboutMePlaceholder
        
        self.education.txtView_UserValue.delegate = self
        self.education.textViewPlaceholder = educationPlaceholder
        self.education.txtView_UserValue.text = educationPlaceholder
        
        self.interest.txtView_UserValue.delegate = self
        self.interest.textViewPlaceholder = interestPlaceholder
        self.interest.txtView_UserValue.text = interestPlaceholder
        
        self.linksAndWebsite.txtView_UserValue.delegate = self
        self.linksAndWebsite.textViewPlaceholder = linksAndWebsitePlaceholder
        self.linksAndWebsite.txtView_UserValue.text = linksAndWebsitePlaceholder
        
        locationDropdown.anchorView = txtLocation
        
        //        locationDropdown.bottomOffset = CGPoint(x: 0, y:(locationDropdown.anchorView?.plainView.bounds.height)!)
        //        locationDropdown.topOffset = CGPoint(x: 0, y:-(locationDropdown.anchorView?.plainView.bounds.height)!)
        
        locationDropdown.bottomOffset = CGPoint(x: 0, y:(locationDropdown.anchorView?.plainView.bounds.height)!/2)
        locationDropdown.topOffset = CGPoint(x: 0, y:-(locationDropdown.anchorView?.plainView.bounds.height)!/2)
        
        dropdownSelection()
        
    }
    
    @IBOutlet weak var datePickerBottomView: UIView!
    
    @IBAction func didTapCancelButton(_ sender: UIButton) {
        self.datePickerView.isHidden = true
    }
    
    @IBAction func didTapSubmitButton(_ sender: UIButton) {
        self.datePickerView.isHidden = true
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let selectedDate = dateFormatter.string(from: self.dobPicker.date)
        print("Selected Date = \(selectedDate)")
        birthDate.txtFld_UserValue.text = "\(selectedDate)"
    }
    
    
    @IBAction func btn_selectProfilePic(_ sender: Any)
    {
        self.view.endEditing(true)
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: MySingleton.shared.selectedLangData.please_select_your_option, message: "", preferredStyle: .actionSheet)
        
        let cameraActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.camera, style: .default) { void in
            print("Camera")
            self.openCamera()
        }
        actionSheetControllerIOS8.addAction(cameraActionButton)
        
        let galleryActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.gallery, style: .default) { void in
            print("Gallery")
            self.openGallary()
        }
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.cancel, style: .default) { void in
            print("Cancel")
            
        }
        
        actionSheetControllerIOS8.addAction(galleryActionButton)
        actionSheetControllerIOS8.addAction(cancelActionButton)
        //        actionSheetControllerIOS8.popoverPresentationController?.sourceView = self.btnSelectProfilePic
        //        actionSheetControllerIOS8.popoverPresentationController?.sourceRect = self.btnSelectProfilePic.bounds
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    
    
    @IBAction func didTapDOBDatePicker(_ sender: UIButton) {
        
        self.datePickerView.isHidden = false
        
    }
    
    @IBAction func didTapSignUpButton(_ sender: UIButton) {
        validation()
    }
    
    
    @IBAction func tap_MaleChangeSign(_ sender: UITapGestureRecognizer? = nil)
    {
        if imgMaleRadio.image == #imageLiteral(resourceName: "radioOffYellow")
        {
            imgMaleRadio.image = #imageLiteral(resourceName: "radioOnYellow")
            imgFemaleRadio.image = #imageLiteral(resourceName: "radioOffYellow")
            self.gender = "M"
        }
    }
    
    @IBAction func tap_FemaleChangeSign(_ sender: UITapGestureRecognizer? = nil)
    {
        if imgFemaleRadio.image == #imageLiteral(resourceName: "radioOffYellow")
        {
            imgMaleRadio.image = #imageLiteral(resourceName: "radioOffYellow")
            imgFemaleRadio.image = #imageLiteral(resourceName: "radioOnYellow")
            self.gender = "F"
        }
    }
    
    func testData() {
        firstName.txtFld_UserValue.text = "test"
        lastName.txtFld_UserValue.text = "test"
        birthDate.txtFld_UserValue.text = "2009-01-17"
        txtMobileNo.txtFld_UserValue.text = "+917021465081"
        
    }
    
    func validation()
    {
        let viewArray : [UIView] = [firstName,lastName,birthDate,txtLocation,txtMobileNo]
        let obj_Validation = Validation()
        let viewsToShake = obj_Validation.validationForEdiProfile(viewList: viewArray)
        
        if(viewsToShake.count == 0)
        {
            if(str_latitude == "" || str_longitude == "")
            {
                TrenderAlertVC.shared.presentAlertController(message: MySingleton.shared.selectedLangData.please_enter_valid_location, completionHandler: nil)
                return
            }
            
            updateProfileDetails()
        }
        else
        {
            let viewShaker = AFViewShaker(viewsArray: viewsToShake)
            print(viewsToShake)
            viewShaker?.shake()
        }
    }
    
    func encodeToBase64String(image: UIImage) -> String
    {
        let imageData = image.jpegData(compressionQuality: 0.5)
        let imag_str:String = imageData!.base64EncodedString(options:[])
        //print(imag_str)
        return imag_str
    }
    
    
    
    func updateProfileDetails(){
        
        var parameters = [String:Any]()
        //(userInfoObj.data[0].UserName ?? "")
        parameters = ["FirstName": firstName.txtFld_UserValue.text!, "LastName": lastName.txtFld_UserValue.text!, "DOB":birthDate.txtFld_UserValue.text!, "Location": txtLocation.txtFld_UserValue.text!,"UserLocationLatitude": self.str_latitude,"UserLocationLongitude": self.str_longitude,"CountryCode":"\(self.countryCode)","Phone":self.countryCode + txtMobileNo.txtFld_UserValue.text!,"Gender":self.gender,"isChangeImage": "false","ImageList":[],"About":((self.aboutMe.txtView_UserValue.text! == aboutMePlaceholder) ? "": self.aboutMe.txtView_UserValue.text!),"Education":((self.education.txtView_UserValue.text! == educationPlaceholder) ? "" : self.education.txtView_UserValue.text!),"Interest":((self.interest.txtView_UserValue.text! == interestPlaceholder) ? "" : self.interest.txtView_UserValue.text!),"Links":((self.linksAndWebsite.txtView_UserValue.text! == linksAndWebsitePlaceholder) ? "" : self.linksAndWebsite.txtView_UserValue.text!)] as [String : Any]
        
        print("Params : ",parameters)
        
        WebServices().callUserService1(service: UserServices.editBio, urlParameter: "", parameters: parameters as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
            if(serviceResponse["Status"] != nil)
            {
                if(serviceResponse["Status"] as! Bool == true)
                {
                    self.profileUpdateDelegate?.profileUpdated()
                    //  let login = MySingleton.shared.loginObject.ProfilePic =
                    self.navigationController?.popViewController(animated: true)
                }
                else
                {
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SignUpToVerifyOTP" {
            let verifyOTPVC = segue.destination as! VerifyOTPViewController
            verifyOTPVC.signUpObject = self.signUpObject
        }
    }
    
    func getProfileInfo(){
        //        WebServices().callUserService(service: UserServices.getUserInfo, parameters: [:], isLazyLoading: false, isHeader: true, CallMethod: .get){ (serviceResponse, serviceData) in
        
        //            if serviceResponse["Status"] != nil{
        //                if (serviceResponse["Status"] as! Bool == true){
        //                    self.userInfoObj = WebServices().decodeDataToClass(data: serviceData, decodeClass: UserInfo.self)!
        
        self.firstName.txtFld_UserValue.text = self.userInfoObj.data[0].FirstName
        self.lastName.txtFld_UserValue.text  = self.userInfoObj.data[0].LastName
        self.birthDate.txtFld_UserValue.text = self.userInfoObj.data[0].DOB
        self.txtMobileNo.txtFld_UserValue.text = self.userInfoObj.data[0].Phone
        self.txtLocation.txtFld_UserValue.text = self.userInfoObj.data[0].Location
        self.str_latitude = (self.userInfoObj.data[0].UserLocationLatitude == nil) ? "" :String(format: "%d", self.userInfoObj.data[0].UserLocationLatitude!)
        self.str_longitude = (self.userInfoObj.data[0].UserLocationLongitude == nil) ? "" :String(format: "%d", self.userInfoObj.data[0].UserLocationLongitude!)
        
        if self.userInfoObj.data[0].CountryCode != nil {
            let PhoneWithoutCountryCode = self.userInfoObj.data[0].Phone!.replacingOccurrences(of: self.countryCode, with: "").trimmingCharacters(in: .whitespaces)
            self.txtMobileNo.txtFld_UserValue.text = PhoneWithoutCountryCode
        }
        
        if self.userInfoObj.data[0].Gender == "F"{
            self.gender = "F"
            self.tap_FemaleChangeSign()
        }
        if self.userInfoObj.data[0].Gender == "M"{
            self.gender = "M"
            self.tap_MaleChangeSign()
        }
        
        if !(self.userInfoObj.data[0].FirstName?.isEmpty ?? true){
            self.firstName.lbl_PlaceHolderTxt.isHidden = false
        }
        if !(self.userInfoObj.data[0].LastName?.isEmpty ?? true){
            self.lastName.lbl_PlaceHolderTxt.isHidden = false
        }
        
        if !(self.userInfoObj.data[0].DOB?.isEmpty ?? true){
            self.birthDate.lbl_PlaceHolderTxt.isHidden = false
        }
        if !(self.userInfoObj.data[0].Phone?.isEmpty ?? true){
            self.txtMobileNo.lbl_PlaceHolderTxt.isHidden = false
        }
        if !(self.userInfoObj.data[0].Location?.isEmpty ?? true){
            self.txtLocation.lbl_PlaceHolderTxt.isHidden = false
        }
        
        if !(self.userInfoObj.data[0].About?.isEmpty ?? true){
            self.aboutMe.lbl_PlaceHolderTxt.isHidden = false
            self.aboutMe.txtView_UserValue.text = self.userInfoObj.data[0].About
        }
        if !(self.userInfoObj.data[0].Education?.isEmpty ?? true){
            self.education.lbl_PlaceHolderTxt.isHidden = false
            self.education.txtView_UserValue.text = self.userInfoObj.data[0].Education
        }
        if !(self.userInfoObj.data[0].Interest?.isEmpty ?? true){
            self.interest.lbl_PlaceHolderTxt.isHidden = false
            self.interest.txtView_UserValue.text = self.userInfoObj.data[0].Interest
        }
        
        //comm
        
        self.changeTextViewHeight(self.interest.txtView_UserValue, self.interest, self.interestHeight,self.userInfoObj.data[0].Interest ?? "",additionalHeight: 24.0,initialHeight: true)
        
        self.changeTextViewHeight(self.aboutMe.txtView_UserValue, self.aboutMe, self.aboutMeHeight,self.userInfoObj.data[0].About ?? "",additionalHeight: 24.0,initialHeight: true)
        
        self.changeTextViewHeight(self.education.txtView_UserValue, self.education, self.educationHeight,self.userInfoObj.data[0].Education ?? "",additionalHeight: 24.0,initialHeight: true)
        
        self.changeTextViewHeight(self.linksAndWebsite.txtView_UserValue, self.linksAndWebsite, self.linksAndWebsiteHeight,self.userInfoObj.data[0].Links ?? "",additionalHeight: 24.0,initialHeight: true)
        
        self.changeFormTextViewHeight = false
        //
        
        if !(self.userInfoObj.data[0].Links?.isEmpty ?? true){
            self.linksAndWebsite.lbl_PlaceHolderTxt.isHidden = false
            self.linksAndWebsite.txtView_UserValue.text = self.userInfoObj.data[0].Links
        }
        
        
        
        //                }else{
        //                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
        //                }
        //            }else{
        //                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
        //            }
        //        }
        
    }
    
    
}


extension EditProfileVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    /*************************
     Method Name:  openGallary()
     Parameter: nil
     return type: nil
     Desc: This function opens the gallery.
     *************************/
    func openGallary()
    {
        picker.allowsEditing = false
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        present(picker, animated: true, completion: nil)
    }
    
    /*************************
     Method Name:  openCamera()
     Parameter: nil
     return type: nil
     Desc: This function opens the Camera.
     *************************/
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            present(picker, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    /*************************
     Method Name:  imagePickerControllerDidCancel()
     Parameter: UIImagePickerController
     return type: nil
     Desc: This function is called if user taps cancel button.
     *************************/
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    /*************************
     Method Name:  imagePickerController()
     Parameter: UIImagePickerController, [String : Any]
     return type: nil
     Desc: This function is called when user selects image from gallery.
     *************************/
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        //        print(chosenImage)
        self.btnSelectProfilePic.setBackgroundImage(chosenImage, for: UIControl.State.normal)
        self.btnCancelPic.isHidden = false
        dismiss(animated:true, completion: nil)
    }
}

extension EditProfileVC: UITextFieldDelegate,UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        
        switch textView {
        case aboutMe.txtView_UserValue:
            //            changeTextView(textView, aboutMe, aboutMeHeight, aboutMePlaceholder)
            changeTextViewHeight(textView, aboutMe, aboutMeHeight, aboutMePlaceholder)
        case education.txtView_UserValue:
            //            changeTextView(textView, education, educationHeight, educationPlaceholder)
            changeTextViewHeight(textView, education, educationHeight, educationPlaceholder)
        case interest.txtView_UserValue:
            //            changeTextView(textView, interest, interestHeight, interestPlaceholder)
            changeTextViewHeight(textView, interest, interestHeight, interestPlaceholder)
        case linksAndWebsite.txtView_UserValue:
            //            changeTextView(textView, linksAndWebsite, linksAndWebsiteHeight, linksAndWebsitePlaceholder)
            changeTextViewHeight(textView, linksAndWebsite, linksAndWebsiteHeight, linksAndWebsitePlaceholder)
        default:
            //            changeTextView(textView, aboutMe, aboutMeHeight, aboutMePlaceholder)
            changeTextViewHeight(textView, aboutMe, aboutMeHeight, aboutMePlaceholder)
            
        }
        
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (textView.text == aboutMePlaceholder) || (textView.text == educationPlaceholder) || (textView.text == interestPlaceholder) || (textView.text == linksAndWebsitePlaceholder){
            textView.text = ""
        }
        
        switch textView {
        case aboutMe.txtView_UserValue:
            aboutMe.lbl_PlaceHolderTxt.isHidden = false
        case education.txtView_UserValue:
            education.lbl_PlaceHolderTxt.isHidden = false
        case interest.txtView_UserValue:
            interest.lbl_PlaceHolderTxt.isHidden = false
        case linksAndWebsite.txtView_UserValue:
            linksAndWebsite.lbl_PlaceHolderTxt.isHidden = false
        default:
            aboutMe.lbl_PlaceHolderTxt.isHidden = false
        }
        
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        
        switch textView {
        case aboutMe.txtView_UserValue:
            showHideTextViewPlacholder(textView, aboutMe, aboutMePlaceholder)
        case education.txtView_UserValue:
            showHideTextViewPlacholder(textView, education, educationPlaceholder)
        case interest.txtView_UserValue:
            showHideTextViewPlacholder(textView, interest, interestPlaceholder)
        case linksAndWebsite.txtView_UserValue:
            showHideTextViewPlacholder(textView, linksAndWebsite, linksAndWebsitePlaceholder)
        default:
            showHideTextViewPlacholder(textView, aboutMe, aboutMePlaceholder)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        
        //        switch textView {
        //        case aboutMe.txtView_UserValue:
        //            changeTextViewHeight(textView, aboutMe, aboutMeHeight, aboutMePlaceholder)
        //        case education.txtView_UserValue:
        //            changeTextViewHeight(textView, education, educationHeight, educationPlaceholder)
        //        case interest.txtView_UserValue:
        //            changeTextViewHeight(textView, interest, interestHeight, interestPlaceholder)
        //        case linksAndWebsite.txtView_UserValue:
        //            changeTextViewHeight(textView, linksAndWebsite, linksAndWebsiteHeight, linksAndWebsitePlaceholder)
        //        default:
        //            changeTextViewHeight(textView, aboutMe, aboutMeHeight, aboutMePlaceholder)
        //        }
        
        if text == "\n"{
            textView.resignFirstResponder()
        }
        
        
        
        let maxLength = 160
        let currentString: NSString = textView.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: text) as NSString
        return newString.length <= maxLength
    }
    
    func showHideTextViewPlacholder(_ textView:UITextView,_ formField:FormFieldTextView,_ placeHolderText:String){
        if textView.text == ""{
            formField.lbl_PlaceHolderTxt.isHidden = true
            formField.txtView_UserValue.text = placeHolderText
        }else{
            formField.lbl_PlaceHolderTxt.isHidden = false
        }
    }
    
    //    func changeTextView(_ textView:UITextView,_ formField:FormFieldTextView,_ heightConstraint:NSLayoutConstraint,_ placeHolderText:String,_ compulsoryUpdate:Bool = false){
    //
    //
    //        let fixedWidth = textView.frame.size.width
    //        textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
    //        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
    //        var newFrame = textView.frame
    //        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
    //        textView.frame = newFrame
    //         if ((newSize.height + 20) > txtMobileNo.frame.height) || compulsoryUpdate{
    ////        if ((newSize.height + 20) > txtMobileNo.frame.height) || compulsoryUpdate{
    //            heightConstraint.constant = newSize.height + 20
    //        }
    //
    //        self.view.layoutIfNeeded()
    //    }
    
    func changeTextViewHeight(_ textView:UITextView,_ formField:FormFieldTextView,_ heightConstraint:NSLayoutConstraint,_ placeHolderText:String,additionalHeight:CGFloat = 30.0,initialHeight:Bool = false){
        
        let extraHeight:CGFloat = (ipadDevice) ? (35.0) : 30.0
        DispatchQueue.main.async {
            
            let fixedWidth = textView.frame.size.width
            textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            
            let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            var newFrame = textView.frame
            newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            textView.frame = newFrame
            
            heightConstraint.constant = newSize.height + extraHeight
            
            if (textView.numberOfLines() == 1){
                heightConstraint.constant = self.txtMobileNo.frame.height + 5
            }
            //    formField.imgIconHeight.constant = self.txtMobileNo.img_IconPic.frame.height
            self.view.layoutIfNeeded()
        }
    }
    
    
    
    /*************************
     Method Name:  textFieldTextChanged()
     Parameter: UITextField
     return type: nil
     Desc: This function cis called when text Field Text is Changed.
     *************************/
    //    @objc func textFieldTextChange(_ sender : UITextField)
    //    {
    //        sender.text = sender.text?.lowercased()
    //    }
    //
    
    @objc func textFieldTextChanged(_ sender : UITextField)
    {
        
        if(txtLocation.txtFld_UserValue.isEditing)
        {
            if  txtLocation.txtFld_UserValue.text!.isEmpty
            {
                self.locationDropdown.hide()
            }
            else
            {
                let getAddress = GetAddress()
                self.location_arr.removeAll()
                //self.locationDropdown.dataSource = self.location_arr
                getAddress.getAddress(location_str: sender.text!) { (locationData) in
                    // self.location_arr = locationData
                    for location in locationData
                    {
                        self.location_arr.append(location["address"]!)
                    }
                    print(self.location_arr)
                    self.locationDropdown.dataSource = []
                    self.locationDropdown.dataSource = self.location_arr
                    
                    if !sender.text!.isEmpty
                    {
                        self.locationDropdown.show()
                    }
                }
                
            }
        }
    }
    
    func dropdownSelection()
    {
        locationDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.txtLocation.txtFld_UserValue.text! = item
            GetAddress().getLocationFromAddressString(addressStr: self.txtLocation.txtFld_UserValue.text!) { (clLocationCoordinate2D,zipcode) in
                self.str_latitude = String(clLocationCoordinate2D.latitude)
                self.str_longitude = String(clLocationCoordinate2D.longitude)
                
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == self.firstName.txtFld_UserValue || textField == self.lastName.txtFld_UserValue{
            let maxLength = 20
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        if textField == self.txtMobileNo.txtFld_UserValue{
            
            let maxLength = 15
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        
        if textField != self.txtLocation.txtFld_UserValue{
            let maxLength = 160
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        
        return true
    }
    
}


extension EditProfileVC: CLLocationManagerDelegate {
    /*************************
     Method Name:  locationManager()
     Parameter: CLLocationManager, CLAuthorizationStatus
     return type: nil
     Desc: This function cis called when didChangeAuthorization.
     *************************/
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            manager.startUpdatingLocation()
        }
    }
    
    /*************************
     Method Name:  locationManager()
     Parameter: CLLocationManager, CLLocation
     return type: nil
     Desc: This function cis called when didUpdateLocations.
     *************************/
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        manager.stopMonitoringSignificantLocationChanges()
        manager.stopUpdatingLocation()
        str_latitude = (String(locations.last!.coordinate.latitude))
        str_longitude = (String(locations.last!.coordinate.longitude))
        GetAddress().getAddressFromLocation(locations.last!) { (address) in
            self.txtLocation.txtFld_UserValue.text! = address
        }
    }
}

extension EditProfileVC: CountryPickerViewDelegate {
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        // Only countryPickerInternal has it's delegate set
        let title = "Selected Country"
        let message = "Name: \(country.name) \nCode: \(country.code) \nPhone: \(country.phoneCode)"
        self.countryCode = "\(country.code)\(country.phoneCode)"
        print(self.countryCode)
    }
}

extension EditProfileVC: CountryPickerViewDataSource {
    func preferredCountries(in countryPickerView: CountryPickerView) -> [Country] {
        if countryPickerView.tag == mobileNo.tag {
            var countries = [Country]()
            ["NG", "US", "GB"].forEach { code in
                if let country = countryPickerView.getCountryByCode(code) {
                    countries.append(country)
                }
            }
            return countries
        }
        return []
    }
    
    func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String? {
        if countryPickerView.tag == mobileNo.tag {
            return "Preferred title"
        }
        return nil
    }
    
    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        return "Select a Country"
    }
    
    
}

