//
//  EditBioVC.swift
//  TrenderAlert
//
//  Created by HPL on 04/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import AFViewShaker

class EditBioVC: UIViewController {

    @IBOutlet weak var editBioTableView: UITableView!
    
    
    
    var userInfoObj = UserInfo()
    
//    let editBioQuestionAnswers = [["user_name","About me",""],["education","What is your education?",""],["interest","What is your interest?",""],["religion-1","What is your religion?",""],["web","What is your Links and Websites?",""]]
    
    let editBioQuestionAnswers = [["user_name","About me",""],["education","What is your education?",""],["interest","What is your interest?",""],["web","What is your Links and Websites?",""]]
    
    var newAnswers:[String] = []
    
    
    var footerHeight = (UIDevice.current.userInterfaceIdiom == .pad) ? 0.08 : 0.1

    static let textViewPlaceholderText = "This field cannot be empty."
    var isError = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupTableView()
        newAnswers = Array(repeating: "", count: editBioQuestionAnswers.count)
        if MySingleton.shared.loginObject.IsFirstTime != "True"
        {
            getUserInfo()
        }
        
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationItem.hidesBackButton = true
        self.navigationItem.hidesBackButton = true
        let logo = UIImage(named: "edit_bio")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationController?.navigationBar.tintColor = .white
        
        let rightBarButton = UIButton()
        rightBarButton.setTitle("Skip", for: .normal)
        rightBarButton.addTarget(self, action: #selector(didTapSkip), for: .touchUpInside)
        let item1 = UIBarButtonItem()
        item1.customView = rightBarButton
        self.navigationItem.rightBarButtonItems = [item1]
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getUserInfo()
    }
   
    @objc func didTapSkip(button: UIButton) {
        self.performSegue(withIdentifier: "EditBioToWelcome", sender: self)
    }
    
    func setupTableView(){
        editBioTableView.delegate = self
        editBioTableView.dataSource = self
        editBioTableView.register(UINib(nibName: EditBioTVC.reuseIdentifier, bundle: nil), forCellReuseIdentifier: EditBioTVC.reuseIdentifier)
        
        let footer = UINib.init(nibName: "EditBioFooterView", bundle: nil)
        editBioTableView.register(footer, forHeaderFooterViewReuseIdentifier: "EditBioFooterView")
        
        editBioTableView.estimatedRowHeight = 200
        editBioTableView.rowHeight = UITableView.automaticDimension
    }
    
   
    
    func validateBioInfo()->Bool{
        
        self.isError = false
        
        for i in newAnswers.indices{
            let val = newAnswers[i].trimmingCharacters(in: .whitespaces)
            if val == ""{
                newAnswers[i] = ""
                self.isError = true
            }
        }
        
    
        DispatchQueue.main.async {
            self.editBioTableView.reloadData()
        }
        
        return self.isError
    }
    
    func saveEditBio() {
        //Validation
//        if validateBioInfo(){
//            return
//        }
        
        var parameters = [String:Any]()
        parameters = ["About":self.newAnswers[0],"Education":self.newAnswers[1],"Interest":self.newAnswers[2],"Religion":"","Links": newAnswers[3]] as [String : Any]
        
        WebServices().callUserService(service: UserServices.updateEditBioInfo, urlParameter: "",parameters:parameters as [String:AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){ (serviceResponse, serviceData) in
            print(serviceResponse)
            if(serviceResponse["Status"] != nil)
            {
                if(serviceResponse["Status"] as! Bool == true)
                {
                    
//                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                    self.performSegue(withIdentifier: "EditBioToSetTrendAlert", sender: self)
                }
                else
                {
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
        
        
    }
    
    func getUserInfo(){
        
        let param = ["UserID":MySingleton.shared.loginObject.UserID]
        
        WebServices().callUserService(service: UserServices.getUserInfo, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: true, isHeader: true, CallMethod: .post){ (serviceResponse, serviceData) in
            
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    self.userInfoObj = WebServices().decodeDataToClass(data: serviceData, decodeClass: UserInfo.self)!
                    self.newAnswers[0] = self.userInfoObj.data[0].About ?? ""
                    self.newAnswers[1] = self.userInfoObj.data[0].Education ?? ""
                    self.newAnswers[2] = self.userInfoObj.data[0].Interest ?? ""
//                    self.newAnswers[3] = self.userInfoObj.data[0].Religion ?? ""
                    self.newAnswers[3] = self.userInfoObj.data[0].Links ?? ""
                    
                    DispatchQueue.main.async {
                        self.editBioTableView.reloadData()
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EditBioVC:UITableViewDataSource,UITableViewDelegate,EditBioFooterViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return editBioQuestionAnswers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: EditBioTVC.reuseIdentifier, for: indexPath) as! EditBioTVC
        cell.iconImage.image = UIImage(named: editBioQuestionAnswers[indexPath.row][0])
        cell.questionLbl.text = editBioQuestionAnswers[indexPath.row][1]
        cell.answerTxtView.text = editBioQuestionAnswers[indexPath.row][2]
        cell.answerTxtView.tag = indexPath.row
        cell.textViewPlaceholderText = EditBioVC.textViewPlaceholderText
        if newAnswers[indexPath.row].trimmingCharacters(in: .whitespaces) != ""{
            cell.answerTxtView.text = newAnswers[indexPath.row]
        }
        cell.isError = self.isError
//        cell.setupTextView(text: newAnswers[indexPath.row])
        cell.answerTxtView.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: "EditBioFooterView") as! EditBioFooterView
        footer.delegate = self
        return footer
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return UIScreen.main.bounds.height * CGFloat(footerHeight)
    }
    
}

extension EditBioVC:UITextViewDelegate{
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {

        if text == "\n"{
            textView.resignFirstResponder()
        }
        
        let maxLength = 160
        let currentString: NSString = textView.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: text) as NSString
        
        if newString.length >= maxLength{
            return false
        }
        
        let textFieldText: NSString = (textView.text ?? "") as NSString
        let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: text)
        newAnswers[textView.tag] = txtAfterUpdate
        
        return true
    }
    
    func textViewDidChange(_ textView:UITextView){
        let size = textView.bounds.size
        let newSize = textView.sizeThatFits(CGSize(width: size.width, height: CGFloat.greatestFiniteMagnitude))

        if size.height != newSize.height{
            UIView.setAnimationsEnabled(false)
            editBioTableView?.beginUpdates()
            editBioTableView?.endUpdates()
            UIView.setAnimationsEnabled(true)

            let indexpath = IndexPath(row: textView.tag, section: 0)
            
            editBioTableView?.scrollToRow(at: indexpath, at: .bottom, animated: false)
           
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == EditBioVC.textViewPlaceholderText{
            textView.text = ""
            textView.textColor = UIColor.black
            
        }
    }
    
//    func textViewDidEndEditing(_ textView: UITextView) {
//        if textView.text == ""{
//            textView.text = EditBioVC.textViewPlaceholderText
//            textView.textColor = UIColor.red
//
//        }
//    }
}




