//
//  UserInfo.swift
//  TrenderAlert
//
//  Created by HPL on 22/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class UserInfoData: NSObject, Codable{
    
    var DOB:String?
    var FirstName:String?
    var LastName:String?
    var Gender:String?
    var Location:String?
    var UserName:String?
    var ProfilePic:String?
    var About:String?
    var Education:String?
    var Interest:String?
    var Religion:String?
    var Links:String?
    var PostedTrends = 0
    var TotalFollower = 0
    var TotalFollowings = 0
    var TotalFriends = 0
    var DateCreated:String?
    var IsVerified:Bool?
    var Phone:String?
    var UserLocationLatitude:Double?
    var UserLocationLongitude:Double?
    var UserProfileImage:String?
    var CoverPic:String?
    var UserProfileId:Int?
    var FoldersList = [Folders]()
    var CountryCode : String?
    var IsFriend:Bool = false
    var FriendReqStatus:Int = 0
    var FriendRequestID:Int = 0
    
    enum CodingKeys:String,CodingKey {
        case DOB = "DOB"
        case FirstName = "Firstname"
        case LastName = "Lastname"
        case Gender = "Gender"
        case Location = "Location"
        case UserName = "UserName"
//        case ProfilePic = "UserProfileImage"
        case About = "About"
        case Education = "Education"
        case Interest = "Interest"
        case Religion = "Religion"
        case Links = "Links"
        case PostedTrends = "PostedTrends"
        case TotalFollower = "TotalFollower"
        case TotalFollowings = "TotalFollowings"
        case TotalFriends = "TotalFriends"
        case DateCreated = "DateCreated"
        case Phone = "Phone"
        case UserLocationLatitude = "UserLocationLatitude"
        case UserLocationLongitude = "UserLocationLongitude"
        case CoverPic = "CoverPic"
        case UserProfileImage = "UserProfileImage"
        case FoldersList = "Folders"
        case UserProfileId = "UserProfileId"
        case CountryCode = "CountryCode"
        case IsFriend = "IsFriend"
        case FriendReqStatus = "FriendReqStatus"
        case FriendRequestID = "FriendRequestID"
    }
}

class Folders:NSObject,Codable{
    
    var FolderId:Int = 0
    var FolderName:String?
    var UserCreated:String?
    
    enum CodingKeys:String,CodingKey{
        case FolderId = "FolderId"
        case FolderName = "FolderName"
        case UserCreated = "UserCreated"
    }
}


class UserInfo:NSObject, Codable{
    var data = [UserInfoData]()
    
    enum CodingKeys:String,CodingKey {
        case data = "data"
    }
}

struct TagTrenderModel {
    var id = Int()
    var FirstName = String()
    var LastName = String()
    var ProfilePic = String()
    var isSelected = Bool()
}

struct TagFriendModel {
    var id = Int()
    var FirstName = String()
    var LastName = String()
    var ProfilePic = String()
    var isSelected = Bool()
}

