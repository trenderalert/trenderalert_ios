//
//  BlockListModel.swift
//  TrenderAlert
//
//  Created by HPL on 30/05/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

class BlockList:NSObject,Codable{
    var data = [BlockListData]()
    
    enum CodingKeys:String,CodingKey{
        case data = "data"
    }
}

class BlockListData:NSObject,Codable{
    
    var Username:String?
    var Firstname:String?
    var Lastname:String?
    var ProfileImage:String?
    var BlockedUserId:Int?
    
    enum CodingKeys:String,CodingKey{
        case Username      = "Username"
        case Firstname     = "Firstname"
        case Lastname      = "Lastname"
        case ProfileImage  = "ProfileImage"
        case BlockedUserId = "BlockedUserId"
    }
}
