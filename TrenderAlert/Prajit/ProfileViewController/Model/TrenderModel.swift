//
//  TrenderModel.swift
//  TrenderAlert
//
//  Created by HPL on 28/03/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

class TrenderData:NSObject,Codable{
    var Username:String?
    var Firstname:String?
    var Lastname:String?
    var EmailID:String?
    var ProfileImage:String?
    var Location:String?
    var UserProfileID:Int?
    var AreYouFollowing:Bool = false
    var HaveYouBlocked:Bool = false
    var IsFollowRequested: Bool = false
    
    enum CodingKeys:String,CodingKey{
        
        case Username = "Username"
        case Firstname = "Firstname"
        case Lastname = "Lastname"
        case EmailID = "EmailID"
        case ProfileImage = "ProfileImage"
        case Location = "Location"
        case UserProfileID = "UserProfileID"
        case AreYouFollowing = "AreYouFollowing"
        case HaveYouBlocked = "HaveYouBlocked"
        case IsFollowRequested = "IsFollowRequested"
    }
}

class TrenderModel:NSObject,Codable{
    var data = [TrenderData]()
    enum CodingKeys:String,CodingKey{
        case data = "data"
    }
}
