//
//  GroupDetailMemberImagesCVC.swift
//  TrenderAlert
//
//  Created by HPL on 18/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class GroupDetailMemberImagesCVC: UICollectionViewCell {

    @IBOutlet weak var memberImage: UIImageView!
    
    static let reuseIdentifier = "GroupDetailMemberImagesCVC"
    
    
    override var bounds: CGRect{
        didSet{
            self.layoutIfNeeded()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // self.memberImage.makeCornerRadius(radius: memberImage.frame.height / 2)
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        self.circularImageView()
    }
    
    func circularImageView(){
        self.memberImage.makeCornerRadius(radius: memberImage.frame.height / 2)
    }
    
    func setup(){
        self.circularImageView()
//        self.memberImage.makeCornerRadius(radius: memberImage.frame.height / 2)
    }

}
