//
//  AddGroupMemberFooter.swift
//  TrenderAlert
//
//  Created by HPL on 06/03/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class AddGroupMemberFooter: UITableViewHeaderFooterView {
    
    //Outlet
    @IBOutlet weak var footerBtn: UIButton!
    static let reuseIdentifier = "AddGroupMemberFooter"

}
