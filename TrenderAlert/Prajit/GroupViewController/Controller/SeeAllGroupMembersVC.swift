//
//  SeeAllGroupMembersVC.swift
//  TrenderAlert
//
//  Created by HPL on 04/03/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import SDWebImage
import DropDown

class SeeAllGroupMembersVC: UIViewController {

    //Outlets
    @IBOutlet weak var membersBtn: UIButton!
    @IBOutlet weak var adminBtn: UIButton!
    @IBOutlet weak var searchByNameTxtField: UITextField!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var adminGroupMemberTblView: UITableView!
    @IBOutlet weak var noDataLbl: UILabel!
    //Properties
    let adminMemberFontSize = (UIDevice.current.userInterfaceIdiom == .pad) ? 18 : 13
    
    let noOfRecordPerRequest = 10
    var totalAdminCount = 0
    var totalMemberCount = 0
    
    var searchText = ""
    
    var isAdminReqInProgress = false
    var isMemberReqInProgress = false
    
    var currentView = 1
    let memberView  = 1
    let adminView   = 2
    
    var groupId = 0
    var isUserAdmin = false
    
    var groupMemberList = GroupAdminAndMemberListModel()
    var groupAdminList  = GroupAdminAndMemberListModel()
    
    var userId = MySingleton.shared.loginObject.UserID
    
    var noOfMembers = 0{
        didSet{
            if currentView == memberView{
                DispatchQueue.main.async {
                    self.adminGroupMemberTblView.reloadData()
                }
            }
        }
    }
    
    var noOfAdmins = 0{
        didSet{
            if currentView == adminView{
                DispatchQueue.main.async {
                    self.adminGroupMemberTblView.reloadData()
                }
            }
        }
    }
    
//    let imageTapGesture = UITapGestureRecognizer(target: self, action: #selector(showZoomedImage))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        userId = "130044"
        userId = MySingleton.shared.loginObject.UserID
        setupNavigationbar()
        setupTableView()
        self.membersBtn.setTitle(MySingleton.shared.selectedLangData.members, for: .normal)
        self.adminBtn.setTitle(MySingleton.shared.selectedLangData.admins, for: .normal)
        searchByNameTxtField.leftPadding(paddingSize: 15)
        searchByNameTxtField.delegate = self
         self.searchByNameTxtField.placeholder = MySingleton.shared.selectedLangData.search_by_name
        adminBtn.tag = 2
        membersBtn.tag = 1
        getMembers(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: false)
        getAdmins(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: true)
    }
    
    func setupNavigationbar(){
        Utilities.setNavigationBarWithTitle(viewController: self, title: MySingleton.shared.selectedLangData.All_Group_Members)
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "img_BackArrow"), style: .plain, target: self, action: #selector(goBack))
        
        navigationItem.leftBarButtonItem = leftButton
    }
    
    @objc func goBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupTableView(){
        self.adminGroupMemberTblView.delegate = self
        self.adminGroupMemberTblView.dataSource = self
        
        self.adminGroupMemberTblView.tableFooterView = UIView()
        self.adminGroupMemberTblView.register(UINib(nibName: GroupMembersTVC.reuseIdentifier, bundle: nil), forCellReuseIdentifier: GroupMembersTVC.reuseIdentifier)
    }
    
    func getMembers(Skip:Int,Take:Int,lazyLoading:Bool,SearchString:String = ""){
        if (groupMemberList.data.count >= totalMemberCount) || isMemberReqInProgress{
            return
        }
        
        self.isMemberReqInProgress = true
        let param = ["GroupID":groupId,"Search":searchText,"Skip":Skip,"Take":Take] as [String : Any]
        
        WebServices().callUserService(service: UserServices.getGroupMemberList, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: lazyLoading, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    let temp = WebServices().decodeDataToClass(data:serviceData, decodeClass: GroupAdminAndMemberListModel.self)!
                    self.groupMemberList.data += temp.data
                    self.totalMemberCount = serviceResponse["TotalRecords"] as! Int
                    self.noOfMembers += 1
                    self.isMemberReqInProgress = false
                    
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func getAdmins(Skip:Int,Take:Int,lazyLoading:Bool,SearchString:String = ""){
        if (groupAdminList.data.count >= totalAdminCount) || isAdminReqInProgress{
            return
        }
        
        self.isAdminReqInProgress = true
        let param = ["GroupID":groupId,"Search":searchText,"Skip":Skip,"Take":Take] as [String : Any]
        
        WebServices().callUserService(service: UserServices.getGroupAdminList, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: lazyLoading, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    let temp = WebServices().decodeDataToClass(data:serviceData, decodeClass: GroupAdminAndMemberListModel.self)!
                    self.groupAdminList.data += temp.data
                    self.totalAdminCount = serviceResponse["TotalRecords"] as! Int
                    self.noOfAdmins += 1
                    self.isAdminReqInProgress = false
                    
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    @IBAction func toggleMemberAdminTab(_ sender: UIButton) {
        if sender.tag == memberView{
            showMemberView()
            currentView = memberView
        }else{
            showAdminView()
            currentView = adminView
        }
        self.adminGroupMemberTblView.reloadData()
    }
    
    func showMemberView(){
//        print("Font Size: ",adminMemberFontSize)
        
//        let fontFamilyNames = UIFont.familyNames
//        for familyName in fontFamilyNames {
//            print("------------------------------")
//            print("Font Family Name = [\(familyName)]")
//            let names = UIFont.fontNames(forFamilyName: familyName)
//            print("Font Names = [\(names)]")
//        }
        
        self.membersBtn.backgroundColor = UIColor.baseColor
        self.membersBtn.titleLabel?.font  = UIFont(name: "OpenSans", size: CGFloat(adminMemberFontSize))
        
        self.adminBtn.backgroundColor = UIColor.white
        self.adminBtn.titleLabel?.font = UIFont(name: "OpenSans-Light", size: CGFloat(adminMemberFontSize))
    }
    
    func showAdminView(){

        self.adminBtn.backgroundColor = UIColor.baseColor
        self.adminBtn.titleLabel?.font  = UIFont(name: "OpenSans", size: CGFloat(adminMemberFontSize))
        
        self.membersBtn.backgroundColor = UIColor.white
        self.membersBtn.titleLabel?.font = UIFont(name: "OpenSans-Light", size: CGFloat(adminMemberFontSize))
    }
    
    
}

extension SeeAllGroupMembersVC:UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate{
    
    

    
    @objc func showZoomedImage(_ sender:UITapGestureRecognizer){
        
        var imageUrl = ""
        if currentView == memberView{
            imageUrl = groupMemberList.data[(sender.view?.tag)!].ProfileImage ?? ""
        }else{
            imageUrl = groupAdminList.data[(sender.view?.tag)!].ProfileImage ?? ""
        }
        let zoomImageView = ZoomImageVC(nibName: "ZoomImageVC", bundle: nil)
        zoomImageView.imageUrl = imageUrl
        self.navigationController?.pushViewController(zoomImageView, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if currentView == memberView{
            if groupMemberList.data.isEmpty && !isMemberReqInProgress{
             noDataLbl.isHidden = false
            }else{
                noDataLbl.isHidden = true
            }
            
        }else{
            if groupAdminList.data.isEmpty && !isAdminReqInProgress{
                noDataLbl.isHidden = false
            }else{
                noDataLbl.isHidden = true
            }
        }
        
        return (currentView == memberView) ? groupMemberList.data.count : groupAdminList.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if currentView == memberView{
            let cell = tableView.dequeueReusableCell(withIdentifier: GroupMembersTVC.reuseIdentifier, for: indexPath) as! GroupMembersTVC
            cell.memberNameLbl.text = "\(groupMemberList.data[indexPath.row].Firstname ?? "") \(groupMemberList.data[indexPath.row].Lastname ?? "")"
            cell.memberImage.sd_setImage(with: URL(string: self.groupMemberList.data[indexPath.row].ProfileImage ?? ""), placeholderImage: UIImage(named: "img_selectProfilePic"), options: [.continueInBackground], completed: nil)
            cell.memberMoreOptionBtn.addTarget(self, action: #selector(showMoreOptions(_:)), for: .touchUpInside)
            cell.memberMoreOptionBtn.tag = indexPath.row
            cell.memberImage.tag = indexPath.row
            let imageTapGesture = UITapGestureRecognizer(target: self, action: #selector(showZoomedImage))
            cell.memberImage.addGestureRecognizer(imageTapGesture)
            cell.memberImage.isUserInteractionEnabled = true
            cell.setup()
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: GroupMembersTVC.reuseIdentifier, for: indexPath) as! GroupMembersTVC
            cell.memberNameLbl.text = "\(groupAdminList.data[indexPath.row].Firstname ?? "") \(groupAdminList.data[indexPath.row].Lastname ?? "")"
            cell.memberImage.sd_setImage(with: URL(string: self.groupAdminList.data[indexPath.row].ProfileImage ?? ""), placeholderImage: UIImage(named: "img_selectProfilePic"), options: [.continueInBackground], completed: nil)
            cell.memberMoreOptionBtn.addTarget(self, action: #selector(showMoreOptions(_:)), for: .touchUpInside)
            cell.memberMoreOptionBtn.tag = indexPath.row
            cell.memberImage.tag = indexPath.row
            let imageTapGesture = UITapGestureRecognizer(target: self, action: #selector(showZoomedImage))
            cell.memberImage.addGestureRecognizer(imageTapGesture)
            cell.memberImage.isUserInteractionEnabled = true
            cell.setup()
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewProfile(index: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch currentView {
        case memberView:
            if indexPath.row == (groupMemberList.data.count - 1){
                getMembers(Skip: groupMemberList.data.count, Take: noOfRecordPerRequest, lazyLoading: true)
            }
        case adminView:
            if indexPath.row == (groupAdminList.data.count - 1){
                getAdmins(Skip: groupAdminList.data.count, Take: noOfRecordPerRequest, lazyLoading: true)
            }
        default:
            if indexPath.row == (groupMemberList.data.count - 1){
                getMembers(Skip: groupMemberList.data.count, Take: noOfRecordPerRequest, lazyLoading: true)
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        if currentView == memberView{
            self.groupMemberList.data = []
            self.noOfMembers = 0
            self.totalMemberCount = 1
            searchText = updatedString!
            getMembers(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: true,SearchString: updatedString!)
        }else{
            self.groupAdminList.data = []
            self.noOfAdmins = 0
            self.totalAdminCount = 1
            searchText = updatedString!
            getAdmins(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: true,SearchString: updatedString!)
        }
        
        return true
    }
    
    @objc func showMoreOptions(_ sender:UIButton){
        
        var options:[String] = []
        
        let showOption = ["View Profile"]
        var memberOnlyOption:[String] = []
        var currentUserProfileId = 0
     //   MySingleton.shared.selectedLangData.Unmute_Member
        if currentView == memberView{ // member list
            let memberOptions:[String] = [((groupMemberList.data[sender.tag].IsMuted) ? MySingleton.shared.selectedLangData.Unmute_Member:MySingleton.shared.selectedLangData.mute_member),MySingleton.shared.selectedLangData.make_admin,MySingleton.shared.selectedLangData.remove_member,MySingleton.shared.selectedLangData.view_profile]
            currentUserProfileId = groupMemberList.data[sender.tag].UserProfileId!
            memberOnlyOption = [((groupMemberList.data[sender.tag].IsMuted) ? MySingleton.shared.selectedLangData.Unmute_Member:MySingleton.shared.selectedLangData.mute_member),MySingleton.shared.selectedLangData.view_profile]
            options = memberOptions
        }else{ // admin list
            let adminOptions:[String] = [((groupAdminList.data[sender.tag].IsMuted) ? MySingleton.shared.selectedLangData.Unmute_Member:MySingleton.shared.selectedLangData.mute_member),MySingleton.shared.selectedLangData.remove_member,MySingleton.shared.selectedLangData.view_profile]
            currentUserProfileId = groupAdminList.data[sender.tag].UserProfileId!
            memberOnlyOption = [((groupAdminList.data[sender.tag].IsMuted) ? MySingleton.shared.selectedLangData.Unmute_Member:MySingleton.shared.selectedLangData.mute_member),MySingleton.shared.selectedLangData.view_profile]
            options = adminOptions
        }
        
        ///////////////// Do not change the order /////////////////////////
        if !isUserAdmin{
            options = memberOnlyOption
        }
        
        if Int(self.userId) == currentUserProfileId{
            options = showOption
        }
       ////////////////////////////////////////////////////////////
        
        let dropDown = DropDown()
        dropDown.anchorView = sender
        dropDown.dataSource = options
        dropDown.backgroundColor = UIColor.white
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.selectionAction = {[unowned self] (index:Int,item:String) in
//            print("Selected item: \(item) at index: \(index)")
            
            if Int(self.userId) == currentUserProfileId{
                self.viewProfile(index: sender.tag)
                return
            }
            
            if self.currentView == self.memberView,self.isUserAdmin{
                
                    switch item {
                    case options[0]:
                        //self.muteMember(index:sender.tag, isMemberView: true, userProfileId: self.groupMemberList.data[sender.tag].UserProfileId!, isMute: self.groupMemberList.data[sender.tag].IsMuted)
                        if self.groupMemberList.data[sender.tag].IsMuted {
                            let confirmationAlert = UIAlertController(title: "TrenderAlert", message: MySingleton.shared.selectedLangData.Are_you_sure_you_want_to_unmute_this_user, preferredStyle: .alert)
                            let yesAction = UIAlertAction(title: MySingleton.shared.selectedLangData.yes, style: .destructive) { (yesAction) in
                                self.muteMember(index:sender.tag, isMemberView: true, userProfileId: self.groupMemberList.data[sender.tag].UserProfileId!, isMute: self.groupMemberList.data[sender.tag].IsMuted)
                            }
                            let noAction = UIAlertAction(title: MySingleton.shared.selectedLangData.no, style: .cancel) { (noAction) in
                                
                            }
                            confirmationAlert.addAction(yesAction)
                            confirmationAlert.addAction(noAction)
                            self.present(confirmationAlert, animated: true, completion: nil)
                        }
                        else {
                            let confirmationAlert = UIAlertController(title: MySingleton.shared.selectedLangData.TrenderAlert, message: MySingleton.shared.selectedLangData.Are_you_sure_you_want_to_mute_this_user, preferredStyle: .alert)
                            let yesAction = UIAlertAction(title: "Yes", style: .destructive) { (yesAction) in
                                self.muteMember(index:sender.tag, isMemberView: true, userProfileId: self.groupMemberList.data[sender.tag].UserProfileId!, isMute: self.groupMemberList.data[sender.tag].IsMuted)
                            }
                            let noAction = UIAlertAction(title: "No", style: .cancel) { (noAction) in
                                
                            }
                            confirmationAlert.addAction(yesAction)
                            confirmationAlert.addAction(noAction)
                            self.present(confirmationAlert, animated: true, completion: nil)
                        }
                    case options[1]:
                        self.makeAdmin(index: sender.tag)
                    case options[2]:
                        let confirmationAlert = UIAlertController(title: MySingleton.shared.selectedLangData.TrenderAlert, message: MySingleton.shared.selectedLangData.Are_you_sure_you_want_to_remove_this_user, preferredStyle: .alert)
                        let yesAction = UIAlertAction(title: MySingleton.shared.selectedLangData.yes, style: .destructive) { (yesAction) in
                            self.removeMember(index: sender.tag)
                        }
                        let noAction = UIAlertAction(title: MySingleton.shared.selectedLangData.no, style: .cancel) { (noAction) in
                            
                        }
                        confirmationAlert.addAction(yesAction)
                        confirmationAlert.addAction(noAction)
                        self.present(confirmationAlert, animated: true, completion: nil)
                    case options[3]:
                        self.viewProfile(index: sender.tag)
                    default:
                        return
                        // self.muteMember(index:sender.tag)
                    }
                
            }else if self.currentView == self.adminView,self.isUserAdmin{
                switch item {
                case options[0]:
                    //self.muteMember(index:sender.tag, isMemberView: false, userProfileId: self.groupAdminList.data[sender.tag].UserProfileId!, isMute: self.groupAdminList.data[sender.tag].IsMuted)
                    if self.groupMemberList.data[sender.tag].IsMuted {
                        let confirmationAlert = UIAlertController(title: MySingleton.shared.selectedLangData.TrenderAlert, message: MySingleton.shared.selectedLangData.Are_you_sure_you_want_to_unmute_this_user, preferredStyle: .alert)
                        let yesAction = UIAlertAction(title: MySingleton.shared.selectedLangData.yes, style: .destructive) { (yesAction) in
                            self.muteMember(index:sender.tag, isMemberView: false, userProfileId: self.groupMemberList.data[sender.tag].UserProfileId!, isMute: self.groupMemberList.data[sender.tag].IsMuted)
                        }
                        let noAction = UIAlertAction(title: MySingleton.shared.selectedLangData.no, style: .cancel) { (noAction) in
                            
                        }
                        confirmationAlert.addAction(yesAction)
                        confirmationAlert.addAction(noAction)
                        self.present(confirmationAlert, animated: true, completion: nil)
                    }
                    else {
                        let confirmationAlert = UIAlertController(title: MySingleton.shared.selectedLangData.TrenderAlert, message: MySingleton.shared.selectedLangData.Are_you_sure_you_want_to_mute_this_user, preferredStyle: .alert)
                        let yesAction = UIAlertAction(title: MySingleton.shared.selectedLangData.yes, style: .destructive) { (yesAction) in
                            self.muteMember(index:sender.tag, isMemberView: false, userProfileId: self.groupMemberList.data[sender.tag].UserProfileId!, isMute: self.groupMemberList.data[sender.tag].IsMuted)
                        }
                        let noAction = UIAlertAction(title: MySingleton.shared.selectedLangData.no, style: .cancel) { (noAction) in
                            
                        }
                        confirmationAlert.addAction(yesAction)
                        confirmationAlert.addAction(noAction)
                        self.present(confirmationAlert, animated: true, completion: nil)
                    }
                case options[1]:
                    let confirmationAlert = UIAlertController(title: MySingleton.shared.selectedLangData.TrenderAlert, message: MySingleton.shared.selectedLangData.Are_you_sure_you_want_to_remove_this_user, preferredStyle: .alert)
                    let yesAction = UIAlertAction(title: MySingleton.shared.selectedLangData.yes, style: .destructive) { (yesAction) in
                        self.removeMember(index: sender.tag)
                    }
                    let noAction = UIAlertAction(title: MySingleton.shared.selectedLangData.no, style: .cancel) { (noAction) in
                        
                    }
                    confirmationAlert.addAction(yesAction)
                    confirmationAlert.addAction(noAction)
                    self.present(confirmationAlert, animated: true, completion: nil)
                case options[2]:
                    self.viewProfile(index: sender.tag)
                default:
                    return
                    // self.muteMember(index:sender.tag)
                }
            }else{ // If user is not an admin user
                switch item {
                case options[0]:
                    if self.groupMemberList.data[sender.tag].IsMuted {
                        let confirmationAlert = UIAlertController(title: MySingleton.shared.selectedLangData.TrenderAlert, message: MySingleton.shared.selectedLangData.Are_you_sure_you_want_to_unmute_this_user, preferredStyle: .alert)
                        let yesAction = UIAlertAction(title: MySingleton.shared.selectedLangData.yes, style: .destructive) { (yesAction) in
                            self.muteMember(index:sender.tag, isMemberView: true, userProfileId: self.groupMemberList.data[sender.tag].UserProfileId!, isMute: self.groupMemberList.data[sender.tag].IsMuted)
                        }
                        let noAction = UIAlertAction(title: MySingleton.shared.selectedLangData.no, style: .cancel) { (noAction) in
                            
                        }
                        confirmationAlert.addAction(yesAction)
                        confirmationAlert.addAction(noAction)
                        self.present(confirmationAlert, animated: true, completion: nil)
                    }
                    else {
                        let confirmationAlert = UIAlertController(title: MySingleton.shared.selectedLangData.TrenderAlert, message: MySingleton.shared.selectedLangData.Are_you_sure_you_want_to_mute_this_user, preferredStyle: .alert)
                        let yesAction = UIAlertAction(title: MySingleton.shared.selectedLangData.yes, style: .destructive) { (yesAction) in
                            self.muteMember(index:sender.tag, isMemberView: true, userProfileId: self.groupMemberList.data[sender.tag].UserProfileId!, isMute: self.groupMemberList.data[sender.tag].IsMuted)
                        }
                        let noAction = UIAlertAction(title: MySingleton.shared.selectedLangData.no, style: .cancel) { (noAction) in
                            
                        }
                        confirmationAlert.addAction(yesAction)
                        confirmationAlert.addAction(noAction)
                        self.present(confirmationAlert, animated: true, completion: nil)
                    }
                case options[1]:
                    self.viewProfile(index: sender.tag)
                default:
                    return
                    // self.muteMember(index:sender.tag)
                }
            }
            
        }
        dropDown.show()
    }
    
    func muteMember(index:Int,isMemberView:Bool,userProfileId:Int,isMute:Bool){
        let isMuteParam = (isMute) ? "false" : "true"
        let param = ["UserProfileID":userProfileId,"GroupID":self.groupId,"IsMute":isMuteParam] as [String : AnyObject]
        print("Param : ",param)
        WebServices().callUserService(service: UserServices.muteUnmuteMember, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    if isMemberView{
                        self.groupMemberList.data[index].IsMuted = !self.groupMemberList.data[index].IsMuted
                    }else{
                        self.groupAdminList.data[index].IsMuted = !self.groupAdminList.data[index].IsMuted
                    }
                    DispatchQueue.main.async{
                       self.adminGroupMemberTblView.reloadData()
                    }
                    
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func makeAdmin(index:Int){
        
     let param = ["UserProfileID":groupMemberList.data[index].UserProfileId,"GroupID":self.groupId] as [String : AnyObject]
       
        
        WebServices().callUserService(service: UserServices.makeAdmin, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                self.groupAdminList.data.insert(self.groupMemberList.data[index], at: 0)
                    self.totalAdminCount += 1
                    
                    self.groupMemberList.data.remove(at: index)
                    self.totalMemberCount -= 1
                    GroupDetailsVC.reloadAboutDiscussionTbl = true
                    DispatchQueue.main.async{
                        self.adminGroupMemberTblView.reloadData()
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func removeMember(index:Int){
        
        var param:[String:Any] = [:]
        
        if self.currentView == memberView{
            param = ["UserProfileID":self.groupMemberList.data[index].UserProfileId!,"GroupID":self.groupId]
        }else{
            param = ["UserProfileID":self.groupAdminList.data[index].UserProfileId!,"GroupID":self.groupId]
        }
        
        WebServices().callUserService(service: UserServices.removeMemberFromGroup, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    if self.currentView == self.memberView{
                        self.groupMemberList.data.remove(at: index)
                        self.totalMemberCount -= 1
                     }else{
                        self.groupAdminList.data.remove(at: index)
                        self.totalAdminCount -= 1
                    }
                    GroupDetailsVC.reloadAboutDiscussionTbl = true
                    DispatchQueue.main.async {
                        self.adminGroupMemberTblView.reloadData()
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func viewProfile(index:Int){
        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
        let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
        if currentView == memberView{
            profileVC.userId = groupMemberList.data[index].UserProfileId ?? 0
        }else{
            profileVC.userId = groupAdminList.data[index].UserProfileId ?? 0
        }
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
}
