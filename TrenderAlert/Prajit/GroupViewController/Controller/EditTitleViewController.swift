//
//  EditTitleViewController.swift
//  TrenderAlert
//
//  Created by Riken Shah on 23/07/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import GrowingTextView

protocol editGroupDelegate {
    func editcontroller(description: String, param : [String:Any],status: Int)
}


class EditTitleViewController: UIViewController,UITextViewDelegate {

    @IBOutlet weak var viewTitle: UILabel!
    @IBOutlet weak var btnedit: UIButton!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var txtEdit: GrowingTextView!
    @IBOutlet weak var viewDescription: UIView!
    
    var txtTitle = String()
    var delegate : editGroupDelegate?
    var status = Int()
    var maxLengthForTextLimit = 0;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtEdit.delegate = self
        self.btnedit.makeCornerRadius(radius: 6)
        self.viewDescription.makeCornerRadius(radius: 4)
//        self.viewDescription.borderColor = UIColor.darkGray
        self.viewDescription.borderColor = UIColor.lightGray;
        self.viewDescription.borderWidth = 0.5;
        self.viewMain.makeCornerRadius(radius: 4)
        if status == 2
        {
            self.viewTitle.text = "\(MySingleton.shared.selectedLangData.edit) \(MySingleton.shared.selectedLangData.title)"
            self.txtEdit.placeholder = MySingleton.shared.selectedLangData.title
            self.maxLengthForTextLimit = 40;
        }
        else
        {
            self.viewTitle.text = "\(MySingleton.shared.selectedLangData.edit) \(MySingleton.shared.selectedLangData.description)"
            self.txtEdit.placeholder = MySingleton.shared.selectedLangData.Description
            self.maxLengthForTextLimit = 1000;
        }
        self.txtEdit.text = self.txtTitle
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btn_SubmitUpdate(_ sender: Any)
    {
        var param:[String:Any] = [:]
        let description = (self.txtEdit.text == "Description") ? "" : self.txtEdit.text
        if status == 2
        {
            if self.txtEdit.text.isEmpty
            {
                self.txtEdit.placeholderColor = UIColor.red
                return
            }
            param = ["GroupName":self.txtEdit.text!,"GroupImage":"","GroupThumbnail":"","IsClosed":"true","IsChangeImage":"false","IsChangeThumbnail":"false","Memberlist":[]] as [String : Any]
        }
        else
        {
            param = ["Description":self.txtEdit.text!,"GroupImage":"","GroupThumbnail":"","IsClosed":"true","IsChangeImage":"false","IsChangeThumbnail":"false","Memberlist":[]] as [String : Any]
        }
        self.delegate?.editcontroller(description: self.txtEdit.text, param: param, status: self.status)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_CancelActio(_ sender: Any)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    func updateInfo()
    {
        
        
//        if isChangeImage{
//            let groupImageToUpload = ["FileExt":"jpg","contenttype":"image/jpeg","ImageData":"data:image/jpg;base64,\(Utilities.shared.encodeToBase64String(image: groupImgBtn.currentBackgroundImage ?? groupImage.image!))"]
//
//            let imageArray:[AnyObject] = [groupImageToUpload as AnyObject]
//
//            param = ["Id":0,"GroupName":self.groupNameTextField.text!,"Description":description!,"GroupImage":["Images":imageArray],"GroupThumbnail":["Images":imageArray],"IsClosed":"true","IsChangeImage":(isChangeImage) ? "true":"false","IsChangeThumbnail":(isChangeImage) ? "true":"false","Memberlist":selectedMembers] as [String : Any]
//        }else{
     
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let maxLength = self.maxLengthForTextLimit;
        let currentString: NSString = textView.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: text) as NSString
        return newString.length <= maxLength
    }
}
