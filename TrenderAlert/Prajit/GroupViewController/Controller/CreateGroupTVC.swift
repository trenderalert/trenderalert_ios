//
//  CreateGroupTVC.swift
//  TrenderAlert
//
//  Created by HPL on 14/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit


protocol CreateGroupTVCDelegate:class{
    func selectUnselectGroupMember()
}


class CreateGroupTVC: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var groupMemberSelectionBtn: UIButton!
    
    static let reuseIdentifier = "CreateGroupTVC"
    var delegate:CreateGroupTVCDelegate?
    var isMemberSelected = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.groupMemberSelectionBtn.makeCornerRadius(radius: 6)
        // Initialization code
    }
    
    func setup(){
        DispatchQueue.main.async {
            self.profileImage.makeCornerRadius(radius: self.profileImage.frame.height/2)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
