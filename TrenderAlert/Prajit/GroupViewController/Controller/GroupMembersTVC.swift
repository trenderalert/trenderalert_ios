//
//  GroupMembersTVC.swift
//  TrenderAlert
//
//  Created by HPL on 04/03/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class GroupMembersTVC: UITableViewCell {
    
    //Outlets
    @IBOutlet weak var memberImage: UIImageView!
    @IBOutlet weak var memberNameLbl: UILabel!
    @IBOutlet weak var memberMoreOptionBtn: UIButton!
    
    //Properties
    static let reuseIdentifier = "GroupMembersTVC"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setup(){
        DispatchQueue.main.async {
            self.memberImage.makeCornerRadius(radius: self.memberImage.frame.height/2)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
