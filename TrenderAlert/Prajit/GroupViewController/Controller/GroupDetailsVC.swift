//
//  GroupDetailsVC.swift
//  TrenderAlert
//
//  Created by HPL on 17/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import SDWebImage
import GoogleMobileAds
import MBProgressHUD
import CropViewController

struct GroupDetail{
    let groupId:Int?
    let isTrendForGroup:Bool?
    
    init(groupId:Int,isTrendForGroup:Bool){
        self.groupId = groupId
        self.isTrendForGroup = isTrendForGroup
    }
}

enum GroupJoinRequestStatus:Int{
    case requestNotSent =  0
    case requestSentFromUserToAdmin = 1
    case adminAcceptedTheRequest = 2
    case adminDecliceTheRequest = 3
}

enum GroupPrivacy:Int{
    case publicGroup = 1
    case closeGroup = 2
    case secretGroup = 3
}

class GroupDetailsVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var groupImage: UIImageView!
    @IBOutlet weak var postTrendImage: UIImageView!
    @IBOutlet weak var groupNameLbl: UILabel!
    @IBOutlet weak var groupInfoLbl: UILabel!
    @IBOutlet weak var groupMembersImagesCollectionView: UICollectionView!
    @IBOutlet weak var aboutBtn: UIButton!
    @IBOutlet weak var discussionBtn: UIButton!
    
    @IBOutlet weak var postTrendViewHeight: NSLayoutConstraint!
    @IBOutlet weak var postTrendInnerViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var postTrendInnerView: UIView!
    @IBOutlet weak var postTrendView: UIView!
    @IBOutlet weak var aboutDiscussionTbl: UITableView!
    @IBOutlet weak var addMemberBtn: UIButton!
    @IBOutlet weak var trendCollectionView: UICollectionView!
    @IBOutlet weak var noDataLbl: UILabel!
    
    @IBOutlet weak var listGridToggleBtn: UIButton!
    @IBOutlet weak var gridView: UIView!
    @IBOutlet weak var gridViewTopConstraint: NSLayoutConstraint!
    
    let picker = UIImagePickerController()
    var saveFolderTrendId = Int()
    var shareTrendId = Int()
    var groupPrivacy = Int()
    var postTrendInnerViewHeightConst:CGFloat = 0
    var postTrendViewHeightConst:CGFloat = 0
    var listGridToggleBtnHeightCons:CGFloat = 0
    var selectedShareFriends = [Int]()
    var totalNotificationCount = 10
    var groupName = String()
    var currentCustomAdCnt = 0
    var currentGoogleAdCnt = 0
    var googleAdHeight = (UIDevice.current.userInterfaceIdiom == .pad) ? 90.0 : 50.0
    
    var trendHeaderViewHeight = (UIDevice.current.userInterfaceIdiom == .phone) ? 50 : 70
    
    var trendStatusViewHeight = (UIDevice.current.userInterfaceIdiom == .phone) ? 45 : 65
    
    var isChangeImage = false
    
    var memberImages:[String] = []{
        didSet{
//            groupMembersImagesCollectionView.reloadData()
        }
    }
    
    var groupId = 0
    var getPostTrendViewHeight = true
    var groupDetails = GroupDetailsModel()

    let aboutTableRows = 2 // 1: AboutGroupTVC, 2: AboutGroupMemberTVC
    var isAboutUsViewVisible = true
    var memberString = MySingleton.shared.selectedLangData.members
    var groupMemberImages:[String] = []
    var groupAdminImages:[String] = []
    var groupAdminNames:[String] = []
    var groupMemberNames:[String] = []
    var isScroll = false
    // MARK: - Variables
    var trendImagesCollectionView: UICollectionView!
    var testImageArray = [UIImage]()
    var trendData = TrendListData()
    var trendImagesArray = [UIImage]()
    var trendImagesPageControl = UIPageControl()
    var trendCurrentImageLabel = UILabel()
    var trendImagesCollectionArray = [[UIImage]]()
    var refreshControl = UIRefreshControl()
    var pageIndex = 0
    static var reloadAboutDiscussionTbl = false
    
    var noOfRecordsPerRequest = 10
    var totalTrends = 1
    var isGetAllTrendRequestInProgress = false
    var showBottomLoader = true
    var isListView = true
    var loadGridView = true
    var isFirstTimeGetTrends = true
    var isFirstTimeGroupDetails = true;
    var reloadTrendList = false;
    
    var privacy = Int()
    
    var isGroupMember = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
       setupTableView()
       registerHeader()
        setupNavigationBar(title: "Details")
       zoomImage()
       initialConfig()
       loadGroupDetails()
        
       self.picker.delegate = self
    }
    
   
    func registerHeader(){
        let headerNib = UINib.init(nibName: "GroupDetailsHeader", bundle: Bundle.main)
        aboutDiscussionTbl.register(headerNib, forHeaderFooterViewReuseIdentifier: "GroupDetailsHeader")
        self.aboutDiscussionTbl.estimatedSectionHeaderHeight = 50
        self.aboutDiscussionTbl.estimatedRowHeight = 50
//        self.aboutDiscussionTbl.estimatedSectionHeaderHeight
    }
    
    func zoomImage(){
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showZoomedImage))
//        groupImage.addGestureRecognizer(tapGesture)
//        groupImage.isUserInteractionEnabled = true
    }
    
    @objc func showZoomedImage(){
        let zoomImageView = ZoomImageVC(nibName: "ZoomImageVC", bundle: nil)
        zoomImageView.imageUrl = groupDetails.data[0].GroupImage ?? ""
        self.navigationController?.pushViewController(zoomImageView, animated: true)
    }
    
    func setupNavigationBar(title:String){
        // Utilities.setNavigationBar(viewController:self)
        Utilities.setNavigationBarWithTitle(viewController: self, title: title)
        self.noDataLbl.text = MySingleton.shared.selectedLangData.no_data_available
        let leftButton = UIBarButtonItem(image: UIImage(named: "img_BackArrow"), style: .plain, target: self, action: #selector(goBack))
        navigationItem.leftBarButtonItem = leftButton
        
//        let rightBtnView = Bundle.main.loadNibNamed("NotificationView", owner: self, options: nil)?.first as! NotificationView
//        rightBtnView.notificationBtn.addTarget(self, action: #selector(goToNotification), for: .touchUpInside)
//        rightBtnView.notificationCountLbl.text = "\(totalNotificationCount)"
//        let rightButton = UIBarButtonItem(customView: rightBtnView)
//        navigationItem.rightBarButtonItem = rightButton
    }
    
    @objc func goToNotification(){
        let storyboard       = UIStoryboard(name: "NotificationSB", bundle: nil)
        let notificationView = storyboard.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.navigationController?.pushViewController(notificationView, animated: true)
    }
    
    @objc func goBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
   @objc func toggleListGridView() {
        /* Images:
         menu_dft, menu_clk, list_clk, list_dft
         */
    let headerView = self.aboutDiscussionTbl.headerView(forSection: 0)
    if let headerView = headerView as? GroupDetailsHeader{
        if isListView{ // For Grid View
            self.aboutDiscussionTbl.setContentOffset(CGPoint(x: 0, y: 0), animated: false)

            self.isListView = false
            self.noDataLbl.isHidden = true
            self.aboutDiscussionTbl.reloadData()
            self.aboutDiscussionTbl.tableFooterView = UIView()
          headerView.listGridToggleBtn.setBackgroundImage(UIImage(named: "list_clk"), for: .normal)
           
        }else{ // For list View
//          self.gridView.isHidden = true
            self.isListView = true
            self.aboutDiscussionTbl.reloadData()
            
            headerView.listGridToggleBtn.setBackgroundImage(UIImage(named: "menu_clk"), for: .normal)
//            self.aboutDiscussionTbl.isScrollEnabled = true
            
        }
      }
    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        pausePlayeVideos()
     
//        reloadTrendData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        pausePlayeVideos()
    }
    
    override func viewDidLayoutSubviews() {
    }
    
    
   @objc func setTrend() {
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let dest = storyboard.instantiateViewController(withIdentifier: "SetTrendViewController") as! SetTrendViewController
            dest.isBackButtonReq = true
        dest.isGroup = true
    
        reloadTrendList = true
    
    let groupParams = GroupDetail(groupId: self.groupDetails.data[0].GroupID!, isTrendForGroup: true)
        dest.groupParams = groupParams
        self.navigationController?.pushViewController(dest, animated: true)
//        SetTrendViewController
    }
    
    func setupCollectionView(){
//        groupMembersImagesCollectionView.isPagingEnabled = true
       
//        groupMembersImagesCollectionView.delegate = self
//        groupMembersImagesCollectionView.dataSource = self
//        groupMembersImagesCollectionView.register(UINib(nibName: GroupDetailMemberImagesCVC.reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: GroupDetailMemberImagesCVC.reuseIdentifier)
    }
    
    func setupTableView(){
        self.aboutDiscussionTbl.delegate = self
        self.aboutDiscussionTbl.dataSource = self
        
        self.aboutDiscussionTbl.register(UINib(nibName: AboutGroupTVC.reuseIdentifier, bundle: nil), forCellReuseIdentifier: AboutGroupTVC.reuseIdentifier)
        self.aboutDiscussionTbl.register(UINib(nibName: AboutGroupMemberTVC.reuseIdentifier, bundle: nil), forCellReuseIdentifier: AboutGroupMemberTVC.reuseIdentifier)
    }
    
    @objc func acceptRejectRequest(_ sender:UIButton){
        let acceptRejectOptions = UIAlertController(title: MySingleton.shared.selectedLangData.Group_Request_Action, message: MySingleton.shared.selectedLangData.accept_reject_request, preferredStyle: .alert)
        
        let acceptAction = UIAlertAction(title: MySingleton.shared.selectedLangData.accept, style: .default) { [weak self] (action) in
            self!.acceptGroupRequest(groupID: (self?.groupDetails.data[0].GroupID)!)
        }
        let rejectAction = UIAlertAction(title: MySingleton.shared.selectedLangData.reject, style: .default) { [weak self] (action) in
            self?.declineGroupRequest(groupID: (self?.groupDetails.data[0].GroupID)!)
        }
        let cancelAction = UIAlertAction(title: MySingleton.shared.selectedLangData.cancel, style: .cancel)
        
        acceptRejectOptions.addAction(acceptAction);
        acceptRejectOptions.addAction(rejectAction);
        acceptRejectOptions.addAction(cancelAction);
        self.present(acceptRejectOptions, animated: true, completion: nil)
     }
    
    
    func acceptGroupRequest(groupID: Int){
        
        let params = ["GroupID":groupID,"GroupjoinResponse":2]
        
        WebServices().callUserService(service: UserServices.groupInvite, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
//                    DispatchQueue.main.async {
//                        self.groupDetails.data[0].IsUserAGroupMember = true
//                        self.groupDetails.data[0].IsRequestedByAdmin = false
//                        self.groupDetails.data[0].JoinRequestStatus = 2
////                        self.aboutDiscussionTbl.reloadData();
//                        let headerView = self.aboutDiscussionTbl.headerView(forSection: 0)
//                        if let headerView = headerView as? GroupDetailsHeader{
//                            headerView.btnJoin.setTitle("Joined", for: .normal)
//                            headerView.btnJoin.isUserInteractionEnabled = false
//                        }
//                    }
                    self.isFirstTimeGroupDetails = true;
                    self.loadGroupDetails();
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func declineGroupRequest(groupID: Int){
        
        let params = ["GroupID":groupID,"GroupjoinResponse":3]
        
        WebServices().callUserService(service: UserServices.groupInvite, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    if (self.groupDetails.data[0].GroupPrivacy == GroupPrivacy.secretGroup.rawValue){ // If secret group request is rejected by user then send the user back;
                        DispatchQueue.main.async{
                          self.navigationController?.popViewController(animated: true)
                        }
                        
                    }else{
                        self.loadGroupDetails();
                    }
                    
//                    DispatchQueue.main.async {
//                        self.groupDetails.data[0].IsUserAGroupMember = false
//                        self.groupDetails.data[0].IsRequestedByAdmin = false
//                        self.groupDetails.data[0].JoinRequestStatus = 3
//
//                        self.aboutDiscussionTbl.reloadData();
//                        let headerView = self.aboutDiscussionTbl.headerView(forSection: 0)
//                        if let headerView = headerView as? GroupDetailsHeader{
//                            headerView.btnJoin.setTitle("Rejected", for: .normal)
//                            headerView.btnJoin.isUserInteractionEnabled = false
//                        }
//                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    
    func setupGroupDetailViewHeaderLabelValues(groupName:String,groupInfo:String,groupImage:String,isUserAdmin:Bool,memberImages:[String]){
        DispatchQueue.main.async {

        let headerView = self.aboutDiscussionTbl.headerView(forSection: 0)
        if let headerView = headerView as? GroupDetailsHeader{
//            self.navigationItem.title = groupName
            self.setupNavigationBar(title: groupName)
            headerView.groupNameLbl.text = groupName
            self.groupName = groupName
            headerView.memberImages = memberImages;
          //  self.groupNameLbl.text = groupName
            headerView.groupImage.sd_setImage(with: URL(string: groupImage), placeholderImage: UIImage(named: "Img_Blur"), options: [.continueInBackground], completed: nil)
            headerView.groupInfoLbl.text = groupInfo
            
            if isUserAdmin{//If user is admin of the group
                 headerView.addMemberBtn.isHidden = false
                 headerView.btnJoin.isHidden = false
                headerView.btnJoin.setTitle(MySingleton.shared.selectedLangData.leave, for: .normal)
                headerView.btnJoin.addTarget(self, action: #selector(self.leaveGroup(_:)), for: .touchUpInside)
                 headerView.btnEdit.isHidden = false
                 headerView.btnEditImg.isHidden = false
                 headerView.btnEdit.tag = 2
                 headerView.showMemberBtn = true
                 headerView.btnEdit.addTarget(self, action: #selector(self.btnEditTitle(_:)), for: .touchUpInside)
                headerView.btnEditImg.addTarget(self, action: #selector(self.btnEditImg(_:)), for: .touchUpInside)
            }else{
                
                headerView.addMemberBtn.isHidden = true
                headerView.btnJoin.isHidden = false
                headerView.btnEdit.isHidden = true
                headerView.btnEditImg.isHidden = true
                headerView.showMemberBtn = (self.groupDetails.data[0].GroupPrivacy == GroupPrivacy.publicGroup.rawValue || self.groupDetails.data[0].IsUserAGroupMember) ? true : false;
                
                headerView.btnJoin.removeTarget(nil, action: nil, for: .allEvents)
                
                if self.groupDetails.data[0].JoinRequestStatus == GroupJoinRequestStatus.requestNotSent.rawValue || (self.groupDetails.data[0].JoinRequestStatus == GroupJoinRequestStatus.adminDecliceTheRequest.rawValue) // If request is not made or request is rejected by admin
                {
                    headerView.btnJoin.setTitle(MySingleton.shared.selectedLangData.join_group, for: .normal)
                    headerView.btnJoin.isUserInteractionEnabled = true
                    headerView.btnJoin.addTarget(self, action: #selector(self.btnJoinGroup(_:)), for: .touchUpInside)
                }
                else if self.groupDetails.data[0].JoinRequestStatus == GroupJoinRequestStatus.requestSentFromUserToAdmin.rawValue
                {
                    headerView.btnJoin.setTitle(MySingleton.shared.selectedLangData.Requested, for: .normal)
                    headerView.btnJoin.isUserInteractionEnabled = false
                }else if self.groupDetails.data[0].JoinRequestStatus == GroupJoinRequestStatus.adminAcceptedTheRequest.rawValue
                {
                    headerView.btnJoin.setTitle(MySingleton.shared.selectedLangData.leave, for: .normal)
                    headerView.btnJoin.addTarget(self, action: #selector(self.leaveGroup(_:)), for: .touchUpInside)
                    headerView.btnJoin.isUserInteractionEnabled = true
                }
//                else if self.groupDetails.data[0].JoinRequestStatus == 3 // Join Request rejected by Admin
//                {
//                    headerView.btnJoin.setTitle("Rejected", for: .normal)
//                    headerView.btnJoin.isUserInteractionEnabled = false
//                }
                
                //If request is made by admin then Accept/Reject option for user
                if (self.groupDetails.data[0].IsRequestedByAdmin) {
                    headerView.btnJoin.setTitle("\(MySingleton.shared.selectedLangData.accept)/\(MySingleton.shared.selectedLangData.reject)", for: .normal)
                    headerView.btnJoin.isUserInteractionEnabled = true
                    headerView.btnJoin.addTarget(self, action: #selector(self.acceptRejectRequest(_:)), for: .touchUpInside)
                }
                // If user is member of the group
//                if (self.groupDetails.data[0].IsUserAGroupMember){
//
//                }
                
                
            }
            headerView.groupMembersImagesCollectionView.reloadData()
        }
    }
    }
    
    @objc func btnEditImg(_ sender: UIButton)
    {
        self.view.endEditing(true)
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: MySingleton.shared.selectedLangData.please_select_your_option, message: "", preferredStyle: .actionSheet)
        
        let cameraActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.camera, style: .default) { void in
            print("Camera")
            self.openCamera()
        }
        actionSheetControllerIOS8.addAction(cameraActionButton)
        
        let galleryActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.gallery, style: .default) { void in
            print("Gallery")
            self.openGallary()
        }
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.cancel, style: .default) { void in
            print("Cancel")
        }
        
        actionSheetControllerIOS8.addAction(galleryActionButton)
        actionSheetControllerIOS8.addAction(cancelActionButton)
        actionSheetControllerIOS8.popoverPresentationController?.sourceView = sender
        actionSheetControllerIOS8.popoverPresentationController?.sourceRect = sender.bounds
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
     @objc func btnEditTitle(_ sender: UIButton)
     {
        let storyboard = UIStoryboard(name: "GroupSB", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "EditTitleViewController") as! EditTitleViewController
        destination.delegate = self
        if sender.tag == 0 {
            let title = self.groupDetails.data[0].GroupDescription!
            destination.txtTitle = title
        }else
        {
            let title = self.groupDetails.data[0].GroupName!
            destination.txtTitle = title
        }
        
        destination.status = sender.tag
        let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height)
        self.navigationController?.present(pop, animated: true, completion: nil)
     }
    
    
    func leaveOrDeleteGroup(isDelete:Bool){
        
        var leaveGroupMsg = MySingleton.shared.selectedLangData.Are_you_sure_you_want_to_leave_this_group
        
        if isDelete{
            leaveGroupMsg = MySingleton.shared.selectedLangData.leave_group_info
        }
        
        
        let confirmLeave = UIAlertController(title: "\(MySingleton.shared.selectedLangData.leave)", message: leaveGroupMsg, preferredStyle: .alert);
        let okAction = UIAlertAction(title: MySingleton.shared.selectedLangData.Confirm, style: .default) { (action) in
            let param = ["GroupID": self.groupId] as [String : Any]
            
            WebServices().callUserService(service: UserServices.leaveGroup, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
                if serviceResponse["Status"] != nil{
                    if (serviceResponse["Status"] as! Bool == true){
                        if isDelete{
                            DispatchQueue.main.async{
                               self.navigationController?.popViewController(animated: false);
                            }
                        }else{
                            
                            self.isFirstTimeGroupDetails = true;
                            self.loadGroupDetails()
                            
                        }
                        
                    }else{
                        TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: MySingleton.shared.selectedLangData.cancel, style: .cancel)
        
        confirmLeave.addAction(okAction);
        confirmLeave.addAction(cancelAction);
        
        DispatchQueue.main.async{
           self.present(confirmLeave, animated: false);
        }
    }
    
    
    @objc func leaveGroup(_ sender:UIButton){
        
        //Check for remaining group member counts.
       let param = ["GroupID": self.groupId] as [String : Any]
        
        WebServices().callUserService(service: UserServices.remainingGroupMemberCount, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    let cnt = serviceResponse["data"] as! String
                    if cnt == "1"{
                       self.leaveOrDeleteGroup(isDelete: true)
                    }else{
                        self.leaveOrDeleteGroup(isDelete: false)
                    }
                    
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
        
    }
    
    
    @objc func btnJoinGroup(_ sender: UIButton)
    {
        let param = ["Id": self.groupId,
                     "GroupName": self.groupName,
                     "GroupPrivacy": self.groupPrivacy] as [String : Any]
        print(param)
        WebServices().callUserService(service: UserServices.requestToJoinGroup, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    print("Request sent")
                        self.isFirstTimeGroupDetails = true;
                        self.loadGroupDetails()
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }

    }
    
    func loadGroupDetails(){
        
        if self.groupId != 0{
            let param = ["GroupID":self.groupId]
            
            // Resetting
            self.groupMemberNames = []
            self.groupMemberImages = []
            self.groupAdminNames = []
            self.groupAdminImages = []
            self.memberImages = []

            
            WebServices().callUserService(service: UserServices.groupDetails, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse, serviceData) in
                if(serviceResponse["Status"] != nil){
                    if(serviceResponse["Status"] as! Bool == true){
                        self.groupDetails = WebServices().decodeDataToClass(data: serviceData, decodeClass: GroupDetailsModel.self)!
                        
                        
                        
                        for member in self.groupDetails.data[0].GroupMembers{
                            self.memberImages.append(member.ProfileImage ?? "")
                            self.groupMemberNames.append(member.Firstname ?? "")
                            self.groupMemberImages.append(member.ProfileImage ?? "")
                        }
                        
                        
                        for member in self.groupDetails.data[0].GroupAdmins{
                            if self.memberImages.count < 5{
                               self.memberImages.append(member.ProfileImage ?? "")
                            }
                            self.groupAdminNames.append(member.Firstname ?? "")
                            self.groupAdminImages.append(member.ProfileImage ?? "")
                        }
                        
                        
                        self.setupGroupDetailViewHeaderLabelValues(groupName: self.groupDetails.data[0].GroupName ?? "", groupInfo: "\(Utilities.groupPrivacyDetails[self.groupDetails.data[0].GroupPrivacy! - 1][0]) \(MySingleton.shared.selectedLangData.group) • \(self.groupDetails.data[0].TotalMember ?? 0) \(self.memberString)", groupImage: self.groupDetails.data[0].GroupImage ?? "", isUserAdmin: self.groupDetails.data[0].IsUserAdmin, memberImages: self.memberImages)
                        
//                        GroupDetailsHeader.memberImages = self.memberImages
                        
                        
                        if self.isFirstTimeGroupDetails{
                            
                            self.isFirstTimeGroupDetails = false;
                            
                            DispatchQueue.main.async{
                                
                                let headerView = self.aboutDiscussionTbl.headerView(forSection: 0);
                                // If user is not a member of the group and it is not public group
                                
                                if ((!self.groupDetails.data[0].IsUserAGroupMember) && (self.groupDetails.data[0].GroupPrivacy != GroupPrivacy.publicGroup.rawValue) ) {
                                    
                                    
                                 // For user other than group member.
                                    
                                    if let headerView = headerView as? GroupDetailsHeader{
                                        headerView.discussionBtn.isEnabled = false;
                                    }
                                    self.toggleAboutOrDiscussionView(viewTag:1);
                                }else{
                                    
                                    if let headerView = headerView as? GroupDetailsHeader{
                                        headerView.discussionBtn.isEnabled = true;
                                    }
                                   self.toggleAboutOrDiscussionView(viewTag:2);
                                }
                                
                            }
                        }else{
                            
                            DispatchQueue.main.async{
                                if self.isAboutUsViewVisible{
                                    self.loadAboutGroupView()
                                }else{
                                    self.loadDiscussionView()
                                }
                                self.aboutDiscussionTbl.reloadData()
                            }
                        }
                        
                       
                    }else{
                        TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }
        }
    }
    
    @objc func highlightAboutView(){
        toggleAboutOrDiscussionView(viewTag: 1)
    }
    
    @objc func highlightDiscussionView(){
        toggleAboutOrDiscussionView(viewTag: 2)
    }
    
     func toggleAboutOrDiscussionView(viewTag:Int) {
        
        let headerView = self.aboutDiscussionTbl.headerView(forSection: 0)
        if let headerView = headerView as? GroupDetailsHeader{
            if (self.groupDetails.data[0].GroupPrivacy == GroupPrivacy.publicGroup.rawValue) && (!self.groupDetails.data[0].IsUserAGroupMember){
                headerView.postTrendInnerView.isHidden = true;
                headerView.postTrendImage.isHidden = true;
            }else{
                headerView.postTrendInnerView.isHidden = false;
                headerView.postTrendImage.isHidden = false;
            }
        }
        
        if viewTag == 1{
            isAboutUsViewVisible = true
            loadAboutGroupView()
            self.trendCollectionView.isHidden = true
            if !isListView{
                self.gridView.isHidden = true
                self.isListView = true
            }
            
        }else{
            isAboutUsViewVisible = false
            loadDiscussionView()
            self.trendCollectionView.isHidden = false
            if isFirstTimeGetTrends{
                getAllTrends(Skip: 0, Take: noOfRecordsPerRequest, lazyLoading: false)
                isFirstTimeGetTrends = false
            }
        }
        
        self.aboutDiscussionTbl.reloadData()
    }
    
    func loadAboutGroupView(){
   
        
        let headerView = self.aboutDiscussionTbl.headerView(forSection: 0)
        
        
        if let headerView = headerView as? GroupDetailsHeader{
            headerView.aboutBtn.titleLabel?.textColor = UIColor.black
            headerView.aboutBtn.backgroundColor = UIColor.baseColor
            headerView.discussionBtn.titleLabel?.textColor = UIColor.black
            headerView.discussionBtn.backgroundColor = UIColor.white
            
            headerView.postTrendViewHeight.constant = 0
            headerView.postTrendViewHeight.isActive = true
            
            headerView.postTrendInnerViewHeight.constant = 0
            headerView.postTrendInnerViewHeight.isActive = true
            
            headerView.listGridToggleBtnHeight.constant = 0
            headerView.listGridToggleBtnHeight.isActive = true
            
        headerView.listGridToggleBtn.setBackgroundImage(UIImage(named: "menu_clk"), for: .normal)
            headerView.postTrendView.layoutIfNeeded();
        }

    }
    
    func loadDiscussionView(){
        
        let headerView = self.aboutDiscussionTbl.headerView(forSection: 0)
        if let headerView = headerView as? GroupDetailsHeader{
            headerView.aboutBtn.titleLabel?.textColor =   UIColor.black
            headerView.aboutBtn.backgroundColor = UIColor.white
            
            headerView.discussionBtn.titleLabel?.textColor =  UIColor.black
            headerView.discussionBtn.backgroundColor =  UIColor.baseColor
            
            headerView.postTrendInnerViewHeight.constant = postTrendInnerViewHeightConst
            headerView.postTrendInnerViewHeight.isActive = true
            
            headerView.postTrendViewHeight.constant = postTrendViewHeightConst
            headerView.postTrendViewHeight.isActive = true
            
            headerView.listGridToggleBtnHeight.constant = listGridToggleBtnHeightCons
            headerView.listGridToggleBtnHeight.isActive = true
            headerView.postTrendView.layoutIfNeeded()
        }
        
    }
    
   
    
    func initialConfig() {
        //        self.menuContainerViewController.menuState = MFSideMenuStateClosed
        //        Utilities.setNavigationBar(viewController: self)
        //        self.navigationController?.navigationBar.barTintColor = .black
        //        self.navigationController?.navigationBar.tintColor = .white
        // Register Table View Cells
        registerTableViewCells()
        
        // Test
        //        callAPI()
        testImageArray = [#imageLiteral(resourceName: "img_guitar"), #imageLiteral(resourceName: "img_DefaultDetail"), #imageLiteral(resourceName: "smoke_DashBoard"), #imageLiteral(resourceName: "img_mapCollection3"), #imageLiteral(resourceName: "img_mapCollection3"), #imageLiteral(resourceName: "img_mapCollection3")]
       
        // getAllTrends()
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refreshAllTrends), for: UIControl.Event.valueChanged)
        aboutDiscussionTbl.addSubview(refreshControl)
        
        aboutDiscussionTbl.tableFooterView = UIView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !self.isChangeImage
        {
            initialConfig()
            if GroupDetailsVC.reloadAboutDiscussionTbl{
                loadGroupDetails()
                GroupDetailsVC.reloadAboutDiscussionTbl = false
            }
            if !isAboutUsViewVisible{
                reloadTrendData()
            }
        }
        
      
        
//        if !self.showBottomLoader{
//            self.aboutDiscussionTbl.tableFooterView = UIView()
//            self.aboutDiscussionTbl.tableFooterView?.isHidden = true
//        }
        
//        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: NSNotification.Name(rawValue: "getAllTrends"), object: nil)
    }
    @objc func onDidReceiveData(_ notification: Notification){
//        getAllTrends(Skip: 0, Take: noOfRecordsPerRequest, lazyLoading: false)
    }
    
    @objc func refreshAllTrends(sender: UIRefreshControl) {
        self.refreshControl.endRefreshing()
        reloadTrendData()
        self.refreshControl.endRefreshing()
    }
    func reloadTrendData(){
        self.trendData.data = []
        self.trendImagesArray = []
        self.currentCustomAdCnt = 0
        self.currentGoogleAdCnt = 0
        self.totalTrends = 1
        self.showBottomLoader = true
        getAllTrends(Skip: 0, Take: noOfRecordsPerRequest, lazyLoading: false)
        self.aboutDiscussionTbl.reloadData()
    }
    // MARK: - Get All Trends
    func getAllTrends(Skip:Int,Take:Int,lazyLoading:Bool = true) {
//        self.groupId
        print("====================== GROUP GET TRENDS ==========================");
        if ((trendData.data.count + self.currentCustomAdCnt + self.currentGoogleAdCnt) >= totalTrends) || isGetAllTrendRequestInProgress{
            if isGetAllTrendRequestInProgress{
                return
            }
            self.showBottomLoader = false
            self.aboutDiscussionTbl.tableFooterView?.isHidden = true
            self.isGetAllTrendRequestInProgress = false
            return
        }
        
        self.isGetAllTrendRequestInProgress = true
        
        let params = ["SortColumn":"","SortDirection":"","Search":"","Skip":Skip,"Take":Take,"GroupId":self.groupId] as [String : Any]
        WebServices().callUserService(service: .getGroupDiscussion, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: lazyLoading, isHeader: true, CallMethod: .post) { (serviceResponse, responseData) in
            print("Get All Trends --- \(serviceResponse)")
            if(serviceResponse["Status"] as! Bool == true)
            {
                DispatchQueue.main.async {
                self.trendImagesArray.removeAll()
                //                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: {})
                let temp = WebServices().decodeDataToClass(data: responseData, decodeClass: TrendListData.self)!
                self.trendData.data += temp.data
                self.totalTrends = serviceResponse["TotalRecords"] as! Int
                
                self.currentCustomAdCnt += serviceResponse["CurrentCustomAdCount"] as! Int
                self.currentGoogleAdCnt += serviceResponse["CurrentGoogleAdCount"] as! Int
                
                self.totalTrends = (self.currentCustomAdCnt + self.currentGoogleAdCnt) + (serviceResponse["TotalRecords"] as! Int)
                
                self.isGetAllTrendRequestInProgress = false
                
                self.aboutDiscussionTbl.delegate = self
                self.aboutDiscussionTbl.dataSource = self
                
                self.aboutDiscussionTbl.reloadData()
            }
                
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
            
        }
    }
    
    func downloadSync(fromURL: String) -> UIImage? {
        let request = NSURLRequest(url: NSURL(string: fromURL)! as URL)
        var response: URLResponse?
        do {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response)
            return UIImage(data: data)!
        } catch {
            print("Error while trying to download following file: " + fromURL)
        }
        return nil
    }
    // MARK: - Like Action
    @objc func didTapLikeButton(button: UIButton) {
        var isTrendLiked = Bool()
        //        for trend in self.trendData.data {
        //            if trend.TrendId == button.tag {
        //                isTrendLiked = trend.IsTrendLike
        //            }
        //        }
        isTrendLiked = (self.trendData.data[Int(button.accessibilityIdentifier!)!].IsTrendLike)
        if isTrendLiked == true {
            dislikeTrend(trendID: "\(button.tag)", index: button.accessibilityIdentifier!)
        }
        else {
            likeTrend(trendID: "\(button.tag)", index: button.accessibilityIdentifier!)
        }
    }
    
    func likeTrend(trendID: String, index: String) {
        WebServices().callUserService(service: .likeUnlikeTrend, urlParameter: "\(trendID)", parameters: nil, isLazyLoading: false, isHeader: true, CallMethod: .post) { (serviceResponse, responseData) in
            
            print("Like Trend Response --- \(serviceResponse)")
            if serviceResponse["Status"] == nil {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                return
            }
            if(serviceResponse["Status"] as! Bool == true)
            {
                DispatchQueue.main.async {
                    self.trendData.data[Int(index)!].IsTrendLike = true
                    self.trendData.data[Int(index)!].TrendLikesCount = self.trendData.data[Int(index)!].TrendLikesCount + 1
                    let cellIndexPath = IndexPath(row: Int(index)!, section: 0)
                    let cell = self.aboutDiscussionTbl.cellForRow(at: cellIndexPath)as? TrendTableViewCell
                    cell!.trendLikesImageView.image = #imageLiteral(resourceName: "Like_clk")
                    cell!.likesCountLabel.text = "\(self.trendData.data[Int(index)!].TrendLikesCount)"
                }
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func dislikeTrend(trendID: String, index: String) {
        WebServices().callUserService(service: .likeUnlikeTrend, urlParameter: "\(trendID)", parameters: nil, isLazyLoading: false, isHeader: true, CallMethod: .post) { (serviceResponse, responseData) in
            
            print("Dislike Trend Response --- \(serviceResponse)")
            if serviceResponse["Status"] == nil {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                return
            }
            if(serviceResponse["Status"] as! Bool == true)
            {
                
                DispatchQueue.main.async {
                    self.trendData.data[Int(index)!].IsTrendLike = false
                    self.trendData.data[Int(index)!].TrendLikesCount = self.trendData.data[Int(index)!].TrendLikesCount - 1
                    let cellIndexPath = IndexPath(row: Int(index)!, section: 0)
                    let cell = self.aboutDiscussionTbl.cellForRow(at: cellIndexPath)as? TrendTableViewCell
                    cell!.trendLikesImageView.image = #imageLiteral(resourceName: "Like")
                    cell!.likesCountLabel.text = "\(self.trendData.data[Int(index)!].TrendLikesCount)"
                }
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    
    // MARK: - Register Table View Cells
    func registerTableViewCells() {
        aboutDiscussionTbl.register(UINib(nibName: "TrendTableViewCell", bundle: nil), forCellReuseIdentifier: "TrendTableViewCell")
        aboutDiscussionTbl.register(UINib(nibName:"GoogleAdTVC",bundle:nil),forCellReuseIdentifier:"GoogleAdTVC")
    }
    
    // MARK: - Register Collection View Cells
    func registerCollectionViewCells() {
        trendImagesCollectionView.register(UINib(nibName: "ImageTrendCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageTrendCollectionViewCell")
        
    }
    
    @objc func btnLearnMoreAction(_ sender : UIButton)
    {
        if trendData.data[sender.tag].IsCustomAd{
            //            if let url = URL(string: "https://stackoverflow.com") {
            
            if var url = trendData.data[sender.tag].AdUrl {
                if !(url.contains("http"))
                {
                    url = url.replacingOccurrences(of: "www.", with: "https://")
                }
                
                if UIApplication.shared.canOpenURL(URL(string: url)!)
                {
                    UIApplication.shared.open(URL(string: url)!)
                }
                //                UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
            }
        }
    }
    
    @objc func btnFullScrProfileImgAction(_ sender : UIButton)
    {
        let zoomImageView = ZoomImageVC(nibName: "ZoomImageVC", bundle: nil)
        if trendData.data[sender.tag].UserProfileImage != nil
        {
            zoomImageView.imageUrl = trendData.data[sender.tag].UserProfileImage!
        }
        self.navigationController?.pushViewController(zoomImageView, animated: true)
    }
    
    @objc func tapActionForLabel(_ recognizer : UITapGestureRecognizer)
    {
        if recognizer.state == UIGestureRecognizer.State.ended {
            print(recognizer.view!.tag)
            
            if trendData.data[recognizer.view!.tag].IsTrendShared == false{
                if trendData.data[recognizer.view!.tag].IsCustomAd{
                }
                else{
                    if (trendData.data[recognizer.view!.tag].AreYouFollowingTrender == 0 || trendData.data[recognizer.view!.tag].AreYouFollowingTrender == 1 || trendData.data[recognizer.view!.tag].AreYouFollowingTrender == 2)
                    {
                        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
                        let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
                        profileVC.userId = trendData.data[recognizer.view!.tag].UserProfileId
                        self.navigationController?.pushViewController(profileVC, animated: true)
                    }
                    else
                    {
                        //                        (self.tabBarController!).selectedIndex = 4
                    }
                }
            }
            else{
                let firstRange = ((recognizer.view! as! UILabel).text as! NSString).range(of: "\(trendData.data[recognizer.view!.tag].UserProfileFirstName!) \(String(describing: trendData.data[recognizer.view!.tag].UserProfileLastName!))")
                
                let secondRange = ((recognizer.view! as! UILabel).text as! NSString).range(of: "\(trendData.data[recognizer.view!.tag].TrendOwner!)")
                
                if recognizer.didTapAttributedTextInLabel(label: (recognizer.view! as! UILabel), inRange: firstRange) {
                    print(firstRange)
                    if Int(MySingleton.shared.loginObject.UserID) == trendData.data[recognizer.view!.tag].UserProfileId
                    {
                        //                        (self.tabBarController!).selectedIndex = 4
                    }
                    else
                    {
                        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
                        let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
                        profileVC.userId = trendData.data[recognizer.view!.tag].UserProfileId
                        self.navigationController?.pushViewController(profileVC, animated: true)
                    }
                    
                } else if recognizer.didTapAttributedTextInLabel(label: (recognizer.view! as! UILabel), inRange: secondRange) {
                    print(secondRange)
                    if Int(MySingleton.shared.loginObject.UserID) != trendData.data[recognizer.view!.tag].TrendOwnerId!
                    {
                        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
                        let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
                        profileVC.userId = trendData.data[recognizer.view!.tag].TrendOwnerId!
                        self.navigationController?.pushViewController(profileVC, animated: true)
                    }
                    else
                    {
                        //                        (self.tabBarController!).selectedIndex = 4
                    }
                    
                } else {
                    print("Tapped none")
                }
            }
            
        }
    }
    
    // MARK: - Config Trend Table View Cell
    func configTrendTableViewCell(tableView: UITableView, withIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "TrendTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! TrendTableViewCell
        trendImagesCollectionView = cell.trendImageCollectionView
        
        // Register Collection View Cells
        registerCollectionViewCells()
        cell.trendCurrentImageLabel.layer.cornerRadius = 3.0
        // Testing
        var calculatedWidth = CGFloat()
        var calculatedHeight = CGFloat()
        calculatedWidth = self.view.frame.size.width
        cell.profileImageView.layer.cornerRadius = 0.5 * cell.profileImageView.bounds.size.width
        cell.profileImageView.clipsToBounds = true
        
        calculatedHeight = (CGFloat(trendData.data[indexPath.row].ImageList![0].ImageHeight ?? 250) * calculatedWidth) / (CGFloat(trendData.data[indexPath.row].ImageList![0].ImageWidth ?? 250))
        
        if trendData.data[indexPath.row].IsTrendShared == false{
            
            if trendData.data[indexPath.row].IsCustomAd{ // Custom Ad
                //            tableView.rowHeight = calculatedHeight
                cell.btnLearnMore.isHidden = false
                cell.btnLearnMore.tag = indexPath.row
                cell.btnLearnMore.addTarget(self, action: #selector(btnLearnMoreAction(_:)), for: .touchUpInside)
                cell.profileImageView.isHidden = true
                cell.shareDescLabel.text = ""
                cell.cnstrntShareDescHeight.constant = 0
                cell.cnstrTrendHeaderViewHeight.constant = 0
                cell.cnstrTrendStatusViewHeight.constant = 0
                cell.cnstrTrendViewCommentViewHeight.isActive = true
                cell.trendViewCommentsView.isHidden = true
                cell.trendStatusView.isHidden = true
                cell.clipsToBounds = true
                //
                cell.videoLayer.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: calculatedHeight)
                //
            }else{
                //             tableView.rowHeight = 286 + calculatedHeight - 61
                cell.btnLearnMore.isHidden = true
                cell.profileImageView.isHidden = false
                cell.profileImageView.layer.cornerRadius = 0.5 * cell.profileImageView.bounds.size.width
                cell.profileImageView.clipsToBounds = true
                
                cell.shareDescLabel.text = ""
                cell.cnstrntShareDescHeight.constant = 0
                cell.cnstrTrendViewCommentViewHeight.isActive = false
                cell.trendViewCommentsView.isHidden = false
                cell.trendStatusView.isHidden = false
                cell.profileNameLabel.text = "\(trendData.data[indexPath.row].UserProfileFirstName ?? "") \(trendData.data[indexPath.row].UserProfileLastName ?? "")"
                cell.profileNameLabel.textColor = UIColor.darkText
                cell.profileNameLabel.tag = indexPath.row
                cell.profileNameTap.numberOfTapsRequired = 1
                cell.profileNameLabel.isUserInteractionEnabled = true
                cell.profileNameTap = UITapGestureRecognizer(target: self, action: #selector(tapActionForLabel(_:)))
                cell.profileNameLabel.addGestureRecognizer(cell.profileNameTap)
                cell.btnFullScrProfileImg.tag = indexPath.row
                cell.btnFullScrProfileImg.addTarget(self, action: #selector(btnFullScrProfileImgAction(_:)), for: .touchUpInside)
                
                cell.locationLabel.text = trendData.data[indexPath.row].TrendLocation ?? ""
                cell.cnstrTrendHeaderViewHeight.constant = CGFloat(trendHeaderViewHeight)
                cell.cnstrTrendStatusViewHeight.constant = CGFloat(trendStatusViewHeight)
                
                if trendData.data[indexPath.row].UserProfileImage != nil {
                    cell.profileImageView.sd_setImage(with: URL(string: trendData.data[indexPath.row].UserProfileImage!), placeholderImage: #imageLiteral(resourceName: "img_selectProfilePic"))
                }
                else
                {
                    cell.profileImageView.image = #imageLiteral(resourceName: "img_selectProfilePic")
                }
                
            }
        }
        else
        {
            cell.btnLearnMore.isHidden = true
            cell.profileImageView.isHidden = false
            cell.profileImageView.layer.cornerRadius = 0.5 * cell.profileImageView.bounds.size.width
            cell.profileImageView.clipsToBounds = true
            
            tableView.rowHeight = 286 + calculatedHeight - 28
            cell.cnstrntShareDescHeight.constant = 33
            cell.trendViewCommentsView.isHidden = false
            cell.cnstrTrendViewCommentViewHeight.isActive = false
            cell.trendStatusView.isHidden = false
            cell.shareDescLabel.text = trendData.data[indexPath.row].SharedTrendDescription ?? ""
            
            cell.btnFullScrProfileImg.tag = indexPath.row
            cell.btnFullScrProfileImg.addTarget(self, action: #selector(btnFullScrProfileImgAction(_:)), for: .touchUpInside)
            
            cell.profileNameLabel.tag = indexPath.row
            cell.profileNameTap.numberOfTapsRequired = 1
            cell.profileNameLabel.isUserInteractionEnabled = true
            cell.profileNameTap = UITapGestureRecognizer(target: self, action: #selector(tapActionForLabel(_:)))
            cell.profileNameLabel.addGestureRecognizer(cell.profileNameTap)
            
            let color1 = UIColor.darkText
            // create the attributed colour
            let attributedStringColor = [NSAttributedString.Key.foregroundColor : color1];
            // create the attributed string
            let attributedString = NSMutableAttributedString(string: "\(trendData.data[indexPath.row].UserProfileFirstName!) \(String(describing: trendData.data[indexPath.row].UserProfileLastName!))", attributes: attributedStringColor)
            // Set the label
            
            
            
            // create the attributed colour
            let attrs1 = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 17), NSAttributedString.Key.foregroundColor : UIColor.black]
            //            let attributedStringColor2 = [NSAttributedString.Key.foregroundColor : color2];
            let attributedString2 = NSMutableAttributedString(string: " shared ", attributes: attrs1)
            attributedString.append(attributedString2)
            let color3 = UIColor.darkText
            let attributedStringColor3 = [NSAttributedString.Key.foregroundColor : color3];
            let attributedString3 = NSMutableAttributedString(string: "\(trendData.data[indexPath.row].TrendOwner ?? "")'s post", attributes: attributedStringColor3)
            attributedString.append(attributedString3)
            cell.profileNameLabel.attributedText = attributedString
            
            cell.locationLabel.text = ""
            cell.cnstrTrendHeaderViewHeight.constant = CGFloat(trendHeaderViewHeight)
            cell.cnstrTrendStatusViewHeight.constant = CGFloat(trendStatusViewHeight)
            
            if trendData.data[indexPath.row].UserProfileImage != nil {
                cell.profileImageView.sd_setImage(with: URL(string: trendData.data[indexPath.row].UserProfileImage!), placeholderImage: #imageLiteral(resourceName: "img_selectProfilePic"))
            }
            else
            {
                cell.profileImageView.image = #imageLiteral(resourceName: "img_selectProfilePic")
            }
        }
        
        if !trendData.data[indexPath.row].IsCustomAd{
            cell.videoLayer.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: calculatedHeight+15)
        }
        
        
        // Data
//        cell.trendExpiryLabel.text = "Expires in \(trendData.data[indexPath.row].RemainingExpiryTime ?? "")"
//        cell.trendAddedLabel.text = "\(trendData.data[indexPath.row].TimeAgo ?? "") Ago"
        cell.trendPostTitleLabel.text = trendData.data[indexPath.row].Title
        cell.postCommentLabel.text = trendData.data[indexPath.row].Description
        cell.viewAllCommentsButton.setTitle("View All \(trendData.data[indexPath.row].CommentCount ?? "") Comment", for: .normal)
        cell.commentsCountLabel.text = "\(trendData.data[indexPath.row].CommentCount ?? "")"
        cell.likesCountLabel.text = "\(trendData.data[indexPath.row].TrendLikesCount)"
        cell.shareCountLabel.text = "\(trendData.data[indexPath.row].TrendShareCount)"
        cell.viewsCountLabel.text = "\(trendData.data[indexPath.row].TrendViewCount ?? "")"
        
        
//        cell.txtAddComment.attributedPlaceholder = NSAttributedString(string: "Write a comment...",
//                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.baseColor])
        
        
        if trendData.data[indexPath.row].IsTrenderOnline
        {
            cell.statusImageView.image = #imageLiteral(resourceName: "img_Active")
        }
        else
        {
            cell.statusImageView.image = #imageLiteral(resourceName: "img_inactive")
        }
        
        if trendData.data[indexPath.row].AreYouFollowingTrender == 0{
            cell.followStatusButton.isHidden = false
            cell.followStatusButton.setTitle(MySingleton.shared.selectedLangData.following, for: .normal)
            cell.followStatusButton.setTitleColor(UIColor.baseColor, for: .normal)
            cell.followStatusButton.tag = indexPath.row
            cell.followStatusButton.addTarget(self, action: #selector(FollowButtonAction(_:)), for: .touchUpInside)
            cell.otherUserProfileButton.isHidden = true
            //            cell.otherUserProfileButton.tag = indexPath.row
            //            cell.otherUserProfileButton.addTarget(self, action: #selector(OtherProfileAction(_:)), for: .touchUpInside)
            
        }
        else if trendData.data[indexPath.row].AreYouFollowingTrender == 1{
            cell.followStatusButton.isHidden = false
            cell.followStatusButton.setTitle(MySingleton.shared.selectedLangData.follow, for: .normal)
            cell.followStatusButton.setTitleColor(UIColor.lightGray, for: .normal)
            cell.followStatusButton.tag = indexPath.row
            cell.followStatusButton.addTarget(self, action: #selector(FollowButtonAction(_:)), for: .touchUpInside)
            cell.otherUserProfileButton.isHidden = true
            //            cell.otherUserProfileButton.tag = indexPath.row
            //            cell.otherUserProfileButton.addTarget(self, action: #selector(OtherProfileAction(_:)), for: .touchUpInside)
        }
        else if trendData.data[indexPath.row].AreYouFollowingTrender == 2{
            cell.followStatusButton.isHidden = false
            cell.followStatusButton.setTitle(MySingleton.shared.selectedLangData.Requested, for: .normal)
            cell.followStatusButton.setTitleColor(UIColor.lightGray, for: .normal)
            cell.followStatusButton.tag = indexPath.row
            cell.followStatusButton.addTarget(self, action: #selector(FollowButtonAction(_:)), for: .touchUpInside)
            cell.otherUserProfileButton.isHidden = true
            //            cell.otherUserProfileButton.tag = indexPath.row
            //            cell.otherUserProfileButton.addTarget(self, action: #selector(OtherProfileAction(_:)), for: .touchUpInside)
        }
        else
        {
            cell.followStatusButton.isHidden = true
            cell.otherUserProfileButton.isHidden = true
            //            cell.otherUserProfileButton.tag = indexPath.row
            //            cell.otherUserProfileButton.addTarget(self, action: #selector(OtherProfileAction(_:)), for: .touchUpInside)
        }
        
        if trendData.data[indexPath.row].EnableTrendLike == true {
            cell.likeButton.isHidden = false
        }
        else {
            cell.likeButton.isHidden = true
        }
        if trendData.data[indexPath.row].IsTrendLike == true {
            cell.trendLikesImageView.image = #imageLiteral(resourceName: "Like_clk")
        }
        else {
            cell.trendLikesImageView.image = #imageLiteral(resourceName: "Like")
        }
        cell.likeButton.accessibilityIdentifier = String(indexPath.row)
        cell.likeButton.tag = trendData.data[indexPath.row].TrendId
        cell.likeButton.addTarget(self, action: #selector(didTapLikeButton), for: .touchUpInside)
        cell.trendImageCollectionView.tag = indexPath.row
        cell.trendImageCollectionView.delegate = self
        cell.trendImageCollectionView.dataSource = self
        cell.trendImagesPageControl.numberOfPages = trendData.data[indexPath.row].ImageList!.count //trendImagesCollectionArray[indexPath.row].count //trendData.data[indexPath.row].ImageList.count
        cell.trendImagesPageControl.hidesForSinglePage = true
        trendImagesPageControl = cell.trendImagesPageControl
        trendCurrentImageLabel = cell.trendCurrentImageLabel
        cell.trendImageCollectionView.reloadData()
        cell.trendImageCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: true)
        
        
        cell.trendCurrentImageLabel.text = "1/\(trendData.data[indexPath.row].ImageList!.count)"
        
        cell.addCommentButton.tag = indexPath.row
        cell.addCommentButton.addTarget(self, action: #selector(self.addCommentAction(_:)), for: .touchUpInside)
        
        if(trendData.data[indexPath.row].ImageList!.count > 1)
        {
            cell.trendCurrentImageLabel.isHidden = false
        }
        else
        {
            cell.trendCurrentImageLabel.isHidden = true
        }
        
        if trendData.data[indexPath.row].ImageList![0].Media == "VIDEO"{
            cell.videoURL = trendData.data[indexPath.row].ImageList![0].VideoUrl
            cell.cnstrntVideoWidth.constant = cell.trendStatusView.frame.size.width/4
            cell.trendViewsView.isHidden = false
            cell.imgVideoIcon.isHidden = false
        }
        else
        {
            cell.videoURL = nil
            cell.cnstrntVideoWidth.constant = 0
            cell.trendViewsView.isHidden = true
            cell.imgVideoIcon.isHidden = true
        }
        
        
        
        cell.trendMoreButton.tag = indexPath.row
        cell.trendMoreButton.addTarget(self, action: #selector(trendMoreButtonAction(_:)), for: .touchUpInside)
        
        cell.shareCountButton.tag = indexPath.row
        cell.shareCountButton.addTarget(self, action: #selector(shareCountButtonAction(_:)), for: .touchUpInside)
        
        cell.btnShareTrend.tag = indexPath.row
        cell.btnShareTrend.addTarget(self, action: #selector(btnShareTrendAction(_:)), for: .touchUpInside)
        
        cell.likeCountButton.tag = indexPath.row
        cell.likeCountButton.addTarget(self, action: #selector(likeCountButtonAction(_:)), for: .touchUpInside)
        cell.viewsCountButton.tag = indexPath.row
        cell.viewsCountButton.addTarget(self, action: #selector(viewsCountButtonAction(_:)), for: .touchUpInside)
        
        cell.txtAddComment.delegate = self
        cell.txtAddComment.returnKeyType = .send
        cell.txtAddComment.tag = indexPath.row
        if MySingleton.shared.loginObject.ProfilePic != ""
        {
            cell.profileButton.layer.cornerRadius = 0.5 * cell.profileButton.bounds.size.width
            cell.profileButton.clipsToBounds = true
            cell.profileButton.sd_setImage(with: URL(string: MySingleton.shared.loginObject.ProfilePic!), for: .normal)
        }
        else
        {
            //            cell.profileButton.layer.cornerRadius = 0.5 * cell.profileButton.bounds.size.width
            //            cell.profileButton.clipsToBounds = true
            cell.profileButton.setImage(#imageLiteral(resourceName: "img_selectProfilePic"), for: .normal)
        }
        
        cell.selectedLang(trendData: self.trendData, index: indexPath.row)
        
        //        cell.setNeedsLayout()
        //        cell.layoutIfNeeded()
        
        return cell
    }
    
    @objc func shareCountButtonAction(_ sender : UIButton)
    {
        let dataToSend = ["TrendId" : self.trendData.data[sender.tag].TrendId, "Type": "ShareCount"] as [String : Any]
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let likeListVC = storyboard.instantiateViewController(withIdentifier:"LikesListViewController") as! LikesListViewController
        
        likeListVC.trendData = dataToSend
        //        likeListVC.trendId = self.trendData.data[sender.tag].TrendId
        self.navigationController?.pushViewController(likeListVC, animated: true)
        
            
        
//        self.performSegue(withIdentifier: "DashboardToLikesCount", sender: dataToSend)
    }
    
    @objc func btnShareTrendAction(_ sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "ShareInternalViewController") as! ShareInternalViewController
        destination.trendId = self.trendData.data[sender.tag].TrendId
        destination.friendsPopupDelegate = self
        let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height)
        self.navigationController?.present(pop, animated: true, completion: nil)
    }
    
    func addComment(index:Int,text:String)
    {
        self.view.endEditing(true)
        var param = [String : Any]()
        
        param = ["TrendID":trendData.data[index].TrendId,"CommentId":"0","CommentText":text ,"IsImage":"false",
                 "ImagePath":[],"gifID":"","TaggedUsers":[]] as [String : Any]
        
        WebServices().callUserService1(service: UserServices.addComment, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
            if(serviceResponse["Status"] as! Bool == true)
            {
                 DispatchQueue.main.async {
                let cell = self.aboutDiscussionTbl.cellForRow(at: IndexPath(row: index, section: 0))as? TrendTableViewCell
                self.trendData.data[index].CommentCount = String(Int(self.trendData.data[index].CommentCount ?? "")! + 1)
                    cell!.commentsCountLabel.text = "\(self.trendData.data[index].CommentCount ?? "")"
                cell!.txtAddComment.text = ""
                }
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        })
    }
    
    @objc func addCommentAction(_ sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Comment", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "CommentViewController") as! CommentViewController
        destination.trendId = trendData.data[sender.tag].TrendId
        destination.trendIndex = sender.tag
        destination.delegate = self
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @objc func viewsCountButtonAction(_ sender : UIButton)
    {
        let dataToSend = ["TrendId" : self.trendData.data[sender.tag].TrendId, "Type": "ViewsCount"] as [String : Any]
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let profileVC = storyboard.instantiateViewController(withIdentifier: "LikesListViewController") as! LikesListViewController
        profileVC.trendData = dataToSend
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    @objc func OtherProfileAction(_ sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
        let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
        profileVC.userId = trendData.data[sender.tag].UserProfileId
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    @objc func FollowButtonAction(_ sender : UIButton)
    {
        if sender.currentTitle == MySingleton.shared.selectedLangData.follow{
            //follow api
            callApiForFavouriteUnfavourite(urlParam: String(trendData.data[sender.tag].UserProfileId), service: .followUser, index: sender.tag)
        }
        else if sender.currentTitle == MySingleton.shared.selectedLangData.following
        {
            //unfollow api
            callApiForFavouriteUnfavourite(urlParam: String(trendData.data[sender.tag].UserProfileId), service: .unfollowUser, index: sender.tag)
        }
    }
    
    @objc func likeCountButtonAction(_ sender : UIButton)
    {
        
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let likeListVC = storyboard.instantiateViewController(withIdentifier:"LikesListViewController") as! LikesListViewController
        let dataToSend = ["TrendId" : self.trendData.data[sender.tag].TrendId, "Type": "LikesCount"] as [String : Any]
        likeListVC.trendData = dataToSend
//        likeListVC.trendId = self.trendData.data[sender.tag].TrendId
        self.navigationController?.pushViewController(likeListVC, animated: true)
        
        //        self.performSegue(withIdentifier: "DashboardToLikesCount", sender: self.trendData.data[sender.tag].TrendId)
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "DashboardToLikesCount" {
//            let likesListViewController = segue.destination as! LikesListViewController
//            likesListViewController.trendId = sender as! Int
//
//        }
//        else if segue.identifier == "DashboardToFullScreen" {
//            let fullScreenViewController = segue.destination as! FullScreenViewController
//            fullScreenViewController.imageList = sender as! [TrendListImageData]
//            fullScreenViewController.pageIndex = pageIndex
//        }
//    }
    
    func callApiForFavouriteUnfavourite(urlParam : String, service : UserServices, index : Int){
        WebServices().callUserService(service: service, urlParameter: urlParam, parameters: nil, isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
            DispatchQueue.main.async {
            if service == .markTrendFavourite{
                self.trendData.data[index].IsTrendFavourite = true
            }
            else if service == .markTrendUnfavourite
            {
                self.trendData.data[index].IsTrendFavourite = false
            }
            else if service == .followUser{
                self.trendData.data[index].AreYouFollowingTrender = 1
                let cell = self.aboutDiscussionTbl.cellForRow(at: IndexPath(row: index, section: 0))as! TrendTableViewCell
                cell.followStatusButton.setTitle(MySingleton.shared.selectedLangData.following, for: .normal)
                cell.followStatusButton.setTitleColor(UIColor.baseColor, for: .normal)
                
            }
            else if service == .unfollowUser{
                self.trendData.data[index].AreYouFollowingTrender = 0
                let cell = self.aboutDiscussionTbl.cellForRow(at: IndexPath(row: index, section: 0))as! TrendTableViewCell
                cell.followStatusButton.setTitle(MySingleton.shared.selectedLangData.follow, for: .normal)
                cell.followStatusButton.setTitleColor(UIColor.lightGray, for: .normal)
            }else if service == .deleteTrend{
                //                self.getAllTrends()
                self.trendData.data.remove(at: index)
//                self.trendImagesArray.remove(at: index)
                self.aboutDiscussionTbl.reloadData()
            }
            else if service == .setTrendPushNotificationStatus
            {
                if(serviceResponse["Status"] as! Bool == true)
                {
                    if (serviceResponse["Message"] as! String).contains("disabled")
                    {
                        self.trendData.data[index].IsPushNotificationEnabled = false
                    }
                    else
                    {
                        self.trendData.data[index].IsPushNotificationEnabled = true
                    }
                }
                else
                {
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }
        }
        })
    }
    
    @objc func trendMoreButtonAction(_ sender : UIButton)
    {
        let cell = aboutDiscussionTbl.cellForRow(at: IndexPath(row: sender.tag, section: 0))as! TrendTableViewCell
        
        cell.trendDropDown.anchorView = sender
        cell.trendDropDown.bottomOffset = CGPoint(x: 0, y:(cell.trendDropDown.anchorView?.plainView.bounds.height)!)
        cell.trendDropDown.topOffset = CGPoint(x: 0, y:-(cell.trendDropDown.anchorView?.plainView.bounds.height)!)
        
        cell.trendDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            print(sender.tag)
            
            switch index{
            case 0:
                let dashboradStoryboard = UIStoryboard(name: "Dashboard", bundle: nil)
                let destination = dashboradStoryboard.instantiateViewController(withIdentifier: "ShareInternalViewController") as! ShareInternalViewController
                destination.trendId = self.trendData.data[sender.tag].TrendId
                destination.friendsPopupDelegate = self
                let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height)
                self.navigationController?.present(pop, animated: true, completion: nil)
                
            case 2:
                if item == MySingleton.shared.selectedLangData.favorite{
                    self.callApiForFavouriteUnfavourite(urlParam: String(self.trendData.data[sender.tag].TrendId), service: .markTrendFavourite, index: sender.tag)
                }
                else
                {
                    self.callApiForFavouriteUnfavourite(urlParam: String(self.trendData.data[sender.tag].TrendId), service: .markTrendUnfavourite, index: sender.tag)
                }
                
            case 3:
                self.callApiForFavouriteUnfavourite(urlParam: String(self.trendData.data[sender.tag].TrendId), service: .setTrendPushNotificationStatus, index: sender.tag)
                
            case 4:
                let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
                let destination = storyboard.instantiateViewController(withIdentifier: "ReportTrendViewController") as! ReportTrendViewController
                let pop = PopUpViewController(destination,withHeight: 270.0)
                self.navigationController?.present(pop, animated: true, completion: nil)
                ////                self.navigationController?.present(destination, animated: true, completion: nil)
                //                self.present(destination, animated: true, completion: nil)
                
            case 5:
                let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
                let destination = storyboard.instantiateViewController(withIdentifier: "SelectFolderViewController") as! SelectFolderViewController
                destination.createFolderPopupDelegate = self
                self.saveFolderTrendId = self.trendData.data[sender.tag].TrendId
                destination.trendId = self.trendData.data[sender.tag].TrendId
                let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height * 0.7)
                self.navigationController?.present(pop, animated: true, completion: nil)
                
            case 6: //break
                let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
                let destination = storyboard.instantiateViewController(withIdentifier: "SetTrendViewController") as! SetTrendViewController
                destination.editTrendParam = self.trendData.data[sender.tag]
                destination.isEdit = true
                //  (self.tabBarController!).selectedIndex = 2
                self.navigationController?.pushViewController(destination, animated: true)
                
            case 7:
                let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: MySingleton.shared.selectedLangData.confirmation, message: MySingleton.shared.selectedLangData.Are_you_sure_you_want_to_delete_this_trend, preferredStyle: .alert)
                let deleteActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.delete, style: .default) { void in
                    self.callApiForFavouriteUnfavourite(urlParam: String(self.trendData.data[sender.tag].TrendId), service: .deleteTrend, index: sender.tag)
                }
                let cancelActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.cancel, style: .cancel) { void in
                }
                actionSheetControllerIOS8.addAction(deleteActionButton)
                actionSheetControllerIOS8.addAction(cancelActionButton)
                actionSheetControllerIOS8.popoverPresentationController?.sourceView = self.view
                actionSheetControllerIOS8.popoverPresentationController?.sourceRect = self.view.bounds
                self.present(actionSheetControllerIOS8, animated: true, completion: nil)
                
            default:
                break
            }
        }
        
        cell.trendMoreButtonAction(index: sender.tag, trendData: self.trendData)
     
    }
    
    // MARK: - Config Trend Image Collection View Cell
    func configTrendImagesCollectionViewCell(collectionView: UICollectionView, withIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = "ImageTrendCollectionViewCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! ImageTrendCollectionViewCell
        cell.trendImageView.sd_setImage(with: URL(string: trendData.data[collectionView.tag].ImageList![indexPath.item].TrendImage), placeholderImage: #imageLiteral(resourceName: "logo_navigation"))//sd_setImage(with: URL(string: trendData.data[collectionView.tag].ImageList[indexPath.item].TrendImage), placeholderImage: #imageLiteral(resourceName: "favourite"), options: SDWebImageOptions.cacheMemoryOnly, completed: nil)//trendImagesCollectionArray[collectionView.tag][indexPath.item]//sd_setImage(with: URL(string: trendData.data[collectionView.tag].ImageList[indexPath.item].TrendImage), placeholderImage: #imageLiteral(resourceName: "favourite"), options: SDWebImageOptions.cacheMemoryOnly, completed: nil)
        return cell
    }
    
    // MARK: - Config Home Collection View Cell
    func configHomeCollectionViewCell(collectionView: UICollectionView, withIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = "HomeCollectionViewCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! HomeCollectionViewCell
        
        cell.trendImageView.image = testImageArray[indexPath.item]
        cell.trendImageView.contentMode = .scaleAspectFill
        collectionView.backgroundColor = .red
        cell.backgroundColor = .white
        return cell
    }
    
}

extension GroupDetailsVC:UITableViewDelegate,UITableViewDataSource,GroupDetailsHeaderDelegate{
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return UIScreen.main.bounds.height * 0.3
        return UITableView.automaticDimension
    }
    
   
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "GroupDetailsHeader") as! GroupDetailsHeader

        cell.groupMembersImagesCollectionView.register(UINib(nibName: GroupDetailMemberImagesCVC.reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: GroupDetailMemberImagesCVC.reuseIdentifier)
        cell.addMemberBtn.addTarget(self, action: #selector(addMembersToGroup), for: .touchUpInside)
        cell.aboutBtn.addTarget(self, action: #selector(highlightAboutView), for: .touchUpInside)
        cell.discussionBtn.addTarget(self, action: #selector(highlightDiscussionView), for: .touchUpInside)
        cell.listGridToggleBtn.addTarget(self, action: #selector(toggleListGridView), for: .touchUpInside)
        cell.postTrendBtn.addTarget(self, action: #selector(setTrend), for: .touchUpInside)
        
        if getPostTrendViewHeight{
            getPostTrendViewHeight = false
            self.postTrendInnerViewHeightConst = cell.postTrendInnerView.frame.height
            self.postTrendViewHeightConst = cell.postTrendView.frame.height
            self.listGridToggleBtnHeightCons = cell.listGridToggleBtn.frame.height
        }
        
        
        cell.memberImages = self.memberImages;
       
        cell.setupElements()
        cell.delegate = self
        self.groupDetailHeaderFunctions(cell: cell)
        
        return cell
    }
    
    func groupDetailHeaderFunctions(cell:GroupDetailsHeader){
       let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showZoomedImage))
        cell.groupImage.addGestureRecognizer(tapGesture)
        cell.groupImage.isUserInteractionEnabled = true
    }
    
    
    @objc func addMembersToGroup() {
        let storyboard = UIStoryboard(name: "GroupSB", bundle: nil)
        let addGroupVC = storyboard.instantiateViewController(withIdentifier: "AddGroupMembersVC") as! AddGroupMembersVC
        addGroupVC.groupId = self.groupId
        addGroupVC.groupName = self.groupName
        addGroupVC.privacy = self.groupPrivacy
        self.navigationController?.pushViewController(addGroupVC, animated: true)
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isAboutUsViewVisible{
            self.noDataLbl.isHidden = true
            if self.groupDetails.data.isEmpty{
                return aboutTableRows
            }else{
                return (self.groupDetails.data[0].IsUserAGroupMember || self.groupDetails.data[0].IsUserAdmin || (self.groupDetails.data[0].GroupPrivacy == GroupPrivacy.publicGroup.rawValue)) ? aboutTableRows : 1;
            }
        }else{
            if trendData.data.isEmpty,isListView{
                self.noDataLbl.isHidden = false
            }else{
                self.noDataLbl.isHidden = true
            }
            if isListView{
                return trendData.data.count
            }else{ // Grid View
                return 1
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isAboutUsViewVisible{
            tableView.rowHeight = UITableView.automaticDimension
            if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: AboutGroupTVC.reuseIdentifier, for: indexPath) as! AboutGroupTVC
                
                if self.groupDetails.data.count > 0{
                    cell.groupTypeLbl.text = "\(Utilities.groupPrivacyDetails[(self.groupDetails.data[0].GroupPrivacy ?? 1) - 1][0] as! String) \(MySingleton.shared.selectedLangData.group)"
                    cell.groupPrivacyDescription.text = Utilities.groupPrivacyDetails[(self.groupDetails.data[0].GroupPrivacy ?? 1) - 1][1] as? String
                    cell.groupDescription.text = self.groupDetails.data[0].GroupDescription ?? ""
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "MM/dd/yy"
                    let date = dateFormatter.date(from: groupDetails.data[0].CreatedOn!)
                    
                    let newDateFormat = DateFormatter()
                    newDateFormat.dateFormat = "MMM d, yyyy"
                    
                    let newDate = newDateFormat.string(from: date!)
                    
                    cell.groupCreationTime.text = "\(MySingleton.shared.selectedLangData.group) \(MySingleton.shared.selectedLangData.created_on) \(newDate)."
                    
                    if self.groupDetails.data[0].IsUserAdmin
                    {
                        cell.btnEdit.isHidden = false
                        cell.btnEdit.tag = indexPath.row
                        cell.btnEdit.addTarget(self, action: #selector(self.btnEditTitle(_:)), for: .touchUpInside)
                    }
                    else
                    {
                         cell.btnEdit.isHidden = true
                    }
                }
                
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: AboutGroupMemberTVC.reuseIdentifier, for: indexPath) as! AboutGroupMemberTVC
                if self.groupDetails.data.count > 0{
                    cell.groupMemberCountLbl.text = "\(MySingleton.shared.selectedLangData.members) ● \(groupDetails.data[0].TotalMember ?? 0)"
                    
                    cell.totalAdminMember = groupDetails.data[0].GroupAdminCount ?? 0
                    cell.totalGroupMember = groupDetails.data[0].GroupMemberCount ?? 0
                    cell.adminImages = groupAdminImages
                    cell.memberImages = groupMemberImages
                    cell.memberNames = groupMemberNames
                    cell.adminNames = groupAdminNames
                    
                    if (groupDetails.data[0].GroupPrivacy == GroupPrivacy.publicGroup.rawValue) || (groupDetails.data[0].IsUserAGroupMember){
                        cell.seeAllMembersBtn.addTarget(self, action: #selector(showAllGroupMembers), for: .touchUpInside)
                        cell.btnsendAdmin.addTarget(self, action: #selector(showAllGroupMembers), for: .touchUpInside)
                        cell.btnSendMember.addTarget(self, action: #selector(showAllGroupMembers), for: .touchUpInside)
                    }
                    else{
                        
                        cell.seeAllMembersBtn.removeTarget(nil, action: nil, for: .allEvents)
                        cell.btnsendAdmin.removeTarget(nil, action: nil, for: .allEvents)
                        cell.btnSendMember.removeTarget(nil, action: nil, for: .allEvents)
                    }
                    
                    cell.setup()
                }
                return cell
            }
        }else{
            if isListView{
                if trendData.data[indexPath.row].IsGoogleAd{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "GoogleAdTVC", for: indexPath) as! GoogleAdTVC
                    cell.bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
                    cell.bannerView.rootViewController = self
                    cell.bannerView.load(GADRequest())
                    return cell
                }else{
                    return configTrendTableViewCell(tableView: tableView, withIndexPath: indexPath)
                }
            }else{
                let cell = UITableViewCell(style: .default, reuseIdentifier: "GridCell")
//                cell.clipsToBounds = false;
                    let storyboard = UIStoryboard(name: "SearchSB", bundle: nil)
                    let dest = storyboard.instantiateViewController(withIdentifier: "GridViewVC") as! GridViewVC
                    dest.hideSearchbar = true
                    dest.isGroupTrends = true
                    dest.groupId = self.groupId
                    //add as a childviewcontroller
                    addChild(dest)
                
                    // Add the child's View as a subview
                    cell.addSubview(dest.view)
                
                    dest.view.frame = cell.bounds
                    dest.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
                    // tell the childviewcontroller it's contained in it's parent
                    dest.didMove(toParent: self)
                
                return cell
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if !isAboutUsViewVisible{
            if let videoCell
                = cell as? ASAutoPlayVideoLayerContainer, let _ = videoCell.videoURL {
                ASVideoPlayerController.sharedVideoPlayer.removeLayerFor(cell: videoCell)
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if !isAboutUsViewVisible{
           pausePlayeVideos()
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
        if !isAboutUsViewVisible{
            if isListView{
//                let spinner = UIActivityIndicatorView(style: .gray)
//                spinner.startAnimating()
//                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.aboutDiscussionTbl.bounds.width, height: CGFloat(44))
//                self.aboutDiscussionTbl.tableFooterView = spinner
//                self.aboutDiscussionTbl.tableFooterView?.isHidden = false
                
                getAllTrends(Skip: self.trendData.data.count, Take: noOfRecordsPerRequest,lazyLoading:true)
            }
        }
            
      }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !isAboutUsViewVisible{
            if !decelerate {
                pausePlayeVideos()
            }
        }
        
    }
    func pausePlayeVideos(){
        ASVideoPlayerController.sharedVideoPlayer.pausePlayeVideosFor(tableView: aboutDiscussionTbl)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if  !self.isAboutUsViewVisible{
            if trendData.data.count == 0 {
                return tableView.frame.size.height / 2
            }
        if trendData.data[indexPath.row].IsGoogleAd {
            return CGFloat(googleAdHeight)
        }else {
            var calculatedWidth = CGFloat()
            var calculatedHeight = CGFloat()
            calculatedWidth = self.view.frame.size.width
            
            calculatedHeight = ((CGFloat(trendData.data[indexPath.row].ImageList![0].ImageHeight ?? 250)) * calculatedWidth) / (CGFloat(trendData.data[indexPath.row].ImageList![0].ImageWidth ?? 250))
            if trendData.data[indexPath.row].IsTrendShared == false{
                
                if trendData.data[indexPath.row].IsCustomAd{ // Custom Ad
                    return calculatedHeight
                }
                return 286 + calculatedHeight - 61
            }else{
                return 286 + calculatedHeight - 28
            }
        }
        }
        else
        {
            return UITableView.automaticDimension
        }
    }
   
}

 extension GroupDetailsVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let headerView = self.aboutDiscussionTbl.headerView(forSection: 0) as? GroupDetailsHeader

            if collectionView == trendCollectionView {
                // GRID
                return testImageArray.count
            }
            else {
                // IMAGEs
                print("Trend Image Collection View Tag --- \(collectionView.tag) --- Count --- \(trendData.data[collectionView.tag].ImageList!.count)")
                return self.trendData.data[collectionView.tag].ImageList!.count//trendImagesCollectionArray[collectionView.tag].count
            }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
//        if isAboutUsViewVisible{
        if collectionView == groupMembersImagesCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GroupDetailMemberImagesCVC.reuseIdentifier, for: indexPath) as! GroupDetailMemberImagesCVC
            cell.memberImage.sd_setImage(with: URL(string: memberImages[indexPath.row]), placeholderImage: UIImage(named: "img_selectProfilePic"), options: [.continueInBackground], completed: nil)
            
            if memberImages.count == (indexPath.row + 1){
                print("memberImages Count : ",memberImages.count)
                print("index : ",(indexPath.row + 1))
                //            DispatchQueue.main.async{
                if let viewWithTag = cell.viewWithTag(999){
                    viewWithTag.removeFromSuperview()
                }
                
                let moreBtn = UIButton()
                moreBtn.frame = cell.memberImage.frame
                moreBtn.setBackgroundImage(UIImage(named: "more_1"), for: .normal)
                moreBtn.makeCornerRadius(radius: moreBtn.frame.height/2)
                moreBtn.layer.opacity = 0.3
                moreBtn.tag = 999
//                moreBtn.addTarget(self, action: #selector(self.showAllGroupMembers), for: .touchUpInside)
                cell.addSubview(moreBtn)
                cell.bringSubviewToFront(moreBtn)
                //            }
                
            }else{
                if let viewWithTag = cell.viewWithTag(999){
                    viewWithTag.removeFromSuperview()
                }
            }
            cell.setup()
            return cell
        }else{
            if collectionView == trendCollectionView {
                return configHomeCollectionViewCell(collectionView:collectionView, withIndexPath: indexPath)
            }
            else {
                return configTrendImagesCollectionViewCell(collectionView: collectionView, withIndexPath: indexPath)
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView != groupMembersImagesCollectionView{

            if collectionView == trendCollectionView{
                
            }
            else
            {
                pageIndex = indexPath.item
                let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
                let fullScreen = storyboard.instantiateViewController(withIdentifier:"FullScreenViewController") as! FullScreenViewController
                fullScreen.imageList = trendData.data[collectionView.tag].ImageList!
                self.navigationController?.pushViewController(fullScreen, animated: true)
                //                    self.present(fullScreen, animated: true, completion: nil)
                
                
                // self.performSegue(withIdentifier: "DashboardToFullScreen", sender: trendData.data[collectionView.tag].ImageList)
                
                //            (string: trendData.data[collectionView.tag].ImageList[indexPath.item].TrendImage)
            }
        }
    }
    
    
    @objc func showAllGroupMembers(){
//        let stotyboard = UIStoryboard(name: "GroupSB", bundle: nil)
        let memberListVC =  storyboard?.instantiateViewController(withIdentifier: "SeeAllGroupMembersVC") as! SeeAllGroupMembersVC
        memberListVC.totalMemberCount = groupDetails.data[0].GroupMemberCount ?? 0
        memberListVC.totalAdminCount  = groupDetails.data[0].GroupAdminCount ?? 0
        memberListVC.groupId = groupDetails.data[0].GroupID ?? 0
        memberListVC.isUserAdmin = groupDetails.data[0].IsUserAdmin
        self.navigationController?.pushViewController(memberListVC, animated: true)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == trendCollectionView {
            var calculatedWidth = CGFloat()
            var calculatedHeight = CGFloat()
            if indexPath.item == 0 {
                calculatedWidth = self.view.frame.size.width / 3
                calculatedHeight = calculatedWidth
            }
            else if indexPath.item == 1 {
                calculatedWidth = self.view.frame.size.width / 3
                calculatedHeight = calculatedWidth
            }
            else if indexPath.item == 2 {
                
                
                calculatedWidth = (self.view.frame.size.width / 3) * 2
                calculatedHeight = calculatedWidth
            }
            else {
                calculatedWidth = self.view.frame.size.width / 3
                calculatedHeight = calculatedWidth
            }
            
            return CGSize(width: calculatedWidth, height: calculatedHeight)
        }
        else {
            var calculatedWidth = CGFloat()
            var calculatedHeight = CGFloat()
            calculatedWidth = self.view.frame.size.width
            
            calculatedHeight = ((CGFloat(trendData.data[collectionView.tag].ImageList![0].ImageHeight ?? 250)) * calculatedWidth) / (CGFloat(trendData.data[collectionView.tag].ImageList![0].ImageWidth ?? 250))
            
            return CGSize(width: calculatedWidth, height: calculatedHeight)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == groupMembersImagesCollectionView{

            return 4
        }else{
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        if collectionView == groupMembersImagesCollectionView{

            if collectionView == trendCollectionView {
            }
            else {
                if trendData.data.count > 0 {
                    let cell = aboutDiscussionTbl.cellForRow(at: IndexPath(row: collectionView.tag, section: 0)) as? TrendTableViewCell
                    if cell != nil {
                        cell!.trendImagesPageControl.currentPage = indexPath.item
                        cell!.trendCurrentImageLabel.text = "\(indexPath.item + 1)/\(trendData.data[collectionView.tag].ImageList!.count)"
                    }
                    
                }
            }
        }
    }
}

extension GroupDetailsVC : CreateFolderPopupDelegate{
    func createFolderPopup() {
        let storyboard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
        let nextViewController = storyboard.instantiateViewController(withIdentifier: "NewFolderPoupVC") as! NewFolderPoupVC
        nextViewController.delegate = self
        let pop = PopUpViewController(nextViewController,withHeight: self.view.frame.size.height)
        self.navigationController?.present(pop, animated: true, completion: nil)
    }
    
}

extension GroupDetailsVC : CreateFolderDelegate {
    func controller() {
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "SelectFolderViewController") as! SelectFolderViewController
        destination.createFolderPopupDelegate = self
        destination.trendId = self.saveFolderTrendId
        destination.shareTrendId = self.shareTrendId
        let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height * 0.7)
        self.navigationController?.present(pop, animated: true, completion: nil)
    }
    
    
    func callApi(requestData: Dictionary<String, Any>,url:String)
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        var urlStr:String = url
        
        urlStr = urlStr.replacingOccurrences(of: " ", with: "&")
        let request :NSMutableURLRequest = NSMutableURLRequest(url:NSURL(string:urlStr)! as URL)
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        var TokenValue = String()
        TokenValue = "Bearer " + MySingleton.shared.loginObject.access_token
        print(TokenValue)
        request.addValue(TokenValue, forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        print(urlStr)
        
        if requestData.count != 0
        {
            do
            {
                let jsonData = try JSONSerialization.data(withJSONObject: requestData, options: JSONSerialization.WritingOptions.prettyPrinted)
                request.httpBody = jsonData
            }
            catch let error as NSError
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                }
            }
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest){ data,response,error in
            
            if error != nil
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error!.localizedDescription, completionHandler: nil)
                }
            }
            do
            {
                if (response as? HTTPURLResponse) != nil
                {
                    //////////////////// JSON RESPONSE /////////////////
                    
                    print((response as? HTTPURLResponse)!.statusCode)
                    
                    let jsonString = String(data: data!, encoding: .utf8)
                    print(jsonString!)
                    
                    //////////////////// JSON RESPONSE /////////////////
                    
                    if data != nil
                    {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        print(jsonData)
                        
                        if(jsonData["Status"] as! Bool == true)
                        {
                            DispatchQueue.main.async {
                                MBProgressHUD.hide(for: self.view, animated: true)
//                                for vc in (self.navigationController?.viewControllers ?? []){
//                                    if vc is GroupVC{
//                                        self.navigationController?.popToViewController(vc, animated: true)
//                                    }
                                }
//                            }
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                MBProgressHUD.hide(for: self.view, animated: true)
                                TrenderAlertVC.shared.presentAlertController(message: jsonData["Message"] as! String, completionHandler: nil)
                            }
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true)
                            TrenderAlertVC.shared.presentAlertController(message: error!.localizedDescription, completionHandler: nil)
                        }
                    }
                    
                }
            }
            catch let error as NSError
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                }
            }
        }
        
        task.resume()
    }
}

extension GroupDetailsVC : TrendViewDelegate{
    func IncreaseTrendViewCount(Index: Int) {
        DispatchQueue.main.async {
            let cell = self.aboutDiscussionTbl.cellForRow(at: IndexPath(row: Index, section: 0))as? TrendTableViewCell
            self.trendData.data[Index].TrendViewCount = String (Int(self.trendData.data[Index].TrendViewCount ?? "0")! + 1)
            cell!.viewsCountLabel.text = "\(self.trendData.data[Index].TrendViewCount ?? "0")"
        }
    }
}

extension GroupDetailsVC : TrendCommentDelegate,friendSelectedDelegate,FriendsPopupDelegate, editGroupDelegate {
    func controller(arraySelectedFriends: [TagFriendModel], index: Int, trendId: Int, description: String) {
     //
    }
    
    
    func editcontroller(description: String, param: [String : Any], status: Int) {
        
        
        if status == 2
        {
            
            var param = param
            
            param["Id"] = self.groupId
            param["GroupPrivacy"] = self.groupDetails.data[0].GroupPrivacy!
            param["Description"] = self.groupDetails.data[0].GroupDescription!
            ///////
            self.groupDetails.data[0].GroupName = param["GroupName"] as! String;
            let headerView = self.aboutDiscussionTbl.headerView(forSection: 0)
            
            if let headerView = headerView as? GroupDetailsHeader{
                headerView.groupNameLbl.text = param["GroupName"] as? String;
                
            }
            ///////
            print(param)
            callApi(requestData: param, url: "\(WebServices.baseURL)\(UserServices.createGroup.rawValue)")
        }
        else
        {
            var param = param
            ///////
            self.groupDetails.data[0].GroupDescription = param["Description"] as? String;
            DispatchQueue.main.async{
                self.aboutDiscussionTbl.reloadData();
            }
            ///////
            param["Id"] = self.groupId
            param["GroupPrivacy"] = self.groupDetails.data[0].GroupPrivacy!
            param["GroupName"] = self.groupDetails.data[0].GroupName
            print(param)
            callApi(requestData: param, url: "\(WebServices.baseURL)\(UserServices.createGroup.rawValue)")
        }
        
    }
    
    func friendsPopup(indexShare: Int, trendId: Int, description: String) {
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "FriendListViewController") as! FriendListViewController
        destination.delegate = self
        destination.trendId = trendId
        destination.index = indexShare
        destination.txtdescprition = description
        let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height)
        self.navigationController?.present(pop, animated: true, completion: nil)
    }
    
//    func controller(arraySelectedFriends: [Int], index: Int, trendId: Int,description: String) {
//        self.selectedShareFriends = arraySelectedFriends
//        let destination = self.storyboard?.instantiateViewController(withIdentifier: "ShareInternalViewController") as! ShareInternalViewController
//        destination.trendId = trendId
//        destination.selectedShareFriends = arraySelectedFriends
//        destination.index = index
//        destination.txtdiscription = description
//        let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height)
//        self.navigationController?.present(pop, animated: true, completion: nil)
//    }
    
    func commentCount(Index: Int,CommentCount: Int) {
        let cell = aboutDiscussionTbl.cellForRow(at: IndexPath(row: Index, section: 0))as? TrendTableViewCell
        trendData.data[Index].CommentCount = String(CommentCount)
        cell!.commentsCountLabel.text = trendData.data[Index].CommentCount
  }

}

extension GroupDetailsVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate,CropViewControllerDelegate
{
    /*************************
     Method Name:  openGallary()
     Parameter: nil
     return type: nil
     Desc: This function opens the gallery.
     *************************/
    func openGallary()
    {
        picker.allowsEditing = false
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        present(picker, animated: true, completion: nil)
    }
    
    /*************************
     Method Name:  openCamera()
     Parameter: nil
     return type: nil
     Desc: This function opens the Camera.
     *************************/
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            present(picker, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            
            present(alert, animated: true, completion: nil)
        }
    }
    
    /*************************
     Method Name:  imagePickerControllerDidCancel()
     Parameter: UIImagePickerController
     return type: nil
     Desc: This function is called if user taps cancel button.
     *************************/
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    /*************************
     Method Name:  imagePickerController()
     Parameter: UIImagePickerController, [String : Any]
     return type: nil
     Desc: This function is called when user selects image from gallery.
     *************************/
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        
        dismiss(animated:true, completion: nil)
        let cropViewController = CropViewController(image: chosenImage!)
        cropViewController.delegate = self
        present(cropViewController, animated: true, completion: nil)
    }
    //    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    //    {
    //        let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
    //        print(chosenImage)
    //        self.btnSelectProfilePic.setBackgroundImage(chosenImage, for: UIControl.State.normal)
    //        self.btnCancelPic.isHidden = false
    //        dismiss(animated:true, completion: nil)
    //    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        
       // self.btnSelectProfilePic.setBackgroundImage(image, for: UIControl.State.normal)
       // self.groupImage.image = image
         self.isChangeImage = true
          let headerView = self.aboutDiscussionTbl.headerView(forSection: 0)
        if let headerView = headerView as? GroupDetailsHeader{
            headerView.groupImage.image = image
        }
            let groupImageToUpload = ["FileExt":"jpg","contenttype":"image/jpeg","ImageData":"data:image/jpg;base64,\(Utilities.shared.encodeToBase64String(image: image))"]
            
            let imageArray:[AnyObject] = [groupImageToUpload as AnyObject]
            
        let param = ["GroupPrivacy":self.groupDetails.data[0].GroupPrivacy!,"Id":self.groupId,"GroupName":self.groupDetails.data[0].GroupName!,"Description":self.groupDetails.data[0].GroupDescription ?? "","GroupImage":["Images":imageArray],"GroupThumbnail":["Images":imageArray],"IsClosed":"true","IsChangeImage":"true","IsChangeThumbnail":"true","Memberlist":[]] as [String : Any]
       // print(param)
       
        
       // self.btnCancelPic.isHidden = false
        dismiss(animated: true) {
            self.callApi(requestData: param, url: "\(WebServices.baseURL)\(UserServices.createGroup.rawValue)")
        }
      //  callApi(requestData: param, url: "\(WebServices.baseURL)\(UserServices.createGroup.rawValue)")
    }
}
