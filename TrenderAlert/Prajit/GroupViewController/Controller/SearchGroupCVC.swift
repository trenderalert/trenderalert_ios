//
//  SearchGroupCVC.swift
//  TrenderAlert
//
//  Created by HPL on 11/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class SearchGroupCVC: UICollectionViewCell {

    @IBOutlet weak var groupImage: UIImageView!
    @IBOutlet weak var groupName: UILabel!
    @IBOutlet weak var groupAgeLbl: UILabel!
    
    
    static let reuseIdentifier = "SearchGroupCVC"
    
    var roundedCornerForImage = (UIDevice.current.userInterfaceIdiom == .pad) ? 10 : 5
    
//    var roundedCornerForLabel = (UIDevice.current.userInterfaceIdiom == .pad) ? 10 : 8
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        groupImage.layer.cornerRadius = CGFloat(roundedCornerForImage)
//        groupImage.layer.masksToBounds = true
        groupAgeLblRoundedCorner()
    }
    
    func groupAgeLblRoundedCorner(){
//        groupAgeLbl.layer.cornerRadius = CGFloat(roundedCornerForLabel)
//        groupAgeLbl.layer.masksToBounds = true
        if UIDevice.current.userInterfaceIdiom == .pad{
            groupAgeLbl.makeCornerRadius(radius: groupAgeLbl.frame.height/2)
        }else{
            groupAgeLbl.makeCornerRadius(radius: 8)
        }
        
    }

}
