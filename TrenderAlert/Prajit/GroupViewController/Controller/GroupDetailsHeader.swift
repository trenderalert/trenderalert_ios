//
//  GroupDetailsHeader.swift
//  TrenderAlert
//
//  Created by HPL on 20/05/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

protocol GroupDetailsHeaderDelegate:class{
    func showAllGroupMembers()
}

class GroupDetailsHeader: UITableViewHeaderFooterView {
    
    @IBOutlet weak var lblPostTrend: UILabel!
    @IBOutlet weak var btnEditImg: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnJoin: UIButton!
    @IBOutlet weak var groupImage: UIImageView!
    @IBOutlet weak var postTrendImage: UIImageView!
    @IBOutlet weak var groupNameLbl: UILabel!
    @IBOutlet weak var groupInfoLbl: UILabel!
    @IBOutlet weak var groupMembersImagesCollectionView: UICollectionView!
    @IBOutlet weak var aboutBtn: UIButton!
    @IBOutlet weak var discussionBtn: UIButton!
    
    @IBOutlet weak var postTrendViewHeight: NSLayoutConstraint!
    @IBOutlet weak var postTrendInnerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var postTrendInnerView: UIView!
    @IBOutlet weak var postTrendView: UIView!
     @IBOutlet weak var addMemberBtn: UIButton!
    @IBOutlet weak var listGridToggleBtn: UIButton!
    @IBOutlet weak var listGridToggleBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var postTrendBtn: UIButton!
   
    
    
    var memberImages:[String] = []
    var showMemberBtn:Bool = false
    
    weak var delegate:GroupDetailsHeaderDelegate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupElements(){
        self.aboutBtn.setTitle(MySingleton.shared.selectedLangData.about, for: .normal)
        self.discussionBtn.setTitle(MySingleton.shared.selectedLangData.discussion, for: .normal)
        self.lblPostTrend.text = MySingleton.shared.selectedLangData.post_trend
        aboutBtn.tag = 1
        discussionBtn.tag = 2
        self.btnJoin.makeCornerRadius(radius: 4.0)
        postTrendImage.layer.borderColor = UIColor.white.cgColor
        postTrendImage.layer.borderWidth = 4
        postTrendImage.makeCornerRadius(radius: postTrendImage.frame.width/2)
        self.addMemberBtn.clipsToBounds = true
        self.addMemberBtn.layer.cornerRadius = 5
        
        // PostTrend view corner radius
        self.postTrendInnerView.clipsToBounds = true
        self.postTrendInnerView.layer.cornerRadius = 20
        self.postTrendInnerView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        
        self.groupMembersImagesCollectionView.delegate = self
        self.groupMembersImagesCollectionView.dataSource = self
        groupMembersImagesCollectionView.register(UINib(nibName: GroupDetailMemberImagesCVC.reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: GroupDetailMemberImagesCVC.reuseIdentifier)
    }
}

extension GroupDetailsHeader:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.memberImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GroupDetailMemberImagesCVC.reuseIdentifier, for: indexPath) as! GroupDetailMemberImagesCVC
        if self.memberImages.count != 0{
        cell.memberImage.sd_setImage(with: URL(string: self.memberImages[indexPath.row]), placeholderImage: UIImage(named: "img_selectProfilePic"), options: [.continueInBackground], completed: nil)
        
        if self.memberImages.count == (indexPath.row + 1){
            print("memberImages Count : ",self.memberImages.count)
            print("index : ",(indexPath.row + 1))
            //            DispatchQueue.main.async{
            if let viewWithTag = cell.viewWithTag(999){
                viewWithTag.removeFromSuperview()
            }
            
            if self.showMemberBtn{
                let moreBtn = UIButton()
                moreBtn.frame = cell.memberImage.frame
                moreBtn.setBackgroundImage(UIImage(named: "more_1"), for: .normal)
                moreBtn.makeCornerRadius(radius: moreBtn.frame.height/2)
                moreBtn.layer.opacity = 0.3
                moreBtn.tag = 999
                //// 123
                moreBtn.addTarget(self, action: #selector(self.showAllGroupMembers), for: .touchUpInside)
                cell.addSubview(moreBtn)
                cell.bringSubviewToFront(moreBtn)
            }
            
            //            }
            
        }else{
            if let viewWithTag = cell.viewWithTag(999){
                viewWithTag.removeFromSuperview()
            }
        }
        cell.setup()
        }
        return cell
    }
    
    @objc func showAllGroupMembers(){
        delegate?.showAllGroupMembers()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.height, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == groupMembersImagesCollectionView{
            
            return 4
        }else{
            return 0
        }
    }
}
