//
//  GroupVC.swift
//  TrenderAlert
//
//  Created by HPL on 11/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import SDWebImage

class GroupVC: UIViewController {

    
    @IBOutlet var btnGroupSugg: UIButton!
    @IBOutlet var btnMyGroup: UIButton!
    @IBOutlet weak var groupSearchUTF: UITextField!
    @IBOutlet weak var groupSearchBtn: UIButton!
    @IBOutlet weak var groupCollectionView: UICollectionView!
    @IBOutlet weak var noDataLbl: UILabel!
    var MyGroups = true
    
    var groupList = GroupListModel(){
        didSet{
            searchGroup.data = groupList.data
            groupCollectionView.reloadData()
        }
    }
    var searchGroup = GroupListModel()
    
    let noOfRecordPerRequest = 21
    var totalGroupCount = 1
    var searchText = ""
    var isGroupApiRequestInProgress = false
    var showBottomLoader = true
    
    
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//    }
    var noOfGroups = 0{
        didSet{
            DispatchQueue.main.async {
                self.groupCollectionView.reloadData()
            }
        }
    }
    
    lazy var refreshControl:UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = UIColor.red
        return refreshControl
    }()
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        getGroupListing(Skip:0,Take:noOfRecordPerRequest,lazyLoading:false)
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupCollectionView()
        groupSearchUTF.leftPadding(paddingSize: 15)
        groupSearchUTF.delegate = self
        groupSearchUTF.placeholder = MySingleton.shared.selectedLangData.search_group
        setupNavigationBar()
        self.groupCollectionView.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
         totalGroupCount = 1
         searchText = ""
        groupSearchUTF.text = ""
         isGroupApiRequestInProgress = false
         groupList.data = [];
        self.btnMyGroup.setTitle(MySingleton.shared.selectedLangData.My_Groups, for: .normal)
        self.btnGroupSugg.setTitle(MySingleton.shared.selectedLangData.Groups_Suggestion, for: .normal)
        if MyGroups
        {
            btnMyGroup.layer.backgroundColor = UIColor.baseColor.cgColor
            btnMyGroup.setTitleColor(UIColor.black, for: .normal)
            
            btnGroupSugg.layer.backgroundColor = UIColor.white.cgColor
            btnGroupSugg.setTitleColor(UIColor.friendSBGrayFontColor, for: .normal)
        
            getGroupListing(Skip:0,Take:noOfRecordPerRequest,lazyLoading:false)
        }
        else
        {
            btnGroupSugg.layer.backgroundColor = UIColor.baseColor.cgColor
            btnGroupSugg.setTitleColor(UIColor.black, for: .normal)
            
            btnMyGroup.layer.backgroundColor = UIColor.white.cgColor
            btnMyGroup.setTitleColor(UIColor.friendSBGrayFontColor, for: .normal)
        
            getGroupSuggestionListing(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: false)
        }
    }
    
    func setupNavigationBar(){
        
        let leftBarButton = UIButton()
        leftBarButton.setImage(#imageLiteral(resourceName: "img_MenuBarBtn"), for: .normal)
        leftBarButton.addTarget(self, action: #selector(didTapMenu), for: .touchUpInside)
        let item1 = UIBarButtonItem()
        item1.customView = leftBarButton
        self.navigationItem.leftBarButtonItems = [item1]
        
        
        
//        Utilities.setNavigationBar(viewController:self)
        Utilities.setNavigationBarWithTitle(viewController: self, title: MySingleton.shared.selectedLangData.groups)
        
//        let leftButton = UIBarButtonItem(image: UIImage(named: "img_BackArrow"), style: .plain, target: self, action: #selector(goBack))
        
        let button = UIBarButtonItem(image: UIImage(named: "add_circle"), style: .plain, target: self, action: #selector(goToCreateGroup))
//        navigationItem.leftBarButtonItem = leftButton
        navigationItem.rightBarButtonItem = button
    }
    
    @IBAction func btnMyGroupsAction(_ sender: Any) {
        if MyGroups
        {
            return
        }
        MyGroups = true
        btnMyGroup.layer.backgroundColor = UIColor.baseColor.cgColor
        btnMyGroup.setTitleColor(UIColor.black, for: .normal)
        
        btnGroupSugg.layer.backgroundColor = UIColor.white.cgColor
        btnGroupSugg.setTitleColor(UIColor.friendSBGrayFontColor, for: .normal)
        self.groupList.data = []
        totalGroupCount = 1
        searchText = ""
        isGroupApiRequestInProgress = false
        groupList.data = [];
        getGroupListing(Skip:0,Take:noOfRecordPerRequest,lazyLoading:false)
        self.groupCollectionView.reloadData()
    }
    
    @IBAction func btnGroupSuggestionAction(_ sender: Any) {
        if !MyGroups
        {
            return
        }
        MyGroups = false
        btnGroupSugg.layer.backgroundColor = UIColor.baseColor.cgColor
        btnGroupSugg.setTitleColor(UIColor.black, for: .normal)
        
        btnMyGroup.layer.backgroundColor = UIColor.white.cgColor
        btnMyGroup.setTitleColor(UIColor.friendSBGrayFontColor, for: .normal)
        self.groupList.data = []
        totalGroupCount = 1
        searchText = ""
        isGroupApiRequestInProgress = false
        groupList.data = [];
        getGroupSuggestionListing(Skip:0,Take:noOfRecordPerRequest,lazyLoading:false)
        self.groupCollectionView.reloadData()
    }
    
    @objc func handleRefresh(_ refreshControl:UIRefreshControl){
        self.groupList.data = []
        totalGroupCount = 1
        self.showBottomLoader = true
        if MyGroups
        {
            getGroupListing(Skip:0,Take:noOfRecordPerRequest,lazyLoading:true)
            self.getGroupListing(Skip:0,Take:noOfRecordPerRequest,lazyLoading:true)
        }
        else
        {
            getGroupSuggestionListing(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: true)
        }
        self.groupCollectionView.reloadData()
        self.refreshControl.endRefreshing()
    }
    
    @objc func didTapMenu(_ sender: UIBarButtonItem) {
        if self.menuContainerViewController != nil {
            self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
        }
    }
    
    @objc func goBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func getGroupSuggestionListing(Skip: Int, Take: Int, lazyLoading: Bool,SearchString:String = ""){
        
        if (groupList.data.count >= totalGroupCount) || isGroupApiRequestInProgress{
            return
        }
        
        self.isGroupApiRequestInProgress = true
        
        let param = ["Skip":Skip,"Take":Take,"Search":searchText] as [String:Any]
        
        WebServices().callUserService(service: UserServices.getGroupSuggestion, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: lazyLoading, isHeader: true, CallMethod: .post){(serviceResponse, serviceData) in
            
            if(serviceResponse["Status"] != nil){
                if(serviceResponse["Status"] as! Bool == true){
                    let temp = WebServices().decodeDataToClass(data: serviceData, decodeClass: GroupListModel.self)!
                    self.groupList.data += temp.data
                    self.totalGroupCount = serviceResponse["TotalRecords"] as! Int
                    self.noOfGroups += 1
                    self.isGroupApiRequestInProgress = false
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func getGroupListing(Skip: Int, Take: Int, lazyLoading: Bool,SearchString:String = ""){
        
        if (groupList.data.count >= totalGroupCount) || isGroupApiRequestInProgress{
                return
        }
        
        self.isGroupApiRequestInProgress = true
        
        let param = ["Skip":Skip,"Take":Take,"Search":searchText] as [String:Any]
        
        WebServices().callUserService(service: UserServices.getGroupListing, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: lazyLoading, isHeader: true, CallMethod: .post){(serviceResponse, serviceData) in
            
            if(serviceResponse["Status"] != nil){
                if(serviceResponse["Status"] as! Bool == true){
                    let temp = WebServices().decodeDataToClass(data: serviceData, decodeClass: GroupListModel.self)!
                    self.groupList.data += temp.data
                    self.totalGroupCount = serviceResponse["TotalRecords"] as! Int
                    self.noOfGroups += 1
                    self.isGroupApiRequestInProgress = false
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    @objc func goToCreateGroup(){
        let storyboard = UIStoryboard(name: "GroupSB", bundle: nil)
        let createGroupController = storyboard.instantiateViewController(withIdentifier: "CreateGroupVC") as! CreateGroupVC
        self.navigationController?.pushViewController(createGroupController, animated: true)
    }
    
    func  setupCollectionView(){
        groupCollectionView.delegate = self
        groupCollectionView.dataSource = self
        
        groupCollectionView.register(UINib(nibName: SearchGroupCVC.reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: SearchGroupCVC.reuseIdentifier)
    }
}

extension GroupVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return groupData.count
        
        if groupList.data.isEmpty,!self.isGroupApiRequestInProgress{
            self.noDataLbl.isHidden = false
        }else{
            self.noDataLbl.isHidden = true
        }
        return groupList.data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if (indexPath.row == (self.groupList.data.count - 1)),showBottomLoader{
            if MyGroups
            {
                getGroupListing(Skip: self.groupList.data.count, Take: self.noOfRecordPerRequest, lazyLoading: true)
            }
            else
            {
                getGroupSuggestionListing(Skip: self.groupList.data.count, Take: self.noOfRecordPerRequest, lazyLoading: true)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let group = collectionView.dequeueReusableCell(withReuseIdentifier: SearchGroupCVC.reuseIdentifier, for: indexPath) as! SearchGroupCVC
        group.groupImage.sd_setImage(with: URL(string: groupList.data[indexPath.row].GroupImage ?? ""), placeholderImage: UIImage(named: "Img_Blur"), options: [.continueInBackground], completed: nil)
        group.groupName.text = groupList.data[indexPath.row].GroupName ?? ""
//        group.groupAgeLbl.text = " " + " \(groupList.data[indexPath.row].TotalMember ?? 0) " + "   "
        
        if groupList.data[indexPath.row].IsMember ?? false
        {
//            group.groupAgeLbl.isHidden = false
            group.groupAgeLbl.text = " " + " \(groupList.data[indexPath.row].TotalMember ?? 0) " + "   "
        }
        else
        {
//            group.groupAgeLbl.isHidden = true
            group.groupAgeLbl.text = " +    "
        }
//        group.groupImage.layer.cornerRadius = 15
        group.groupImage.makeCornerRadius(radius: 15)
        return group
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
            let height = (collectionView.frame.height / 4)+12
            let width = (collectionView.frame.width / 3)
            return CGSize(width: width, height: height)
     }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "GroupSB", bundle: nil)
        let groupDetailsVC = storyboard.instantiateViewController(withIdentifier: "GroupDetailsVC") as! GroupDetailsVC
        groupDetailsVC.groupId = groupList.data[indexPath.row].GroupID ?? 0
        groupDetailsVC.groupPrivacy = groupList.data[indexPath.row].GroupPrivacy!
        groupDetailsVC.groupName = groupList.data[indexPath.row].GroupName!
        self.navigationController?.pushViewController(groupDetailsVC, animated: true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        self.groupList.data = []
        self.noOfGroups = 0
        self.totalGroupCount = 1
        searchText = updatedString!
        if MyGroups
        {
            self.getGroupListing(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: true,SearchString: updatedString ?? "")
//        self.searchGroups(searchString: updatedString ?? "")
        }else
        {
            self.getGroupSuggestionListing(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: true,SearchString: updatedString ?? "")
        }
        return true
    }
    
    func searchGroups(searchString:String){

        self.groupList.data = self.searchGroup.data
        
        if searchString.trimmingCharacters(in: .whitespaces) == ""{
            self.groupList.data = self.searchGroup.data
        }else{
            let data = groupList.data.filter{
                ($0.GroupName?.trimmingCharacters(in: .whitespaces).lowercased().contains(searchString))!
            }
            self.groupList.data = data
        }
        
        self.groupCollectionView.reloadData()
    }
    
}
