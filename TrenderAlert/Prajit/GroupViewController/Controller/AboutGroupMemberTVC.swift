//
//  AboutGroupMemberTVC.swift
//  TrenderAlert
//
//  Created by HPL on 18/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import SDWebImage

class AboutGroupMemberTVC: UITableViewCell {

    @IBOutlet weak var btnsendAdmin: UIButton!
    @IBOutlet weak var btnSendMember: UIButton!
    @IBOutlet weak var groupMemberCollectionView: UICollectionView!
    @IBOutlet weak var adminMemberCollectionView: UICollectionView!
    
    @IBOutlet weak var groupMemberCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var adminMemberCollectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var groupMemberCountLbl: UILabel!
    @IBOutlet weak var memberNamesOfGroupLbl: UILabel!
    @IBOutlet weak var adminNamesOfGroupLbl: UILabel!
    @IBOutlet weak var seeAllMembersBtn: UIButton!
    
    static let reuseIdentifier = "AboutGroupMemberTVC"
    
    
//    var memberImages = ["img_Jhon","img_Jhon","img_Jhon","img_Jhon","img_Jhon"]
    var adminImages:[String] = []
    var memberImages:[String] = []
    
    var totalAdminMember = 0
    var totalGroupMember = 0
    
    
    var adminNames:[String] = []{
        didSet{
            adminNamesOfGroupLbl.text =  stringForMemberNames(totalCount: totalAdminMember, memberOrAdminSuffixPlurals: "admins", memberOrAdminSuffixSingular: "admin", memberArr: adminNames)
        }
    }
    
    var memberNames:[String] = []{
        didSet{
            memberNamesOfGroupLbl.text = stringForMemberNames(totalCount: totalGroupMember, memberOrAdminSuffixPlurals: "members", memberOrAdminSuffixSingular: "member", memberArr: memberNames)
        }
    }
    
    let heightForCollectionView = 0.03
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCollectionView()
    }
    
    func setup(){
        adminMemberCollectionView.reloadData()
        groupMemberCollectionView.reloadData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func stringForMemberNames(totalCount:Int,memberOrAdminSuffixPlurals:String,memberOrAdminSuffixSingular:String,memberArr:[String])->String{
        var str = ""
        
        if totalCount > 3{
            str = "\(memberArr[0]),\(memberArr[1]) and \(memberArr[0]) \((totalCount - 3)) other  are \(memberOrAdminSuffixPlurals)."
        }else if ((totalCount > 0) && (totalCount <= 3)){
            var names = ""
            for (index,name) in memberArr.enumerated(){
                if memberArr.count > 1,((memberArr.count - 1) != index){
                    names += "\(name) ,"
                }else{
                    names += name
                }
            }
            str = "\(names) \((totalCount == 1) ? "is "+memberOrAdminSuffixSingular : " are "+memberOrAdminSuffixPlurals)."
        }else{
            str = "\(memberOrAdminSuffixSingular.capitalized) not available"
        }
        return str
    }
    
    func setupCollectionView(){
        groupMemberCollectionView.dataSource = self
        groupMemberCollectionView.delegate = self
        
        adminMemberCollectionView.dataSource = self
        adminMemberCollectionView.delegate = self
        
        groupMemberCollectionView.register(UINib(nibName: GroupDetailMemberImagesCVC.reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: GroupDetailMemberImagesCVC.reuseIdentifier)
        
        adminMemberCollectionView.register(UINib(nibName: GroupDetailMemberImagesCVC.reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: GroupDetailMemberImagesCVC.reuseIdentifier)
        
        
        groupMemberCollectionHeight.constant = UIScreen.main.bounds.height * CGFloat(heightForCollectionView)
        adminMemberCollectionViewHeight.constant = UIScreen.main.bounds.height * CGFloat(heightForCollectionView)
    }
    
}

extension AboutGroupMemberTVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.groupMemberCollectionView{
            return memberImages.count
        }else{
            return adminImages.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: GroupDetailMemberImagesCVC.reuseIdentifier, for: indexPath) as! GroupDetailMemberImagesCVC
//        cell.memberImage.frame.size = CGSize(width: cell.memberImage.frame.width, height: cell.memberImage.frame.width)
        if collectionView == self.groupMemberCollectionView{
            cell.memberImage.sd_setImage(with: URL(string: memberImages[indexPath.row]), placeholderImage: UIImage(named: "img_selectProfilePic"), options: [.continueInBackground], completed: nil)
            
        }else{
            cell.memberImage.sd_setImage(with: URL(string: adminImages[indexPath.row]), placeholderImage: UIImage(named: "img_selectProfilePic"), options: [.continueInBackground], completed: nil)
        }
        
        cell.setup()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: ((collectionView.frame.width - 40) / 10), height: collectionView.frame.height)
        return CGSize(width: collectionView.frame.height, height: collectionView.frame.height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
}
