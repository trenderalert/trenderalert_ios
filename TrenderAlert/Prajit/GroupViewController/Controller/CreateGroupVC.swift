//
//  CreateGroupVC.swift
//  TrenderAlert
//
//  Created by HPL on 14/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import AFViewShaker
import MBProgressHUD
import CropViewController

class CreateGroupVC: UIViewController {

    @IBOutlet weak var groupNameTextField: UITextField!
    @IBOutlet weak var searchNameEmailTextField: UITextField!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var peopleListTable: UITableView!
    @IBOutlet weak var cameraImgBackgroundView: UIView!
    @IBOutlet weak var groupImage: UIImageView!
    @IBOutlet weak var groupImgBtn: UIButton!
    @IBOutlet weak var groupDescriptionTxtView: UITextView!
    @IBOutlet weak var noDataLbl: UILabel!
    
    let noOfRecordPerRequest = 10
    var friendListObj = FriendList()
    var totalFriendCount = 1
    var searchText = ""
    let picker = UIImagePickerController()
    let actionSheetController:UIAlertController = UIAlertController(title:  MySingleton.shared.selectedLangData.please_select_your_option, message: "", preferredStyle: .actionSheet)
    var isChangeImage = false
    var isRequestInProgress = false
    var groupDescriptionPlaceholder = MySingleton.shared.selectedLangData.Group_Description
    var groupDescriptionMaxLength = 1000;
    var groupTitleMaxLength = 40;
    
    
    var noOfFriends = 0{
        didSet{
             DispatchQueue.main.async {
                //  self.friendListObj.data = Array(Set(self.friendListObj.data))
                //  self.friendListObj.data.removeDuplicates()
                self.peopleListTable.reloadData()
            }
        }
    }
    
    var selectedMembers = [Int]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupNavigationBar()
        groupDescriptionTxtView.text = groupDescriptionPlaceholder
        
        groupNameTextField.setBottomBorder(borderColor: UIColor(displayP3Red: 237/255, green: 237/255, blue: 237/255, alpha: 1).cgColor, borderHeight: 2, borderWidth: 0)
        groupNameTextField.leftPadding(paddingSize: 15)
        searchNameEmailTextField.leftPadding(paddingSize: 15)
        searchNameEmailTextField.placeholder = MySingleton.shared.selectedLangData.search_by_name
        setupCameraActionSheet()
        searchNameEmailTextField.delegate = self
        groupDescriptionTxtView.delegate = self
        
        groupNameTextField.delegate = self;
        
        getFriendList(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: true)
    }
    
    override func viewDidLayoutSubviews() {
        cameraImgBackgroundView.makeCornerRadius(radius: cameraImgBackgroundView.frame.height/2)
    }
    
    func setupCameraActionSheet(){
        let cameraActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.camera, style: .default) { void in
            print("Camera")
            self.openCamera()
        }
        let galleryActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.gallery, style: .default) { void in
            print("Gallery")
            self.openGallary()
        }
    actionSheetController.addAction(UIAlertAction(title: MySingleton.shared.selectedLangData.cancel, style: UIAlertAction.Style.cancel, handler: { (action) -> Void in
            
        }))
    actionSheetController.addAction(cameraActionButton)
    actionSheetController.addAction(galleryActionButton)
    }
        
    
    func setupNavigationBar(){
        
        Utilities.setNavigationBarWithTitle(viewController: self, title: MySingleton.shared.selectedLangData.create_group)
        let leftButton = UIBarButtonItem(image: UIImage(named: "img_BackArrow"), style: .plain, target: self, action: #selector(goBack))
        let buttonTitle = UIBarButtonItem(title: MySingleton.shared.selectedLangData.next, style: .plain, target: self, action: #selector(goToGroupPrivacyPage))
        navigationItem.leftBarButtonItem = leftButton
        navigationItem.rightBarButtonItem = buttonTitle
    }
    
    @objc func goBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func getFriendList(Skip: Int, Take: Int, lazyLoading: Bool,SearchString:String = ""){
        
        if (friendListObj.data.count >=  totalFriendCount) || isRequestInProgress{
            return
        }
        
        isRequestInProgress = true
        
        let param = ["Skip":Skip,"Take":Take,"Search":searchText] as [String : Any]
        
        WebServices().callUserService(service: UserServices.getFriendList, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: lazyLoading, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    let temp = WebServices().decodeDataToClass(data:serviceData, decodeClass: FriendList.self)!
//                    DispatchQueue.main.async {
                        self.friendListObj.data += temp.data
                        self.totalFriendCount = serviceResponse["TotalRecords"] as! Int
                        self.noOfFriends += 1
                        self.isRequestInProgress = false
//                    }
                    
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    
  
    func validate()->Bool{
        
        guard self.groupNameTextField.text != nil,self.groupNameTextField.text?.trimmingCharacters(in: .whitespaces) != "" else{
            self.groupNameTextField.attributedPlaceholder = NSAttributedString(string: MySingleton.shared.selectedLangData.Group_Name_is_required, attributes: [NSAttributedString.Key.foregroundColor:UIColor.red])
            let viewShaker = AFViewShaker(view: self.groupNameTextField)
            viewShaker?.shake()
            return false
        }
        
        return true
    }
    
    @objc func goToGroupPrivacyPage(){
        
        if !validate(){
            return
        }
        
//        if (groupNameTextField.text?.count)! > 20 {
//            TrenderAlertVC.shared.presentAlertController(message: "Group name should be 20 characters long.", completionHandler: nil)
//            return
//        }
        
//        if (groupDescriptionTxtView.text?.count)! > 500 {
//            TrenderAlertVC.shared.presentAlertController(message: "Group description should be 500 characters long.", completionHandler: nil)
//            return
//        }
        
        self.selectedMembers = []
        
        for friend in friendListObj.data{
            if friend.isSelected{
                self.selectedMembers.append(friend.UserProfileID)
            }
        }
        
        var param:[String:Any] = [:]
        let description = (self.groupDescriptionTxtView.text == groupDescriptionPlaceholder) ? "" : self.groupDescriptionTxtView.text
        
        if isChangeImage{
            let groupImageToUpload = ["FileExt":"jpg","contenttype":"image/jpeg","ImageData":"data:image/jpg;base64,\(Utilities.shared.encodeToBase64String(image: groupImgBtn.currentBackgroundImage ?? groupImage.image!))"]
            
            let imageArray:[AnyObject] = [groupImageToUpload as AnyObject]
            
            param = ["Id":0,"GroupName":self.groupNameTextField.text!,"Description":description!,"GroupImage":["Images":imageArray],"GroupThumbnail":["Images":imageArray],"IsClosed":"true","IsChangeImage":(isChangeImage) ? "true":"false","IsChangeThumbnail":(isChangeImage) ? "true":"false","Memberlist":selectedMembers] as [String : Any]
        }else{
            param = ["Id":0,"GroupName":self.groupNameTextField.text!,"Description":description!,"GroupImage":"","GroupThumbnail":"","IsClosed":"true","IsChangeImage":(isChangeImage) ? "true":"false","IsChangeThumbnail":(isChangeImage) ? "true":"false","Memberlist":selectedMembers] as [String : Any]
        }
        
        
        let storyboard = UIStoryboard(name: "GroupSB", bundle: nil)
        let GroupPrivacySelectionVC = storyboard.instantiateViewController(withIdentifier: "GroupPrivacySelectionVC") as! GroupPrivacySelectionVC
        GroupPrivacySelectionVC.createGroupParam = param
        self.navigationController?.pushViewController(GroupPrivacySelectionVC, animated: true)
    }
    
    func setupTableView(){
        peopleListTable.delegate = self
        peopleListTable.dataSource = self
        peopleListTable.tableFooterView = UIView()
        
        peopleListTable.register(UINib(nibName: CreateGroupTVC.reuseIdentifier, bundle: nil), forCellReuseIdentifier: CreateGroupTVC.reuseIdentifier)
    }
    
    @IBAction func selectGroupImage(_ sender: UIButton) {
        picker.delegate = self
        if let presenter = actionSheetController.popoverPresentationController{
            presenter.sourceView = sender;
            presenter.sourceRect = sender.bounds
        }
        self.present(actionSheetController,animated: true,completion:nil)
    }
    
    @IBAction func searchFriends(_ sender: UIButton) {
        if searchText.trimmingCharacters(in: .whitespaces) == ""{
            return
        }

        self.friendListObj.data = []
        self.noOfFriends = 0
        self.totalFriendCount = 1
        getFriendList(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: true,SearchString: searchText)
    }
}


extension CreateGroupVC:UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate,CropViewControllerDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if friendListObj.data.isEmpty && !isRequestInProgress{
            self.noDataLbl.isHidden = false
        }else{
            self.noDataLbl.isHidden = true
        }
        return friendListObj.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: CreateGroupTVC.reuseIdentifier, for: indexPath) as! CreateGroupTVC
        
        cell.profileImage.sd_setImage(with: URL(string: friendListObj.data[indexPath.row].ProfileImage ?? ""), placeholderImage: UIImage(named: "img_selectProfilePic"), options: [.continueInBackground], completed: nil)
        cell.nameLbl.text = "\(friendListObj.data[indexPath.row].Firstname ?? "") \(friendListObj.data[indexPath.row].Lastname ?? "")"
        cell.cityLbl.text = friendListObj.data[indexPath.row].Location ?? ""
        cell.groupMemberSelectionBtn.tag = indexPath.row
        cell.groupMemberSelectionBtn.addTarget(self, action: #selector(self.selectUnselectGroupMember(_:)), for: .touchUpInside)
//        if selectedMembers.contains(friendListObj.data[indexPath.row].UserProfileID){
//        if friendListObj.data[indexPath.row].isSelected{
          //  cell.groupMemberSelectionBtn.setBackgroundImage(UIImage(named: "checked_box"), for: .normal)
//             cell.groupMemberSelectionBtn.setTitle("Remove", for: .normal)
//        }else{
          //  cell.groupMemberSelectionBtn.setBackgroundImage(UIImage(named: "uncheck_box"), for: .normal)
            cell.groupMemberSelectionBtn.setTitle(MySingleton.shared.selectedLangData.add, for: .normal)
//        }
        
        DispatchQueue.main.async {
            cell.setup()
        }
        return cell
    }
    
    @objc func selectUnselectGroupMember(_ sender: UIButton){
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
        let cell = self.peopleListTable.cellForRow(at: indexPath) as! CreateGroupTVC
        
        
        
        if friendListObj.data[sender.tag].isSelected{
            friendListObj.data[sender.tag].isSelected = false
           // cell.groupMemberSelectionBtn.setBackgroundImage(UIImage(named: "uncheck_box"), for: .normal)
            cell.groupMemberSelectionBtn.setTitle(MySingleton.shared.selectedLangData.add, for: .normal)
        }else{
            friendListObj.data[sender.tag].isSelected = true
          //  cell.groupMemberSelectionBtn.setBackgroundImage(UIImage(named: "checked_box"), for: .normal)
            cell.groupMemberSelectionBtn.setTitle(MySingleton.shared.selectedLangData.remove, for: .normal)
//           self.selectedMembers.append(self.friendListObj.data[sender.tag].UserProfileID)
        }
    }

    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (friendListObj.data.count - 1){
           getFriendList(Skip: friendListObj.data.count, Take: noOfRecordPerRequest, lazyLoading: true)
        }
    }
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (textField == groupNameTextField){
            let maxLength = self.groupTitleMaxLength;
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        self.friendListObj.data = []
        
        self.noOfFriends = 0
        self.totalFriendCount = 1
        searchText = updatedString!
        getFriendList(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: true,SearchString: updatedString!)
        return true
    }
    
    
    
    func openGallary()
    {
        picker.allowsEditing = false
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        present(picker, animated: true, completion: nil)
    }
    
    func openCamera()
    {
    if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            present(picker,animated: true,completion: nil)
        }else{
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        self.isChangeImage = false
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let chosenImage = info[.originalImage] as! UIImage
//        self.groupImgBtn.setImage(chosenImage, for: .normal)
        
        dismiss(animated: true)
        let cropViewController = CropViewController(image: chosenImage)
        cropViewController.delegate = self
        present(cropViewController, animated: true, completion: nil)
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.isChangeImage = true
        // 'image' is the newly cropped version of the original image
        self.groupImgBtn.setBackgroundImage(image, for: .normal)
        dismiss(animated:true, completion: nil)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (textView.text == groupDescriptionPlaceholder){
            textView.text = ""
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let maxLength = self.groupDescriptionMaxLength;
        let currentString: NSString = textView.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: text) as NSString
        groupDescriptionTxtView.textColor = UIColor.black
        return newString.length <= maxLength
    }
    
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == ""{
            groupDescriptionTxtView.text = groupDescriptionPlaceholder
            groupDescriptionTxtView.textColor = UIColor.lightGray
        }
            
    }
    
}

//extension Array where Element: Equatable {
//    mutating func removeDuplicates() {
//        var result = [Element]()
//        for value in self {
//            if !result.contains(value) {
//                result.append(value)
//            }
//        }
//        self = result
//    }
//}
