//
//  GroupPrivacySelectionVC.swift
//  TrenderAlert
//
//  Created by HPL on 25/02/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import MBProgressHUD

class GroupPrivacySelectionVC: UIViewController {

    @IBOutlet weak var groupPrivacyTable: UITableView!
    // 1 = public, 2 = closed, 3 = secret
//    let privacyListOption = [["Public","Anyone can the post & What the post",1],["Closed","Anyone can search the group. Only members can seee who's in it and what they post.",2],["Secret","Only members can search for group,see members & there post.",3]]
    
    var isPrivacySelected   = false
    var selectedPrivacyOption = -1
    var createGroupParam:[String:Any] = [:]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableview()
        setupNavigationBar()
    }
    
    func setupTableview(){
        groupPrivacyTable.register(UINib(nibName: GroupPrivacyTVC.reuseIdentifier, bundle: nil), forCellReuseIdentifier: GroupPrivacyTVC.reuseIdentifier)
        groupPrivacyTable.dataSource = self
        groupPrivacyTable.delegate = self
    }
    
    func setupNavigationBar(){
        Utilities.setNavigationBarWithTitle(viewController: self, title: "Select Privacy")
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "img_BackArrow"), style: .plain, target: self, action: #selector(goBack))
        let buttonTitle = UIBarButtonItem(title: "Create", style: .plain, target: self, action: #selector(createGroup))
        navigationItem.leftBarButtonItem = leftButton
        navigationItem.rightBarButtonItem = buttonTitle
    }
    
    @objc func goBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func validate()->Bool{

        guard selectedPrivacyOption != -1 else{
            TrenderAlertVC.shared.presentAlertController(message: MySingleton.shared.selectedLangData.Please_select_privacy_option, completionHandler: nil)
            return false
        }
        return true
    }
 
    @objc func createGroup(){
        
        if !validate(){
            return
        }
        createGroupParam["GroupPrivacy"] = selectedPrivacyOption
        self.callApi(requestData: createGroupParam, url: "\(WebServices.baseURL)\(UserServices.createGroup.rawValue)")
    }
    
    func callApi(requestData: Dictionary<String, Any>,url:String)
    {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        var urlStr:String = url
        
        urlStr = urlStr.replacingOccurrences(of: " ", with: "&")
        let request :NSMutableURLRequest = NSMutableURLRequest(url:NSURL(string:urlStr)! as URL)
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        var TokenValue = String()
        TokenValue = "Bearer " + MySingleton.shared.loginObject.access_token
        print(TokenValue)
        request.addValue(TokenValue, forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        if requestData.count != 0
        {
            do
            {
                let jsonData = try JSONSerialization.data(withJSONObject: requestData, options: JSONSerialization.WritingOptions.prettyPrinted)
                request.httpBody = jsonData
            }
            catch let error as NSError
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                }
            }
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest){ data,response,error in
            
            if error != nil
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error!.localizedDescription, completionHandler: nil)
                }
                
            }
            do
            {
                if (response as? HTTPURLResponse) != nil
                {
                    
                    //////////////////// JSON RESPONSE /////////////////
                    
                    
                    let jsonString = String(data: data!, encoding: .utf8)
                    print(jsonString!)
                    
                    //////////////////// JSON RESPONSE /////////////////
                    
                    if data != nil
                    {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        
                        print(jsonData)
                        
                        if(jsonData["Status"] as! Bool == true)
                        {
                            
                            DispatchQueue.main.async {
                                MBProgressHUD.hide(for: self.view, animated: true)
                                for vc in (self.navigationController?.viewControllers ?? []){
                                    if vc is GroupVC{
                                        self.navigationController?.popToViewController(vc, animated: true)
                                    }
                                }
                            }
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                MBProgressHUD.hide(for: self.view, animated: true)
                                TrenderAlertVC.shared.presentAlertController(message: jsonData["Message"] as! String, completionHandler: nil)
                            }
                        }
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true)
                            TrenderAlertVC.shared.presentAlertController(message: error!.localizedDescription, completionHandler: nil)
                        }
                    }
                    
                }
            }
            catch let error as NSError
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                }
            }
        }
        
        task.resume()
    }
}

extension GroupPrivacySelectionVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return privacyListOption.count
        return Utilities.groupPrivacyDetails.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: GroupPrivacyTVC.reuseIdentifier, for: indexPath) as! GroupPrivacyTVC
  
        cell.privacyStatusLbl.text = Utilities.groupPrivacyDetails[indexPath.row][0] as? String
        cell.privacyDescription.text = Utilities.groupPrivacyDetails[indexPath.row][1] as? String
        cell.privacyRadioBtn.tag = Utilities.groupPrivacyDetails[indexPath.row][2] as! Int
        
        if selectedPrivacyOption == Utilities.groupPrivacyDetails[indexPath.row][2]  as! Int{
          cell.privacyRadioBtn.setBackgroundImage(UIImage(named: "radio_On"), for: .normal)
        }else{
            cell.privacyRadioBtn.setBackgroundImage(UIImage(named: "radio_Off"), for: .normal)
        }
        cell.privacyRadioBtn.addTarget(self, action: #selector(selectPrivacyOption(_:)), for: .touchUpInside)
        return cell
    }
    
    @objc func selectPrivacyOption(_ sender:UIButton){
        isPrivacySelected = true
        selectedPrivacyOption = sender.tag
        sender.setBackgroundImage(UIImage(named: "radio_On"), for: .normal)
        groupPrivacyTable.reloadData()
    }
}
