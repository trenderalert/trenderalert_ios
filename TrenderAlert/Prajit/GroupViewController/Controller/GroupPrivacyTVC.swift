//
//  GroupPrivacyTVC.swift
//  TrenderAlert
//
//  Created by HPL on 26/02/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class GroupPrivacyTVC: UITableViewCell {

    static let reuseIdentifier = "GroupPrivacyTVC"
    
    @IBOutlet weak var privacyStatusLbl: UILabel!
    
    @IBOutlet weak var privacyDescription: UILabel!
    
    @IBOutlet weak var privacyRadioBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
