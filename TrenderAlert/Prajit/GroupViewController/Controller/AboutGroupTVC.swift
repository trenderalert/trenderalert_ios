//
//  AboutGroupTVC.swift
//  TrenderAlert
//
//  Created by HPL on 18/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class AboutGroupTVC: UITableViewCell {

    @IBOutlet weak var lblHistory: UILabel!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var groupDescription: UILabel!
    @IBOutlet weak var groupPrivacyDescription: UILabel!
    @IBOutlet weak var groupCreationTime: UILabel!
    @IBOutlet weak var groupTypeLbl: UILabel!
    
    static let reuseIdentifier = "AboutGroupTVC"
    
    override func awakeFromNib() {
        super.awakeFromNib()
       self.lblHistory.text = MySingleton.shared.selectedLangData.history
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
