//
//  AddGroupMembersVC.swift
//  TrenderAlert
//
//  Created by HPL on 06/03/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import SDWebImage

class AddGroupMembersVC: UIViewController {

    //Outlets
    @IBOutlet weak var friendListTbl: UITableView!
    @IBOutlet weak var searchFriendsUTF: UITextField!
    @IBOutlet weak var noDataLbl: UILabel!
    var groupName = String()
    var privacy = Int()
    //Properties
    override func viewDidLoad() {
        super.viewDidLoad()
        searchFriendsUTF.leftPadding(paddingSize: 10)
        searchFriendsUTF.delegate = self
        setupNavigationBar()
        setupTableView()
        getFriendList(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: true)
    }
    
    var groupId = 0
    
    var friendListObj = FriendList()
    
    let noOfRecordPerRequest = 10
    var totalFriendCount = 1
    var isFriendApiRequestInProgress = false
    var noOfFriends = 0{
        didSet{
            DispatchQueue.main.async {
                self.friendListTbl.reloadData()
            }
        }
    }
    
    var searchText = ""
    
    var selectedMembers = [Int]()
    
    func setupNavigationBar(){
        Utilities.setNavigationBarWithTitle(viewController: self, title: MySingleton.shared.selectedLangData.add_group_members)
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "img_BackArrow"), style: .plain, target: self, action: #selector(goBack))
//        let rightButton = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addGroupMembers))
       
        navigationItem.leftBarButtonItem = leftButton
//        navigationItem.rightBarButtonItem = rightButton
    }
    
    @objc func goBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func addGroupMembers(){
        
        self.selectedMembers = []
        for friend in friendListObj.data{
            if friend.isSelected{
                self.selectedMembers.append(friend.UserProfileID)
            }
        }
        
        if selectedMembers.count == 0{
            TrenderAlertVC.shared.presentAlertController(message: MySingleton.shared.selectedLangData.please_select_member , completionHandler: nil)
            return
        }
        
        let param = ["GroupID":self.groupId,"GroupMembers":self.selectedMembers] as [String : Any]
        WebServices().callUserService(service: UserServices.requestToJoinGroup, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: true, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    GroupDetailsVC.reloadAboutDiscussionTbl = true
                    self.navigationController?.popViewController(animated: true)
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
        
//        let param = ["GroupID":self.groupId,"GroupMembers":self.selectedMembers] as [String : Any]
//        WebServices().callUserService(service: UserServices.addGroupMembers, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: true, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
//            if serviceResponse["Status"] != nil{
//                if (serviceResponse["Status"] as! Bool == true){
//                    GroupDetailsVC.reloadAboutDiscussionTbl = true
//                    self.navigationController?.popViewController(animated: true)
//                }else{
//                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
//                }
//            }else{
//                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
//            }
//        }
        
    }
    
    
    func setupTableView(){
        friendListTbl.dataSource = self
        friendListTbl.delegate = self
        friendListTbl.register(UINib(nibName: AddGroupMemberFooter.reuseIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: AddGroupMemberFooter.reuseIdentifier)
        friendListTbl.register(UINib(nibName: CreateGroupTVC.reuseIdentifier, bundle: nil), forCellReuseIdentifier: CreateGroupTVC.reuseIdentifier)
        friendListTbl.tableFooterView = UIView()
        self.searchFriendsUTF.placeholder = MySingleton.shared.selectedLangData.search_by_name
    }
    
    
    @IBAction func searchFriends(_ sender: UIButton) {
        if searchText.trimmingCharacters(in: .whitespaces) == ""{
            return
        }
        
        self.friendListObj.data = []
        self.noOfFriends = 0
        self.totalFriendCount = 1
         getFriendList(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: true,SearchString: searchText)
    }
    
    func getFriendList(Skip: Int, Take: Int, lazyLoading: Bool,SearchString:String = ""){
        
        if (friendListObj.data.count >=  totalFriendCount) || isFriendApiRequestInProgress{
            return
        }
        
        self.isFriendApiRequestInProgress = true
        let param = ["GroupID":self.groupId,"Search":searchText,"Skip":Skip,"Take":Take] as [String : Any]
        
        WebServices().callUserService(service: UserServices.getFriendListForGroup, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: true, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    let temp = WebServices().decodeDataToClass(data:serviceData, decodeClass: FriendList.self)!
                    self.friendListObj.data += temp.data
                    self.totalFriendCount = serviceResponse["TotalRecords"] as! Int
                    self.noOfFriends += 1
                    self.isFriendApiRequestInProgress = false
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
}

extension AddGroupMembersVC:UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if friendListObj.data.isEmpty && !isFriendApiRequestInProgress{
            noDataLbl.isHidden = false
        }else{
            noDataLbl.isHidden = true
        }
        return self.friendListObj.data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CreateGroupTVC.reuseIdentifier, for: indexPath) as! CreateGroupTVC
        cell.profileImage.sd_setImage(with: URL(string: friendListObj.data[indexPath.row].ProfileImage ?? ""), placeholderImage: UIImage(named: "img_selectProfilePic"), options: [.continueInBackground], completed: nil)
        cell.nameLbl.text = "\(friendListObj.data[indexPath.row].Firstname ?? "") \(friendListObj.data[indexPath.row].Lastname ?? "")"
        cell.cityLbl.text = friendListObj.data[indexPath.row].Location ?? ""
        cell.groupMemberSelectionBtn.tag = indexPath.row
        cell.groupMemberSelectionBtn.addTarget(self, action: #selector(selectUnselectGroupMember(_:)), for: .touchUpInside)
        if friendListObj.data[indexPath.row].RequestStatus == 0{
        //    cell.groupMemberSelectionBtn.setBackgroundImage(UIImage(named: "checked_box"), for: .normal)
             cell.groupMemberSelectionBtn.setTitle(MySingleton.shared.selectedLangData.send_request, for: .normal)
            cell.groupMemberSelectionBtn.isEnabled = true;
        }else if friendListObj.data[indexPath.row].RequestStatus == 1{
          //  cell.groupMemberSelectionBtn.setBackgroundImage(UIImage(named: "uncheck_box"), for: .normal)
             cell.groupMemberSelectionBtn.setTitle(MySingleton.shared.selectedLangData.pending, for: .normal)
            cell.groupMemberSelectionBtn.isEnabled = false;
        }else if friendListObj.data[indexPath.row].RequestStatus == 2{
            cell.groupMemberSelectionBtn.setTitle(MySingleton.shared.selectedLangData.Joined, for: .normal)
            cell.groupMemberSelectionBtn.isEnabled = false;
        }
        else
        {
             cell.groupMemberSelectionBtn.setTitle(MySingleton.shared.selectedLangData.rejected, for: .normal)
            cell.groupMemberSelectionBtn.isEnabled = false;
        }
        
        cell.setup()
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (friendListObj.data.count - 1){
            getFriendList(Skip: friendListObj.data.count, Take: noOfRecordPerRequest, lazyLoading: true)
        }
    }
    
//    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
//        let footer = tableView.dequeueReusableHeaderFooterView(withIdentifier: AddGroupMemberFooter.reuseIdentifier) as! AddGroupMemberFooter
//        footer.footerBtn.addTarget(self, action: #selector(addGroupMembers), for: .touchUpInside)
//        return footer
//    }
    
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return UIScreen.main.bounds.height * 0.1
//    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        self.friendListObj.data = []
        self.noOfFriends = 0
        self.totalFriendCount = 1
        searchText = updatedString!
        getFriendList(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: true,SearchString: updatedString!)
        return true
    }
    
    
    @objc func selectUnselectGroupMember(_ sender: UIButton){
        
        let indexPath = IndexPath(row: sender.tag, section: 0)
//        let cell = self.friendListTbl.cellForRow(at: indexPath) as! CreateGroupTVC
        
    
        
//        if friendListObj.data[sender.tag].isSelected{
//            friendListObj.data[sender.tag].isSelected = false
//           // cell.groupMemberSelectionBtn.setBackgroundImage(UIImage(named: "uncheck_box"), for: .normal)
//             cell.groupMemberSelectionBtn.setTitle("Send Request", for: .normal)
//        }else{
        
          //  cell.groupMemberSelectionBtn.setBackgroundImage(UIImage(named: "checked_box"), for: .normal)
            
            
//            let param = ["Id": friendListObj.data[indexPath.row].UserProfileID,
//                         "GroupName": self.groupName,
//                         "GroupPrivacy": self.privacy,"Memberlist":[friendListObj.data[indexPath.row].UserProfileID]] as [String : Any]
            
            
            let param = ["GroupId": self.groupId,"GroupMembers":[friendListObj.data[indexPath.row].UserProfileID]] as [String : Any]
            
            
            
            WebServices().callUserService(service: UserServices.addGroupMembers, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: true, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
                if serviceResponse["Status"] != nil{
                    if (serviceResponse["Status"] as! Bool == true){
                        DispatchQueue.main.async {
                        GroupDetailsVC.reloadAboutDiscussionTbl = true
                       self.friendListObj.data[sender.tag].RequestStatus = 1;
                        
                            self.friendListTbl.reloadData();
                        }
                        
//                        cell.groupMemberSelectionBtn.setTitle("Requested", for: .normal)
//                        cell.groupMemberSelectionBtn.isEnabled = false
                    }else{
                        TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }
//        }
       
    }
    
}
