//
//  GroupAdminAndMemberListModel.swift
//  TrenderAlert
//
//  Created by HPL on 04/03/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

class GroupAdminAndMemberListModel:NSObject,Codable{
    
    var data = [GroupMember]()
    
    enum CodingKeys:String,CodingKey{
        case data = "data"
    }
}
