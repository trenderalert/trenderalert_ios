//
//  GroupListModel.swift
//  TrenderAlert
//
//  Created by HPL on 21/02/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

class GroupListModel:NSObject,Codable{
    var data = [GroupListModelData]()
    
    enum CodingKeys:String,CodingKey{
        case data = "data"
    }
}

class GroupListModelData:NSObject,Codable{
    
    var GroupName:String?
    var GroupDescription:String?
    var GroupImage:String?
    var GroupThumbnail:String?
    var GroupMemberCount = 0
    var CreatedOn:String?
    var CreatedBy:String?
    var GroupMembers:String?
    var GroupPrivacy:Int?
    var GroupID:Int?
    var TotalMember:Int?
    var IsMember:Bool?
    
    enum CodingKeys:String,CodingKey{
        case GroupName = "GroupName"
        case GroupDescription = "GroupDescription"
        case GroupImage = "GroupImage"
        case GroupThumbnail = "GroupThumbnail"
        case GroupMemberCount = "GroupMemberCount"
        case CreatedOn = "CreatedOn"
        case CreatedBy = "CreatedBy"
        case GroupMembers = "GroupMembers"
        case GroupPrivacy = "GroupPrivacy"
        case GroupID = "GroupID"
        case TotalMember = "TotalMember"
        case IsMember = "IsMember"
    }
}



