//
//  GroupDetailsModel.swift
//  TrenderAlert
//
//  Created by HPL on 26/02/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

class GroupDetailsModel:NSObject,Codable{
    var data = [GroupDetails]()
    
    enum CodingKeys:String,CodingKey{
        case data = "data"
    }
}


class GroupDetails:NSObject,Codable{
    
    var GroupName:String?
    var GroupDescription:String?
    var GroupImage:String?
    var GroupThumbnail:String?
    var GroupMemberCount:Int?
    var CreatedOn:String?
    var CreatedBy:String?
    var GroupMembers = [GroupMember]()
    var GroupAdmins = [GroupMember]()
    var Location:String?
    var GroupPrivacy:Int?
    var GroupID:Int?
    var TotalMember:Int?
    var GroupAdminCount:Int?
    var IsUserAdmin:Bool = false
    var isMemberUpdate = Int()
    var JoinRequestStatus:Int?
    var IsRequestedByAdmin:Bool = false
    var IsUserAGroupMember:Bool = false
    
    enum CodingKeys:String,CodingKey{
        case GroupName = "GroupName"
        case GroupDescription = "GroupDescription"
        case GroupImage = "GroupImage"
        case GroupThumbnail = "GroupThumbnail"
        case GroupMemberCount = "GroupMemberCount"
        case CreatedOn = "CreatedOn"
        case CreatedBy = "CreatedBy"
        case GroupMembers = "GroupMembers"
        case Location = "Location"
        case GroupPrivacy = "GroupPrivacy"
        case GroupID = "GroupID"
        case GroupAdmins = "GroupAdmin"
        case TotalMember = "TotalMember"
        case GroupAdminCount = "GroupAdminCount"
        case IsUserAdmin = "IsUserAdmin"
        case isMemberUpdate
        case JoinRequestStatus = "JoinRequestStatus"
        case IsRequestedByAdmin = "IsRequestedByAdmin"
        case IsUserAGroupMember = "IsUserAGroupMember"
    }
}


class GroupMember:NSObject,Codable{
    
    var UserProfileId:Int?
    var Firstname:String?
    var Lastname:String?
    var ProfileImage:String?
    var IsAdmin:Bool = false
    var IsMuted:Bool = false
    
     enum CodingKeys:String,CodingKey{
        case UserProfileId = "UserProfileId"
        case Firstname = "Firstname"
        case Lastname = "Lastname"
        case ProfileImage = "ProfileImage"
        case IsAdmin = "IsAdmin"
        case IsMuted = "IsMuted"
    }
}
