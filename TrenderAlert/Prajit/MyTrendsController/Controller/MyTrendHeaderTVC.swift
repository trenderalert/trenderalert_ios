//
//  MyTrendHeaderTVC.swift
//  TrenderAlert
//
//  Created by HPL on 15/04/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class MyTrendHeaderTVC: UITableViewCell {

    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var changeImgBtn: UIButton!
    @IBOutlet weak var profileNameLbl: UILabel!
    @IBOutlet weak var editProfileBtn: UIButton!
    @IBOutlet weak var allBtn: UIButton!
    @IBOutlet weak var createFolderBtn: UIButton!
    @IBOutlet weak var folderNameLbl: UILabel!
    @IBOutlet weak var sortBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.editProfileBtn.setTitle(MySingleton.shared.selectedLangData.edit_profile, for: .normal)
        self.createFolderBtn.setTitle(MySingleton.shared.selectedLangData.create_folder, for: .normal)
        // Initialization code
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func cornerRadius(){
        self.editProfileBtn.makeCornerRadius(radius: 5)
        self.createFolderBtn.makeCornerRadius(radius: 5)
        self.allBtn.makeCornerRadius(radius: 5)
        self.sortBtn.makeCornerRadius(radius:5)
    }

}
