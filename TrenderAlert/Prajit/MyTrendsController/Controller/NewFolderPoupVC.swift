//
//  NewFolderPoupVC.swift
//  TrenderAlert
//
//  Created by HPL on 09/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import AFViewShaker
import MBProgressHUD

protocol CreateFolderDelegate {
    func controller()
}

class NewFolderPoupVC: UIViewController {

    var delegate : CreateFolderDelegate?
    @IBOutlet weak var folderNameUTF: UITextField!
    @IBOutlet weak var newFolderPopupView: UIView!
    @IBOutlet weak var lblCreateFolder: UILabel!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        newFolderPopupView.makeCornerRadius(radius: 10)
        folderNameUTF.leftPadding(paddingSize: 15)
        folderNameUTF.setBottomBorder()
        
        folderNameUTF.delegate = self
        folderNameUTF.returnKeyType = .go
        
        self.lblCreateFolder.text = MySingleton.shared.selectedLangData.create_folder
        self.btnSave.setTitle(MySingleton.shared.selectedLangData.save, for: .normal)
        self.btnCancel.setTitle(MySingleton.shared.selectedLangData.cancel, for: .normal)
        self.folderNameUTF.placeholder = MySingleton.shared.selectedLangData.folder_name
    }
    
    @IBAction func cancelCreateFolder(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func createFolderOnServer(_ sender: UIButton) {
        createFolderOnServer()
    }
    
    func createFolderOnServer(){
        
        if !(folderNameUTF.text ?? "").isEmpty{
            let parameters:[String:Any] = ["FolderID":0,"FolderName":folderNameUTF.text!,"IsRename":"false"]
            print("Params : ",parameters as [String:AnyObject])
            
            MBProgressHUD.showAdded(to: self.view, animated: true)
            WebServices().callUserService(service: UserServices.createFolder, urlParameter: "", parameters: parameters as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
                
                if(serviceResponse["Status"] != nil){
                    if(serviceResponse["Status"] as! Bool == true){
                        DispatchQueue.main.async {

                        self.folderNameUTF.resignFirstResponder()
                        MyTrendsVC.options.append(self.folderNameUTF.text!)
                        self.hideApiResponse(responseMessage: serviceResponse["Message"] as! String, isError: false){
                            self.dismiss(animated: true, completion: {
                                self.delegate?.controller()
                            })
                        }
                    }
//                        self.dismiss(animated: true, completion: nil)
                    }else{
                        self.hideApiResponse(responseMessage: serviceResponse["Message"] as! String, isError: true, handler: nil)
                    }
                }else{
                    self.hideApiResponse(responseMessage: serviceResponse["Message"] as! String, isError: true, handler: nil)
                }
            }
        }else{
            validatefolderNameUTF(placholderMsg: "Folder name cannot be empty.")
        }
    }
    
    
    func validatefolderNameUTF(placholderMsg:String){
        folderNameUTF.text = ""
        folderNameUTF.attributedPlaceholder = NSAttributedString(string: placholderMsg, attributes: [NSAttributedString.Key.foregroundColor:UIColor.red])
        let viewShaker = AFViewShaker(view: folderNameUTF)
        viewShaker?.shake()
    }
    
    func hideApiResponse(responseMessage:String,isError:Bool,handler:(()->Void)?){
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if isError{
                self.validatefolderNameUTF(placholderMsg: responseMessage)
            }else{
                if let handler = handler{
                    handler()
                }
            }
            
//            let alertController = UIAlertController(title: "Message", message: responseMessage, preferredStyle: UIAlertController.Style.alert)
//            alertController.addAction(UIAlertAction(title: "Close", style: UIAlertAction.Style.cancel, handler: handler))
            
//            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
//            alertWindow.rootViewController = UIViewController()
//            alertWindow.windowLevel = UIWindow.Level.alert + 1;
//            alertWindow.makeKeyAndVisible()
//            alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
//
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension NewFolderPoupVC:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        createFolderOnServer()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxLength = 50
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}
