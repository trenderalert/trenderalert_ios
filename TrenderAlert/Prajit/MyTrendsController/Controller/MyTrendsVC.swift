//
//  MyTrendsVC.swift
//  TrenderAlert
//
//  Created by HPL on 09/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import MBProgressHUD
import SDWebImage
import DropDown
import AVFoundation
import AVKit
import CropViewController
import GoogleMobileAds

class MyTrendsVC: UIViewController,UITextFieldDelegate {
    
    
    // MARK: - Outlets
    
    @IBOutlet weak var trendTableView: UITableView!
    @IBOutlet weak var trendCollectionView: UICollectionView!
    @IBOutlet weak var changeProfilePicBtn: UIButton!
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var allFolderBtn: UIButton!
    @IBOutlet weak var profileNameLbl: UILabel!
    @IBOutlet weak var folderNameLbl: UILabel!
    @IBOutlet weak var noDataLbl: UILabel!
    
    @IBOutlet weak var gridView: UIView!
    
    // MARK: - Variables
    var isScroll = false
    // MARK: - Variables
    var trendImagesCollectionView: UICollectionView!
    var testImageArray = [UIImage]()
    var trendData = TrendListData()
    var trendImagesArray = [UIImage]()
    var trendImagesPageControl = UIPageControl()
    var trendCurrentImageLabel = UILabel()
    var trendImagesCollectionArray = [[UIImage]]()
    var refreshControl = UIRefreshControl()
    var pageIndex = 0
    var showTrendsByFolder = ""{
        didSet{
            reloadTrendData()
            //            trendTableView.reloadData()
        }
    }
    
    let picker = UIImagePickerController()
    let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: MySingleton.shared.selectedLangData.please_select_your_option, message: "", preferredStyle: .actionSheet)
    var userInfoObj = UserInfo()
    static var options:[String] = ["All"]
    var saveFolderTrendId = Int()
    var shareTrendId = Int()
    var selectedShareFriends = [AnyObject]()
    var noOfRecordsPerRequest = 10
    var totalTrends = 1
    var isGetAllTrendRequestInProgress = false
    var showBottomLoader = false
    var isListView = true
    var loadGridView = true
    var direction = String()
    var titleSort = String()
    var currentCustomAdCnt = 0
    var currentGoogleAdCnt = 0
    var googleAdHeight = (UIDevice.current.userInterfaceIdiom == .pad) ? 90.0 : 50.0
    
    var trendHeaderViewHeight = (UIDevice.current.userInterfaceIdiom == .phone) ? 50 : 70
    
    var trendStatusViewHeight = (UIDevice.current.userInterfaceIdiom == .phone) ? 45 : 65
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MyTrendsVC.options = ["All"]
        setupNavigationbar()
        setupCameraActionSheet()
        initialConfig()
        registerHeader()
        getProfileInfo()
        getAllTrends(SortColumn: "",SortDirection: "",Skip: 0, Take: noOfRecordsPerRequest, lazyLoading: false)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        SDImageCache.shared().clearDisk();
        SDImageCache.shared().clearMemory();
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        /////
        //self.profileImg.makeCornerRadius(radius: self.profileImg.frame.height/2)
        /////
    }
    
    func registerHeader(){
        let headerNib = UINib.init(nibName: "MyTrendHeader", bundle: Bundle.main)
        trendTableView.register(headerNib, forHeaderFooterViewReuseIdentifier: "MyTrendHeader")
    }
    
    func setupCameraActionSheet(){
        
        let cameraActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.camera, style: .default) { void in
            print("Camera")
            self.openCamera()
        }
        let galleryActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.gallery, style: .default) { void in
            print("Gallery")
            self.openGallary()
        }
        actionSheetControllerIOS8.addAction(UIAlertAction(title: MySingleton.shared.selectedLangData.cancel, style: UIAlertAction.Style.cancel, handler: { (action) -> Void in
            
        }))
        actionSheetControllerIOS8.addAction(cameraActionButton)
        actionSheetControllerIOS8.addAction(galleryActionButton)
    }
    
    @objc func goToEditProfile(_ sender: UIButton) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "ProfileSB", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        nextViewController.profileUpdateDelegate = self
        nextViewController.userInfoObj = self.userInfoObj
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @objc func changeProfilePic(_ sender: UIButton) {
        picker.delegate = self
        if let presenter = actionSheetControllerIOS8.popoverPresentationController {
            presenter.sourceView = sender;
            presenter.sourceRect = sender.bounds;
            
        }
        self.present(actionSheetControllerIOS8,animated: true,completion: nil)
    }
    
    func getProfileInfo(){
        
        let parameters = ["UserID":MySingleton.shared.loginObject.UserID]
        //        let parameters = ["UserID":130044]
        
        
        WebServices().callUserService(service: UserServices.getUserInfo, urlParameter: "", parameters: parameters as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){ (serviceResponse, serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    self.userInfoObj = WebServices().decodeDataToClass(data: serviceData, decodeClass: UserInfo.self)!
                    /////////
                    //                    self.profileNameLbl.text = "\(self.userInfoObj.data[0].FirstName ?? "") \(self.userInfoObj.data[0].LastName ?? "")"
                    
                    //                    self.profileImg.sd_setImage(with: URL(string: self.userInfoObj.data[0].UserProfileImage ?? ""), placeholderImage: UIImage(named: "Img_Blur"), options: [.continueInBackground], completed: nil)
                    ////////
                    //Setup Folder lists
                    for option in self.userInfoObj.data[0].FoldersList{
                        MyTrendsVC.options.append(option.FolderName ?? "")
                    }
                    DispatchQueue.main.async {
                        self.trendTableView.reloadData()
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    @objc func showAllFolders(_ sender: UIButton) {
        
        let dropDown = DropDown()
        dropDown.anchorView = sender
        dropDown.dataSource = MyTrendsVC.options
        dropDown.backgroundColor = UIColor.white
        
        dropDown.bottomOffset = CGPoint(x: 0, y: ((dropDown.anchorView?.plainView.bounds.height)!))
        dropDown.selectionAction = {[unowned self] (index:Int,item:String) in
            //            let indexPath = IndexPath(row: 0, section: 0)
            //            let cell = self.trendTableView.cellForRow(at: indexPath) as! MyTrendHeaderTVC
            //            cell.folderNameLbl.text = item
            //            self.showTrendsByFolder = item
            
            let headerView = self.trendTableView.headerView(forSection: 0)
            if let headerView = headerView as? MyTrendHeader{
                headerView.folderNameLbl.text = item
            }
            
            if item == "All"{
                self.showTrendsByFolder = ""
            }else{
                self.showTrendsByFolder = item
            }
        }
        dropDown.show()
    }
    
    func setupNavigationbar(){
        Utilities.setNavigationBarWithTitle(viewController: self, title: MySingleton.shared.selectedLangData.my_trends)
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "img_BackArrow"), style: .plain, target: self, action: #selector(goBack))
        let rightButton = UIBarButtonItem(image: UIImage(named: "menu_clk"), style: .plain, target: self, action: #selector(toggleListGridView))
        navigationItem.leftBarButtonItem = leftButton
        navigationItem.rightBarButtonItem = rightButton
    }
    
    @objc func goBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func toggleListGridView(){
        
        if isListView{ // For Grid View
            
            self.loadGridView = false
            self.gridView.isHidden = false
            self.isListView = false
            let rightButton = UIBarButtonItem(image: UIImage(named: "list_clk"), style: .plain, target: self, action: #selector(toggleListGridView))
            navigationItem.rightBarButtonItem = rightButton
            
            let storyboard = UIStoryboard(name: "SearchSB", bundle: nil)
            let dest = storyboard.instantiateViewController(withIdentifier: "GridViewVC") as! GridViewVC
            dest.hideSearchbar = true
            dest.isMyTrend = true
            dest.showTrendsByFolder = self.showTrendsByFolder
            //add as a childviewcontroller
            addChild(dest)
            
            // Add the child's View as a subview
            self.gridView.addSubview(dest.view)
            dest.view.frame = gridView.bounds
            dest.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            // tell the childviewcontroller it's contained in it's parent
            dest.didMove(toParent: self)
            
            
        }else{ // For list View
            self.gridView.isHidden = true
            self.isListView = true
            let rightButton = UIBarButtonItem(image: UIImage(named: "menu_clk"), style: .plain, target: self, action: #selector(toggleListGridView))
            navigationItem.rightBarButtonItem = rightButton
        }
    }
    
    @objc func showCreateFolderPopup(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
        let nextViewController = storyboard.instantiateViewController(withIdentifier: "NewFolderPoupVC") as! NewFolderPoupVC
        nextViewController.modalPresentationStyle = .overCurrentContext
        //        self.navigationController?.pushViewController(nextViewController, animated: true)
        self.navigationController?.present(nextViewController, animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        pausePlayeVideos()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        pausePlayeVideos()
    }
    
    //    func setupNavigationbar(){
    //        Utilities.setNavigationBarWithTitle(viewController: self, title: "Favourite Trends")
    //        let leftButton = UIBarButtonItem(image: UIImage(named: "img_BackArrow"), style: .plain, target: self, action: #selector(goBack))
    //        navigationItem.leftBarButtonItem = leftButton
    //    }
    func initialConfig() {
        //        self.menuContainerViewController.menuState = MFSideMenuStateClosed
        //        Utilities.setNavigationBar(viewController: self)
        //        self.navigationController?.navigationBar.barTintColor = .black
        //        self.navigationController?.navigationBar.tintColor = .white
        // Register Table View Cells
        registerTableViewCells()
        
        // Test
        //        callAPI()
        testImageArray = [#imageLiteral(resourceName: "img_guitar"), #imageLiteral(resourceName: "img_DefaultDetail"), #imageLiteral(resourceName: "smoke_DashBoard"), #imageLiteral(resourceName: "img_mapCollection3"), #imageLiteral(resourceName: "img_mapCollection3"), #imageLiteral(resourceName: "img_mapCollection3")]
        if self.menuContainerViewController != nil {
            //            self.menuContainerViewController.panMode = MFSideMenuPanModeDefault
        }
        else {
            //            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            //            let container = MFSideMenuContainerViewController()
            //            let storyBoard = UIStoryboard(name: "Dashboard", bundle: nil)
            //            let centerViewController = storyBoard.instantiateViewController(withIdentifier: "DashboardNavigationController") as! UINavigationController
            //            container.centerViewController = centerViewController
            //            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            //            let leftViewController = mainStoryBoard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
            //            container.leftMenuViewController = leftViewController
        }
        // getAllTrends()
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refreshAllTrends), for: UIControl.Event.valueChanged)
        trendTableView.addSubview(refreshControl)
        
        trendTableView.tableFooterView = UIView()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !self.showBottomLoader{
            self.trendTableView.tableFooterView = UIView()
            self.trendTableView.tableFooterView?.isHidden = true
        }
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: NSNotification.Name(rawValue: "getAllTrends"), object: nil)
    }
    
    @objc func onDidReceiveData(_ notification: Notification){
        getAllTrends(SortColumn: "",SortDirection: "",Skip: 0, Take: noOfRecordsPerRequest, lazyLoading: false)
    }
    
    @objc func refreshAllTrends(sender: UIRefreshControl) {
        reloadTrendData()
        self.refreshControl.endRefreshing()
    }
    
    func reloadTrendData(){
        self.trendData.data = []
        self.trendImagesArray = []
        self.totalTrends = 1
        self.currentCustomAdCnt = 0;
        self.currentGoogleAdCnt = 0;
        self.showBottomLoader = true
        getAllTrends(SortColumn: "",SortDirection: "",Skip: 0, Take: noOfRecordsPerRequest, lazyLoading: false)
        self.trendTableView.reloadData()
        //        self.refreshControl.endRefreshing()
    }
    
    func getAllTrends(SortColumn:String,SortDirection:String,Skip:Int,Take:Int,lazyLoading:Bool = false) {
        
        if ((trendData.data.count + self.currentCustomAdCnt + self.currentGoogleAdCnt) >= totalTrends) || isGetAllTrendRequestInProgress{
            if isGetAllTrendRequestInProgress{
                return
            }
            self.showBottomLoader = false
            self.trendTableView.tableFooterView?.isHidden = true
            self.isGetAllTrendRequestInProgress = false
            return
        }
        
        self.isGetAllTrendRequestInProgress = true
        
        let params = ["SortColumn":SortColumn,"SortDirection":SortDirection,"Search":"","Skip":Skip,"Take":Take,"FolderName":showTrendsByFolder] as [String:Any]
        print("Param : ",params)
        print("Param totalTrends", totalTrends,"trendData.count",trendData.data.count)
        
        WebServices().callUserService(service: .getMyTrends, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: lazyLoading, isHeader: true, CallMethod: .post) { (serviceResponse, responseData) in
            print("Get All Trends --- \(serviceResponse)")
            if(serviceResponse["Status"] as! Bool == true)
            {
                DispatchQueue.main.async
                    {
                        //  self.trendImagesArray.removeAll()
                        
                        let temp = WebServices().decodeDataToClass(data: responseData, decodeClass: TrendListData.self)!
                        self.trendData.data += temp.data
                        
                        self.currentCustomAdCnt += serviceResponse["CurrentCustomAdCount"] as! Int
                        self.currentGoogleAdCnt += serviceResponse["CurrentGoogleAdCount"] as! Int
                        
                        self.totalTrends = (self.currentCustomAdCnt + self.currentGoogleAdCnt) + (serviceResponse["TotalRecords"] as! Int)
                        
                        self.isGetAllTrendRequestInProgress = false
                        
                        let tempTrendImageArray = NSMutableArray()
                        let tempTrendImageDic = NSMutableDictionary()
                        
                        self.trendTableView.reloadData()
                }
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
            
        }
    }
    
    @objc func sortAction(_ sender: UIButton){
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "SortingViewController") as! SortingViewController
        destination.delegate = self
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    func downloadSync(fromURL: String) -> UIImage? {
        let request = NSURLRequest(url: NSURL(string: fromURL)! as URL)
        var response: URLResponse?
        do {
            let data = try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response)
            return UIImage(data: data)!
        } catch {
            print("Error while trying to download following file: " + fromURL)
        }
        return nil
    }
    
    // MARK: - Like Action
    @objc func didTapLikeButton(button: UIButton) {
        var isTrendLiked = Bool()
        //        for trend in self.trendData.data {
        //            if trend.TrendId == button.tag {
        //                isTrendLiked = trend.IsTrendLike
        //            }
        //        }
        isTrendLiked = (self.trendData.data[Int(button.accessibilityIdentifier!)!].IsTrendLike)
        if isTrendLiked == true {
            dislikeTrend(trendID: "\(button.tag)", index: button.accessibilityIdentifier!)
        }
        else {
            likeTrend(trendID: "\(button.tag)", index: button.accessibilityIdentifier!)
        }
    }
    
    func likeTrend(trendID: String, index: String) {
        WebServices().callUserService(service: .likeUnlikeTrend, urlParameter: "\(trendID)", parameters: nil, isLazyLoading: false, isHeader: true, CallMethod: .post) { (serviceResponse, responseData) in
            
            print("Like Trend Response --- \(serviceResponse)")
            if serviceResponse["Status"] == nil {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                return
            }
            if(serviceResponse["Status"] as! Bool == true)
            {
                DispatchQueue.main.async {
                    self.trendData.data[Int(index)!].IsTrendLike = true
                    self.trendData.data[Int(index)!].TrendLikesCount = self.trendData.data[Int(index)!].TrendLikesCount + 1
                    let cellIndexPath = IndexPath(row: Int(index)!, section: 0)
                    let cell = self.trendTableView.cellForRow(at: cellIndexPath)as? TrendTableViewCell
                    cell!.trendLikesImageView.image = #imageLiteral(resourceName: "Like_clk")
                    cell!.likesCountLabel.text = "\(self.trendData.data[Int(index)!].TrendLikesCount)"
                }
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func dislikeTrend(trendID: String, index: String) {
        WebServices().callUserService(service: .likeUnlikeTrend, urlParameter: "\(trendID)", parameters: nil, isLazyLoading: false, isHeader: true, CallMethod: .post) { (serviceResponse, responseData) in
            
            print("Dislike Trend Response --- \(serviceResponse)")
            if serviceResponse["Status"] == nil {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                return
            }
            if(serviceResponse["Status"] as! Bool == true)
            {
                
                DispatchQueue.main.async {
                    self.trendData.data[Int(index)!].IsTrendLike = false
                    self.trendData.data[Int(index)!].TrendLikesCount = self.trendData.data[Int(index)!].TrendLikesCount - 1
                    let cellIndexPath = IndexPath(row: Int(index)!, section: 0)
                    let cell = self.trendTableView.cellForRow(at: cellIndexPath)as? TrendTableViewCell
                    cell!.trendLikesImageView.image = #imageLiteral(resourceName: "Like")
                    cell!.likesCountLabel.text = "\(self.trendData.data[Int(index)!].TrendLikesCount)"
                }
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    
    // MARK: - Register Table View Cells
    func registerTableViewCells() {
        trendTableView.register(UINib(nibName: "TrendTableViewCell", bundle: nil), forCellReuseIdentifier: "TrendTableViewCell")
        trendTableView.register(UINib(nibName:"GoogleAdTVC",bundle:nil),forCellReuseIdentifier:"GoogleAdTVC")
    }
    
    // MARK: - Register Collection View Cells
    func registerCollectionViewCells() {
        trendImagesCollectionView.register(UINib(nibName: "ImageTrendCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageTrendCollectionViewCell")
        
    }
    
    @objc func btnLearnMoreAction(_ sender : UIButton)
    {
        if trendData.data[sender.tag].IsCustomAd{
            //            if let url = URL(string: "https://stackoverflow.com") {
            
            if var url = trendData.data[sender.tag].AdUrl {
                if !(url.contains("http"))
                {
                    url = url.replacingOccurrences(of: "www.", with: "https://")
                }
                
                if UIApplication.shared.canOpenURL(URL(string: url)!)
                {
                    UIApplication.shared.open(URL(string: url)!)
                }
                //                UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
            }
        }
    }
    
    @objc func btnFullScrProfileImgAction(_ sender : UIButton)
    {
        let zoomImageView = ZoomImageVC(nibName: "ZoomImageVC", bundle: nil)
        if trendData.data[sender.tag].UserProfileImage != nil
        {
            zoomImageView.imageUrl = trendData.data[sender.tag].UserProfileImage!
        }
        self.navigationController?.pushViewController(zoomImageView, animated: true)
    }
    
    @objc func tapActionForLabel(_ recognizer : UITapGestureRecognizer)
    {
        if recognizer.state == UIGestureRecognizer.State.ended {
            print(recognizer.view!.tag)
            
            if trendData.data[recognizer.view!.tag].IsTrendShared == false{
                if trendData.data[recognizer.view!.tag].IsCustomAd{
                }
                else{
                    if (trendData.data[recognizer.view!.tag].AreYouFollowingTrender == 0 || trendData.data[recognizer.view!.tag].AreYouFollowingTrender == 1 || trendData.data[recognizer.view!.tag].AreYouFollowingTrender == 2)
                    {
                        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
                        let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
                        profileVC.userId = trendData.data[recognizer.view!.tag].UserProfileId
                        self.navigationController?.pushViewController(profileVC, animated: true)
                    }
                    else
                    {
                        //                        (self.tabBarController!).selectedIndex = 4
                    }
                }
            }
            else{
                let firstRange = ((recognizer.view! as! UILabel).text as! NSString).range(of: "\(trendData.data[recognizer.view!.tag].UserProfileFirstName!) \(String(describing: trendData.data[recognizer.view!.tag].UserProfileLastName!))")
                
                let secondRange = ((recognizer.view! as! UILabel).text as! NSString).range(of: "\(trendData.data[recognizer.view!.tag].TrendOwner!)")
                
                if recognizer.didTapAttributedTextInLabel(label: (recognizer.view! as! UILabel), inRange: firstRange) {
                    print(firstRange)
                    if Int(MySingleton.shared.loginObject.UserID) == trendData.data[recognizer.view!.tag].UserProfileId
                    {
                        //                        (self.tabBarController!).selectedIndex = 4
                    }
                    else
                    {
                        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
                        let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
                        profileVC.userId = trendData.data[recognizer.view!.tag].UserProfileId
                        self.navigationController?.pushViewController(profileVC, animated: true)
                    }
                    
                } else if recognizer.didTapAttributedTextInLabel(label: (recognizer.view! as! UILabel), inRange: secondRange) {
                    print(secondRange)
                    if Int(MySingleton.shared.loginObject.UserID) != trendData.data[recognizer.view!.tag].TrendOwnerId!
                    {
                        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
                        let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
                        profileVC.userId = trendData.data[recognizer.view!.tag].TrendOwnerId!
                        self.navigationController?.pushViewController(profileVC, animated: true)
                    }
                    else
                    {
                        //                        (self.tabBarController!).selectedIndex = 4
                    }
                    
                } else {
                    print("Tapped none")
                }
            }
            
        }
    }
    
    // MARK: - Config Trend Table View Cell
    func configTrendTableViewCell(tableView: UITableView, withIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "TrendTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! TrendTableViewCell
        trendImagesCollectionView = cell.trendImageCollectionView
        
        // Register Collection View Cells
        registerCollectionViewCells()
        cell.trendCurrentImageLabel.layer.cornerRadius = 3.0
        // Testing
        var calculatedWidth = CGFloat()
        var calculatedHeight = CGFloat()
        calculatedWidth = self.view.frame.size.width
        cell.profileImageView.layer.cornerRadius = 0.5 * cell.profileImageView.bounds.size.width
        cell.profileImageView.clipsToBounds = true
        
        calculatedHeight = (CGFloat(trendData.data[indexPath.row].ImageList![0].ImageHeight ?? 250) * calculatedWidth) / (CGFloat(trendData.data[indexPath.row].ImageList![0].ImageWidth ?? 250))
        
        if trendData.data[indexPath.row].IsTrendShared == false{
            
            if trendData.data[indexPath.row].IsCustomAd{ // Custom Ad
                cell.btnLearnMore.isHidden = false
                cell.btnLearnMore.tag = indexPath.row
                cell.btnLearnMore.addTarget(self, action: #selector(btnLearnMoreAction(_:)), for: .touchUpInside)
                cell.profileImageView.isHidden = true
                cell.shareDescLabel.text = ""
                cell.cnstrntShareDescHeight.constant = 0
                cell.cnstrTrendHeaderViewHeight.constant = 0
                cell.cnstrTrendStatusViewHeight.constant = 0
                cell.cnstrTrendViewCommentViewHeight.isActive = true
                cell.trendViewCommentsView.isHidden = true
                cell.trendStatusView.isHidden = true
                cell.clipsToBounds = true
                //
                cell.videoLayer.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: calculatedHeight)
            }else{
                //             tableView.rowHeight = 286 + calculatedHeight - 61
                cell.btnLearnMore.isHidden = true
                cell.profileImageView.isHidden = false
                cell.profileImageView.layer.cornerRadius = 0.5 * cell.profileImageView.bounds.size.width
                cell.profileImageView.clipsToBounds = true
                
                cell.shareDescLabel.text = ""
                cell.cnstrntShareDescHeight.constant = 0
                cell.cnstrTrendViewCommentViewHeight.isActive = false
                cell.trendViewCommentsView.isHidden = false
                cell.trendStatusView.isHidden = false
                cell.profileNameLabel.text = "\(trendData.data[indexPath.row].UserProfileFirstName ?? "") \(trendData.data[indexPath.row].UserProfileLastName ?? "")"
                cell.profileNameLabel.textColor = UIColor.darkText
                cell.profileNameLabel.tag = indexPath.row
                cell.profileNameTap.numberOfTapsRequired = 1
                cell.profileNameLabel.isUserInteractionEnabled = true
                cell.profileNameTap = UITapGestureRecognizer(target: self, action: #selector(tapActionForLabel(_:)))
                cell.profileNameLabel.addGestureRecognizer(cell.profileNameTap)
                cell.btnFullScrProfileImg.tag = indexPath.row
                cell.btnFullScrProfileImg.addTarget(self, action: #selector(btnFullScrProfileImgAction(_:)), for: .touchUpInside)
                
                cell.locationLabel.text = trendData.data[indexPath.row].TrendLocation ?? ""
                cell.cnstrTrendHeaderViewHeight.constant = CGFloat(trendHeaderViewHeight)
                cell.cnstrTrendStatusViewHeight.constant = CGFloat(trendStatusViewHeight)
                
                if trendData.data[indexPath.row].UserProfileImage != nil {
                    cell.profileImageView.sd_setImage(with: URL(string: trendData.data[indexPath.row].UserProfileImage!), placeholderImage: #imageLiteral(resourceName: "img_selectProfilePic"))
                }
                else
                {
                    cell.profileImageView.image = #imageLiteral(resourceName: "img_selectProfilePic")
                }
                
            }
        }
        else
        {
            cell.btnLearnMore.isHidden = true
            cell.profileImageView.isHidden = false
            cell.profileImageView.layer.cornerRadius = 0.5 * cell.profileImageView.bounds.size.width
            cell.profileImageView.clipsToBounds = true
            
            tableView.rowHeight = 286 + calculatedHeight - 28
            cell.cnstrntShareDescHeight.constant = 33
            cell.trendViewCommentsView.isHidden = false
            cell.cnstrTrendViewCommentViewHeight.isActive = false
            cell.trendStatusView.isHidden = false
            cell.shareDescLabel.text = trendData.data[indexPath.row].SharedTrendDescription ?? ""
            
            cell.btnFullScrProfileImg.tag = indexPath.row
            cell.btnFullScrProfileImg.addTarget(self, action: #selector(btnFullScrProfileImgAction(_:)), for: .touchUpInside)
            
            cell.profileNameLabel.tag = indexPath.row
            cell.profileNameTap.numberOfTapsRequired = 1
            cell.profileNameLabel.isUserInteractionEnabled = true
            cell.profileNameTap = UITapGestureRecognizer(target: self, action: #selector(tapActionForLabel(_:)))
            cell.profileNameLabel.addGestureRecognizer(cell.profileNameTap)
            
            let color1 = UIColor.darkText
            // create the attributed colour
            let attributedStringColor = [NSAttributedString.Key.foregroundColor : color1];
            // create the attributed string
            let attributedString = NSMutableAttributedString(string: "\(trendData.data[indexPath.row].UserProfileFirstName!) \(String(describing: trendData.data[indexPath.row].UserProfileLastName!))", attributes: attributedStringColor)
            // Set the label
            
            
            
            // create the attributed colour
            let attrs1 = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 17), NSAttributedString.Key.foregroundColor : UIColor.black]
            //            let attributedStringColor2 = [NSAttributedString.Key.foregroundColor : color2];
            let attributedString2 = NSMutableAttributedString(string: " shared ", attributes: attrs1)
            attributedString.append(attributedString2)
            let color3 = UIColor.darkText
            let attributedStringColor3 = [NSAttributedString.Key.foregroundColor : color3];
            let attributedString3 = NSMutableAttributedString(string: "\(trendData.data[indexPath.row].TrendOwner ?? "")'s post", attributes: attributedStringColor3)
            attributedString.append(attributedString3)
            cell.profileNameLabel.attributedText = attributedString
            
            cell.locationLabel.text = ""
            cell.cnstrTrendHeaderViewHeight.constant = CGFloat(trendHeaderViewHeight)
            cell.cnstrTrendStatusViewHeight.constant = CGFloat(trendStatusViewHeight)
            
            if trendData.data[indexPath.row].UserProfileImage != nil {
                cell.profileImageView.sd_setImage(with: URL(string: trendData.data[indexPath.row].UserProfileImage!), placeholderImage: #imageLiteral(resourceName: "img_selectProfilePic"))
            }
            else
            {
                cell.profileImageView.image = #imageLiteral(resourceName: "img_selectProfilePic")
            }
        }
        
        if !trendData.data[indexPath.row].IsCustomAd{
            cell.videoLayer.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: calculatedHeight+20)
        }
        
        
        // Data
//        cell.trendExpiryLabel.text = "Expires in \(trendData.data[indexPath.row].RemainingExpiryTime ?? "")"
//        cell.trendAddedLabel.text = "\(trendData.data[indexPath.row].TimeAgo ?? "") Ago"
        cell.trendPostTitleLabel.text = trendData.data[indexPath.row].Title
        cell.postCommentLabel.text = trendData.data[indexPath.row].Description
        cell.viewAllCommentsButton.setTitle("View All \(trendData.data[indexPath.row].CommentCount ?? "") Comment", for: .normal)
        cell.commentsCountLabel.text = "\(trendData.data[indexPath.row].CommentCount ?? "")"
        cell.likesCountLabel.text = "\(trendData.data[indexPath.row].TrendLikesCount)"
        cell.shareCountLabel.text = "\(trendData.data[indexPath.row].TrendShareCount)"
        cell.viewsCountLabel.text = "\(trendData.data[indexPath.row].TrendViewCount ?? "")"
        
        
//        cell.txtAddComment.attributedPlaceholder = NSAttributedString(string: "Write a comment...",
//                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.baseColor])
        
        
        if trendData.data[indexPath.row].IsTrenderOnline
        {
            cell.statusImageView.image = #imageLiteral(resourceName: "img_Active")
        }
        else
        {
            cell.statusImageView.image = #imageLiteral(resourceName: "img_inactive")
        }
        
        if trendData.data[indexPath.row].AreYouFollowingTrender == 0{
            cell.followStatusButton.isHidden = false
            cell.followStatusButton.setTitle(MySingleton.shared.selectedLangData.following, for: .normal)
            cell.followStatusButton.setTitleColor(UIColor.baseColor, for: .normal)
            cell.followStatusButton.tag = indexPath.row
            cell.followStatusButton.addTarget(self, action: #selector(FollowButtonAction(_:)), for: .touchUpInside)
            cell.otherUserProfileButton.isHidden = true
            //            cell.otherUserProfileButton.tag = indexPath.row
            //            cell.otherUserProfileButton.addTarget(self, action: #selector(OtherProfileAction(_:)), for: .touchUpInside)
            
        }
        else if trendData.data[indexPath.row].AreYouFollowingTrender == 1{
            cell.followStatusButton.isHidden = false
            cell.followStatusButton.setTitle(MySingleton.shared.selectedLangData.follow, for: .normal)
            cell.followStatusButton.setTitleColor(UIColor.lightGray, for: .normal)
            cell.followStatusButton.tag = indexPath.row
            cell.followStatusButton.addTarget(self, action: #selector(FollowButtonAction(_:)), for: .touchUpInside)
            cell.otherUserProfileButton.isHidden = true
            //            cell.otherUserProfileButton.tag = indexPath.row
            //            cell.otherUserProfileButton.addTarget(self, action: #selector(OtherProfileAction(_:)), for: .touchUpInside)
        }
        else if trendData.data[indexPath.row].AreYouFollowingTrender == 2{
            cell.followStatusButton.isHidden = false
            cell.followStatusButton.setTitle(MySingleton.shared.selectedLangData.Requested, for: .normal)
            cell.followStatusButton.setTitleColor(UIColor.lightGray, for: .normal)
            cell.followStatusButton.tag = indexPath.row
            cell.followStatusButton.addTarget(self, action: #selector(FollowButtonAction(_:)), for: .touchUpInside)
            cell.otherUserProfileButton.isHidden = true
            //            cell.otherUserProfileButton.tag = indexPath.row
            //            cell.otherUserProfileButton.addTarget(self, action: #selector(OtherProfileAction(_:)), for: .touchUpInside)
        }
        else
        {
            cell.followStatusButton.isHidden = true
            cell.otherUserProfileButton.isHidden = true
            //            cell.otherUserProfileButton.tag = indexPath.row
            //            cell.otherUserProfileButton.addTarget(self, action: #selector(OtherProfileAction(_:)), for: .touchUpInside)
        }
        
        
        if trendData.data[indexPath.row].EnableTrendLike == true {
            cell.likeButton.isHidden = false
        }
        else {
            cell.likeButton.isHidden = true
        }
        if trendData.data[indexPath.row].IsTrendLike == true {
            cell.trendLikesImageView.image = #imageLiteral(resourceName: "Like_clk")
        }
        else {
            cell.trendLikesImageView.image = #imageLiteral(resourceName: "Like")
        }
        cell.likeButton.accessibilityIdentifier = String(indexPath.row)
        cell.likeButton.tag = trendData.data[indexPath.row].TrendId
        cell.likeButton.addTarget(self, action: #selector(didTapLikeButton), for: .touchUpInside)
        cell.trendImageCollectionView.tag = indexPath.row
        cell.trendImageCollectionView.delegate = self
        cell.trendImageCollectionView.dataSource = self
        cell.trendImagesPageControl.numberOfPages = trendData.data[indexPath.row].ImageList!.count //trendImagesCollectionArray[indexPath.row].count //trendData.data[indexPath.row].ImageList.count
        cell.trendImagesPageControl.hidesForSinglePage = true
        trendImagesPageControl = cell.trendImagesPageControl
        trendCurrentImageLabel = cell.trendCurrentImageLabel
        cell.trendImageCollectionView.reloadData()
        cell.trendImageCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: true)
        
        
        cell.trendCurrentImageLabel.text = "1/\(trendData.data[indexPath.row].ImageList!.count)"
        
        cell.addCommentButton.tag = indexPath.row
        cell.addCommentButton.addTarget(self, action: #selector(self.addCommentAction(_:)), for: .touchUpInside)
        
        if(trendData.data[indexPath.row].ImageList!.count > 1)
        {
            cell.trendCurrentImageLabel.isHidden = false
        }
        else
        {
            cell.trendCurrentImageLabel.isHidden = true
        }
        
        if trendData.data[indexPath.row].ImageList![0].Media == "VIDEO"{
            cell.videoURL = trendData.data[indexPath.row].ImageList![0].VideoUrl
            cell.cnstrntVideoWidth.constant = cell.trendStatusView.frame.size.width/4
            cell.trendViewsView.isHidden = false
            cell.imgVideoIcon.isHidden = false
        }
        else
        {
            cell.videoURL = nil
            cell.cnstrntVideoWidth.constant = 0
            cell.trendViewsView.isHidden = true
            cell.imgVideoIcon.isHidden = true
        }
        
        
        
        cell.trendMoreButton.tag = indexPath.row
        cell.trendMoreButton.addTarget(self, action: #selector(trendMoreButtonAction(_:)), for: .touchUpInside)
        
        cell.shareCountButton.tag = indexPath.row
        cell.shareCountButton.addTarget(self, action: #selector(shareCountButtonAction(_:)), for: .touchUpInside)
        
        cell.btnShareTrend.tag = indexPath.row
        cell.btnShareTrend.addTarget(self, action: #selector(btnShareTrendAction(_:)), for: .touchUpInside)
        
        cell.likeCountButton.tag = indexPath.row
        cell.likeCountButton.addTarget(self, action: #selector(likeCountButtonAction(_:)), for: .touchUpInside)
        cell.viewsCountButton.tag = indexPath.row
        cell.viewsCountButton.addTarget(self, action: #selector(viewsCountButtonAction(_:)), for: .touchUpInside)
        
        cell.txtAddComment.delegate = self
        cell.txtAddComment.returnKeyType = .send
        cell.txtAddComment.tag = indexPath.row
        if MySingleton.shared.loginObject.ProfilePic != ""
        {
            cell.profileButton.layer.cornerRadius = 0.5 * cell.profileButton.bounds.size.width
            cell.profileButton.clipsToBounds = true
            cell.profileButton.sd_setImage(with: URL(string: MySingleton.shared.loginObject.ProfilePic!), for: .normal)
        }
        else
        {
            //            cell.profileButton.layer.cornerRadius = 0.5 * cell.profileButton.bounds.size.width
            //            cell.profileButton.clipsToBounds = true
            cell.profileButton.setImage(#imageLiteral(resourceName: "img_selectProfilePic"), for: .normal)
        }
        
        cell.selectedLang(trendData: self.trendData,index:indexPath.row)
        
        //        cell.setNeedsLayout()
        //        cell.layoutIfNeeded()
        
        return cell
    }
    
    // MARK: - Save comment
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if !((textField.text?.isEmpty)!)
        {
            addComment(index: textField.tag,text: textField.text!)
        }
        return true
    }
    
    func addComment(index:Int,text:String)
    {
        self.view.endEditing(true)
        var param = [String : Any]()
        
        param = ["TrendID":trendData.data[index].TrendId,"CommentId":"0","CommentText":text ,"IsImage":"false",
                 "ImagePath":[],"gifID":"","TaggedUsers":[]] as [String : Any]
        
        WebServices().callUserService(service: UserServices.addComment, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
            if(serviceResponse["Status"] as! Bool == true)
            {
                let cell = self.trendTableView.cellForRow(at: IndexPath(row: index, section: 0))as? TrendTableViewCell
                self.trendData.data[index].CommentCount = String(Int(self.trendData.data[index].CommentCount ?? "")! + 1)
                cell!.commentsCountLabel.text = "\(self.trendData.data[index].CommentCount)"
                cell!.txtAddComment.text = ""
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        })
    }
    
    @objc func addCommentAction(_ sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Comment", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "CommentViewController") as! CommentViewController
        destination.trendId = trendData.data[sender.tag].TrendId
        destination.trendIndex = sender.tag
        destination.delegate = self
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @objc func viewsCountButtonAction(_ sender : UIButton)
    {
        let dataToSend = ["TrendId" : self.trendData.data[sender.tag].TrendId, "Type": "ViewsCount"] as [String : Any]
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "LikesListViewController") as! LikesListViewController
        destination.trendData = dataToSend
        self.navigationController?.pushViewController(destination, animated: true)
        //        self.performSegue(withIdentifier: "DashboardToLikesCount", sender: dataToSend)
    }
    
    @objc func OtherProfileAction(_ sender : UIButton)
    {
        if (trendData.data[sender.tag].AreYouFollowingTrender == 0 || trendData.data[sender.tag].AreYouFollowingTrender == 1)
        {
            let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
            let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
            profileVC.userId = trendData.data[sender.tag].UserProfileId
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        else
        {
            //            (self.tabBarController!).selectedIndex = 4
        }
    }
    
    @objc func FollowButtonAction(_ sender : UIButton)
    {
        if sender.currentTitle == MySingleton.shared.selectedLangData.follow{
            //follow api
            callApiForFavouriteUnfavourite(urlParam: String(trendData.data[sender.tag].UserProfileId), service: .followUser, index: sender.tag)
        }
        else if sender.currentTitle == MySingleton.shared.selectedLangData.following
        {
            //unfollow api
            callApiForFavouriteUnfavourite(urlParam: String(trendData.data[sender.tag].UserProfileId), service: .unfollowUser, index: sender.tag)
        }
    }
    
    @objc func likeCountButtonAction(_ sender : UIButton)
    {
        let dataToSend = ["TrendId" : self.trendData.data[sender.tag].TrendId, "Type": "LikesCount"] as [String : Any]
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "LikesListViewController") as! LikesListViewController
        destination.trendData = dataToSend
        self.navigationController?.pushViewController(destination, animated: true)
        //        self.performSegue(withIdentifier: "DashboardToLikesCount", sender: dataToSend)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DashboardToLikesCount" {
            let likesListViewController = segue.destination as! LikesListViewController
            likesListViewController.trendData = sender as! [String : Any]
            
        }
        else if segue.identifier == "DashboardToFullScreen" {
            let fullScreenViewController = segue.destination as! FullScreenViewController
            fullScreenViewController.imageList = trendData.data[sender as! Int].ImageList!
            fullScreenViewController.cellIndex = sender as! Int
            fullScreenViewController.delegate = self
            //                sender as! [TrendListImageData]
            fullScreenViewController.pageIndex = pageIndex
        }
    }
    
    func callApiForFavouriteUnfavourite(urlParam : String, service : UserServices, index : Int){
        WebServices().callUserService(service: service, urlParameter: urlParam, parameters: nil, isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
            if service == .markTrendFavourite{
                self.trendData.data[index].IsTrendFavourite = true
            }
            else if service == .markTrendUnfavourite
            {
                self.trendData.data[index].IsTrendFavourite = false
            }
            else if service == .followUser{
                self.trendData.data[index].AreYouFollowingTrender = 2
                let cell = self.trendTableView.cellForRow(at: IndexPath(row: index, section: 0))as! TrendTableViewCell
                cell.followStatusButton.setTitle(MySingleton.shared.selectedLangData.Requested, for: .normal)
                cell.followStatusButton.setTitleColor(UIColor.lightGray, for: .normal)
                
            }
            else if service == .unfollowUser{
                self.trendData.data[index].AreYouFollowingTrender = 1
                let cell = self.trendTableView.cellForRow(at: IndexPath(row: index, section: 0))as! TrendTableViewCell
                cell.followStatusButton.setTitle(MySingleton.shared.selectedLangData.follow, for: .normal)
                cell.followStatusButton.setTitleColor(UIColor.lightGray, for: .normal)
            }
            else if service == .deleteTrend{
                //                self.getAllTrends()
                self.trendData.data.remove(at: index)
                // self.trendImagesArray.remove(at: index)
                self.trendTableView.reloadData()
            }
            else if service == .setTrendPushNotificationStatus
            {
                if(serviceResponse["Status"] as! Bool == true)
                {
                    if (serviceResponse["Message"] as! String).contains("disabled")
                    {
                        self.trendData.data[index].IsPushNotificationEnabled = false
                    }
                    else
                    {
                        self.trendData.data[index].IsPushNotificationEnabled = true
                    }
                }
                else
                {
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }
            
        })
    }
    
    @objc func shareCountButtonAction(_ sender : UIButton)
    {
        let dataToSend = ["TrendId" : self.trendData.data[sender.tag].TrendId, "Type": "ShareCount"] as [String : Any]
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "LikesListViewController") as! LikesListViewController
        destination.trendData = dataToSend
        self.navigationController?.pushViewController(destination, animated: true)
        //        self.performSegue(withIdentifier: "DashboardToLikesCount", sender: dataToSend)
    }
    
    @objc func btnShareTrendAction(_ sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "ShareInternalViewController") as! ShareInternalViewController
        destination.trendId = self.trendData.data[sender.tag].TrendId
        destination.friendsPopupDelegate = self
        let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height)
        self.navigationController?.present(pop, animated: true, completion: nil)
    }
    
    @objc func trendMoreButtonAction(_ sender : UIButton)
    {
        let cell = trendTableView.cellForRow(at: IndexPath(row: sender.tag, section: 0))as! TrendTableViewCell
        
        cell.trendDropDown.anchorView = sender
        cell.trendDropDown.bottomOffset = CGPoint(x: 0, y:(cell.trendDropDown.anchorView?.plainView.bounds.height)!)
        cell.trendDropDown.topOffset = CGPoint(x: 0, y:-(cell.trendDropDown.anchorView?.plainView.bounds.height)!)
        
        cell.trendDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            print(sender.tag)
            
            switch index{
            case 0:
                let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
                let destination = storyboard.instantiateViewController(withIdentifier: "ShareInternalViewController") as! ShareInternalViewController
                destination.trendId = self.trendData.data[sender.tag].TrendId
                destination.friendsPopupDelegate = self
                let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height)
                self.navigationController?.present(pop, animated: true, completion: nil)
                
            case 2:
                if item == "Favorite"{
                    self.callApiForFavouriteUnfavourite(urlParam: String(self.trendData.data[sender.tag].TrendId), service: .markTrendFavourite, index: sender.tag)
                }
                else
                {
                    self.callApiForFavouriteUnfavourite(urlParam: String(self.trendData.data[sender.tag].TrendId), service: .markTrendUnfavourite, index: sender.tag)
                }
                
            case 3:
                self.callApiForFavouriteUnfavourite(urlParam: String(self.trendData.data[sender.tag].TrendId), service: .setTrendPushNotificationStatus, index: sender.tag)
                
            case 4:
                let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
                let destination = storyboard.instantiateViewController(withIdentifier: "ReportTrendViewController") as! ReportTrendViewController
                let pop = PopUpViewController(destination,withHeight: 270.0)
                self.navigationController?.present(pop, animated: true, completion: nil)
                ////                self.navigationController?.present(destination, animated: true, completion: nil)
                //                self.present(destination, animated: true, completion: nil)
                
            case 5:
                let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
                let destination = storyboard.instantiateViewController(withIdentifier: "SelectFolderViewController") as! SelectFolderViewController
                destination.createFolderPopupDelegate = self
                self.saveFolderTrendId = self.trendData.data[sender.tag].TrendId
                destination.trendId = self.trendData.data[sender.tag].TrendId
                let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height * 0.7)
                self.navigationController?.present(pop, animated: true, completion: nil)
                
            case 6:
                let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
                let destination = storyboard.instantiateViewController(withIdentifier: "SetTrendViewController") as! SetTrendViewController
                destination.editTrendParam = self.trendData.data[sender.tag]
                destination.isEdit = true
                //  (self.tabBarController!).selectedIndex = 2
                self.navigationController?.pushViewController(destination, animated: true)
                
            case 7:
                let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: MySingleton.shared.selectedLangData.confirmation, message: MySingleton.shared.selectedLangData.Are_you_sure_you_want_to_delete_this_trend, preferredStyle: .alert)
                let deleteActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.delete, style: .default) { void in
                    self.callApiForFavouriteUnfavourite(urlParam: String(self.trendData.data[sender.tag].TrendId), service: .deleteTrend, index: sender.tag)
                }
                let cancelActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.cancel, style: .cancel) { void in
                }
                actionSheetControllerIOS8.addAction(deleteActionButton)
                actionSheetControllerIOS8.addAction(cancelActionButton)
                actionSheetControllerIOS8.popoverPresentationController?.sourceView = self.view
                actionSheetControllerIOS8.popoverPresentationController?.sourceRect = self.view.bounds
                self.present(actionSheetControllerIOS8, animated: true, completion: nil)
                
            default:
                break
            }
        }
        
      cell.trendMoreButtonAction(index: sender.tag, trendData: self.trendData)
    }
    
    // MARK: - Config Trend Image Collection View Cell
    func configTrendImagesCollectionViewCell(collectionView: UICollectionView, withIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = "ImageTrendCollectionViewCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! ImageTrendCollectionViewCell
        cell.trendImageView.sd_setImage(with: URL(string: trendData.data[collectionView.tag].ImageList![indexPath.item].TrendImage), placeholderImage: #imageLiteral(resourceName: "logo_navigation"))//sd_setImage(with: URL(string: trendData.data[collectionView.tag].ImageList[indexPath.item].TrendImage), placeholderImage: #imageLiteral(resourceName: "favourite"), options: SDWebImageOptions.cacheMemoryOnly, completed: nil)//trendImagesCollectionArray[collectionView.tag][indexPath.item]//sd_setImage(with: URL(string: trendData.data[collectionView.tag].ImageList[indexPath.item].TrendImage), placeholderImage: #imageLiteral(resourceName: "favourite"), options: SDWebImageOptions.cacheMemoryOnly, completed: nil)
        return cell
    }
    
    // MARK: - Config Home Collection View Cell
    func configHomeCollectionViewCell(collectionView: UICollectionView, withIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = "HomeCollectionViewCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! HomeCollectionViewCell
        
        cell.trendImageView.image = testImageArray[indexPath.item]
        cell.trendImageView.contentMode = .scaleAspectFill
        collectionView.backgroundColor = .red
        cell.backgroundColor = .white
        return cell
    }
    
}

extension MyTrendsVC:UIImagePickerControllerDelegate, UINavigationControllerDelegate,CropViewControllerDelegate{
    
    func openGallary()
    {
        picker.allowsEditing = false
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        present(picker, animated: true, completion: nil)
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            present(picker, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        let chosenImage = info[.originalImage] as! UIImage
        
        let image: UIImage = chosenImage
        //Load an image
        dismiss(animated:true, completion: nil)
        let cropViewController = CropViewController(image: image)
        cropViewController.delegate = self
        present(cropViewController, animated: true, completion: nil)
        
        //        self.profileImg.image = chosenImage
        //
        //        dismiss(animated:true, completion: nil)
        //        uploadImage(chosenImage)
    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        // 'image' is the newly cropped version of the original image
        //        let indexPath = IndexPath(row: 0, section: 0)
        //        let cell = trendTableView.cellForRow(at: indexPath) as! MyTrendHeaderTVC
        let headerView = trendTableView.headerView(forSection: 0) as! MyTrendHeader
        headerView.profileImg.image = image
        //        cell.profileImg.image = image
        //        self.profileImg.image = image
        
        dismiss(animated:true, completion: nil)
        uploadImage(image)
    }
    
    func uploadImage(_ imageToUpload:UIImage){
        var parameters = [String:AnyObject]()
        
        let imageData = ["FileExt":"jpg","contenttype":"image/jpeg","ImageData":"data:image/jpg;base64,\(Utilities.shared.encodeToBase64String(image: imageToUpload))"]
        
        let imageArray:[AnyObject] = [imageData as AnyObject]
        
        parameters = ["ProfileImage":["Images":imageArray]] as [String:Any] as [String : AnyObject]
        callApi(requestData: parameters, url: "\(WebServices.baseURL)\(UserServices.uploadProfileImage.rawValue)")
        
    }
    
    func callApi(requestData: Dictionary<String, Any>,url:String)
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        var urlStr:String = url
        
        urlStr = urlStr.replacingOccurrences(of: " ", with: "&")
        let request :NSMutableURLRequest = NSMutableURLRequest(url:NSURL(string:urlStr)! as URL)
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        var TokenValue = String()
        TokenValue = "Bearer " + MySingleton.shared.loginObject.access_token
        print(TokenValue)
        request.addValue(TokenValue, forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        if requestData.count != 0
        {
            do
            {
                let jsonData = try JSONSerialization.data(withJSONObject: requestData, options: JSONSerialization.WritingOptions.prettyPrinted)
                request.httpBody = jsonData
            }
            catch let error as NSError
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                }
            }
        }
        
        let task = URLSession.shared.dataTask(with: request as URLRequest){ data,response,error in
            
            if error != nil
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error!.localizedDescription, completionHandler: nil)
                }
                
            }
            do
            {
                if (response as? HTTPURLResponse) != nil
                {
                    
                    //////////////////// JSON RESPONSE /////////////////
                    
                    
                    let jsonString = String(data: data!, encoding: .utf8)
                    print(jsonString!)
                    
                    //////////////////// JSON RESPONSE /////////////////
                    
                    
                    if data != nil
                    {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        
                        print(jsonData)
                        
                        if(jsonData["Status"] as! Bool == true)
                        {
                            
                            DispatchQueue.main.async {
                                MBProgressHUD.hide(for: self.view, animated: true)
                                //                                TrenderAlertVC.shared.presentAlertController(message: jsonData["Message"] as! String, completionHandler: nil)
                            }
                            
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                MBProgressHUD.hide(for: self.view, animated: true)
                                TrenderAlertVC.shared.presentAlertController(message: jsonData["Message"] as! String, completionHandler: nil)
                            }
                        }
                        
                        
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true)
                            TrenderAlertVC.shared.presentAlertController(message: error!.localizedDescription, completionHandler: nil)
                        }
                    }
                    
                }
            }
            catch let error as NSError
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                }
            }
        }
        
        task.resume()
    }
}



extension MyTrendsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UIScreen.main.bounds.height * 0.2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if trendData.data[indexPath.row].IsGoogleAd {
            return CGFloat(googleAdHeight)
        }else {
            var calculatedWidth = CGFloat()
            var calculatedHeight = CGFloat()
            calculatedWidth = self.view.frame.size.width
            
            calculatedHeight = ((CGFloat(trendData.data[indexPath.row].ImageList![0].ImageHeight ?? 250)) * calculatedWidth) / (CGFloat(trendData.data[indexPath.row].ImageList![0].ImageWidth ?? 250))
            if trendData.data[indexPath.row].IsTrendShared == false{
                
                if trendData.data[indexPath.row].IsCustomAd{ // Custom Ad
                    return calculatedHeight
                }
                return 286 + calculatedHeight - 61
            }else{
                return 286 + calculatedHeight - 28
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "MyTrendHeader") as! MyTrendHeader
        cell.editProfileBtn.addTarget(self, action: #selector(goToEditProfile(_:)), for: .touchUpInside)
        cell.changeImgBtn.addTarget(self, action: #selector(changeProfilePic(_:)), for: .touchUpInside)
        cell.createFolderBtn.addTarget(self, action: #selector(showCreateFolderPopup(_:)), for: .touchUpInside)
        cell.allBtn.addTarget(self, action: #selector(showAllFolders(_:)), for: .touchUpInside)
        cell.sortBtn.addTarget(self, action: #selector(sortAction(_:)), for: .touchUpInside)
        cell.cornerRadius()
        if !self.userInfoObj.data.isEmpty{
            cell.profileNameLbl.text = "\(self.userInfoObj.data[0].FirstName ?? "") \(self.userInfoObj.data[0].LastName ?? "")"
            cell.profileImg.sd_setImage(with: URL(string: self.userInfoObj.data[0].UserProfileImage ?? ""), placeholderImage: UIImage(named: "img_selectProfilePic"), options: [.continueInBackground], completed: nil)
            //                DispatchQueue.main.async {
            cell.profileImg.makeCornerRadius(radius: cell.profileImg.frame.height/2)
            //                }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if trendData.data.isEmpty && !isGetAllTrendRequestInProgress{
            self.noDataLbl.isHidden = false
        }else{
            self.noDataLbl.isHidden = true
        }
        
        return trendData.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if trendData.data[indexPath.row].IsGoogleAd{
            let cell = tableView.dequeueReusableCell(withIdentifier: "GoogleAdTVC", for: indexPath) as! GoogleAdTVC
            cell.bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
            cell.bannerView.rootViewController = self
            cell.bannerView.load(GADRequest())
            return cell
        }else{
            return configTrendTableViewCell(tableView: tableView, withIndexPath: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let videoCell = cell as? ASAutoPlayVideoLayerContainer, let _ = videoCell.videoURL {
            ASVideoPlayerController.sharedVideoPlayer.removeLayerFor(cell: videoCell)
        }
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            pausePlayeVideos()
        }
    }
    
    func pausePlayeVideos(){
        ASVideoPlayerController.sharedVideoPlayer.pausePlayeVideosFor(tableView: trendTableView)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
            if showBottomLoader{
                //            let spinner = UIActivityIndicatorView(style: .gray)
                //            spinner.startAnimating()
                //            spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: self.trendTableView.bounds.width, height: CGFloat(44))
                //            self.trendTableView.tableFooterView = spinner
                //            self.trendTableView.tableFooterView?.isHidden = false
            }
            getAllTrends(SortColumn: self.titleSort,SortDirection: self.direction,Skip: self.trendData.data.count, Take: noOfRecordsPerRequest,lazyLoading:true)
            
        }
    }
    
    
    
    //    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
    //        if scrollView == self.trendTableView {
    //            if self.trendTableView.indexPathsForVisibleRows!.count > 1
    //            {
    //                let topVisibleIndexPath:IndexPath = self.trendTableView.indexPathsForVisibleRows![1]
    //                print(topVisibleIndexPath.row)
    //                let cell_indexPath = NSIndexPath(row: topVisibleIndexPath.row, section: topVisibleIndexPath.section)
    //                let tableViewCell = self.trendTableView.cellForRow(at: cell_indexPath as IndexPath) as? TrendTableViewCell
    //
    //                if trendData.data[topVisibleIndexPath.row].ImageList[0].Media == "VIDEO"
    //                {
    //                    DispatchQueue.main.async
    //                        {
    //                            let topVisibleIndexPathArray = self.trendTableView.indexPathsForVisibleRows!
    //                            for i in 0...topVisibleIndexPathArray.count-1
    //                            {
    //                                let cell_indexPath = NSIndexPath(row: topVisibleIndexPathArray[i].row, section: topVisibleIndexPathArray[i].section)
    //                                let tableViewCell = self.trendTableView.cellForRow(at: cell_indexPath as IndexPath) as? TrendTableViewCell
    //                                tableViewCell?.viewVideo.stopVideo()
    //                                tableViewCell?.viewVideo.isHidden = true
    //
    //                                //                            if self.trendData.data[topVisibleIndexPathArray[i].row].MediaType == "IMAGE"
    //                                //                            {
    //                                //
    //                                //                            }
    //                            }
    //                            let topVisibleIndexPathFirst:IndexPath = self.trendTableView.indexPathsForVisibleRows![0]
    //                            print(topVisibleIndexPathFirst.row)
    //                            let cellRect = self.trendTableView.rectForRow(at: topVisibleIndexPathFirst)
    //                            let completelyVisible = self.trendTableView.bounds.contains(cellRect)
    //                            let indexPath = NSIndexPath(row: 0, section: 0)
    //                            let tableViewCellFirst = self.trendTableView.cellForRow(at: indexPath as IndexPath) as? TrendTableViewCell
    //                            if (self.trendData.data[0].ImageList[0].Media == "VIDEO" && completelyVisible)
    //                            {
    //                                tableViewCellFirst?.viewVideo.isHidden = false
    //                                tableViewCellFirst?.viewVideo.playerURl = self.trendData.data[0].ImageList[0].VideoUrl!
    //                                tableViewCellFirst?.viewVideo.playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
    //                                tableViewCellFirst?.viewVideo.playVideo()
    //                                tableViewCellFirst?.viewVideo.playerLayer.player?.volume = 0.0
    //                            }
    //                            else
    //                            {
    //                                if self.trendData.data[topVisibleIndexPath.row].ImageList[0].VideoUrl != ""
    //                                {
    //                                    tableViewCellFirst?.viewVideo.isHidden = false
    //                                    tableViewCellFirst?.viewVideo.playerURl = self.trendData.data[topVisibleIndexPath.row].ImageList[0].VideoUrl!
    //                                    tableViewCellFirst?.viewVideo.playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
    //                                    tableViewCellFirst?.viewVideo.playVideo()
    //                                    tableViewCellFirst?.viewVideo.playerLayer.player?.volume = 0.0
    //                                }
    //                            }
    //                    }
    //                }
    //                else
    //                {
    //                    tableViewCell?.viewVideo.stopVideo()
    //                    tableViewCell?.viewVideo.isHidden = true
    //                }
    //            }
    //        }
    //    }
    
    //    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    //        print("scrollViewDidEndDragging")
    //        if scrollView == self.trendTableView {
    //            if self.trendTableView.indexPathsForVisibleRows!.count > 1
    //            {
    //                let topVisibleIndexPath:IndexPath = self.trendTableView.indexPathsForVisibleRows![1]
    //                print(topVisibleIndexPath.row)
    //                let cell_indexPath = NSIndexPath(row: topVisibleIndexPath.row, section: topVisibleIndexPath.section)
    //                let tableViewCell = self.trendTableView.cellForRow(at: cell_indexPath as IndexPath) as? TrendTableViewCell
    //
    //                if trendData.data[topVisibleIndexPath.row].ImageList[0].Media == "VIDEO"
    //                {
    //                    DispatchQueue.main.async
    //                    {
    //                        let topVisibleIndexPathArray = self.trendTableView.indexPathsForVisibleRows!
    //                        for i in 0...topVisibleIndexPathArray.count-1
    //                        {
    //                            let cell_indexPath = NSIndexPath(row: topVisibleIndexPathArray[i].row, section: topVisibleIndexPathArray[i].section)
    //                            let tableViewCell = self.trendTableView.cellForRow(at: cell_indexPath as IndexPath) as? TrendTableViewCell
    //                            if tableViewCell?.viewVideo.isHidden == false
    //                            {
    //                                tableViewCell?.viewVideo.stopVideo()
    //                                tableViewCell?.viewVideo.isHidden = true
    //                            }
    //
    //                        }
    //                        let topVisibleIndexPathFirst:IndexPath = self.trendTableView.indexPathsForVisibleRows![0]
    //                        print(topVisibleIndexPathFirst.row)
    //                        let cellRect = self.trendTableView.rectForRow(at: topVisibleIndexPathFirst)
    //                        let completelyVisible = self.trendTableView.bounds.contains(cellRect)
    //                        let indexPath = NSIndexPath(row: 0, section: 0)
    //                        let tableViewCellFirst = self.trendTableView.cellForRow(at: indexPath as IndexPath) as? TrendTableViewCell
    //                        if (self.trendData.data[0].ImageList[0].Media == "VIDEO" && completelyVisible)
    //                        {
    //                            tableViewCellFirst?.viewVideo.isHidden = false
    //                            tableViewCellFirst?.viewVideo.playerURl = self.trendData.data[0].ImageList[0].VideoUrl!
    //                            tableViewCellFirst?.viewVideo.playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
    //                            tableViewCellFirst?.viewVideo.playVideo()
    //                            tableViewCellFirst?.viewVideo.playerLayer.player?.volume = 0.0
    //                        }
    //                        else
    //                        {
    //                            if self.trendData.data[topVisibleIndexPath.row].ImageList[0].VideoUrl != ""
    //                            {
    //                                tableViewCellFirst?.viewVideo.isHidden = false
    //                                tableViewCellFirst?.viewVideo.playerURl = self.trendData.data[topVisibleIndexPath.row].ImageList[0].VideoUrl!
    //
    //                                tableViewCellFirst?.viewVideo.playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
    //                                tableViewCellFirst?.viewVideo.playVideo()
    //                                tableViewCellFirst?.viewVideo.playerLayer.player?.volume = 0.0
    //                            }
    //                        }
    //                    }
    //                }
    //                else
    //                {
    //                    tableViewCell?.viewVideo.stopVideo()
    //                    tableViewCell?.viewVideo.isHidden = true
    //                }
    //            }
    //        }
    //    }
    
    //    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //        print("started \(indexPath.row)")
    //        if trendData.data[indexPath.row].MediaType == "VIDEO"
    //        {
    //            if trendData.data[indexPath.row].TrendVideo != nil
    //            {
    //
    //                let url = URL(string:self.trendData.data[indexPath.row].TrendVideo!)
    //
    //                let player = AVPlayer(url: url!)
    //                self.avpController.player = player
    //
    //                self.avpController.view.frame.size.height = cell.contentView.frame.size.height
    //
    //                self.avpController.view.frame.size.width = cell.contentView.frame.size.width
    //
    //                cell.contentView.addSubview(self.avpController.view)
    ////                avplayer = AVPlayer()
    ////                let fileURL = URL(fileURLWithPath: trendData.data[1].TrendVideo!)
    ////                avplayer = AVPlayer(url: fileURL)
    ////                avplayer.actionAtItemEnd = .none
    ////
    ////                let videoLayer = AVPlayerLayer(player: avplayer)
    ////                videoLayer.frame = viewTestVideo.bounds
    ////                videoLayer.videoGravity = .resizeAspectFill
    ////                view.layer.addSublayer(videoLayer)
    ////
    ////                avplayer.play()
    //            }
    //        }
    //
    //    }
    
}

extension MyTrendsVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == trendCollectionView {
            // GRID
            return testImageArray.count
        }
        else {
            // IMAGEs
            print("Trend Image Collection View Tag --- \(collectionView.tag) --- Count --- \(trendData.data[collectionView.tag].ImageList!.count)")
            return self.trendData.data[collectionView.tag].ImageList!.count//trendImagesCollectionArray[collectionView.tag].count
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == trendCollectionView {
            return configHomeCollectionViewCell(collectionView:collectionView, withIndexPath: indexPath)
        }
        else {
            return configTrendImagesCollectionViewCell(collectionView: collectionView, withIndexPath: indexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if trendData.data[collectionView.tag].IsCustomAd{
            if let url = trendData.data[collectionView.tag].AdUrl{
                UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
            }
            return
        }
        
        if collectionView == trendCollectionView{
            
        }
        else
        {
            pageIndex = indexPath.item
            //            self.performSegue(withIdentifier: "DashboardToFullScreen", sender: collectionView.tag)
            let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
            let destination = storyboard.instantiateViewController(withIdentifier: "FullScreenViewController") as! FullScreenViewController
            destination.imageList = trendData.data[collectionView.tag].ImageList!
            destination.cellIndex = collectionView.tag
            destination.delegate = self
            
            self.navigationController?.pushViewController(destination, animated: true)
            //            (string: trendData.data[collectionView.tag].ImageList[indexPath.item].TrendImage)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == trendCollectionView {
            var calculatedWidth = CGFloat()
            var calculatedHeight = CGFloat()
            //            if Utilities.getImageWidth(image: testImageArray[indexPath.item]) > self.view.frame.size.width {
            //                // Actual image
            //                calculatedWidth = CGFloat(self.view.frame.size.width / 3) * 2
            //                calculatedHeight = calculatedWidth//(calculatedWidth * Utilities.getImageHeight(image: testImageArray[indexPath.item])) / Utilities.getImageWidth(image: testImageArray[indexPath.item])
            //            }
            //            else if ((self.view.frame.size.width / 3) * 2) < Utilities.getImageWidth(image: testImageArray[indexPath.item]) {
            //
            //
            //                // Doubled the width of one cell
            //                calculatedWidth = (self.view.frame.size.width / 3) * 2
            //                calculatedHeight = self.view.frame.size.width / 3
            //            }
            //            else {
            //                // Normal
            //                calculatedWidth = self.view.frame.size.width / 3//Utilities.getImageWidth(image: testImageArray[indexPath.item])
            //                calculatedHeight = calculatedWidth//Utilities.getImageHeight(image: testImageArray[indexPath.item])
            //            }
            
            if indexPath.item == 0 {
                calculatedWidth = self.view.frame.size.width / 3
                calculatedHeight = calculatedWidth
            }
            else if indexPath.item == 1 {
                calculatedWidth = self.view.frame.size.width / 3
                calculatedHeight = calculatedWidth
            }
            else if indexPath.item == 2 {
                
                
                calculatedWidth = (self.view.frame.size.width / 3) * 2
                calculatedHeight = calculatedWidth
            }
            else {
                calculatedWidth = self.view.frame.size.width / 3
                calculatedHeight = calculatedWidth
            }
            
            return CGSize(width: calculatedWidth, height: calculatedHeight)
        }
        else {
            var calculatedWidth = CGFloat()
            var calculatedHeight = CGFloat()
            calculatedWidth = self.view.frame.size.width
            
            calculatedHeight = ((CGFloat(trendData.data[collectionView.tag].ImageList![0].ImageHeight ?? 250)) * calculatedWidth) / (CGFloat(trendData.data[collectionView.tag].ImageList![0].ImageWidth ?? 250))
            
            return CGSize(width: calculatedWidth, height: calculatedHeight)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == trendCollectionView {
        }
        else {
            if trendData.data.count > 0 {
                let cell = trendTableView.cellForRow(at: IndexPath(row: collectionView.tag, section: 0)) as? TrendTableViewCell
                if cell != nil {
                    cell!.trendImagesPageControl.currentPage = indexPath.item
                    cell!.trendCurrentImageLabel.text = "\(indexPath.item + 1)/\(trendData.data[collectionView.tag].ImageList!.count)"
                }
                
            }
        }
    }
}

extension MyTrendsVC : CreateFolderPopupDelegate{
    func createFolderPopup() {
        let storyboard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
        let nextViewController = storyboard.instantiateViewController(withIdentifier: "NewFolderPoupVC") as! NewFolderPoupVC
        nextViewController.delegate = self
        let pop = PopUpViewController(nextViewController,withHeight: self.view.frame.size.height)
        self.navigationController?.present(pop, animated: true, completion: nil)
    }
    
}

extension MyTrendsVC : CreateFolderDelegate {
    func controller() {
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "SelectFolderViewController") as! SelectFolderViewController
        destination.createFolderPopupDelegate = self
        destination.trendId = self.saveFolderTrendId
        destination.shareTrendId = self.shareTrendId
        let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height * 0.7)
        self.navigationController?.present(pop, animated: true, completion: nil)
    }
    
}

extension MyTrendsVC : TrendViewDelegate{
    func IncreaseTrendViewCount(Index: Int) {
        DispatchQueue.main.async {
            let cell = self.trendTableView.cellForRow(at: IndexPath(row: Index, section: 0))as? TrendTableViewCell
            self.trendData.data[Index].TrendViewCount = String (Int(self.trendData.data[Index].TrendViewCount ?? "0")! + 1)
            cell!.viewsCountLabel.text = "\(self.trendData.data[Index].TrendViewCount ?? "")"
        }
    }
}


extension MyTrendsVC : TrendCommentDelegate,friendSelectedDelegate,FriendsPopupDelegate,EditProfileVCDelegate,SortViewDelegate {
    func sortParameters(direction: String, title: String) {
        self.titleSort = title
        self.direction = direction
        self.trendData.data.removeAll()
        self.totalTrends = 1
        self.currentCustomAdCnt = 0
        self.currentGoogleAdCnt = 0
        self.showBottomLoader = true
        self.noOfRecordsPerRequest = 10
        getAllTrends(SortColumn: self.titleSort, SortDirection: self.direction, Skip: 0, Take: self.noOfRecordsPerRequest, lazyLoading: false)
    }
    
    
    func profileUpdated() {
        self.getProfileInfo()
    }
    
    
    func friendsPopup(indexShare: Int, trendId: Int, description: String) {
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "FriendListViewController") as! FriendListViewController
        destination.delegate = self
        destination.trendId = trendId
        destination.index = indexShare
        destination.txtdescprition = description
        let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height)
        self.navigationController?.present(pop, animated: true, completion: nil)
    }
    
    
    func controller(arraySelectedFriends: [TagFriendModel], index: Int, trendId: Int,description: String) {
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "ShareInternalViewController") as! ShareInternalViewController
        destination.trendId = trendId
        
        if arraySelectedFriends.count != 0
        {
            for i in 0...arraySelectedFriends.count-1
            {
                if(arraySelectedFriends[i].isSelected)
                {
                    let dict = ["FriendId": arraySelectedFriends[i].id]
                    selectedShareFriends.append(dict as AnyObject)
                }
            }
        }
        destination.selectedShareFriends = selectedShareFriends
        destination.index = index
        destination.txtdiscription = description
        let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height)
        self.navigationController?.present(pop, animated: true, completion: nil)
    }
    
    func commentCount(Index: Int,CommentCount: Int) {
        let cell = trendTableView.cellForRow(at: IndexPath(row: Index, section: 0))as? TrendTableViewCell
        trendData.data[Index].CommentCount = String(CommentCount)
        cell!.commentsCountLabel.text = trendData.data[Index].CommentCount
    }
    
    
}
