//
//  MyTrendHeader.swift
//  TrenderAlert
//
//  Created by HPL on 18/04/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class MyTrendHeader: UITableViewHeaderFooterView {
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var changeImgBtn: UIButton!
    @IBOutlet weak var profileNameLbl: UILabel!
    @IBOutlet weak var editProfileBtn: UIButton!
    @IBOutlet weak var allBtn: UIButton!
    @IBOutlet weak var createFolderBtn: UIButton!
    @IBOutlet weak var folderNameLbl: UILabel!
    @IBOutlet weak var sortBtn: UIButton!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    
    func cornerRadius(){
        self.editProfileBtn.makeCornerRadius(radius: 5)
        self.createFolderBtn.makeCornerRadius(radius: 5)
        self.allBtn.makeCornerRadius(radius: 5)
        self.sortBtn.makeCornerRadius(radius:5)
        self.editProfileBtn.setTitle(MySingleton.shared.selectedLangData.edit_profile, for: .normal)
        self.createFolderBtn.setTitle(MySingleton.shared.selectedLangData.create_folder, for: .normal)
    }
}
