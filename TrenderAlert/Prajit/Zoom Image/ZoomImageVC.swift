//
//  ZoomImageVC.swift
//  TrenderAlert
//
//  Created by HPL on 07/03/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import SDWebImage

class ZoomImageVC: UIViewController,UIScrollViewDelegate{

    //Outlets
    @IBOutlet weak var imageImg: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    //Properties
    var imageUrl = ""
    var defaultImageName = "Img_Blur"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = self
        
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 10.0
        setupNavigationBar()

        loadImage()
    }
    
    func setupNavigationBar(){
        Utilities.setNavigationBarWithTitle(viewController: self, title: "")
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "img_BackArrow"), style: .plain, target: self, action: #selector(goBack))
        
        navigationItem.leftBarButtonItem = leftButton
    }
    
    @objc func goBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func loadImage(){
        self.imageImg.sd_setImage(with: URL(string: imageUrl), placeholderImage: UIImage(named: defaultImageName), options: [.progressiveDownload], completed: nil)
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageImg
    }
}
