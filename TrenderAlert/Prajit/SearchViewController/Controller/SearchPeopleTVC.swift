//
//  SearchPeopleTVC.swift
//  TrenderAlert
//
//  Created by HPL on 08/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class SearchPeopleTVC: UITableViewCell {
    
    @IBOutlet weak var cellImg: UIImageView!
    @IBOutlet weak var peopleNameLbl: UILabel!
    @IBOutlet weak var subtitleLbl: UILabel!
    @IBOutlet weak var removeBtn: UIButton!
    
    static let reuseIdentifier = "SearchPeopleTVC"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(){
        DispatchQueue.main.async{
            self.cellImg.makeCornerRadius(radius: self.cellImg.frame.height/2)
        }
    }
    
}
