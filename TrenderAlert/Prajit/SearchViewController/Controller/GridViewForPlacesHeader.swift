//
//  GridViewForPlacesHeader.swift
//  TrenderAlert
//
//  Created by HPL on 30/04/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import MapKit

class GridViewForPlacesHeader: UITableViewHeaderFooterView {
    @IBOutlet weak var mapView: MKMapView!
    
    var lat:String = ""
    var long:String = ""
    let regionRadius: CLLocationDistance = 1000
    let annotation = MKPointAnnotation()
    
    func showMap(){
        print("Lat : ",lat,"Long : ",long)
        let initLocation = CLLocation(latitude: ((self.lat as NSString).doubleValue), longitude: ((self.long as NSString).doubleValue))
        let coordinateRegion = MKCoordinateRegion(center: initLocation.coordinate,
                                                  latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)

        
        annotation.coordinate = CLLocationCoordinate2D(latitude: ((self.lat as NSString).doubleValue) , longitude: ((self.long as NSString).doubleValue))
        mapView.addAnnotation(annotation)
        DispatchQueue.main.async {
//            self.mapView.setCenter(CLLocationCoordinate2D(latitude: ((self.lat as NSString).doubleValue) , longitude: ((self.long as NSString).doubleValue)), animated: true)
        }
    }
}
