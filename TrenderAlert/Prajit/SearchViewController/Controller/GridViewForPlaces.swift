//
//  GridViewForPlaces.swift
//  TrenderAlert
//
//  Created by HPL on 29/04/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class GridViewForPlaces: UIViewController {

    @IBOutlet weak var gridView: UIView!
    @IBOutlet weak var gridViewContainer: UIView!
    @IBOutlet weak var gridPlacesTbl: UITableView!
    
    var lat:String = ""
    var long:String = ""
    var searchString = ""
    var showGridForTagId = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationbar()
//        gridPlacesTbl.delegate = self
//        gridPlacesTbl.dataSource = self
//        gridPlacesTbl.rowHeight = UITableView.automaticDimension
//        gridPlacesTbl.estimatedRowHeight = 100
//        gridPlacesTbl.tableFooterView = UIView()
//        let headerNib = UINib.init(nibName: "GridViewForPlacesHeader", bundle: nil)
//        gridPlacesTbl.register(headerNib, forHeaderFooterViewReuseIdentifier: "GridViewForPlacesHeader")
    }
    
    func setNavigationbar(){
        Utilities.setNavigationBarWithTitle(viewController: self, title: searchString)
        let leftButton = UIBarButtonItem(image: UIImage(named: "img_BackArrow"), style: .plain, target: self, action: #selector(goBack))
            navigationItem.leftBarButtonItem = leftButton
    }
    
    @objc func goBack(){
        self.navigationController?.popViewController(animated: false)
    }
    
    override func viewDidLayoutSubviews() {
        let storyboard = UIStoryboard(name: "SearchSB", bundle: nil)
        let dest = storyboard.instantiateViewController(withIdentifier: "GridViewVC") as! GridViewVC
        dest.showGridForPlaces = true
        dest.latitude = self.lat
        dest.longitude = self.long
        //add as a childviewcontroller
        addChild(dest)

        // Add the child's View as a subview
        self.gridView.addSubview(dest.view)
        dest.view.frame = gridView.bounds
        dest.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        // tell the childviewcontroller it's contained in it's parent
        dest.didMove(toParent: self)
    }
}

//extension GridViewForPlaces:UITableViewDataSource,UITableViewDelegate{
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 1
//    }
    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
//
//        let storyboard = UIStoryboard(name: "SearchSB", bundle: nil)
//        let dest = storyboard.instantiateViewController(withIdentifier: "GridViewVC") as! GridViewVC
//        dest.showGridForPlaces = true
//        dest.latitude = self.lat
//        dest.longitude = self.long
//        //add as a childviewcontroller
//        addChild(dest)
//
//        // Add the child's View as a subview
////        self.gridView.addSubview(dest.view)
////        dest.view.frame = gridView.bounds
////        dest.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//
//        cell.addSubview(dest.view)
//        dest.view.frame = cell.bounds
//        dest.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        // tell the childviewcontroller it's contained in it's parent
//        dest.didMove(toParent: self)
//        return cell
//    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return (gridPlacesTbl.bounds.height - (gridPlacesTbl.bounds.height * 0.2))
//    }
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "GridViewForPlacesHeader") as! GridViewForPlacesHeader
//        return cell
//    }
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return gridPlacesTbl.bounds.height * 0.2
//    }
//}
