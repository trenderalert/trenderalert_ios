//
//  PlayerViewClass.swift
//  TrenderAlert
//
//  Created by HPL on 01/04/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import AVKit
import AVFoundation

class PlayerViewClass:UIView{
    
   
    var pause:Bool = false
    
    override static var layerClass:AnyClass{
        return AVPlayerLayer.self;
    }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    var playerLayer:AVPlayerLayer{
        return layer as! AVPlayerLayer
    }

    var player:AVPlayer?{
        get{
            return playerLayer.player;
        }
        set{
            playerLayer.player = newValue;
        }
    }
   
    
}
