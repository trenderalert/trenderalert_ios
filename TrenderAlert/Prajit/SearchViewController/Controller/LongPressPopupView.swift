//
//  LongPressPopupView.swift
//  TrenderAlert
//
//  Created by HPL on 02/04/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class LongPressPopupView: UIView {

    @IBOutlet weak var profileImg: UIImageView!
    
    @IBOutlet weak var userProfileNameLbl: UILabel!
    @IBOutlet weak var centerView: UIView!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var profileBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var likeOptionView: UIView!
    @IBOutlet weak var viewProfileOptionView: UIView!
    @IBOutlet weak var shareOptionView: UIView!
    @IBOutlet weak var moreOptionView: UIView!
    
    @IBOutlet weak var likeBtnTitleLbl: UILabel!
    
    @IBOutlet weak var heartOptionImage: UIImageView!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func awakeFromNib() {
        self.mainView.makeCornerRadius(radius: 15)
        likeOptionView.makeCornerRadius(radius:10)
        viewProfileOptionView.makeCornerRadius(radius:10)
        shareOptionView.makeCornerRadius(radius:10)
        moreOptionView.makeCornerRadius(radius:10)
    }
}
