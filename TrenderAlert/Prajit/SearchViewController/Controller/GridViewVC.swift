//
//  GridViewVC.swift
//  TrenderAlert
//
//  Created by HPL on 26/03/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import SDWebImage
import AVFoundation
import Alamofire
import MBProgressHUD

enum ViewTag:Int{
    case blurViewTag = 888
    case longPressViewTag = 999
    case likeBtnTag = 1050
    case profileBtnTag = 1051
    case shareBtnTag = 1052
    case optionBtnTag = 1053
    case likeViewTag = 2050
    case profileViewTag = 2051
    case shareViewTag = 2052
    case optionViewTag = 2053
    case playerViewTag = 111
    case view1Tag = 765
    case view2Tag = 766
    case view3Tag = 767
    case view4Tag = 768
    case view5Tag = 769
    case view6Tag = 770
    case view7Tag = 771
    case view8Tag = 772
    case view9Tag = 773
    case cell2View1Tag = 774
    case cell2View2Tag = 775
    case cell2View3Tag = 776
    case cell2View4Tag = 777
    case cell2View5Tag = 778
    case cell2View6Tag = 779
    case cell2View7Tag = 780
    case cell2View8Tag = 781
    case cell2View9Tag = 782
}
//
//  GridViewVC.swift
//  TrenderAlert
//
//  Created by HPL on 26/03/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import SDWebImage
import AVFoundation


class GridViewVC: UIViewController {
    
    @IBOutlet weak var gridTable: UITableView!
    @IBOutlet weak var searchGridBtn: UIButton!
    @IBOutlet weak var noDataLbl: UILabel!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchViewHeight: NSLayoutConstraint!
    @IBOutlet weak var searchViewHeightZero: NSLayoutConstraint!
    @IBOutlet weak var searchLbl: UILabel!
    
    
    var heightForGridCell = (UIDevice.current.userInterfaceIdiom == .pad) ? 0.5 : 0.6
    var totalTrendCount = 1
    let noOfRecordPerRequest = 18
    var searchText = ""
    var isGridRequestInProgress = false
    var isLikeRequestInProgress = false
    var totalViews = 9
    var videoCellIndex = 9
    var isHotTrend = false
    var isMyTrend = false
    var isFavouriteOrOtherUserTrend = false
    var isGroupTrends = false
    var groupId = 0
    var showTrendsByFolder = ""
    var OtherUserProfileID:Int!
    
    var trendDetailsParam:[String:Any] = [:]
    var trendDetailsUrl:UserServices?
    
    
    var noOfGridTrends = 0{
        didSet{
            DispatchQueue.main.async {
                self.gridTable?.reloadData()
            }
        }
    }
    
    var gridData = GridViewModel()
    var longPressOptionInProgress = false
    var longPressView:LongPressPopupView?
    var showGridForTagId  = false
    var hideSearchbar = false
    var selectedShareFriends = [AnyObject]()
    var showGridForPlaces = false
    var latitude = ""
    var longitude = ""
    
    var sortColumn = String()
    var sortDirection = String()
//    var delegate : SortViewDelegate?
    
    var TagId:Int = 0
    var viewRef:UIView!
    
    lazy var refreshControl:UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = UIColor.red
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        reloadGridView(lazyLoading: false);
        refreshControl.endRefreshing()
    }
    
    func reloadGridView(lazyLoading:Bool){
        self.gridData.data = []
        totalTrendCount = 1
        getTrendsForGridView(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: lazyLoading, sortColumn: sortColumn, sortDIrection: sortDirection)
        self.gridTable.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationbar()
        setupTableview()
        if showGridForTagId || showGridForPlaces || hideSearchbar{
            self.searchViewHeight.isActive = false
            self.searchViewHeightZero.priority = UILayoutPriority(rawValue: 1000)
            self.searchLbl.removeFromSuperview()
        }
        if self.isGroupTrends{
            viewRef = self.parent!.view
        }else{
            viewRef = self.view
        }
        SDImageCache.shared().config.shouldCacheImagesInMemory = false
        getTrendsForGridView(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: false, sortColumn: sortColumn, sortDIrection: sortDirection)
        self.searchLbl.text = MySingleton.shared.selectedLangData.search
        self.noDataLbl.text = MySingleton.shared.selectedLangData.no_data_found
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        SDImageCache.shared().clearDisk();
        SDImageCache.shared().clearMemory();
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        SDImageCache.shared().clearMemory();
        // SDImageCache.shared().clearDisk();
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SDImageCache.shared().clearDisk();
        SDImageCache.shared().clearMemory();
    }
    
    func setupNavigationbar(){
        let leftBarButton = UIButton()
        leftBarButton.setImage(#imageLiteral(resourceName: "img_MenuBarBtn"), for: .normal)
        leftBarButton.addTarget(self, action: #selector(didTapMenu), for: .touchUpInside)
        let item1 = UIBarButtonItem()
        item1.customView = leftBarButton
        self.navigationItem.leftBarButtonItems = [item1]
        Utilities.setNavigationBarWithTitle(viewController: self, title: MySingleton.shared.selectedLangData.search)
        
        if showGridForTagId || showGridForPlaces{
            let leftButton = UIBarButtonItem(image: UIImage(named: "img_BackArrow"), style: .plain, target: self, action: #selector(goBack))
            navigationItem.leftBarButtonItem = leftButton
        }
    }
    
    @objc func goBack(){
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func didTapMenu(_ sender: UIBarButtonItem) {
        if self.menuContainerViewController != nil {
           self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
        }
    }
    
    
    func setupTableview(){
        self.gridTable.delegate = self
        self.gridTable.dataSource = self
        self.gridTable.tableFooterView = UIView()
        self.gridTable.register(UINib(nibName: GridViewCell1.reuseIdentifier, bundle: nil), forCellReuseIdentifier: GridViewCell1.reuseIdentifier)
        self.gridTable.register(UINib(nibName: GridViewCell2.reuseIdentifier, bundle: nil), forCellReuseIdentifier: GridViewCell2.reuseIdentifier)
//        self.gridTable.estimatedRowHeight = UIScreen.main.bounds.height * CGFloat(heightForGridCell)
        self.gridTable.estimatedRowHeight = UIScreen.main.bounds.width
        self.gridTable.rowHeight = UITableView.automaticDimension
        self.gridTable.addSubview(self.refreshControl)
        if showGridForPlaces{
            let headerNib = UINib.init(nibName: "GridViewForPlacesHeader", bundle: nil)
            gridTable.register(headerNib, forHeaderFooterViewReuseIdentifier: "GridViewForPlacesHeader")
        }
    }
    
    func getTrendsForGridView(Skip:Int,Take:Int,lazyLoading:Bool,SearchString:String = "",sortColumn:String,sortDIrection:String){
        
        if (gridData.data.count >= totalTrendCount) || isGridRequestInProgress{
            return
        }
        
        self.isGridRequestInProgress = true
        
        var param:[String:Any] = ["Search":"","SortColumn":sortColumn,"SortDirection":sortDIrection,"Skip":Skip,"Take":Take] as [String : Any]
        var url:String = "\(WebServices.baseURL)\(UserServices.getTrendsForGridView.rawValue)"
        
        self.trendDetailsUrl = .getTrendById
        
        if showGridForTagId{ // Get Grid view for selected Tag
            let tagIds = ["TagId":"\(self.TagId)"]
            let tagsArray:[AnyObject] = [tagIds as AnyObject]
            param = ["TagIds":tagsArray,"Skip":Skip,"Take":Take] as [String : Any]
            print("Param : ",tagIds)
            url = "\(WebServices.baseURL)\(UserServices.getTagByPreference.rawValue)"
            self.trendDetailsUrl = .tagByPreferenceList
        }else if showGridForPlaces{
            param = ["Latitude":self.latitude,"Longitude":self.longitude,"Miles":"0","Skip":Skip,"Take":Take] as [String : Any]
            print("Param : ",param)
            url = "\(WebServices.baseURL)\(UserServices.searchPlaceForTrend.rawValue)"
            self.trendDetailsUrl = .SearchPlacesForTrendList
        }else if isHotTrend{
            param = ["Search":"","SortColumn":"","SortDirection":"","Skip":Skip,"Take":Take] as [String : Any]
            url = "\(WebServices.baseURL)\(UserServices.hottestTrendsGrid.rawValue)"
            self.trendDetailsUrl = .getHottestTrends
        }else if isMyTrend{
            param = ["Search":"","SortColumn":sortColumn,"SortDirection":sortDIrection,"Skip":Skip,"Take":Take,"FolderName":self.showTrendsByFolder] as [String : Any]
            url = "\(WebServices.baseURL)\(UserServices.getMyTrendsGrid.rawValue)"
            self.trendDetailsUrl = .getMyTrends
        }else if isFavouriteOrOtherUserTrend{ // Favourite and OtherUser trends
            if OtherUserProfileID != nil{
                param = ["Search":"","SortColumn":sortColumn,"SortDirection":sortDIrection,"Skip":Skip,"Take":Take,"OtherUserProfileID":"\(self.OtherUserProfileID ?? 0)"] as [String : Any]
                url = "\(WebServices.baseURL)\(UserServices.otherUserTrendGrid.rawValue)"
                self.trendDetailsUrl = .getOtherUserTrends
            }else{
                param = ["Search":"","SortColumn":sortColumn,"SortDirection":sortDIrection,"Skip":Skip,"Take":Take] as [String : Any]
                url = "\(WebServices.baseURL)\(UserServices.favouriteTrendGrid.rawValue)"
                self.trendDetailsUrl = .myFavouriteTrend
            }
            
        }else if isGroupTrends{
            param = ["Search":"","SortColumn":sortColumn,"SortDirection":sortDIrection,"Skip":Skip,"Take":Take,"GroupId":self.groupId] as [String : Any]
            url = "\(WebServices.baseURL)\(UserServices.getGroupGrid.rawValue)"
            self.trendDetailsUrl = .getGroupDiscussion
        }
        
        self.trendDetailsParam = param
        
         if (NetworkReachabilityManager()!.isReachable) {
            if lazyLoading == false{
                MBProgressHUD.showAdded(to: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
            }
            print(param)
            
            Alamofire.request(url, method: .post, parameters: param as [String:AnyObject], encoding: JSONEncoding.default, headers: ["Authorization": "Bearer \(MySingleton.shared.loginObject.access_token)"]).responseJSON{response in
                
                switch response.result{
                case .success:
                    DispatchQueue.main.async {
                        MBProgressHUD.hide(for: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
                    }
                    if(response.result.value is [String:AnyObject])
                    {
                        if let JSON  = response.result.value as? [String:AnyObject]{
                            print(JSON)
                            if (JSON["Status"] != nil),(JSON["Status"] as! Bool == true){
                                let temp = WebServices().decodeDataToClass(data: response.data!, decodeClass: GridViewModel.self)!
                                self.gridData.data += temp.data
                                self.totalTrendCount = JSON["TotalRecords"] as! Int
                                self.noOfGridTrends += 1
                                self.isGridRequestInProgress = false
                            }else{
                                TrenderAlertVC.shared.presentAlertController(message: JSON["Message"] as! String, completionHandler: nil)
                            }
                        }
                    }
                    
                case .failure(let error):
                    DispatchQueue.main.async {
                        MBProgressHUD.hide(for: UIApplication.shared.keyWindow!.rootViewController!.view, animated: true)
                        print(error)
                        TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                    }
                }
            }
        }else{
            DispatchQueue.main.async {
                TrenderAlertVC.shared.presentAlertController(message: "Please check your internet connection", completionHandler: nil)
            }
        }
        
    
    }
    
    @IBAction func searchGrid(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "SearchSB", bundle: nil)
        let dest = storyboard.instantiateViewController(withIdentifier: "GridSearchVC") as! GridSearchVC
        self.navigationController?.pushViewController(dest, animated: false)
    }
}

extension GridViewVC:UITableViewDataSource,UITableViewDelegate,GridViewTrendDetailsVCDelegate{
    
    func trendDeleted() {
        self.reloadGridView(lazyLoading: true);
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
         if showGridForPlaces{
            let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "GridViewForPlacesHeader") as! GridViewForPlacesHeader
            cell.lat = self.latitude
            cell.long = self.longitude
            cell.showMap()
            return cell
         }else{
            return UIView(frame: CGRect(x: 0.0, y: 0.0, width: tableView.bounds.width, height: CGFloat.leastNormalMagnitude))
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if showGridForPlaces{
           return UIScreen.main.bounds.height * 0.2
        }
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let elemCnt = gridData.data.count
        if elemCnt == 0 && !isGridRequestInProgress{
            self.noDataLbl.isHidden = false
        }else{
            self.noDataLbl.isHidden = true
        }
        
        if elemCnt > 0, elemCnt < totalViews{
            return 1
        }else{
            let rowCount = ceil(Double(elemCnt)/Double(totalViews))
            return Int(rowCount)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let multiplierIndex = (indexPath.row + 1)
        let startIndex = ((multiplierIndex * totalViews) - (totalViews - 1))
        let endIndex = ((multiplierIndex * totalViews))
        
        if (indexPath.row) % 2 == 0{ // Even
            
            let cell = tableView.dequeueReusableCell(withIdentifier: GridViewCell1.reuseIdentifier, for: indexPath) as! GridViewCell1
            var isVideoAdded = false
            for index in startIndex ... endIndex {
                
                clearSubviewsCell1(cell: cell, viewIndex: index)
                
                if (index % totalViews) == 1,!isVideoAdded{
                    addVideoToCell1(cell: cell, rowIndex: indexPath.row, isImageArr: true, viewIndex: index)
                    isVideoAdded = true
                }else{
                    showImageVideoThumbnailCell1(cell: cell,rowIndex: indexPath.row,isImageArr: true, viewIndex: index)
                }
            }
            return cell
        }else{ // Odd
            let cell = tableView.dequeueReusableCell(withIdentifier: GridViewCell2.reuseIdentifier, for: indexPath) as! GridViewCell2
            var isVideoAdded = false
            for index in startIndex ... endIndex {
                clearSubviewsCell2(cell: cell, viewIndex: index)
                print("Index check : ",index)
                if (index % totalViews) == 1,!isVideoAdded{
                    addVideoToCell2(cell: cell, rowIndex: indexPath.row, isImageArr: true, viewIndex: index)
                    isVideoAdded = true
                }else{
                    showImageVideoThumbnailCell2(cell: cell,rowIndex: indexPath.row,isImageArr: true, viewIndex: index)
                }
            }
            return cell
        }
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let storyboard = UIStoryboard(name: "SearchSB", bundle: nil)
//        let dest = storyboard.instantiateViewController(withIdentifier: "GridViewTrendDetailsVC") as! GridViewTrendDetailsVC
//        dest.trendId = gridData.data[indexPath.row].TrendID!
//        self.navigationController?.pushViewController(dest, animated: true)
//    }
    
    func addLongPressToCell(view:UIView,elemIndex:Int,isVideoCheck:Bool){
        
        view.tag = elemIndex
        view.gestureRecognizers?.removeAll()
        //Show customAd
        if gridData.data[elemIndex].IsCustomAd{
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showCustomAdDetail(sender:)))
            view.addGestureRecognizer(tapGesture)
            return
        }
        
        if isVideoCheck{
            let longPress = UILongPressGestureRecognizer(target: self, action: #selector(showLongPressOptionWithVideoCheck(sender:)))
            view.addGestureRecognizer(longPress)
        }else{
            let longPress = UILongPressGestureRecognizer(target: self, action: #selector(showLongPressOptionsWithImageCheck(sender:)))
            view.addGestureRecognizer(longPress)
        }
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showTrendDetails(sender:)))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc func showCustomAdDetail(sender:UITapGestureRecognizer){
        
        if var url = gridData.data[sender.view!.tag].AdUrl{
            if !(url.contains("http"))
            {
                url = url.replacingOccurrences(of: "www.", with: "https://")
            }
            if UIApplication.shared.canOpenURL(URL(string: url)!) {
                UIApplication.shared.open(URL(string: url)!, options: [:])
            }
//            UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
        }
    }
    
    @objc func showTrendDetails(sender:UITapGestureRecognizer){
        let storyboard = UIStoryboard(name: "SearchSB", bundle: nil)
        let dest = storyboard.instantiateViewController(withIdentifier: "GridViewTrendDetailsVC") as! GridViewTrendDetailsVC
        dest.trendId = gridData.data[sender.view!.tag].TrendID!
        self.trendDetailsParam["TrendId"] = gridData.data[sender.view!.tag].TrendID!
        dest.trendDetailsParam = self.trendDetailsParam
        
        dest.trendDetailsUrl = self.trendDetailsUrl
        dest.gridTrendDelegate = self; self.navigationController?.pushViewController(dest, animated: true)
    }
    
    @objc func showLongPressOptionWithVideoCheck(sender:UILongPressGestureRecognizer){
        if sender.state == UIGestureRecognizer.State.began{
            
            self.removeLongPressViews()
            self.addBlurView()
            
            var resourceUrl = ""
            var isVideo = false
            
            if !gridData.data[sender.view!.tag].Video.isEmpty{
                resourceUrl = gridData.data[sender.view!.tag].Video[0].VideoUrl ?? ""
                isVideo = true
            }else if !gridData.data[sender.view!.tag].Images.isEmpty{
                resourceUrl = gridData.data[sender.view!.tag].Images[0].TrendImage ?? ""
                isVideo = false
            }
            if isVideo{
                self.addVideoToLongPressView(resourceUrl: resourceUrl, sender: sender)
            }else{
                self.addImageToLongPressView(resourceUrl: resourceUrl, sender: sender)
            }
            print("Long pressed : ",sender.view!.tag)
            
        }else if sender.state == UIGestureRecognizer.State.changed{
            showActionTitle(sender:sender)
        }else{
            checkForActions(sender:sender)
        }
    }
    
    func removeLongPressViews(){
        if let blurView = self.viewRef.viewWithTag(ViewTag.blurViewTag.rawValue){
            blurView.removeFromSuperview()
        }
        if let longPressView = self.viewRef.viewWithTag(ViewTag.longPressViewTag.rawValue){
            longPressView.removeFromSuperview()
        }
    }
    func addBlurView(){
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.tag = ViewTag.blurViewTag.rawValue
        blurEffectView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
        viewRef.addSubview(blurEffectView)
    }
    func addVideoToLongPressView(resourceUrl:String,sender:UILongPressGestureRecognizer){
        DispatchQueue.main.async {
            self.longPressView = self.getLongPressView(profleImage: self.gridData.data[sender.view!.tag].Video[0].ProfileImage ?? "", profileName: "\(self.gridData.data[sender.view!.tag].Video[0].FirstName ?? "") \(self.gridData.data[sender.view!.tag].Video[0].LastName ?? "")")
            // Like unlike condition
            var likeImage = "Like"
            if self.gridData.data[sender.view!.tag].IsLiked{
                likeImage = "Like_clk"
            }
            self.longPressView!.heartOptionImage.image = UIImage(named: likeImage)
            self.viewRef.addSubview(self.longPressView!)
            let avPlayer = AVPlayer(url: URL(string:resourceUrl)!)
            avPlayer.isMuted = true
            //////////////
            avPlayer.cancelPendingPrerolls()
            ///////////////////
            let playerView = PlayerViewClass(frame: self.longPressView!.centerView.bounds)
            playerView.isUserInteractionEnabled = true
            
            playerView.contentMode = .scaleAspectFill
            playerView.layer.masksToBounds = true
            playerView.playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
            playerView.autoresizingMask = [.flexibleWidth,.flexibleHeight,.flexibleBottomMargin,.flexibleLeftMargin,.flexibleRightMargin,.flexibleTopMargin]
            playerView.playerLayer.player = avPlayer
            self.longPressView!.centerView.addSubview(playerView)
            let name = NSNotification.Name.AVPlayerItemDidPlayToEndTime
            NotificationCenter.default.removeObserver(self, name: name, object: nil)
            NotificationCenter.default.addObserver(forName: name, object: avPlayer.currentItem, queue: .main){_ in
                avPlayer.seek(to: CMTime.zero)
                avPlayer.play()
            }
            playerView.playerLayer.player?.play()
        }
        
    }
    func addImageToLongPressView(resourceUrl:String,sender:UILongPressGestureRecognizer){
        DispatchQueue.main.async {
            self.longPressView = self.getLongPressView(profleImage: self.gridData.data[sender.view!.tag].Images[0].ProfileImage ?? "", profileName: "\(self.gridData.data[sender.view!.tag].Images[0].FirstName ?? "") \(self.gridData.data[sender.view!.tag].Images[0].LastName ?? "")")
            // Like unlike condition
            var likeImage = "Like"
            if self.gridData.data[sender.view!.tag].IsLiked{
                likeImage = "Like_clk"
            }
            self.longPressView!.heartOptionImage.image = UIImage(named: likeImage)
            self.viewRef.addSubview(self.longPressView!)
            let imageView = UIImageView(frame: self.longPressView!.centerView.bounds)
            imageView.contentMode = .scaleToFill
            imageView.autoresizingMask = [.flexibleWidth,.flexibleHeight,.flexibleBottomMargin,.flexibleLeftMargin,.flexibleRightMargin,.flexibleTopMargin]
//            imageView.sd_setImage(with: URL(string: resourceUrl), placeholderImage: UIImage(named: "Img_Blur"), options: [.continueInBackground], completed: nil)
            SDImageCache.shared().removeImage(forKey: resourceUrl)
            imageView.sd_setImage(with: URL(string: resourceUrl), placeholderImage: UIImage(named: "Img_Blur"), options: [.continueInBackground,.highPriority], completed: nil)
            
            self.longPressView!.centerView.addSubview(imageView)
        }
    }
    
    
    @objc func showLongPressOptionsWithImageCheck(sender:UILongPressGestureRecognizer){
       
        if sender.state == UIGestureRecognizer.State.began{
            self.removeLongPressViews()
            self.addBlurView()
            
            var resourceUrl = ""
            var isVideo = false
            
            if !gridData.data[sender.view!.tag].Images.isEmpty{
                resourceUrl = gridData.data[sender.view!.tag].Images[0].TrendImage ?? ""
                isVideo = false
            }else if !gridData.data[sender.view!.tag].Video.isEmpty{
                resourceUrl = gridData.data[sender.view!.tag].Video[0].VideoUrl ?? ""
                isVideo = true
            }
            if isVideo{
                self.addVideoToLongPressView(resourceUrl: resourceUrl, sender: sender)
            }else{
                self.addImageToLongPressView(resourceUrl: resourceUrl, sender: sender)
            }
            
        }else if sender.state == UIGestureRecognizer.State.changed{
            showActionTitle(sender:sender)
        }else{
            // Check for action
            checkForActions(sender:sender)
        }
    }
    //search_clk
    
    
    func showActionTitle(sender:UILongPressGestureRecognizer){
        // Like Btn
        if let likeBtn = viewRef.viewWithTag(ViewTag.likeBtnTag.rawValue){
            let likeView = viewRef.viewWithTag(ViewTag.likeViewTag.rawValue)
            if likeBtn.convert(likeBtn.bounds, to: self.viewRef).contains(sender.location(in: self.viewRef)){
                likeView?.isHidden = false
//                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            }else{
                likeView?.isHidden = true
            }
        }
        
        // Profile Btn
        if let profileBtn = viewRef.viewWithTag(ViewTag.profileBtnTag.rawValue){
            let profileView = viewRef.viewWithTag(ViewTag.profileViewTag.rawValue)
            if profileBtn.convert(profileBtn.bounds, to: self.viewRef).contains(sender.location(in: self.viewRef)){
                profileView?.isHidden = false
            }else{
                profileView?.isHidden = true
            }
        }
        
        //Share Trend
        // Profile Btn
        if let shareBtn = viewRef.viewWithTag(ViewTag.shareBtnTag.rawValue){
            let shareView = viewRef.viewWithTag(ViewTag.shareViewTag.rawValue)
            if shareBtn.convert(shareBtn.bounds, to: self.viewRef).contains(sender.location(in: self.viewRef)){
                shareView?.isHidden = false
            }else{
                shareView?.isHidden = true
            }
        }
        
        // Options
        if let optionBtn = viewRef.viewWithTag(ViewTag.optionBtnTag.rawValue){
            let optionView = viewRef.viewWithTag(ViewTag.optionViewTag.rawValue)
            if optionBtn.convert(optionBtn.bounds, to: self.viewRef).contains(sender.location(in: self.viewRef)){
                optionView?.isHidden = false
            }else{
                optionView?.isHidden = true
            }
        }
        
    }
    
    
    func checkForActions(sender:UILongPressGestureRecognizer){
        
        if let likeBtn = viewRef.viewWithTag(ViewTag.likeBtnTag.rawValue){
            if likeBtn.convert(likeBtn.bounds, to: self.viewRef).contains(sender.location(in: self.viewRef)){ // Like Button
                self.likeUnlikeTrend(index: sender.view!.tag)
            }
        }
        
        if let viewProfileBtn = viewRef.viewWithTag(ViewTag.profileBtnTag.rawValue){
            if viewProfileBtn.convert(viewProfileBtn.bounds, to: self.viewRef).contains(sender.location(in: self.viewRef)){ // Like Button
                self.viewProfile(index: sender.view!.tag)
            }
        }
        
        if let shareTrendBtn = viewRef.viewWithTag(ViewTag.shareBtnTag.rawValue){
            if shareTrendBtn.convert(shareTrendBtn.bounds, to: self.viewRef).contains(sender.location(in: self.viewRef)){ // Like Button
                self.shareTrend(index: sender.view!.tag)
            }
        }
        
        if let showMoreOptionsBtn = viewRef.viewWithTag(ViewTag.optionBtnTag.rawValue){
            if showMoreOptionsBtn.convert(showMoreOptionsBtn.bounds, to: self.viewRef).contains(sender.location(in: self.viewRef)){
                showMoreOptionsForPopup()
            }
        }
        
        if !self.isLikeRequestInProgress{
            self.removeLongPressViews()
        }
    }
    func likeUnlikeTrend(index:Int){
        self.isLikeRequestInProgress = true
        
        var likeImage = "Like"
        gridData.data[index].IsLiked = !gridData.data[index].IsLiked
        if gridData.data[index].IsLiked{
            likeImage = "Like_clk"
        }
        longPressView!.heartOptionImage.image = UIImage(named: likeImage)
        
        let param:[String:String] = [:]
        let trendId = gridData.data[index].TrendID
        WebServices().callUserService(service: UserServices.likeUnlikeTrend, urlParameter: "\(trendId ?? 0)", parameters: param as [String : AnyObject], isLazyLoading: true, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            
            
            if (serviceResponse["Status"] != nil),(serviceResponse["Status"] as! Bool == true){
                if self.gridData.data[index].IsLiked{
                    self.showLikeEffect()
                }else{
                    self.isLikeRequestInProgress = false
                    DispatchQueue.main.async{
                        self.removeLongPressViews()
                    }
                }
                
            }else{
                self.isLikeRequestInProgress = false
            }
        }
    }
    
    func showLikeEffect(){
        DispatchQueue.main.async{
            
        
        let likeView = self.viewRef.viewWithTag(3050)
        likeView?.transform = .identity
        UIView.animate(withDuration: 0.5, animations: {
            likeView?.isHidden = false
            likeView?.transform = CGAffineTransform(scaleX: 2, y: 2)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4){
                likeView?.transform = .identity
                likeView?.isHidden = true
                self.isLikeRequestInProgress = false
                self.removeLongPressViews()
            }
        }, completion: nil)
        }
    }
    
    func viewProfile(index:Int){
        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
        let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
        profileVC.userId = gridData.data[index].UserProfileId!
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    func shareTrend(index:Int){
        self.removeLongPressViews()
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "ShareInternalViewController") as! ShareInternalViewController
        destination.trendId = self.gridData.data[index].TrendID!
        destination.friendsPopupDelegate = self
        let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height)
        self.navigationController?.present(pop, animated: true, completion: nil)
    }
    
    func showMoreOptionsForPopup(){
        self.removeLongPressViews()
        
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "ReportTrendViewController") as! ReportTrendViewController
        
        let pop = PopUpViewController(destination,withHeight: 272)
        self.navigationController?.present(pop, animated: true, completion: nil)
    }
    
    func getLongPressView(profleImage:String,profileName:String)->LongPressPopupView{
        let longPressView = Bundle.main.loadNibNamed("LongPressPopupView", owner: self, options: nil)?.first as! LongPressPopupView
        longPressView.tag = ViewTag.longPressViewTag.rawValue
        longPressView.frame.size.width = UIScreen.main.bounds.width * 0.95
        longPressView.frame.size.height = UIScreen.main.bounds.height * 0.6
       
        longPressView.center = CGPoint(x: self.viewRef.frame.width/2, y: self.viewRef.frame.height/2)
        
        //        if self.isGroupTrends{
        ////            longPressView.center = CGPoint(x: self.view.frame.width/2, y: self.view.frame.height/2)
        //            longPressView.center = CGPoint(x: self.viewRef.frame.width/2, y: self.viewRef.frame.height/2)
        ////            longPressView.clipsToBounds = true
        //        }
        
        longPressView.profileImg.sd_setImage(with: URL(string: profleImage), placeholderImage: UIImage(named: "img_selectProfilePic"), options: [.continueInBackground], completed: nil)
        longPressView.userProfileNameLbl.text = profileName
        longPressView.profileImg.makeCornerRadius(radius: longPressView.profileImg.frame.height/2)
        return longPressView
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1){
        //            if tableView.visibleCells.contains(cell){
        let elemCnt = self.gridData.data.count
        let rowCount = ceil(Double(elemCnt)/Double(totalViews))
        
        if indexPath.row == (Int(rowCount) - 1){
            
            self.getTrendsForGridView(Skip: self.gridData.data.count, Take: self.noOfRecordPerRequest, lazyLoading: true, sortColumn: self.sortColumn, sortDIrection: self.sortDirection)
        }
        //            }
        //        }
        
    }
    
    func addMultipleItemIconCell1(cell:GridViewCell1,mediaCount:Int,subView:UIView,isRequired:Bool){
        if mediaCount > 1,isRequired{
            let multiImage = UIImageView(frame: CGRect(x: (subView.frame.width - 28), y: (subView.frame.origin.y + 3), width: 20, height: 10))
            multiImage.image = UIImage(named: "Copy")
            subView.addSubview(multiImage)
        }
    }
    func addMultipleItemIconCell2(cell:GridViewCell2,mediaCount:Int,subView:UIView,isRequired:Bool){
        if mediaCount > 1,isRequired{
            let multiImage = UIImageView(frame: CGRect(x: (subView.frame.width - 28), y: (subView.frame.origin.y + 3), width: 20, height: 10))
            multiImage.image = UIImage(named: "Copy")
            subView.addSubview(multiImage)
        }
    }
    
    
    func addVideoToCell1(cell:GridViewCell1,rowIndex:Int,isImageArr:Bool,viewIndex:Int,isVideoThumbnail:Bool = false){
        
        let elemIndex = (viewIndex - 1)
        var mediaCount = 0
        
        
        if !gridData.data.indices.contains(elemIndex){
            clearSubviewsCell1(cell: cell, viewIndex: viewIndex)
            return
        }
        
        if !gridData.data[elemIndex].Video.isEmpty{
//            clearSubviewsCell1(cell: cell, viewIndex: viewIndex)
            let videoUrl = URL(string:gridData.data[elemIndex].Video[0].VideoUrl ?? "")
            mediaCount = gridData.data[elemIndex].Video.count
            let avPlayer = AVPlayer(url: videoUrl!)
            avPlayer.isMuted = true
            if let view = cell.view1.viewWithTag(ViewTag.playerViewTag.rawValue){
                view.removeFromSuperview()
            }
            
            let playerView = PlayerViewClass(frame: cell.view1.bounds)
            playerView.contentMode = .scaleAspectFill
            playerView.layer.masksToBounds = true
            
            playerView.playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
            playerView.autoresizingMask = [.flexibleWidth,.flexibleHeight,.flexibleBottomMargin,.flexibleLeftMargin,.flexibleRightMargin,.flexibleTopMargin]
            playerView.tag = ViewTag.playerViewTag.rawValue
            playerView.playerLayer.player = avPlayer
            addLongPressToCell(view: cell.view1, elemIndex: elemIndex, isVideoCheck: true)
            //////
            addMultipleItemIconCell1(cell: cell, mediaCount: mediaCount, subView: cell.view1, isRequired: false)
            /////
            cell.view1.addSubview(playerView)
            
            
            let name = NSNotification.Name.AVPlayerItemDidPlayToEndTime
            NotificationCenter.default.removeObserver(self, name: name, object: nil)
            NotificationCenter.default.addObserver(forName: name, object: avPlayer.currentItem, queue: .main){_ in
                
                avPlayer.seek(to: CMTime.zero)
                avPlayer.play()
            }
            playerView.playerLayer.player?.play()
        }else if !gridData.data[elemIndex].Images.isEmpty{
            let url = gridData.data[elemIndex].Images[0].TrendImage ?? ""
            mediaCount = gridData.data[elemIndex].Video.count
            
            if let view = cell.view1.viewWithTag(ViewTag.view1Tag.rawValue){
                view.removeFromSuperview()
            }
            
            let imageView = UIImageView(frame: cell.view1.bounds)
            imageView.contentMode = .scaleAspectFill
            imageView.autoresizingMask = [.flexibleWidth,.flexibleHeight,.flexibleBottomMargin,.flexibleLeftMargin,.flexibleRightMargin,.flexibleTopMargin]
            imageView.tag = ViewTag.view1Tag.rawValue
//            imageView.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "Img_Blur"), options: [.continueInBackground], completed: nil)
            
            /////////////// Downsampling //////////////
            
            
            
            imageView.image =  UIImage(named: "Img_Blur");
            
            if let cacheImage = SDImageCache.shared().imageFromCache(forKey: url){
                DispatchQueue.main.async {
                    imageView.image = cacheImage;
                }
            }else{
                SDWebImageManager.shared().imageDownloader?.downloadImage(with: URL(string: url), options: .continueInBackground, progress: nil, completed: {[weak self] (image:UIImage?, data:Data?, error:Error?, finished:Bool) in
                    if image != nil {
                        
                        let newImage = self?.resizeImage(image: image!, newWidth: 300)
                        DispatchQueue.main.async {
                            imageView.image = newImage
                        }
                        SDImageCache.shared().store(newImage, forKey: url, completion: nil)
                        
                        
                    }
                })
            }
            
            /////////////// Downsampling //////////////
            
            
            addLongPressToCell(view: cell.view1, elemIndex: elemIndex, isVideoCheck: false)
            //////
            addMultipleItemIconCell1(cell: cell, mediaCount: mediaCount, subView: cell.view1, isRequired: true)
            /////
            cell.view1.addSubview(imageView)
        }
        
    }
    
    
    func addVideoToCell2(cell:GridViewCell2,rowIndex:Int,isImageArr:Bool,viewIndex:Int,isVideoThumbnail:Bool = false){
        
        let elemIndex = (viewIndex - 1)
        var mediaCount = 0
        
        
        if !gridData.data.indices.contains(elemIndex){
            clearSubviewsCell2(cell: cell, viewIndex: viewIndex)
            return
        }
        
        if !gridData.data[elemIndex].Video.isEmpty{
//            clearSubviewsCell2(cell: cell, viewIndex: viewIndex)
            let videoUrl = URL(string:gridData.data[elemIndex].Video[0].VideoUrl ?? "")
            mediaCount = gridData.data[elemIndex].Video.count
            let avPlayer = AVPlayer(url: videoUrl!)
            avPlayer.isMuted = true
            if let view = cell.view1.viewWithTag(ViewTag.playerViewTag.rawValue){
                view.removeFromSuperview()
            }
            
            let playerView = PlayerViewClass(frame: cell.view1.bounds)
            playerView.contentMode = .scaleAspectFill
            playerView.layer.masksToBounds = true
            
            playerView.playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
            playerView.autoresizingMask = [.flexibleWidth,.flexibleHeight,.flexibleBottomMargin,.flexibleLeftMargin,.flexibleRightMargin,.flexibleTopMargin]
            playerView.tag = ViewTag.playerViewTag.rawValue
            playerView.playerLayer.player = avPlayer
            addLongPressToCell(view: cell.view1, elemIndex: elemIndex, isVideoCheck: true)
            //////
            addMultipleItemIconCell2(cell: cell, mediaCount: mediaCount, subView: cell.view1, isRequired: false)
            /////
            cell.view1.addSubview(playerView)
            
            
            let name = NSNotification.Name.AVPlayerItemDidPlayToEndTime
            NotificationCenter.default.removeObserver(self, name: name, object: nil)
            NotificationCenter.default.addObserver(forName: name, object: avPlayer.currentItem, queue: .main){_ in
                
                avPlayer.seek(to: CMTime.zero)
                avPlayer.play()
            }
            playerView.playerLayer.player?.play()
        }else if !gridData.data[elemIndex].Images.isEmpty{
            let url = gridData.data[elemIndex].Images[0].TrendImage ?? ""
            mediaCount = gridData.data[elemIndex].Video.count
            
            if let view = cell.view1.viewWithTag(ViewTag.cell2View1Tag.rawValue){
                view.removeFromSuperview()
            }
            
            let imageView = UIImageView(frame: cell.view1.bounds)
            imageView.contentMode = .scaleAspectFill
            imageView.autoresizingMask = [.flexibleWidth,.flexibleHeight,.flexibleBottomMargin,.flexibleLeftMargin,.flexibleRightMargin,.flexibleTopMargin]
            imageView.tag = ViewTag.cell2View1Tag.rawValue
//            imageView.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "Img_Blur"), options: [.continueInBackground], completed: nil)
            /////////////// Downsampling //////////////
            imageView.image =  UIImage(named: "Img_Blur");
            
            if let cacheImage = SDImageCache.shared().imageFromCache(forKey: url){
                DispatchQueue.main.async {
                    imageView.image = cacheImage;
                }
            }else{
                SDWebImageManager.shared().imageDownloader?.downloadImage(with: URL(string: url), options: .continueInBackground, progress: nil, completed: {[weak self] (image:UIImage?, data:Data?, error:Error?, finished:Bool) in
                    if image != nil {
                        
                        let newImage = self?.resizeImage(image: image!, newWidth: 300)
                        DispatchQueue.main.async {
                            imageView.image = newImage
                        }
                        SDImageCache.shared().store(newImage, forKey: url, completion: nil)
                        
                        
                    }
                })
            }
            
            /////////////// Downsampling //////////////
            
            addLongPressToCell(view: cell.view1, elemIndex: elemIndex, isVideoCheck: false)
            //////
            addMultipleItemIconCell2(cell: cell, mediaCount: mediaCount, subView: cell.view1, isRequired: true)
            /////
            cell.view1.addSubview(imageView)
        }
        
    }
    
    
    @objc func playerItemDidReachEnd(notification: NSNotification) {
        let player = notification.object as! AVPlayer
        let item = player.currentItem!
        item.seek(to: CMTime.zero, completionHandler: nil)
    }
    
    
    func clearSubviewsCell1(cell:GridViewCell1,viewIndex:Int){
        let newViewIndex = (viewIndex - 1) % totalViews
        
        switch newViewIndex{
        case 0:
            cell.view1.subviews.forEach({$0.removeFromSuperview()})
            break
        case 1:
            cell.view2.subviews.forEach({$0.removeFromSuperview()})
            break
        case 2:
            cell.view3.subviews.forEach({$0.removeFromSuperview()})
            break
        case 3:
            cell.view4.subviews.forEach({$0.removeFromSuperview()})
            break
        case 4:
            cell.view5.subviews.forEach({$0.removeFromSuperview()})
            break
        case 5:
            cell.view6.subviews.forEach({$0.removeFromSuperview()})
            break
        case 6:
            cell.view7.subviews.forEach({$0.removeFromSuperview()})
            break
        case 7:
            cell.view8.subviews.forEach({$0.removeFromSuperview()})
            break
        case 8:
            cell.view9.subviews.forEach({$0.removeFromSuperview()})
            break
        default:
            break
        }
    }
    
    
    func clearSubviewsCell2(cell:GridViewCell2,viewIndex:Int){
        let newViewIndex = (viewIndex - 1) % totalViews
        
        switch newViewIndex{
        case 0:
            cell.view1.subviews.forEach({$0.removeFromSuperview()})
            break
        case 1:
            cell.view2.subviews.forEach({$0.removeFromSuperview()})
            break
        case 2:
            cell.view3.subviews.forEach({$0.removeFromSuperview()})
            break
        case 3:
            cell.view4.subviews.forEach({$0.removeFromSuperview()})
            break
        case 4:
            cell.view5.subviews.forEach({$0.removeFromSuperview()})
            break
        case 5:
            cell.view6.subviews.forEach({$0.removeFromSuperview()})
            break
        case 6:
            cell.view7.subviews.forEach({$0.removeFromSuperview()})
            break
        case 7:
            cell.view8.subviews.forEach({$0.removeFromSuperview()})
            break
        case 8:
            cell.view9.subviews.forEach({$0.removeFromSuperview()})
            break
        default:
            break
        }
    }
    
    /**
     isImage : true if data from image array to be use.
     isVideoThumbnail : true if data from video array to be use for showing only video thumbnail
     */
    func showImageVideoThumbnailCell1(cell:GridViewCell1,rowIndex:Int,isImageArr:Bool,viewIndex:Int,isVideoThumbnail:Bool = false){
        
        var url = ""
        var mediaCount = 0
        var subview = UIView()
        var isVideoCheck = false
        var showVideoIcon = false
        
        let elemIndex = (viewIndex - 1)
        
        if !gridData.data.indices.contains(elemIndex){
            clearSubviewsCell1(cell: cell, viewIndex: viewIndex)
            return
        }
        
        if !gridData.data[elemIndex].Images.isEmpty{
            url = gridData.data[elemIndex].Images[0].TrendImage ?? ""
            mediaCount = gridData.data[elemIndex].Images.count
            isVideoCheck = false
        }else if !gridData.data[elemIndex].Video.isEmpty{
            url = gridData.data[elemIndex].Video[0].TrendThumbnail ?? ""
            mediaCount = gridData.data[elemIndex].Video.count
            isVideoCheck = true
            showVideoIcon = true
        }
        let newViewIndex = (viewIndex - 1) % totalViews
        
        switch newViewIndex{
        case 0:
            configureViewForCell(view: cell.view1, tag: ViewTag.view1Tag.rawValue, url: url, elemIndex: elemIndex, isVideoCheck: isVideoCheck)
            subview = cell.view1
            break
        case 1:
            configureViewForCell(view: cell.view2, tag: ViewTag.view2Tag.rawValue, url: url, elemIndex: elemIndex, isVideoCheck: isVideoCheck)
            subview = cell.view2
            break
        case 2:
            configureViewForCell(view: cell.view3, tag: ViewTag.view3Tag.rawValue, url: url, elemIndex: elemIndex, isVideoCheck: isVideoCheck)
            subview = cell.view3
            break
        case 3:
            configureViewForCell(view: cell.view4, tag: ViewTag.view4Tag.rawValue, url: url, elemIndex: elemIndex, isVideoCheck: isVideoCheck)
            subview = cell.view4
            break
        case 4:
            configureViewForCell(view: cell.view5, tag: ViewTag.view5Tag.rawValue, url: url, elemIndex: elemIndex, isVideoCheck: isVideoCheck)
            subview = cell.view5
            break
        case 5:
            configureViewForCell(view: cell.view6, tag: ViewTag.view6Tag.rawValue, url: url, elemIndex: elemIndex, isVideoCheck: isVideoCheck)
            subview = cell.view6
            break
        case 6:
            configureViewForCell(view: cell.view7, tag: ViewTag.view7Tag.rawValue, url: url, elemIndex: elemIndex, isVideoCheck: isVideoCheck)
            subview = cell.view7
            break
        case 7:
            configureViewForCell(view: cell.view8, tag: ViewTag.view8Tag.rawValue, url: url, elemIndex: elemIndex, isVideoCheck: isVideoCheck)
            subview = cell.view8
            break
        case 8:
            configureViewForCell(view: cell.view9, tag: ViewTag.view9Tag.rawValue, url: url, elemIndex: elemIndex, isVideoCheck: isVideoCheck)
            subview = cell.view9
            break
        
        default:
            break
        }
        //////
//        addMultipleItemIconCell1(cell: cell, mediaCount: mediaCount, subView: subview, isRequired: true)
        /////
       
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func showImageVideoThumbnailCell2(cell:GridViewCell2,rowIndex:Int,isImageArr:Bool,viewIndex:Int,isVideoThumbnail:Bool = false){
        
        var url = ""
        var mediaCount = 0
        var subview = UIView()
        var isVideoCheck = false
        var showVideoIcon = false
        
        let elemIndex = (viewIndex - 1)
        
        if !gridData.data.indices.contains(elemIndex){
            clearSubviewsCell2(cell: cell, viewIndex: viewIndex)
            return
        }
        
        if !gridData.data[elemIndex].Images.isEmpty{
            url = gridData.data[elemIndex].Images[0].TrendImage ?? ""
            mediaCount = gridData.data[elemIndex].Images.count
            isVideoCheck = false
            showVideoIcon = false
        }else if !gridData.data[elemIndex].Video.isEmpty{
            url = gridData.data[elemIndex].Video[0].TrendThumbnail ?? ""
            mediaCount = gridData.data[elemIndex].Video.count
            isVideoCheck = true
            showVideoIcon = true
        }
        let newViewIndex = (viewIndex - 1) % totalViews
        
        switch newViewIndex{
        case 0:
            configureViewForCell(view: cell.view1, tag: ViewTag.cell2View1Tag.rawValue, url: url, elemIndex: elemIndex, isVideoCheck: isVideoCheck)
            subview = cell.view1
            break
        case 1:
            configureViewForCell(view: cell.view2, tag: ViewTag.cell2View2Tag.rawValue, url: url, elemIndex: elemIndex, isVideoCheck: isVideoCheck)
            subview = cell.view2
            break
        case 2:
            configureViewForCell(view: cell.view3, tag: ViewTag.cell2View3Tag.rawValue, url: url, elemIndex: elemIndex, isVideoCheck: isVideoCheck)
            subview = cell.view3
            break
        case 3:
            configureViewForCell(view: cell.view4, tag: ViewTag.cell2View4Tag.rawValue, url: url, elemIndex: elemIndex, isVideoCheck: isVideoCheck)
            subview = cell.view4
            break
        case 4:
            configureViewForCell(view: cell.view5, tag: ViewTag.cell2View5Tag.rawValue, url: url, elemIndex: elemIndex, isVideoCheck: isVideoCheck)
            subview = cell.view5
            break
        case 5:
            configureViewForCell(view: cell.view6, tag: ViewTag.cell2View6Tag.rawValue, url: url, elemIndex: elemIndex, isVideoCheck: isVideoCheck)
            subview = cell.view6
            break
        case 6:
            configureViewForCell(view: cell.view7, tag: ViewTag.cell2View7Tag.rawValue, url: url, elemIndex: elemIndex, isVideoCheck: isVideoCheck)
            subview = cell.view7
            break
        case 7:
            configureViewForCell(view: cell.view8, tag: ViewTag.cell2View8Tag.rawValue, url: url, elemIndex: elemIndex, isVideoCheck: isVideoCheck)
            subview = cell.view8
            break
        case 8:
            configureViewForCell(view: cell.view9, tag: ViewTag.cell2View9Tag.rawValue, url: url, elemIndex: elemIndex, isVideoCheck: isVideoCheck)
            subview = cell.view9
            break
            
        default:
            break
        }
        //////
//        addMultipleItemIconCell2(cell: cell, mediaCount: mediaCount, subView: subview, isRequired: true)
        /////
        
    }
    
    
    
    
    func configureViewForCell(view:UIView,tag:Int,url:String,elemIndex:Int,isVideoCheck:Bool){
        if let view = view.viewWithTag(tag){
            view.removeFromSuperview()
        }
        let imageView = UIImageView(frame: view.bounds)
        imageView.contentMode = .scaleAspectFill
        imageView.autoresizingMask = [.flexibleWidth,.flexibleHeight,.flexibleBottomMargin,.flexibleLeftMargin,.flexibleRightMargin,.flexibleTopMargin]
        imageView.tag = tag
//        imageView.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "Img_Blur"), options: [.continueInBackground], completed: nil)
        
        /////////////// Downsampling //////////////
        
        imageView.image =  UIImage(named: "Img_Blur");
        
        if let cacheImage = SDImageCache.shared().imageFromCache(forKey: url){
            DispatchQueue.main.async {
                imageView.image = cacheImage;
            }
        }else{
            SDWebImageManager.shared().imageDownloader?.downloadImage(with: URL(string: url), options: .continueInBackground, progress: nil, completed: {[weak self] (image:UIImage?, data:Data?, error:Error?, finished:Bool) in
                if image != nil {
                    
                    let newImage = self?.resizeImage(image: image!, newWidth: 250)
                    DispatchQueue.main.async {
                        imageView.image = newImage
                    }
                    SDImageCache.shared().store(newImage, forKey: url, completion: nil)
                    
                    
                }
            })
        }
        
        
        
        
        /////////////// Downsampling //////////////
        
        
        
        addLongPressToCell(view: view, elemIndex: elemIndex, isVideoCheck: isVideoCheck)
        view.addSubview(imageView)
        
        if gridData.data[elemIndex].IsCustomAd{ // Custmo Ad Icon
            let videoIcon = UIImageView(image: UIImage(named: "Ads"))
            videoIcon.frame = CGRect(x: (imageView.frame.maxX - 30), y: (imageView.frame.maxY - 25), width: 26, height: 22)
            imageView.addSubview(videoIcon)
        }
        
        if isVideoCheck{
            let videoIcon = UIImageView(image: UIImage(named: "YouTubeIcon"))
//            videoIcon.frame.size.width = 20
//            videoIcon.frame.size.height = 20
//            videoIcon.center = imageView.center
            videoIcon.frame = CGRect(x: (imageView.frame.maxX - 25), y: (5), width: 20, height: 15)
            imageView.addSubview(videoIcon)
            //            let v = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 20));
            //            v.backgroundColor = UIColor.red;
            //            view.addSubview(v)
        }
    }
    
    func playVideo(){
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UIScreen.main.bounds.height * CGFloat(heightForGridCell)
        return (UIScreen.main.bounds.width * 0.75)
    }
}

extension GridViewVC : friendSelectedDelegate,FriendsPopupDelegate,SortViewDelegate {
    func sortParameters(direction: String, title: String) {
        self.sortDirection = direction
        self.sortColumn = title
        if let objHome = self.parent as? HomeViewController
        {
            print("grid sort")
            objHome.direction = direction
            objHome.titleSort = title
        }
        
        reloadGridView(lazyLoading: true)
//        self.titleSort = title
//        self.direction = direction
//        self.trendData.data.removeAll()
//        self.totalTrends = 1
//        self.currentCustomAdCnt = 0
//        self.currentGoogleAdCnt = 0
//        self.showBottomLoader = true
//        self.noOfRecordsPerRequest = 10
//            self.gridData.data = []
//            totalTrendCount = 1
//            getTrendsForGridView(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: false, sortColumn: sortColumn, sortDIrection: sortDirection)
//            self.gridTable.reloadData()
        //        trendTableView.scrollToRow(at: IndexPath(row: 0, section: 0) , at: .top, animated: false)
    }
    
    func friendsPopup(indexShare: Int, trendId: Int, description: String) {
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "FriendListViewController") as! FriendListViewController
        destination.delegate = self
        destination.trendId = trendId
        destination.index = indexShare
        destination.txtdescprition = description
        let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height)
        self.navigationController?.present(pop, animated: true, completion: nil)
    }
    
    func controller(arraySelectedFriends: [TagFriendModel], index: Int, trendId: Int,description: String) {
        // self.selectedShareFriends = arraySelectedFriends
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "ShareInternalViewController") as! ShareInternalViewController
        destination.trendId = trendId
        
        if arraySelectedFriends.count != 0
        {
            for i in 0...arraySelectedFriends.count-1
            {
                if(arraySelectedFriends[i].isSelected)
                {
                    let dict = ["FriendId": arraySelectedFriends[i].id]
                    selectedShareFriends.append(dict as AnyObject)
                }
            }
        }
        destination.selectedShareFriends = selectedShareFriends
        destination.index = index
        destination.txtdiscription = description
        let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height)
        self.navigationController?.present(pop, animated: true, completion: nil)
    }
    
    
}

