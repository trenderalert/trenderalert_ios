//
//  SearchVC.swift
//  TrenderAlert
//
//  Created by HPL on 08/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import AVFoundation

class SearchVC: UIViewController {

    @IBOutlet weak var searchTbl: UITableView!
    @IBOutlet weak var searchTFT: UITextField!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var peopleBtn: UIButton!
    @IBOutlet weak var tagsBtn: UIButton!
    @IBOutlet weak var placesBtn: UIButton!
    
    let friends = [["contest_img_2","John S. Shure","New Berlin"],["contest_img_2","James Anderson ","New Berlin"],
        ["contest_img_2","John S. Shure","New Berlin"],
        ["contest_img_2","John S. Shure","New Berlin"]]
    
    var currentView = 1
   
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        searchTFT.backgroundColor = UIColor.friendsSBGrayColor
        searchBtn.backgroundColor = UIColor.friendsSBGrayColor
        
        highlightPeopleBtn()
        searchTFT.delegate = self
        searchTFT.leftPadding(paddingSize: 15)
        searchTFT.attributedPlaceholder = NSAttributedString(string: MySingleton.shared.selectedLangData.search_people,attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        self.peopleBtn.setTitle(MySingleton.shared.selectedLangData.people, for: .normal)
        self.tagsBtn.setTitle(MySingleton.shared.selectedLangData.tags, for: .normal)
        self.placesBtn.setTitle(MySingleton.shared.selectedLangData.places, for: .normal)
    }
    
    func setupTableView(){
        searchTbl.dataSource = self
        searchTbl.delegate = self
        searchTbl.register(UINib(nibName: SearchPeopleTVC.reuseIdentifier, bundle: nil), forCellReuseIdentifier: SearchPeopleTVC.reuseIdentifier)
    }
    
    @IBAction func togglePeopleTagsPlacesBtn(_ sender: UIButton) {
        changeBtnSelection(btnIdentifier: sender.tag)
    }
    
  
    func changeBtnSelection(btnIdentifier:Int){
        switch btnIdentifier{
        case 1:
            highlightPeopleBtn()
        case 2:
            highlightTagBtn()
        case 3:
            highlightPlacesBtn()
        default:
            highlightPeopleBtn()
        }
    }
    
    func loadFriendView(viewIdentifier:Int){
        switch viewIdentifier {
        case 1:
            self.currentView = 1
        case 2:
            self.currentView = 2
        case 3:
            self.currentView = 3
        default:
            self.currentView = 1
        }
    }
    
    func highlightPeopleBtn(){
        peopleBtn.layer.backgroundColor = UIColor.baseColor.cgColor
        peopleBtn.setTitleColor(UIColor.black, for: .normal)
        
        tagsBtn.layer.backgroundColor = UIColor.white.cgColor
        placesBtn.layer.backgroundColor = UIColor.white.cgColor
        tagsBtn.setTitleColor(UIColor.friendSBGrayFontColor, for: .normal)
        placesBtn.setTitleColor(UIColor.friendSBGrayFontColor, for: .normal)
    }
    func highlightTagBtn(){
        tagsBtn.layer.backgroundColor = UIColor.baseColor.cgColor
        tagsBtn.tintColor = UIColor.black
        tagsBtn.setTitleColor(UIColor.black, for: .normal)
        
        peopleBtn.layer.backgroundColor = UIColor.white.cgColor
        placesBtn.layer.backgroundColor = UIColor.white.cgColor
        peopleBtn.setTitleColor(UIColor.friendSBGrayFontColor, for: .normal)
        placesBtn.setTitleColor(UIColor.friendSBGrayFontColor, for: .normal)
    }
    func highlightPlacesBtn(){
        placesBtn.layer.backgroundColor = UIColor.baseColor.cgColor
        placesBtn.tintColor = UIColor.black
        placesBtn.setTitleColor(UIColor.black, for: .normal)
        
        tagsBtn.layer.backgroundColor = UIColor.white.cgColor
        peopleBtn.layer.backgroundColor = UIColor.white.cgColor
        tagsBtn.setTitleColor(UIColor.friendSBGrayFontColor, for: .normal)
        peopleBtn.setTitleColor(UIColor.friendSBGrayFontColor, for: .normal)
    }
  }

extension SearchVC:UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{
    
    @objc func showZoomedImage(_ sender:UITapGestureRecognizer){
        
        var imageUrl = ""
        
        let zoomImageView = ZoomImageVC(nibName: "ZoomImageVC", bundle: nil)
        zoomImageView.imageUrl = imageUrl
        zoomImageView.defaultImageName  = "contest_img_2"
        self.navigationController?.pushViewController(zoomImageView, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friends.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SearchPeopleTVC.reuseIdentifier, for: indexPath) as! SearchPeopleTVC
        cell.cellImg.image = UIImage(named: friends[indexPath.row][0])
        cell.peopleNameLbl.text = friends[indexPath.row][1]
        cell.subtitleLbl.text = friends[indexPath.row][2]
        cell.cellImg.tag = indexPath.row
        cell.cellImg.isUserInteractionEnabled = true
        let imageTapGesture = UITapGestureRecognizer(target: self, action: #selector(showZoomedImage))
        cell.cellImg.addGestureRecognizer(imageTapGesture)
        cell.setup()
        return cell
    }
    
}
