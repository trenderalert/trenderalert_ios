//
//  GridSearchVC.swift
//  TrenderAlert
//
//  Created by HPL on 23/04/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import DropDown

enum SearchViews:Int{
    case peopleView = 1
    case tagsView = 2
    case placesView = 3
}

class GridSearchVC: UIViewController {

    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var peopleBtn: UIButton!
    @IBOutlet weak var tagsBtn: UIButton!
    @IBOutlet weak var placesBtn: UIButton!
    @IBOutlet weak var searchTbl: UITableView!
    @IBOutlet weak var NoDataLbl: UILabel!
    
    var currentView = SearchViews.peopleView.rawValue // 1 = People View, 2 = Tags View, 3 = Places View
    var friendListObj = FriendList()
    var tagListObj = TagModel()
    
    let noOfRecordPerRequest = 10
    var totalFriendCount = 1
    var totalTagCount = 1
    var totalPlacesCount = 1
    
    var location_arr = [String]()
    var str_latitude = String()
    var str_longitude = String()
    var locationDropdown = DropDown()
    
    var searchText = ""
    
    var isFriendApiRequestInProgress = false
    var isTagRequestInProgress = false
    var isPlaceRequestInProgress = false
    
    
    lazy var refreshControl:UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = UIColor.red
        return refreshControl
    }()
    
    var noOfFriends = 0{
        didSet{
            DispatchQueue.main.async {
                self.searchTbl.reloadData()
            }
        }
    }
    var noOfTags = 0{
        didSet{
            DispatchQueue.main.async {
                self.searchTbl.reloadData()
            }
        }
    }
    var noOfPlaces = 0{
        didSet{
            DispatchQueue.main.async {
                self.searchTbl.reloadData()
            }
        }
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.friendListObj.data = []
        totalFriendCount = 1
        getFriendList(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: true)
        self.searchTbl.reloadData()
        refreshControl.endRefreshing()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchTextField.delegate = self
        searchTextField.addTarget(self, action: #selector(self.textFieldTextChanged(_:)), for: .editingChanged)
        locationDropdown.anchorView = searchTextField
        locationDropdown.bottomOffset = CGPoint(x: 0, y:(locationDropdown.anchorView?.plainView.bounds.height)!)

        dropdownSelection()
        setupNavigationbar()
        setupTableView()
        getFriendList(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: true)
        self.peopleBtn.setTitle(MySingleton.shared.selectedLangData.people, for: .normal)
        self.tagsBtn.setTitle(MySingleton.shared.selectedLangData.tags, for: .normal)
        self.placesBtn.setTitle(MySingleton.shared.selectedLangData.places, for: .normal)
        self.NoDataLbl.text = MySingleton.shared.selectedLangData.no_data_found
//        getTagList(Skip: 0, Take: noOfRecordPerRequest, isLazyLoading: true)
    }
  
    
    func setupTableView(){
        searchTbl.delegate = self
        searchTbl.dataSource = self
        
        searchTbl.tableFooterView = UIView()
        searchTbl.register(UINib(nibName: SearchPeopleTVC.reuseIdentifier, bundle: nil), forCellReuseIdentifier: SearchPeopleTVC.reuseIdentifier)
    }
    
    func setupNavigationbar(){
     Utilities.setNavigationBarWithTitle(viewController: self, title: "Search")
        let leftButton = UIBarButtonItem(image: UIImage(named: "img_BackArrow"), style: .plain, target: self, action: #selector(goBack))
        navigationItem.leftBarButtonItem = leftButton
    }
    
    @objc func goBack(){
        self.navigationController?.popViewController(animated: false)
    }
    
    func getFriendList(Skip: Int, Take: Int, lazyLoading: Bool,SearchString:String = ""){
        
        if (friendListObj.data.count >=  totalFriendCount) || isFriendApiRequestInProgress{
            return
        }
        
        self.isFriendApiRequestInProgress = true
        
        let param = ["Skip":Skip,"Take":Take,"Search":searchText] as [String : Any]
        
        WebServices().callUserService(service: UserServices.getPeopleList, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: lazyLoading, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    let temp = WebServices().decodeDataToClass(data:serviceData, decodeClass: FriendList.self)!
                    self.friendListObj.data += temp.data
                    self.totalFriendCount = serviceResponse["TotalRecords"] as! Int
                    self.noOfFriends += 1
                    self.isFriendApiRequestInProgress = false
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func getTagList(Skip:Int,Take:Int,isLazyLoading:Bool,SearchString:String = ""){
       
        if (tagListObj.data.count >=  totalTagCount) || isTagRequestInProgress{
            return
        }
        
      
        self.isTagRequestInProgress = true
        let param = ["Skip":Skip,"Take":Take,"Search":searchText] as [String : Any]
        
        WebServices().callUserService(service: UserServices.getTagByName, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: isLazyLoading, isHeader: true, CallMethod: .post){
            (serviceResponse,serviceData) in
            
            if serviceResponse["Status"] != nil,(serviceResponse["Status"] as! Bool == true){
                let temp = WebServices().decodeDataToClass(data:serviceData, decodeClass: TagModel.self)!
                self.tagListObj.data += temp.data
                self.totalTagCount = serviceResponse["TotalRecords"] as! Int
                self.noOfTags += 1
                self.isTagRequestInProgress = false
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func getLocationList(Skip:Int,Take:Int,isLazyLoading:Bool,SearchString:String = ""){
        
    }
    
    
    @IBAction func toggleViews(_ sender: UIButton) {
        self.searchTextField.resignFirstResponder()
        switch sender.tag{
        case SearchViews.peopleView.rawValue:
            highlightPeopleBtn()
            showPeopleView()
            break
        case SearchViews.tagsView.rawValue:
            highlightTagsBtn()
            
            showTagsView()
            break
        case SearchViews.placesView.rawValue:
            highlightPlacesBtn()
            showPlacesView()
            break
        default:
            highlightPeopleBtn()
            showPeopleView()
            break
        }
    }
    
    func showPeopleView(){
        self.currentView = SearchViews.peopleView.rawValue
        self.searchTextField.placeholder = "\(MySingleton.shared.selectedLangData.search) \(MySingleton.shared.selectedLangData.people)"
        self.friendListObj.data = []
        self.noOfFriends = 0
        self.totalFriendCount = 1
        getFriendList(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: true)
//        self.searchTbl.reloadData()
    }
    
    func showTagsView(){
        self.currentView = SearchViews.tagsView.rawValue
        self.searchTextField.placeholder = "\(MySingleton.shared.selectedLangData.search) \(MySingleton.shared.selectedLangData.tags)"
        self.tagListObj.data = []
        self.noOfTags = 0
        self.totalTagCount = 1
        
        getTagList(Skip: 0, Take: noOfRecordPerRequest, isLazyLoading: true)
//        self.searchTbl.reloadData()
    }
    
    func showPlacesView(){
        self.currentView = SearchViews.placesView.rawValue
        self.searchTextField.placeholder = "\(MySingleton.shared.selectedLangData.search) \(MySingleton.shared.selectedLangData.places)"
        self.searchTbl.reloadData()
        self.searchForAddress()
    }
    
    func highlightPeopleBtn(){
        peopleBtn.layer.backgroundColor = UIColor.baseColor.cgColor
        peopleBtn.setTitleColor(UIColor.black, for: .normal)
        
        tagsBtn.layer.backgroundColor = UIColor.white.cgColor
        tagsBtn.setTitleColor(UIColor.darkGray, for: .normal)
        
        placesBtn.layer.backgroundColor = UIColor.white.cgColor
        placesBtn.setTitleColor(UIColor.darkGray, for: .normal)
        
    }
    func highlightTagsBtn(){
        tagsBtn.layer.backgroundColor = UIColor.baseColor.cgColor
        tagsBtn.setTitleColor(UIColor.black, for: .normal)
        
        peopleBtn.layer.backgroundColor = UIColor.white.cgColor
        peopleBtn.setTitleColor(UIColor.darkGray, for: .normal)
        
        placesBtn.layer.backgroundColor = UIColor.white.cgColor
        placesBtn.setTitleColor(UIColor.darkGray, for: .normal)
    }
    func highlightPlacesBtn(){
        placesBtn.layer.backgroundColor = UIColor.baseColor.cgColor
        placesBtn.setTitleColor(UIColor.black, for: .normal)
        
        tagsBtn.layer.backgroundColor = UIColor.white.cgColor
        tagsBtn.setTitleColor(UIColor.darkGray, for: .normal)
        
        peopleBtn.layer.backgroundColor = UIColor.white.cgColor
        peopleBtn.setTitleColor(UIColor.darkGray, for: .normal)
    }
    
 }

extension GridSearchVC:UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if currentView == SearchViews.peopleView.rawValue{
            if self.friendListObj.data.isEmpty{
                self.NoDataLbl.isHidden = false
            }else{
                self.NoDataLbl.isHidden = true
            }
            return self.friendListObj.data.count
        }else if currentView == SearchViews.tagsView.rawValue{
            if self.tagListObj.data.isEmpty{
                self.NoDataLbl.isHidden = false
            }else{
                self.NoDataLbl.isHidden = true
            }
            return self.tagListObj.data.count
        }else{
            if self.location_arr.isEmpty{
                self.NoDataLbl.isHidden = false
            }else{
                self.NoDataLbl.isHidden = true
            }
         return self.location_arr.count
        }
      }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if currentView == SearchViews.peopleView.rawValue{
            if indexPath.row == (friendListObj.data.count - 1){
                getFriendList(Skip: friendListObj.data.count, Take: noOfRecordPerRequest, lazyLoading: true)
            }
        }else if currentView == SearchViews.tagsView.rawValue{
            if indexPath.row == (tagListObj.data.count - 1){
                getTagList(Skip: tagListObj.data.count, Take: noOfRecordPerRequest, isLazyLoading: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if currentView == SearchViews.peopleView.rawValue{ // People View
            let cell = tableView.dequeueReusableCell(withIdentifier: SearchPeopleTVC.reuseIdentifier, for: indexPath) as! SearchPeopleTVC
            let imageTapGesture = UITapGestureRecognizer(target: self, action: #selector(showZoomedImage))
            cell.peopleNameLbl.text = "\(self.friendListObj.data[indexPath.row].Username ?? "")"
            cell.subtitleLbl.text = "\(self.friendListObj.data[indexPath.row].Firstname ?? "") \(self.friendListObj.data[indexPath.row].Lastname ?? "")"
            cell.cellImg.sd_setImage(with: URL(string: self.friendListObj.data[indexPath.row].ProfileImage ?? ""), placeholderImage: UIImage(named: "img_selectProfilePic"), options: [.continueInBackground], completed: nil)
            cell.cellImg.tag = indexPath.row
            cell.cellImg.isUserInteractionEnabled = true
            cell.cellImg.addGestureRecognizer(imageTapGesture)
            DispatchQueue.main.async {
                cell.setup()
            }
            return cell
        }else if currentView == SearchViews.tagsView.rawValue{
//
                //{ // Tags View
            let cell = UITableViewCell(style: .default, reuseIdentifier: "TagViewCell")
            cell.textLabel?.text = "\(self.tagListObj.data[indexPath.row].TagName ?? "")"
            cell.selectionStyle = .none
            return cell
        }else{ // Places View
            let cell = UITableViewCell(style:.default,reuseIdentifier:"PlacesCell")
            cell.textLabel?.text = "\(self.location_arr[indexPath.row])"
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var profileId = 0
        if currentView == SearchViews.peopleView.rawValue{ //  People View
            profileId = self.friendListObj.data[indexPath.row].UserProfileID
            viewProfile(profileId: profileId)
        }else if currentView == SearchViews.tagsView.rawValue{ // Tags View
            if let tagId = self.tagListObj.data[indexPath.row].TagsId{
                let storyboard = UIStoryboard(name: "SearchSB", bundle: nil)
                let dest = storyboard.instantiateViewController(withIdentifier: "GridViewVC") as! GridViewVC
                dest.showGridForTagId = true
                dest.TagId = tagId
                self.navigationController?.pushViewController(dest, animated: true)
            }
        }else{

            GetAddress().getLocationFromAddressString(addressStr: self.location_arr[indexPath.row]) { (clLocationCoordinate2D,zipcode) in
                let storyboard = UIStoryboard(name: "SearchSB", bundle: nil)
                let dest = storyboard.instantiateViewController(withIdentifier: "GridViewForPlaces") as! GridViewForPlaces
                dest.lat = String(clLocationCoordinate2D.latitude)
                dest.long = String(clLocationCoordinate2D.longitude)
                dest.searchString = self.location_arr[indexPath.row]
                self.navigationController?.pushViewController(dest, animated: false)
            }
        }
    }
    
    func viewProfile(profileId:Int){
        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
        let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
        profileVC.userId = profileId
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    @objc func showZoomedImage(_ sender:UITapGestureRecognizer){
        var imageUrl = ""
        imageUrl = friendListObj.data[(sender.view?.tag)!].ProfileImage ?? ""
        let zoomImageView = ZoomImageVC(nibName: "ZoomImageVC", bundle: nil)
        zoomImageView.imageUrl = imageUrl
        zoomImageView.defaultImageName = "img_selectProfilePic"
        self.navigationController?.pushViewController(zoomImageView, animated: true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        searchText = updatedString!
        if currentView == SearchViews.peopleView.rawValue{
            self.friendListObj.data = []
            self.noOfFriends = 0
            self.totalFriendCount = 1
            getFriendList(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: true, SearchString: updatedString!)
        }else if currentView == SearchViews.tagsView.rawValue{
            self.tagListObj.data = []
            self.noOfTags = 0
            self.totalTagCount = 1
            getTagList(Skip: 0, Take: noOfRecordPerRequest, isLazyLoading: true,SearchString: updatedString!)
        }
        return true
    }
    
    
    @objc func textFieldTextChanged(_ sender : UITextField)
    {
        if currentView == SearchViews.placesView.rawValue{
            if  self.searchTextField.text!.isEmpty
            {
                self.location_arr = []
                self.searchTbl.reloadData()
            }
            else
            {
                self.searchForAddress()
            }
        }
    }
    
    func searchForAddress(){
        let getAddress = GetAddress()
        getAddress.getAddress(location_str: self.searchTextField.text!) { (locationData) in
            self.location_arr = []
            for location in locationData
            {
                self.location_arr.append(location["address"]!)
            }
            self.locationDropdown.dataSource = self.location_arr
            if !self.searchTextField.text!.isEmpty
            {
                self.searchTbl.reloadData()
            }
        }
    }
    
    
    func dropdownSelection()
    {
        locationDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.searchTextField.text! = item
            GetAddress().getLocationFromAddressString(addressStr: self.searchTextField.text!) { (clLocationCoordinate2D,ZipCode) in
                self.str_latitude  = String(clLocationCoordinate2D.latitude)
                self.str_longitude = String(clLocationCoordinate2D.longitude)
            }
        }
    }
    
}

