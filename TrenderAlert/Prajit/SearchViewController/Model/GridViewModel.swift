//
//  GridViewModel.swift
//  TrenderAlert
//
//  Created by HPL on 01/04/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

class GridViewModel:NSObject,Codable{
    var data = [GridViewTrend]()
    enum CodingKeys:String,CodingKey{
        case data = "data"
    }
}

class GridViewTrend:NSObject,Codable{
    var TrendID:Int?
    var Title:String?
    var IsLiked:Bool = false
    var UserProfileId:Int?
    var IsCustomAd:Bool = false;
    var AdUrl:String?
    
    var Images = [TrendImages]()
    var Video = [TrendVideos]()
    
    enum CodingKeys:String,CodingKey{
        case Images = "TrendImages"
        case Video = "TrendVideos"
        case TrendID = "TrendID"
        case Title = "Title"
        case IsLiked = "IsLiked"
        case UserProfileId = "UserprofileId"
        case IsCustomAd = "IsCustomAd"
        case AdUrl = "AdUrl"
    }
}




class TrendImages:NSObject,Codable{
    
    var TrendID:Int?
    var TrendImage:String?
    var UserProfileId:Int?
    var FirstName:String?
    var LastName:String?
    var ProfileImage:String?
    
    enum CodingKeys:String,CodingKey{
       case TrendID = "TrendID"
       case TrendImage = "TrendImage"
       case UserProfileId = "UserProfileId"
       case FirstName = "FirstName"
       case LastName = "LastName"
       case ProfileImage = "ProfileImage"
    }
}


class TrendVideos:NSObject,Codable{
    
    var TrendID:Int?
    var VideoUrl:String?
    var UserProfileId:Int?
    var FirstName:String?
    var LastName:String?
    var ProfileImage:String?
    var TrendThumbnail:String?
    
    enum CodingKeys:String,CodingKey{
        
        case TrendID = "TrendID"
        case VideoUrl = "VideoUrl"
        case UserProfileId = "UserProfileId"
        case FirstName = "FirstName"
        case LastName = "LastName"
        case ProfileImage = "ProfileImage"
        case TrendThumbnail = "TrendThumbnail"
    }
}
