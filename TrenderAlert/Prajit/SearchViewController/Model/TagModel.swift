//
//  TagModel.swift
//  TrenderAlert
//
//  Created by HPL on 24/04/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

class TagModel:NSObject,Codable{
    var data = [TagData]()
    enum CodingKeys:String,CodingKey{
        case data = "data"
    }
}

class TagData:NSObject,Codable{

    var TagsId:Int?
    var TagName:String?
    var UserProfileTagMappingId:Int?
    
    enum CodingKeys:String,CodingKey{
        case TagsId = "TagsId"
        case TagName = "TagName"
        case UserProfileTagMappingId = "UserProfileTagMappingId"
    }
}
