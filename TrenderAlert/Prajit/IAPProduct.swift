//
//  IAPProducts.swift
//  TrenderAlert
//
//  Created by HPL on 12/03/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

enum IAPProduct:String{
    case trenderAlertPremiumNonConsumable = "com.app.TrenderAlert.TrenderAlertPremium"
    case trenderAlertPremiumConsumable    = "com.app.TrenderAlert.PremiumConsumable"
}
