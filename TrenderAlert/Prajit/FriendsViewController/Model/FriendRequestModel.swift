//
//  FriendRequestModel.swift
//  TrenderAlert
//
//  Created by HPL on 13/02/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class FriendRequestListData:NSObject, Codable{
    
    var Username:String?
    var Firstname:String?
    var Lastname:String?
    var EmailID:String?
    var ProfileImage:String?
    var RequestID:Int?
    var RequestFromUserID:Int?
    var Location:String?
    
    enum CodingKeys:String,CodingKey{
        case Username  = "Username"
        case Firstname = "Firstname"
        case Lastname  = "Lastname"
        case EmailID   = "EmailID"
        case ProfileImage = "ProfileImage"
        case RequestID = "RequestID"
        case RequestFromUserID = "RequestFromUserID"
        case Location = "Location"
    }
}

class FriendRequestList:NSObject,Codable{
    var data = [FriendRequestListData]()
    
    enum CodingKeys:String,CodingKey{
        case data = "data"
    }
}

