//
//  FriendSuggestionListModel.swift
//  TrenderAlert
//
//  Created by HPL on 13/02/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class FriendSuggestionListData:NSObject, Codable{
    
    var Username:String?
    var Firstname:String?
    var Lastname:String?
    var EmailID:String?
    var ProfileImage:String?
    var SuggestionID:Int?
    var UserProfileID:Int?
    var Location:String?
    
    enum CodingKeys:String,CodingKey{
        case Username  = "Username"
        case Firstname = "Firstname"
        case Lastname  = "Lastname"
        case EmailID   = "EmailID"
        case ProfileImage = "ProfileImage"
        case SuggestionID = "SuggestionID"
        case UserProfileID = "UserProfileID"
        case Location = "Location"
    }
}

class FriendSuggestionList:NSObject,Codable{
    var data = [FriendSuggestionListData]()
    
    enum CodingKeys:String,CodingKey{
        case data = "data"
    }
}

