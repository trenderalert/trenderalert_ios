//
//  FriendSuggestionTVC.swift
//  TrenderAlert
//
//  Created by HPL on 07/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

struct FriendSuggestionTVCData{
    var userId:Int
    var suggestionId:Int
    
    init(userId:Int,suggestionId:Int){
        self.userId = userId
        self.suggestionId = suggestionId
    }
}


protocol FriendSuggestionTVCDelegate:class{
    func sendFriendRequest(userId:Int,index:Int)
    func removeFriendSuggestion(suggestionId:Int,index:Int)
}

class FriendSuggestionTVC: UITableViewCell {

    @IBOutlet weak var friendImg: UIImageView!
    @IBOutlet weak var friendNameLbl: UILabel!
    @IBOutlet weak var friendCityLbl: UILabel!
    @IBOutlet weak var addFriendBtn: UIButton!
    @IBOutlet weak var removeFriendBtn: UIButton!
    
    
    static let reuseIdentifier = "FriendSuggestionTVC"
    
    var delegate:FriendSuggestionTVCDelegate?
    var requData:FriendSuggestionTVCData?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addFriendBtn.addTarget(self, action: #selector(sendFriendRequest), for: .touchUpInside)
        removeFriendBtn.addTarget(self, action: #selector(removeFriendSuggestion), for: .touchUpInside)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(){
        friendImg.makeCornerRadius(radius: friendImg.frame.width/2)
        addFriendBtn.layer.backgroundColor = UIColor.baseColor.cgColor
        addFriendBtn.makeCornerRadius(radius: 2)
        
        removeFriendBtn.layer.borderWidth = 1
        removeFriendBtn.layer.borderColor = UIColor.friendsSBGrayColor.cgColor
        removeFriendBtn.makeCornerRadius(radius: 2)
    }
    
    @objc func sendFriendRequest(){
        guard let reqData = self.requData,self.requData?.userId != 0 else{
                return
        }
        delegate?.sendFriendRequest(userId: reqData.userId, index: self.addFriendBtn.tag)
    }
    
    @objc func removeFriendSuggestion(){
        
        guard let reqData = self.requData,self.requData?.suggestionId != 0 else{
            return
        }
        delegate?.removeFriendSuggestion(suggestionId: reqData.suggestionId, index: self.removeFriendBtn.tag)
    }
}
