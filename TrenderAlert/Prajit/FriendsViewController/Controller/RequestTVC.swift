//
//  RequestTVC.swift
//  TrenderAlert
//
//  Created by HPL on 04/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

struct FriendRequestTVCData{
    var requestId:Int
    var requsetFrom:Int
    
    init(requId:Int,requFrom:Int){
        self.requestId = requId
        self.requsetFrom = requFrom
    }
}


protocol RequestTVCDelegate:class{
    func acceptFriendRequest(reqId:Int,reqFrom:Int,index:Int)
    func declineFriendRequest(reqId:Int,reqFrom:Int,index:Int)
}

class RequestTVC: UITableViewCell {
    
    @IBOutlet weak var friendImg: UIImageView!
    @IBOutlet weak var friendNameLbl: UILabel!
    @IBOutlet weak var friendCityLbl: UILabel!
    @IBOutlet weak var acceptBtn: UIButton!
    @IBOutlet weak var declineBtn: UIButton!
    
    @IBOutlet weak var acceptBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var declineBtnHeight: NSLayoutConstraint!
    
    static let resuseIdentifier = "RequestTVC"
    
    var delegate:RequestTVCDelegate?
    var requData:FriendRequestTVCData?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        acceptBtn.addTarget(self, action: #selector(acceptFriendReq), for: .touchUpInside)
        declineBtn.addTarget(self, action: #selector(declineFriendReq), for: .touchUpInside)

//        setup()
    }
    
    func setup(){
        friendImg.makeCornerRadius(radius: friendImg.frame.width/2)
        
//        friendImg.makeCornerRadius(radius: friendImg.frame.width/2)
//        acceptBtn.layer.borderWidth = 0.5
        acceptBtn.layer.backgroundColor = UIColor.baseColor.cgColor
        acceptBtn.makeCornerRadius(radius: 2)
        
//        declineBtn.layer.borderWidth = 1
        declineBtn.layer.borderColor = UIColor.friendsSBGrayColor.cgColor
        declineBtn.makeCornerRadius(radius: 2)
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @objc func acceptFriendReq(){
        guard let reqData = self.requData,self.requData?.requestId != 0,self.requData?.requsetFrom != 0 else {
            return
        }
        delegate?.acceptFriendRequest(reqId: reqData.requestId, reqFrom: reqData.requsetFrom, index: self.acceptBtn.tag)
    }
    
    @objc func declineFriendReq(){
        guard let reqData = self.requData,self.requData?.requestId != 0,self.requData?.requsetFrom != 0 else {
            return
        }
        delegate?.declineFriendRequest(reqId: reqData.requestId, reqFrom: reqData.requsetFrom, index: self.declineBtn.tag)
    }
    
    func hideAcceptDeclineBtn(){
        hideAcceptBtn()
        hideDeclineBtn()
     }
    
    func hideAcceptBtn(){
        acceptBtnHeight.priority = UILayoutPriority(rawValue: 1000)
        acceptBtnHeight.constant = 0
    }
    
    func hideDeclineBtn(){
        declineBtnHeight.priority = UILayoutPriority(rawValue: 1000)
        declineBtnHeight.constant = 0
    }
    
}
