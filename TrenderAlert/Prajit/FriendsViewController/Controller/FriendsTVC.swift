//
//  FriendsTVC.swift
//  TrenderAlert
//
//  Created by HPL on 04/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class FriendsTVC: UITableViewCell {

    @IBOutlet weak var friendImage: UIImageView!
    @IBOutlet weak var friendNameLbl: UILabel!
    @IBOutlet weak var msgBtn: UIButton!
    @IBOutlet weak var menuBtn: UIButton!
    
    
    @IBOutlet weak var friendNameLblTrailingSpace: NSLayoutConstraint!
    @IBOutlet weak var msgBtnWidth: NSLayoutConstraint!
    @IBOutlet weak var optionBtnWidth: NSLayoutConstraint!
    
    static let reuseIdentifier = "FriendsTVC"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(){
        msgBtn.makeCornerRadius(radius: 4)
        msgBtn.layer.borderColor = UIColor.baseColor.cgColor
        msgBtn.layer.borderWidth = 0.5
        self.friendImage.makeCornerRadius(radius: self.friendImage.frame.width/2)
    }
    
    func removeMsgAndOptionBtn(){
        friendNameLblTrailingSpace.constant = 0
        msgBtnWidth.priority = UILayoutPriority(rawValue: 1000)
        msgBtnWidth.constant = 0
        optionBtnWidth.constant = 0
        msgBtn.isHidden = true
        menuBtn.isHidden = true
    }
    
}
