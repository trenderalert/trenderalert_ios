//
//  FriendsViewHeader.swift
//  TrenderAlert
//
//  Created by HPL on 07/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class FriendsViewHeader: UITableViewHeaderFooterView {

    @IBOutlet weak var searchUTF: UITextField!
    @IBOutlet weak var searchBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        searchUTF.leftPadding(paddingSize: 15)
        searchUTF.layer.backgroundColor = UIColor.friendsSBGrayColor.cgColor
        searchBtn.layer.backgroundColor = UIColor.friendsSBGrayColor.cgColor
    }

}
