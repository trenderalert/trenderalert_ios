//
//  FriendsVC.swift
//  TrenderAlert
//
//  Created by HPL on 04/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import SDWebImage
import MFSideMenu
import DropDown


class FriendsVC: UIViewController {
    
    @IBOutlet weak var friendsTbl: UITableView!
    @IBOutlet weak var friendsBtn: UIButton!
    @IBOutlet weak var requestBtn: UIButton!
    @IBOutlet weak var friendSuggestionBtn: UIButton!
    @IBOutlet weak var friendBtnHeight: NSLayoutConstraint!
    @IBOutlet weak var friendsBtnEqualHeight: NSLayoutConstraint!
    
    @IBOutlet weak var noDataLbl: UILabel!
    
    var headerSearchViewHeight = (UIDevice.current.userInterfaceIdiom == .pad) ? 60.0 : 40.0
    
    var currentView = 1
    
    var friendViewRef:FriendsViewHeader?
    
    var friendListObj = FriendList()
    //    var friendRequestListObj = FriendRequestList()
    var friendRequestListObj = FriendList()
    var friendSuggestionListObj = FriendSuggestionList()
    
    let noOfRecordPerRequest = 10
    var totalFriendCount = 1
    var totalFriendRequestCount = 1
    var totalFriendSuggestionCount = 1
    
    var isFriendSearchOn:Bool = false
    var searchText = ""
    
    //View
    let friendView = 1
    let friendRequestView = 2
    let friendSuggestioView = 3
    
    var isFriendApiRequestInProgress = false
    var isFriendSuggRequestInProgress = false
    var isFriendRequestInProgress = false
    
    var backToSideMenu = false
    
    var OtherUserProfileID = 0
    
    
    lazy var refreshControl:UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = UIColor.red
        return refreshControl
    }()
    
    
    var noOfFriends = 0{
        didSet{
            if currentView == friendView{
                DispatchQueue.main.async {
                    //                    self.friendsTbl.reloadData()
                }
            }
        }
    }
    
    var noOfFriendSuggestions = 0{
        didSet{
            if currentView == friendSuggestioView{
                DispatchQueue.main.async {
                    //                    self.friendsTbl.reloadData()
                }
            }
        }
    }
    
    var noOfFriendRequests = 0{
        didSet{
            if currentView == friendRequestView{
                DispatchQueue.main.async {
                    //                    self.friendsTbl.reloadData()
                }
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationbar()
        setupTableView()
        setButtons()
        checkForOtherUsersFriends()
    }
    
    func checkForOtherUsersFriends(){
        if OtherUserProfileID != 0{ // Other user friend
            
            self.friendBtnHeight.constant = 0
            self.friendBtnHeight.isActive = true
            self.friendBtnHeight.priority = UILayoutPriority(rawValue: 1000)
            friendsBtnEqualHeight.isActive = false
            
            
            self.friendsBtn.isHidden = true
            self.requestBtn.isHidden = true
            self.friendSuggestionBtn.isHidden = true
            getFriendList(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: false)
        }else{
            getFriendList(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: false)
            getFriendRequestList(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: false)
            getFriendSuggestionList(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: false)
        }
    }
    
    func setButtons(){
        self.noDataLbl.text = MySingleton.shared.selectedLangData.no_data_available
        self.friendsBtn.setTitle(MySingleton.shared.selectedLangData.friends, for: .normal)
        self.requestBtn.setTitle(MySingleton.shared.selectedLangData.friend_request, for: .normal)
        self.friendSuggestionBtn.setTitle(MySingleton.shared.selectedLangData.friend_suggestions, for: .normal)
        //        friendsBtn.addBottomBorderWithColor(color: UIColor.lightGray, width: 2)
        //        friendsBtn.addLeftBorderWithColor(color:UIColor.lightGray, width: 2)
        //
        //        friendsBtn.layer.shadowOpacity = 1
        //        friendsBtn.layer.shadowColor = UIColor.lightGray.cgColor
        //        friendsBtn.layer.shadowRadius = 10
        //        friendsBtn.layer.shadowOffset = CGSize(width: 0, height: -10)
    }
    
    func setupNavigationbar(){
        
        let leftBarButton = UIButton()
        leftBarButton.setImage(#imageLiteral(resourceName: "img_MenuBarBtn"), for: .normal)
        leftBarButton.addTarget(self, action: #selector(didTapMenu), for: .touchUpInside)
        let item1 = UIBarButtonItem()
        item1.customView = leftBarButton
        self.navigationItem.leftBarButtonItems = [item1]
        
        Utilities.setNavigationBarWithTitle(viewController: self, title: MySingleton.shared.selectedLangData.friends)
        
        if !backToSideMenu{
            let leftButton = UIBarButtonItem(image: UIImage(named: "img_BackArrow"), style: .plain, target: self, action: #selector(goBack))
            navigationItem.leftBarButtonItem = leftButton
        }
        
    }
    @objc func didTapMenu(_ sender: UIBarButtonItem) {
        if self.menuContainerViewController != nil {
            self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
        }
        
    }
    //    @objc func btnmsgAction(_ sender : UIButton)
    //    {
    //        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
    //        let nextViewController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
    //        self.navigationController?.pushViewController(nextViewController, animated: true)
    //    }
    @objc func goBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        switch currentView{
        case friendView:
            self.friendListObj.data = []
            totalFriendCount = 1
            getFriendList(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: true)
        case friendRequestView:
            self.friendRequestListObj.data = []
            totalFriendRequestCount = 1
            getFriendRequestList(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: true)
        case friendSuggestioView:
            self.friendSuggestionListObj.data = []
            totalFriendSuggestionCount = 1
            getFriendSuggestionList(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: true)
        default:
            self.friendListObj.data = []
            totalFriendCount = 1
            getFriendList(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: true)
        }
        self.friendsTbl.reloadData()
        refreshControl.endRefreshing()
    }
    
    
    
    func setupTableView(){
        
        friendsTbl.delegate   = self
        friendsTbl.dataSource = self
        
        friendsTbl.tableFooterView = UIView()
        
        friendsTbl.register(UINib(nibName: FriendsTVC.reuseIdentifier, bundle: nil), forCellReuseIdentifier: FriendsTVC.reuseIdentifier)
        
        friendsTbl.register(UINib(nibName: RequestTVC.resuseIdentifier, bundle: nil), forCellReuseIdentifier: RequestTVC.resuseIdentifier)
        
        friendsTbl.register(UINib(nibName: FriendSuggestionTVC.reuseIdentifier, bundle: nil), forCellReuseIdentifier: FriendSuggestionTVC.reuseIdentifier)
        
        let header = UINib.init(nibName: "FriendsViewHeader", bundle: nil)
        friendsTbl.register(header, forHeaderFooterViewReuseIdentifier: "FriendsViewHeader")
        
        self.friendsTbl.addSubview(self.refreshControl)
    }
    
    @IBAction func toggleFriendRequestSuggestionScreen(_ sender: UIButton) {
        if friendViewRef != nil{
            friendViewRef?.searchUTF.resignFirstResponder()
        }
        changeBtnSelection(btnIdentifier:sender.tag)
        loadFriendView(viewIdentifier: sender.tag)
    }
    
    func changeBtnSelection(btnIdentifier:Int){
        switch btnIdentifier{
        case friendView:
            highlightFriendBtn()
        case friendRequestView:
            highlightRequestBtn()
        case friendSuggestioView:
            highlightFriendSuggestionBtn()
        default:
            highlightFriendBtn()
        }
    }
    
    func loadFriendView(viewIdentifier:Int){
        switch viewIdentifier {
        case friendView:
            self.currentView = friendView // Friend
        case friendRequestView:
            self.currentView = friendRequestView // Request List
        case friendSuggestioView:
            self.currentView = friendSuggestioView // Suggestion
        default:
            self.currentView = friendView // Friend
        }
        self.friendsTbl.reloadData()
    }
    
    func getFriendList(Skip: Int, Take: Int, lazyLoading: Bool,SearchString:String = ""){
        
        if (friendListObj.data.count >=  totalFriendCount) || isFriendApiRequestInProgress{
            return
        }
        
        self.isFriendApiRequestInProgress = true
        
        var param = ["Skip":Skip,"Take":Take,"Search":searchText] as [String : Any]
        
        if OtherUserProfileID != 0{
            param["OtherUserProfileID"] = "\(self.OtherUserProfileID)"
        }
        
        WebServices().callUserService(service: UserServices.getFriendList, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: lazyLoading, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    let temp = WebServices().decodeDataToClass(data:serviceData, decodeClass: FriendList.self)!
                    self.friendListObj.data += temp.data
                    self.totalFriendCount = serviceResponse["TotalRecords"] as! Int
                    self.noOfFriends += 1
                    self.isFriendApiRequestInProgress = false
                    DispatchQueue.main.async{
                        self.friendsTbl.reloadData()
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    
    
    
    func getFriendRequestList(Skip: Int, Take: Int, lazyLoading: Bool){
        
        if (friendRequestListObj.data.count >=  totalFriendRequestCount) || isFriendRequestInProgress{
            return
        }
        self.isFriendRequestInProgress = true
        
        let param = ["Skip":Skip,"Take":Take]
        
        WebServices().callUserService(service: UserServices.getFriendRequestList, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: lazyLoading, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    let temp = WebServices().decodeDataToClass(data:serviceData, decodeClass: FriendList.self)!
                    self.friendRequestListObj.data += temp.data
                    self.totalFriendRequestCount = serviceResponse["TotalRecords"] as! Int
                    self.noOfFriendRequests += 1
                    self.isFriendRequestInProgress = false
                    DispatchQueue.main.async{
                        self.friendsTbl.reloadData()
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func getFriendSuggestionList(Skip:Int,Take:Int,lazyLoading:Bool){
        
        if (friendSuggestionListObj.data.count >=  totalFriendSuggestionCount) || isFriendSuggRequestInProgress{
            return
        }
        self.isFriendSuggRequestInProgress = true
        let params = ["Skip":Skip,"Take":Take]
        
        WebServices().callUserService(service: UserServices.getFriendSuggestionList, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: lazyLoading, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    // self.friendSuggestionListObj = WebServices().decodeDataToClass(data:serviceData, decodeClass: FriendSuggestionList.self)!
                    let temp = WebServices().decodeDataToClass(data:serviceData, decodeClass: FriendSuggestionList.self)!
                    self.friendSuggestionListObj.data += temp.data
                    self.totalFriendSuggestionCount = serviceResponse["TotalRecords"] as! Int
                    self.noOfFriendSuggestions += 1
                    self.isFriendSuggRequestInProgress = false
                    DispatchQueue.main.async{
                        self.friendsTbl.reloadData()
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    
    
    func highlightFriendBtn(){
        friendsBtn.layer.backgroundColor = UIColor.baseColor.cgColor
        friendsBtn.setTitleColor(UIColor.black, for: .normal)
        
        requestBtn.layer.backgroundColor = UIColor.white.cgColor
        friendSuggestionBtn.layer.backgroundColor = UIColor.white.cgColor
        requestBtn.setTitleColor(UIColor.friendSBGrayFontColor, for: .normal)
        friendSuggestionBtn.setTitleColor(UIColor.friendSBGrayFontColor, for: .normal)
        
        Utilities.setNavigationBarWithTitle(viewController: self, title: MySingleton.shared.selectedLangData.friends)
    }
    
    func highlightRequestBtn(){
        requestBtn.layer.backgroundColor = UIColor.baseColor.cgColor
        requestBtn.tintColor = UIColor.black
        requestBtn.setTitleColor(UIColor.black, for: .normal)
        
        friendsBtn.layer.backgroundColor = UIColor.white.cgColor
        friendSuggestionBtn.layer.backgroundColor = UIColor.white.cgColor
        friendsBtn.setTitleColor(UIColor.friendSBGrayFontColor, for: .normal)
        friendSuggestionBtn.setTitleColor(UIColor.friendSBGrayFontColor, for: .normal)
        Utilities.setNavigationBarWithTitle(viewController: self, title: MySingleton.shared.selectedLangData.request)
    }
    
    func highlightFriendSuggestionBtn(){
        friendSuggestionBtn.layer.backgroundColor = UIColor.baseColor.cgColor
        friendSuggestionBtn.tintColor = UIColor.black
        friendSuggestionBtn.setTitleColor(UIColor.black, for: .normal)
        
        requestBtn.layer.backgroundColor = UIColor.white.cgColor
        friendsBtn.layer.backgroundColor = UIColor.white.cgColor
        requestBtn.setTitleColor(UIColor.friendSBGrayFontColor, for: .normal)
        friendsBtn.setTitleColor(UIColor.friendSBGrayFontColor, for: .normal)
        Utilities.setNavigationBarWithTitle(viewController: self, title: MySingleton.shared.selectedLangData.friend_suggestions)
    }
    
}



extension FriendsVC:UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,RequestTVCDelegate,FriendSuggestionTVCDelegate{
    
    @objc func showZoomedImage(_ sender:UITapGestureRecognizer){
        
        var imageUrl = ""
        switch currentView{
        case 1:
            imageUrl = friendListObj.data[(sender.view?.tag)!].ProfileImage ?? ""
        case 2:
            imageUrl = friendRequestListObj.data[(sender.view?.tag)!].ProfileImage ?? ""
        case 3:
            imageUrl = friendSuggestionListObj.data[(sender.view?.tag)!].ProfileImage ?? ""
        default:
            return
        }
        
        let zoomImageView = ZoomImageVC(nibName: "ZoomImageVC", bundle: nil)
        zoomImageView.imageUrl = imageUrl
        self.navigationController?.pushViewController(zoomImageView, animated: true)
    }
    
    
    func sendFriendRequest(userId: Int, index: Int) {
        
        let params = ["UserID":userId]
        
        WebServices().callUserService(service: UserServices.sendFriendRequest, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    self.friendSuggestionListObj.data.remove(at: index)
                    DispatchQueue.main.async {
                        self.friendsTbl.reloadData()
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func removeFriendSuggestion(suggestionId: Int, index: Int) {
        let params = ["SuggestionID":suggestionId]
        
        WebServices().callUserService(service: UserServices.removeFriendSuggestion, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    self.friendSuggestionListObj.data.remove(at: index)
                    DispatchQueue.main.async {
                        self.friendsTbl.reloadData()
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    
    func acceptFriendRequest(reqId: Int, reqFrom: Int,index:Int){
        
        let params = ["RequestID":reqId,"RequestFrom":reqFrom]
        
        WebServices().callUserService(service: UserServices.acceptFriendRequest, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    
                    self.friendListObj.data.insert(self.friendRequestListObj.data[index], at: 0)
                    //                    self.friendListObj.data.insert(newFriend, at: 0)
                    self.totalFriendCount += 1
                    self.totalFriendRequestCount -= 1
                    self.friendRequestListObj.data.remove(at: index)
                    
                    DispatchQueue.main.async {
                        self.friendsTbl.reloadData()
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func declineFriendRequest(reqId: Int, reqFrom: Int,index: Int){
        
        let params = ["RequestID":reqId,"RequestFrom":reqFrom]
        
        WebServices().callUserService(service: UserServices.declineFriendRequest, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    self.friendRequestListObj.data.remove(at: index)
                    DispatchQueue.main.async {
                        self.friendsTbl.reloadData()
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch currentView{
        case 1:
            (self.friendListObj.data.isEmpty && !isFriendApiRequestInProgress && !isFriendSuggRequestInProgress && !isFriendRequestInProgress) ? (noDataLbl.isHidden = false) : (noDataLbl.isHidden = true)
            return self.friendListObj.data.count
        case 2:
            (self.friendRequestListObj.data.isEmpty && !isFriendRequestInProgress) ? (noDataLbl.isHidden = false) : (noDataLbl.isHidden = true)
            return self.friendRequestListObj.data.count
        case 3:
            (self.friendSuggestionListObj.data.isEmpty && !isFriendSuggRequestInProgress) ? (noDataLbl.isHidden = false) : (noDataLbl.isHidden = true)
            return self.friendSuggestionListObj.data.count
        default:
            (self.friendListObj.data.isEmpty) ? (noDataLbl.isHidden = false) : (noDataLbl.isHidden = true)
            return self.friendListObj.data.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch currentView{
        case 1:
            if indexPath.row == (friendListObj.data.count - 1){
                getFriendList(Skip: friendListObj.data.count, Take: noOfRecordPerRequest, lazyLoading: true)
            }
        case 2:
            if indexPath.row == (friendRequestListObj.data.count - 1){
                getFriendRequestList(Skip: friendRequestListObj.data.count, Take: noOfRecordPerRequest, lazyLoading: true)
            }
        case 3:
            if indexPath.row == (friendSuggestionListObj.data.count - 1){
                getFriendSuggestionList(Skip: friendSuggestionListObj.data.count, Take: noOfRecordPerRequest, lazyLoading: true)
            }
        default:
            if indexPath.row == (friendListObj.data.count - 1){
                getFriendList(Skip: friendListObj.data.count, Take: noOfRecordPerRequest, lazyLoading: true)
            }
        }
    }
    
    @objc func showMoreOptions(_ sender:UIButton){
        let options:[String] = [MySingleton.shared.selectedLangData.unfriend,(self.friendListObj.data[sender.tag].HaveYouBlocked) ? MySingleton.shared.selectedLangData.unblock : MySingleton.shared.selectedLangData.block]
        let dropdown = DropDown()
        
        dropdown.anchorView = sender
        
        dropdown.dataSource = options
        dropdown.backgroundColor = UIColor.white
        dropdown.bottomOffset = CGPoint(x: 0, y:(dropdown.anchorView?.plainView.bounds.height)!)
        
        dropdown.selectionAction = {[unowned self] (index:Int,item:String) in
            switch item{
            case options[0]:
                self.unfriend(index:sender.tag)
            case options[1]:
                self.blockFriend(index:sender.tag)
            default:
                return
            }
        }
        
        dropdown.show()
    }
    
    func unfriend(index:Int){
        
        let param = ["UserID":friendListObj.data[index].UserProfileID]
        
        WebServices().callUserService(service: UserServices.unfriend, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    self.friendListObj.data.remove(at: index)
                    self.totalFriendCount -= 1
                    DispatchQueue.main.async{
                        self.friendsTbl.reloadData()
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func blockFriend(index:Int){
        
        let userId = "\(friendListObj.data[index].UserProfileID ?? 0)"
        
        let param:[String:String] = ["UserID":userId]
        
        WebServices().callUserService(service: .blockUnblockUser, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    self.friendListObj.data[index].HaveYouBlocked = !self.friendListObj.data[index].HaveYouBlocked
                    self.friendListObj.data.remove(at: index)
                    self.totalFriendCount -= 1
                    DispatchQueue.main.async{
                        self.friendsTbl.reloadData()
                    }
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    @objc func btnmsgAction(_ sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let nextViewController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        nextViewController.ToUserId = self.friendListObj.data[sender.tag].UserProfileID
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    @objc func showUserProfile(_ sender:UITapGestureRecognizer){
        
        var profileId = 0
        switch currentView{
        case 1:
            profileId = self.friendListObj.data[(sender.view?.tag)!].UserProfileID
            viewProfile(profileId: profileId)
            break
        case 2:
            profileId = self.friendRequestListObj.data[(sender.view?.tag)!].UserProfileID
            viewProfile(profileId: profileId)
            break
        case 3:
            profileId = self.friendSuggestionListObj.data[(sender.view?.tag)!].UserProfileID ?? 0
            viewProfile(profileId: profileId)
            break
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch currentView{
        case 1: // Friend
            let cell = tableView.dequeueReusableCell(withIdentifier: FriendsTVC.reuseIdentifier, for: indexPath) as! FriendsTVC
            
            cell.friendImage.sd_setImage(with: URL(string: self.friendListObj.data[indexPath.row].ProfileImage ?? ""), placeholderImage: UIImage(named: "img_selectProfilePic"), options: [.continueInBackground], completed: nil)
            let imageTapGesture = UITapGestureRecognizer(target: self, action: #selector(showZoomedImage))
            cell.friendImage.tag = indexPath.row
            cell.menuBtn.tag = indexPath.row
            cell.friendImage.addGestureRecognizer(imageTapGesture)
            cell.friendImage.isUserInteractionEnabled = true
            cell.menuBtn.addTarget(self, action: #selector(showMoreOptions(_:)), for: .touchUpInside)
            
            let friendNameGesture = UITapGestureRecognizer(target: self, action: #selector(showUserProfile(_:)))
            cell.friendNameLbl.text = "\(self.friendListObj.data[indexPath.row].Firstname ?? "") \(self.friendListObj.data[indexPath.row].Lastname ?? "")"
            cell.friendNameLbl.tag = indexPath.row;
            cell.friendNameLbl.addGestureRecognizer(friendNameGesture);
            cell.friendNameLbl.isUserInteractionEnabled = true;
            //          cell.msgBtn.tag = indexPath.row
            //          cell.msgBtn.addTarget(self, action: #selector(btnmsgAction(_:)), for: .touchUpInside)
            if OtherUserProfileID != 0{
                cell.removeMsgAndOptionBtn()
            }
            
            DispatchQueue.main.async {
                cell.setup()
            }
            
            cell.msgBtn.tag = indexPath.row
            cell.msgBtn.setTitle(MySingleton.shared.selectedLangData.message, for: .normal)
            cell.msgBtn.addTarget(self, action: #selector(btnmsgAction(_:)), for: .touchUpInside)
            
            return cell
            
        case 2: // Request
            
            let cell = tableView.dequeueReusableCell(withIdentifier: RequestTVC.resuseIdentifier, for: indexPath) as! RequestTVC
            cell.friendImg.sd_setImage(with: URL(string: self.friendRequestListObj.data[indexPath.row].ProfileImage ?? ""), placeholderImage: UIImage(named: "img_selectProfilePic"), options: [.continueInBackground], completed: nil)
            let friendNameGesture = UITapGestureRecognizer(target: self, action: #selector(showUserProfile(_:)))
            
            cell.friendNameLbl.text = "\(self.friendRequestListObj.data[indexPath.row].Firstname ?? "") \(self.friendRequestListObj.data[indexPath.row].Lastname ?? "")"
            cell.friendNameLbl.addGestureRecognizer(friendNameGesture);
            cell.friendNameLbl.isUserInteractionEnabled = true;
            cell.friendNameLbl.tag = indexPath.row;
            cell.friendCityLbl.text = (self.friendRequestListObj.data[indexPath.row].Location ?? "")
            cell.requData = FriendRequestTVCData(requId: self.friendRequestListObj.data[indexPath.row].RequestID ?? 0, requFrom: self.friendRequestListObj.data[indexPath.row].RequestFromUserID ?? 0)
            cell.delegate = self
            cell.acceptBtn.tag = indexPath.row
            cell.acceptBtn.setTitle(MySingleton.shared.selectedLangData.accept, for: .normal)
            cell.declineBtn.tag = indexPath.row
            cell.declineBtn.setTitle(MySingleton.shared.selectedLangData.remove, for: .normal)
            let imageTapGesture = UITapGestureRecognizer(target: self, action: #selector(showZoomedImage))
            cell.friendImg.tag = indexPath.row
            cell.friendImg.addGestureRecognizer(imageTapGesture)
            cell.friendImg.isUserInteractionEnabled = true
            DispatchQueue.main.async {
                cell.setup()
            }
            
            return cell
            
        case 3: // Suggestion
            let cell = tableView.dequeueReusableCell(withIdentifier: FriendSuggestionTVC.reuseIdentifier, for: indexPath) as! FriendSuggestionTVC
            cell.friendImg.sd_setImage(with: URL(string: self.friendSuggestionListObj.data[indexPath.row].ProfileImage ?? ""), placeholderImage: UIImage(named: "img_selectProfilePic"), options: [.continueInBackground], completed: nil)
            let friendNameGesture = UITapGestureRecognizer(target: self, action: #selector(showUserProfile(_:)))
            cell.friendNameLbl.text = "\(self.friendSuggestionListObj.data[indexPath.row].Firstname ?? "") \(self.friendSuggestionListObj.data[indexPath.row].Lastname ?? "")"
            cell.friendNameLbl.tag = indexPath.row;
            cell.friendNameLbl.addGestureRecognizer(friendNameGesture);
            cell.friendNameLbl.isUserInteractionEnabled = true;
            cell.friendCityLbl.text = (self.friendSuggestionListObj.data[indexPath.row].Location ?? "")
            cell.removeFriendBtn.tag = indexPath.row
            cell.addFriendBtn.tag = indexPath.row
            cell.addFriendBtn.setTitle(MySingleton.shared.selectedLangData.add_Friend, for: .normal)
            cell.removeFriendBtn.setTitle(MySingleton.shared.selectedLangData.remove, for: .normal)
            cell.requData = FriendSuggestionTVCData(userId: self.friendSuggestionListObj.data[indexPath.row].UserProfileID ?? 0, suggestionId: self.friendSuggestionListObj.data[indexPath.row].SuggestionID ?? 0)
            
            cell.delegate = self
            cell.friendImg.tag = indexPath.row
            let imageTapGesture = UITapGestureRecognizer(target: self, action: #selector(showZoomedImage))
            cell.friendImg.addGestureRecognizer(imageTapGesture)
            cell.friendImg.isUserInteractionEnabled = true
            DispatchQueue.main.async {
                cell.setup()
            }
            return cell
            
        default: // Friend
            let cell = tableView.dequeueReusableCell(withIdentifier: FriendsTVC.reuseIdentifier, for: indexPath) as! FriendsTVC
            
            cell.friendNameLbl.text = "\(self.friendListObj.data[indexPath.row].Firstname ?? "") \(self.friendListObj.data[indexPath.row].Lastname ?? "")"
            cell.friendImage.sd_setImage(with: URL(string: self.friendListObj.data[indexPath.row].ProfileImage ?? ""), placeholderImage: UIImage(named: "img_selectProfilePic"), options: [.continueInBackground], completed: nil)
            let imageTapGesture = UITapGestureRecognizer(target: self, action: #selector(showZoomedImage))
            cell.friendImage.tag = indexPath.row
            cell.friendImage.addGestureRecognizer(imageTapGesture)
            cell.friendImage.isUserInteractionEnabled = true
            cell.menuBtn.addTarget(self, action: #selector(showMoreOptions(_:)), for: .touchUpInside)
            if OtherUserProfileID != 0{
                cell.removeMsgAndOptionBtn()
            }
            DispatchQueue.main.async {
                cell.setup()
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        var profileId = 0
        //        switch currentView{
        //        case 1:
        //            profileId = self.friendListObj.data[indexPath.row].UserProfileID
        //            viewProfile(profileId: profileId)
        //            break
        //        case 2:
        //            profileId = self.friendRequestListObj.data[indexPath.row].UserProfileID
        //            viewProfile(profileId: profileId)
        //            break
        //        case 3:
        //            profileId = self.friendSuggestionListObj.data[indexPath.row].UserProfileID ?? 0
        //            viewProfile(profileId: profileId)
        //            break
        //        default:
        //            break
        //        }
        
    }
    
    func viewProfile(profileId:Int){
        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
        let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
        
        profileVC.userId = profileId
        
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "FriendsViewHeader") as! FriendsViewHeader
        header.searchUTF.delegate = self
        friendViewRef = header
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if currentView == 1{
            return CGFloat(headerSearchViewHeight)
        }else{
            return 0
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        
        //        if updatedString?.trimmingCharacters(in: .whitespaces) == ""{
        //            return true
        //        }
        
        self.friendListObj.data = []
        self.noOfFriends = 0
        self.totalFriendCount = 1
        searchText = updatedString!
        getFriendList(Skip: 0, Take: noOfRecordPerRequest, lazyLoading: true,SearchString: updatedString!)
        return true
    }
    
}
