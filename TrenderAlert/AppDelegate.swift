//
//  AppDelegate.swift
//  TrenderAlert
//
//  Created by Admin on 02/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import MFSideMenu
import AVFoundation
import GooglePlaces
import GoogleMaps
import Stripe
import UserNotifications

@UIApplicationMain
 class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let container = MFSideMenuContainerViewController()
    var wordList = LanguagesModel()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
//        IQKeyboardManager.shared.enable = true
//        SocketIoManager.sharedInstance.establishConnection()
        UIApplication.shared.applicationIconBadgeNumber = 0
        if UserDefaults.standard.object(forKey: "SelectedLang") != nil
        {
            initialConfig(Lang: UserDefaults.standard.object(forKey: "SelectedLang") as! String)
        }
        else
        {
            initialConfig(Lang: "english")
        }
        print("Selected Lang ------  \(String(describing: UserDefaults.standard.object(forKey: "SelectedLang"))) ------")
        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
            statusBar.backgroundColor = .baseColor
        }
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        SocketIoManager.sharedInstance.closeConnection()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
//        if UserDefaults.standard.object(forKey: "access_token") != nil {
//            SocketIoManager.sharedInstance.establishConnection()
//        }
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        if UserDefaults.standard.object(forKey: "access_token") != nil {
            SocketIoManager.sharedInstance.establishConnection()
            
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - open url
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        print(url.host!)
        let userIdObj = ["id" : url.host!]
        let seconds = 2.0
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "getTrendDetails"), object: self, userInfo: userIdObj)
        }
        return true
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "TrenderAlert")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    
    // MARK: - Initial Configuration
    func initialConfig(Lang:String) {
    
        if let path = Bundle.main.path(forResource: Lang, ofType: "json", inDirectory: "trender_alert") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                let jsonData = try JSONSerialization.data(withJSONObject: jsonResult, options: .prettyPrinted)
                print(jsonResult)
                
                self.wordList = WebServices().decodeDataToClass(data: jsonData, decodeClass: LanguagesModel.self)!
                print(self.wordList)
                MySingleton.shared.selectedLangData = self.wordList
                // do stuff
            } catch {
                // handle error
            }
        }
        
        
        // Keyboard Config
        IQKeyboardManager.shared.enable = true
        
        STPPaymentConfiguration.shared().publishableKey = "pk_test_BPJsllLCiXF4yAZpkiw1IOtb000rLsRxjm"
        // Side Menu Config
        
        registerForPushNotifications()
//        UNUserNotificationCenter.current().delegate = self
        
        GMSServices.provideAPIKey("AIzaSyCJ0wGNpwuNd7q2NhyNAHKr80FRy8zIJAU")
        GMSPlacesClient.provideAPIKey("AIzaSyCJ0wGNpwuNd7q2NhyNAHKr80FRy8zIJAU")
        
        switch AVAudioSession.sharedInstance().recordPermission {
        case AVAudioSession.RecordPermission.granted:
            print("Permission granted")
        case AVAudioSession.RecordPermission.denied:
            print("Pemission denied")
        case AVAudioSession.RecordPermission.undetermined:
            print("Request permission here")
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                // Handle granted
            })
        }
        
    //    GMSServices.provideAPIKey("AIzaSyDOqh8kBUF2_tbQF_uc2HiRPCGFC77_vE4")
        
        var usersArray = [Data]()
        if UserDefaults.standard.object(forKey: "usersArray") != nil {
            usersArray = UserDefaults.standard.object(forKey: "usersArray") as! [Data]
        }
        if UserDefaults.standard.object(forKey: "access_token") != nil {
            // Dashboard
            for userData in usersArray {
                let userLoginData = WebServices().decodeDataToClass(data: userData, decodeClass: Login.self)
                if ("Bearer " + (userLoginData?.access_token)!) == UserDefaults.standard.object(forKey: "access_token") as! String {
                    if userLoginData?.IsVerified == "True" && userLoginData?.IsFirstTime == "False" {
                        // Dashboard
                        MySingleton.shared.loginObject = userLoginData!
                        let storyBoard = UIStoryboard(name: "Dashboard", bundle: nil)
                        let centerViewController = storyBoard.instantiateViewController(withIdentifier: "TrendAlertTabBarController") as! UITabBarController
                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                        let leftViewController = mainStoryBoard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
                        container.leftMenuViewController = leftViewController
                        container.centerViewController = centerViewController
                        container.leftMenuWidth = (centerViewController.view.frame.size.width / 4) * 3
                        self.window!.rootViewController = container
                        self.window!.makeKeyAndVisible()
                    }
                }
            }
        }
        else if usersArray.count > 0 {
            // Switch User
//            let storyBoard = UIStoryboard(name: "Dashboard", bundle: nil)
//            let centerViewController = storyBoard.instantiateViewController(withIdentifier: "TrendAlertTabBarController") as! UITabBarController
//            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
//            let switchUserNavigationController = mainStoryBoard.instantiateViewController(withIdentifier: "switchUserNavigationController") as! UINavigationController
//
//            centerViewController.selectedViewController = switchUserNavigationController
//            let leftViewController = mainStoryBoard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
//            container.leftMenuViewController = leftViewController
//            container.centerViewController = centerViewController
//            self.window!.rootViewController = container
//            self.window!.makeKeyAndVisible()
            
            
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let loginNavigationController = mainStoryBoard.instantiateViewController(withIdentifier: "switchUserNavigationController") as! UINavigationController
            self.window!.rootViewController = loginNavigationController
            self.window!.makeKeyAndVisible()
   
        }
        else {
                        
//            let mainStoryBoard = UIStoryboard(name: "BusinessAccountDetailsSB", bundle: nil)
//            let loginNavigationController = mainStoryBoard.instantiateViewController(withIdentifier: "BusinessActivityVC") as! BusinessActivityVC
//            self.window!.rootViewController = loginNavigationController
//            self.window!.makeKeyAndVisible()

            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let loginNavigationController = mainStoryBoard.instantiateViewController(withIdentifier: "navigationController") as! UINavigationController
            self.window!.rootViewController = loginNavigationController
            self.window!.makeKeyAndVisible()
        }
        
        
    }
    
    func registerForPushNotifications()
    {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            print("Permission granted: \(granted)")
            
            guard granted else { return }
            self.getNotificationSettings()
        }
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
             DispatchQueue.main.async {
                 UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print(deviceTokenString)
        UserDefaults.standard.set(deviceTokenString, forKey: "deviceToken")
        let deviceId = UIDevice.current.identifierForVendor?.uuidString
        print(deviceId!)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any])
    {
        print("Notification Payload.")
        print(userInfo)
    }
}

