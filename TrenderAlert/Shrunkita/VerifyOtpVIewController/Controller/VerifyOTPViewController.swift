//
//  SetUserNameViewController.swift
//  TrenderAlert
//
//  Created by Admin on 04/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import AFViewShaker

class VerifyOTPViewController: UIViewController {

    @IBOutlet var txtOtp: FormFields!
    
    var signUpObject = SignUp()
    var OTPCode = String()
    var userID = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

      //  initialSettings()
    }
    
 
    @IBAction func btn_SubmitOtp(_ sender: UIButton)
    {
        ProceedToSendPassword()
    }
    
    @IBAction func btn_ResendOtpAction(_ sender: UIButton)
    {
        ProceedToReSendOtp()
    }
    
    
    func ProceedToReSendOtp(){
        
        let param = ["UserID": "\(userID)"]
        WebServices().callUserService1(service: UserServices.resendOtp, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: false, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
                print(serviceResponse)
               // self.txtOtp.txtFld_UserValue.text = "\((serviceResponse["Data"] as! NSArray).lastObject!)"
                if(serviceResponse["Status"] as! Bool == true)
                {
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
                else
                {
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            })
    }
    
    
    func ProceedToSendPassword(){
        let isValid = validation()
        if (isValid == true) {
            let param = ["OTPCode": self.txtOtp.txtFld_UserValue.text!,
                         "UserID": "\(userID)"] as [String : Any]
            WebServices().callUserService1(service: UserServices.verifyOtp, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: false, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
                print(serviceResponse)
                if(serviceResponse["Status"] as! Bool == true)
                {
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: {
                        if MySingleton.shared.isForgotPassword == true {
                            self.performSegue(withIdentifier: "VerifyOtpToChangePassword", sender: self)
                        }
                        else {
                            self.performSegue(withIdentifier: "verifyOtpToLogin", sender: self)
                        }
                    })
                }
                else
                {
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            })
        }
    }
    
    func validation() -> Bool
    {
        let whitespaceSet = CharacterSet.whitespaces
        if(txtOtp.txtFld_UserValue.text!.isEmpty)
        {
            txtOtp.txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Please enter verification code.",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            let viewShaker = AFViewShaker(view: self.txtOtp)
            viewShaker?.shake()
            return false
        }
        else if (txtOtp.txtFld_UserValue.text?.trimmingCharacters(in: whitespaceSet).isEmpty)!
        {
            txtOtp.txtFld_UserValue.text = ""
            txtOtp.txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Verification code invalid.",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            let viewShaker = AFViewShaker(view: self.txtOtp)
            viewShaker?.shake()
        }
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "verifyOtpToLogin" {
            let loginVC = segue.destination as! LoginViewController
            loginVC.signUpObject = signUpObject
        }
        if segue.identifier == "VerifyOtpToChangePassword" {
            let changePasswordVC = segue.destination as! ChangePasswordViewController
            changePasswordVC.userId = userID
        }
    }

}
