//
//  LoginViewController.swift
//  TrenderAlert
//
//  Created by OSP LABS on 26/12/18.
//  Copyright © 2018 Single. All rights reserved.
//

import UIKit
import MFSideMenu
import AFViewShaker

class LoginViewController: UIViewController {
 
    @IBOutlet var btnSignup: UIButton!
    @IBOutlet weak var btnForgotPass: UIButton!
    @IBOutlet var email: FormFields!
    @IBOutlet var pssword: FormFields!
    @IBOutlet weak var eyeButton: UIButton!
    var userInfoFlag = String()
    var signUpObject = SignUp()
    var loginObject = Login()

    override func viewDidLoad() {
        super.viewDidLoad()
       self.initialise()
    }
    
    // MARK : - setup to initialise view
    func initialise()
    {
//        self.email.txtFld_UserValue.text = "ishwar@yopmail.com"
//        self.pssword.txtFld_UserValue.text = "Ishwar@123"
        
        self.btnForgotPass.titleLabel?.text! = "FORGOT PASSWORD?".localizableString(loc: "es")
        self.pssword.txtFld_UserValue.isSecureTextEntry = true
        self.navigationController?.isNavigationBarHidden = true
        if self.menuContainerViewController != nil {
            self.menuContainerViewController.panMode = MFSideMenuPanModeNone
        }
        
        btnSignup.makeCornerRadius(radius: 8.0)
        
    }
    
    @IBAction func btn_FOrgotPassAction(_ sender: UIButton)
    {
        self.performSegue(withIdentifier: "LoginToForgotPassword", sender: self)
    }
    
    
    func proceedToLogin()
    {
        let viewArray : [UIView] = [email, pssword]
        let obj_Validation = Validation()
        let viewsToShake = obj_Validation.validationForLogin(viewList: viewArray)
        if(viewsToShake.count == 0)
        {
            let param = ["grant_type":"password","username": self.email.txtFld_UserValue.text!,"password": self.pssword.txtFld_UserValue.text!,"devicetype":"ios","devicetoken": ((UserDefaults.standard.object(forKey: "deviceToken") != nil) ?  (UserDefaults.standard.object(forKey: "deviceToken")!) : "adguhkjdfgughjgukhjmnergsdgdfg")]
        
            WebServices().callUserService1(service: UserServices.NormalLogin, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: false, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
                print(serviceResponse)
                
                if let error = serviceResponse["error_description"] as? String
                {
                    TrenderAlertVC.shared.presentAlertController(message: error, completionHandler: nil)
                }
                else
                {
                    self.loginObject = WebServices().decodeDataToClass(data: serviceData, decodeClass: Login.self)!
                    MySingleton.shared.loginObject = self.loginObject
                    
                    if ((serviceResponse["IsFirstTime"] as! String) == "True")
                    {
                        var usersArray = [Data]()
                        if UserDefaults.standard.object(forKey: "usersArray") != nil {
                            usersArray = UserDefaults.standard.object(forKey: "usersArray") as! [Data]
                        }
                        var isSameUser = false
                        if usersArray.count != 0
                        {
                            for i in 0...usersArray.count-1 {
                                let userObject = WebServices().decodeDataToClass(data: usersArray[i], decodeClass: Login.self)
                                if userObject?.UserID == self.loginObject.UserID {
                                    isSameUser = true
                                    usersArray[i] = serviceData
                                    UserDefaults.standard.set(usersArray, forKey: "usersArray")
                                    UserDefaults.standard.synchronize()
                                }
                            }
                        }
                        if isSameUser == false {
                            usersArray.append(serviceData)
                            UserDefaults.standard.set(usersArray, forKey: "usersArray")
                            UserDefaults.standard.synchronize()
                        }
                        //                        for userData in usersArray {
                        //                            MySingleton.shared.usersArray.append(WebServices().decodeDataToClass(data: userData, decodeClass: Login.self)!)
                        //                        }
                        UserDefaults.standard.setValue("Bearer " + (serviceResponse["access_token"] as! String), forKey: "access_token")
                   //     self.userInfoFlag = "AlreadyUser"
                        
                        if serviceResponse["IsVerified"] as! String == "True" {
                            self.userInfoFlag = "FirstTime"
                            self.performSegue(withIdentifier: "loginToSelectLang", sender: self)
                            //  self.getUserDetails()
                        }
                        else {
                            
                            self.performSegue(withIdentifier: "loginToVerifyOTP", sender: self)
                        }
                    }
                    else
                    {
                        var usersArray = [Data]()
                        if UserDefaults.standard.object(forKey: "usersArray") != nil {
                            usersArray = UserDefaults.standard.object(forKey: "usersArray") as! [Data]
                        }
                        var isSameUser = false
                        if usersArray.count != 0
                        {
                            for i in 0...usersArray.count-1 {
                                let userObject = WebServices().decodeDataToClass(data: usersArray[i], decodeClass: Login.self)
                                if userObject?.UserID == self.loginObject.UserID {
                                    isSameUser = true
    //                                userObject?.access_token = (serviceResponse["access_token"] as! String)
                                    usersArray[i] = serviceData
                                    UserDefaults.standard.set(usersArray, forKey: "usersArray")
                                    UserDefaults.standard.synchronize()
                                }
                            }
                        }
                        if isSameUser == false {
                            usersArray.append(serviceData)
                            UserDefaults.standard.set(usersArray, forKey: "usersArray")
                            UserDefaults.standard.synchronize()
                        }
//                        for userData in usersArray {
//                            MySingleton.shared.usersArray.append(WebServices().decodeDataToClass(data: userData, decodeClass: Login.self)!)
//                        }
                        UserDefaults.standard.setValue("Bearer " + (serviceResponse["access_token"] as! String), forKey: "access_token")
                        self.userInfoFlag = "AlreadyUser"
//                        self.performSegue(withIdentifier: "loginToDashboard", sender: self)
                        
                        
                        let appDelegatge = UIApplication.shared.delegate as! AppDelegate
                        let storyBoard = UIStoryboard(name: "Dashboard", bundle: nil)
                        let centerViewController = storyBoard.instantiateViewController(withIdentifier: "TrendAlertTabBarController") as! UITabBarController
                        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                        let leftViewController = mainStoryBoard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
                        appDelegatge.container.leftMenuViewController = leftViewController
                        appDelegatge.container.centerViewController = centerViewController
                        appDelegatge.container.leftMenuWidth = (centerViewController.view.frame.size.width / 4) * 3
                        appDelegatge.window!.rootViewController = appDelegatge.container
                        appDelegatge.window!.makeKeyAndVisible()
                        
                      //  self.getUserDetails()
                    }
                    
                }
            })
        }
        else
        {
            let viewShaker = AFViewShaker(viewsArray: viewsToShake)
            print(viewsToShake)
            viewShaker?.shake()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "loginToSelectLang" {
            let loginVC = segue.destination as! SetLangViewController
             loginVC.signUpObject = signUpObject
        }
        if segue.identifier == "loginToVerifyOTP" {
            let verifyVC = segue.destination as! VerifyOTPViewController
            verifyVC.userID = loginObject.UserID
        }
    }


    
//    func getUserDetails()
//    {
//        WebServices().callUserService(service: UserServices.GetUserProfile, parameters: nil, isLazyLoading: false, isHeader: true, CallMethod: .get, actionAfterServiceResponse: { (serviceResponse) in
//        print(serviceResponse)
//        let userCredentials: NSData = NSKeyedArchiver.archivedData(withRootObject: ((serviceResponse["UserProfile_Details"] as AnyObject).object(at: 0) as AnyObject)) as NSData
//        UserDefaults.standard.set(userCredentials, forKey: "UserProfile_Details")
//        print(UserDefaults.standard.object(forKey: "UserProfile_Details")!)
//
//            if self.userInfoFlag == "FirstTime"
//            {
//
//            }
//            else
//            {
//
//            }
//        })
//    }
    
    @IBAction func btn_LoginAction(_ sender: UIButton)
    {
        proceedToLogin()
    }
    
    @IBAction func btn_SignUpAction(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
        self.pssword.txtFld_UserValue.isSecureTextEntry = !self.pssword.txtFld_UserValue.isSecureTextEntry
    }
    
    @IBAction func btn_showAction(_ sender: UIButton)
    {
        if sender.currentImage == UIImage(named: "SHow")
        {
            sender.setImage(UIImage(named: "Hide"), for: .normal)
            self.pssword.txtFld_UserValue.isSecureTextEntry = true
        }
        else
        {
             sender.setImage(UIImage(named: "SHow"), for: .normal)
             self.pssword.txtFld_UserValue.isSecureTextEntry = false
        }
    }
    
}
