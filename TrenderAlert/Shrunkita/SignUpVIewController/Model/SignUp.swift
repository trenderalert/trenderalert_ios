//
//  SignUp.swift
//  TrenderAlert
//
//  Created by OSP LABS on 17/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit


class SignUpData: NSObject, Codable {
    var OTPCode = String()
    var UserID = 0
    var Username = [String]()
    
    enum CodingKeys: String, CodingKey {
        case OTPCode
        case UserID
        case Username
    }
}

class SignUp: NSObject, Codable {
    var Data = [SignUpData]()
    
    enum CodingKeys: String, CodingKey {
        case Data
    }
}
