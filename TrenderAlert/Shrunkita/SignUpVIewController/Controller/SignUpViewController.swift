//
//  SignUpViewController.swift
//  TrenderAlert
//
//  Created by Admin on 31/12/18.
//  Copyright © 2018 Single. All rights reserved.
//

import UIKit
import AFViewShaker
import GooglePlaces
import DropDown
import MBProgressHUD
import CountryPickerView
import CropViewController

class SignUpViewController: UIViewController {

    @IBOutlet var imgFemaleRadio: UIImageView!
    @IBOutlet var imgMaleRadio: UIImageView!
    @IBOutlet var txtConfirmPass: FormFields!
    @IBOutlet var txtPassword: FormFields!
    @IBOutlet var txtEmail: FormFields!
    @IBOutlet var txtLocation: FormFields!
    @IBOutlet var txtMobileNo: FormFields!
    @IBOutlet var MobileNo: CountryPickerView!
    @IBOutlet var btnCancelPic: UIButton!
    @IBOutlet var btnSelectProfilePic: UIButton!
    @IBOutlet var firstName: FormFields!
    @IBOutlet var lastName: FormFields!
    @IBOutlet var birthDate: FormFields!
    
    @IBOutlet weak var datePickerView: UIView!
    @IBOutlet weak var dobPicker: UIDatePicker!
    
    let picker = UIImagePickerController()
    var location_arr = [String]()
    var str_latitude = String()
    var str_longitude = String()
    var gender = "F"
    var countryCode = String()
    var locationDropdown = DropDown()
    var locationManager = CLLocationManager()
    
    var signUpObject = SignUp()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSettings()
    }
    
    override func viewWillLayoutSubviews()
    {
        self.btnSelectProfilePic.makeCornerRadius(radius: self.btnSelectProfilePic.frame.size.height/2)
    }
    
    func initialSettings()
    {
        
        self.firstName.txtFld_UserValue.delegate = self
        self.lastName.txtFld_UserValue.delegate = self
        self.MobileNo.dataSource = self
        self.MobileNo.delegate = self
        self.txtLocation.txtFld_UserValue.delegate = self
        txtMobileNo.img_IconPic.removeFromSuperview()
        countryCode = self.MobileNo.selectedCountry.phoneCode
        self.picker.delegate = self
        self.btnCancelPic.isHidden = true
        self.txtPassword.txtFld_UserValue.isSecureTextEntry = true
        self.txtConfirmPass.txtFld_UserValue.isSecureTextEntry = true
        self.txtMobileNo.txtFld_UserValue.keyboardType = .numberPad
        self.txtEmail.txtFld_UserValue.keyboardType = .emailAddress
        self.datePickerView.isHidden = true
        self.dobPicker.backgroundColor = .white
        self.dobPicker.datePickerMode = UIDatePicker.Mode.date
        self.dobPicker.set16YearValidation()
        self.txtLocation.txtFld_UserValue.addTarget(self, action: #selector(self.textFieldTextChanged(_:)), for: .editingChanged)
        self.txtLocation.txtFld_UserValue.delegate = self
//        self.txtPassword.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 60)
        self.txtConfirmPass.txtFld_UserValue.delegate = self
        self.txtConfirmPass.txtFld_UserValue.editingRect(forBounds: CGRect(x: 0, y: 0, width:self.txtConfirmPass.txtFld_UserValue.frame.width, height: self.txtConfirmPass.txtFld_UserValue.frame.height))
        self.txtConfirmPass.txtFld_UserValue.textRect(forBounds: CGRect(x: 0, y: 0, width:self.txtConfirmPass.txtFld_UserValue.frame.width - 100, height: self.txtConfirmPass.txtFld_UserValue.frame.height))
        self.txtConfirmPass.txtFld_UserValue.placeholderRect(forBounds: CGRect(x: 0, y: 0, width:self.txtConfirmPass.txtFld_UserValue.frame.width, height: self.txtConfirmPass.txtFld_UserValue.frame.height))
        self.txtPassword.txtFld_UserValue.rightPadding(paddingSize: 140)
        locationDropdown.anchorView = txtLocation
        locationDropdown.bottomOffset = CGPoint(x: 0, y:(locationDropdown.anchorView?.plainView.bounds.height)!/2)
        locationDropdown.topOffset = CGPoint(x: 0, y:-(locationDropdown.anchorView?.plainView.bounds.height)!/2)
        dropdownSelection()
        
        locationManager.delegate = self
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse
        {
            locationManager.startUpdatingLocation()
            //searchMap.mapType = .satellite
            // searchMap.sd_setShowActivityIndicatorView(true)
          //  searchMap.isMyLocationEnabled = true
        }
        else
        {
            locationManager.requestWhenInUseAuthorization()
            
        }
        
    }
  
    @IBOutlet weak var datePickerBottomView: UIView!
    
    @IBAction func didTapCancelButton(_ sender: UIButton) {
        self.datePickerView.isHidden = true
    }
    
    @IBAction func didTapSubmitButton(_ sender: UIButton) {
        self.datePickerView.isHidden = true
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-YYYY"
        let selectedDate = dateFormatter.string(from: self.dobPicker.date)
        print("Selected Date = \(selectedDate)")
        
        birthDate.txtFld_UserValue.text = "\(selectedDate)"
    }
    
    
    @IBAction func btn_selectProfilePic(_ sender: Any)
    {
        self.view.endEditing(true)
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select your option", message: "", preferredStyle: .actionSheet)
        
        let cameraActionButton: UIAlertAction = UIAlertAction(title: "Camera", style: .default) { void in
            print("Camera")
            self.openCamera()
        }
        actionSheetControllerIOS8.addAction(cameraActionButton)
        
        let galleryActionButton: UIAlertAction = UIAlertAction(title: "Gallery", style: .default) { void in
            print("Gallery")
            self.openGallary()
        }
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: "Cancel", style: .default) { void in
            print("Cancel")
        }
        
        
        actionSheetControllerIOS8.addAction(galleryActionButton)
        actionSheetControllerIOS8.addAction(cancelActionButton)
        actionSheetControllerIOS8.popoverPresentationController?.sourceView = self.btnSelectProfilePic
        actionSheetControllerIOS8.popoverPresentationController?.sourceRect = self.btnSelectProfilePic.bounds
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    @IBAction func btn_CancelPic(_ sender: Any)
    {
        self.btnCancelPic.isHidden = true
        self.btnSelectProfilePic.setBackgroundImage(UIImage(named: "img_selectProfilePic"), for: .normal)
    }
    
    
    
    @IBAction func didTapDOBDatePicker(_ sender: UIButton) {
        self.view.endEditing(true)
        self.datePickerView.isHidden = false
        
    }
    
    @IBAction func didTapSignUpButton(_ sender: UIButton) {
        validation()
    }
    
    
    @IBAction func passwordShowButton(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.txtPassword.txtFld_UserValue.isSecureTextEntry = !self.txtPassword.txtFld_UserValue.isSecureTextEntry
    }
    
    @IBAction func didTapConfirmPasswordShowButton(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.txtConfirmPass.txtFld_UserValue.isSecureTextEntry = !self.txtConfirmPass.txtFld_UserValue.isSecureTextEntry
    }
    
    
    
    @IBAction func tap_MaleChangeSign(_ sender: UITapGestureRecognizer)
    {
        if imgMaleRadio.image == #imageLiteral(resourceName: "radioOffYellow")
        {
            imgMaleRadio.image = #imageLiteral(resourceName: "radioOnYellow")
            imgFemaleRadio.image = #imageLiteral(resourceName: "radioOffYellow")
            self.gender = "M"
        }
    }
    
    @IBAction func tap_FemaleChangeSign(_ sender: UITapGestureRecognizer)
    {
        if imgFemaleRadio.image == #imageLiteral(resourceName: "radioOffYellow")
        {
            imgMaleRadio.image = #imageLiteral(resourceName: "radioOffYellow")
            imgFemaleRadio.image = #imageLiteral(resourceName: "radioOnYellow")
            self.gender = "F"
        }
    }
    
    func testData() {
        firstName.txtFld_UserValue.text = "Ishwar"
        lastName.txtFld_UserValue.text = "OSP"
//        birthDate.txtFld_UserValue.text = "01/17/2009"
  //      txtMobileNo.txtFld_UserValue.text = "+917021465081"
        txtEmail.txtFld_UserValue.text = "ishwar@yopmail.com"
        txtPassword.txtFld_UserValue.text = "Test@123"
        txtConfirmPass.txtFld_UserValue.text = "Test@123"
    }
    
    func validation()
    {
        let viewArray : [UIView] = [firstName,lastName,birthDate,txtLocation,txtMobileNo,txtEmail,txtPassword,txtConfirmPass]
        let obj_Validation = Validation()
        let viewsToShake = obj_Validation.validationForSocialSignUp(viewList: viewArray)
        if(viewsToShake.count == 0)
        {
            if(str_latitude == "" || str_longitude == "")
            {
                TrenderAlertVC.shared.presentAlertController(message: "Please select proper location", completionHandler: nil)
                return
            }
            
            if(self.txtPassword.txtFld_UserValue.text != self.txtConfirmPass.txtFld_UserValue.text)
            {
                if(self.txtConfirmPass.txtFld_UserValue.text?.isEmpty)!
                {
                    self.txtConfirmPass.txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Confirm Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
                }
                else
                {
                    self.txtConfirmPass.txtFld_UserValue.text! = ""
                    self.txtConfirmPass.txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Passwords do not match",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
                }
                let viewShaker = AFViewShaker(view: self.txtConfirmPass)
                viewShaker?.shake()
                return
            }
            
            getSocialSignUp()
            
        }
        else
        {
            let viewShaker = AFViewShaker(viewsArray: viewsToShake)
            print(viewsToShake)
            viewShaker?.shake()
        }
    }
    
    
    func encodeToBase64String(image: UIImage) -> String
    {
        let imageData = image.jpegData(compressionQuality: 0.5)
        let imag_str:String = imageData!.base64EncodedString(options:[])
        //print(imag_str)
        return imag_str
    }
    
    func getSocialSignUp(){
        
            var parameters = [String:AnyObject]()
            if(btnSelectProfilePic.currentBackgroundImage == UIImage(named: "img_selectProfilePic"))
            {
//                let dateFormatter = DateFormatter()
//                dateFormatter.dateFormat = "YYYY-MM-dd"
//                let selectedDate = dateFormatter.string(from: self.dobPicker.date)
                
                parameters = ["FirstName": firstName.txtFld_UserValue.text!, "LastName": lastName.txtFld_UserValue.text!, "BirthDate":birthDate.txtFld_UserValue.text!, "Location": txtLocation.txtFld_UserValue.text!, "Email": txtEmail.txtFld_UserValue.text!,"Password":txtPassword.txtFld_UserValue.text!,"ConfirmPassword": txtConfirmPass.txtFld_UserValue.text!,"UserLocationLatitude": self.str_latitude,"UserLocationLongitude": self.str_longitude,"MobileNumber":txtMobileNo.txtFld_UserValue.text!,"Gender":self.gender,"CountryCode":"\(self.countryCode)","isChangeImage": "false","ImageList":[]] as [String : AnyObject]
                
                
                WebServices().callUserService1(service: UserServices.SignUp, urlParameter: "", parameters: parameters as [String : AnyObject], isLazyLoading: false, isHeader: false, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
                    print(serviceResponse)
                    if(serviceResponse["Status"] != nil)
                    {
                        if(serviceResponse["Status"] as! Bool == true)
                        {
                            self.signUpObject = WebServices().decodeDataToClass(data: serviceData, decodeClass: SignUp.self)!
                            print("Sign Up --- \(self.signUpObject.Data[0].Username)")
                            TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: {
                                self.performSegue(withIdentifier: "SignUpToVerifyOTP", sender: self)
                            })
                            
                        }
                        else
                        {
                            TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                        }
                    }
                    else
                    {
                        TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                    }
                })
            }
            else
            {
                let imageData = ["FileExt":"jpg","contenttype":"image/jpg","ImageData":"data:image/jpg;base64,\(encodeToBase64String(image: btnSelectProfilePic.currentBackgroundImage!))"]
             //   print(imageData)
                let imageArray : [AnyObject] = [imageData as AnyObject]
              //  print(imageArray)
                
           
                parameters = ["FirstName":firstName.txtFld_UserValue.text!, "LastName": lastName.txtFld_UserValue.text!, "BirthDate":birthDate.txtFld_UserValue.text!, "Location": txtLocation.txtFld_UserValue.text!, "Email": txtEmail.txtFld_UserValue.text!, "isChangeImage": "true", "ImageList": ["Images": imageArray],"UserLocationLatitude": self.str_latitude,"UserLocationLongitude": self.str_longitude,"Password":txtPassword.txtFld_UserValue.text!,"ConfirmPassword": txtConfirmPass.txtFld_UserValue.text!,"MobileNumber":self.countryCode + txtMobileNo.txtFld_UserValue.text!,"Gender":self.gender,"CountryCode":"\(self.countryCode)"] as [String : AnyObject]
            //    print(parameters)
                
                callApi(requestData: parameters)
            }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SignUpToVerifyOTP" {
            let verifyOTPVC = segue.destination as! VerifyOTPViewController
            verifyOTPVC.signUpObject = self.signUpObject
            verifyOTPVC.userID = String(signUpObject.Data[0].UserID)
            MySingleton.shared.isForgotPassword = false
        }
    }
    
    
    func callApi(requestData: Dictionary<String, Any>)
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)

//        var urlStr: String = "http://172.16.3.12:96/api/apiAccount/Register"
//        var urlStr: String = "https://trenderalert.azurewebsites.net/api/ApiAccount/Register"
        var urlStr: String = "\(WebServices.baseURL)api/ApiAccount/Register"
        
        
        urlStr = urlStr.replacingOccurrences(of: " ", with: "&")
        let request :NSMutableURLRequest = NSMutableURLRequest(url:NSURL(string:urlStr)! as URL)
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        if requestData.count != 0
        {
            do
            {
                let jsonData = try JSONSerialization.data(withJSONObject: requestData, options: JSONSerialization.WritingOptions.prettyPrinted)
                request.httpBody = jsonData
            }
            catch let error as NSError
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                }
            }
        }
        //        if ((UserDefaults.standard.object(forKey:CONSTANT.token.rawValue)) != nil)
        //        {
        //            let authToken:String = "Bearer " + (UserDefaults.standard.object(forKey:CONSTANT.token.rawValue) as! String)
        //            print(authToken)
        //            request.setValue(authToken, forHTTPHeaderField:CONSTANT.Authorization.rawValue )
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        //        }
        
        //        request.setValue(CONSTANT.contentType.rawValue, forHTTPHeaderField: CONSTANT.contentKey.rawValue)
        let task = URLSession.shared.dataTask(with: request as URLRequest){ data,response,error in
            
            if error != nil
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error!.localizedDescription, completionHandler: nil)
                }
            }
            do
            {
                if (response as? HTTPURLResponse) != nil
                {
                    
                    //////////////////// JSON RESPONSE /////////////////
   
                    
                    let jsonString = String(data: data!, encoding: .utf8)
                    print(jsonString!)
                    
                    //////////////////// JSON RESPONSE /////////////////
                    
                    
                    
                    if data != nil
                    {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        
                        print(jsonData)
                        
                        if(jsonData["Status"] as! Bool == true)
                        {
                            self.signUpObject = WebServices().decodeDataToClass(data: data!, decodeClass: SignUp.self)!
                            print("Sign Up --- \(self.signUpObject.Data[0].Username)")
                            DispatchQueue.main.async {
                                MBProgressHUD.hide(for: self.view, animated: true)
                          //  TrenderAlertVC.shared.presentAlertController(message: jsonData["Message"] as! String, completionHandler: {
                                self.performSegue(withIdentifier: "SignUpToVerifyOTP", sender: self)
                          //  })
                            }
                            
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                MBProgressHUD.hide(for: self.view, animated: true)
                            TrenderAlertVC.shared.presentAlertController(message: jsonData["Message"] as! String, completionHandler: nil)
                            }
                        }
                        
                        
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true)
                            TrenderAlertVC.shared.presentAlertController(message: error!.localizedDescription, completionHandler: nil)
                        }
                    }
                    
                }
            }
            catch let error as NSError
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                }
            }
        }
        task.resume()
    }
    
    
}


extension SignUpViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate,CropViewControllerDelegate
{
    /*************************
     Method Name:  openGallary()
     Parameter: nil
     return type: nil
     Desc: This function opens the gallery.
     *************************/
    func openGallary()
    {
        picker.allowsEditing = false
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        present(picker, animated: true, completion: nil)
    }
    
    /*************************
     Method Name:  openCamera()
     Parameter: nil
     return type: nil
     Desc: This function opens the Camera.
     *************************/
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            present(picker, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            
            present(alert, animated: true, completion: nil)
        }
    }
    
    /*************************
     Method Name:  imagePickerControllerDidCancel()
     Parameter: UIImagePickerController
     return type: nil
     Desc: This function is called if user taps cancel button.
     *************************/
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    /*************************
     Method Name:  imagePickerController()
     Parameter: UIImagePickerController, [String : Any]
     return type: nil
     Desc: This function is called when user selects image from gallery.
     *************************/
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage

        dismiss(animated:true, completion: nil)
        let cropViewController = CropViewController(image: chosenImage!)
        cropViewController.delegate = self
        present(cropViewController, animated: true, completion: nil)
    }
//    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
//    {
//        let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
//        print(chosenImage)
//        self.btnSelectProfilePic.setBackgroundImage(chosenImage, for: UIControl.State.normal)
//        self.btnCancelPic.isHidden = false
//        dismiss(animated:true, completion: nil)
//    }
    
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {

        self.btnSelectProfilePic.setBackgroundImage(image, for: UIControl.State.normal)
        self.btnCancelPic.isHidden = false
        dismiss(animated:true, completion: nil)
    }
}

extension SignUpViewController: UITextFieldDelegate {
    /*************************
     Method Name:  textFieldTextChanged()
     Parameter: UITextField
     return type: nil
     Desc: This function cis called when text Field Text is Changed.
     *************************/
//    @objc func textFieldTextChange(_ sender : UITextField)
//    {
//        sender.text = sender.text?.lowercased()
//    }
//
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if textField == self.firstName.txtFld_UserValue || textField == self.lastName.txtFld_UserValue{
            
            let maxLength = 20
            let currentString: NSString = textField.text! as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }
        
        return true
    }
    
    
    @objc func textFieldTextChanged(_ sender : UITextField)
    {
        if(txtLocation.txtFld_UserValue.isEditing)
        {
            if  txtLocation.txtFld_UserValue.text!.isEmpty
            {
              self.locationDropdown.hide()
            }
            else
            {
                let getAddress = GetAddress()
                self.location_arr.removeAll()
                //self.locationDropdown.dataSource = self.location_arr
                getAddress.getAddress(location_str: sender.text!) { (locationData) in
                    // self.location_arr = locationData
                    for location in locationData
                    {
                        self.location_arr.append(location["address"]!)
                    }
                    print(self.location_arr)
                    self.locationDropdown.dataSource = []
                    self.locationDropdown.dataSource = self.location_arr
                    
                    if !sender.text!.isEmpty
                    {
                        self.locationDropdown.show()
                    }
                }
            }
        }else if (txtEmail.txtFld_UserValue.isEditing)
        {
            sender.text = sender.text?.lowercased()
        }
    }
    
    func dropdownSelection()
    {
        locationDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.txtLocation.txtFld_UserValue.text! = item
            GetAddress().getLocationFromAddressString(addressStr: self.txtLocation.txtFld_UserValue.text!) { (clLocationCoordinate2D,zipcode) in
                self.str_latitude = String(clLocationCoordinate2D.latitude)
                self.str_longitude = String(clLocationCoordinate2D.longitude)
            }
        }
    }
}


extension SignUpViewController: CLLocationManagerDelegate {
    /*************************
     Method Name:  locationManager()
     Parameter: CLLocationManager, CLAuthorizationStatus
     return type: nil
     Desc: This function cis called when didChangeAuthorization.
     *************************/
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            manager.startUpdatingLocation()
            DispatchQueue.main.async {
                MBProgressHUD.showAdded(to: self.view, animated: true)
            }
            
        }
        else
        {
//            TrenderAlertVC.shared.presentAlertController(message: "Please go to settings and enable location services.", completionHandler: nil)
        }
    }
    
    /*************************
     Method Name:  locationManager()
     Parameter: CLLocationManager, CLLocation
     return type: nil
     Desc: This function cis called when didUpdateLocations.
     *************************/
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        manager.stopMonitoringSignificantLocationChanges()
        manager.stopUpdatingLocation()
        str_latitude = (String(locations.last!.coordinate.latitude))
        str_longitude = (String(locations.last!.coordinate.longitude))
        GetAddress().getAddressFromLocation(locations.last!) { (address) in
            self.txtLocation.txtFld_UserValue.text! = address
        }
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
}

extension SignUpViewController: CountryPickerViewDelegate {
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        // Only countryPickerInternal has it's delegate set
        let title = "Selected Country"
        let message = "Name: \(country.name) \nCode: \(country.code) \nPhone: \(country.phoneCode)"
        //        self.countryCode = country.phoneCode
        self.countryCode = "\(country.code)\(country.phoneCode)"
        print(self.countryCode)
    }
}

extension SignUpViewController: CountryPickerViewDataSource {
    func preferredCountries(in countryPickerView: CountryPickerView) -> [Country] {
        if countryPickerView.tag == MobileNo.tag {
            var countries = [Country]()
            ["NG", "US", "GB"].forEach { code in
                if let country = countryPickerView.getCountryByCode(code) {
                    countries.append(country)
                }
            }
            return countries
        }
        return []
    }
    
    func sectionTitleForPreferredCountries(in countryPickerView: CountryPickerView) -> String? {
        if countryPickerView.tag == MobileNo.tag {
            return "Preferred title"
        }
        return nil
    }
    
    func navigationTitle(in countryPickerView: CountryPickerView) -> String? {
        return "Select a Country"
    }
    
 
}

class TextField: UITextField {
    
    let padding = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 100)
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
