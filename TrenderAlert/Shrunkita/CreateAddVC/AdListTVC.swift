//
//  AdListTVC.swift
//  TrenderAlert
//
//  Created by Riken Shah on 19/07/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class AdListTVC: UITableViewCell {

    @IBOutlet weak var lblExpiresIn: UILabel!
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgProfilePIC: UIImageView!
    @IBOutlet weak var btnStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.btnStatus.makeCornerRadius(radius: 4)
        let path = UIBezierPath(ovalIn: imgProfilePIC.bounds)
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        imgProfilePIC.layer.mask = maskLayer
//        self.imgProfilePIC.layer.cornerRadius = 0.5 * self.imgProfilePIC.bounds.size.width
//        self.imgProfilePIC.layer.masksToBounds = false
//        self.imgProfilePIC.clipsToBounds = true
//        self.imgProfilePIC.makeCornerRadius(radius: self.imgProfilePIC.layer.frame.height/2)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
