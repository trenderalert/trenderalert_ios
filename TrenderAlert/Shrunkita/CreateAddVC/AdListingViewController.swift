//
//  AdListingViewController.swift
//  TrenderAlert
//
//  Created by Riken Shah on 19/07/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class AdListingViewController: UIViewController {

    @IBOutlet var lblNoData: UILabel!
    @IBOutlet weak var adListView: UITableView!
    var advertiseList = AdvertiseDataModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerTableViewCells()
        self.lblNoData.isHidden = true
        setupNavigationbar()
        
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "CreateAdViewController") as! CreateAdViewController
        destination.delegate = self
        self.navigationController?.pushViewController(destination, animated: false)
        // Do any additional setup after loading the view.
    }
    
    lazy var refreshControl:UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(_:)), for: .valueChanged)
        refreshControl.tintColor = UIColor.red
        return refreshControl
    }()
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        getAllAdvertise()
        refreshControl.endRefreshing()
    }
    
    func registerTableViewCells() {
        adListView.register(UINib(nibName: "AdListTVC", bundle: nil), forCellReuseIdentifier: "AdListTVC")
        adListView.delegate = self
        adListView.dataSource = self
        adListView.reloadData()
        getAllAdvertise()
    }

    func setupNavigationbar(){
        
        let leftBarButton = UIButton()
        leftBarButton.setImage(#imageLiteral(resourceName: "img_MenuBarBtn"), for: .normal)
        leftBarButton.addTarget(self, action: #selector(didTapMenu), for: .touchUpInside)
        let item1 = UIBarButtonItem()
        item1.customView = leftBarButton
        self.navigationItem.leftBarButtonItems = [item1]
        
        let rightBarButton = UIButton()
        rightBarButton.setImage(UIImage(named: "add_white"), for: .normal)
        rightBarButton.addTarget(self, action: #selector(didTapCreate), for: .touchUpInside)
        let item2 = UIBarButtonItem()
        item2.customView = rightBarButton
        self.navigationItem.rightBarButtonItems = [item2]
        
        Utilities.setNavigationBarWithTitle(viewController: self, title: MySingleton.shared.selectedLangData.Advertise_List)
      
    }
    
    @objc func didTapMenu(_ sender: UIBarButtonItem) {
        if self.menuContainerViewController != nil {
            self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
        }
        
    }
    
    @objc func didTapCreate(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "CreateAdViewController") as! CreateAdViewController
        destination.delegate = self
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    func getAllAdvertise()
    {
        WebServices().callUserService1(service: UserServices.getAdvertiseList, urlParameter: "", parameters: nil, isLazyLoading: false, isHeader: true, CallMethod: .get, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
            DispatchQueue.main.async{
            if(serviceResponse["Status"] as! Bool == true)
            {
                self.advertiseList = WebServices().decodeDataToClass(data: serviceData, decodeClass: AdvertiseDataModel.self)!
                if self.advertiseList.data.count != 0
                {
                   self.adListView.reloadData()
                    self.lblNoData.isHidden = true
                }
                else
                {
                    self.lblNoData.isHidden = false
                }
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
                
            }
        })
    }
    
}

extension AdListingViewController:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.advertiseList.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AdListTVC", for: indexPath) as! AdListTVC
        if self.advertiseList.data[indexPath.row].Thumbnail != nil
        {
            cell.imgProfilePIC.sd_setImage(with: URL(string: self.advertiseList.data[indexPath.row].Thumbnail!), placeholderImage: #imageLiteral(resourceName: "Img_LogoNly"), options: .delayPlaceholder, completed: nil)
        }
        if self.advertiseList.data[indexPath.row].Status! == 0
        {
            cell.btnStatus.text = MySingleton.shared.selectedLangData.pending
        }
        else if self.advertiseList.data[indexPath.row].Status! == 1
        {
            cell.btnStatus.text = MySingleton.shared.selectedLangData.approved
        }
        else
        {
            cell.btnStatus.text = MySingleton.shared.selectedLangData.rejected
        }
        cell.lblTitle.text = self.advertiseList.data[indexPath.row].Title!
        if self.advertiseList.data[indexPath.row].DateCreated != nil
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
            let date = dateFormatter.date(from: self.advertiseList.data[indexPath.row].DateCreated)!
            dateFormatter.dateFormat = "MMM dd, yyyy"
            cell.lblStartDate.text = "\(MySingleton.shared.selectedLangData.created_on): \(dateFormatter.string(from: date))"
        }
        else
        {
            cell.lblStartDate.text = "-"
        }
        if self.advertiseList.data[indexPath.row].ExpireIn != nil
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
            let date = dateFormatter.date(from: self.advertiseList.data[indexPath.row].ExpireIn)!
            dateFormatter.dateFormat = "MMM dd, yyyy"
            cell.lblExpiresIn.text = "\(MySingleton.shared.selectedLangData.expires_in): \(dateFormatter.string(from: date))"
        }
        else
        {
            cell.lblStartDate.text = "-"
        }
        return cell
    }
    
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let cell = tableView.cellForRow(at: indexPath)
//        cell?.contentView.backgroundColor = UIColor.baseColor
//
//    }
}
extension AdListingViewController : createAdDelegate{
    func createAdAction() {
        getAllAdvertise()
    }
    
    
}
