//
//  AdvertiseDataModel.swift
//  TrenderAlert
//
//  Created by Riken Shah on 05/08/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

class AdvertiseDataModel: NSObject, Codable {
    var data = [AdvertiseList]()
    
    enum CodingKeys: String, CodingKey {
        case data
    }
}

class AdvertiseList: NSObject, Codable {
    var AdPosition = Int()
    var Title : String?
    var Description : String?
    var Status : Int?
    var Thumbnail : String?
    var Video : String?
    var DateCreated = String()
    var IsPaid = Bool()
    var UpdatedOn = String()
    var ExpireIn = String()
    
    enum CodingKeys: String, CodingKey {
        case AdPosition
        case Title
        case Description
        case Status
        case Thumbnail
        case Video
        case DateCreated
        case IsPaid
        case UpdatedOn
        case ExpireIn
    }
}
