//
//  CommentHeader.swift
//  TrenderAlert
//
//  Created by Riken Shah on 14/08/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import ActiveLabel

class CommentHeader: UITableViewHeaderFooterView {
 
    @IBOutlet var btnPlayPause: UIButton!
    @IBOutlet var cnstAudioHeight: NSLayoutConstraint!
    @IBOutlet var lblAudioTime: UILabel!
    @IBOutlet var audioSlider: UISlider!
    @IBOutlet var viewAudioComment: UIView!
    @IBOutlet var viewUserComment: UIView!
    @IBOutlet var lblUserComment: ActiveLabel!
    @IBOutlet var imgUserPic: UIImageView!
    @IBOutlet var btnLike: UIButton!
    @IBOutlet var lblExpires: UILabel!
    @IBOutlet var imgReply: UIImageView!
    @IBOutlet var cnstReplyImgHeight: NSLayoutConstraint!
    @IBOutlet var cnstReplyWidth: NSLayoutConstraint!
}
