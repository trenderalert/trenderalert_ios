//
//  CommentViewController.swift
//  TrenderAlert
//
//  Created by OSP LABS on 10/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import SwiftOnoneSupport
import AVFoundation
import AGEmojiKeyboard
import MBProgressHUD
import SDWebImage
import GrowingTextView
import Alamofire
import ActiveLabel
import DropDown
import SwiftyGif

protocol TrendCommentDelegate {
    func commentCount(Index: Int,CommentCount: Int)
}

class CommentViewController: UIViewController, AGEmojiKeyboardViewDelegate, AGEmojiKeyboardViewDataSource,GrowingTextViewDelegate,UITextViewDelegate,ActiveLabelDelegate,UITextFieldDelegate{
   
    @IBOutlet weak var searchGif: UITextField!
    @IBOutlet weak var cnstGIfHeight: NSLayoutConstraint!
    @IBOutlet weak var cnstSearchGIfHeight: NSLayoutConstraint!
    @IBOutlet weak var viewGIFSearch: UIView!
    @IBOutlet weak var viewGIf: UIView!
    @IBOutlet var btnRecord: UIButton!
    @IBOutlet var txtCommentView: GrowingTextView!
    @IBOutlet var recordingTimerlabel: UILabel!
    @IBOutlet var viewOptions: UIView!
    @IBOutlet var cnstGifHeight: NSLayoutConstraint!
    @IBOutlet var collectionGifImg: UICollectionView!
    @IBOutlet var btnCamera: UIButton!
//    @IBOutlet weak var txtCommentView: UITextView!
    @IBOutlet weak var tblComment: UITableView!
    @IBOutlet weak var viewReply: UIView!
    @IBOutlet weak var viewComment: UIView!
    var trendId = Int()
    var gifId = Int()
    let picker = UIImagePickerController()
    var CommentData = CommentListData()
    var delegate : TrendCommentDelegate?
    var imgCollection = [AnyObject]()
    var emojiImages = [#imageLiteral(resourceName: "time_clk"),
                       #imageLiteral(resourceName: "emoji_clk"),
                       #imageLiteral(resourceName: "bell_clk"),
                       #imageLiteral(resourceName: "flower_clk"),
                       #imageLiteral(resourceName: "car_clk"),
                       #imageLiteral(resourceName: "Recent_clk")]
    var emojiSelectedIndex = -1
    var emojiNotSelectedIndex = -1
    var txtData = String()
    var imgComment : UIImage? = nil
    var agEmojiKeyboardView = AGEmojiKeyboardView()
    var chatroom = String()
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var audioRecorderPlayer : AVPlayer!
    var audioPlayer : AVAudioPlayer!
    var timer : Timer?
    var timeInMin = 600
    var audioFilename : URL!
    var cache = [UICollectionViewLayoutAttributes]()
    var duration = String()
    var updateTimerRecording = Timer()
    var updateTimerForDownloadedRecording = Timer()
    var updateTimerForRecorded = Timer()
    var audioPlayIndex = Int()
    var friendListObj = FriendList()
    var friendListObjData = FriendList()
    var searchText = ""
    var dropDown = DropDown()
    var tagUsersList = [Int]()
    var tagString = String()
    var trendIndex = Int()
    var commentCount = Int()

    override func viewDidLoad() {
        super.viewDidLoad()
       automaticallyAdjustsScrollViewInsets = false
      
        SocketIoManager.sharedInstance.socketIOManagerDelegate = self
        
        let paramSocket = ["trendId":self.trendId]
        SocketIoManager.sharedInstance.sendDataToEvent(.connectComment, data: paramSocket)
        
        getAllComments()
        initialSetup()
        // Do any additional setup after loading the view.
    }
    
    func setupNavaigationbar(){
        Utilities.setNavigationBarWithTitle(viewController: self, title: "")
        let leftButton = UIBarButtonItem(image: UIImage(named: "img_BackArrow"), style: .plain, target: self, action: #selector(goBack))
        navigationItem.leftBarButtonItem = leftButton
    }
    
    @objc func goBack(){
        self.updateTimerForRecorded.invalidate()
        delegate?.commentCount(Index: trendIndex, CommentCount: self.commentCount)
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillLayoutSubviews()
    {
//       self.viewOptions.makeCornerRadius(radius: self.viewOptions.layer.frame.size.height/2)
        self.collectionGifImg.collectionViewLayout.invalidateLayout()
        self.collectionGifImg.layoutSubviews()
        self.txtCommentView.makeCornerRadius(radius: 18)
    }
    
    func dropdownfolderSelection()
    {
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            let textFieldText: String = self.txtCommentView.text!
            if textFieldText.contains("@") {
                self.recursiveTextDrop(textViewText: textFieldText,item: item,index:index)
                return
            }
            else
            {
                self.txtCommentView.text! = self.txtCommentView.text! + item
                self.tagUsersList.append(self.friendListObj.data[index].UserProfileID)
            }
             self.dropDown.hide()
        }
    }
    
//    @IBAction func btnBackAction(_ sender: Any)
//    {
//        self.navigationController?.popViewController(animated: true)
//    }
    
    
    func recursiveTextDrop(textViewText : String,item:String,index:Int)
    {
        let endIndex = textViewText.range(of: "@")!.upperBound
        let textFieldTextNew = textViewText.substring(to: endIndex).trimmingCharacters(in: .whitespacesAndNewlines)
        print(textFieldTextNew)
        
        if textFieldTextNew.contains("@") {
            let endIndex = textViewText.range(of: "@")!.upperBound
            let textFieldTextNe = textViewText.substring(from: endIndex).trimmingCharacters(in: .whitespacesAndNewlines)
            print(textFieldTextNe)
            if textFieldTextNe.contains("@")
            {
                self.tagString = self.tagString + textFieldTextNew
                 recursiveTextDrop(textViewText: textFieldTextNe, item: item, index: index)
                return
            }
            else
            {
                
            }
        }
        print("after recursive \(textFieldTextNew)")
        self.txtCommentView.text! = self.tagString + textFieldTextNew + item
        self.tagUsersList.append(self.friendListObj.data[index].UserProfileID)
        print(self.tagUsersList)
       // dropdownfolderSelection()
    }
    
    func initialSetup()
    {
        dropDown.anchorView = self.txtCommentView
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)
        dropdownfolderSelection()

//        self.collectionGifImg.delegate = self
//        self.collectionGifImg.dataSource = self
        self.txtCommentView.placeholder = MySingleton.shared.selectedLangData.write_a_comment
        self.txtCommentView.placeholderColor = UIColor.lightGray
        self.txtCommentView.textContainerInset = UIEdgeInsets(top: 8, left: 8, bottom: 0, right: 100)
    //    self.txtCommentView.translatesAutoresizingMaskIntoConstraints = true
        self.txtCommentView.delegate = self
      //  self.txtCommentView.layer.cornerRadius = 4.0
        self.recordingTimerlabel.isHidden = true
        self.picker.delegate = self
        self.cnstGIfHeight.constant = 0
        self.cnstSearchGIfHeight.constant = 0
        self.searchGif.delegate = self
        self.viewGIFSearch.makeCornerRadius(radius: self.viewGIFSearch.frame.height/2)
        self.viewGIFSearch.giveBorderColor(color: UIColor.darkGray)
        setupNavaigationbar()
        Utilities.setNavigationBar(viewController: self)
        self.tblComment.tableFooterView = UIView()
        self.txtCommentView.giveBorderColor(color: .lightGray)
        self.txtCommentView.makeCornerRadius(radius: self.txtCommentView.layer.frame.size.height/2)
        self.txtCommentView.delegate = self
        
        self.tblComment.register(UINib(nibName: "AudioCommentTableViewCell", bundle: nil), forCellReuseIdentifier: "AudioCommentTableViewCell")
        self.tblComment.register(UINib(nibName: "CommentsTableViewCell", bundle: nil), forCellReuseIdentifier: "CommentsTableViewCell")
        self.tblComment.register(UINib(nibName: "OthersCommentTableViewCell", bundle: nil), forCellReuseIdentifier: "OthersCommentTableViewCell")
        self.collectionGifImg.register(UINib(nibName: "SelectImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SelectImageCollectionViewCell")
       
        let paramSocket = ["trendId":self.trendId]
        SocketIoManager.sharedInstance.sendDataToEvent(.connectComment, data: paramSocket)
        
    }
    
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        print("Add line")
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in
            self.view.layoutIfNeeded()
        }, completion: nil)
//        UIView.animate(withDuration: 0.2) {
//            self.view.layoutIfNeeded()
//        }
    }
    
    func didSelect(_ text: String, type: ActiveType) {
        print(text)
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
        
        if textView.text.isEmpty
        {
            self.tagUsersList.removeAll()
        }
    
        if self.txtCommentView.inputView == agEmojiKeyboardView
        {
            self.txtData = self.txtCommentView.text
           // self.txtCommentView.inputView = nil
            self.txtCommentView.inputView = UIView()
           // self.txtCommentView.keyboardType = UIKeyboardType.default
            self.txtCommentView.keyboardType = UIKeyboardType.default
            self.txtCommentView.keyboardAppearance = .default
            self.txtCommentView.becomeFirstResponder()
            self.txtCommentView.text = self.txtData
        }
        else
        {
            var textFieldText: String = textView.text
           
            if textFieldText.contains("@") {
                
                recursiveText(textViewText: textFieldText)
            }
            else
            {
                self.dropDown.hide()
            }
        }
    }
    
    func recursiveTextSend(textViewText : String)
    {
        print("before recursive send \(textViewText)")
      
//        let endIndexLower = textViewText.range(of: "@")!.lowerBound
//        let textFieldTextNewLower = textViewText.substring(from: endIndexLower)
//        print(textFieldTextNewLower)
        let endIndex = textViewText.range(of: "@")!.upperBound
        let textFieldTextNew = textViewText.substring(from: endIndex)
        print(textFieldTextNew)
        
        if textFieldTextNew.contains("@") {
            recursiveTextSend(textViewText: textFieldTextNew)
            return
        }

        let char: Character = "@"
        let sensitiveCount = self.txtCommentView.text.characters.filter { $0 == char }.count
        print("@ Count \(sensitiveCount)")

       if sensitiveCount > 1
       {
        if textFieldTextNew.contains(" ")
        {
            let text = textFieldTextNew.dropLast()
            for i in 0...self.friendListObjData.data.count-1
            {
                let name = "\(self.friendListObjData.data[i].Firstname!)"+"\(self.friendListObjData.data[i].Lastname!)"
                if name == text
                {
                    return
                }
            }
            self.tagUsersList.append(0)
            print(self.tagUsersList)
        }
        }
//         getFriendList(Skip: 0, Take: 15, lazyLoading: true,SearchString: textViewText)
    
    }
 
    func recursiveText(textViewText : String)
    {
        print("before recursive \(textViewText)")

            let endIndex = textViewText.range(of: "@")!.upperBound
            let textFieldTextNew = textViewText.substring(from: endIndex)
            print(textFieldTextNew)

            if textFieldTextNew.contains("@") {
                recursiveText(textViewText: textFieldTextNew)
                return
            }
        
            print("after recursive \(textFieldTextNew)")
        
            getFriendList(Skip: 0, Take: 15, lazyLoading: true,SearchString: textFieldTextNew)
                
    }
    
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
      //  ...
    }
    
    
    // MARK: - get all comments from api
    
    func getAllComments()
    {
        let param = ["TrendID":self.trendId]
        WebServices().callUserService(service: UserServices.getAllCommentsByTrend, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, responseData) in
            print(serviceResponse)
           // self.getAllGIF()
            if(serviceResponse["Status"] as! Bool == true)
            {
                DispatchQueue.main.async {
                self.CommentData.data.removeAll()
                 self.CommentData = WebServices().decodeDataToClass(data: responseData, decodeClass: CommentListData.self)!
                if self.CommentData.data.count != 0
                {
                    self.commentCount = self.CommentData.data.count
                    self.tblComment.delegate = self
                    self.tblComment.dataSource = self
                    self.tblComment.reloadData()
               //     self.tblComment.reloadData()
                    
                    self.tblComment.performBatchUpdates(nil, completion: {
                        (result) in
                        // ready
                        print("loaded")
                        self.tblComment.scrollToRow(at: IndexPath(row: self.CommentData.data.count - 1, section: 0), at: .bottom, animated: false)
                    })
                }
                else
                {
                    self.commentCount = self.CommentData.data.count
                    self.txtCommentView.resignFirstResponder()
                    
                }
                self.getFriendListData(lazyLoading: true)
            }
            }
            else
            {
                 TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        })
    }
    
    // MARK: - get all gif from api
    func getAllGIF(Skip: Int, Take: Int, lazyLoading: Bool,SearchString:String = "")
    {
        self.imgCollection.removeAll()
      //  self.searchGif.text = ""
        let param = ["SearchTag":SearchString,
                     "SortColumn":"",
                     "SortDirection":"",
                     "Skip":"0",
                     "Take":"10"]
        WebServices().callUserService(service: UserServices.getAllGIF, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: true, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, responseData) in
            print(serviceResponse)
            if(serviceResponse["Status"] as! Bool == true)
            {
                DispatchQueue.main.async {
                    if (serviceResponse["data"] as! [AnyObject]).count != 0
                    {
                        
                        self.imgCollection.removeAll()
                        self.imgCollection = (serviceResponse["data"] as! [AnyObject])
                        self.collectionGifImg.delegate = self
                        self.collectionGifImg.dataSource = self
                        self.collectionGifImg.reloadData()
                    }
                    else
                    {
                        self.imgCollection.removeAll()
                        self.collectionGifImg.reloadData()
                       // self.txtCommentView.resignFirstResponder()
                    }
                }
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        })
    }
    
    // MARK: - Save comment
    
    func addComment()
    {
        self.view.endEditing(true)
        var param = [String : Any]()
        
//                if self.txtCommentView.text!.contains("@")
//                {
//                    for i in 0...self.friendListObjData.data.count-1
//                    {
//                        let name = "\(self.friendListObjData.data[i].Firstname!)"+"\(self.friendListObjData.data[i].Lastname!)"
//                        if name != self.txtCommentView.text!
//                        {
//                            self.tagUsersList.append(0)
//                            print(self.tagUsersList)
//                        }
//                    }
//                }
        
        
        if self.imgComment != nil
        {
            var imageList = [AnyObject]()
            let imageData = ["FileExt":"jpg","contenttype":"image/jpg","ImageData":"data:image/jpg;base64,\(encodeToBase64String(image: self.imgComment!))"]
            imageList.append(imageData as AnyObject)
            param = ["TrendID":self.trendId,"CommentId":"0","CommentText":self.txtCommentView.text! ,"IsImage":"true",
                     "ImagePath":["Images": imageList],"gifID":"","TaggedUsers":self.tagUsersList] as [String : Any]
          //  print(param)
          
            callApi(requestData: param, url: "\(WebServices.baseURL)api/comment/ManageComments")
            return
        }
        else
        {
            param = ["TrendID":self.trendId,"CommentId":"0","CommentText":self.txtCommentView.text! ,"IsImage":"false",
                     "ImagePath":[],"gifID":self.gifId,"TaggedUsers":self.tagUsersList] as [String : Any]
        }
        if self.txtCommentView.text == "" && self.gifId == 0
        {
            return
        }
         self.cnstGIfHeight.constant = 0
        WebServices().callUserService1(service: UserServices.addComment, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
            if(serviceResponse["Status"] as! Bool == true)
            {
                 DispatchQueue.main.async {
                let loginObject = Login()
                let data = serviceResponse["data"] as! [AnyObject]
                var socketData = ["chatroom":self.chatroom,
                                  "Id":data[0]["CommentID"] as! Int,
                                  "UserProfileId":MySingleton.shared.loginObject.UserID,
                                  "IsReply":0] as [String : Any]
                  
                SocketIoManager.sharedInstance.sendDataToEvent(.message, data: socketData)
                self.CommentData.data.removeAll()
                self.imgCollection.removeAll()
                self.tagUsersList.removeAll()
                self.audioPlayIndex = 0
                self.getAllComments()
                self.updateTimerForRecorded.invalidate()
                self.gifId = 0
                self.imgComment = nil
                self.txtCommentView.text = ""
                self.txtCommentView.placeholder = MySingleton.shared.selectedLangData.write_a_comment
              }
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        })
    }
    
    func callApi(requestData: Dictionary<String, Any>, url: String)
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
      
        var urlStr: String = url
        
        urlStr = urlStr.replacingOccurrences(of: " ", with: "&")
        let request :NSMutableURLRequest = NSMutableURLRequest(url:NSURL(string:urlStr)! as URL)
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        if requestData.count != 0
        {
            do
            {
                let jsonData = try JSONSerialization.data(withJSONObject: requestData, options: JSONSerialization.WritingOptions.prettyPrinted)
                request.httpBody = jsonData
             //   print(requestData)
            }
            catch let error as NSError
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                }
            }
        }
        let TokenValue = "Bearer " + MySingleton.shared.loginObject.access_token
        print(TokenValue)
        request.setValue(TokenValue, forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        let task = URLSession.shared.dataTask(with: request as URLRequest){ data,response,error in
            
            if error != nil
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error!.localizedDescription, completionHandler: nil)
                }
                
            }
            do
            {
                if (response as? HTTPURLResponse) != nil
                {
                    
                    //////////////////// JSON RESPONSE /////////////////
                    
                    let jsonString = String(data: data!, encoding: .utf8)
                    print(jsonString!)
                    
                    //////////////////// JSON RESPONSE /////////////////
                    
                    if data != nil
                    {
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        
                        print(jsonData)
                        
                        
                        if(jsonData["Status"] as! Bool == true)
                        {
                              let data = jsonData["data"] as! [AnyObject]
                             DispatchQueue.main.async {
                                var socketData = ["chatroom":self.chatroom,
                                              "Id":data[0]["CommentID"] as! Int,
                                              "UserProfileId":MySingleton.shared.loginObject.UserID,
                                              "IsReply":0] as [String : Any]
                             
                            SocketIoManager.sharedInstance.sendDataToEvent(.message, data: socketData)
                           
                            self.CommentData.data.removeAll()
                            self.getAllComments()
                            self.gifId = 0
                            self.tagUsersList.removeAll()
                            self.audioPlayIndex = 0
                            self.imgCollection.removeAll()
                            self.updateTimerForRecorded.invalidate()
                            self.imgComment = nil
                            self.txtCommentView.text = ""
                            self.txtCommentView.placeholder = MySingleton.shared.selectedLangData.write_a_comment
                            self.cnstGIfHeight.constant = 0
                          }  
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                MBProgressHUD.hide(for: self.view, animated: true)
                                TrenderAlertVC.shared.presentAlertController(message: jsonData["Message"] as! String, completionHandler: nil)
                            }
                        }
                        
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true)
                            TrenderAlertVC.shared.presentAlertController(message: error!.localizedDescription, completionHandler: nil)
                        }
                    }
                    
                }
            }
            catch let error as NSError
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                }
            }
        }
        task.resume()
    }
    
    
    @IBAction func btn_AddCommentAction(_ sender: Any)
    {
//        if self.txtCommentView.text != ""
//        {
            addComment()
  //      }
    }
    
    
    @IBAction func btn_EmojiKeyboardAction(_ sender: UIButton)
    {
        self.view.endEditing(true)
        agEmojiKeyboardView = AGEmojiKeyboardView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216), dataSource: self)
        agEmojiKeyboardView.autoresizingMask = .flexibleHeight
        agEmojiKeyboardView.delegate = self
        self.txtCommentView.inputView = agEmojiKeyboardView
        self.txtCommentView.becomeFirstResponder()
        self.txtCommentView.text = self.txtData
    }
    
    // MARK: - Emoji Delegate
    func emojiKeyBoardView(_ emojiKeyBoardView: AGEmojiKeyboardView!, didUseEmoji emoji: String!) {
        self.txtCommentView.text = self.txtCommentView.text + emoji
    }
    
    func emojiKeyBoardViewDidPressBackSpace(_ emojiKeyBoardView: AGEmojiKeyboardView!) {
        self.txtCommentView.text = String(self.txtCommentView.text.dropLast())
    }

    // MARK: - Emoji Data Source
    func emojiKeyboardView(_ emojiKeyboardView: AGEmojiKeyboardView!, imageForSelectedCategory category: AGEmojiKeyboardViewCategoryImage) -> UIImage! {
        if emojiSelectedIndex <= 4 {
            emojiSelectedIndex = emojiSelectedIndex + 1
        }
        return emojiImages[emojiSelectedIndex]
    }
    
    func emojiKeyboardView(_ emojiKeyboardView: AGEmojiKeyboardView!, imageForNonSelectedCategory category: AGEmojiKeyboardViewCategoryImage) -> UIImage! {
        if emojiNotSelectedIndex <= 4 {
            emojiNotSelectedIndex = emojiNotSelectedIndex + 1
        }
        return emojiImages[emojiNotSelectedIndex]
    }
    
    func backSpaceButtonImage(for emojiKeyboardView: AGEmojiKeyboardView!) -> UIImage! {
        return #imageLiteral(resourceName: "backspace_clk")
    }
    
    @objc func proceedToReply(_ sender: UIButton)
    {
        let storyboard = UIStoryboard(name: "Comment", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "ReplyViewController") as! ReplyViewController
        self.updateTimerForRecorded.invalidate()
        destination.delegate = self
        let commentData = ["tagUsers":self.self.CommentData.data[sender.tag].TaggedUsers,"chatRoom":self.chatroom,"audioDuration":self.CommentData.data[sender.tag].Duration,"commentAudio":self.CommentData.data[sender.tag].CommentAudio ?? "","expiry": self.CommentData.data[sender.tag].TimeAgo,"canDelete":self.CommentData.data[sender.tag].CanDelete,"isLike":self.CommentData.data[sender.tag].IsLiked,"trendId":self.trendId,"firstName": self.CommentData.data[sender.tag].UserFirstname,"lastName": self.CommentData.data[sender.tag].UserLastname,"commentId": self.CommentData.data[sender.tag].CommentID,"commentName": self.CommentData.data[sender.tag].CommentText ?? "","commentProfile": self.CommentData.data[sender.tag].ProfileImage ?? "","commentImg": self.CommentData.data[sender.tag].CommentImage ?? ""] as [String:Any]
        destination.CommentList = commentData
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @objc func proceedToLikeComment(_ sender: UIButton)
    {
        let cell_indexPath = NSIndexPath(row: sender.tag, section: 0)
        let tableViewCell = self.tblComment.cellForRow(at: cell_indexPath as IndexPath) as! CommentsTableViewCell
        
        var param = [String : Any]()
        
        param = [ "CommentID":self.CommentData.data[sender.tag].CommentID,
                  "TrendID":self.trendId] as [String : Any]
        
        WebServices().callUserService(service: UserServices.likeUnLikeComment, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
            if(serviceResponse["Status"] as! Bool == true)
            {
                DispatchQueue.main.async {
                
                if tableViewCell.btnLike.currentTitleColor == UIColor.black
                {
                  tableViewCell.btnLike.setTitleColor(UIColor(displayP3Red: 255.0/255.0, green: 83.0/255.0, blue: 119.0/255.0, alpha: 1.0), for: .normal)
                    self.CommentData.data[sender.tag].IsLiked = true
                }
                else
                {
                     tableViewCell.btnLike.setTitleColor(UIColor.black, for: .normal)
                    self.CommentData.data[sender.tag].IsLiked = false
                }
                //self.getAllComments()
            }
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        })
    }
    
    @objc func proceedToLikeCommentOhersCell(_ sender: UIButton)
    {
        let cell_indexPath = NSIndexPath(row: sender.tag, section: 0)
        let tableViewCell = self.tblComment.cellForRow(at: cell_indexPath as IndexPath) as! OthersCommentTableViewCell
        
        var param = [String : Any]()
        
        param = [ "CommentID":self.CommentData.data[sender.tag].CommentID,
                  "TrendID":self.trendId] as [String : Any]
        
        WebServices().callUserService(service: UserServices.likeUnLikeComment, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
            if(serviceResponse["Status"] as! Bool == true)
            {
                DispatchQueue.main.async {
                if tableViewCell.btnLike.currentTitleColor == UIColor.black
                {
                tableViewCell.btnLike.setTitleColor(UIColor(displayP3Red: 255.0/255.0, green: 83.0/255.0, blue: 119.0/255.0, alpha: 1.0), for: .normal)
                }
                else
                {
                     tableViewCell.btnLike.setTitleColor(UIColor.black, for: .normal)
                }
                }
                //self.getAllComments()
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        })
    }
    
    @objc func proceedToLikeRply(_ sender: UIButton)
    {
        let cell_indexPath = NSIndexPath(row: sender.tag, section: 0)
        let tableViewCellOther = self.tblComment.cellForRow(at: cell_indexPath as IndexPath) as! OthersCommentTableViewCell
        
        var param = [String : Any]()
        
        param = [ "ReplyID":self.CommentData.data[sender.tag].Replies[0].ReplyID] as [String : Any]
        
        WebServices().callUserService(service: UserServices.likeUnLikeReply, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
            if(serviceResponse["Status"] as! Bool == true)
            {
                DispatchQueue.main.async {
                if tableViewCellOther.btnLike.currentTitleColor == UIColor.black
                {
                tableViewCellOther.btnLike.setTitleColor(UIColor(displayP3Red: 255.0/255.0, green: 83.0/255.0, blue: 119.0/255.0, alpha: 1.0), for: .normal)
                }
                else
                {
                     tableViewCellOther.btnLike.setTitleColor(UIColor.black, for: .normal)
                }
                }
                //self.getAllComments()
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        })
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        var textFieldText: String = textView.text
        self.searchText = textFieldText
         //let endIndex = textFieldText.range(of: "@")!.lowerBound
//            if textFieldText.contains("@") {
//                let endIndex = textFieldText.range(of: "@")!.upperBound
//                textFieldText = textFieldText.substring(to: endIndex).trimmingCharacters(in: .whitespacesAndNewlines)
//            print(textFieldText)
//
//
////                if let dotRangeA = textFieldText.range(of: "@")?.upperBound {
////                let afterEqualsTo = String(textFieldText.suffix(dotRangeA))
////                print(afterEqualsTo)
////            }
//                //Get the string up to and after the @ symbol
//                getFriendList(Skip: 0, Take: 15, lazyLoading: true,SearchString: textFieldText)
//        }
        return true
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
     if textView.text == MySingleton.shared.selectedLangData.write_a_comment
        {
          textView.text = ""
            textView.textColor = UIColor.black
        }
        if self.txtCommentView.inputView == agEmojiKeyboardView
        {
            self.txtData = self.txtCommentView.text
            self.txtCommentView.inputView = nil
         //   self.txtCommentView.inputView = UIView()
         //   self.txtCommentView.keyboardType = UIKeyboardType.default
//self.txtCommentView.keyboardAppearance = .default
            self.txtCommentView.becomeFirstResponder()
            self.txtCommentView.text = self.txtData
        }
        
    }
    
    @IBAction func btn_CameraRecordAction(_ sender: Any)
    {
        if self.audioRecorder == nil {
            self.startRecording()
            self.btnRecord.setImage(#imageLiteral(resourceName: "record"), for: .normal)
        }
        else {
            self.finishRecording(success: true)
            self.btnRecord.setImage(#imageLiteral(resourceName: "UnRecord"), for: .normal)
        }
    }
    
    
    @IBAction func btn_SetImageAction(_ sender: Any)
    {
        self.cnstGIfHeight.constant = 0
        self.view.endEditing(true)
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: MySingleton.shared.selectedLangData.please_select_your_option, message: "", preferredStyle: .actionSheet)

        let cameraActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.camera, style: .default) { void in
            print("Camera")
            self.openCamera()
        }
        actionSheetControllerIOS8.addAction(cameraActionButton)

        let galleryActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.gallery, style: .default) { void in
            print("Gallery")
            self.openGallary()
        }

        let cancelActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.cancel, style: .default) { void in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(galleryActionButton)
        actionSheetControllerIOS8.addAction(cancelActionButton)
        actionSheetControllerIOS8.popoverPresentationController?.sourceView = self.btnCamera
        actionSheetControllerIOS8.popoverPresentationController?.sourceRect = self.btnCamera.bounds
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    func encodeToBase64String(image: UIImage) -> String
    {
        let imageData = image.jpegData(compressionQuality: 0.5)
        let imag_str:String = imageData!.base64EncodedString(options:[])
        //print(imag_str)
        return imag_str
    }
    
    @IBAction func btn_GIFAction(_ sender: Any)
    {
        self.searchGif.text = ""
        if self.cnstGIfHeight.constant == 0
        {
            //self.imgComment
            getAllGIF(Skip: 0, Take: 15, lazyLoading: true)
            self.cnstGIfHeight.constant = 136
            self.cnstSearchGIfHeight.constant = 32
        }
        else
        {
            self.cnstGIfHeight.constant = 0
            self.cnstSearchGIfHeight.constant = 0
        }
    }
    
    @objc func longPressReply(_ sender: UILongPressGestureRecognizer)
    {
        print("longpressed")
      
        let touchPoint = sender.location(in: self.tblComment)
        if let indexPath = self.tblComment.indexPathForRow(at: touchPoint) {
            print("Long pressed row: \(indexPath.row)")
        }
        
        self.view.endEditing(true)
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: MySingleton.shared.selectedLangData.please_select_your_option, message: "", preferredStyle: .actionSheet)
        
                if let indexPath = self.tblComment.indexPathForRow(at: touchPoint) {
                    if !(self.CommentData.data[indexPath.row].Replies[0].ReplyImage == nil || self.CommentData.data[indexPath.row].Replies[0].ReplyAudio == nil)
                    {
                        let cameraActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.edit, style: .default) { void in
                            let storyboard = UIStoryboard(name: "Comment", bundle: nil)
                            let destination = storyboard.instantiateViewController(withIdentifier: "EditCommentViewController") as! EditCommentViewController
                            let data = ["tagUsers":self.CommentData.data[indexPath.row].Replies[0].TaggedUsers,"replyId":self.CommentData.data[indexPath.row].Replies[0].ReplyID,"isComment":false,"trendId":self.trendId,"commentid":self.CommentData.data[indexPath.row].CommentID,"commentTxt":self.CommentData.data[indexPath.row].CommentText ?? ""] as [String : Any]
                            destination.data = data
                            destination.txtComment.text = self.CommentData.data[indexPath.row].Replies[0].ReplyText!
                            self.navigationController?.pushViewController(destination, animated: true)
                        }
                        actionSheetControllerIOS8.addAction(cameraActionButton)
                    }
              }
        
        
        let galleryActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.delete, style: .default) { void in
         //   let touchPoint = sender.location(in: self.tblComment)
            if let indexPath = self.tblComment.indexPathForRow(at: touchPoint) {
                print("Long pressed row reply: \(indexPath.row)")
                self.proceedToDeleteRply(indexPath: indexPath.row)
            }
        }
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.cancel, style: .default) { void in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(galleryActionButton)
        actionSheetControllerIOS8.addAction(cancelActionButton)
        actionSheetControllerIOS8.popoverPresentationController?.sourceView = self.tblComment
        actionSheetControllerIOS8.popoverPresentationController?.sourceRect = self.tblComment.bounds
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    @objc func longPressed(_ sender: UILongPressGestureRecognizer)
    {
        print("longpressed")
        let touchPoint = sender.location(in: self.tblComment)
        if let indexPath = self.tblComment.indexPathForRow(at: touchPoint) {
            print("Long pressed row: \(indexPath.row)")
        }
        self.view.endEditing(true)
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: MySingleton.shared.selectedLangData.please_select_your_option, message: "", preferredStyle: .actionSheet)
      
        if let indexPath = self.tblComment.indexPathForRow(at: touchPoint) {
            if (self.CommentData.data[indexPath.row].CommentImage == nil && self.CommentData.data[indexPath.row].CommentAudio == nil)
            {
                let cameraActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.edit, style: .default) { void in
                    let storyboard = UIStoryboard(name: "Comment", bundle: nil)
                    let destination = storyboard.instantiateViewController(withIdentifier: "EditCommentViewController") as! EditCommentViewController
                    destination.delegate = self
                    let data = ["tagUsers":self.CommentData.data[indexPath.row].TaggedUsers,"isComment":true,"trendId":self.trendId,"commentid":self.CommentData.data[indexPath.row].CommentID,"commentTxt":self.CommentData.data[indexPath.row].CommentText ?? ""] as [String : Any]
                    destination.data = data
                    //  destination.txtComment.text = self.CommentData.data[indexPath.row].CommentText
                    self.navigationController?.pushViewController(destination, animated: true)
                }
                actionSheetControllerIOS8.addAction(cameraActionButton)
            }
            
        }
      
        
        let galleryActionButton: UIAlertAction = UIAlertAction(title: "Delete", style: .default) { void in
            
      //      let tapLocation = sender.location(in: self.tblComment)
            if let tapIndexPath = self.tblComment.indexPathForRow(at: touchPoint) {
                if (self.tblComment.cellForRow(at: tapIndexPath) as? CommentsTableViewCell) != nil {
                    print("Long pressed row delete : \(tapIndexPath.row)")
                    
                    var param = [String : Any]()
                    
                    param = [ "CommentID":self.CommentData.data[tapIndexPath.row].CommentID,
                              "TrendID":self.trendId] as [String : Any]
                    
                    WebServices().callUserService(service: UserServices.deleteComment, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
                        print(serviceResponse)
                        if(serviceResponse["Status"] as! Bool == true)
                        {
                            self.CommentData.data.removeAll()
                            self.getAllComments()
                        }
                        else
                        {
                            // TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                        }
                    })
                   // sender.state = .ended
                }
            }
        }
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.cancel, style: .default) { void in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(galleryActionButton)
        actionSheetControllerIOS8.addAction(cancelActionButton)
        actionSheetControllerIOS8.popoverPresentationController?.sourceView = self.tblComment
        actionSheetControllerIOS8.popoverPresentationController?.sourceRect = self.tblComment.bounds
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    @objc func longPressedCommentReply(_ sender: UILongPressGestureRecognizer)
    {
        print("longpressed")
        let touchPoint = sender.location(in: self.tblComment)
        if let indexPath = self.tblComment.indexPathForRow(at: touchPoint) {
            print("Long pressed row: \(indexPath.row)")
        }
        self.view.endEditing(true)
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: MySingleton.shared.selectedLangData.please_select_your_option, message: "", preferredStyle: .actionSheet)
        
        if let indexPath = self.tblComment.indexPathForRow(at: touchPoint) {
            if (self.CommentData.data[indexPath.row].CommentImage == nil && self.CommentData.data[indexPath.row].CommentAudio == nil)
            {
                let cameraActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.edit, style: .default) { void in
                    let storyboard = UIStoryboard(name: "Comment", bundle: nil)
                    let destination = storyboard.instantiateViewController(withIdentifier: "EditCommentViewController") as! EditCommentViewController
                    destination.delegate = self
                    let data = ["isComment":true,"trendId":self.trendId,"commentid":self.CommentData.data[indexPath.row].CommentID,"commentTxt":self.CommentData.data[indexPath.row].CommentText ?? ""] as [String : Any]
                    destination.data = data
                    //  destination.txtComment.text = self.CommentData.data[indexPath.row].CommentText
                    self.navigationController?.pushViewController(destination, animated: true)
                }
                actionSheetControllerIOS8.addAction(cameraActionButton)
            }
            
        }
        
        
        let galleryActionButton: UIAlertAction = UIAlertAction(title: "Delete", style: .default) { void in
            
            //      let tapLocation = sender.location(in: self.tblComment)
            if let tapIndexPath = self.tblComment.indexPathForRow(at: touchPoint) {
                if (self.tblComment.cellForRow(at: tapIndexPath) as? OthersCommentTableViewCell) != nil {
                    print("Long pressed row delete : \(tapIndexPath.row)")
                    
                    var param = [String : Any]()
                    
                    param = [ "CommentID":self.CommentData.data[tapIndexPath.row].CommentID,
                              "TrendID":self.trendId] as [String : Any]
                    
                    WebServices().callUserService(service: UserServices.deleteComment, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
                        print(serviceResponse)
                        if(serviceResponse["Status"] as! Bool == true)
                        {
                            self.CommentData.data.removeAll()
                            self.getAllComments()
                        }
                        else
                        {
                            // TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                        }
                    })
                    // sender.state = .ended
                }
            }
        }
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.cancel, style: .default) { void in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(galleryActionButton)
        actionSheetControllerIOS8.addAction(cancelActionButton)
        actionSheetControllerIOS8.popoverPresentationController?.sourceView = self.tblComment
        actionSheetControllerIOS8.popoverPresentationController?.sourceRect = self.tblComment.bounds
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    func proceedToDeleteRply(indexPath: Int)
    {
        var param = [String : Any]()
        
        param = [ "ReplyID":self.CommentData.data[indexPath].Replies[0].ReplyID] as [String : Any]
        
        WebServices().callUserService(service: UserServices.deleteRply, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
            if(serviceResponse["Status"] as! Bool == true)
            {
                self.getAllComments()
            }
            else
            {
              //  TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        })
    }
    
    func proceedToDeleteComment(indexPath: Int)
    {
        var param = [String : Any]()
        
        param = [ "CommentID":self.CommentData.data[indexPath].CommentID,
                  "TrendID":self.trendId] as [String : Any]
        
        WebServices().callUserService(service: UserServices.deleteComment, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
            if(serviceResponse["Status"] as! Bool == true)
            {
                self.CommentData.data.removeAll()
                self.getAllComments()
            }
            else
            {
               // TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        })
    }
    
    // MARK: Audio submit and play methods start
    
    @IBAction func btnStartRecordingAction(_ sender: Any)
    {
        if self.audioRecorder == nil {
            self.startRecording()
            self.btnRecord.setImage(#imageLiteral(resourceName: "record"), for: .normal)
        }
        else {
            self.finishRecording(success: true)
            self.btnRecord.setImage(#imageLiteral(resourceName: "UnRecord"), for: .normal)
        }
    }
    
    
//    @objc func downloadFileFromURL(_ sender: UIButton){
//        let url = URL(fileURLWithPath: self.CommentData.data[sender.tag].CommentAudio!)
//        var downloadTask:URLSessionDownloadTask
//        downloadTask = URLSession.shared.downloadTask(with: url as URL, completionHandler: { (URL, response, error) -> Void in
//            //self.count += 1
//            if error != nil {
//                print("There was some problem with the Sound")
//            } else {
//                print(URL!)
////                DispatchQueue.main.async {
////                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
////                }
//                self.play(url: URL! as URL, tableView: "")
//              //  self.play(url: URL! as URL, indexpath: sender.tag)
//            }
//        })
//
//        downloadTask.resume()
//    }
    
//    @objc func updateTime() {
//
//        let cell_indexPath = NSIndexPath(row: self.audioPlayIndex, section: 0)
//        let tableViewCell = self.tblComment.cellForRow(at: cell_indexPath as IndexPath) as! CommentsTableViewCell
//
//        let currentTime = Int(self.audioRecorderPlayer.currentTime)
//        let duration = Int(self.audioRecorderPlayer.duration)
//        let total = currentTime - duration
//        let totalString = String(total)
//
//        let minutes = currentTime/60
//        let seconds = currentTime - minutes / 60
//
//        tableViewCell.lblTime.text = NSString(format: "%02d:%02d", minutes,seconds) as String
//        tableViewCell.audioSlider.value = Float(self.audioPlayer.currentTime)
//        print("Timer Update for recording list")
//
//
//    }
    
    
    @objc func updateTimeForRecordingSlider() {
        let cell_indexPath = NSIndexPath(row: self.audioPlayIndex, section: 0)
        let tableViewCell = self.tblComment.cellForRow(at: cell_indexPath as IndexPath) as? CommentsTableViewCell
        if tableViewCell?.audioPlayer.currentItem != nil
        {
            let currentTime = Int(Float(CMTimeGetSeconds((tableViewCell?.audioPlayer.currentItem!.currentTime())!)))
            let duration = Int(Float(CMTimeGetSeconds((tableViewCell?.audioPlayer.currentItem!.asset.duration)!)))
            let total = currentTime - duration
            let totalString = String(total)
            
            let minutes = currentTime/60
            let seconds = currentTime - minutes / 60
            print("Current time \(currentTime)")
            tableViewCell?.audioSlider.value = Float(CMTimeGetSeconds((tableViewCell?.audioPlayer.currentItem!.currentTime())!))
            tableViewCell?.lblTime.text = NSString(format: "%02d:%02d", minutes,seconds) as String
            //
            print("Timer Update for added recording \(tableViewCell?.lblTime.text)")
            
            if currentTime == duration
            {
                print("Timer Update to 0")
                tableViewCell?.audioPlayer.pause()
                tableViewCell?.audioSlider.value = 0.0
                tableViewCell?.btnPlayPause.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
                self.updateTimerForRecorded.invalidate()
            }
        }
    }
    
    @objc func updateTimeForRecordingSliderReply() {
        let cell_indexPath = NSIndexPath(row: self.audioPlayIndex, section: 0)
        let tableViewCell = self.tblComment.cellForRow(at: cell_indexPath as IndexPath) as? OthersCommentTableViewCell
        let currentTime = Int(Float(CMTimeGetSeconds((tableViewCell?.audioPlayer.currentItem!.currentTime())!)))
        let duration = Int(Float(CMTimeGetSeconds((tableViewCell?.audioPlayer.currentItem!.asset.duration)!)))
        let total = currentTime - duration
        let totalString = String(total)
        
        let minutes = currentTime/60
        let seconds = currentTime - minutes / 60
        print("Current time \(currentTime)")
        tableViewCell?.commentAudioSLider.value = Float(CMTimeGetSeconds((tableViewCell?.audioPlayer.currentItem!.currentTime())!))
        tableViewCell?.lblCommentTime.text = NSString(format: "%02d:%02d", minutes,seconds) as String
        //
        print("Timer Update for added recording \(tableViewCell?.lblCommentTime.text)")
        
        if currentTime == duration
        {
            print("Timer Update to 0")
            tableViewCell?.audioPlayer.pause()
            tableViewCell?.commentAudioSLider.value = 0.0
            tableViewCell?.btnComentAudioPlay.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
            self.updateTimerForRecorded.invalidate()
        }
    }
    
    
    @objc func updateTimeForRecording(_ sender: Timer) {
       
     //   print(audioRecorder)
        if audioRecorder == nil {
            updateTimerRecording.invalidate()
            return
        }
   
        let currentTime = Int(self.audioRecorder.currentTime)
        let minutes = currentTime/60
        let seconds = currentTime - minutes / 60
        print("Current time \(currentTime)")
        recordingTimerlabel.text = NSString(format: "%02d:%02d", minutes,seconds) as String
        self.duration = NSString(format: "%02d:%02d", minutes,seconds) as String
        recordingTimerlabel.text = recordingTimerlabel.text! + MySingleton.shared.selectedLangData.tap_To_stop_recording_audio//" (Tap To stop recording audio)"
        print("Timer Update \(recordingTimerlabel.text!)")
        
        
    }
    
    @objc func sliderActionForValueChanged(_ sender: UISlider) {
        let cell_indexPath = NSIndexPath(row: sender.tag, section: 0)
        let tableViewCell = self.tblComment.cellForRow(at: cell_indexPath as IndexPath) as? CommentsTableViewCell
      
    if tableViewCell?.audioPlayer.currentItem != nil
    {
        let currentTime = Int(Float(CMTimeGetSeconds((tableViewCell?.audioPlayer.currentItem!.currentTime())!)))
        let duration = Int(Float(CMTimeGetSeconds((tableViewCell?.audioPlayer.currentItem!.asset.duration)!)))
        
        let minutes = Int64(sender.value)/60
        let seconds = Int64(sender.value) - minutes / 60
        print("Current time \(currentTime)")
        let targetTime:CMTime = CMTimeMake(value: seconds, timescale: 1)
        tableViewCell?.audioPlayer.seek(to: targetTime)
        
        tableViewCell?.audioSlider.value = Float(CMTimeGetSeconds(targetTime))
        tableViewCell?.lblTime.text = NSString(format: "%02d:%02d", minutes,seconds) as String
        //
        print("Timer Update for added recording \(tableViewCell?.lblTime.text)")
        
        if currentTime == duration
        {
            print("Timer Update to 0")
            tableViewCell?.audioPlayer.pause()
            tableViewCell?.audioSlider.value = 0.0
            tableViewCell?.btnPlayPause.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
            self.updateTimerForRecorded.invalidate()
        }
        }
        else
       {
        if self.CommentData.data[sender.tag].Replies.count == 0
        {
            DispatchQueue.main.async {
                //   self.downloadFileFromURL(url: URL(string:self.CommentData.data[sender.tag].CommentAudio!)!,tableView: "comment")
                self.play(url: URL(string:self.CommentData.data[sender.tag].CommentAudio!)!, tableView: "comment")
            }
            self.audioPlayIndex = sender.tag
        }
        else
        {
            DispatchQueue.main.async {
                //   self.downloadFileFromURL(url: URL(string:self.CommentData.data[sender.tag].CommentAudio!)!,tableView: "reply")
                self.play(url: URL(string:self.CommentData.data[sender.tag].CommentAudio!)!, tableView: "reply")
            }
            self.audioPlayIndex = sender.tag
        }
       }
    }
    
    
    func startRecording() {
        
        self.recordingTimerlabel.isHidden = false
        self.txtCommentView.text = ""
        self.txtCommentView.placeholder = ""
        self.audioFilename = nil
        self.updateTimerForRecorded.invalidate()
        self.updateTimerRecording.invalidate()
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        //  self.loadRecordingUI()
                    } else {
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }

        
         self.audioFilename = getDocumentsDirectory().appendingPathComponent("recording.mp4")
        
        do {
            audioRecorder = try AVAudioRecorder(url: self.audioFilename, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record()
            print("Recording Started")
          
            DispatchQueue.main.async {
                self.recordingTimerlabel.text = "00:00"
                 self.recordingTimerlabel.text = self.recordingTimerlabel.text! + MySingleton.shared.selectedLangData.tap_To_stop_recording_audio
                self.updateTimerRecording = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(self.updateTimeForRecording(_:)), userInfo: nil, repeats: true)
            }
            
            
        } catch {
        //    finishRecording(success: false)
             self.btnRecord.setImage(#imageLiteral(resourceName: "UnRecord"), for: .normal)
            print("Recording Stoped")
        }
    }
    
    
    func finishRecording(success: Bool) {
        self.btnRecord.setImage(#imageLiteral(resourceName: "UnRecord"), for: .normal)
        self.txtCommentView.placeholder = MySingleton.shared.selectedLangData.write_a_comment
        self.updateTimerForRecorded.invalidate()
        self.recordingTimerlabel.text = ""
        self.audioPlayIndex = 0
        self.audioRecorder?.stop()
        self.timer?.invalidate()
        self.updateTimerRecording.invalidate()
        
        print("Finished Recording")
        print(self.duration)
        
        if self.duration == ""
        {
            return
        }
        
        
        let alertController = UIAlertController(title: "TrenderAlert", message: MySingleton.shared.selectedLangData.are_you_sure_you_want_to_send_this_audio_message , preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: MySingleton.shared.selectedLangData.ok, style: UIAlertAction.Style.default, handler: { (action) in
            self.audioRecorder = nil
            let dict = ["Duration":self.recordingTimerlabel.text!] as [String : Any]
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
                self.UploadAudio(parameter: jsonData as NSData)
                
            } catch {
                print(error.localizedDescription)
            }
            
          //  self.UploadAudio()
         
        })
        alertController.addAction(okAction)
        let cancelAction = UIAlertAction(title: MySingleton.shared.selectedLangData.cancel, style: UIAlertAction.Style.default, handler: { (action) in
            self.audioRecorder?.stop()
            self.timer?.invalidate()
            self.updateTimerRecording.invalidate()
            self.audioRecorder = nil
            self.txtCommentView.placeholder = MySingleton.shared.selectedLangData.write_a_comment
            //  self.txtCommentView.text = ""
            self.recordingTimerlabel.text = ""
             self.btnRecord.setImage(#imageLiteral(resourceName: "UnRecord"), for: .normal)
        })
        alertController.addAction(cancelAction)
        UIApplication.shared.keyWindow!.rootViewController!.present(alertController, animated: true, completion: nil)
        
        self.btnRecord.setImage(#imageLiteral(resourceName: "UnRecord"), for: .normal)
//        recordPlayButton.isEnabled = true
        
    }
    
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        
        return documentsDirectory
    }
    
    @objc func playPauseActionReply(_ sender: UIButton)
    {
        
        let cell_indexPath = NSIndexPath(row: sender.tag, section: 0)
        let tableViewCell = self.tblComment.cellForRow(at: cell_indexPath as IndexPath) as! OthersCommentTableViewCell
        print(tableViewCell.audioPlayer.timeControlStatus.rawValue)
        tableViewCell.recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try tableViewCell.recordingSession.setCategory(.playAndRecord, mode: .default)
            try tableViewCell.recordingSession.setActive(true)
            tableViewCell.recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        //  self.loadRecordingUI()
                    } else {
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }
        if  tableViewCell.audioPlayer.timeControlStatus == AVPlayer.TimeControlStatus.paused
        {
            print("play")
            if tableViewCell.commentAudioSLider.value > 0.0 && self.audioPlayIndex == sender.tag
            {
                tableViewCell.audioPlayer.play()
                tableViewCell.btnComentAudioPlay.setImage(#imageLiteral(resourceName: "img_pauseAudio"), for: .normal)
            }
            else
            {
                 self.updateTimerForRecorded.invalidate()
                if self.CommentData.data[sender.tag].Replies.count != 0
                {
                    for i in 0...self.CommentData.data.count-1
                    {
                        if self.CommentData.data[i].CommentAudio != nil
                        {
                            let cell_indexPath = NSIndexPath(row: i, section: 0)
                            let tableViewCell = self.tblComment.cellForRow(at: cell_indexPath as IndexPath) as! OthersCommentTableViewCell
                            tableViewCell.lblCommentTime.text = self.CommentData.data[i].Duration
                            tableViewCell.btnComentAudioPlay.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
                            tableViewCell.commentAudioSLider.value = 0
                            tableViewCell.audioPlayer.pause()
                            self.updateTimerForRecorded.invalidate()
                        }
                    }
                    DispatchQueue.main.async {
                        //   self.downloadFileFromURL(url: URL(string:self.CommentData.data[sender.tag].CommentAudio!)!,tableView: "comment")
                        self.play(url: URL(string:self.CommentData.data[sender.tag].CommentAudio!)!, tableView: "reply")
                    }
                    self.audioPlayIndex = sender.tag
                    
                }
              
            }
        }
        else if tableViewCell.audioPlayer.timeControlStatus == AVPlayer.TimeControlStatus.playing
        {
            print("pause")
            tableViewCell.audioPlayer.pause()
            tableViewCell.btnComentAudioPlay.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
            
        }
        
    }
    
    
    @objc func playPauseAction(_ sender: UIButton)
    {
        let cell_indexPath = NSIndexPath(row: sender.tag, section: 0)
        let tableViewCell = self.tblComment.cellForRow(at: cell_indexPath as IndexPath) as! CommentsTableViewCell
        print(tableViewCell.audioPlayer.timeControlStatus.rawValue)
        tableViewCell.recordingSession = AVAudioSession.sharedInstance()
        do {
            try tableViewCell.recordingSession.setCategory(.playAndRecord, mode: .default)
            try tableViewCell.recordingSession.setActive(true)
            tableViewCell.recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        //  self.loadRecordingUI()
                    } else {
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }
       if  tableViewCell.audioPlayer.timeControlStatus == AVPlayer.TimeControlStatus.paused
       {
        print("play")
        if tableViewCell.audioSlider.value > 0.0 && self.audioPlayIndex == sender.tag
        {
         tableViewCell.audioPlayer.play()
         tableViewCell.btnPlayPause.setImage(#imageLiteral(resourceName: "img_pauseAudio"), for: .normal)
        }
        else
        {
            self.updateTimerForRecorded.invalidate()
        if self.CommentData.data[sender.tag].Replies.count == 0
        {
            for i in 0...self.CommentData.data.count-1
            {
                if self.CommentData.data[i].CommentAudio != nil
                {
                    let cell_indexPath = NSIndexPath(row: i, section: 0)
                    let tableViewCell = self.tblComment.cellForRow(at: cell_indexPath as IndexPath) as? CommentsTableViewCell
                    tableViewCell?.lblTime.text = self.CommentData.data[i].Duration
                    tableViewCell?.btnPlayPause.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
                    tableViewCell?.audioSlider.value = 0
                    tableViewCell?.audioPlayer.pause()
                    self.updateTimerForRecorded.invalidate()
                }
            }
            DispatchQueue.main.async {
                //   self.downloadFileFromURL(url: URL(string:self.CommentData.data[sender.tag].CommentAudio!)!,tableView: "comment")
                self.play(url: URL(string:self.CommentData.data[sender.tag].CommentAudio!)!, tableView: "comment")
            }
            self.audioPlayIndex = sender.tag
            
        }
        else
        {
            DispatchQueue.main.async {
                //   self.downloadFileFromURL(url: URL(string:self.CommentData.data[sender.tag].CommentAudio!)!,tableView: "reply")
                self.play(url: URL(string:self.CommentData.data[sender.tag].CommentAudio!)!, tableView: "reply")
            }
            self.audioPlayIndex = sender.tag
        }
        }
       }
       else if tableViewCell.audioPlayer.timeControlStatus == AVPlayer.TimeControlStatus.playing
       {
         print("pause")
          tableViewCell.audioPlayer.pause()
        tableViewCell.btnPlayPause.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
        
       }

    }
    
    func downloadFileFromURL(url:URL,tableView: String){
        var downloadTask:URLSessionDownloadTask
        downloadTask = URLSession.shared.downloadTask(with: url as URL, completionHandler: { (URL, response, error) -> Void in
            //self.count += 1
            if error != nil {
                print("There was some problem with the Sound")
            } else {
                print(URL!)
                self.play(url: URL! as URL, tableView: tableView)
            }
        })
        
        downloadTask.resume()
    }
    
    @objc func playerDidFinishPlaying(_ note: NSNotification) {
        let cell_indexPath = NSIndexPath(row: self.audioPlayIndex, section: 0)
        let tableViewCell = self.tblComment.cellForRow(at: cell_indexPath as IndexPath) as! CommentsTableViewCell
        tableViewCell.audioPlayer.pause()
        tableViewCell.btnPlayPause.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
    }
    
    func play(url:URL,tableView:String)
    {
        if tableView == "comment"
        {
         DispatchQueue.main.async {
        let cell_indexPath = NSIndexPath(row: self.audioPlayIndex, section: 0)
        let tableViewCell = self.tblComment.cellForRow(at: cell_indexPath as IndexPath) as! CommentsTableViewCell
            if tableViewCell.btnPlayPause.currentImage == #imageLiteral(resourceName: "img_PlayAudio")
            {
                print("playing \(url)")
                do {
                    tableViewCell.audioPlayer = try AVPlayer(url: url)
                    NotificationCenter.default.addObserver(self, selector: #selector(self.playerDidFinishPlaying(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: tableViewCell.audioPlayer)
                    tableViewCell.audioPlayer.volume = 1.0
                    tableViewCell.audioPlayer.play()
                    
                    tableViewCell.audioSlider.maximumValue = Float(CMTimeGetSeconds(tableViewCell.audioPlayer.currentItem!.asset.duration))
                    tableViewCell.btnPlayPause.setImage(#imageLiteral(resourceName: "img_pauseAudio"), for: .normal)
                    tableViewCell.audioSlider.value = 0.0
                    // self.audioPlayIndex = cell_indexPath.row
                    self.updateTimerForRecorded = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(CommentViewController.updateTimeForRecordingSlider), userInfo: nil, repeats: true)
                    
                } catch let error as NSError {
                    //self.player = nil
                    print(error.localizedDescription)
                } catch {
                    print("AVAudioPlayer init failed")
                }
            
            }
                else
                {
                     DispatchQueue.main.async {
                    let cell_indexPath = NSIndexPath(row: self.audioPlayIndex, section: 0)
                    let tableViewCell = self.tblComment.cellForRow(at: cell_indexPath as IndexPath) as! CommentsTableViewCell
                   tableViewCell.audioPlayer.pause()
                    tableViewCell.btnPlayPause.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
                    }
                }
            }
        }
        else
        {
            DispatchQueue.main.async {
                let cell_indexPath = NSIndexPath(row: self.audioPlayIndex, section: 0)
                let tableViewCell = self.tblComment.cellForRow(at: cell_indexPath as IndexPath) as! OthersCommentTableViewCell
                if tableViewCell.btnComentAudioPlay.currentImage == #imageLiteral(resourceName: "img_PlayAudio")
                {
                    print("playing \(url)")
                    do {
                        
                        
                        tableViewCell.audioPlayer = try AVPlayer(url: url)
                        tableViewCell.audioPlayer.volume = 1.0
                        tableViewCell.audioPlayer.play()
                        
                        tableViewCell.commentAudioSLider.maximumValue = Float(CMTimeGetSeconds(tableViewCell.audioPlayer.currentItem!.asset.duration))
                        tableViewCell.btnComentAudioPlay.setImage(#imageLiteral(resourceName: "img_pauseAudio"), for: .normal)
                        tableViewCell.commentAudioSLider.value = 0.0
                        // self.audioPlayIndex = cell_indexPath.row
                        self.updateTimerForRecorded = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(CommentViewController.updateTimeForRecordingSliderReply), userInfo: nil, repeats: true)
                
                        
                    } catch let error as NSError {
                        //self.player = nil
                        print(error.localizedDescription)
                    } catch {
                        print("AVAudioPlayer init failed")
                    }
                    //  self.audioRecorderPlayer.play()
                    
                    
                }
                else
                {
                    DispatchQueue.main.async {
                        let cell_indexPath = NSIndexPath(row: self.audioPlayIndex, section: 0)
                        let tableViewCell = self.tblComment.cellForRow(at: cell_indexPath as IndexPath) as! OthersCommentTableViewCell
                        tableViewCell.audioPlayer.pause()
                        tableViewCell.btnComentAudioPlay.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
                    }
                }
            }
        }
    }
    
    func UploadAudio(parameter:NSData?)-> Void{
        let boundary = "Boundary-\(UUID().uuidString)"
        DispatchQueue.main.async {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        }
        let headers: HTTPHeaders = [
            "Authorization": "Bearer " + MySingleton.shared.loginObject.access_token,
            "content-type": "multipart/form-data; boundary= \(boundary)",
            "cache-control": "no-cache"
        ]
        
        
//          var request = URLRequest(url: NSURL.init(string: "http://172.16.3.11:9091/api/comment/AddCommentAudio/\(self.trendId)")! as URL)
        
         var request = URLRequest(url: NSURL.init(string: "\(WebServices.baseURL)api/comment/AddCommentAudio/\(self.trendId)")! as URL)
        
        request.allHTTPHeaderFields = headers
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        Alamofire.upload(multipartFormData: { multipartFormData in
//             DispatchQueue.main.async {
            multipartFormData.append(self.audioFilename!, withName: "fileUpload")
            let data = (self.duration).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
             print(data)
            multipartFormData.append(data,  withName: "Duration")
//            multipartFormData.append(self.duration.data(using: String.Encoding.utf8, allowLossyConversion: false)! ,  withName: "Duration")
//            }
        }, with: request, encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _,_):
                upload.responseJSON { response in
                    
                    DispatchQueue.main.async {
                        MBProgressHUD.hide(for: self.view, animated: true)
                    }
                    if(response.result.value is [String:AnyObject])
                    {
                        if let JSON  = response.result.value as? [String:AnyObject]{
                            print(JSON)
                             let data = JSON["data"] as! [AnyObject]
                            debugPrint("SUCCESS RESPONSE: \(response)")
                            let loginObject = Login()
                            let socketData = ["chatroom":self.chatroom,
                                              "Id":data[0]["CommentID"] as! Int,
                                              "UserProfileId":MySingleton.shared.loginObject.UserID,
                                              "IsReply":0] as [String : Any]
                            SocketIoManager.sharedInstance.sendDataToEvent(.message, data: socketData)
                        }
                    }
                    
                    self.CommentData.data.removeAll()
                    self.imgCollection.removeAll()
                    self.getAllComments()
                    self.gifId = 0
                    self.imgComment = nil
                    self.audioPlayIndex = 0
                    self.duration = ""
                    self.txtCommentView.placeholder = MySingleton.shared.selectedLangData.write_a_comment
                    self.recordingTimerlabel.text = ""
                    self.audioRecorder?.stop()
                    self.timer?.invalidate()
                    self.updateTimerRecording.invalidate()
                    self.updateTimerForRecorded.invalidate()
                    self.audioRecorder = nil
                }
            case .failure(let encodingError):
                // hide progressbas here
                debugPrint("SUCCESS Failure: \(encodingError)")
            }
        })
        
    }
    
    func getFriendListData(lazyLoading: Bool){
        
        var param = ["Skip":"","Take":"","Search":""] as [String : Any]
        WebServices().callUserService(service: UserServices.getFriendList, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: true, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            DispatchQueue.main.async{
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    DispatchQueue.main.async {
                    self.friendListObjData.data.removeAll()
                    self.friendListObjData = WebServices().decodeDataToClass(data:serviceData, decodeClass: FriendList.self)!
                    // self.friendListObj.data += temp.data
                    if self.friendListObjData.data.count != 0
                    {
                        var dataString = [String]()
                        for i in 0...self.friendListObjData.data.count-1
                        {
                            dataString.append("\(self.friendListObjData.data[i].Firstname!)" + "\(self.friendListObjData.data[i].Lastname!)")
                        }
                      
                    }
                    else
                    {
                    }
                }
                    
                }else{
                    //  TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
              //  TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
            }
        }
    }
    
    func getFriendList(Skip: Int, Take: Int, lazyLoading: Bool,SearchString:String = ""){
        
        var param = ["Skip":Skip,"Take":Take,"Search":SearchString] as [String : Any]
        print(SearchString)
        WebServices().callUserService(service: UserServices.getFriendList, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: true, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            DispatchQueue.main.async{
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    DispatchQueue.main.async {
                    self.friendListObj.data.removeAll()
                    self.friendListObj = WebServices().decodeDataToClass(data:serviceData, decodeClass: FriendList.self)!
                   // self.friendListObj.data += temp.data
                    if self.friendListObj.data.count != 0
                    {
                        var dataString = [String]()
                        for i in 0...self.friendListObj.data.count-1
                        {
                            dataString.append("\(self.friendListObj.data[i].Firstname!)" + "\(self.friendListObj.data[i].Lastname!)")
                        }
                        self.dropDown.dataSource = dataString
                        self.dropDown.show()
                    }
                    else
                    {
                        self.dropDown.hide()
                    }
                    }
                 
                }else{
                  //  TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        self.imgCollection.removeAll()
        searchText = updatedString!
       // self.getGroupListing(Skip: 0, Take: 15, lazyLoading: true,SearchString: updatedString ?? "")
//        Alamofire.SessionManager.default.session.getAllTasks { (task) in
//            task.forEach { $0.cancel() }
//        }
        self.getAllGIF(Skip: 0, Take: 15, lazyLoading: true,SearchString: updatedString ?? "")
        //        self.searchGroups(searchString: updatedString ?? "")
        return true
    }
    
}

extension CommentViewController : AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    
    
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool)
    {
        DispatchQueue.main.async {
            let cell_indexPath = NSIndexPath(row: self.audioPlayIndex, section: 0)
            let tableViewCell = self.tblComment.cellForRow(at: cell_indexPath as IndexPath) as? CommentsTableViewCell
            tableViewCell?.btnPlayPause.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
            let cellindexPath = NSIndexPath(row: self.audioPlayIndex, section: 0)
            let tableViewCellOther = self.tblComment.cellForRow(at: cellindexPath as IndexPath) as? OthersCommentTableViewCell
            tableViewCellOther?.btnComentAudioPlay.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
        }
       // self.audioRecorderPlayer?.stop()
      //  self.audioRecorder?.stop()
        self.updateTimerRecording.invalidate()
        self.updateTimerForRecorded.invalidate()
        self.updateTimerForDownloadedRecording.invalidate()
    }
    
}

extension CommentViewController : UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate
{
    
    func alert(_ title: String, message: String) {
        let vc = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        vc.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        present(vc, animated: true, completion: nil)
    }
    
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.CommentData.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if (self.CommentData.data[indexPath.row].Replies.count) == 0
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentsTableViewCell", for: indexPath) as! CommentsTableViewCell
            
            cell.lblComment.delegate = self
            
            let name = "\(self.CommentData.data[indexPath.row].UserFirstname) " + "\(self.CommentData.data[indexPath.row].UserLastname) "
            
//            let customType = ActiveType.custom(pattern: "\\sare\\b") //Looks for "are"
//            let customType2 = ActiveType.custom(pattern: "\\sit\\b") //Looks for "it"
            let customType3 = ActiveType.custom(pattern: "\(name)") //Looks for "supports"
            
//            cell.lblComment.enabledTypes.append(customType)
//            cell.lblComment.enabledTypes.append(customType2)
            cell.lblComment.enabledTypes.append(customType3)
            
            cell.lblComment.urlMaximumLength = 31
            
            cell.lblComment.customize { label in
                label.text = "\(name)\(self.CommentData.data[indexPath.row].CommentText ?? "")"
                //                label.text = "gayatri R"
                label.numberOfLines = 0
                
                label.textColor = UIColor.black
                label.hashtagColor = UIColor.tagColor
                label.mentionColor = UIColor.tagColor
                label.URLColor = UIColor.tagColor
                label.URLSelectedColor = UIColor.tagColor
                
                var tagArray = [String]()
                
                label.handleMentionTap {
                    if self.CommentData.data[indexPath.row].TaggedUsers.count != 0
                    {
                        for i in 0...self.CommentData.data[indexPath.row].TaggedUsers.count-1
                        {
                            tagArray.append("\(self.CommentData.data[indexPath.row].TaggedUsers[i].UserProfileFirstName)\(self.CommentData.data[indexPath.row].TaggedUsers[i].UserProfileLastName)")
                        }
                        
                        print(self.CommentData.data[indexPath.row].TaggedUsers)
                        print(tagArray)
                        
                        for a in 0...self.CommentData.data[indexPath.row].TaggedUsers.count-1
                        {
                            print("\(self.CommentData.data[indexPath.row].TaggedUsers[a].UserProfileFirstName)\(self.CommentData.data[indexPath.row].TaggedUsers[a].UserProfileLastName)")
                            print("\($0)")
                            if $0 == "\(self.CommentData.data[indexPath.row].TaggedUsers[a].UserProfileFirstName)\(self.CommentData.data[indexPath.row].TaggedUsers[a].UserProfileLastName)"
                            {
                                tagArray.removeAll()
                                print(self.CommentData.data[indexPath.row].TaggedUsers[a].UserProfileID)
                                let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
                                let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
                                profileVC.userId = self.CommentData.data[indexPath.row].TaggedUsers[a].UserProfileID
                                self.navigationController?.pushViewController(profileVC, animated: true)
                                print(self.CommentData.data[indexPath.row].TaggedUsers[a].UserProfileID)
                                return
                            }
                            else
                            {
                                //self.alert("ALert", message: "No user found")
                            }
                        }
                        self.alert("ALert", message: "No user found")
                    }
                    else
                    {
                        self.alert("ALert", message: "No user found")
                    }
                }
                
                label.handleHashtagTap { self.alert("Hashtag", message: $0) }
                
                label.handleURLTap { guard var url = URL(string: $0.absoluteString) else { return }
                     if !((url.absoluteString).contains("http"))
                     {
                        let strUrl = (url.absoluteString).replacingOccurrences(of: "www.", with: "https://")
                        url = URL(string: strUrl)!
                    }
                    UIApplication.shared.open(url) }
                
                //Custom types
                
//                label.customColor[customType] = UIColor.purple
//                label.customSelectedColor[customType] = UIColor.green
//                label.customColor[customType2] = UIColor.magenta
//                label.customSelectedColor[customType2] = UIColor.green
                
                label.configureLinkAttribute = { (type, attributes, isSelected) in
                    var atts = attributes
                    switch type {
                    case customType3:
                        atts[NSAttributedString.Key.font] = isSelected ? UIFont.boldSystemFont(ofSize: 13) : UIFont.boldSystemFont(ofSize: 13)
                        print(attributes)
                        print(type)
                    default: ()
                    }
                    return atts
                }
                
                //                cell.lblComment.handleCustomTap(for: customType) { self.alert("Custom type", message: $0) }
                //                cell.lblComment.handleCustomTap(for: customType2) { self.alert("Custom type", message: $0) }
                //                cell.lblComment.handleCustomTap(for: customType3) { self.alert("Custom type", message: $0) }
            }
            
            //    cell.lblComment.makeCornerRadius(radius: 6)
            cell.imgProfilePic.makeCornerRadius(radius: cell.imgProfilePic.frame.size.height/2)
            cell.imgProfilePic.sd_setImage(with: URL(string: self.CommentData.data[indexPath.row].ProfileImage ?? ""), placeholderImage: #imageLiteral(resourceName: "img_selectProfilePic"))
            cell.lblExpire.text = self.CommentData.data[indexPath.row].TimeAgo
            cell.btnReply.tag = indexPath.row
            cell.btnReply.addTarget(self, action: #selector(self.proceedToReply(_:)), for: .touchUpInside)
            cell.btnLike.tag = indexPath.row
            cell.btnLike.addTarget(self, action: #selector(self.proceedToLikeComment(_:)), for: .touchUpInside)
            
            
            if self.CommentData.data[indexPath.row].CanDelete == true
            {
                let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressed(_:)))
                let longPressImg = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressed(_:)))
                cell.lblComment.isUserInteractionEnabled = true
                cell.imgComment.isUserInteractionEnabled = true
                longPress.delegate = self
                longPressImg.delegate = self
                cell.imgComment.addGestureRecognizer(longPressImg)
                cell.lblComment.addGestureRecognizer(longPress)
            }
            else
            {
                //   cell.lblComment.isUserInteractionEnabled = false
                // cell.imgComment.isUserInteractionEnabled = false
            }
            
            
            if self.CommentData.data[indexPath.row].IsLiked == false
            {
                cell.btnLike.setTitleColor(UIColor.black, for: .normal)
            }
            else
            {
                cell.btnLike.setTitleColor(UIColor(displayP3Red: 255.0/255.0, green: 83.0/255.0, blue: 119.0/255.0, alpha: 1.0), for: .normal)
            }
            if self.CommentData.data[indexPath.row].CommentImage == nil && self.CommentData.data[indexPath.row].CommentAudio == nil
            {
                cell.cnstImgHeight.constant = 0
                cell.cnstAudioHeight.constant = 0
                
            }
            else if self.CommentData.data[indexPath.row].CommentAudio != nil
            {
                
                cell.cnstImgHeight.constant = 0
                cell.cnstAudioHeight.constant = 40
                cell.lblExpire.text = self.CommentData.data[indexPath.row].TimeAgo
                cell.lblTime.text = self.CommentData.data[indexPath.row].Duration
                //   cell.audioSlider.setThumbImage(UIImage(named:"img_SilderThumb"), for: .highlighted)
//                cell.audioSlider.value = 0.0
                cell.audioSlider.tag = indexPath.row
                cell.audioSlider.addTarget(self, action: #selector(self.sliderActionForValueChanged(_:)), for: .valueChanged)
//                self.updateTimerForRecorded.invalidate()
                cell.btnPlayPause.tag = indexPath.row
                cell.btnPlayPause.addTarget(self, action: #selector(self.playPauseAction(_:)), for: .touchUpInside)
            }
            else
            {
                
                cell.cnstImgHeight.constant = 120
                cell.cnstAudioHeight.constant = 0
                let dataType = String(self.CommentData.data[indexPath.row].CommentImage!.suffix(3))
                print(dataType)
                if dataType == "gif"
                {
                    cell.imgComment.image = nil
                    cell.imgComment.setGifImage(#imageLiteral(resourceName: "img_RecoveryIcon"))
                     DispatchQueue.main.async {
                    do {
                        
//                        let data = try? Data(contentsOf: URL(string: self.CommentData.data[indexPath.row].CommentImage!)!)
//                        if data != nil {
//                            cell.imgComment.image = nil
//                            cell.imgComment.image = UIImage(data: data!)
//                            let imageData = try Data(contentsOf: URL(string:self.CommentData.data[indexPath.row].CommentImage!)!)
                       //     DispatchQueue.global(qos: .background).async {
                        
                            let url = URL(string: self.CommentData.data[indexPath.row].CommentImage!)
                         //   cell.imgComment.delegate = self
                            cell.imgComment.setGifFromURL(url)
                        
//                            cell.imgComment.gif
//                                    var calculatedWidth = CGFloat()
//                                    calculatedWidth = (Utilities.getImageWidth(image: cell.imgComment.image!))
//                                    var calculatedHeight = CGFloat()
//                                    calculatedHeight = (Utilities.getImageHeight(image: cell.imgComment.image!))
//
//                                    let updatedwidth = (calculatedWidth * 120)/calculatedHeight
//
//                                    //     cell.imgComment.frame = CGRect(x: 0, y: 0, width: updatedwidth, height: 120)
//                                    print(updatedwidth)
//                                    cell.cnstCOmmentImgWidth.constant = updatedwidth
//                                    print(cell.cnstCOmmentImgWidth.constant)
                        
                              //     cell.imgComment.image = UIImage.sd_animatedGIF(with: imageData)
                   //             }
                        //    }
                    //    }
                    } catch {
                        print("Unable to load data: \(error)")
                    }
                    }
                }
                else
                {
                    cell.imgComment.image = nil
                    cell.imgComment.sd_setImage(with: URL(string: self.CommentData.data[indexPath.row].CommentImage!), placeholderImage: #imageLiteral(resourceName: "img_RecoveryIcon"))
                    do {
                        
                     //   DispatchQueue.global(qos: .background).async {
                                DispatchQueue.main.async {
                                    let data = try? Data(contentsOf: URL(string: self.CommentData.data[indexPath.row].CommentImage!)!)
                                    if data != nil {
//                                    cell.imgComment.image = nil
//                                    cell.imgComment.image = UIImage(data: data!)
                                    
                                    var calculatedWidth = CGFloat()
                                    calculatedWidth = (Utilities.getImageWidth(image: cell.imgComment.image!))
                                    var calculatedHeight = CGFloat()
                                    calculatedHeight = (Utilities.getImageHeight(image: cell.imgComment.image!))
                                    
                                    let updatedwidth = (calculatedWidth * 120)/calculatedHeight
                                    
                                 //   cell.imgComment.frame = CGRect(x: 0, y: 0, width: updatedwidth, height: 120)
                                    print(updatedwidth)
                                    cell.cnstCOmmentImgWidth.constant = updatedwidth
                                    if cell.cnstCOmmentImgWidth.constant > self.view.frame.size.width - 90
                                    {
                                        cell.cnstCOmmentImgWidth.constant = self.view.frame.size.width - 90
                                    }
                                    else
                                    {
                                        cell.cnstCOmmentImgWidth.constant = updatedwidth
                                    }
                                    print(cell.cnstCOmmentImgWidth.constant)
                                    cell.imgComment.updateConstraints()
                                   
                                }
                            }
                      //  }
                    } catch {
                        print("Unable to load data: \(error)")
                    }
                    
                }
            }
            
            return cell
            
        }
         
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OthersCommentTableViewCell", for: indexPath) as! OthersCommentTableViewCell
            
            cell.lblUserComment.delegate = self
            
            let name = "\(self.CommentData.data[indexPath.row].UserFirstname) " + "\(self.CommentData.data[indexPath.row].UserLastname) "
            
//            let customType = ActiveType.custom(pattern: "\\sare\\b") //Looks for "are"
//            let customType2 = ActiveType.custom(pattern: "\\sit\\b") //Looks for "it"
            let customType3 = ActiveType.custom(pattern: "\(name)") //Looks for "supports"
            
//            cell.lblUserComment.enabledTypes.append(customType)
//            cell.lblUserComment.enabledTypes.append(customType2)
            cell.lblUserComment.enabledTypes.append(customType3)
            
            cell.lblUserComment.urlMaximumLength = 31
            
            cell.lblUserComment.customize { label in
                
                label.text = "\(name)\(self.CommentData.data[indexPath.row].CommentText ?? "")"
                //                label.text = "gayatri R"
                label.numberOfLines = 0
                //    label.lineSpacing = 4
                
                label.textColor = UIColor.black
                label.hashtagColor = UIColor.tagColor
                label.mentionColor = UIColor.tagColor
                label.URLColor = UIColor.tagColor
                label.URLSelectedColor = UIColor.tagColor
                
                var tagArray = String()
                
                label.handleMentionTap {
                    if self.CommentData.data[indexPath.row].TaggedUsers.count != 0
                    {
                        for i in 0...self.CommentData.data[indexPath.row].TaggedUsers.count-1
                        {
                            tagArray.append("\(self.CommentData.data[indexPath.row].TaggedUsers[i].UserProfileFirstName)\(self.CommentData.data[indexPath.row].TaggedUsers[i].UserProfileLastName)")
                        }
                        
                        print(self.CommentData.data[indexPath.row].TaggedUsers)
                        print(tagArray)
                        
                        for a in 0...self.CommentData.data[indexPath.row].TaggedUsers.count-1
                        {
                            print("\(self.CommentData.data[indexPath.row].TaggedUsers[a].UserProfileFirstName)\(self.CommentData.data[indexPath.row].TaggedUsers[a].UserProfileLastName)")
                            print("\($0)")
                            if $0 == "\(self.CommentData.data[indexPath.row].TaggedUsers[a].UserProfileFirstName)\(self.CommentData.data[indexPath.row].TaggedUsers[a].UserProfileLastName)"
                            {
                                tagArray.removeAll()
                                print(self.CommentData.data[indexPath.row].TaggedUsers[a].UserProfileID)
                                let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
                                let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
                                profileVC.userId = self.CommentData.data[indexPath.row].TaggedUsers[a].UserProfileID
                                self.navigationController?.pushViewController(profileVC, animated: true)
                                print(self.CommentData.data[indexPath.row].TaggedUsers[a].UserProfileID)
                                return
                            }
                            else
                            {
                                //self.alert("ALert", message: "No user found")
                            }
                        }
                        self.alert("ALert", message: "No user found")
                    }
                    else
                    {
                        self.alert("ALert", message: "No user found")
                    }
                }
                
                label.handleHashtagTap { self.alert("Hashtag", message: $0) }
                
                label.handleURLTap { guard var url = URL(string: $0.absoluteString) else { return }
                    if !((url.absoluteString).contains("http"))
                    {
                        let strUrl = (url.absoluteString).replacingOccurrences(of: "www.", with: "https://")
                        url = URL(string: strUrl)!
                    }
                    UIApplication.shared.open(url) }
                
                
                
                //Custom types
                
//                label.customColor[customType] = UIColor.purple
//                label.customSelectedColor[customType] = UIColor.green
//                label.customColor[customType2] = UIColor.magenta
//                label.customSelectedColor[customType2] = UIColor.green
                
                label.configureLinkAttribute = { (type, attributes, isSelected) in
                    var atts = attributes
                    switch type {
                    case customType3:
                        atts[NSAttributedString.Key.font] = isSelected ? UIFont.boldSystemFont(ofSize: 13) : UIFont.boldSystemFont(ofSize: 13)
                        print(attributes)
                        print(type)
                    default: ()
                    }
                    return atts
                }
            }
            
            cell.lblUserComment.makeCornerRadius(radius: 6)
            cell.imgUserProfilePic.makeCornerRadius(radius: cell.imgUserProfilePic.frame.size.height/2)
            cell.imgUserProfilePic.sd_setImage(with: URL(string: self.CommentData.data[indexPath.row].ProfileImage ?? ""), placeholderImage: #imageLiteral(resourceName: "img_selectProfilePic"))
            cell.lblExpires.text = self.CommentData.data[indexPath.row].TimeAgo
            cell.btnRply.tag = indexPath.row
            cell.btnRply.addTarget(self, action: #selector(self.proceedToReply(_:)), for: .touchUpInside)
            cell.btnLike.tag = indexPath.row
            cell.btnLike.addTarget(self, action: #selector(self.proceedToLikeCommentOhersCell(_:)), for: .touchUpInside)
            
            if self.CommentData.data[indexPath.row].CanDelete == true
            {
                let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressedCommentReply(_:)))
                let longPressImg = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressedCommentReply(_:)))
                cell.lblUserComment.isUserInteractionEnabled = true
                cell.imgComment.isUserInteractionEnabled = true
                longPress.delegate = self
                longPressImg.delegate = self
                cell.imgComment.addGestureRecognizer(longPressImg)
                cell.lblUserComment.addGestureRecognizer(longPress)
            }
            else
            {
                //                cell.lblUserComment.isUserInteractionEnabled = true
                //                cell.imgComment.isUserInteractionEnabled = true
            }
            
            if self.CommentData.data[indexPath.row].Replies[0].IsLiked == false
            {
                cell.btnLike.setTitleColor(UIColor.black, for: .normal)
            }
            else
            {
                cell.btnLike.setTitleColor(UIColor(displayP3Red: 255.0/255.0, green: 83.0/255.0, blue: 119.0/255.0, alpha: 1.0), for: .normal)
            }
            
            // MARK: Other user Comment details
            
            cell.lblOtherReply.delegate = self
            
            let nameReply = "\(self.CommentData.data[indexPath.row].Replies[0].UserFirstname) " + "\(self.CommentData.data[indexPath.row].Replies[0].UserLastname) "
             cell.imgOtherProfilePic.makeCornerRadius(radius: cell.imgOtherProfilePic.frame.size.height/2)
            cell.imgOtherProfilePic.sd_setImage(with: URL(string:self.CommentData.data[indexPath.row].Replies[0].ProfileImage ?? ""), placeholderImage: #imageLiteral(resourceName: "img_selectProfilePic"))
            let customType3Reply = ActiveType.custom(pattern: "\(nameReply)") //Looks for "supports"
            
            cell.lblOtherReply.enabledTypes.append(customType3Reply)
            
            cell.lblOtherReply.urlMaximumLength = 31
            
            cell.lblOtherReply.customize { label in
                if self.CommentData.data[indexPath.row].Replies[0].ReplyAudio != nil
                {
                    label.text = "\(name) sent an audio message"
                }
                else
                {
                    label.text = "\(name)\(self.CommentData.data[indexPath.row].Replies[0].ReplyText ?? "")"
                }

                label.numberOfLines = 1
                //    label.lineSpacing = 4
                
                label.textColor = UIColor.black
                label.hashtagColor = UIColor.tagColor
                label.mentionColor = UIColor.tagColor
                label.URLColor = UIColor.tagColor
                label.URLSelectedColor = UIColor.tagColor
                
                var tagArrayReply = String()
                
                label.handleMentionTap {
                    if self.CommentData.data[indexPath.row].Replies[0].TaggedUsers.count != 0
                    {
                    for i in 0...self.CommentData.data[indexPath.row].Replies[0].TaggedUsers.count-1
                    {
                        
                        if $0 == "\(self.CommentData.data[indexPath.row].Replies[0].TaggedUsers[i].UserProfileFirstName)\(self.CommentData.data[indexPath.row].TaggedUsers[i].UserProfileLastName)"
                        {
                            // proceed to user profile
                            let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
                            let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
                            profileVC.userId = self.CommentData.data[indexPath.row].Replies[0].TaggedUsers[i].UserProfileID
                            self.navigationController?.pushViewController(profileVC, animated: true)
                            print(self.CommentData.data[indexPath.row].Replies[0].TaggedUsers[i].UserProfileID)
                        }
                        else
                        {
                            self.alert("ALert", message: "No user found")
                        }
                    }
                    }
                    else
                    {
                         self.alert("ALert", message: "No user found")
                    }
                    }
                
                label.handleHashtagTap { self.alert("Hashtag", message: $0) }
                
                label.handleURLTap { guard var url = URL(string: $0.absoluteString) else { return }
                    if !((url.absoluteString).contains("http"))
                    {
                        let strUrl = (url.absoluteString).replacingOccurrences(of: "www.", with: "https://")
                        url = URL(string: strUrl)!
                    }
                    UIApplication.shared.open(url) }
                
                //Custom types
                
//                label.customColor[customType] = UIColor.purple
//                label.customSelectedColor[customType] = UIColor.green
//                label.customColor[customType2] = UIColor.magenta
//                label.customSelectedColor[customType2] = UIColor.green
                
                label.configureLinkAttribute = { (type, attributes, isSelected) in
                    var atts = attributes
                    switch type {
                    case customType3Reply:
                        atts[NSAttributedString.Key.font] = isSelected ? UIFont.boldSystemFont(ofSize: 13) : UIFont.boldSystemFont(ofSize: 13)
                        print(attributes)
                        print(type)
                    default: ()
                    }
                    return atts
                }
            }
            
            //            cell.lblOtherReply.attributedText = attributedStringOther
            //  var longPressRply = UILongPressGestureRecognizer()
            if self.CommentData.data[indexPath.row].Replies[0].CanDelete == true
            {
                let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressReply(_:)))
                let longPressImg = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressReply(_:)))
                cell.lblOtherReply.isUserInteractionEnabled = true
                cell.imgRply.isUserInteractionEnabled = true
                longPress.delegate = self
                longPressImg.delegate = self
                cell.imgRply.addGestureRecognizer(longPressImg)
                cell.lblOtherReply.addGestureRecognizer(longPress)
                
            }
            else
            {
                cell.lblOtherReply.isUserInteractionEnabled = false
                cell.imgRply.isUserInteractionEnabled = false
            }
            
            // MARK:  user Comment image
            
            if self.CommentData.data[indexPath.row].CommentImage == nil && self.CommentData.data[indexPath.row].CommentAudio == nil
            {
                cell.cnstUserCommentHieght.constant = 0
                cell.cnstCommentAudioHeoght.constant = 0
            }
            else if self.CommentData.data[indexPath.row].CommentAudio != nil
            {
                cell.cnstUserCommentHieght.constant = 0
                cell.cnstCommentAudioHeoght.constant = 40
                cell.lblExpires.text = self.CommentData.data[indexPath.row].TimeAgo
                cell.lblCommentTime.text = self.CommentData.data[indexPath.row].Duration
                //  cell.commentAudioSLider.value = 0.0
                cell.btnComentAudioPlay.tag = indexPath.row
                cell.btnComentAudioPlay.addTarget(self, action: #selector(self.playPauseActionReply(_:)), for: .touchUpInside)
                cell.replyAudioSlider.setThumbImage(UIImage(named:"img_SilderThumb"), for: .highlighted)
            }
            else
            {
                cell.imgComment.image = nil
                cell.cnstUserCommentHieght.constant = 120
                cell.cnstCommentAudioHeoght.constant = 0
                let dataType = String(self.CommentData.data[indexPath.row].CommentImage!.suffix(3))
                print(dataType)
                if dataType == "gif"
                {
                     cell.imgComment.image = nil
                    cell.imgComment.setGifImage(#imageLiteral(resourceName: "img_RecoveryIcon"))
                  ///   DispatchQueue.global(qos: .background).async {
                     DispatchQueue.main.async {
                    do {
                    
                        let data = try? Data(contentsOf: URL(string: self.CommentData.data[indexPath.row].CommentImage!)!)
//                        if data != nil {
//
//                         //   cell.imgComment.image = UIImage.sd_animatedGIF(with: imageData)
//                            cell.imgComment.image = nil
//                            cell.imgComment.image = UIImage(data: data!)
////
//                            let imageData = try Data(contentsOf: URL(string:self.CommentData.data[indexPath.row].CommentImage!)!)
                        //    DispatchQueue.global(qos: .background).async {
                        
                        let url = URL(string: self.CommentData.data[indexPath.row].CommentImage!)
                        cell.imgComment.setGifFromURL(url)
                        
//                                    var calculatedWidth = CGFloat()
//                                    calculatedWidth = (Utilities.getImageWidth(image: cell.imgComment.image!))
//                                    var calculatedHeight = CGFloat()
//                                    calculatedHeight = (Utilities.getImageHeight(image: cell.imgComment.image!))
//
//                                    let updatedwidth = (calculatedWidth * 120)/calculatedHeight
//
//                                    // cell.imgComment.frame = CGRect(x: 0, y: 0, width: updatedwidth, height: 120)
//                                    print(updatedwidth)
//                                    cell.cnstCommentWidth.constant = updatedwidth
//                                    print(cell.cnstCommentWidth.constant)
                        
                             //   cell.imgComment.image = UIImage.sd_animatedGIF(with: imageData)
                          //      }
                         //   }
                  //      }
                    } catch {
                        print("Unable to load data: \(error)")
                    }
               //         }
                    }
                }
                else
                {
                    cell.imgComment.image = nil
                   
                    do {
                                DispatchQueue.main.async {
                                     cell.imgComment.sd_setImage(with: URL(string: self.CommentData.data[indexPath.row].CommentImage!), placeholderImage: #imageLiteral(resourceName: "img_RecoveryIcon"))
                                    let data = try? Data(contentsOf: URL(string: self.CommentData.data[indexPath.row].CommentImage!)!)
                                    if data != nil {
                                 if cell.imgComment.image != nil
                                 {
                                    var calculatedWidth = CGFloat()
                                    calculatedWidth = (Utilities.getImageWidth(image: cell.imgComment.image!))
                                    var calculatedHeight = CGFloat()
                                    calculatedHeight = (Utilities.getImageHeight(image: cell.imgComment.image!))
                                    
                                    let updatedwidth = (calculatedWidth * 120)/calculatedHeight
                                    
                                  //  cell.imgComment.frame = CGRect(x: 0, y: 0, width: updatedwidth, height: 120)
                                    print(updatedwidth)
                                    cell.cnstCommentWidth.constant = updatedwidth
                                    if cell.cnstCommentWidth.constant > self.view.frame.size.width - 90
                                    {
                                        cell.cnstCommentWidth.constant = self.view.frame.size.width - 90
                                    }
                                    else
                                    {
                                        cell.cnstCommentWidth.constant = updatedwidth
                                    }
                                         cell.imgComment.sd_setImage(with: URL(string: self.CommentData.data[indexPath.row].CommentImage!), placeholderImage: #imageLiteral(resourceName: "img_RecoveryIcon"))
                                }
                            }
                        //   }
                        }
                    } catch {
                        print("Unable to load data: \(error)")
                    }
                    
                }
            }
            
            // MARK: Other user Comment image
            
            if self.CommentData.data[indexPath.row].Replies[0].ReplyImage == nil
            {
                cell.cnstOtherReplyHeight.constant = 0
                cell.cnstReplyAudioHeigth.constant = 0
            }
            else if self.CommentData.data[indexPath.row].Replies[0].ReplyAudio != nil
            {
                // cell.cnstReplyAudioHeigth.constant = 40
                
                cell.cnstOtherReplyHeight.constant = 0
            }
            else
            {
                cell.imgComment.image = nil
                cell.cnstOtherReplyHeight.constant = 120
                cell.cnstReplyAudioHeigth.constant = 0
                let dataType = String(self.CommentData.data[indexPath.row].Replies[0].ReplyImage!.suffix(3))
                print(dataType)
                if dataType == "gif"
                {
                    cell.imgRply.image = nil
                    cell.imgRply.setGifImage(#imageLiteral(resourceName: "img_RecoveryIcon"))
                  //  DispatchQueue.global(qos: .background).async {
                    DispatchQueue.main.async {
                    do {
                        
                          let data = try? Data(contentsOf: URL(string: self.CommentData.data[indexPath.row].Replies[0].ReplyImage!)!)
//                        if data != nil {
//                            let imageData = try Data(contentsOf: URL(string:self.CommentData.data[indexPath.row].Replies[0].ReplyImage!)!)
//                      //      DispatchQueue.global(qos: .background).async {
//
//                            cell.imgRply.image = nil
//                            cell.imgRply.image = UIImage(data: data!)
                        
                        let url = URL(string: self.CommentData.data[indexPath.row].Replies[0].ReplyImage!)
                        cell.imgRply.setGifFromURL(url)
                        
                                    var calculatedWidth = CGFloat()
                        calculatedWidth = (Utilities.getImageWidth(image: cell.imgRply.image ?? #imageLiteral(resourceName: "img_RecoveryIcon") ))
                                    var calculatedHeight = CGFloat()
                        calculatedHeight = (Utilities.getImageHeight(image: cell.imgRply.image ?? #imageLiteral(resourceName: "img_RecoveryIcon")))
                                    
                                    let updatedwidth = (calculatedWidth * 120)/calculatedHeight
                                    
                                    //    cell.imgRply.frame = CGRect(x: 0, y: 0, width: updatedwidth, height: 120)
                                    print(updatedwidth)
                                    cell.cnstReplyWidth.constant = updatedwidth
                            
                        //     cell.imgRply.image = UIImage.sd_animatedGIF(with: imageData)
                     //           }
                     //       }
                    //    }
                    } catch {
                        print("Unable to load data: \(error)")
                    }
              //          }
                }
                }
                else
                {
                    cell.imgRply.image = nil
                    cell.imgRply.sd_setImage(with: URL(string: self.CommentData.data[indexPath.row].Replies[0].ReplyImage!), placeholderImage: #imageLiteral(resourceName: "img_RecoveryIcon"))
                    do {
                        
                  //      DispatchQueue.global(qos: .background).async {
                        
                                DispatchQueue.main.async {
//                                    cell.imgRply.image = nil
//                                    cell.imgRply.image = UIImage(data: data!)
                                    let data = try? Data(contentsOf: URL(string: self.CommentData.data[indexPath.row].Replies[0].ReplyImage!)!)
                                    if data != nil {
                                    var calculatedWidth = CGFloat()
                                    calculatedWidth = (Utilities.getImageWidth(image: cell.imgRply.image!))
                                    var calculatedHeight = CGFloat()
                                    calculatedHeight = (Utilities.getImageHeight(image: cell.imgRply.image!))
                                    
                                    let updatedwidth = (calculatedWidth * 120)/calculatedHeight
                                    
                                 //   cell.imgRply.frame = CGRect(x: 0, y: 0, width: updatedwidth, height: 120)
                                    print(updatedwidth)
                                    cell.cnstReplyWidth.constant = updatedwidth
                                    print(cell.cnstReplyWidth.constant)
                                    
                                    if cell.cnstReplyWidth.constant > self.view.frame.size.width - 140
                                    {
                                        cell.cnstReplyWidth.constant = self.view.frame.size.width - 140
                                    }
                                    else
                                    {
                                        cell.cnstReplyWidth.constant = updatedwidth
                                    }
                                           cell.imgRply.sd_setImage(with: URL(string: self.CommentData.data[indexPath.row].Replies[0].ReplyImage!), placeholderImage: #imageLiteral(resourceName: "img_RecoveryIcon"))
                                }
                   //         }
                        }
                    } catch {
                        print("Unable to load data: \(error)")
                    }
//                    do {
//
//                        DispatchQueue.global(qos: .background).async {
//                            let data = try? Data(contentsOf: URL(string: self.CommentData.data[indexPath.row].Replies[0].ReplyImage!)!)
//                            if data != nil {
//                                DispatchQueue.main.async {
//                                    cell.imgRply.image = nil
//                                    cell.imgRply.image = UIImage(data: data!)
//
//                                    var calculatedWidth = CGFloat()
//                                    calculatedWidth = (Utilities.getImageWidth(image: cell.imgRply.image!))
//                                    var calculatedHeight = CGFloat()
//                                    calculatedHeight = (Utilities.getImageHeight(image: cell.imgRply.image!))
//
//                                    let updatedwidth = (calculatedWidth * 120)/calculatedHeight
//
//                                    cell.imgRply.frame = CGRect(x: 0, y: 0, width: updatedwidth, height: 120)
//                                    print(updatedwidth)
//                                    cell.cnstReplyWidth.constant = updatedwidth
//
//                                }
//                            }
//                        }
//                    } catch {
//                        print("Unable to load data: \(error)")
//                    }
//
//
                    
                }
            }
            
            return cell
        }
        
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if (cell is CommentsTableViewCell)
//        {
//            let attrs = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 13)]
//            let attributedString = NSMutableAttributedString(string:"\(self.CommentData.data[indexPath.row].UserFirstname) " + "\(self.CommentData.data[indexPath.row].UserLastname) ", attributes:attrs)
//
//            let attrsNormal = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 13)]
//            let normalString = NSMutableAttributedString(string:self.CommentData.data[indexPath.row].CommentText ?? "", attributes:attrsNormal)
//
//            attributedString.append(normalString)
//
//            (cell as! CommentsTableViewCell).lblComment.attributedText = attributedString
//        }
//        else
//        {
//            //MARK: Comment details
//
//            let attrs = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 13)]
//            let attributedString = NSMutableAttributedString(string:"\(self.CommentData.data[indexPath.row].UserFirstname) " + "\(self.CommentData.data[indexPath.row].UserLastname) ", attributes:attrs)
//
//            let normalString = NSMutableAttributedString(string:self.CommentData.data[indexPath.row].CommentText ?? "")
//
//            attributedString.append(normalString)
//
//            (cell as! OthersCommentTableViewCell).lblUserComment.attributedText = attributedString
//
//
//            // MARK: Other user Comment details
//
//            let attrsOther = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 13)]
//            let attributedStringOther = NSMutableAttributedString(string:"\(self.CommentData.data[indexPath.row].UserFirstname) " + "\(self.CommentData.data[indexPath.row].UserLastname) ", attributes:attrsOther)
//
//            let normalStringOther = NSMutableAttributedString(string:self.CommentData.data[indexPath.row].Replies[0].ReplyText ?? "")
//
//            attributedStringOther.append(normalStringOther)
//
//            (cell as! OthersCommentTableViewCell).lblOtherReply.attributedText = attributedStringOther
//        }
//    }

    
}

extension CommentViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    /*************************
     Method Name:  openGallary()
     Parameter: nil
     return type: nil
     Desc: This function opens the gallery.
     *************************/
    func openGallary()
    {
        picker.allowsEditing = false
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.cnstGIfHeight.constant = 100
        self.cnstSearchGIfHeight.constant = 0
        self.imgCollection.removeAll()
        present(picker, animated: true, completion: nil)
    }
    
    /*************************
     Method Name:  openCamera()
     Parameter: nil
     return type: nil
     Desc: This function opens the Camera.
     *************************/
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            self.cnstGIfHeight.constant = 100
            present(picker, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    /*************************
     Method Name:  imagePickerControllerDidCancel()
     Parameter: UIImagePickerController
     return type: nil
     Desc: This function is called if user taps cancel button.
     *************************/
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        self.cnstGIfHeight.constant = 0
        dismiss(animated: true, completion: nil)
    }
    
    /*************************
     Method Name:  imagePickerController()
     Parameter: UIImagePickerController, [String : Any]
     return type: nil
     Desc: This function is called when user selects image from gallery.
     *************************/
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        print(chosenImage!)
        
        self.imgCollection.removeAll()
        self.imgCollection.append(chosenImage!)
        self.imgComment = chosenImage!
        self.collectionGifImg.delegate = self
        self.collectionGifImg.dataSource = self
        self.collectionGifImg.reloadData()
//        self.collectionGifImg.collectionViewLayout.invalidateLayout()
//        self.collectionGifImg.layoutSubviews()
        dismiss(animated:true, completion: nil)
    }
    
 
}


extension CommentViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.collectionGifImg.collectionViewLayout.invalidateLayout()
        return self.imgCollection.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectImageCollectionViewCell", for: indexPath) as! SelectImageCollectionViewCell
        if self.imgCollection.count != 0
        {
        cell.image.makeCornerRadius(radius: 6.0)
        cell.indicator.isHidden = false
        if self.imgCollection.count > 1 || self.cnstSearchGIfHeight.constant == 32
        {
        let dataType = String((imgCollection[indexPath.row]["GIF"] as! String).suffix(3))
        print(dataType)
        if dataType == "gif"
        {
            cell.btnClose.isHidden = true
            do
            {
                let imageData = try Data(contentsOf: URL(string:imgCollection[indexPath.row]["GIF"] as! String)!)
                 DispatchQueue.global(qos: .background).async {
                DispatchQueue.main.async {
                    cell.image.image = UIImage.sd_animatedGIF(with: imageData)
                  }
                }
                cell.indicator.isHidden = true
            } catch {
                print("Unable to load data: \(error)")
            }
         }
        }
       else
        {
            cell.indicator.isHidden = true
            cell.btnClose.isHidden = false
            cell.image.image = self.imgComment
            cell.RemoveCell = {
                collectionView.performBatchUpdates({
                    self.imgCollection.remove(at: indexPath.row);
                    if self.imgCollection.count == 0
                    {
                        self.cnstSearchGIfHeight.constant = 0
                        self.cnstGIfHeight.constant == 0
                    }
                    collectionView.deleteItems(at: [indexPath]);
                }, completion: { (bool) in
                    collectionView.reloadItems(at: collectionView.indexPathsForVisibleItems);
                })
            }
        }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.cnstSearchGIfHeight.constant != 0
        {
           self.gifId = imgCollection[indexPath.row]["gifID"] as! Int
            self.cnstGIfHeight.constant = 0
            self.cnstSearchGIfHeight.constant = 0
            self.txtCommentView.text = ""
            addComment()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width:collectionView.frame.height, height:collectionView.frame.height)
    }
    
}

extension CommentViewController: SocketIOManagerDelegate {
  
    func connectRoom(_ data: [String : Any]) {
        print(data)
        self.chatroom = data["chatroom"] as? String ?? ""
        print(self.chatroom)
    }
    
    func messageReceived(_ data: [String:Any]) {
        print(data)
        let commentList = CommentList()
        let replyListData = ReplyList()
        
        if self.navigationController?.topViewController is CommentViewController
        {
            if data["IsReply"] as? Bool == true
            {
                let replyData = data["commentData"] as! [String:Any]
                for i in 0...self.CommentData.data.count-1
                {
                    if self.CommentData.data[i].CommentID == replyData["CommentID"] as! Int
                    {
                        if commentList.Replies.count != 0
                        {
                            commentList.Replies[0].CommentID = replyData["CommentID"] as! Int
                            commentList.Replies[0].ReplyID = replyData["ReplyID"] as! Int
                            commentList.Replies[0].IsLiked = replyData["IsLiked"] as! Bool
                            commentList.Replies[0].ReplyText = replyData["ReplyText"] as? String
                            commentList.Replies[0].ReplyImage = replyData["ReplyImage"] as? String
                            commentList.Replies[0].ProfileImage = replyData["ProfileImage"] as? String
                            commentList.Replies[0].UserFirstname = replyData["UserFirstname"] as! String
                            commentList.Replies[0].UserLastname = replyData["UserLastname"] as! String
                            commentList.Replies[0].TimeAgo = replyData["TimeAgo"] as! String
                            commentList.Replies[0].CanDelete = replyData["CanDelete"] as! Bool
                            commentList.Replies[0].ReplyAudio = replyData["ReplyAudio"] as? String
                            commentList.Replies[0].Duration = replyData["Duration"] as? String
                            
                            commentList.CommentID = self.CommentData.data[i].CommentID
                            commentList.CommentImage = self.CommentData.data[i].CommentImage
                            commentList.CommentText = self.CommentData.data[i].CommentText
                            commentList.TimeAgo = self.CommentData.data[i].TimeAgo
                            commentList.UserFirstname = self.CommentData.data[i].UserFirstname
                            commentList.UserLastname = self.CommentData.data[i].UserLastname
                            commentList.Username = self.CommentData.data[i].Username
                            commentList.ProfileImage = self.CommentData.data[i].ProfileImage
                            commentList.IsLiked = self.CommentData.data[i].IsLiked
                            commentList.CanDelete = self.CommentData.data[i].CanDelete
                            commentList.CommentAudio = self.CommentData.data[i].CommentAudio
                            commentList.Duration = self.CommentData.data[i].Duration
                            if commentList.Replies.count != 0
                            {
                                replyListData.CommentID = commentList.Replies[0].CommentID
                                replyListData.ReplyID = commentList.Replies[0].ReplyID
                                replyListData.IsLiked = commentList.Replies[0].IsLiked
                                replyListData.ReplyText = commentList.Replies[0].ReplyText
                                replyListData.ReplyImage = commentList.Replies[0].ReplyImage
                                replyListData.ProfileImage = commentList.Replies[0].ProfileImage
                                replyListData.UserFirstname = commentList.Replies[0].UserFirstname
                                replyListData.UserLastname = commentList.Replies[0].UserLastname
                                replyListData.TimeAgo = commentList.Replies[0].TimeAgo
                                replyListData.CanDelete = commentList.Replies[0].CanDelete
                                replyListData.ReplyAudio = commentList.Replies[0].ReplyAudio
                                replyListData.Duration = commentList.Replies[0].Duration
                            }
                            
                            self.CommentData.data.remove(at: i)
                            
                            self.CommentData.data.insert(commentList, at: i)
                        }
                    }
                }
            }
            else
            {
                let CommentData = data["commentData"] as! [String:Any]
                commentList.CommentID = CommentData["CommentID"] as! Int
                commentList.CommentImage = CommentData["CommentImage"] as? String
                commentList.Replies = CommentData["Replies"] as! [ReplyListData]
                commentList.CommentText = CommentData["CommentText"] as? String
                commentList.TimeAgo = CommentData["TimeAgo"] as! String
                commentList.UserFirstname = CommentData["UserFirstname"] as! String
                commentList.UserLastname = CommentData["UserLastname"] as! String
                commentList.Username = CommentData["Username"] as? String
                commentList.ProfileImage = CommentData["ProfileImage"] as? String
                commentList.IsLiked = CommentData["IsLiked"] as! Bool
                commentList.CanDelete = CommentData["CanDelete"] as! Bool
                commentList.CommentAudio = CommentData["CommentAudio"] as? String
                commentList.Duration = CommentData["Duration"] as? String
                
                self.CommentData.data.append(commentList)
            }
            
            self.tblComment.reloadData()
        }
    }
}

extension CommentViewController : ReplyData,ChangeData
{
    func fetchChangeData(Reply: String) {
        self.CommentData.data.removeAll()
        getAllComments()
    }
    
    func fetchReplyData(Reply: String) {
        self.CommentData.data.removeAll()
        getAllComments()
    }
    
}
