//
//  EditCommentViewController.swift
//  TrenderAlert
//
//  Created by Admin on 15/03/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import AGEmojiKeyboard
import GrowingTextView
import DropDown

protocol ReplyData {
    func fetchReplyData(Reply : String)
}

class EditCommentViewController: UIViewController,UITextViewDelegate,AGEmojiKeyboardViewDelegate, AGEmojiKeyboardViewDataSource , GrowingTextViewDelegate{

    @IBOutlet var txtComment: GrowingTextView!
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var btnUpdate: UIButton!
  //  @IBOutlet var txtComment: UITextView!
   // @IBOutlet var viewBorder: UIView!
    var emojiImages = [#imageLiteral(resourceName: "time_clk"),
                       #imageLiteral(resourceName: "emoji_clk"),
                       #imageLiteral(resourceName: "bell_clk"),
                       #imageLiteral(resourceName: "flower_clk"),
                       #imageLiteral(resourceName: "car_clk"),
                       #imageLiteral(resourceName: "Recent_clk")]
    var emojiSelectedIndex = -1
    var emojiNotSelectedIndex = -1
    var txtData = String()
    var delegate : ReplyData?
    var data = [String:Any]()
    var agEmojiKeyboardView = AGEmojiKeyboardView()
    var tagUsersList = [Int]()
    var friendListObj = FriendList()
    var dropDown = DropDown()
    var tagString = String()
    override func viewDidLoad() {
        super.viewDidLoad()
          self.txtComment.textContainerInset = UIEdgeInsets(top: 8, left: 8, bottom: 0, right: 45)
        self.txtComment.makeCornerRadius(radius: 4)
        self.txtComment.giveBorderColor(color: UIColor.lightGray)
        self.btnCancel.makeCornerRadius(radius: 4)
        self.btnCancel.giveBorderColor(color: UIColor.lightGray)
        self.btnUpdate.makeCornerRadius(radius: 4)
        self.btnUpdate.giveBorderColor(color: UIColor.lightGray)
        self.txtComment.delegate = self
        self.txtComment.text = data["commentTxt"] as? String
         let taggUserData = self.data["tagUsers"] as! [TaggedUsersData]
        if taggUserData.count != 0
        {
            for i in 0...taggUserData.count-1
            {
                 self.tagUsersList.append(taggUserData[i].UserProfileID)
            }
        }
        self.btnUpdate.isUserInteractionEnabled = false
        self.btnUpdate.setTitleColor(UIColor.darkGray, for: .normal)
        
        dropDown.anchorView = self.txtComment
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)
        dropdownfolderSelection()
        // Do any additional setup after loading the view.
    }
    
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        print("Add line")
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in
            self.view.layoutIfNeeded()
        }, completion: nil)
        //        UIView.animate(withDuration: 0.2) {
        //            self.view.layoutIfNeeded()
        //        }
    }
    
    @IBAction func btn_EmojiAction(_ sender: Any)
    {
        self.view.endEditing(true)
        self.txtData = (data["commentTxt"] as? String)!
        agEmojiKeyboardView = AGEmojiKeyboardView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216), dataSource: self)
        agEmojiKeyboardView.autoresizingMask = .flexibleHeight
        agEmojiKeyboardView.delegate = self
        self.txtComment.inputView = agEmojiKeyboardView
        self.txtComment.becomeFirstResponder()
        self.txtComment.text = self.txtData
         self.txtComment.inputView = agEmojiKeyboardView
    }
    
    
    @IBAction func btn_BackAction(_ sender: Any)
    {
        delegate?.fetchReplyData(Reply: "fetch")
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Emoji Delegate
    func emojiKeyBoardView(_ emojiKeyBoardView: AGEmojiKeyboardView!, didUseEmoji emoji: String!) {
        self.txtComment.text = self.txtComment.text + emoji
    }
    
    func emojiKeyBoardViewDidPressBackSpace(_ emojiKeyBoardView: AGEmojiKeyboardView!) {
        self.txtComment.text = String(self.txtComment.text.dropLast())
    }
    
    // MARK: - Emoji Data Source
    func emojiKeyboardView(_ emojiKeyboardView: AGEmojiKeyboardView!, imageForSelectedCategory category: AGEmojiKeyboardViewCategoryImage) -> UIImage! {
        if emojiSelectedIndex <= 4 {
            emojiSelectedIndex = emojiSelectedIndex + 1
        }
        return emojiImages[emojiSelectedIndex]
    }
    
    func emojiKeyboardView(_ emojiKeyboardView: AGEmojiKeyboardView!, imageForNonSelectedCategory category: AGEmojiKeyboardViewCategoryImage) -> UIImage! {
        if emojiNotSelectedIndex <= 4 {
            emojiNotSelectedIndex = emojiNotSelectedIndex + 1
        }
        return emojiImages[emojiNotSelectedIndex]
    }
    
    func backSpaceButtonImage(for emojiKeyboardView: AGEmojiKeyboardView!) -> UIImage! {
        return #imageLiteral(resourceName: "backspace_clk")
    }
    
    @IBAction func btnUpdateAction(_ sender: Any)
        {
            if data["isComment"] as! Bool == true
            {
            let param = ["TrendID":data["trendId"]!,"Commentid":data["commentid"]!,"CommentText":self.txtComment.text! ,"IsImage":"false",
                     "ImagePath":[],"gifID":"","TaggedUsers":self.tagUsersList] as [String : Any]
        WebServices().callUserService1(service: UserServices.addComment, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
        print(serviceResponse)
        if(serviceResponse["Status"] as! Bool == true)
        {
            self.delegate?.fetchReplyData(Reply: "Change")
            self.navigationController?.popViewController(animated: true)
        }
        else
        {
        TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
        }
        })
            }
            else
            {
                let param = ["ReplyID":data["replyId"]!,"CommentId":data["commentid"]!,"ReplyText":self.txtComment.text! ,"IsImage":"false",
                         "ImagePath":[],"gifID":""] as [String : Any]
         
            WebServices().callUserService1(service: UserServices.addReply, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
                print(serviceResponse)
                if(serviceResponse["Status"] as! Bool == true)
                {
                    self.delegate?.fetchReplyData(Reply: "Change")
                  self.navigationController?.popViewController(animated: true)
                }
                else
                {
                    //  TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            })
            }
        }
    
    
    @IBAction func btn_CancelAction(_ sender: Any)
    {
         delegate?.fetchReplyData(Reply: "fetch")
        self.navigationController?.popViewController(animated: true)
    }
    
     func textViewDidBeginEditing(_ textView: UITextView) {
       self.btnUpdate.isUserInteractionEnabled = true
        self.btnUpdate.setTitleColor(UIColor.black, for: .normal)
        self.txtComment.textColor = UIColor.black
      //
//        self.txtComment.inputView = nil
//        self.txtData = self.txtComment.text
//        self.txtComment.text = self.txtData
//        self.txtComment.becomeFirstResponder()
        if self.txtComment.inputView == agEmojiKeyboardView
        {
            //agEmojiKeyboardView.delegate = nil
            self.txtComment.inputView = nil
            self.txtData = self.txtComment.text
            self.txtComment.text = self.txtData
            self.txtComment.becomeFirstResponder()
        }
        else
        {
            self.txtComment.inputView = nil
            self.txtData = self.txtComment.text
            self.txtComment.text = self.txtData
            self.txtComment.becomeFirstResponder()
        }
//            self.txtComment.inputView = nil
//            //  self.txtRply.inputView = UIView()
//            //   self.txtRply.keyboardType = UIKeyboardType.default
//            self.txtComment.becomeFirstResponder()
//            self.txtData = self.txtComment.text
////            self.txtComment.keyboardType = UIKeyboardType.default
////            self.txtComment.keyboardAppearance = .default
//            self.txtComment.text = self.txtData
//        }
    }
    
    func textViewDidChange(_ textView: UITextView)
    {
        
        if self.txtComment.inputView == agEmojiKeyboardView
        {
            self.txtData = self.txtComment.text
            // self.txtCommentView.inputView = nil
            self.txtComment.inputView = UIView()
            // self.txtCommentView.keyboardType = UIKeyboardType.default
            self.txtComment.keyboardType = UIKeyboardType.default
            self.txtComment.keyboardAppearance = .default
            self.txtComment.becomeFirstResponder()
            self.txtComment.text = self.txtData
        }
        else
        {
            var textFieldText: String = textView.text
            if textFieldText.contains("@") {
                recursiveText(textViewText: textFieldText)
            }
            else
            {
                self.dropDown.hide()
            }
        }
    }
 
        func recursiveText(textViewText : String)
        {
            print("before recursive \(textViewText)")
            
            let endIndex = textViewText.range(of: "@")!.upperBound
            let textFieldTextNew = textViewText.substring(from: endIndex)
            print(textFieldTextNew)
            
            if textFieldTextNew.contains("@") {
                recursiveText(textViewText: textFieldTextNew)
                return
            }
            
            print("after recursive \(textFieldTextNew)")
            
            getFriendList(Skip: 0, Take: 15, lazyLoading: true,SearchString: textFieldTextNew)
            
        }
    
    func getFriendList(Skip: Int, Take: Int, lazyLoading: Bool,SearchString:String = ""){
        
        
        var param = ["Skip":Skip,"Take":Take,"Search":SearchString] as [String : Any]
        print(SearchString)
        WebServices().callUserService(service: UserServices.getFriendList, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: true, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    self.friendListObj.data.removeAll()
                    self.friendListObj = WebServices().decodeDataToClass(data:serviceData, decodeClass: FriendList.self)!
                    // self.friendListObj.data += temp.data
                    if self.friendListObj.data.count != 0
                    {
                        var dataString = [String]()
                        for i in 0...self.friendListObj.data.count-1
                        {
                           dataString.append("\(self.friendListObj.data[i].Firstname!)" + "\(self.friendListObj.data[i].Lastname!)")
                        }
                        self.dropDown.dataSource = dataString
                        self.dropDown.show()
                    }
                    else
                    {
                        self.dropDown.hide()
                    }
                    
                }else{
                    //  TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
  
    func dropdownfolderSelection()
    {
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            let textFieldText: String = self.txtComment.text!
            if textFieldText.contains("@") {
                self.recursiveTextDrop(textViewText: textFieldText,item: item,index:index)
                return
            }
                
            else
            {
                self.txtComment.text! = self.txtComment.text! + item
                self.tagUsersList.append(self.friendListObj.data[index].UserProfileID)
            }
            self.dropDown.hide()
        }
    }
    
    func recursiveTextDrop(textViewText : String,item:String,index:Int)
    {
        let endIndex = textViewText.range(of: "@")!.upperBound
        let textFieldTextNew = textViewText.substring(to: endIndex).trimmingCharacters(in: .whitespacesAndNewlines)
        print(textFieldTextNew)
        
        if textFieldTextNew.contains("@") {
            let endIndex = textViewText.range(of: "@")!.upperBound
            let textFieldTextNe = textViewText.substring(from: endIndex).trimmingCharacters(in: .whitespacesAndNewlines)
            print(textFieldTextNe)
            if textFieldTextNe.contains("@")
            {
                self.tagString = self.tagString + textFieldTextNew
                recursiveTextDrop(textViewText: textFieldTextNe, item: item, index: index)
                return
            }
            else
            {
                
            }
        }
        print("after recursive \(textFieldTextNew)")
        self.txtComment.text! = self.tagString + textFieldTextNew + item
        self.tagUsersList.append(self.friendListObj.data[index].UserProfileID)
        print(self.tagUsersList)
    }
    

}
