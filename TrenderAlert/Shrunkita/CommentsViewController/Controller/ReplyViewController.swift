//
//  ReplyViewController.swift
//  TrenderAlert
//
//  Created by Admin on 27/02/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import AGEmojiKeyboard
import SDWebImage
import MBProgressHUD
import AVFoundation
import GrowingTextView
import Alamofire
import ActiveLabel
import DropDown
import SwiftyGif

protocol ChangeData {
    func fetchChangeData(Reply : String)
}



class ReplyViewController: UIViewController,AGEmojiKeyboardViewDelegate, AGEmojiKeyboardViewDataSource ,UITextViewDelegate,ActiveLabelDelegate,GrowingTextViewDelegate,UITextFieldDelegate{
  

    @IBOutlet weak var searchGif: UITextField!
    @IBOutlet weak var cnstGIfHeight: NSLayoutConstraint!
    @IBOutlet weak var cnstSearchGIfHeight: NSLayoutConstraint!
    @IBOutlet weak var viewGIFSearch: UIView!
    @IBOutlet weak var viewGIf: UIView!
    @IBOutlet var btnPlayPause: UIButton!
    @IBOutlet var lblRecordTime: UILabel!
    @IBOutlet var btnRecord: UIButton!
    @IBOutlet var cnstAudioHeight: NSLayoutConstraint!
    @IBOutlet var lblAudioTime: UILabel!
    @IBOutlet var audioSlider: UISlider!
    @IBOutlet var viewAudioComment: UIView!
    @IBOutlet var txtRply: GrowingTextView!
    @IBOutlet var tapCommentImg: UILongPressGestureRecognizer!
    @IBOutlet var tapCommentView: UILongPressGestureRecognizer!
    @IBOutlet var btnCamera: UIButton!
    @IBOutlet var collectionGifImg: UICollectionView!
    @IBOutlet var cnstReplyWidth: NSLayoutConstraint!
    @IBOutlet var imgReply: UIImageView!
    @IBOutlet var btnLike: UIButton!
    @IBOutlet var cnstReplyImgHeight: NSLayoutConstraint!
    @IBOutlet var btnReplyImg: UIButton!
    @IBOutlet var viewComment: UIView!
    @IBOutlet var viewUserComment: UIView!
    @IBOutlet var lblExpires: UILabel!
    @IBOutlet var lblUserComment: ActiveLabel!
    @IBOutlet var imgUserPic: UIImageView!
//    @IBOutlet var txtRply: UITextView!
    @IBOutlet var tblRply: UITableView!
    var commentId = Int()
    var CommentList = [String:Any]()
    var CommentData = ReplyListModel()
    var comment = CommentListData()
     var searchText = ""
    var trendId = Int()
    var gifId = Int()
    let picker = UIImagePickerController()
    var imgCollection = [AnyObject]()
    var txtData = String()
    var chatroom = String()
    var imgComment : UIImage? = nil
    var emojiImages = [#imageLiteral(resourceName: "time_clk"),
                       #imageLiteral(resourceName: "emoji_clk"),
                       #imageLiteral(resourceName: "bell_clk"),
                       #imageLiteral(resourceName: "flower_clk"),
                       #imageLiteral(resourceName: "car_clk"),
                       #imageLiteral(resourceName: "Recent_clk")]
    var emojiSelectedIndex = -1
    var emojiNotSelectedIndex = -1
    var delegate : ChangeData?
    var agEmojiKeyboardView = AGEmojiKeyboardView()
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var audioRecorderPlayer = AVPlayer()
    var audioPlayer : AVAudioPlayer!
    var timer : Timer?
    var timeInMin = 600
    var audioFilename : URL!
    var friendListObj = FriendList()
    var dropDown = DropDown()
    var duration = String()
    var updateTimerRecording = Timer()
    var updateTimerForDownloadedRecording = Timer()
    var updateTimerForRecorded = Timer()
    var audioPlayIndex = Int()
    var tagUsersList = [Int]()
    var tagString = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        setupNavaigationbar()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews()
    {
        self.collectionGifImg.collectionViewLayout.invalidateLayout()
        self.collectionGifImg.layoutSubviews()
       self.txtRply.makeCornerRadius(radius: 18)
        self.imgUserPic.makeCornerRadius(radius: self.imgUserPic.frame.size.height/2)
    }
    
    
    @IBAction func btn_BackAction(_ sender: Any)
    {
        self.updateTimerForRecorded.invalidate()
        delegate?.fetchChangeData(Reply: "Change")
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func didSelect(_ text: String, type: ActiveType) {
        print(text)
    }
    
    
    func setupNavaigationbar(){
        Utilities.setNavigationBarWithTitle(viewController: self, title: "")
        let leftButton = UIBarButtonItem(image: UIImage(named: "img_BackArrow"), style: .plain, target: self, action: #selector(goBack))
        navigationItem.leftBarButtonItem = leftButton
    }
    
    @objc func goBack(){
        self.updateTimerForRecorded.invalidate()
        delegate?.fetchChangeData(Reply: "Change")
        self.navigationController?.popViewController(animated: true)
    }
    
    func initialSetup()
    {
        self.collectionGifImg.register(UINib(nibName: "SelectImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SelectImageCollectionViewCell")
        self.cnstGIfHeight.constant = 0
        self.cnstSearchGIfHeight.constant = 0
        self.viewGIFSearch.makeCornerRadius(radius: self.viewGIFSearch.frame.height/2)
        self.viewGIFSearch.giveBorderColor(color: UIColor.darkGray)
        SocketIoManager.sharedInstance.socketIOManagerDelegateReply = self
         self.btnPlayPause.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)

        self.lblRecordTime.text = ""
        self.txtRply.placeholder = MySingleton.shared.selectedLangData.Write_a_reply
        self.txtRply.placeholderColor = UIColor.lightGray
        self.txtRply.textContainerInset = UIEdgeInsets(top: 6, left: 8, bottom: 0, right: 100)
        //    self.txtCommentView.translatesAutoresizingMaskIntoConstraints = true
        self.txtRply.giveBorderColor(color: .lightGray)
        // self.viewComment.giveBorderColor(color: .lightGray)
//        self.txtRply.makeCornerRadius(radius: 18)
//        self.txtRply.delegate = self
        
        self.txtRply.giveBorderColor(color: .lightGray)
        self.txtRply.makeCornerRadius(radius: 18)
        self.txtRply.delegate = self
         self.searchGif.delegate = self
        Utilities.setNavigationBar(viewController: self)
        self.tblRply.tableFooterView = UIView()
//        self.viewComment.giveBorderColor(color: .lightGray)
        self.tblRply.register(UINib(nibName: "ReplyTableViewCell", bundle: nil), forCellReuseIdentifier: "ReplyTableViewCell")
        registerHeader()
        self.picker.delegate = self
      
        self.commentId = self.CommentList["commentId"] as! Int
      //   self.audioSlider.setThumbImage(UIImage(named:"img_SilderThumb"), for: .highlighted)
 
        dropDown.anchorView = self.txtRply
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)
        dropdownfolderSelection()
        
        self.lblUserComment.delegate = self
        
        let name = "\(self.CommentList["firstName"] as! String) " + "\(self.CommentList["lastName"] as! String) "
        
        let customType = ActiveType.custom(pattern: "\\sare\\b") //Looks for "are"
        let customType2 = ActiveType.custom(pattern: "\\sit\\b") //Looks for "it"
        let customType3 = ActiveType.custom(pattern: "\(name)") //Looks for "supports"
        
        self.lblUserComment.enabledTypes.append(customType)
        self.lblUserComment.enabledTypes.append(customType2)
        self.lblUserComment.enabledTypes.append(customType3)
        
        self.lblUserComment.urlMaximumLength = 31
        
        self.lblUserComment.customize { label in
            label.text = "\(name)\(self.CommentList["commentName"] ?? "")"
            label.numberOfLines = 0
            
            label.textColor = UIColor.black
            label.hashtagColor = UIColor.tagColor
            label.mentionColor = UIColor.tagColor
            label.URLColor = UIColor.tagColor
            label.URLSelectedColor = UIColor.tagColor
            
            
         //   label.handleMentionTap { self.alert("Mention", message: $0) }
         //   label.handleHashtagTap { self.alert("Hashtag", message: $0) }
            label.handleURLTap { guard var url = URL(string: $0.absoluteString) else { return }
                if !((url.absoluteString).contains("http"))
                {
                    let strUrl = (url.absoluteString).replacingOccurrences(of: "www.", with: "https://")
                    url = URL(string: strUrl)!
                }
                UIApplication.shared.open(url) }
            
            //Custom types
            
            label.customColor[customType] = UIColor.purple
            label.customSelectedColor[customType] = UIColor.green
            label.customColor[customType2] = UIColor.magenta
            label.customSelectedColor[customType2] = UIColor.green
            
            label.configureLinkAttribute = { (type, attributes, isSelected) in
                var atts = attributes
                switch type {
                case customType3:
                    atts[NSAttributedString.Key.font] = isSelected ? UIFont.boldSystemFont(ofSize: 13) : UIFont.boldSystemFont(ofSize: 13)
                    print(attributes)
                    print(type)
                default: ()
                }
                return atts
            }
        }
        
        if self.CommentList["commentAudio"] as! String != ""
        {
            self.cnstAudioHeight.constant = 40.0
            self.audioSlider.value = 0.0
          //  self.audioSlider.tag = 0
        }
        else
        {
            self.cnstAudioHeight.constant = 0
        }
        
        if self.CommentList["isLike"] as! Bool == true
        {
            btnLike.setTitleColor(UIColor(displayP3Red: 255.0/255.0, green: 83.0/255.0, blue: 119.0/255.0, alpha: 1.0), for: .normal)
        }
        else
        {
            btnLike.setTitleColor(UIColor.black, for: .normal)
        }
        
        self.imgUserPic.sd_setImage(with: URL(string: self.CommentList["commentProfile"] as? String ?? ""), placeholderImage: #imageLiteral(resourceName: "img_selectProfilePic"))
       
        if self.CommentList["commentImg"] as! String != ""
        {
            self.cnstReplyImgHeight.constant = 120
            let dataType = String((self.CommentList["commentImg"] as! String).suffix(3))
            print(dataType)
            if dataType == "gif"
            {

                do {
                    
                    let data = try? Data(contentsOf: URL(string: self.CommentList["commentImg"] as! String)!)
                    if data != nil {
                        
                        self.imgReply.image = UIImage(data: data!)
                        var calculatedWidth = CGFloat()
                        calculatedWidth = (Utilities.getImageWidth(image: self.imgReply.image!))
                        var calculatedHeight = CGFloat()
                        calculatedHeight = (Utilities.getImageHeight(image: self.imgReply.image!))
                        
                        let updatedwidth = (calculatedWidth * 120)/calculatedHeight
                        
                        self.imgReply.frame = CGRect(x: 0, y: 0, width: updatedwidth, height: 120)
                        print(updatedwidth)
                        self.cnstReplyWidth.constant = updatedwidth
                        
                        let imageData = try Data(contentsOf: URL(string:self.CommentList["commentImg"] as! String)!)
                        DispatchQueue.global(qos: .background).async {
                            DispatchQueue.main.async {
                                self.imgReply.image = UIImage.sd_animatedGIF(with: imageData)
                            }
                        }
                    }
                } catch {
                    print("Unable to load data: \(error)")
                }
            }
            else
            {
                do {
                    
                    let data = try? Data(contentsOf: URL(string: self.CommentList["commentImg"] as! String)!)
                    if data != nil {
                        
                        self.imgReply.image = UIImage(data: data!)
                        var calculatedWidth = CGFloat()
                        calculatedWidth = (Utilities.getImageWidth(image: self.imgReply.image!))
                        var calculatedHeight = CGFloat()
                        calculatedHeight = (Utilities.getImageHeight(image: self.imgReply.image!))
                        
                        let updatedwidth = (calculatedWidth * 120)/calculatedHeight
                        
                        self.imgReply.frame = CGRect(x: 0, y: 0, width: updatedwidth, height: 120)
                        print(updatedwidth)
                        self.cnstReplyWidth.constant = updatedwidth
                        
                        self.cnstReplyWidth.constant = updatedwidth
                        if self.cnstReplyWidth.constant > self.view.frame.size.width - 100
                        {
                            self.cnstReplyWidth.constant = self.view.frame.size.width - 100
                        }
                        else
                        {
                           self.cnstReplyWidth.constant = updatedwidth
                        }
                        self.imgReply.sd_setImage(with: URL(string: self.CommentList["commentImg"] as! String), placeholderImage: #imageLiteral(resourceName: "img_RecoveryIcon"))
                        
                    }
                } catch {
                    print("Unable to load data: \(error)")
                }
               //  self.btnReplyImg.sd_setImage(with: URL(string: (self.CommentList["commentImg"] as! String)), for: .disabled, completed: nil)
            }
        }
        else
        {
             self.cnstReplyImgHeight.constant = 0
        }
        self.lblExpires.text = self.CommentList["expiry"] as? String
        self.lblAudioTime.text = self.CommentList["audioDuration"] as? String
        getAllRplyComments()
    }
    
    func registerHeader(){
        let headerNib = UINib.init(nibName: "CommentHeader", bundle: Bundle.main)
        tblRply.register(headerNib, forHeaderFooterViewReuseIdentifier: "CommentHeader")
        self.tblRply.sectionHeaderHeight = UITableView.automaticDimension
        self.tblRply.estimatedSectionHeaderHeight = 80
        self.tblRply.delegate = self
        self.tblRply.dataSource = self
    }
    
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        print("Add line")
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in
            self.view.layoutIfNeeded()
        }, completion: nil)
        //        UIView.animate(withDuration: 0.2) {
        //            self.view.layoutIfNeeded()
        //        }
    }
    
    func textViewDidChange(_ textView: UITextView)
    {
        
        if self.txtRply.inputView == agEmojiKeyboardView
        {
            self.txtData = self.txtRply.text
            // self.txtCommentView.inputView = nil
            self.txtRply.inputView = UIView()
            // self.txtCommentView.keyboardType = UIKeyboardType.default
            self.txtRply.keyboardType = UIKeyboardType.default
            self.txtRply.keyboardAppearance = .default
            self.txtRply.becomeFirstResponder()
            self.txtRply.text = self.txtData
        }
        else
        {
            var textFieldText: String = textView.text
            if textFieldText.contains("@") {
                recursiveText(textViewText: textFieldText)
            }
            else
            {
                self.dropDown.hide()
            }
        }
    }
    
    func recursiveText(textViewText : String)
    {
        print("before recursive \(textViewText)")
        let endIndex = textViewText.range(of: "@")!.upperBound
        let textFieldTextNew = textViewText.substring(from: endIndex).trimmingCharacters(in: .whitespacesAndNewlines)
        print(textFieldTextNew)
        if textFieldTextNew.contains("@") {
            recursiveText(textViewText: textFieldTextNew)
            return
        }
        print("after recursive \(textFieldTextNew)")
        getFriendList(Skip: 0, Take: 15, lazyLoading: true,SearchString: textFieldTextNew)
    }
    
    func getFriendList(Skip: Int, Take: Int, lazyLoading: Bool,SearchString:String = ""){
        
        
        var param = ["Skip":Skip,"Take":Take,"Search":SearchString] as [String : Any]
        print(SearchString)
        WebServices().callUserService(service: UserServices.getFriendList, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: true, isHeader: true, CallMethod: .post){(serviceResponse,serviceData) in
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    DispatchQueue.main.async {
                    self.friendListObj.data.removeAll()
                    self.friendListObj = WebServices().decodeDataToClass(data:serviceData, decodeClass: FriendList.self)!
                    // self.friendListObj.data += temp.data
                    if self.friendListObj.data.count != 0
                    {
                        var dataString = [String]()
                        for i in 0...self.friendListObj.data.count-1
                        {
                           dataString.append("\(self.friendListObj.data[i].Firstname!)" + "\(self.friendListObj.data[i].Lastname!)")
                        }
                        self.dropDown.dataSource = dataString
                        self.dropDown.show()
                    }
                    else
                    {
                        self.dropDown.hide()
                    }
                    }
                }else{
                    //  TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
  
    func dropdownfolderSelection()
    {
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            let textFieldText: String = self.txtRply.text!
            if textFieldText.contains("@") {
                self.recursiveTextDrop(textViewText: textFieldText,item: item,index:index)
                return
            }
                
            else
            {
                self.txtRply.text! = self.txtRply.text! + item
                self.tagUsersList.append(self.friendListObj.data[index].UserProfileID)
            }
            self.dropDown.hide()
        }
    }
    
    func recursiveTextDrop(textViewText : String,item:String,index:Int)
    {
        let endIndex = textViewText.range(of: "@")!.upperBound
        let textFieldTextNew = textViewText.substring(to: endIndex).trimmingCharacters(in: .whitespacesAndNewlines)
        print(textFieldTextNew)
        
        if textFieldTextNew.contains("@") {
            let endIndex = textViewText.range(of: "@")!.upperBound
            let textFieldTextNe = textViewText.substring(from: endIndex).trimmingCharacters(in: .whitespacesAndNewlines)
            print(textFieldTextNe)
            if textFieldTextNe.contains("@")
            {
                self.tagString = self.tagString + textFieldTextNew
                recursiveTextDrop(textViewText: textFieldTextNe, item: item, index: index)
                return
            }
            else
            {
                
            }
        }
        print("after recursive \(textFieldTextNew)")
        self.txtRply.text! = self.tagString + textFieldTextNew + item
        self.tagUsersList.append(self.friendListObj.data[index].UserProfileID)
        print(self.tagUsersList)
        // dropdownfolderSelection()
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        //  ...
    }
    
    
    // MARK: - get all gif from api
    func getAllGIF(Skip: Int, Take: Int, lazyLoading: Bool,SearchString:String = "")
    {
        self.imgCollection.removeAll()
        let param = ["SearchTag":SearchString,
                     "SortColumn":"",
                     "SortDirection":"",
                     "Skip":"0",
                     "Take":"10"]
        WebServices().callUserService(service: UserServices.getAllGIF, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: true, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, responseData) in
            print(serviceResponse)
            if(serviceResponse["Status"] as! Bool == true)
            {
                DispatchQueue.main.async {
                if (serviceResponse["data"] as! [AnyObject]).count != 0
                {
                    self.imgCollection = (serviceResponse["data"] as! [AnyObject])
                    self.collectionGifImg.delegate = self
                    self.collectionGifImg.dataSource = self
                    self.collectionGifImg.reloadData()
                }
                else
                {
                    self.imgCollection.removeAll()
                    self.collectionGifImg.reloadData()
                    // self.txtCommentView.resignFirstResponder()
                }
                }
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        })
    }
    
    // MARK: - get all comments reply from api
    
    func getAllRplyComments()
    {
        let param = ["CommentID":self.commentId,
                     "Search":"",
                     "SortColumn":"",
                     "SortDirection":"",
                     "Skip":0,
                     "Take":10] as [String : Any]
        WebServices().callUserService(service: UserServices.getAllReply, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, responseData) in
            print(serviceResponse)
            if(serviceResponse["Status"] as! Bool == true)
            {
                DispatchQueue.main.async {

                let headerView = self.tblRply.headerView(forSection: 0)
                if let headerView = headerView as? CommentHeader{

                headerView.lblUserComment.delegate = self
                headerView.viewUserComment.makeCornerRadius(radius: 6)
                headerView.imgUserPic.makeCornerRadius(radius: headerView.imgUserPic.frame.height/2)
                self.CommentData.data.removeAll()

                let name = "\(self.CommentList["firstName"] as! String) " + "\(self.CommentList["lastName"] as! String) "

//                let customType = ActiveType.custom(pattern: "\\sare\\b") //Looks for "are"
//                let customType2 = ActiveType.custom(pattern: "\\sit\\b") //Looks for "it"
                let customType3 = ActiveType.custom(pattern: "\(name)") //Looks for "supports"

//                self.lblUserComment.enabledTypes.append(customType)
//                self.lblUserComment.enabledTypes.append(customType2)
                headerView.lblUserComment.enabledTypes.append(customType3)

                headerView.lblUserComment.urlMaximumLength = 31

                headerView.lblUserComment.customize { label in
                    label.text = "\(name)\(self.CommentList["commentName"] ?? "")"
                    label.numberOfLines = 0

                    label.textColor = UIColor.black
                    label.hashtagColor = UIColor.tagColor
                    label.mentionColor = UIColor.tagColor
                    label.URLColor = UIColor.tagColor
                    label.URLSelectedColor = UIColor.tagColor


                    //   label.handleMentionTap { self.alert("Mention", message: $0) }
                    //   label.handleHashtagTap { self.alert("Hashtag", message: $0) }
                    label.handleURLTap { guard var url = URL(string: $0.absoluteString) else { return }
                        if !((url.absoluteString).contains("http"))
                        {
                            let strUrl = (url.absoluteString).replacingOccurrences(of: "www.", with: "https://")
                            url = URL(string: strUrl)!
                        }
                        UIApplication.shared.open(url) }

                    var tagArray = String()
                    let taggUserData = self.CommentList["tagUsers"] as! [TaggedUsersData]

                    label.handleMentionTap {
                        if taggUserData.count != 0
                        {
                            for i in 0...taggUserData.count-1
                            {
                                tagArray.append("\(taggUserData[i].UserProfileFirstName)\(taggUserData[i].UserProfileLastName)")
                            }

                            print(tagArray)

                            for a in 0...taggUserData.count-1
                            {
                                print("\(taggUserData[a].UserProfileFirstName)\(taggUserData[a].UserProfileLastName)")
                                print("\($0)")
                                if $0 == "\(taggUserData[a].UserProfileFirstName)\(taggUserData[a].UserProfileLastName)"
                                {
                                    tagArray.removeAll()
                                    print(taggUserData[a].UserProfileID)
                                    let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
                                    let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
                                    profileVC.userId = taggUserData[a].UserProfileID
                                    self.navigationController?.pushViewController(profileVC, animated: true)
                                    return
                                }
                                else
                                {
                                    //self.alert("ALert", message: "No user found")
                                }
                            }
                            self.alert("ALert", message: "No user found")
                        }
                        else
                        {
                            self.alert("ALert", message: "No user found")
                        }
                    }


                    //Custom types

//                    label.customColor[customType] = UIColor.purple
//                    label.customSelectedColor[customType] = UIColor.green
//                    label.customColor[customType2] = UIColor.magenta
//                    label.customSelectedColor[customType2] = UIColor.green

                    label.configureLinkAttribute = { (type, attributes, isSelected) in
                        var atts = attributes
                        switch type {
                        case customType3:
                            atts[NSAttributedString.Key.font] = isSelected ? UIFont.boldSystemFont(ofSize: 13) : UIFont.boldSystemFont(ofSize: 13)
                            print(attributes)
                            print(type)
                        default: ()
                        }
                        return atts
                    }
                }
                    if self.CommentList["commentAudio"] as! String != ""
                    {
                        headerView.cnstAudioHeight.constant = 40.0
                        headerView.audioSlider.value = 0.0
                        //  self.audioSlider.tag = 0
                    }
                    else
                    {
                        headerView.cnstAudioHeight.constant = 0
                    }
                    
                    if self.CommentList["isLike"] as! Bool == true
                    {
                        headerView.btnLike.setTitleColor(UIColor(displayP3Red: 255.0/255.0, green: 83.0/255.0, blue: 119.0/255.0, alpha: 1.0), for: .normal)
                    }
                    else
                    {
                        headerView.btnLike.setTitleColor(UIColor.black, for: .normal)
                    }
                    
                    headerView.imgUserPic.sd_setImage(with: URL(string: self.CommentList["commentProfile"] as? String ?? ""), placeholderImage: #imageLiteral(resourceName: "img_selectProfilePic"))
                    
                    if self.CommentList["commentImg"] as! String != ""
                    {
                        headerView.cnstReplyImgHeight.constant = 120
                        let dataType = String((self.CommentList["commentImg"] as! String).suffix(3))
                        print(dataType)
                        if dataType == "gif"
                        {
                            do {
                                
                                let data = try? Data(contentsOf: URL(string: self.CommentList["commentImg"] as! String)!)
                                if data != nil {
                                    
                                    headerView.imgReply.image = UIImage(data: data!)
                                    var calculatedWidth = CGFloat()
                                    calculatedWidth = (Utilities.getImageWidth(image: headerView.imgReply.image!))
                                    var calculatedHeight = CGFloat()
                                    calculatedHeight = (Utilities.getImageHeight(image: headerView.imgReply.image!))
                                    
                                    let updatedwidth = (calculatedWidth * 120)/calculatedHeight
                                    
                                    headerView.imgReply.frame = CGRect(x: 0, y: 0, width: updatedwidth, height: 120)
                                    print(updatedwidth)
                                    headerView.cnstReplyWidth.constant = updatedwidth
                                    
                                    let imageData = try Data(contentsOf: URL(string:self.CommentList["commentImg"] as! String)!)
                                    DispatchQueue.global(qos: .background).async {
                                        DispatchQueue.main.async {
                                            headerView.imgReply.image = UIImage.sd_animatedGIF(with: imageData)
                                        }
                                    }
                                }
                            } catch {
                                print("Unable to load data: \(error)")
                            }
                        }
                        else
                        {
                            do {
                                headerView.imgReply.sd_setImage(with: URL(string:  self.CommentList["commentImg"] as! String), placeholderImage: #imageLiteral(resourceName: "img_RecoveryIcon"))
                                
                                let data = try? Data(contentsOf: URL(string: self.CommentList["commentImg"] as! String)!)
                                if data != nil {
                                    
                                    //   self.imgReply.image = UIImage(data: data!)
                                    var calculatedWidth = CGFloat()
                                    calculatedWidth = (Utilities.getImageWidth(image: headerView.imgReply.image!))
                                    var calculatedHeight = CGFloat()
                                    calculatedHeight = (Utilities.getImageHeight(image: headerView.imgReply.image!))
                                    
                                    let updatedwidth = (calculatedWidth * 120)/calculatedHeight
                                    
                                    headerView.imgReply.frame = CGRect(x: 0, y: 0, width: updatedwidth, height: 120)
                                    print(updatedwidth)
                                    headerView.cnstReplyWidth.constant = updatedwidth
                                    
                                    headerView.cnstReplyWidth.constant = updatedwidth
                                    if headerView.cnstReplyWidth.constant > self.view.frame.size.width - 100
                                    {
                                        headerView.cnstReplyWidth.constant = self.view.frame.size.width - 100
                                    }
                                    else
                                    {
                                        headerView.cnstReplyWidth.constant = updatedwidth
                                    }
                                    headerView.imgReply.sd_setImage(with: URL(string: self.CommentList["commentImg"] as! String), placeholderImage: #imageLiteral(resourceName: "img_RecoveryIcon"))
                                    
                                }
                            } catch {
                                print("Unable to load data: \(error)")
                            }
                            //  self.btnReplyImg.sd_setImage(with: URL(string: (self.CommentList["commentImg"] as! String)), for: .disabled, completed: nil)
                        }
                    }
                    else
                    {
                        headerView.cnstReplyImgHeight.constant = 0
                    }
                    headerView.lblExpires.text = self.CommentList["expiry"] as? String
                    headerView.lblAudioTime.text = self.CommentList["audioDuration"] as? String
                }
                
               
                
                self.CommentData = WebServices().decodeDataToClass(data: responseData, decodeClass: ReplyListModel.self)!
                if self.CommentData.data.count != 0
                {
                    self.tblRply.reloadData()
                    self.tblRply.reloadData()
                    self.tblRply.performBatchUpdates(nil, completion: {
                        (result) in
                        // ready
                        print("loaded")
                        self.tblRply.scrollToRow(at: IndexPath(row: self.CommentData.data.count - 1, section: 0), at: .bottom, animated: false)
                    })
                }
                else
                {
                    self.CommentData.data.removeAll()
                  
                    self.tblRply.reloadData()

                }
            }
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        })
    }
    
    @IBAction func btn_GifAction(_ sender: Any)
    {
        
        if self.cnstGIfHeight.constant == 0
        {
            getAllGIF(Skip: 0, Take: 15, lazyLoading: true)
            self.cnstGIfHeight.constant = 136
            self.cnstSearchGIfHeight.constant = 32
        }
        else
        {
            self.cnstGIfHeight.constant = 0
            self.cnstSearchGIfHeight.constant = 0
        }
    }
    
    func encodeToBase64String(image: UIImage) -> String
    {
        let imageData = image.jpegData(compressionQuality: 0.5)
        let imag_str:String = imageData!.base64EncodedString(options:[])
        //print(imag_str)
        return imag_str
    }
    
    @IBAction func btn_EmojiAction(_ sender: Any)
    {
        agEmojiKeyboardView = AGEmojiKeyboardView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216), dataSource: self)
        agEmojiKeyboardView.autoresizingMask = .flexibleHeight
        agEmojiKeyboardView.delegate = self
        self.txtRply.inputView = agEmojiKeyboardView
        self.txtRply.becomeFirstResponder()
        self.txtRply.text = self.txtData
        self.txtRply.inputView = agEmojiKeyboardView
    }
    
    @IBAction func bnt_RecordAudio(_ sender: Any)
    {
        if self.audioRecorder == nil {
            self.startRecording()
            self.btnRecord.setImage(#imageLiteral(resourceName: "record"), for: .normal)
        }
        else {
            self.finishRecording(success: true)
            self.btnRecord.setImage(#imageLiteral(resourceName: "UnRecord"), for: .normal)
        }
    }
    
//    @objc func downloadFileFromURL(_ sender: UIButton){
//        let url = URL(fileURLWithPath: self.CommentData.data[sender.tag].ReplyAudio!)
//        var downloadTask:URLSessionDownloadTask
//        downloadTask = URLSession.shared.downloadTask(with: url as URL, completionHandler: { (URL, response, error) -> Void in
//            //self.count += 1
//            if error != nil {
//                print("There was some problem with the Sound")
//            } else {
//                print(URL!)
//                //                DispatchQueue.main.async {
//                //                    MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
//                //                }
//                self.play(url: URL! as URL)
//                //  self.play(url: URL! as URL, indexpath: sender.tag)
//            }
//        })
//
//        downloadTask.resume()
//    }
    
//    @objc func updateTime() {
//
//        let cell_indexPath = NSIndexPath(row: self.audioPlayIndex, section: 0)
//        let tableViewCell = self.tblRply.cellForRow(at: cell_indexPath as IndexPath) as! ReplyTableViewCell
//
//        let currentTime = Int(self.audioRecorderPlayer.currentTime)
//        let duration = Int(self.audioRecorderPlayer.duration)
//        let total = currentTime - duration
//        let totalString = String(total)
//
//        let minutes = currentTime/60
//        let seconds = currentTime - minutes / 60
//
//        tableViewCell.lblTime.text = NSString(format: "%02d:%02d", minutes,seconds) as String
//        tableViewCell.audioSlider.value = Float(self.audioPlayer.currentTime)
//        print("Timer Update for recording list")
//
//
//    }
    
    
    @objc func updateTimeForRecordingSlider() {
//        //  MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
        let cell_indexPath = NSIndexPath(row: self.audioPlayIndex, section: 0)
        let tableViewCell = self.tblRply.cellForRow(at: cell_indexPath as IndexPath) as? ReplyTableViewCell

        
        let currentTime = Int(Float(CMTimeGetSeconds((tableViewCell?.audioPlayer.currentItem!.currentTime())!)))
        let duration = Int(Float(CMTimeGetSeconds((tableViewCell?.audioPlayer.currentItem!.asset.duration)!)))
        let total = currentTime - duration
        let totalString = String(total)
        
        let minutes = currentTime/60
        let seconds = currentTime - minutes / 60
        print("Current time \(currentTime)")
        tableViewCell?.audioSlider.value = Float(CMTimeGetSeconds((tableViewCell?.audioPlayer.currentItem!.currentTime())!))
        tableViewCell?.lblTime.text = NSString(format: "%02d:%02d", minutes,seconds) as String
        //
        print("Timer Update for added recording \(tableViewCell?.lblTime.text)")
        
        if currentTime == duration
        {
            print("Timer Update to 0")
            tableViewCell?.audioPlayer.pause()
            tableViewCell?.audioSlider.value = 0.0
            tableViewCell?.btnPlayPause.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
            self.updateTimerForRecorded.invalidate()
        }
        
    }
    
    @objc func updateTimeForRecordingSliderComment() {
        //        //  MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
      
        let currentTime = Int(Float(CMTimeGetSeconds(audioRecorderPlayer.currentItem!.currentTime())))
        let duration = Int(Float(CMTimeGetSeconds(audioRecorderPlayer.currentItem!.asset.duration)))
        let total = currentTime - duration
        let totalString = String(total)
        
        let minutes = currentTime/60
        let seconds = currentTime - minutes / 60
        print("Current time \(currentTime)")
        self.audioSlider.value = Float(CMTimeGetSeconds(audioRecorderPlayer.currentItem!.currentTime()))
        self.lblAudioTime.text = NSString(format: "%02d:%02d", minutes,seconds) as String
        //
        print("Timer Update for added recording \(self.lblAudioTime.text)")
        
        if currentTime == duration
        {
            print("Timer Update to 0")
            audioRecorderPlayer.pause()
            self.audioSlider.value = 0.0
            self.btnPlayPause.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
            self.updateTimerForRecorded.invalidate()
        }
        
    }
    
    @objc func updateTimeForRecording(_ sender: Timer) {
        
        //   print(audioRecorder)
        if audioRecorder == nil {
            updateTimerRecording.invalidate()
            return
        }
        
        let currentTime = Int(self.audioRecorder.currentTime)
        let minutes = currentTime/60
        let seconds = currentTime - minutes / 60
         print("seconds \(seconds)")
        self.lblRecordTime.text = NSString(format: "%02d:%02d", minutes,seconds) as String
        //
        lblRecordTime.text = self.lblRecordTime.text! + MySingleton.shared.selectedLangData.tap_To_stop_recording_audio
        self.duration = NSString(format: "%02d:%02d", minutes,seconds) as String
        print("Timer Update \(lblRecordTime.text!)")
        
    }
    
    
    @IBAction func commentSliderActionChange(_ sender: UISlider) {
        if self.audioRecorderPlayer.currentItem != nil
        {
            let currentTime = Int(Float(CMTimeGetSeconds(self.audioRecorderPlayer.currentItem!.currentTime())))
            let duration = Int(Float(CMTimeGetSeconds(self.audioRecorderPlayer.currentItem!.asset.duration)))
            
            let minutes = Int64(sender.value)/60
            let seconds = Int64(sender.value) - minutes / 60
            print("Current time \(currentTime)")
            let targetTime:CMTime = CMTimeMake(value: seconds, timescale: 1)
            self.audioRecorderPlayer.seek(to: targetTime)
            
            self.audioSlider.value = Float(CMTimeGetSeconds(targetTime))
            self.lblAudioTime.text = NSString(format: "%02d:%02d", minutes,seconds) as String
            //
            print("Timer Update for added recording \(self.lblAudioTime.text)")
            
            if currentTime == duration
            {
                print("Timer Update to 0")
                self.audioRecorderPlayer.pause()
                self.audioSlider.value = 0.0
                self.btnPlayPause.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
                self.updateTimerForRecorded.invalidate()
            }
        }
        else
        {
            DispatchQueue.main.async {
                self.play(url: URL(string:self.CommentList["commentAudio"] as! String)!, comment: "comment")
                //self.downloadFileFromURL(url: URL(string:self.CommentList["commentAudio"] as! String)!,comment : "comment")
            }
            self.audioPlayIndex = sender.tag
        }
    }
    
    
    @objc func sliderActionForValueChanged(_ sender: UISlider) {
        let cell_indexPath = NSIndexPath(row: sender.tag, section: 0)
        let tableViewCell = self.tblRply.cellForRow(at: cell_indexPath as IndexPath) as? ReplyTableViewCell
        
        if tableViewCell?.audioPlayer.currentItem != nil
        {
            let currentTime = Int(Float(CMTimeGetSeconds((tableViewCell?.audioPlayer.currentItem!.currentTime())!)))
            let duration = Int(Float(CMTimeGetSeconds((tableViewCell?.audioPlayer.currentItem!.asset.duration)!)))
            
            let minutes = Int64(sender.value)/60
            let seconds = Int64(sender.value) - minutes / 60
            print("Current time \(currentTime)")
            let targetTime:CMTime = CMTimeMake(value: seconds, timescale: 1)
            tableViewCell?.audioPlayer.seek(to: targetTime)
            
            tableViewCell?.audioSlider.value = Float(CMTimeGetSeconds(targetTime))
            tableViewCell?.lblTime.text = NSString(format: "%02d:%02d", minutes,seconds) as String
            //
            print("Timer Update for added recording \(tableViewCell?.lblTime.text)")
            
            if currentTime == duration
            {
                print("Timer Update to 0")
                tableViewCell?.audioPlayer.pause()
                tableViewCell?.audioSlider.value = 0.0
                tableViewCell?.btnPlayPause.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
                self.updateTimerForRecorded.invalidate()
            }
        }
        else
        {
                DispatchQueue.main.async {
                    //   self.downloadFileFromURL(url: URL(string:self.CommentData.data[sender.tag].CommentAudio!)!,tableView: "reply")
                    self.play(url: URL(string:self.CommentData.data[sender.tag].ReplyAudio!)!, comment: "reply")
                }
                self.audioPlayIndex = sender.tag
        }
    }
    
    func startRecording() {
        self.lblRecordTime.isHidden = false
        self.txtRply.text = ""
        self.txtRply.placeholder = ""
        self.audioFilename = nil
        self.btnRecord.setImage(#imageLiteral(resourceName: "record"), for: .normal)
        self.updateTimerRecording.invalidate()
         self.updateTimerForRecorded.invalidate()
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        //  self.loadRecordingUI()
                    } else {
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }

        
        self.audioFilename = getDocumentsDirectory().appendingPathComponent("recording.mp4")
        
        do {
            audioRecorder = try AVAudioRecorder(url: self.audioFilename, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record()
            print("Recording Started")
            
            DispatchQueue.main.async {
                
                self.updateTimerRecording = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTimeForRecording(_:)), userInfo: nil, repeats: true)
            }
            
            //   recordPlayButton.setImage(UIImage(named: "1X_Play"), for: .normal)
            
        } catch {
         //   finishRecording(success: false)
            self.btnRecord.setImage(#imageLiteral(resourceName: "UnRecord"), for: .normal)
            print("Recording Stoped")
        }
    }
    
    
    func finishRecording(success: Bool) {
        self.btnRecord.setImage(#imageLiteral(resourceName: "UnRecord"), for: .normal)
        self.txtRply.placeholder = MySingleton.shared.selectedLangData.Write_a_reply
        self.updateTimerForRecorded.invalidate()
        self.lblRecordTime.text = ""
        self.audioPlayIndex = 0
        self.audioRecorder?.stop()
        self.timer?.invalidate()
        self.updateTimerRecording.invalidate()
      //  self.audioRecorder = nil
        print("Finished Recording")
              
        let alertController = UIAlertController(title: "TrenderAlert", message: MySingleton.shared.selectedLangData.are_you_sure_you_want_to_send_this_audio_message , preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: MySingleton.shared.selectedLangData.ok, style: UIAlertAction.Style.default, handler: { (action) in
             self.audioRecorder = nil
            let dict = ["Duration":self.lblRecordTime.text!] as [String : Any]
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted)
                self.UploadAudio(parameter: jsonData as NSData)
                
            } catch {
                print(error.localizedDescription)
            }
            
        })
        alertController.addAction(okAction)
        let cancelAction = UIAlertAction(title: MySingleton.shared.selectedLangData.cancel, style: UIAlertAction.Style.default, handler: { (action) in
            self.audioRecorder?.stop()
            self.timer?.invalidate()
            self.updateTimerRecording.invalidate()
            self.audioRecorder = nil
            self.txtRply.placeholder = MySingleton.shared.selectedLangData.Write_a_reply
              self.txtRply.text = ""
            self.self.lblRecordTime.text = ""
        })
        alertController.addAction(cancelAction)
        UIApplication.shared.keyWindow!.rootViewController!.present(alertController, animated: true, completion: nil)
        
        self.btnRecord.setImage(#imageLiteral(resourceName: "UnRecord"), for: .normal)
        //        recordPlayButton.isEnabled = true
        
    }
    
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    
    @IBAction func btn_PlayPauseAction(_ sender: UIButton)
    {
        
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        //  self.loadRecordingUI()
                    } else {
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }

        
        if  self.audioRecorderPlayer.timeControlStatus == AVPlayer.TimeControlStatus.paused
        {
            if self.audioSlider.value > 0.0
            {
                self.audioRecorderPlayer.play()
                self.btnPlayPause.setImage(#imageLiteral(resourceName: "img_pauseAudio"), for: .normal)
            }
            else
            {
                DispatchQueue.main.async {
                    self.play(url: URL(string:self.CommentList["commentAudio"] as! String)!, comment: "comment")
                    //self.downloadFileFromURL(url: URL(string:self.CommentList["commentAudio"] as! String)!,comment : "comment")
                }
            }
        }
        else
        {
            self.audioRecorderPlayer.pause()
            self.btnPlayPause.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
        }
    }
    
    
    @objc func playPauseAction(_ sender: UIButton)
    {
        let cell_indexPath = NSIndexPath(row: sender.tag, section: 0)
        let tableViewCell = self.tblRply.cellForRow(at: cell_indexPath as IndexPath) as! ReplyTableViewCell
        recordingSession = AVAudioSession.sharedInstance()
        
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(.playAndRecord, mode: .default)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        //  self.loadRecordingUI()
                    } else {
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }

        if  tableViewCell.audioPlayer.timeControlStatus == AVPlayer.TimeControlStatus.paused
        {
            print("play")
            if tableViewCell.audioSlider.value > 0.0 && self.audioPlayIndex == sender.tag
            {
                tableViewCell.audioPlayer.play()
                tableViewCell.btnPlayPause.setImage(#imageLiteral(resourceName: "img_pauseAudio"), for: .normal)
            }
            else
            {
                self.updateTimerForRecorded.invalidate()
                for i in 0...self.CommentData.data.count-1
                {
                    if self.CommentData.data[i].ReplyAudio != nil
                    {
                        let cell_indexPath = NSIndexPath(row: i, section: 0)
                        let tableViewCell = self.tblRply.cellForRow(at: cell_indexPath as IndexPath) as! ReplyTableViewCell
                        tableViewCell.lblTime.text = self.CommentData.data[i].Duration
                        tableViewCell.btnPlayPause.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
                        tableViewCell.audioSlider.value = 0
                        tableViewCell.audioPlayer.pause()
                        self.updateTimerForRecorded.invalidate()
                    }
                }
                DispatchQueue.main.async {
                    self.play(url: URL(string:self.CommentData.data[sender.tag].ReplyAudio!)!, comment: "reply")
                  //  self.downloadFileFromURL(url: URL(string:self.CommentData.data[sender.tag].ReplyAudio!)!,comment : "reply")
                }
                self.audioPlayIndex = sender.tag
            }
        }
        else if tableViewCell.audioPlayer.timeControlStatus == AVPlayer.TimeControlStatus.playing
        {
            print("pause")
            tableViewCell.audioPlayer.pause()
            tableViewCell.btnPlayPause.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
            
        }
    }
    
    func downloadFileFromURL(url:URL,comment:String){
        var downloadTask:URLSessionDownloadTask
        downloadTask = URLSession.shared.downloadTask(with: url as URL, completionHandler: { (URL, response, error) -> Void in
            //self.count += 1
            if error != nil {
                print("There was some problem with the Sound")
            } else {
                print(URL!)
                self.play(url: url, comment: comment)
            }
        })
        
        downloadTask.resume()
    }
    
    func play(url:URL,comment:String)
    {
        if comment == "reply"
        {
        DispatchQueue.main.async {
            let cell_indexPath = NSIndexPath(row: self.audioPlayIndex, section: 0)
            let tableViewCell = self.tblRply.cellForRow(at: cell_indexPath as IndexPath) as! ReplyTableViewCell
            if tableViewCell.btnPlayPause.currentImage == #imageLiteral(resourceName: "img_PlayAudio")
            {
                print("playing \(url)")
                do {
                    tableViewCell.audioPlayer = try AVPlayer(url: url)
                    tableViewCell.audioPlayer.volume = 1.0
                    tableViewCell.audioPlayer.play()
                    tableViewCell.audioSlider.maximumValue = Float(CMTimeGetSeconds(tableViewCell.audioPlayer.currentItem!.asset.duration))
                    tableViewCell.btnPlayPause.setImage(#imageLiteral(resourceName: "img_pauseAudio"), for: .normal)
                    tableViewCell.audioSlider.value = 0.0
                    // self.audioPlayIndex = cell_indexPath.row
                    self.updateTimerForRecorded = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(ReplyViewController.updateTimeForRecordingSlider), userInfo: nil, repeats: true)
                    
                } catch let error as NSError {
                    //self.player = nil
                    print(error.localizedDescription)
                } catch {
                    print("AVAudioPlayer init failed")
                }
                
            }
            else
            {
                DispatchQueue.main.async {
                    let cell_indexPath = NSIndexPath(row: self.audioPlayIndex, section: 0)
                    let tableViewCell = self.tblRply.cellForRow(at: cell_indexPath as IndexPath) as! ReplyTableViewCell
                    tableViewCell.audioPlayer.pause()
                    tableViewCell.btnPlayPause.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
                }
            }
            
        }
        }
        else
        {
            DispatchQueue.main.async {
               
                if self.btnPlayPause.currentImage == #imageLiteral(resourceName: "img_PlayAudio")
                {
                    print("playing \(url)")
                    do {
                        self.audioRecorderPlayer = try AVPlayer(url: url)
                        self.audioRecorderPlayer.volume = 1.0
                        self.audioRecorderPlayer.play()
                        DispatchQueue.main.async {
                          
                            self.btnPlayPause.setImage(#imageLiteral(resourceName: "img_pauseAudio"), for: .normal)
                            self.audioSlider.maximumValue = Float(CMTimeGetSeconds(self.audioRecorderPlayer.currentItem!.asset.duration))
                            self.btnPlayPause.setImage(#imageLiteral(resourceName: "img_pauseAudio"), for: .normal)
                            self.audioSlider.value = 0.0
                            self.updateTimerForRecorded = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(ReplyViewController.updateTimeForRecordingSliderComment), userInfo: nil, repeats: true)
                        }
                        
                    } catch let error as NSError {
                        //self.player = nil
                        print(error.localizedDescription)
                    } catch {
                        print("AVAudioPlayer init failed")
                    }
                    //  self.audioRecorderPlayer.play()
                    
                    
                }
                else
                {
                    DispatchQueue.main.async {
                    
                        self.audioRecorderPlayer.pause()
                        self.btnPlayPause.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
                    }
                }
            }
        }
    }
    
    func UploadAudio(parameter:NSData?)-> Void{
        
        let boundary = "Boundary-\(UUID().uuidString)"
        DispatchQueue.main.async {
            MBProgressHUD.showAdded(to: self.view, animated: true)
        }
        let headers: HTTPHeaders = [
            "Authorization": "Bearer " + MySingleton.shared.loginObject.access_token,
            "content-type": "multipart/form-data; boundary= \(boundary)",
            "cache-control": "no-cache"
        ]
        
        //        var request = URLRequest(url: NSURL.init(string: "http://trenderalert.azurewebsites.net/api/trend/UploadTrendAudio/\(self.trendId)")! as URL)
//        var request = URLRequest(url: NSURL.init(string: "http://172.16.3.11:9091/api/comment/AddReplyAudio/\(self.commentId)")! as URL)
        var request = URLRequest(url: NSURL.init(string: "\(WebServices.baseURL)api/comment/AddReplyAudio/\(self.commentId)")! as URL)
        
        request.allHTTPHeaderFields = headers
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
       // request.httpBody = parameter! as Data
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(self.audioFilename!, withName: "fileUpload")
            let data = (self.duration).data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!
            print(data)
            multipartFormData.append(data,  withName: "Duration")
     
        }, with: request, encodingCompletion: {
            encodingResult in
            switch encodingResult {
            case .success(let upload, _,_):
                upload.responseJSON { response in
                    DispatchQueue.main.async {
                        MBProgressHUD.hide(for: self.view, animated: true)
                    }
                    debugPrint("SUCCESS RESPONSE: \(response)")
                    if(response.result.value is [String:AnyObject])
                    {
                        if let JSON  = response.result.value as? [String:AnyObject]{
                            print(JSON)
                            let loginObject = Login()
                             let data = JSON["data"] as! [AnyObject]
                            let socketData = ["chatroom":self.chatroom,
                                              "Id":data[0]["ReplyID"]!,
                                              "UserProfileId":MySingleton.shared.loginObject.UserID,
                                              "IsReply":1] as [String : Any]
                            SocketIoManager.sharedInstance.sendDataToEvent(.message, data: socketData)
                        }
                    }
                   
                    
                    self.CommentData.data.removeAll()
                    self.imgCollection.removeAll()
                    self.getAllRplyComments()
                    self.gifId = 0
                    self.imgComment = nil
                    self.audioPlayIndex = 0
                    self.txtRply.placeholder = MySingleton.shared.selectedLangData.Write_a_reply
                    self.lblRecordTime.text = ""
                    self.audioRecorder?.stop()
                    self.timer?.invalidate()
                    self.updateTimerRecording.invalidate()
                    self.audioRecorder = nil
                }
            case .failure(let encodingError):
                // hide progressbas here
                debugPrint("SUCCESS Failure: \(encodingError)")
            }
        })
        
    }
    
    
    @IBAction func btn_LikeCommentAction(_ sender: Any)
    {
            var param = [String : Any]()
            
            param = ["CommentID":self.commentId,
                      "TrendID":self.CommentList["trendId"] as! Int] as [String : Any]
            
            WebServices().callUserService(service: UserServices.likeUnLikeComment, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
                print(serviceResponse)
                if(serviceResponse["Status"] as! Bool == true)
                {
                    DispatchQueue.main.async {
                    if (sender as! UIButton).currentTitleColor == UIColor.black
                    {
                        (sender as! UIButton).setTitleColor(UIColor(displayP3Red: 255.0/255.0, green: 83.0/255.0, blue: 119.0/255.0, alpha: 1.0), for: .normal)
                    }
                    else
                    {
                        (sender as! UIButton).setTitleColor(UIColor.black, for: .normal)
                    }
                    }
                    //self.getAllComments()
                }
                else
                {
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            })

    }
    
    @objc func proceedToLikeRply(_ sender: UIButton)
    {
        let cell_indexPath = NSIndexPath(row: sender.tag, section: 0)
        let tableViewCellOther = self.tblRply.cellForRow(at: cell_indexPath as IndexPath) as! ReplyTableViewCell
        
        var param = [String : Any]()

        param = [ "ReplyID":self.CommentData.data[sender.tag].ReplyID] as [String : Any]
        
        
        WebServices().callUserService(service: UserServices.likeUnLikeReply, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
            if(serviceResponse["Status"] as! Bool == true)
            {
                DispatchQueue.main.async {
                if tableViewCellOther.btnLike.currentTitleColor == UIColor.black
                {
                    tableViewCellOther.btnLike.setTitleColor(UIColor(displayP3Red: 255.0/255.0, green: 83.0/255.0, blue: 119.0/255.0, alpha: 1.0), for: .normal)
                }
                else
                {
                    tableViewCellOther.btnLike.setTitleColor(UIColor.black, for: .normal)
                }
                }
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        })
    }
    
    
    
    
    @IBAction func btn_ReplyAction(_ sender: Any)
    {
        self.txtRply.resignFirstResponder()
    }
    
    
    @IBAction func btn_ShareAction(_ sender: Any)
    {
        addReply()
    }
    
    
    func callApi(requestData: Dictionary<String, Any>, url: String)
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        var urlStr: String = url
        
        urlStr = urlStr.replacingOccurrences(of: " ", with: "&")
        let request :NSMutableURLRequest = NSMutableURLRequest(url:NSURL(string:urlStr)! as URL)
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        if requestData.count != 0
        {
            do
            {
                let jsonData = try JSONSerialization.data(withJSONObject: requestData, options: JSONSerialization.WritingOptions.prettyPrinted)
                request.httpBody = jsonData
            }
            catch let error as NSError
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                }
            }
        }
        let TokenValue = "Bearer " + MySingleton.shared.loginObject.access_token
        print(TokenValue)
        request.setValue(TokenValue, forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        let task = URLSession.shared.dataTask(with: request as URLRequest){ data,response,error in
            
            if error != nil
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error!.localizedDescription, completionHandler: nil)
                }
                
            }
            do
            {
                if (response as? HTTPURLResponse) != nil
                {
                    
                    //////////////////// JSON RESPONSE /////////////////
                    
                    let jsonString = String(data: data!, encoding: .utf8)
                    print(jsonString!)
                    
                    //////////////////// JSON RESPONSE /////////////////
                    
                    if data != nil
                    {
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        
                        print(jsonData)
                        
                        if(jsonData["Status"] as! Bool == true)
                        {
                            DispatchQueue.main.async {
                                let loginObject = Login()
                                let socketData = ["chatroom":self.CommentList["chatRoom"]!,
                                                  "Id":jsonData["data"]!,
                                                  "UserProfileId":MySingleton.shared.loginObject.UserID,
                                                  "IsReply":1] as [String : Any]
                                SocketIoManager.sharedInstance.sendDataToEvent(.message, data: socketData)
                                self.getAllRplyComments()
                                self.gifId = 0
                                self.imgComment = nil
                                self.txtRply.placeholder = MySingleton.shared.selectedLangData.Write_a_reply
                                self.txtRply.text = ""
                                self.cnstGIfHeight.constant = 0
                            }
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                MBProgressHUD.hide(for: self.view, animated: true)
                                TrenderAlertVC.shared.presentAlertController(message: jsonData["Message"] as! String, completionHandler: nil)
                            }
                        }
                        
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true)
                            TrenderAlertVC.shared.presentAlertController(message: error!.localizedDescription, completionHandler: nil)
                        }
                    }
                    
                }
            }
            catch let error as NSError
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                }
            }
        }
        task.resume()
    }
    
    
    @IBAction func btn_CameraAction(_ sender: Any)
    {
        self.view.endEditing(true)
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: MySingleton.shared.selectedLangData.please_select_your_option, message: "", preferredStyle: .actionSheet)
        
        let cameraActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.camera, style: .default) { void in
            print("Camera")
            self.openCamera()
        }
        actionSheetControllerIOS8.addAction(cameraActionButton)
        
        let galleryActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.gallery, style: .default) { void in
            print("Gallery")
            self.openGallary()
        }
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.cancel, style: .default) { void in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(galleryActionButton)
        actionSheetControllerIOS8.addAction(cancelActionButton)
        actionSheetControllerIOS8.popoverPresentationController?.sourceView = self.btnCamera
        actionSheetControllerIOS8.popoverPresentationController?.sourceRect = self.btnCamera.bounds
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    // MARK: - Emoji Delegate
    func emojiKeyBoardView(_ emojiKeyBoardView: AGEmojiKeyboardView!, didUseEmoji emoji: String!) {
        self.txtRply.text = self.txtRply.text + emoji
    }
    
    func emojiKeyBoardViewDidPressBackSpace(_ emojiKeyBoardView: AGEmojiKeyboardView!) {
        self.txtRply.text = String(self.txtRply.text.dropLast())
    }
    
    // MARK: - Emoji Data Source
    func emojiKeyboardView(_ emojiKeyboardView: AGEmojiKeyboardView!, imageForSelectedCategory category: AGEmojiKeyboardViewCategoryImage) -> UIImage! {
        if emojiSelectedIndex <= 4 {
            emojiSelectedIndex = emojiSelectedIndex + 1
        }
        return emojiImages[emojiSelectedIndex]
    }
    
    func emojiKeyboardView(_ emojiKeyboardView: AGEmojiKeyboardView!, imageForNonSelectedCategory category: AGEmojiKeyboardViewCategoryImage) -> UIImage! {
        if emojiNotSelectedIndex <= 4 {
            emojiNotSelectedIndex = emojiNotSelectedIndex + 1
        }
        return emojiImages[emojiNotSelectedIndex]
    }
    
    func backSpaceButtonImage(for emojiKeyboardView: AGEmojiKeyboardView!) -> UIImage! {
        return #imageLiteral(resourceName: "backspace_clk")
    }
    
    // MARK: - Save comment
    
    func addReply()
    {
        self.view.endEditing(true)
        var param = [String : Any]()
        if self.imgComment != nil
        {
            var imageList = [AnyObject]()
            let imageData = ["FileExt":"jpg","contenttype":"image/jpg","ImageData":"data:image/jpg;base64,\(encodeToBase64String(image: self.imgComment!))"]
            imageList.append(imageData as AnyObject)
            param = ["ReplyID":0,"CommentId":self.commentId,"ReplyText":self.txtRply.text! ,"IsImage":"true",
                     "ImagePath":["Images": imageList],"gifID":"","TaggedUsers":self.tagUsersList] as [String : Any]
            //  print(param)
            callApi(requestData: param, url: "\(WebServices.baseURL)api/comment/ManageReplies")
            return
        }
        else
        {
            param = ["ReplyID":0,"CommentId":self.commentId,"ReplyText":self.txtRply.text! ,"IsImage":"false",
                     "ImagePath":[],"gifID":self.gifId,"TaggedUsers":self.tagUsersList] as [String : Any]
        }
        self.cnstGIfHeight.constant = 0
        if self.txtRply.text == "" && self.gifId == 0
        {
            return
        }
        
      //  let param = ["ReplyID":"0","CommentId":self.commentId,"ReplyText":self.txtRply.text] as [String : Any]
        WebServices().callUserService1(service: UserServices.addReply, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
            if(serviceResponse["Status"] as! Bool == true)
            {
                let loginObject = Login()
                  let data = serviceResponse["data"] as! [AnyObject]
                let socketData = ["chatroom":self.CommentList["chatRoom"]!,
                                  "Id":data[0]["ReplyID"]!,
                                  "UserProfileId":MySingleton.shared.loginObject.UserID,
                                  "IsReply":1] as [String : Any]
                SocketIoManager.sharedInstance.sendDataToEvent(.message, data: socketData)
                self.gifId = 0
                self.imgComment = nil
                self.txtRply.placeholder = MySingleton.shared.selectedLangData.Write_a_reply
                self.txtRply.text = ""
                self.getAllRplyComments()
            }
            else
            {
              //  TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        })
    }
    
    @objc func proceedToReply(_ sender: UIButton)
    {
       let name = "\(self.CommentData.data[sender.tag].UserFirstname) " + "\(self.CommentData.data[sender.tag].UserLastname) "
        self.txtRply.becomeFirstResponder()
        self.txtRply.text = name
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n"{
            textView.resignFirstResponder()
        }
        let textFieldText: NSString = (textView.text ?? "") as NSString
        let txtAfterUpdate = textFieldText.replacingCharacters(in: range, with: text)
        // newAnswers[textView.tag] = txtAfterUpdate
        return true
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
//        if textView.text == "Write Comments here..."
//        {
//            textView.text = ""
//        }
        if self.txtRply.inputView == agEmojiKeyboardView
        {
            self.txtData = self.txtRply.text
            self.txtRply.inputView = nil
          //  self.txtRply.inputView = UIView()
         //   self.txtRply.keyboardType = UIKeyboardType.default
            self.txtRply.becomeFirstResponder()
            self.txtRply.text = self.txtData
        }
    }
    
    @objc func longPressed(_ sender: UILongPressGestureRecognizer)
    {

        let touchPoint = sender.location(in: self.tblRply)
        if let indexPath = self.tblRply.indexPathForRow(at: touchPoint) {
            print("Long pressed row: \(indexPath.row)")
        }
        
        self.view.endEditing(true)
        let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: MySingleton.shared.selectedLangData.please_select_your_option, message: "", preferredStyle: .actionSheet)
   
        let galleryActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.delete, style: .default) { void in
            //   let touchPoint = sender.location(in: self.tblComment)
            if let indexPath = self.tblRply.indexPathForRow(at: touchPoint) {
                print("Long pressed row reply: \(indexPath.row)")
                self.proceedToDeleteRply(indexPath: indexPath.row)
            }
        }
        
        if let indexPath = self.tblRply.indexPathForRow(at: touchPoint) {
            if self.CommentData.data[indexPath.row].ReplyImage == nil && self.CommentData.data[indexPath.row].ReplyAudio == nil
            {
                let cameraActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.edit, style: .default) { void in
                    let storyboard = UIStoryboard(name: "Comment", bundle: nil)
                    let destination = storyboard.instantiateViewController(withIdentifier: "EditCommentViewController") as! EditCommentViewController
                    let data = ["tagUsers":self.CommentData.data[indexPath.row].TaggedUsers,"replyId":self.CommentData.data[indexPath.row].ReplyID,"isComment":false,"trendId":self.trendId,"commentid":self.CommentData.data[indexPath.row].CommentID,"commentTxt":self.CommentData.data[indexPath.row].ReplyText ?? ""] as [String : Any]
                    destination.data = data
                    destination.delegate = self
                    //  destination.txtComment.text = self.CommentData.data[indexPath.row].ReplyText
                    self.navigationController?.pushViewController(destination, animated: true)
                }
                actionSheetControllerIOS8.addAction(cameraActionButton)
            }
        }
        
        let cancelActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.cancel, style: .default) { void in
            print("Cancel")
        }
        actionSheetControllerIOS8.addAction(galleryActionButton)
        actionSheetControllerIOS8.addAction(cancelActionButton)
        actionSheetControllerIOS8.popoverPresentationController?.sourceView = self.tblRply
        actionSheetControllerIOS8.popoverPresentationController?.sourceRect = self.tblRply.bounds
        self.present(actionSheetControllerIOS8, animated: true, completion: nil)
    }
    
    func proceedToDeleteRply(indexPath: Int)
    {
        var param = [String : Any]()
        
        param = [ "ReplyID":self.CommentData.data[indexPath].ReplyID] as [String : Any]
        
        WebServices().callUserService(service: UserServices.deleteRply, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
            if(serviceResponse["Status"] as! Bool == true)
            {
               self.CommentData.data.removeAll()
               self.getAllRplyComments()
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        })
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        self.CommentData.data = []
        searchText = updatedString!
        // self.getGroupListing(Skip: 0, Take: 15, lazyLoading: true,SearchString: updatedString ?? "")
        self.getAllGIF(Skip: 0, Take: 15, lazyLoading: true,SearchString: updatedString ?? "")
        //        self.searchGroups(searchString: updatedString ?? "")
        return true
    }
  //  }
}

extension ReplyViewController : AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool)
    {
        DispatchQueue.main.async {
            let cell_indexPath = NSIndexPath(row: self.audioPlayIndex, section: 0)
            let tableViewCell = self.tblRply.cellForRow(at: cell_indexPath as IndexPath) as! ReplyTableViewCell
            tableViewCell.btnPlayPause.setImage(#imageLiteral(resourceName: "img_PlayAudio"), for: .normal)
        }
        self.updateTimerRecording.invalidate()
        self.updateTimerForRecorded.invalidate()
        self.updateTimerForDownloadedRecording.invalidate()
    }
    
}

extension ReplyViewController : UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate
{
    
    func alert(_ title: String, message: String) {
        let vc = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        vc.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        present(vc, animated: true, completion: nil)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.CommentData.data.count == 0
        {
            return 0
        }
        else
        {
           return self.CommentData.data.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       // return UIScreen.main.bounds.height * 0.2
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "CommentHeader") as! CommentHeader
        cell.lblUserComment.delegate = self
        cell.btnLike.setTitle(MySingleton.shared.selectedLangData.like, for: .normal)
        let name = "\(self.CommentList["firstName"] as! String) " + "\(self.CommentList["lastName"] as! String) "
        
        let customType = ActiveType.custom(pattern: "\\sare\\b") //Looks for "are"
        let customType2 = ActiveType.custom(pattern: "\\sit\\b") //Looks for "it"
        let customType3 = ActiveType.custom(pattern: "\(name)") //Looks for "supports"
        
        cell.lblUserComment.enabledTypes.append(customType)
        cell.lblUserComment.enabledTypes.append(customType2)
        cell.lblUserComment.enabledTypes.append(customType3)
        
        cell.lblUserComment.urlMaximumLength = 31
        
        self.lblUserComment.customize { label in
            label.text = "\(name)\(self.CommentList["commentName"] ?? "")"
            label.numberOfLines = 0
            
            label.textColor = UIColor.black
            label.hashtagColor = UIColor.tagColor
            label.mentionColor = UIColor.tagColor
            label.URLColor = UIColor.tagColor
            label.URLSelectedColor = UIColor.tagColor
            
            
            //   label.handleMentionTap { self.alert("Mention", message: $0) }
            //   label.handleHashtagTap { self.alert("Hashtag", message: $0) }
            label.handleURLTap { guard var url = URL(string: $0.absoluteString) else { return }
                if !((url.absoluteString).contains("http"))
                {
                    let strUrl = (url.absoluteString).replacingOccurrences(of: "www.", with: "https://")
                    url = URL(string: strUrl)!
                }
                UIApplication.shared.open(url) }
            
            //Custom types
            
            label.customColor[customType] = UIColor.purple
            label.customSelectedColor[customType] = UIColor.green
            label.customColor[customType2] = UIColor.magenta
            label.customSelectedColor[customType2] = UIColor.green
            
            label.configureLinkAttribute = { (type, attributes, isSelected) in
                var atts = attributes
                switch type {
                case customType3:
                    atts[NSAttributedString.Key.font] = isSelected ? UIFont.boldSystemFont(ofSize: 13) : UIFont.boldSystemFont(ofSize: 13)
                    print(attributes)
                    print(type)
                default: ()
                }
                return atts
            }
        }
        
        if self.CommentList["commentAudio"] as! String != ""
        {
            cell.cnstAudioHeight.constant = 40.0
            cell.audioSlider.value = 0.0
            //  self.audioSlider.tag = 0
        }
        else
        {
            cell.cnstAudioHeight.constant = 0
        }
        
        if self.CommentList["isLike"] as! Bool == true
        {
            cell.btnLike.setTitleColor(UIColor(displayP3Red: 255.0/255.0, green: 83.0/255.0, blue: 119.0/255.0, alpha: 1.0), for: .normal)
        }
        else
        {
            cell.btnLike.setTitleColor(UIColor.black, for: .normal)
        }
        
        cell.imgUserPic.sd_setImage(with: URL(string: self.CommentList["commentProfile"] as? String ?? ""), placeholderImage: #imageLiteral(resourceName: "img_selectProfilePic"))
        
        if self.CommentList["commentImg"] as! String != ""
        {
            cell.cnstReplyImgHeight.constant = 120
            let dataType = String((self.CommentList["commentImg"] as! String).suffix(3))
            print(dataType)
            if dataType == "gif"
            {
                do {
                    
                    let data = try? Data(contentsOf: URL(string: self.CommentList["commentImg"] as! String)!)
                    if data != nil {
                        
                        cell.imgReply.image = UIImage(data: data!)
                        var calculatedWidth = CGFloat()
                        calculatedWidth = (Utilities.getImageWidth(image: self.imgReply.image!))
                        var calculatedHeight = CGFloat()
                        calculatedHeight = (Utilities.getImageHeight(image: self.imgReply.image!))
                        
                        let updatedwidth = (calculatedWidth * 120)/calculatedHeight
                        
                        cell.imgReply.frame = CGRect(x: 0, y: 0, width: updatedwidth, height: 120)
                        print(updatedwidth)
                        cell.cnstReplyWidth.constant = updatedwidth
                        
                        let imageData = try Data(contentsOf: URL(string:self.CommentList["commentImg"] as! String)!)
                        DispatchQueue.global(qos: .background).async {
                            DispatchQueue.main.async {
                                cell.imgReply.image = UIImage.sd_animatedGIF(with: imageData)
                            }
                        }
                    }
                } catch {
                    print("Unable to load data: \(error)")
                }
            }
            else
            {
                do {
                    cell.imgReply.sd_setImage(with: URL(string:  self.CommentList["commentImg"] as! String), placeholderImage: #imageLiteral(resourceName: "img_RecoveryIcon"))
                    
                    let data = try? Data(contentsOf: URL(string: self.CommentList["commentImg"] as! String)!)
                    if data != nil {
                        
                     //   self.imgReply.image = UIImage(data: data!)
                        var calculatedWidth = CGFloat()
                        calculatedWidth = (Utilities.getImageWidth(image: cell.imgReply.image!))
                        var calculatedHeight = CGFloat()
                        calculatedHeight = (Utilities.getImageHeight(image: cell.imgReply.image!))
                        
                        let updatedwidth = (calculatedWidth * 120)/calculatedHeight
                        
                        cell.imgReply.frame = CGRect(x: 0, y: 0, width: updatedwidth, height: 120)
                        print(updatedwidth)
                        cell.cnstReplyWidth.constant = updatedwidth
                        
                        cell.cnstReplyWidth.constant = updatedwidth
                        if cell.cnstReplyWidth.constant > self.view.frame.size.width - 100
                        {
                            cell.cnstReplyWidth.constant = self.view.frame.size.width - 100
                        }
                        else
                        {
                            cell.cnstReplyWidth.constant = updatedwidth
                        }
                        cell.imgReply.sd_setImage(with: URL(string: self.CommentList["commentImg"] as! String), placeholderImage: #imageLiteral(resourceName: "img_RecoveryIcon"))
                        
                    }
                } catch {
                    print("Unable to load data: \(error)")
                }
                //  self.btnReplyImg.sd_setImage(with: URL(string: (self.CommentList["commentImg"] as! String)), for: .disabled, completed: nil)
            }
        }
        else
        {
            self.cnstReplyImgHeight.constant = 0
        }
        self.lblExpires.text = self.CommentList["expiry"] as? String
        self.lblAudioTime.text = self.CommentList["audioDuration"] as? String
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReplyTableViewCell", for: indexPath) as! ReplyTableViewCell
//        let name = "\(self.CommentData.data[indexPath.row].UserFirstname) " + "\(self.CommentData.data[indexPath.row].UserLastname)"
//        cell.lblCOmmentText.text = "\(name) \(self.CommentData.data[indexPath.row].ReplyText)"
        
        // Part of string to be bold
        cell.lblCOmmentText.delegate = self
        
        let name = "\(self.CommentData.data[indexPath.row].UserFirstname) " + "\(self.CommentData.data[indexPath.row].UserLastname) "
        
//        let customType = ActiveType.custom(pattern: "\\sare\\b") //Looks for "are"
//        let customType2 = ActiveType.custom(pattern: "\\sit\\b") //Looks for "it"
        let customType3 = ActiveType.custom(pattern: "\(name)") //Looks for "supports"
        
//        cell.lblCOmmentText.enabledTypes.append(customType)
//        cell.lblCOmmentText.enabledTypes.append(customType2)
        cell.lblCOmmentText.enabledTypes.append(customType3)
        
        cell.lblCOmmentText.urlMaximumLength = 31
        
        cell.lblCOmmentText.customize { label in
            label.text = "\(name)\(self.CommentData.data[indexPath.row].ReplyText ?? "")"
            //                label.text = "gayatri R"
            label.numberOfLines = 0
            //    label.lineSpacing = 4
            
            label.textColor = UIColor.black
            label.hashtagColor = UIColor.tagColor
            label.mentionColor = UIColor.tagColor
            label.URLColor = UIColor.tagColor
            label.URLSelectedColor = UIColor.tagColor
            
            var tagArray = [String]()
            
            label.handleMentionTap {
                if self.CommentData.data[indexPath.row].TaggedUsers.count != 0
                {
                    for i in 0...self.CommentData.data[indexPath.row].TaggedUsers.count-1
                    {
                        tagArray.append("\(self.CommentData.data[indexPath.row].TaggedUsers[i].UserProfileFirstName)\(self.CommentData.data[indexPath.row].TaggedUsers[i].UserProfileLastName)")
                    }
                    
                    print(self.CommentData.data[indexPath.row].TaggedUsers)
                    print(tagArray)
                    
                    for a in 0...self.CommentData.data[indexPath.row].TaggedUsers.count-1
                    {
                        print("\(self.CommentData.data[indexPath.row].TaggedUsers[a].UserProfileFirstName)\(self.CommentData.data[indexPath.row].TaggedUsers[a].UserProfileLastName)")
                        print("\($0)")
                        if $0 == "\(self.CommentData.data[indexPath.row].TaggedUsers[a].UserProfileFirstName)\(self.CommentData.data[indexPath.row].TaggedUsers[a].UserProfileLastName)"
                        {
                            tagArray.removeAll()
                            print(self.CommentData.data[indexPath.row].TaggedUsers[a].UserProfileID)
                            let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
                            let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
                            profileVC.userId = self.CommentData.data[indexPath.row].TaggedUsers[a].UserProfileID
                            self.navigationController?.pushViewController(profileVC, animated: true)
                            print(self.CommentData.data[indexPath.row].TaggedUsers[a].UserProfileID)
                            return
                        }
                        else
                        {
                            //self.alert("ALert", message: "No user found")
                        }
                    }
                    self.alert("ALert", message: "No user found")
                }
                else
                {
                    self.alert("ALert", message: "No user found")
                }
            }
            
            label.handleHashtagTap { self.alert("Hashtag", message: $0) }
            
            label.handleURLTap { guard var url = URL(string: $0.absoluteString) else { return }
                if !((url.absoluteString).contains("http"))
                {
                    let strUrl = (url.absoluteString).replacingOccurrences(of: "www.", with: "https://")
                    url = URL(string: strUrl)!
                }
                UIApplication.shared.open(url) }
            
//            label.handleMentionTap { self.alert("Mention", message: $0) }
//            label.handleHashtagTap { self.alert("Hashtag", message: $0) }
//            label.handleURLTap { self.alert("URL", message: $0.absoluteString) }
            
            //Custom types
            
//            label.customColor[customType] = UIColor.purple
//            label.customSelectedColor[customType] = UIColor.green
//            label.customColor[customType2] = UIColor.magenta
//            label.customSelectedColor[customType2] = UIColor.green
//
            label.configureLinkAttribute = { (type, attributes, isSelected) in
                var atts = attributes
                switch type {
                case customType3:
                    atts[NSAttributedString.Key.font] = isSelected ? UIFont.boldSystemFont(ofSize: 13) : UIFont.boldSystemFont(ofSize: 13)
                    print(attributes)
                    print(type)
                default: ()
                }
                return atts
            }
        }

        cell.lblExpires.text = self.CommentData.data[indexPath.row].TimeAgo
        
        
            cell.imgProfilePic.makeCornerRadius(radius: cell.imgProfilePic.frame.size.height/2)
            cell.imgProfilePic.sd_setImage(with: URL(string: self.CommentData.data[indexPath.row].ProfileImage ?? ""), placeholderImage: #imageLiteral(resourceName: "img_selectProfilePic"))
            cell.btnLike.tag = indexPath.row
            cell.btnLike.addTarget(self, action: #selector(self.proceedToLikeRply(_:)), for: .touchUpInside)
        
        if self.CommentData.data[indexPath.row].IsLiked == false
        {
            cell.btnLike.setTitleColor(UIColor.black, for: .normal)
        }
        else
        {
            cell.btnLike.setTitleColor(UIColor(displayP3Red: 100.0/255.0, green: 33.0/255.0, blue: 47.0/255.0, alpha: 1.0), for: .normal)
        }
        cell.btnRply.tag = indexPath.row
        cell.btnRply.addTarget(self, action: #selector(self.proceedToReply(_:)), for: .touchUpInside)
        
        if self.CommentData.data[indexPath.row].CanDelete == true
        {
            let longPress = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressed(_:)))
            let longPressImg = UILongPressGestureRecognizer(target: self, action: #selector(self.longPressed(_:)))
            cell.lblCOmmentText.isUserInteractionEnabled = true
            cell.imgRply.isUserInteractionEnabled = true
            longPress.delegate = self
            longPressImg.delegate = self
            cell.lblCOmmentText.addGestureRecognizer(longPressImg)
            cell.imgRply.addGestureRecognizer(longPress)
        }
        
        if self.CommentData.data[indexPath.row].ReplyImage == nil && self.CommentData.data[indexPath.row].ReplyAudio == nil
        {
            cell.cnstImgReplyHeight.constant = 0
            cell.cnstAudioHeight.constant = 0
        }
        else if self.CommentData.data[indexPath.row].ReplyAudio != nil
        {
            cell.cnstImgReplyHeight.constant = 0
            cell.cnstAudioHeight.constant = 40
            cell.lblTime.text = self.CommentData.data[indexPath.row].Duration
          
            cell.audioSlider.tag = indexPath.row
            cell.audioSlider.addTarget(self, action: #selector(self.sliderActionForValueChanged(_:)), for: .valueChanged)
            
            cell.btnPlayPause.tag = indexPath.row
            cell.btnPlayPause.addTarget(self, action: #selector(self.playPauseAction(_:)), for: .touchUpInside)
        }
        else
        {
            cell.imgRply.image = nil
            cell.cnstAudioHeight.constant = 0
            cell.cnstImgReplyHeight.constant = 120
            let dataType = String(self.CommentData.data[indexPath.row].ReplyImage!.suffix(3))
            print(dataType)
            if dataType == "gif"
            {
                cell.imgRply.image = nil
                cell.imgRply.setGifImage(#imageLiteral(resourceName: "img_RecoveryIcon"))
                // DispatchQueue.global(qos: .background).async {
                   DispatchQueue.main.async {
                do {
                   
                    let data = try? Data(contentsOf: URL(string: self.CommentData.data[indexPath.row].ReplyImage!)!)
                    if data != nil {
                        
                    //    cell.imgRply.image = UIImage(data: data!)
                        
                        let url = URL(string: self.CommentData.data[indexPath.row].ReplyImage!)
                        cell.imgRply.setGifFromURL(url)
                        
//                        let imageData = try Data(contentsOf: URL(string:self.CommentData.data[indexPath.row].ReplyImage!)!)
//                    //    DispatchQueue.global(qos: .background).async {
//
//
//                                var calculatedWidth = CGFloat()
//                                calculatedWidth = (Utilities.getImageWidth(image: cell.imgRply.image!))
//                                var calculatedHeight = CGFloat()
//                                calculatedHeight = (Utilities.getImageHeight(image: cell.imgRply.image!))
//
//                                let updatedwidth = (calculatedWidth * 120)/calculatedHeight
//
//                                //    cell.imgRply.frame = CGRect(x: 0, y: 0, width: updatedwidth, height: 120)
//                                print(updatedwidth)
//                                cell.cnstReplyWidth.constant = updatedwidth
//
//                        cell.imgRply.image = UIImage.sd_animatedGIF(with: imageData)
                    //        }
                    //    }
                    }
                } catch {
                    print("Unable to load data: \(error)")
            //    }
                }
                }
            }
            else
            {
              //  cell.imgRply.image = nil
                
                do {
                    
               //     DispatchQueue.global(qos: .background).async {
                    cell.imgRply.sd_setImage(with: URL(string: self.CommentData.data[indexPath.row].ReplyImage!), placeholderImage: #imageLiteral(resourceName: "img_RecoveryIcon"))
                            DispatchQueue.main.async {
                                let data = try? Data(contentsOf: URL(string: self.CommentData.data[indexPath.row].ReplyImage!)!)
                                if data != nil {
                            //    cell.imgRply.image = UIImage(data: data!)
                                
                                var calculatedWidth = CGFloat()
                                calculatedWidth = (Utilities.getImageWidth(image: cell.imgRply.image!))
                                var calculatedHeight = CGFloat()
                                calculatedHeight = (Utilities.getImageHeight(image: cell.imgRply.image!))
                                
                                let updatedwidth = (calculatedWidth * 120)/calculatedHeight
                                
                              //  cell.imgRply.frame = CGRect(x: 0, y: 0, width: updatedwidth, height: 120)
                                print(updatedwidth)
                                cell.cnstReplyWidth.constant = updatedwidth
                                if cell.cnstReplyWidth.constant > self.view.frame.size.width - 150
                                {
                                    cell.cnstReplyWidth.constant = self.view.frame.size.width - 150
                                }
                                else
                                {
                                    cell.cnstReplyWidth.constant = updatedwidth
                                }
                                    cell.imgRply.sd_setImage(with: URL(string: self.CommentData.data[indexPath.row].ReplyImage!), placeholderImage: #imageLiteral(resourceName: "img_RecoveryIcon"))
                            }
                     //   }
                    }
                } catch {
                    print("Unable to load data: \(error)")
                }
            }
        }
        
            return cell
       
    }
    
}


extension ReplyViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    /*************************
     Method Name:  openGallary()
     Parameter: nil
     return type: nil
     Desc: This function opens the gallery.
     *************************/
    func openGallary()
    {
        picker.allowsEditing = false
        picker.sourceType = UIImagePickerController.SourceType.photoLibrary
        present(picker, animated: true, completion: nil)
        self.cnstGIfHeight.constant = 100
        self.cnstSearchGIfHeight.constant = 0
    }
    
    /*************************
     Method Name:  openCamera()
     Parameter: nil
     return type: nil
     Desc: This function opens the Camera.
     *************************/
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            self.cnstGIfHeight.constant = 100
            present(picker, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
            alert.addAction(ok)
            present(alert, animated: true, completion: nil)
        }
    }
    
    /*************************
     Method Name:  imagePickerControllerDidCancel()
     Parameter: UIImagePickerController
     return type: nil
     Desc: This function is called if user taps cancel button.
     *************************/
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    /*************************
     Method Name:  imagePickerController()
     Parameter: UIImagePickerController, [String : Any]
     return type: nil
     Desc: This function is called when user selects image from gallery.
     *************************/
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        print(chosenImage!)
        self.imgCollection.removeAll()
        self.imgCollection.append(chosenImage!)
        self.imgComment = chosenImage!
        self.collectionGifImg.delegate = self
        self.collectionGifImg.dataSource = self
        self.collectionGifImg.reloadData()
        self.cnstGIfHeight.constant = 100
        dismiss(animated:true, completion: nil)
    }
}


extension ReplyViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgCollection.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectImageCollectionViewCell", for: indexPath) as! SelectImageCollectionViewCell
        
        if self.imgCollection.count > 1
        {
            let dataType = String((imgCollection[indexPath.row]["GIF"] as! String).suffix(3))
            print(dataType)
            if dataType == "gif"
            {
                cell.indicator.isHidden = false
                cell.btnClose.isHidden = true
                do
                {
                    let imageData = try Data(contentsOf: URL(string:imgCollection[indexPath.row]["GIF"] as! String)!)
                     DispatchQueue.global(qos: .background).async {
                    DispatchQueue.main.async {
                        cell.indicator.isHidden = true
                        cell.image.makeCornerRadius(radius: 6.0)
                        cell.image.image = UIImage.sd_animatedGIF(with: imageData)
                    }
                    }
                } catch {
                    print("Unable to load data: \(error)")
                }
            }
        }
        else
        {
            cell.btnClose.isHidden = false
            cell.indicator.isHidden = true
            cell.image.image = self.imgComment
            cell.RemoveCell = {
                collectionView.performBatchUpdates({
                    self.imgCollection.remove(at: indexPath.row);
                    if self.imgCollection.count == 0
                    {
                        
                    }
                    collectionView.deleteItems(at: [indexPath]);
                }, completion: { (bool) in
                    collectionView.reloadItems(at: collectionView.indexPathsForVisibleItems);
                })
            }
            
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.cnstSearchGIfHeight.constant != 0
        {
            self.gifId = imgCollection[indexPath.row]["gifID"] as! Int
            self.cnstGIfHeight.constant = 0
            self.cnstSearchGIfHeight.constant = 0
            self.txtRply.text = ""
            addReply()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width:collectionView.frame.height, height:collectionView.frame.height)
    }
    
}

extension ReplyViewController : ReplyData
{
    func fetchReplyData(Reply: String) {
        self.CommentData.data.removeAll()
        getAllRplyComments()
    }
    
}

extension ReplyViewController: SocketIOManagerDelegateReply {

    func messageReceivedReply(_ data: [String : Any]) {
        
        print(data)
        if self.navigationController?.topViewController is ReplyViewController
        {
            if data["IsReply"] as? Bool == true
            {
                let replyData = data["commentData"] as! [String:Any]
                let commentList = ReplyList()
                commentList.CommentID = replyData["CommentID"] as! Int
                commentList.ReplyID = replyData["ReplyID"] as! Int
                commentList.ReplyImage = replyData["ReplyImage"] as? String
                commentList.ReplyText = replyData["ReplyText"] as? String
                commentList.TimeAgo = replyData["TimeAgo"] as! String
                commentList.UserFirstname = replyData["UserFirstname"] as! String
                commentList.UserLastname = replyData["UserLastname"] as! String
                commentList.ProfileImage = replyData["ProfileImage"] as? String
                commentList.IsLiked = replyData["IsLiked"] as! Bool
                commentList.CanDelete = replyData["CanDelete"] as! Bool
                commentList.ReplyAudio = replyData["ReplyAudio"] as? String
                commentList.Duration = replyData["Duration"] as? String
                
                self.CommentData.data.append(commentList)
                
                self.tblRply.reloadData()
            }
        }
    }
    
//
//    func connectRoom(_ data: [String : Any]) {
//        print(data)
////        self.chatroom = data["chatroom"] as? String ?? ""
////        print(self.chatroom)
//    }

}
