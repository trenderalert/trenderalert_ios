//
//  ReplyListModel.swift
//  TrenderAlert
//
//  Created by Admin on 06/03/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import Foundation
import UIKit

class ReplyListModel: NSObject, Codable {
    var data = [ReplyList]()
    
    enum CodingKeys: String, CodingKey {
        case data
    }
}

class TaggedReplyData: NSObject, Codable {
    var CommentID = Int()
    var ReplyID = Int()
    var UserProfileFirstName = String()
    var UserProfileLastName = String()
    var Username : String?
    var UserProfileID = Int()
    
    enum CodingKeys: String, CodingKey {
        case ReplyID
        case UserProfileFirstName
        case UserProfileLastName
        case CommentID
        case Username
        case UserProfileID
        //  case CommentAudio
    }
}

class ReplyList: NSObject, Codable {
    var CommentID = Int()
    var TaggedUsers = [TaggedReplyData]()
    var ReplyID = Int()
    var ReplyText : String?
    var ReplyImage : String?
    var ProfileImage : String?
    var UserFirstname = String()
    var UserLastname = String()
    var TimeAgo = String()
    var IsLiked = Bool()
    var UserProfileId = Int()
    var CanDelete = Bool()
    var ReplyAudio : String?
    var Duration : String?
    
    enum CodingKeys: String, CodingKey {
        case ReplyID
        case ReplyText
        case TaggedUsers
        case ReplyImage
        case ProfileImage
        case UserFirstname
        case UserLastname
        case TimeAgo
        case CommentID
        case UserProfileId
        case IsLiked
        case CanDelete
        case ReplyAudio
        case Duration
    }
}
