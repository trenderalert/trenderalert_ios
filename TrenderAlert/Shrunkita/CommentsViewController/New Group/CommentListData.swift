//
//  CommentListData.swift
//  TrenderAlert
//
//  Created by Admin on 27/02/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import UIKit

class CommentListData: NSObject, Codable {
    var data = [CommentList]()
    
    enum CodingKeys: String, CodingKey {
        case data
    }
}

class ReplyListData: NSObject, Codable {
    var CommentID = Int()
    var ReplyID = Int()
    var IsLiked = Bool()
    var ReplyText : String?
    var ReplyImage : String?
    var ProfileImage : String?
    var UserFirstname = String()
    var UserLastname = String()
    var TimeAgo = String()
    var CanDelete = Bool()
    var ReplyAudio : String?
    var Duration : String?
    var TaggedUsers = [TaggedUsersData]()
    
    enum CodingKeys: String, CodingKey {
        case ReplyID
        case ReplyText
        case IsLiked
        case ReplyImage
        case ProfileImage
        case UserFirstname
        case UserLastname
        case TimeAgo
        case CommentID
        case CanDelete
        case ReplyAudio
        case Duration
        case TaggedUsers
        //  case CommentAudio
    }
}

class TaggedUsersData: NSObject, Codable {
    var CommentID = Int()
    var ReplyID = Int()
    var UserProfileFirstName = String()
    var UserProfileLastName = String()
    var Username : String?
    var UserProfileID = Int()
    
    enum CodingKeys: String, CodingKey {
        case ReplyID
        case UserProfileFirstName
        case UserProfileLastName
        case CommentID
        case Username
        case UserProfileID
        //  case CommentAudio
    }
}

class CommentList: NSObject, Codable {
    var CommentID = Int()
    var CommentImage : String?
    var Replies = [ReplyListData]()
    var TaggedUsers = [TaggedUsersData]()
    var CommentText : String?
    var TimeAgo = String()
    var UserFirstname = String()
    var UserLastname = String()
    var Username : String?
    var ProfileImage : String?
    var IsLiked = Bool()
    var CanDelete = Bool()
    var CommentAudio : String?
    var Duration : String?
    
    enum CodingKeys: String, CodingKey {
        case CommentID
        case CommentImage
        case CommentText
        case TimeAgo
        case Replies
        case TaggedUsers
        case UserFirstname
        case UserLastname
        case Username
        case ProfileImage
        case IsLiked
        case CanDelete
        case CommentAudio
        case Duration
    }
}
