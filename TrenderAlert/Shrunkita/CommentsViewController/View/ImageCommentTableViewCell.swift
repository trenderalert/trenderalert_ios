//
//  ImageCommentTableViewCell.swift
//  TrenderAlert
//
//  Created by OSP LABS on 10/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ImageCommentTableViewCell: UITableViewCell {

    @IBOutlet weak var viewComment: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
         self.viewComment.makeCornerRadius(radius: 6)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
