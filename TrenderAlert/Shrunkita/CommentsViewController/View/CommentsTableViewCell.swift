//
//  CommentsTableViewCell.swift
//  TrenderAlert
//
//  Created by OSP LABS on 07/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import AVFoundation
import ActiveLabel

class CommentsTableViewCell: UITableViewCell {

    @IBOutlet var cnstAudioHeight: NSLayoutConstraint!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var btnPlayPause: UIButton!
    @IBOutlet var audioSlider: UISlider!
    @IBOutlet var viewAudio: UIView!
    @IBOutlet var imgComment: UIImageView!
    @IBOutlet var cnstCOmmentImgWidth: NSLayoutConstraint!
    @IBOutlet var cnstImgHeight: NSLayoutConstraint!
    @IBOutlet var btnLike: UIButton!
    @IBOutlet var lblExpire: UILabel!
    @IBOutlet var imgProfilePic: UIImageView!
    @IBOutlet weak var viewComment: UIView!
    @IBOutlet var lblComment: ActiveLabel!
    @IBOutlet var btnReply: UIButton!
    var audioPlayer = AVPlayer()
    var recordingSession: AVAudioSession!
    var updateTimerForRecorded = Timer()
    
    override func awakeFromNib() {
        super.awakeFromNib()
//          self.lblComment.padding = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
          self.viewComment.makeCornerRadius(radius: 6)
         self.imgProfilePic.makeCornerRadius(radius: self.imgProfilePic.frame.size.height/2)
        self.btnLike.setTitle(MySingleton.shared.selectedLangData.like, for: .normal)
        self.btnReply.setTitle(MySingleton.shared.selectedLangData.reply, for: .normal)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
//extension UILabel {
//    private struct AssociatedKeys {
//        static var padding = UIEdgeInsets()
//    }
//
//    public var padding: UIEdgeInsets? {
//        get {
//            return objc_getAssociatedObject(self, &AssociatedKeys.padding) as? UIEdgeInsets
//        }
//        set {
//            if let newValue = newValue {
//                objc_setAssociatedObject(self, &AssociatedKeys.padding, newValue as UIEdgeInsets?, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
//            }
//        }
//    }
//
//    override open func draw(_ rect: CGRect) {
//        if let insets = padding {
//            self.drawText(in: rect.inset(by: insets))
//        } else {
//            self.drawText(in: rect)
//        }
//    }
//
//    override open var intrinsicContentSize: CGSize {
//        guard let text = self.text else { return super.intrinsicContentSize }
//
//        var contentSize = super.intrinsicContentSize
//        var textWidth: CGFloat = frame.size.width
//        var insetsHeight: CGFloat = 0.0
//        var insetsWidth: CGFloat = 0.0
//
//        if let insets = padding {
//            insetsWidth += insets.left + insets.right
//            insetsHeight += insets.top + insets.bottom
//            textWidth -= insetsWidth
//        }
//
//        let newSize = text.boundingRect(with: CGSize(width: textWidth, height: CGFloat.greatestFiniteMagnitude),
//                                        options: NSStringDrawingOptions.usesLineFragmentOrigin,
//                                        attributes: [NSAttributedString.Key.font: self.font], context: nil)
//
//        contentSize.height = ceil(newSize.size.height) + insetsHeight
//        contentSize.width = ceil(newSize.size.width) + insetsWidth
//
//        return contentSize
//    }
//}
//
