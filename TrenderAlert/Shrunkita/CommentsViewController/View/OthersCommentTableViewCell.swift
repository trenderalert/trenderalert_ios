//
//  OthersCommentTableViewCell.swift
//  TrenderAlert
//
//  Created by OSP LABS on 08/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import AVFoundation
import ActiveLabel

class OthersCommentTableViewCell: UITableViewCell {

  
    @IBOutlet var cnstReplyAudioHeigth: NSLayoutConstraint!
    @IBOutlet var replyAudioSlider: UISlider!
    @IBOutlet var lblReplyTime: UILabel!
    @IBOutlet var btnReplyAudioPlay: UIButton!
    @IBOutlet var viewReplyAudio: UIView!
    @IBOutlet var cnstCommentAudioHeoght: NSLayoutConstraint!
    @IBOutlet var commentAudioSLider: UISlider!
    @IBOutlet var lblCommentTime: UILabel!
    @IBOutlet var btnComentAudioPlay: UIButton!
    @IBOutlet var viewCommentAudio: UIView!
    @IBOutlet var cnstCommentWidth: NSLayoutConstraint!
    @IBOutlet var cnstReplyWidth: NSLayoutConstraint!
    @IBOutlet var imgRply: UIImageView!
    @IBOutlet var imgComment: UIImageView!
    @IBOutlet var lblExpires: UILabel!
    @IBOutlet var btnRplyImg: UIButton!
    @IBOutlet var cnstOtherReplyHeight: NSLayoutConstraint!
    @IBOutlet var cnstUserCommentHieght: NSLayoutConstraint!
    @IBOutlet var imgUserProfilePic: UIImageView!
    @IBOutlet var imgOtherProfilePic: UIImageView!
    @IBOutlet var lblOtherReply: ActiveLabel!
    @IBOutlet var lblUserComment: ActiveLabel!
    @IBOutlet weak var btnRply: UIButton!
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var viewOtherComment: UIView!
    @IBOutlet weak var viewComment: UIView!
    var longPress = UILongPressGestureRecognizer()
    var audioPlayer = AVPlayer()
    var updateTimerForRecorded = Timer()
    var recordingSession: AVAudioSession!
    override func awakeFromNib() {
        super.awakeFromNib()
         self.viewComment.makeCornerRadius(radius: 6)
         self.viewOtherComment.makeCornerRadius(radius: 6)
         self.imgUserProfilePic.makeCornerRadius(radius: self.imgUserProfilePic.frame.size.height/2)
         self.imgOtherProfilePic.makeCornerRadius(radius: self.imgOtherProfilePic.frame.size.height/2)
        self.btnLike.setTitle(MySingleton.shared.selectedLangData.like, for: .normal)
        self.btnRply.setTitle(MySingleton.shared.selectedLangData.reply, for: .normal)
       // self.lblUserComment.padding = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
  
    
}

//extension UILabel {
//    private struct AssociatedKeys {
//        static var padding = UIEdgeInsets()
//    }
//    
//    public var padding: UIEdgeInsets? {
//        get {
//            return objc_getAssociatedObject(self, &AssociatedKeys.padding) as? UIEdgeInsets
//        }
//        set {
//            if let newValue = newValue {
//                objc_setAssociatedObject(self, &AssociatedKeys.padding, newValue as UIEdgeInsets?, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
//            }
//        }
//    }
//    
//    override open func draw(_ rect: CGRect) {
//        if let insets = padding {
//            self.drawText(in: rect.inset(by: insets))
//        } else {
//            self.drawText(in: rect)
//        }
//    }
//    
//    override open var intrinsicContentSize: CGSize {
//        guard let text = self.text else { return super.intrinsicContentSize }
//        
//        var contentSize = super.intrinsicContentSize
//        var textWidth: CGFloat = frame.size.width
//        var insetsHeight: CGFloat = 0.0
//        var insetsWidth: CGFloat = 0.0
//        
//        if let insets = padding {
//            insetsWidth += insets.left + insets.right
//            insetsHeight += insets.top + insets.bottom
//            textWidth -= insetsWidth
//        }
//        
//        let newSize = text.boundingRect(with: CGSize(width: textWidth, height: CGFloat.greatestFiniteMagnitude),
//                                        options: NSStringDrawingOptions.usesLineFragmentOrigin,
//                                        attributes: [NSAttributedString.Key.font: self.font], context: nil)
//        
//        contentSize.height = ceil(newSize.size.height) + insetsHeight
//        contentSize.width = ceil(newSize.size.width) + insetsWidth
//        
//        return contentSize
//    }
//}
