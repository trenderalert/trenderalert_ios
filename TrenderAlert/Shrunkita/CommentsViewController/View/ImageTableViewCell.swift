//
//  ImageTableViewCell.swift
//  TrenderAlert
//
//  Created by OSP LABS on 10/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class AudioCommentTableViewCell: UITableViewCell {

    @IBOutlet var imgProfilePic: UIImageView!
    @IBOutlet var btnRply: UIButton!
    @IBOutlet var btnLike: UIButton!
    @IBOutlet var lblTimeAgo: UILabel!
    @IBOutlet var imgComment: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
