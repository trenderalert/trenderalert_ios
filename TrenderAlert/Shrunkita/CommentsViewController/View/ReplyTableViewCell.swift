//
//  ReplyTableViewCell.swift
//  TrenderAlert
//
//  Created by Admin on 28/02/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import AVFoundation
import ActiveLabel

class ReplyTableViewCell: UITableViewCell {

    @IBOutlet var cnstAudioHeight: NSLayoutConstraint!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var audioSlider: UISlider!
    @IBOutlet var btnPlayPause: UIButton!
    @IBOutlet var viewAudio: UIView!
    @IBOutlet var btnRply: UIButton!
    @IBOutlet var cnstReplyWidth: NSLayoutConstraint!
    @IBOutlet var imgRply: UIImageView!
    @IBOutlet var cnstImgReplyHeight: NSLayoutConstraint!
    @IBOutlet var btnLike: UIButton!
    @IBOutlet var imgProfilePic: UIImageView!
    @IBOutlet var viewBorder: UIView!
    @IBOutlet var lblCOmmentText: ActiveLabel!
    @IBOutlet var lblExpires: UILabel!
     var longPress = UILongPressGestureRecognizer()
    var audioPlayer = AVPlayer()
     var recordingSession: AVAudioSession!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.btnLike.setTitle(MySingleton.shared.selectedLangData.like, for: .normal)
        self.btnRply.setTitle(MySingleton.shared.selectedLangData.reply, for: .normal)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
