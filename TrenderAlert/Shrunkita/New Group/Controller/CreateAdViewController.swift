//
//  CreateAdViewController.swift
//  TrenderAlert
//
//  Created by Riken Shah on 16/07/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import Photos
import AVFoundation
import CropViewController
import GooglePlaces
import DropDown
import MBProgressHUD
import Alamofire
import MobileCoreServices
import BSImagePicker
import AFViewShaker

protocol createAdDelegate {
    func createAdAction()
}

class CreateAdViewController: UIViewController {

    @IBOutlet weak var lblAddnew: UILabel!
    @IBOutlet weak var lblAddmg: UILabel!
    @IBOutlet weak var viewAddMedia: UIView!
    @IBOutlet weak var viewTitle: FormFields!
    @IBOutlet weak var collectionImages: UICollectionView!
    @IBOutlet weak var viewAdUrl: FormFields!
    @IBOutlet weak var viewBusinessName: FormFields!
    @IBOutlet weak var viewBusinessType: FormFields!
    @IBOutlet weak var viewDuration: FormFields!
    @IBOutlet weak var viewPlacement: FormFields!
    @IBOutlet weak var btnPlus: UIButton!
    
    var photoArray = [PHAsset]()
    let picker = UIImagePickerController()
    var imgCollection = [UIImage]()
    var imgCollectionMediaType = [String]()
    var arraySelectedTagTrendersId = [AnyObject]()
    var media = String()
    private var croppingStyle = CropViewCroppingStyle.default
    private var croppedRect = CGRect.zero
    private var croppedAngle = 0
    var cropIndexPath = IndexPath()
    var arrayOnlyImagesSelected = [UIImage]()
    var videoURL = [URL]()
    var location_arr = [String]()
    var str_latitude = String()
    var str_longitude = String()
    var locationDropdown = DropDown()
    var locationManager = CLLocationManager()
    var advertisementID = Int()
    var delegate : createAdDelegate?
    var zipCode = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetting()
        setupNavigationbar()
        // Do any additional setup after loading the view.
    }
    
    func initialSetting()
    {
        picker.delegate = self
        viewTitle.viewSeperator.isHidden = true
        viewAdUrl.viewSeperator.isHidden = true
        viewBusinessName.viewSeperator.isHidden = true
        viewBusinessType.viewSeperator.isHidden = true
        viewDuration.viewSeperator.isHidden = true
        viewPlacement.viewSeperator.isHidden = true
        viewAddMedia.makeCornerRadius(radius: 12)
        viewDuration.txtFld_UserValue.keyboardType = .numberPad
        viewPlacement.txtFld_UserValue.keyboardType = .default
        self.viewPlacement.txtFld_UserValue.addTarget(self, action: #selector(self.textFieldTextChanged(_:)), for: .editingChanged)
        self.viewPlacement.txtFld_UserValue.delegate = self
        self.viewTitle.txtFld_UserValue.delegate = self
        self.viewBusinessName.txtFld_UserValue.delegate = self
        self.viewAdUrl.txtFld_UserValue.delegate = self
        self.viewBusinessType.txtFld_UserValue.delegate = self
        locationDropdown.anchorView = viewPlacement
        locationDropdown.bottomOffset = CGPoint(x: 0, y:(locationDropdown.anchorView?.plainView.bounds.height)!/2)
        locationDropdown.topOffset = CGPoint(x: 0, y:-(locationDropdown.anchorView?.plainView.bounds.height)!/2)
        dropdownSelection()
        collectionImages.register(UINib(nibName: "SelectImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SelectImageCollectionViewCell")
        locationManager.delegate = self
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse
        {
            locationManager.startUpdatingLocation()
        }
        else
        {
            locationManager.requestWhenInUseAuthorization()
        }
        self.viewTitle.txtFld_UserValue.placeholder = MySingleton.shared.selectedLangData.title
        self.viewAdUrl.txtFld_UserValue.placeholder = MySingleton.shared.selectedLangData.ad_url_will_open_in_external_browser
        self.viewDuration.txtFld_UserValue.placeholder = MySingleton.shared.selectedLangData.Duration_of_ad
        self.viewPlacement.txtFld_UserValue.placeholder = MySingleton.shared.selectedLangData.placement_prefrences
        self.viewBusinessName.txtFld_UserValue.placeholder = MySingleton.shared.selectedLangData.bussines_name
        self.viewBusinessType.txtFld_UserValue.placeholder = MySingleton.shared.selectedLangData.bussines_type
        self.lblAddmg.text = MySingleton.shared.selectedLangData.add_imgs_or_video
        self.lblAddnew.text = MySingleton.shared.selectedLangData.Add_new
    }
    
    func setupNavigationbar(){
        let leftBarButton = UIButton()
        leftBarButton.setTitle("BACK", for: .normal)
        leftBarButton.tintColor = UIColor.white
        leftBarButton.addTarget(self, action: #selector(didTapMenu), for: .touchUpInside)
        let item1 = UIBarButtonItem()
        item1.customView = leftBarButton
        self.navigationItem.leftBarButtonItems = [item1]
        Utilities.setNavigationBarWithTitle(viewController: self, title: MySingleton.shared.selectedLangData.create_ad)
    }
    
    @objc func didTapMenu(_ sender: UIBarButtonItem) {
       self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_AddImagesOrVideo(_ sender: Any) {
        let msg = MySingleton.shared.selectedLangData.please_select_your_option
        
        let alertController = UIAlertController(title: msg, message: "", preferredStyle: UIAlertController.Style.alert)
     
        
        let cameraActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.image, style: .default) { void in
            let imgStatusCount = self.arrayOnlyImagesSelected.count
            if MySingleton.shared.loginObject.IsPremiumUser == "True"{
                if imgStatusCount >= 15
                {
                    DispatchQueue.main.async {
                        TrenderAlertVC.shared.presentAlertController(message: MySingleton.shared.selectedLangData.You_can_select_only_15_images_or_status, completionHandler: nil)
                    }
                }
                else
                {
                    self.OptionForCameraOrGallery(imgStatus: true)
                }
            }
            else{
                if imgStatusCount >= 1
                {
                    DispatchQueue.main.async {
                        TrenderAlertVC.shared.presentAlertController(message: "\(MySingleton.shared.selectedLangData.You_can_select_only_15_images_or_status) \(MySingleton.shared.selectedLangData.To_upload_more_get_premium_package)", completionHandler: {
                            MySingleton.shared.isFirstTimeSideMenu = false
                            let storyboard = UIStoryboard(name: "NotificationSB", bundle: nil)
                            let paymentVC = storyboard.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
                            let tabBar = self.menuContainerViewController.centerViewController as! UITabBarController
                            let navigationController = tabBar.selectedViewController as! UINavigationController
                            navigationController.viewControllers = [paymentVC]
                        })
                    }
                }
                else
                {
                    self.OptionForCameraOrGallery(imgStatus: true)
                }
            }
            
        }
        
        let galleryActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.video, style: .default) { void in
            if self.videoURL.count == 0
            {
                self.OptionForCameraOrGallery(imgStatus: false)
            }else
            {
                DispatchQueue.main.async {
                    TrenderAlertVC.shared.presentAlertController(message: MySingleton.shared.selectedLangData.You_can_select_only_1_video, completionHandler: nil)
                }
            }
        }
        
        let cancelAction = UIAlertAction(title: MySingleton.shared.selectedLangData.cancel, style: UIAlertAction.Style.default, handler: nil)
        
        
        alertController.addAction(cameraActionButton)
        alertController.addAction(galleryActionButton)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)

    }
    
   
    
    func OptionForCameraOrGallery(imgStatus: Bool)
    {
        let msg = MySingleton.shared.selectedLangData.please_select_your_option
        
        let alertController = UIAlertController(title: msg, message: "", preferredStyle: UIAlertController.Style.alert)
        let noAction = UIAlertAction(title: MySingleton.shared.selectedLangData.camera, style: UIAlertAction.Style.default, handler: {(alert: UIAlertAction!) in
            self.openCamera(imgStatus: imgStatus)
            
        })
        let yesAction = UIAlertAction(title: MySingleton.shared.selectedLangData.gallery, style: UIAlertAction.Style.default, handler: {(alert: UIAlertAction!) in
            
            self.openGallary(imgStatus: imgStatus)
        })
        
        let cancelAction = UIAlertAction(title: MySingleton.shared.selectedLangData.cancel, style: UIAlertAction.Style.default, handler: nil)
        
        
        alertController.addAction(noAction)
        alertController.addAction(yesAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func encodeToBase64String(image: UIImage) -> String
    {
        let imageData = image.jpegData(compressionQuality: 0.5)
        let imag_str:String = imageData!.base64EncodedString(options:[])
        return imag_str
    }
    
    //ZipCode
   
    
    @IBAction func btn_CreateAddAction(_ sender: Any) {
        createAd()
    }
    
    func createAd()
    {
        
        if imgCollection.count == 0
        {
            DispatchQueue.main.async {
                TrenderAlertVC.shared.presentAlertController(message: "Please select media!", completionHandler: nil)
            }
            return
        }
        
        var param = ["advertisementID": 0,
                     "url": self.viewAdUrl.txtFld_UserValue.text!,
                     "title": self.viewTitle.txtFld_UserValue.text!,
                    // "mediaType": self.media,
                     "description": "",
                     "adLocation": self.viewPlacement.txtFld_UserValue.text!,
                     "adLatitude": self.str_latitude,
                     "adLongitude": self.str_longitude,
                     "ExpiryDuration" : self.viewDuration.txtFld_UserValue.text!
                     ] as [String : Any]
        
//        "ZipCode": self.zipCode
        
        if arrayOnlyImagesSelected.count != 0
        {
            param["isChangeImage"] = true
            param["isChangeTrendThumbnail"] = false
            param["MediaType"] = "image"
            var imageList = [AnyObject]()
            for i in 0...self.arrayOnlyImagesSelected.count-1
            {
                let imageData = ["FileExt":"jpg","Height": Int(self.arrayOnlyImagesSelected[i].size.height),"Width": Int(self.arrayOnlyImagesSelected[i].size.width),"contenttype":"image/jpg","ImageData":"data:image/jpg;base64,\(self.encodeToBase64String(image: self.arrayOnlyImagesSelected[i]))"] as [String : Any]
                imageList.append(imageData as AnyObject)
            }
            param["ImageList"] = ["Images": imageList]
        }
        
        if self.videoURL.count != 0
        {
            param["isChangeImage"] = false
            param["isChangeTrendThumbnail"] = true
            param["MediaType"] = "VIDEO"
            var imageList = [AnyObject]()
            
            for i in 0...self.imgCollectionMediaType.count-1
            {
                if self.imgCollectionMediaType[i] == "VIDEO"
                {
                    let imageData = ["FileExt":"jpg","Height": Int(self.imgCollection[i].size.height),"Width": Int(self.imgCollection[i].size.width),"contenttype":"image/jpg","ImageData":"data:image/jpg;base64,\(self.encodeToBase64String(image: self.imgCollection[i]))"] as [String : Any]
                    imageList.append(imageData as AnyObject)
                }
            }
            
            param["ThumbnailList"] = ["Images": imageList]
        }
        
        if self.viewAdUrl.txtFld_UserValue.text != ""
        {
            param["isVideoLink"] = true
            param["trendVideo"] = self.viewAdUrl.txtFld_UserValue.text!
        }
        else
        {
            param["isVideoLink"] = false
            param["trendVideo"] = ""
        }
        
        print(param)
        
        let viewArray : [UIView] = [viewTitle,viewAdUrl,viewDuration,viewPlacement]
        let obj_Validation = Validation()
        let viewsToShake = obj_Validation.validationForCreateAd(viewList: viewArray)
        if(viewsToShake.count == 0)
        {
            callApi(requestData: param, url: "\(WebServices.baseURL)api/Admin/AddCustomAdvertisement")
        }
        else
        {
            let viewShaker = AFViewShaker(viewsArray: viewsToShake)
            print(viewsToShake)
            viewShaker?.shake()
        }
      //  print(param)
        
    }
    
    func callApi(requestData: Dictionary<String, Any>, url: String)
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        var urlStr: String = url
        
        urlStr = urlStr.replacingOccurrences(of: " ", with: "&")
        let request :NSMutableURLRequest = NSMutableURLRequest(url:NSURL(string:urlStr)! as URL)
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        if requestData.count != 0
        {
            do
            {
                let jsonData = try JSONSerialization.data(withJSONObject: requestData, options: JSONSerialization.WritingOptions.prettyPrinted)
                request.httpBody = jsonData
            }
            catch let error as NSError
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                }
            }
        }
        let TokenValue = "Bearer " + MySingleton.shared.loginObject.access_token
        print(TokenValue)
        request.setValue(TokenValue, forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        let task = URLSession.shared.dataTask(with: request as URLRequest){ data,response,error in
            
            if error != nil
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error!.localizedDescription, completionHandler: nil)
                }
                
            }
            do
            {
                if (response as? HTTPURLResponse) != nil
                {
                    //////////////////// JSON RESPONSE /////////////////
                    
                    let jsonString = String(data: data!, encoding: .utf8)
                    print(jsonString!)
                    
                    //////////////////// JSON RESPONSE /////////////////
                    
                    if data != nil
                    {
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        
                        print(jsonData)
                        
                        if(jsonData["Status"] as! Bool == true)
                        {
                            
                            DispatchQueue.main.async {
                                
                                if self.videoURL.count != 0
                                {
                                    self.UploadVideo(advertisementID: jsonData["data"] as! String)
                                }
                                else
                                {
                                 //  TrenderAlertVC.shared.presentAlertController(message: "Advertisement has been requested sucessfully. \n Thank you.", completionHandler: nil)
                                    self.delegate?.createAdAction()
                                    self.navigationController?.popViewController(animated: true)
                                    self.imgCollection.removeAll()
                                    self.collectionImages.reloadData()
                                    self.viewTitle.txtFld_UserValue.text = ""
                                    self.viewDuration.txtFld_UserValue.text = ""
                                    self.viewBusinessName.txtFld_UserValue.text = ""
                                    self.viewBusinessType.txtFld_UserValue.text = ""
                                    self.viewAdUrl.txtFld_UserValue.text = ""
                                }
                            }
                            //                            }
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                MBProgressHUD.hide(for: self.view, animated: true)
                                TrenderAlertVC.shared.presentAlertController(message: jsonData["Message"] as! String, completionHandler: nil)
                            }
                        }
                        
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true)
                            TrenderAlertVC.shared.presentAlertController(message: error!.localizedDescription, completionHandler: nil)
                        }
                    }
                    
                }
            }
            catch let error as NSError
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                }
            }
        }
        task.resume()
    }
    
    func UploadVideo(advertisementID : String){
        if self.videoURL.count != 0
        {
            MBProgressHUD.showAdded(to: self.view, animated: true)
            let boundary = "Boundary-\(UUID().uuidString)"
            let TokenValue = "Bearer " + MySingleton.shared.loginObject.access_token
            print(TokenValue)
            let headers: HTTPHeaders = ["Authorization": TokenValue,
                                        "content-type": "multipart/form-data; boundary= \(boundary)",
                "cache-control": "no-cache"
            ]
            
            var request = URLRequest(url: NSURL.init(string: "\(WebServices.baseURL)api/admin/UploadAdVideo/\(advertisementID)")! as URL)
            request.allHTTPHeaderFields = headers
            request.httpMethod = HTTPMethod.post.rawValue
            request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
            // request.httpBody = self.vdata as Data
            
            // print(self.vdata as Data)
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                //                for i in 0...self.videoURL.count-1
                //                {
                multipartFormData.append(self.videoURL[0] , withName: "fileUpload")
                //                }
            }, with: request, encodingCompletion: {
                encodingResult in
                switch encodingResult {
                case .success(let upload, _,_):
                    upload.responseJSON { response in
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true)
                        }
                        debugPrint("SUCCESS RESPONSE: \(response)")
                      //   TrenderAlertVC.shared.presentAlertController(message: "Advertisement has been requested sucessfully. \n Thank you.", completionHandler: nil)
                        self.delegate?.createAdAction()
                        self.navigationController?.popViewController(animated: true)
                    }
                case .failure(let encodingError):
                    // hide progressbas here
                    DispatchQueue.main.async {
                        MBProgressHUD.hide(for: self.view, animated: true)
                        print("ERROR RESPONSE: \(encodingError)")
                        TrenderAlertVC.shared.presentAlertController(message: encodingError.localizedDescription, completionHandler: nil)
                    }
                    
                }
            })
        }
        else
        {
            
        }
    }
    
}

extension CreateAdViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    
    /*************************
     Method Name:  openGallary()
     Parameter: nil
     return type: nil
     Desc: This function opens the gallery.
     *************************/
    func openGallary(imgStatus: Bool)
    {
        if imgStatus
        {
            if MySingleton.shared.loginObject.IsPremiumUser == "True"
            {
                let vc = BSImagePickerViewController()
                let imgStatusCount = self.arrayOnlyImagesSelected.count
                vc.maxNumberOfSelections = 15 - imgStatusCount
                
                bs_presentImagePickerController(vc, animated: true,
                                                select: { (asset: PHAsset) -> Void in
                                                    print("Selected: \(asset)")
                }, deselect: { (asset: PHAsset) -> Void in
                    print("Deselected: \(asset)")
                }, cancel: { (assets: [PHAsset]) -> Void in
                    print("Cancel: \(assets)")
                }, finish: { (assets: [PHAsset]) -> Void in
                    self.photoArray = assets
                    for i in 0...self.photoArray.count-1
                    {
                        let requestOptions = PHImageRequestOptions()
                        requestOptions.resizeMode = PHImageRequestOptionsResizeMode.exact
                        requestOptions.deliveryMode = PHImageRequestOptionsDeliveryMode.highQualityFormat
                        requestOptions.isSynchronous = true
                        PHImageManager.default().requestImage(for: self.photoArray[i], targetSize: PHImageManagerMaximumSize, contentMode: PHImageContentMode.default, options: requestOptions, resultHandler: { (pickedImage, info) in
                            
                            self.imgCollection.append(self.fixOrientation(img: pickedImage!))
                            self.imgCollectionMediaType.append("IMAGE")
                            self.arrayOnlyImagesSelected.append(self.fixOrientation(img: pickedImage!))
                          
                        })
                    }
                    DispatchQueue.main.async {
                        self.collectionImages.delegate = self
                        self.collectionImages.dataSource = self
                        self.collectionImages.reloadData()
                    }
                    print(self.imgCollection.count)
                }, completion: nil)
            }
            else
            {
                picker.allowsEditing = false
                picker.sourceType = UIImagePickerController.SourceType.photoLibrary
                picker.mediaTypes = [kUTTypeImage as String]
                present(picker, animated: true, completion: nil)
            }
        }
        else
        {
            picker.allowsEditing = false
            picker.sourceType = UIImagePickerController.SourceType.savedPhotosAlbum
            picker.mediaTypes = [kUTTypeMovie as String]
            present(picker, animated: true, completion: nil)
        }
    }
    
    /*************************
     Method Name:  openCamera()
     Parameter: nil
     return type: nil
     Desc: This function opens the Camera.
     *************************/
    func openCamera(imgStatus: Bool)
    {
        
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
                if imgStatus
                {
                    picker.allowsEditing = false
                    picker.sourceType = UIImagePickerController.SourceType.camera
                    picker.mediaTypes = [kUTTypeImage as String]
                    picker.cameraCaptureMode = .photo
                    present(picker, animated: true, completion: nil)
                }
                else
                {
                    picker.allowsEditing = false
                    print("video")
                    picker.sourceType = .camera
                    picker.mediaTypes = [kUTTypeMovie as String]
                    picker.cameraCaptureMode = .video
                    present(picker, animated: true, completion: nil)
                }
            }
            else
            {
                let alert = UIAlertController(title: "Camera Not Found", message: "This device has no Camera", preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style:.default, handler: nil)
                alert.addAction(ok)
                present(alert, animated: true, completion: nil)
            }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    
    /*************************
     Method Name:  imagePickerController()
     Parameter: UIImagePickerController, [String : Any]
     return type: nil
     Desc: This function is called when user selects image from gallery.
     *************************/
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
        
        if let chosenImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage
        {
            self.media = "image"
            print(chosenImage)
            imgCollection.append(fixOrientation(img: chosenImage))
            imgCollectionMediaType.append("IMAGE")
            arrayOnlyImagesSelected.append(fixOrientation(img: chosenImage))
            //            self.cnstrntCollectionHeight.constant = 180
            dismiss(animated: true) {
                
                self.collectionImages.delegate = self
                self.collectionImages.dataSource = self
                self.collectionImages.reloadData()
            }
        }
        else{
            if((info[UIImagePickerController.InfoKey.mediaType] as! String) == "public.movie")
            {
                self.media = "video"
                let mediaURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL
                
                let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".mov")
                compressVideo(inputURL: mediaURL as! URL, outputURL: compressedURL) { (exportSession) in
                    guard let session = exportSession else {
                        return
                    }
                    
                    switch session.status {
                    case .unknown:
                        break
                    case .waiting:
                        break
                    case .exporting:
                        break
                    case .completed:
                       
                        self.encodeVideo(videoURLNew: compressedURL as NSURL)
                        
                    case .failed:
                        break
                    case .cancelled:
                        break
                    }
                }
           
            }
        }
    }
    
    func fixOrientation(img:UIImage) -> UIImage {
        if (img.imageOrientation == UIImage.Orientation.up) {
            return img;
        }
        UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale);
        let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
        img.draw(in: rect)
        let normalizedImage : UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext();
        return normalizedImage;
    }

    
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) else {
            handler(nil)
            
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mov
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
    
    func encodeVideo(videoURLNew: NSURL)
    {
        let avAsset = AVURLAsset(url: videoURLNew as URL, options: nil)
        
        let exportSession = AVAssetExportSession(asset: avAsset, presetName: AVAssetExportPresetPassthrough)
        
        let documentsDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let myDocumentPath = NSURL(fileURLWithPath: documentsDirectory).appendingPathComponent("temp.mp4")?.absoluteString
        
        let documentsDirectory2 = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0] as NSURL
        
        let filePath = documentsDirectory2.appendingPathComponent("rendered-Video.mp4")
        deleteFile(filePath: filePath! as NSURL)
        
        if FileManager.default.fileExists(atPath: myDocumentPath!) {
            do {
                try FileManager.default.removeItem(atPath: myDocumentPath!)
            }
            catch let error {
                print(error)
            }
        }
        exportSession?.outputURL = filePath! as URL
        //set the output file format if you want to make it in other file format (ex .3gp)
        exportSession?.outputFileType = AVFileType.mp4
        exportSession?.shouldOptimizeForNetworkUse = true
        
        let start = CMTimeMakeWithSeconds(0.0, preferredTimescale: 0)
        let range = CMTimeRangeMake(start: start, duration: avAsset.duration)
        exportSession?.timeRange = range
        
        exportSession?.exportAsynchronously(completionHandler: {() -> Void in
            switch exportSession?.status {
            case .failed?:
                print("%@",exportSession?.error!)
            case .cancelled?:
                print("Export canceled")
            case .completed?:
                //Video conversion finished
                print("Successful!")
                print(exportSession?.outputURL!)
                self.videoURL.append(((exportSession?.outputURL!)!))
            default:
                break
            }
            do {
                
                let assets = AVURLAsset(url: self.videoURL[self.videoURL.count-1] , options: nil)
                let imgGenerator = AVAssetImageGenerator(asset: assets)
                imgGenerator.appliesPreferredTrackTransform = true
                let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
                // !! check the error before proceeding
                let uiImage = UIImage(cgImage: cgImage)
                
                self.imgCollection.append(uiImage)
                self.imgCollectionMediaType.append("VIDEO")
                DispatchQueue.main.async {
                    self.dismiss(animated: true) {
                        self.collectionImages.delegate = self
                        self.collectionImages.dataSource = self
                        self.collectionImages.reloadData()
                    }
                }
                
            }
            catch let error {
                print(error.localizedDescription)
                DispatchQueue.main.async {
                    self.dismiss(animated:true, completion: nil)
                }
            }
        })
    }
    
    func deleteFile(filePath:NSURL) {
        guard FileManager.default.fileExists(atPath: filePath.path!) else {
            return
        }
        
        do {
            try FileManager.default.removeItem(atPath: filePath.path!)
        }catch{
            fatalError("Unable to delete file: \(error) : \(#function).")
        }
    }
   
}

extension CreateAdViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgCollection.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectImageCollectionViewCell", for: indexPath) as! SelectImageCollectionViewCell
        cell.image.makeCornerRadius(radius: 10.0)
        cell.image.image = imgCollection[indexPath.row]
        cell.indicator.isHidden = true
        cell.RemoveCell = {
            collectionView.performBatchUpdates({
                if self.imgCollectionMediaType[indexPath.row] == "IMAGE"
                {
                    for i in 0...self.arrayOnlyImagesSelected.count-1
                    {
                        if self.imgCollection[indexPath.row] == self.arrayOnlyImagesSelected[i]
                        {
                            self.arrayOnlyImagesSelected.remove(at: i)
                            break
                        }
                    }
                }
                else if self.imgCollectionMediaType[indexPath.row] == "VIDEO"
                {
                    var index = 0
                    for i in 0...indexPath.row
                    {
                        if self.imgCollectionMediaType[i] == "VIDEO"
                        {
                            index = index+1
                        }
                    }
                    self.videoURL.remove(at: index-1)
                }
                self.imgCollection.remove(at: indexPath.row)
                self.imgCollectionMediaType.remove(at: indexPath.row)
                if self.imgCollection.count == 0
                {
                    
                    self.videoURL = [URL]()
                }
                collectionView.deleteItems(at: [indexPath]);
            }, completion: { (bool) in
                collectionView.reloadItems(at: collectionView.indexPathsForVisibleItems);
            })
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:collectionView.frame.height, height:collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if imgCollectionMediaType[indexPath.row] == "IMAGE"
        {
            let cell = collectionView.cellForItem(at: indexPath)as! SelectImageCollectionViewCell
            let cropController = CropViewController(croppingStyle: croppingStyle, image: cell.image.image!)
            cropController.delegate = self
            cropIndexPath = indexPath
            self.present(cropController, animated: true, completion: nil)
        }
    }
    
    func deleteImage(index : Int)
    {
//        let param = [
//            "TrendId": self.editTrendParam.TrendId,
//            "ImageId": self.imageIdArray[index]
//        ]
//        WebServices().callUserService(service: .deleteImage, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse,serviceData) in
//            if serviceResponse["Status"] != nil{
//                if (serviceResponse["Status"] as! Bool == true){
//                    print(serviceResponse["Status"]!)
//                }
//            }
//        })
    }
    
    func deleteVideo()
    {
//        WebServices().callUserService(service: .deleteVideo, urlParameter: "\(self.editTrendParam.TrendId)", parameters: nil, isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse,serviceData) in
//            if serviceResponse["Status"] != nil{
//                if (serviceResponse["Status"] as! Bool == true){
//                    print(serviceResponse["Status"]!)
//                }
//            }
//        })
    }
    
  
}

extension CreateAdViewController: CropViewControllerDelegate
{
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.croppedRect = cropRect
        self.croppedAngle = angle
        //        updateImageViewWithImage(image, fromCropViewController: cropViewController)
        cropViewController.dismiss(animated: true) {
            let cell = self.collectionImages.cellForItem(at: self.cropIndexPath)as! SelectImageCollectionViewCell
            
            for i in 0...self.arrayOnlyImagesSelected.count-1
            {
                if self.imgCollection[self.cropIndexPath.row] == self.arrayOnlyImagesSelected[i]
                {
                    self.arrayOnlyImagesSelected[i] = image
                    break
                }
            }
            cell.image.image = image
            self.imgCollection[self.cropIndexPath.row] = image
        }
    }
}
extension CreateAdViewController: UITextFieldDelegate {
    /*************************
     Method Name:  textFieldTextChanged()
     Parameter: UITextField
     return type: nil
     Desc: This function cis called when text Field Text is Changed.
     *************************/
  
    @objc func textFieldTextChanged(_ sender : UITextField)
    {
        if(viewPlacement.txtFld_UserValue.isEditing)
        {
            if  viewPlacement.txtFld_UserValue.text!.isEmpty
            {
                self.locationDropdown.hide()
            }
            else
            {
                let getAddress = GetAddress()
                self.location_arr.removeAll()
                //self.locationDropdown.dataSource = self.location_arr
                getAddress.getAddress(location_str: sender.text!) { (locationData) in
                    // self.location_arr = locationData
                    for location in locationData
                    {
                        self.location_arr.append(location["address"]!)
                    }
                    print(self.location_arr)
                    self.locationDropdown.dataSource = []
                    self.locationDropdown.dataSource = self.location_arr

                    if !sender.text!.isEmpty
                    {
                        self.locationDropdown.show()
                    }
                }
            }
        }
        else
        {

        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentCharacterCount = textField.text?.count ?? 0
        if range.length + range.location > currentCharacterCount {
            return false
        }
        let newLength = currentCharacterCount + string.count - range.length
        return newLength <= 20
    }
    
    
    func dropdownSelection()
    {
        locationDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.viewPlacement.txtFld_UserValue.text! = item
            GetAddress().getLocationFromAddressString(addressStr: self.viewPlacement.txtFld_UserValue.text!) { (clLocationCoordinate2D,postalCode)  in
                self.str_latitude = String(clLocationCoordinate2D.latitude)
                self.str_longitude = String(clLocationCoordinate2D.longitude)
                self.zipCode = postalCode ?? ""
            }
            
        }
    }
}
extension CreateAdViewController: CLLocationManagerDelegate {
    /*************************
     Method Name:  locationManager()
     Parameter: CLLocationManager, CLAuthorizationStatus
     return type: nil
     Desc: This function cis called when didChangeAuthorization.
     *************************/
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            manager.startUpdatingLocation()
            DispatchQueue.main.async {
                MBProgressHUD.showAdded(to: self.view, animated: true)
            }
            
        }
        else
        {
            //            TrenderAlertVC.shared.presentAlertController(message: "Please go to settings and enable location services.", completionHandler: nil)
        }
    }
    
    /*************************
     Method Name:  locationManager()
     Parameter: CLLocationManager, CLLocation
     return type: nil
     Desc: This function cis called when didUpdateLocations.
     *************************/
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        manager.stopMonitoringSignificantLocationChanges()
        manager.stopUpdatingLocation()
        str_latitude = (String(locations.last!.coordinate.latitude))
        str_longitude = (String(locations.last!.coordinate.longitude))
        GetAddress().getAddressFromLocation(locations.last!) { (address) in
            self.viewPlacement.txtFld_UserValue.text! = address
        }
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: self.view, animated: true)
        }
    }
    
}
