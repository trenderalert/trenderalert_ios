//
//  SortingViewController.swift
//  TrenderAlert
//
//  Created by Riken Shah on 18/09/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

protocol SortViewDelegate {
    func sortParameters(direction: String,title: String)
}

class SortingViewController: UIViewController {

    @IBOutlet weak var txtView: UILabel!
    @IBOutlet weak var txtLike: UILabel!
    @IBOutlet weak var txtStartDate: UILabel!
    @IBOutlet weak var txtDescending: UILabel!
    @IBOutlet weak var txtAscending: UILabel!
    @IBOutlet weak var viewMarker: UIImageView!
    @IBOutlet weak var likeMarker: UIImageView!
    @IBOutlet weak var expiryMarker: UIImageView!
    @IBOutlet weak var imgAscending: UIImageView!
    @IBOutlet weak var imgDescending: UIImageView!
    var delegate : SortViewDelegate?
    var direction = "asc"
    var titleSort = "Start date"
    var isListView = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.direction = "asc"
        setupNavaigationbar()
        setUpSorting()
        // Do any additional setup after loading the view.
    }
    
    func setUpSorting()
    {
        if self.direction == "asc"
        {
            self.imgAscending.image = #imageLiteral(resourceName: "radio_On")
            self.imgDescending.image = #imageLiteral(resourceName: "radio_Off")
        }
        else
        {
            self.imgAscending.image = #imageLiteral(resourceName: "radio_Off")
            self.imgDescending.image = #imageLiteral(resourceName: "radio_On")
        }
        
        if self.titleSort == "Start date"
        {
            self.expiryMarker.image = #imageLiteral(resourceName: "radio_On")
            self.likeMarker.image = #imageLiteral(resourceName: "radio_Off")
            self.viewMarker.image = #imageLiteral(resourceName: "radio_Off")
        }
        else if self.titleSort == "TrendLikesCount"
        {
            self.expiryMarker.image = #imageLiteral(resourceName: "radio_Off")
            self.likeMarker.image = #imageLiteral(resourceName: "radio_On")
            self.viewMarker.image = #imageLiteral(resourceName: "radio_Off")
        }
        else if self.titleSort ==  "TrendViewCount"
        {
            self.expiryMarker.image = #imageLiteral(resourceName: "radio_Off")
            self.likeMarker.image = #imageLiteral(resourceName: "radio_Off")
            self.viewMarker.image = #imageLiteral(resourceName: "radio_On")
        }
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
       self.navigationController?.popViewController(animated: true)
//        if isListView
//        {
            self.delegate?.sortParameters(direction: self.direction, title: self.titleSort)
//        }
    }
    
    func setupNavaigationbar(){
        Utilities.setNavigationBarWithTitle(viewController: self, title: MySingleton.shared.selectedLangData.sorting)
        let leftButton = UIBarButtonItem(image: UIImage(named: "img_BackArrow"), style: .plain, target: self, action: #selector(goBack))
        navigationItem.leftBarButtonItem = leftButton
        
        let rightButton = UIBarButtonItem(title: "Clear All", style: .plain, target: self, action: #selector(clearAllFilter))
        
        navigationItem.rightBarButtonItem = rightButton
        self.txtLike.text = MySingleton.shared.selectedLangData.like
        self.txtView.text = MySingleton.shared.selectedLangData.view
        self.txtStartDate.text = MySingleton.shared.selectedLangData.start_Date
        self.txtAscending.text = MySingleton.shared.selectedLangData.ascending
        self.txtDescending.text = MySingleton.shared.selectedLangData.descending
    }
    
    @objc func clearAllFilter(){
        self.navigationController?.popViewController(animated: true)
//        if isListView
//        {
            self.delegate?.sortParameters(direction: "", title: "")
//        }
    }
    
    @objc func goBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btn_BackAction(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func tap_AscendingView(_ sender: UITapGestureRecognizer) {
        self.imgAscending.image = #imageLiteral(resourceName: "radio_On")
        self.imgDescending.image = #imageLiteral(resourceName: "radio_Off")
        self.direction = "asc"
    }
    
    @IBAction func tap_descendingView(_ sender: Any) {
        self.imgAscending.image = #imageLiteral(resourceName: "radio_Off")
        self.imgDescending.image = #imageLiteral(resourceName: "radio_On")
         self.direction = "desc"
    }
    
    @IBAction func tap_ExpiryView(_ sender: UITapGestureRecognizer) {
        self.expiryMarker.image = #imageLiteral(resourceName: "radio_On")
        self.likeMarker.image = #imageLiteral(resourceName: "radio_Off")
        self.viewMarker.image = #imageLiteral(resourceName: "radio_Off")
        self.titleSort = "Start date"
    }
    
    @IBAction func tap_LikesView(_ sender: UITapGestureRecognizer) {
        self.expiryMarker.image = #imageLiteral(resourceName: "radio_Off")
        self.likeMarker.image = #imageLiteral(resourceName: "radio_On")
        self.viewMarker.image = #imageLiteral(resourceName: "radio_Off")
        self.titleSort = "TrendLikesCount"
    }
    
    @IBAction func tap_ViewsView(_ sender: UITapGestureRecognizer) {
        self.expiryMarker.image = #imageLiteral(resourceName: "radio_Off")
        self.likeMarker.image = #imageLiteral(resourceName: "radio_Off")
        self.viewMarker.image = #imageLiteral(resourceName: "radio_On")
        self.titleSort = "TrendViewCount"
    }
}
