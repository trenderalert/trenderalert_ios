//
//  SwitchUserTableViewCell.swift
//  TrenderAlert
//
//  Created by OSP LABS on 23/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class SwitchUserTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var usernameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        // Initialization code
        
    }

    override func layoutSubviews() {
//        self.userImageView.layer.cornerRadius = 0.5 * self.userImageView.bounds.size.width
//        self.userImageView.clipsToBounds = true
//        userImageView.round()
//        userImageView.layer.cornerRadius = userImageView.bounds.width / 2
////        userImageView.clipsToBounds = true
//        userImageView.layer.masksToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
