//
//  Login.swift
//  TrenderAlert
//
//  Created by OSP LABS on 24/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class Login: NSObject, Codable {
    var Status = String()
    var IsPremiumUser = String()
    var UserID = String()
    var issued = String()
    var expires_in = 0
    var Key = String()
    var IsFirstTime = String()
    var expires = String()
    var access_token = String()
    var IsVerified = String()
    var ProfilePic : String?
    var token_type = String()
    var userName = String()
    var ReceiptDate = String()
    var ReceiptID = String()
    var AspNetUserId = String()
    
    enum CodingKeys: String, CodingKey {
        case Status
        case IsPremiumUser
        case UserID
        case issued = ".issued"
        case expires_in
        case Key
        case IsFirstTime
        case expires = ".expires"
        case access_token
        case IsVerified
        case ProfilePic
        case token_type
        case userName
        case ReceiptDate = "ReceiptDate"
        case ReceiptID = "ReceiptID"
        case AspNetUserId
  
    }
}
