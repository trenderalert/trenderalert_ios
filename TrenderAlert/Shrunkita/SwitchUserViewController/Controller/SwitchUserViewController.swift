//
//  SwitchUserViewController.swift
//  TrenderAlert
//
//  Created by OSP LABS on 23/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import MBProgressHUD
import MFSideMenu


class SwitchUserViewController: UIViewController {

    @IBOutlet weak var switchUserTableView: UITableView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var switchUserTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var btnSignUp: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSettings()
    }
    
    func initialSettings() {
        self.navigationController?.isNavigationBarHidden = true
        self.btnSignUp.makeCornerRadius(radius: 6)
        var usersArray = [Data]()
        if UserDefaults.standard.object(forKey: "usersArray") != nil {
            usersArray = UserDefaults.standard.object(forKey: "usersArray") as! [Data]
        }
//        for userData in usersArray {
//            MySingleton.shared.usersArray.append(WebServices().decodeDataToClass(data: userData, decodeClass: Login.self)!)
//        }
        switchUserTableView.reloadData()
        
        
        if usersArray.count == 0 {
            switchUserTableViewHeightConstraint.constant = 0
        }
        else {
            if CGFloat(50 * usersArray.count) > 310 {
                switchUserTableViewHeightConstraint.constant = 310
            }
            else {
                switchUserTableViewHeightConstraint.constant = CGFloat(60 * usersArray.count)
            }
        }
        
    }

    @IBAction func didTapLoginButton(_ sender: UIButton) {
        self.performSegue(withIdentifier: "SwitchUserToLogin", sender: self)
    }
    
    
    @IBAction func didTapCreateNewAccount(_ sender: UIButton) {
        self.performSegue(withIdentifier: "SwitchUserToSignUp", sender: self)
    }
    
    @objc func didTapCross(button: UIButton) {
//        MySingleton.shared.usersArray.remove(at: button.tag)
        
        var usersArray = [Data]()
        if UserDefaults.standard.object(forKey: "usersArray") != nil {
            
            usersArray = UserDefaults.standard.object(forKey: "usersArray") as! [Data]
            
        }
        usersArray.remove(at: button.tag)
        
        UserDefaults.standard.set(usersArray, forKey: "usersArray")
        UserDefaults.standard.synchronize()
        initialSettings()
    }
    
}

extension SwitchUserViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var usersArray = [Data]()
        if UserDefaults.standard.object(forKey: "usersArray") != nil {
            usersArray = UserDefaults.standard.object(forKey: "usersArray") as! [Data]
        }
        return usersArray.count
//        return MySingleton.shared.usersArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "SwitchUserTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SwitchUserTableViewCell
        
        var usersArray = [Data]()
        if UserDefaults.standard.object(forKey: "usersArray") != nil {
            usersArray = UserDefaults.standard.object(forKey: "usersArray") as! [Data]
        }
        let userObject = WebServices().decodeDataToClass(data: usersArray[indexPath.row], decodeClass: Login.self)
        
//        cell.userImageView.layer.cornerRadius = 0.5 * cell.userImageView.bounds.size.width
//        cell.userImageView.clipsToBounds = true
        cell.usernameLabel.text = userObject!.userName
        cell.userImageView.sd_setImage(with: URL(string: (userObject?.ProfilePic)!), placeholderImage: #imageLiteral(resourceName: "img_articleProfile"))
        cell.moreButton.tag = indexPath.row
        cell.moreButton.addTarget(self, action: #selector(didTapCross), for: .touchUpInside)
        tableView.rowHeight = 50
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerTitleLabel = UILabel()
        headerTitleLabel.textColor = .gray
        headerTitleLabel.text = "Tap to Log In"
        return headerTitleLabel
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            MBProgressHUD.showAdded(to: self.view, animated: true)
        }
        var usersArray = [Data]()
        if UserDefaults.standard.object(forKey: "usersArray") != nil {
            usersArray = UserDefaults.standard.object(forKey: "usersArray") as! [Data]
        }
        let userObject = WebServices().decodeDataToClass(data: usersArray[indexPath.row], decodeClass: Login.self)
        MySingleton.shared.loginObject = userObject!
        UserDefaults.standard.set(("Bearer " + (userObject?.access_token)!), forKey: "access_token")
        
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: self.view, animated: true)
            
            let appDelegatge = UIApplication.shared.delegate as! AppDelegate
            let storyBoard = UIStoryboard(name: "Dashboard", bundle: nil)
            let centerViewController = storyBoard.instantiateViewController(withIdentifier: "TrendAlertTabBarController") as! UITabBarController
            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            let leftViewController = mainStoryBoard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
            appDelegatge.container.leftMenuViewController = leftViewController
            appDelegatge.container.centerViewController = centerViewController
            appDelegatge.container.menuState = MFSideMenuStateClosed
            appDelegatge.container.leftMenuWidth = (centerViewController.view.frame.size.width / 4) * 3
            appDelegatge.window!.rootViewController = appDelegatge.container
            appDelegatge.window!.makeKeyAndVisible()
        }
//        self.performSegue(withIdentifier: "SwitchUserToDashboard", sender: self)
    }
    
}
