//
//  StaticPagesViewController.swift
//  TrenderAlert
//
//  Created by Riken Shah on 19/09/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import MFSideMenu
import WebKit

class StaticPagesViewController: UIViewController {

    @IBOutlet weak var staticWebView: WKWebView!
    var index = Int()
    var titleStatic = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.menuContainerViewController.menuState = MFSideMenuStateClosed
        if self.menuContainerViewController != nil {
            self.menuContainerViewController.panMode = MFSideMenuPanModeNone
        }
        else {
        }
        Utilities.setNavigationBarWithTitle(viewController: self, title: titleStatic)
        let leftBarButton = UIButton()
        leftBarButton.setImage(#imageLiteral(resourceName: "img_MenuBarBtn"), for: .normal)
        leftBarButton.addTarget(self, action: #selector(didTapMenu), for: .touchUpInside)
        let item1 = UIBarButtonItem()
        item1.customView = leftBarButton
        self.navigationItem.leftBarButtonItems = [item1]
        displayStaticPages()
        // Do any additional setup after loading the view.
    }

    @objc func didTapMenu(_ sender: UIBarButtonItem) {
        if self.menuContainerViewController != nil {
            self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
        }
    }
//
//
//
    func displayStaticPages(){
        if index == 3 {
//            let url = Bundle.main.url(forResource: "about-us", withExtension: "html")
//            staticWebView.loadRequest(URLRequest(url: url!))

            let htmlFile = Bundle.main.path(forResource: "about-us", ofType: "html", inDirectory: "trender_alert")
//            Bundle.main.path(forResource: "about-us", ofType: "html")
//            let html = try! String(contentsOfFile: htmlFile!, encoding: String.Encoding.utf8)
            self.staticWebView.load(URLRequest(url: URL(fileURLWithPath: htmlFile!)))


        }else if index == 1{
//            let url = Bundle.main.url(forResource: "terms_and_privacy", withExtension: "html")
//            staticWebView.loadRequest(URLRequest(url: url!))
            let htmlFile =  Bundle.main.path(forResource: "terms_and_privacy", ofType: "html", inDirectory: "trender_alert")
            self.staticWebView.load(URLRequest(url: URL(fileURLWithPath: htmlFile!)))
//            Bundle.main.path(forResource: "terms_and_privacy", ofType: "html")
//            let html = try! String(contentsOfFile: htmlFile!, encoding: String.Encoding.utf8)
//            self.staticWebView.loadHTMLString(html, baseURL: nil)
        }
        else
        {
//             let url = Bundle.main.url(forResource: "contact-us", withExtension: "html")
//          staticWebView.loadRequest(URLRequest(url: url!))
            let htmlFile = Bundle.main.path(forResource: "contact-us", ofType: "html", inDirectory: "trender_alert")
            self.staticWebView.load(URLRequest(url: URL(fileURLWithPath: htmlFile!)))
//            Bundle.main.path(forResource: "contact-us", ofType: "html")
//            let html = try! String(contentsOfFile: htmlFile!, encoding: String.Encoding.utf8)
//            self.staticWebView.loadHTMLString(html, baseURL: nil)
        }

    }
    
   
}
