//
//  AlertTableViewCell.swift
//  TrenderAlert
//
//  Created by Admin on 02/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import TagListView

class AlertTableViewCell: UITableViewCell {

    @IBOutlet weak var viewBorder: UIView!
    @IBOutlet weak var tagHeightConstraint: NSLayoutConstraint!
    @IBOutlet var btnAddTag: UIButton!
    @IBOutlet var btnAddLocation: UIButton!
    @IBOutlet var tagView: TagListView!
    @IBOutlet var locationView: TagListView!
    @IBOutlet weak var locationHeightConstraint: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        viewBorder.layer.shadowColor = UIColor.lightGray.cgColor
        viewBorder.layer.shadowOpacity = 1
        viewBorder.layer.shadowOffset = CGSize.zero
        viewBorder.layer.shadowRadius = 4
        viewBorder.layer.cornerRadius = 5.0
        
        btnAddTag.makeCornerRadius(radius: 4)
        btnAddLocation.makeCornerRadius(radius: 4)
        
        self.btnAddTag.setTitle("    \(MySingleton.shared.selectedLangData.add_tag)    ", for: .normal)
        self.btnAddLocation.setTitle("    \(MySingleton.shared.selectedLangData.add_location)    ", for: .normal)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
