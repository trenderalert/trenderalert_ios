//
//  SetAlertViewController.swift
//  TrenderAlert
//
//  Created by Admin on 02/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import DropDown
import TagListView
import MBProgressHUD

class SetAlertViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet var setAlertTableView: UITableView!
    @IBOutlet var cnstNextBtn: NSLayoutConstraint!
    
    
    // MARK: - Variables
    var selectedRow = -1
    var isLocationTag = Bool()
    var trendAlertTagLocationData = TrendAlertTagLocationData()
    var newTrendCount = 0
    var selectedID = 0
    var addedTag = NSDictionary()
    var addedLocation = NSDictionary()
    

    // MARK: - View Load Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        Utilities.setNavigationBar(viewController: self)
      
        
        if MySingleton.shared.loginObject.IsFirstTime == "True"
        {
            let rightBarButton = UIButton()
            //        rightBarButton.setTitle("+", for: .normal)
            rightBarButton.setImage(UIImage(named: "add_white"), for: .normal)
            rightBarButton.addTarget(self, action: #selector(didTapAdd), for: .touchUpInside)
            let item2 = UIBarButtonItem()
            item2.customView = rightBarButton
            self.navigationItem.rightBarButtonItems = [item2]
        }
        else
        {
            self.cnstNextBtn.constant = 0
            self.updateViewConstraints()
            let leftBarButton = UIButton()
            leftBarButton.setImage(#imageLiteral(resourceName: "img_MenuBarBtn"), for: .normal)
            leftBarButton.addTarget(self, action: #selector(didTapMenu), for: .touchUpInside)
            let item1 = UIBarButtonItem()
            item1.customView = leftBarButton
            self.navigationItem.leftBarButtonItems = [item1]
            
            let rightBarButton = UIButton()
            //        rightBarButton.setTitle("+", for: .normal)
            rightBarButton.setImage(UIImage(named: "add_white"), for: .normal)
            rightBarButton.addTarget(self, action: #selector(didTapAdd), for: .touchUpInside)
            let item2 = UIBarButtonItem()
            item2.customView = rightBarButton
            self.navigationItem.rightBarButtonItems = [item2]
        }
        
        // Testing
        registerTableViewCells()
    }
    
    // MARK: - Add Tag and Location Section
    @objc func didTapAdd(button: UIButton) {
//        if newTrendCount == 1 {
//            TrenderAlertVC.shared.presentAlertController(message: "Please add tag or location", completionHandler: nil)
//            return
//        }
//        newTrendCount = newTrendCount + 1
//        self.setAlertTableView.reloadData()
//        self.setAlertTableView.scrollToRow(at: IndexPath(row: 0, section: 1), at: .bottom, animated: true)
        
        if self.trendAlertTagLocationData.data.count == 5 {
            TrenderAlertVC.shared.presentAlertController(message: "You can not add more than 5 alert preferances.", completionHandler: nil)
            return
        }
        
        /// NEW CHANGE
        isLocationTag = false
        selectedID = 0
        print("selected ID --- \(selectedID)")
        self.performSegue(withIdentifier: "SetAlertToAddTag", sender: nil)
        ///
        
        
    }
    
    
    @IBAction func btn_NextAction(_ sender: Any)
    {
        if self.trendAlertTagLocationData.data.count > 0 {
            MySingleton.shared.loginObject.IsFirstTime = "False"
            self.performSegue(withIdentifier: "SetTrendAlertToWelcome", sender: self)
        }
        else {
            TrenderAlertVC.shared.presentAlertController(message: "Please add alert preference.", completionHandler: nil)
        }
        
    }
    
    
    // MARK: - Menu Action
    @IBAction func didTapMenu(_ sender: UIBarButtonItem) {
        if self.menuContainerViewController != nil {
            self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getAlerts()
        newTrendCount = 0
        self.setAlertTableView.reloadData()
    }
    
    // MARK: - Register Table View Cells
    func registerTableViewCells() {
        setAlertTableView.register(UINib(nibName: "AlertTableViewCell", bundle: nil), forCellReuseIdentifier: "AlertTableViewCell")
    }
    
    
    
    // MARK: - Save Alerts
    func saveAlerts() {
        var params = [String : Any]()
//        if addedTag.allKeys.count > 0 {
//            let tagArray = NSMutableArray()
//            tagArray.add(addedTag)
//            params = ["Id": "\(selectedID)",
//                "Tags": tagArray,
//                "Locations": NSMutableArray()] as [String : Any]
//        }
//        else if addedLocation.allKeys.count > 0 {
//            let locationArray = NSMutableArray()
//            locationArray.add(addedLocation)
//            params = ["Id": "\(selectedID)",
//                "Tags": NSMutableArray(),
//                "Locations": locationArray] as [String : Any]
//        }
        
        
        
        /// NEW CHANGE
        if isLocationTag == false {
//            let tagArray = NSMutableArray()
//            tagArray.add(addedTag)
            params = ["TagLocationMappingId": "\(selectedID)",
                "TagName": addedTag["TagName"]!,
                "TagId": addedTag["TagsId"]!] as [String : Any]
        }
        else if addedLocation.allKeys.count > 0 {
            let locationArray = NSMutableArray()
            locationArray.add(addedLocation)
            params = ["TagLocationMappingId": "\(selectedID)",
                "Location": addedLocation["Location"]!] as [String : Any]
        }
        ///
        print("Set Trend Alerts Parameters --- \(params)")
        
        
//        callApi(requestData: params)
        WebServices().callUserService(service: .setTrendAlerts, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post) { (serviceResponse, responseData) in

            print("Set Trend Alert --- \(serviceResponse)")
            if serviceResponse["Status"] == nil {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                return
            }
            if(serviceResponse["Status"] as! Bool == true)
            {

//                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: {
                    self.getAlerts()
//                })
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }

        }
    }
    
    // MARK: - Get Alerts
    func getAlerts() {
        WebServices().callUserService(service: .getTrendAlerts, urlParameter: "", parameters: [:], isLazyLoading: false, isHeader: true, CallMethod: .get) { (serviceResponse, responseData) in
            print("Get Trend Alert --- \(serviceResponse)")
            if(serviceResponse["Status"] as! Bool == true)
            {
                DispatchQueue.main.async {
                self.trendAlertTagLocationData = WebServices().decodeDataToClass(data: responseData, decodeClass: TrendAlertTagLocationData.self)!
                print("Trend First Tag --- \(self.trendAlertTagLocationData.data.count)")
                self.setAlertTableView.reloadData()
                if self.trendAlertTagLocationData.data.count > 0 {
                    self.setAlertTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
                }
             }
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
            
        }
    }
    
    // MARK: - Delete Alerts
    func deleteAlerts(alertID: String) {
        WebServices().callUserService(service: .deleteTrendAlerts, urlParameter: "/\(alertID)", parameters: [:], isLazyLoading: false, isHeader: true, CallMethod: .post) { (serviceResponse, responseData) in
            print("Delete Trend Alert --- \(serviceResponse)")
            if(serviceResponse["Status"] as! Bool == true)
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: {
                    self.getAlerts()
                })
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
            
        }
    }
    
    
    // MARK: - Configure Table View Cell
    func configTableViewCell(tableView: UITableView, withIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "AlertTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! AlertTableViewCell
        if indexPath.section == 0 {
            // Trend Alerts From Backend
            cell.btnAddTag.tag = indexPath.row
            cell.btnAddLocation.tag = indexPath.row
            cell.tagView.removeAllTags()
            cell.locationView.removeAllTags()
            for tag in self.trendAlertTagLocationData.data[indexPath.row].Tags {
                cell.tagView.addTag(tag.TagName)
                
                for specifiedTag in cell.tagView.tagViews {
                    if specifiedTag.tag == 0 {
                        specifiedTag.tag = tag.TagsId
                        specifiedTag.setTitle("\(tag.UserProfileTagMappingId)", for: .selected)
                        print("Tag ID --- \(tag.TagsId) with Map ID --- \(tag.UserProfileTagMappingId)")
                    }
                }
            }
            
            
            for location in self.trendAlertTagLocationData.data[indexPath.row].Locations {
                cell.locationView.addTag(location.Location)
                
                for specifiedLocation in cell.locationView.tagViews {
                    if specifiedLocation.tag == 0 {
                        specifiedLocation.tag = location.UserProfileTagMappingId
                    }
                }
            }
        }
        else {
            // New Trend Alerts
            cell.tagView.removeAllTags()
            cell.locationView.removeAllTags()
            cell.btnAddTag.tag = -1
            cell.btnAddLocation.tag = -1
        }
        cell.tagView.delegate = self
        cell.locationView.delegate = self
        cell.tagView.tag = indexPath.row
        cell.locationView.tag = indexPath.row
        cell.btnAddTag.addTarget(self, action: #selector(didTapAddTagButton), for: .touchUpInside)
        cell.btnAddLocation.addTarget(self, action: #selector(didTapAddLocationButton), for: .touchUpInside)
        cell.tagView.textFont = UIFont.systemFont(ofSize: 13)
        cell.locationView.textFont = UIFont.systemFont(ofSize: 13)
        if cell.tagView.tagViews.count > 0 {
            cell.tagHeightConstraint.constant = cell.tagView.intrinsicContentSize.height
        }
        if cell.locationView.tagViews.count > 0 {
            cell.locationHeightConstraint.constant = cell.locationView.intrinsicContentSize.height
        }
        
        return cell
    }
    
    

    // MARK: - Add Tag Action
    @objc func didTapAddTagButton(button: UIButton) {
        
        if self.trendAlertTagLocationData.data[button.tag].Tags.count == 3 {
            TrenderAlertVC.shared.presentAlertController(message: MySingleton.shared.selectedLangData.You_can_not_add_more_than_3_tags, completionHandler: nil)
            return
        }
        
        isLocationTag = false
        selectedRow = button.tag
        if selectedRow == -1 {
            selectedID = 0
        }
        else {
            selectedID = self.trendAlertTagLocationData.data[button.tag].Id
        }
        print("selected ID --- \(selectedID)")
        self.performSegue(withIdentifier: "SetAlertToAddTag", sender: nil)
    }
    
    // MARK: - Add Location Action
    @objc func didTapAddLocationButton(button: UIButton) {
        
        if self.trendAlertTagLocationData.data[button.tag].Locations.count == 3 {
            TrenderAlertVC.shared.presentAlertController(message: MySingleton.shared.selectedLangData.You_can_not_add_more_than_3_locations, completionHandler: nil)
            return
        }
        
        isLocationTag = true
        selectedRow = button.tag
        if self.trendAlertTagLocationData.data.count > 0 {
            selectedID = self.trendAlertTagLocationData.data[button.tag].Id
        }
        print("selected ID --- \(selectedID)")
        self.performSegue(withIdentifier: "SetAlertToAddTag", sender: nil)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SetAlertToAddTag" {
            let addTagVC = segue.destination as! AddTagViewController
            addTagVC.addTagDelegate = self
            addTagVC.selectedRow = selectedRow
            addTagVC.isLocationTag = isLocationTag
            addTagVC.selectedID = selectedID
        }
    }
    
    // MARK: - Delete Tags
    func deleteTags(tagID: String, tagLocationMappingID: String) {
        WebServices().callUserService(service: .deleteTrendTags, urlParameter: "", parameters: ["TagLocationMappingId": tagLocationMappingID as AnyObject, "TagId": tagID as AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post) { (serviceResponse, responseData) in
            print("Delete Trend Tags --- \(serviceResponse)")
            if(serviceResponse["Status"] as! Bool == true)
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: {
                    self.getAlerts()
                })
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
            
        }
    }
    
    
    // MARK: - Delete Location
    func deleteLocation(locationText: String, tagLocationMappingID: String) {
        WebServices().callUserService(service: .deleteTrendTags, urlParameter: "", parameters: ["TagLocationMappingId": tagLocationMappingID as AnyObject, "Location": locationText as AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post) { (serviceResponse, responseData) in
            print("Delete Trend Location --- \(serviceResponse)")
            if(serviceResponse["Status"] as! Bool == true)
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: {
                    self.getAlerts()
                })
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
            
        }
    }

    
    
    
    ///////////
    
    func callApi(requestData: Dictionary<String, Any>)
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        //        var urlStr: String = "http://172.16.3.12:96/api/apiAccount/Register"
        var urlStr: String = "\(WebServices.baseURL)api/alert/Save"
        
        
        urlStr = urlStr.replacingOccurrences(of: " ", with: "&")
        let request :NSMutableURLRequest = NSMutableURLRequest(url:NSURL(string:urlStr)! as URL)
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        if requestData.count != 0
        {
            do
            {
                let jsonData = try JSONSerialization.data(withJSONObject: requestData, options: JSONSerialization.WritingOptions.prettyPrinted)
                request.httpBody = jsonData
            }
            catch let error as NSError
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                }
            }
        }
        //        if ((UserDefaults.standard.object(forKey:CONSTANT.token.rawValue)) != nil)
        //        {
        //            let authToken:String = "Bearer " + (UserDefaults.standard.object(forKey:CONSTANT.token.rawValue) as! String)
        //            print(authToken)
        request.setValue("Bearer " + MySingleton.shared.loginObject.access_token, forHTTPHeaderField:"Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        //        }
        
        //        request.setValue(CONSTANT.contentType.rawValue, forHTTPHeaderField: CONSTANT.contentKey.rawValue)
        let task = URLSession.shared.dataTask(with: request as URLRequest){ data,response,error in
            
            if error != nil
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error!.localizedDescription, completionHandler: nil)
                }
                
            }
            do
            {
                if (response as? HTTPURLResponse) != nil
                {
                    
                    //////////////////// JSON RESPONSE /////////////////
                    
                    
                    
                    
                    
                    let jsonString = String(data: data!, encoding: .utf8)
                    print(jsonString!)
                    
                    //////////////////// JSON RESPONSE /////////////////
                    
                    
                    
                    if data != nil
                    {
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        
                        print(jsonData)
                        
                        if(jsonData["Status"] as! Bool == true)
                        {
                            DispatchQueue.main.async {
                                MBProgressHUD.hide(for: self.view, animated: true)
                                TrenderAlertVC.shared.presentAlertController(message: jsonData["Message"] as! String, completionHandler: {
                                    self.getAlerts()
                                })
                            }
                            
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                MBProgressHUD.hide(for: self.view, animated: true)
                                TrenderAlertVC.shared.presentAlertController(message: jsonData["Message"] as! String, completionHandler: nil)
                            }
                        }
                        
                        
                    }
                    else
                    {
                        DispatchQueue.main.async {
                            MBProgressHUD.hide(for: self.view, animated: true)
                            TrenderAlertVC.shared.presentAlertController(message: error!.localizedDescription, completionHandler: nil)
                        }
                    }
                    
                }
            }
            catch let error as NSError
            {
                DispatchQueue.main.async {
                    MBProgressHUD.hide(for: self.view, animated: true)
                    TrenderAlertVC.shared.presentAlertController(message: error.localizedDescription, completionHandler: nil)
                }
            }
        }
        task.resume()
    }
    
}


extension SetAlertViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            // Trend Alerts from Backend
            return self.trendAlertTagLocationData.data.count
        }
        else {
            // New Trend Alert
            return newTrendCount
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return configTableViewCell(tableView: tableView, withIndexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            if indexPath.section == 0 {
                
                let deleteAlert = UIAlertController(title: "", message: "Are you sure you want to delete this alert preferance?", preferredStyle: .alert)
                deleteAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (yesAction) in
                    // Remove Alert
                    self.deleteAlerts(alertID: "\(self.trendAlertTagLocationData.data[indexPath.row].Id)")
                }))
                deleteAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
                self.present(deleteAlert, animated: true, completion: nil)
                
                
            }
        }
    }
}

extension SetAlertViewController: AddTagProtocol,TagListViewDelegate {
    
    func sendTag(tag: NSDictionary, withSelectedRow selectedRow: Int) {
//        let cell = setAlertTableView.cellForRow(at: IndexPath(row: selectedRow, section: 0)) as! AlertTableViewCell
        if isLocationTag == true {
            // Add to Location Tag
            addedLocation = tag
//            cell.locationView.addTag(tag["Location"] as! String)
        }
        else {
            // Add to Tags
            addedTag = tag
//            cell.tagView.addTag(tag["TagName"] as! String)
        }
        self.saveAlerts()
    }
    
    func tagRemoveButtonPressed(_ title: String, tagView: TagView, sender: TagListView) -> Void
    {
        
//        if selectedRow != -1 {
            let cell = setAlertTableView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as! AlertTableViewCell
        
            if sender == cell.locationView {
                
                let deleteAlert = UIAlertController(title: "", message: "Are you sure you want to delete this location?", preferredStyle: .alert)
                deleteAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (yesAction) in
                    // Remove Location Tag
                    self.deleteLocation(locationText: (tagView.titleLabel?.text!)!, tagLocationMappingID: "\(tagView.tag)")
                    //                cell.locationView.removeTagView(tagView)
                }))
                deleteAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
                self.present(deleteAlert, animated: true, completion: nil)
                
            }
            else {
                
                let deleteAlert = UIAlertController(title: "", message: "Are you sure you want to delete this tag?", preferredStyle: .alert)
                deleteAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (yesAction) in
                    // Remove Tags
                    self.deleteTags(tagID: "\(tagView.tag)", tagLocationMappingID: tagView.title(for: .selected)!)
                    //                cell.tagView.removeTagView(tagView)
                }))
                deleteAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
                self.present(deleteAlert, animated: true, completion: nil)
                
                
                
            }
            setAlertTableView.reloadData()
//        }
        
    }
    
}
