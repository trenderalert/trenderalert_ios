//
//  AddTagViewController.swift
//  TrenderAlert
//
//  Created by Admin on 11/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import TagListView
import DropDown

protocol AddTagProtocol {
    func sendTag(tag: NSDictionary, withSelectedRow selectedRow: Int)
}

class AddTagViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet var viewBorder: UIView!
    @IBOutlet var txtTagField: UITextField!
    @IBOutlet weak var tagTitleLabel: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    
    // MARK: - Variables
    var dropDown = DropDown()
    var tagsAllData = [String]()
    var searchedTagsData = [String]()
    var addTagDelegate: AddTagProtocol?
    var selectedRow = -1
    var trendAlertTagsObj = TrendAlertData()
    var selectedTagID = String()
    var selectedTagName = String()
    var isLocationTag = Bool()
    var selectedLocation = String()
    var selectedID = 0
    
    // MARK: - View Load Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialConfig()
    }
    
    // MARK: - Initial Config
    func initialConfig() {
        self.viewBorder.giveBorderColor(color: .lightGray)
        self.navigationController?.isNavigationBarHidden = false
        Utilities.setNavigationBar(viewController: self)
        self.viewBorder.makeCornerRadius(radius: 6)
        self.btnAdd.setTitle(MySingleton.shared.selectedLangData.add, for: .normal)
        self.txtTagField.placeholder = MySingleton.shared.selectedLangData.start_typing
        if isLocationTag == true {
            // Location
            tagTitleLabel.text = "\(MySingleton.shared.selectedLangData.add_location) :"
        }
        else {
            // Tag
            tagTitleLabel.text = "\(MySingleton.shared.selectedLangData.add_tag) :"
        }
        
        
        /// NEW CHANGE
        selectedTagID = "0"
        ///
        
        configDropDown()
    }
    
    // MARK: - Config DropDown
    func configDropDown() {
        dropDown.dataSource = tagsAllData
        dropDown.anchorView = viewBorder
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y:-(dropDown.anchorView?.plainView.bounds.height)!)
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.txtTagField.text! = item
            if self.isLocationTag == false {
                self.selectedTagID = "\(self.trendAlertTagsObj.data[index].TagsId)"
                self.selectedTagName = self.txtTagField.text!
            }
            else {
                self.selectedLocation = self.txtTagField.text!
            }
        }
    }
    
    // MARK: - Tag Button Action
    @IBAction func btn_AddTagAction(_ sender: UIButton) {
         selectedTagName = self.txtTagField.text!
    if self.selectedTagName.isEmpty
    {
        if self.isLocationTag == true
        {
            TrenderAlertVC.shared.presentAlertController(message: MySingleton.shared.selectedLangData.please_enter_valid_location, completionHandler: nil)
        }
        else
        {
            TrenderAlertVC.shared.presentAlertController(message: MySingleton.shared.selectedLangData.please_enter_a_tag, completionHandler: nil)
        }
        
        return
    }
        if self.selectedID != 0 {
            var tagOrLocationToAdd = NSDictionary()
            if self.isLocationTag == true {
                // Location
                tagOrLocationToAdd = ["Location":self.selectedLocation]
            }
            else {
                // Tag
                tagOrLocationToAdd = ["TagsId":self.selectedTagID,
                                      "TagName":self.selectedTagName]
            }
            
            self.addTagDelegate?.sendTag(tag: tagOrLocationToAdd as NSDictionary, withSelectedRow: self.selectedRow)
            self.navigationController?.popViewController(animated: true)
        }
        else {
//            setTags()
            
            /// NEW CHANGE
            // Tag
            let tagOrLocationToAdd = ["TagsId":self.selectedTagID,
                                  "TagName":self.selectedTagName]
            self.addTagDelegate?.sendTag(tag: tagOrLocationToAdd as NSDictionary, withSelectedRow: self.selectedRow)
            self.navigationController?.popViewController(animated: true)
            ///
        }
        
    }
    
    // MARK: - Get Tags
    func getTags(searchedText: String) {
        WebServices().callUserService(service: .getTrendTags, urlParameter: "", parameters: ["tagName": searchedText as AnyObject], isLazyLoading: true, isHeader: true, CallMethod: .get) { (response, responseData) in
            self.trendAlertTagsObj = WebServices().decodeDataToClass(data: responseData, decodeClass: TrendAlertData.self)!
            self.tagsAllData.removeAll()
            for tag in self.trendAlertTagsObj.data {
                self.tagsAllData.append(tag.TagName)
            }
            DispatchQueue.main.async {
          
            self.dropDown.dataSource = self.tagsAllData
            self.dropDown.show()
            }
        }
    }
    
    // MARK: - Set Tags
    func setTags() {
        if self.txtTagField.text!.isEmpty
        {
            if self.isLocationTag == true
            {
                TrenderAlertVC.shared.presentAlertController(message: MySingleton.shared.selectedLangData.please_enter_valid_location, completionHandler: nil)
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: MySingleton.shared.selectedLangData.please_enter_a_tag, completionHandler: nil)
            }
            return
        }
        let params = ["TagsId":"0",
                      "TagName":txtTagField.text!]
        WebServices().callUserService(service: .setTrendTags, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post) { (serviceResponse, responseData) in
            
            
            print("Set Trend Tags --- \(serviceResponse)")
            if(serviceResponse["Status"] as! Bool == true)
            {
                
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: {
                    self.selectedTagID = serviceResponse["data"] as! String
                    self.selectedTagName = self.txtTagField.text!
                    
                    var tagOrLocationToAdd = NSDictionary()
                    if self.isLocationTag == true {
                        // Location
                        tagOrLocationToAdd = ["Location":self.selectedLocation]
//                        tagOrLocationToAdd.setValue(self.selectedLocation, forKey: "Location")
                    }
                    else {
                        // Tag
                        tagOrLocationToAdd = ["TagsId":self.selectedTagID,
                                              "TagName":self.selectedTagName]
//                        tagOrLocationToAdd.setValue(self.selectedTagID, forKey: "TagsId")
//                        tagOrLocationToAdd.setValue(self.selectedTagName, forKey: "TagName")
                    }
                    
                    self.addTagDelegate?.sendTag(tag: tagOrLocationToAdd as NSDictionary, withSelectedRow: self.selectedRow)
                    self.navigationController?.popViewController(animated: true)
                })
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
            
        }
    }
    
    // MARK: - Search Text Field
    var location_arr = [String]()
    @IBAction func didSearch(_ sender: UITextField) {
        print(sender.text!)
        
        
        if isLocationTag == true {
            // Location
            if sender.text!.isEmpty {
                dropDown.hide()
                //            dropDown.dataSource = self.tagsAllData
            }
            else {
                let getAddress = GetAddress()
                getAddress.getAddress(location_str: sender.text!) { (locationData) in
                    // self.location_arr = locationData
                    self.location_arr.removeAll()
                    for location in locationData
                    {
                        self.location_arr.append(location["address"]!)
                    }
                    print(self.location_arr)
                    self.dropDown.dataSource = self.location_arr
                    if !sender.text!.isEmpty
                    {
                        DispatchQueue.main.async {
                        self.dropDown.show()
                        }
                    }
                }
            }
        }
        else {
            // Tag
            if sender.text!.isEmpty {
                dropDown.hide()
                //            dropDown.dataSource = self.tagsAllData
            }
        
            getTags(searchedText: sender.text!)
        }
    }
    
   
    
}
