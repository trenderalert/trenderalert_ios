//
//  ContestWinnerListTVC.swift
//  TrenderAlert
//
//  Created by HPL on 16/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ContestWinnerListTVC: UITableViewCell {

    @IBOutlet weak var lblPrize: UILabel!
    @IBOutlet weak var contestWinnerImage: UIImageView!
    @IBOutlet weak var contestWinnerNameLbl: UILabel!
    @IBOutlet weak var contestNameNTimeLbl: UILabel!
    @IBOutlet weak var prizeAmountLbl: UILabel!
    
    
    
    static let reuseIdentifier = "ContestWinnerListTVC"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblPrize.text = MySingleton.shared.selectedLangData.prize
     //   contestWinnerImage.makeCornerRadius(radius: contestWinnerImage.layer.frame.width/2)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
