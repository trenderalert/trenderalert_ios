//
//  ContestListTVC.swift
//  TrenderAlert
//
//  Created by HPL on 15/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ContestListTVC: UITableViewCell {

    @IBOutlet weak var lblPostedTrend: UILabel!
    @IBOutlet weak var contestImage: UIImageView!
    @IBOutlet weak var contestNameLbl: UILabel!
    @IBOutlet weak var contestVenueLbl: UILabel!
    @IBOutlet weak var trendCountLbl: UILabel!
    
    static let reuseIdentifier = "ContestListTVC"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblPostedTrend.text = MySingleton.shared.selectedLangData.posted_trends
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
