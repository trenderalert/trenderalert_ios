//
//  ContestWinnerListVC.swift
//  TrenderAlert
//
//  Created by HPL on 16/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ContestWinnerListVC: UIViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblEmpty: UILabel!
    @IBOutlet weak var contestWinnerSearchTextField: UITextField!
    @IBOutlet weak var contestWinnerSearchBtn: UIButton!
    @IBOutlet weak var contestWinnerListTbl: UITableView!
    var winnerData = WinnerDataModel()
     var searchText = ""
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblEmpty.isHidden = true
        self.lblEmpty.text = MySingleton.shared.selectedLangData.No_winner_declared_yet
        self.lblTitle.text = MySingleton.shared.selectedLangData.contest_winner_list
        setupTableView()
        setupNaviagationbar()
        
        if UIDevice.current.userInterfaceIdiom == .pad{
            contestWinnerSearchTextField.attributedPlaceholder = NSAttributedString(string: MySingleton.shared.selectedLangData.search_winner, attributes: [NSAttributedString.Key.foregroundColor:UIColor.white,NSAttributedString.Key.font:UIFont(name: "OpenSans-Semibold", size: 22) ?? UIFont.systemFont(ofSize: 22)])
            
        }else{
            contestWinnerSearchTextField.attributedPlaceholder = NSAttributedString(string: MySingleton.shared.selectedLangData.search_winner, attributes: [NSAttributedString.Key.foregroundColor:UIColor.white,NSAttributedString.Key.font:UIFont(name: "OpenSans-Semibold", size: 16) ?? UIFont.systemFont(ofSize: 16)])
        }
        
        
        contestWinnerSearchTextField.leftPadding(paddingSize: 15)
    }

    func setupNaviagationbar(){
        
        let leftBarButton = UIButton()
        leftBarButton.setImage(#imageLiteral(resourceName: "img_MenuBarBtn"), for: .normal)
        leftBarButton.addTarget(self, action: #selector(didTapMenu), for: .touchUpInside)
        let item1 = UIBarButtonItem()
        item1.customView = leftBarButton
        self.navigationItem.leftBarButtonItems = [item1]
        
        //  Utilities.setNavigationBarWithTitle(viewController: self, title: "Monthly Contest")
    }

    @objc func didTapMenu(_ sender: UIBarButtonItem) {
        if self.menuContainerViewController != nil {
            self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
        }
    }

    func setupTableView(){
        contestWinnerListTbl.tableFooterView = UIView()
        contestWinnerListTbl.register(UINib(nibName: ContestWinnerListTVC.reuseIdentifier, bundle: nil), forCellReuseIdentifier: ContestWinnerListTVC.reuseIdentifier)
        self.contestWinnerSearchTextField.delegate = self
        getWinnerList(Skip: 0, Take: 15, lazyLoading: false)
    }

    override func viewDidLayoutSubviews() {
        Utilities.setNavigationBarWithTitle(viewController: self, title: MySingleton.shared.selectedLangData.contest_winners)
    }

    func getWinnerList(Skip: Int, Take: Int, lazyLoading: Bool,SearchString:String = "")
    {
        let params = ["Search":SearchString,
                      "SortColumn":"",
                      "SortDirection":"",
                      "Skip":"0",
                      "Take":""]
        WebServices().callUserService(service: .getContestWinner, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: lazyLoading, isHeader: true, CallMethod: .post) { (serviceResponse, responseData) in
            print("Get All Contest Winner list--- \(serviceResponse)")
            if(serviceResponse["Status"] as! Bool == true)
            {
                DispatchQueue.main.async {
              
                self.winnerData.data.removeAll()
                self.winnerData = WebServices().decodeDataToClass(data: responseData, decodeClass: WinnerDataModel.self)!
                if self.winnerData.data.count == 0
                {
                    if lazyLoading == false
                    {
                        self.lblEmpty.isHidden = false
                    }
                    self.contestWinnerListTbl.delegate = self
                    self.contestWinnerListTbl.dataSource = self
                    self.contestWinnerListTbl.reloadData()
                    return
                }
                self.contestWinnerListTbl.delegate = self
                self.contestWinnerListTbl.dataSource = self
                self.contestWinnerListTbl.reloadData()
                }
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
            
        }
    }
    }

    extension ContestWinnerListVC:UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate{
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.winnerData.data.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: ContestWinnerListTVC.reuseIdentifier, for: indexPath) as! ContestWinnerListTVC
         //   cell.contestWinnerImage.makeCornerRadius(radius: cell.contestWinnerImage.frame.height/2)
            if self.winnerData.data[indexPath.row].UserProfileImage != nil
            {
                cell.contestWinnerImage.sd_setImage(with: URL(string: winnerData.data[indexPath.row].UserProfileImage!), completed: nil)
            }
            else
            {
                cell.contestWinnerImage.image = UIImage(named: "contest_img_1")
            }
            cell.contestWinnerNameLbl.text = "\(winnerData.data[indexPath.row].UserProfileFirstName ?? "") \("\(winnerData.data[indexPath.row].UserProfileLastName ?? "") ")"
            cell.contestNameNTimeLbl.text  = "\(winnerData.data[indexPath.row].ContestTitle ?? "") - \("\(winnerData.data[indexPath.row].ContestClosingDateDisplay) ")"
            cell.prizeAmountLbl.text = "$ \(winnerData.data[indexPath.row].ContestPrize)"
            return cell
        }
        
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            let updatedString = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
            self.winnerData.data = []
            searchText = updatedString!
            getWinnerList(Skip: 0, Take: 15, lazyLoading: true,SearchString: updatedString!)
            return true
        }
    }
