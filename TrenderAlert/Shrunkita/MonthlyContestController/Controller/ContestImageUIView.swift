//
//  ContestImageUIView.swift
//  TrenderAlert
//
//  Created by HPL on 16/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class ContestImageUIView: UIView {

    @IBOutlet weak var contestImage: UIImageView!
    
    static let identifier = "ContestImageUIView"
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
