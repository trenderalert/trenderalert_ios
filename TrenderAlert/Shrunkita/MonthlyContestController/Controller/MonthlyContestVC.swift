//
//  MonthlyContestVC.swift
//  TrenderAlert
//
//  Created by HPL on 15/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class MonthlyContestVC: UIViewController {

    @IBOutlet weak var lblContestList: UILabel!
    @IBOutlet weak var contestListTable: UITableView!
    var contestList = ContestDataModel()
    @IBOutlet weak var lblEmpty: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblEmpty.isHidden = true
        self.lblContestList.text = MySingleton.shared.selectedLangData.contest_list
        self.lblEmpty.text = MySingleton.shared.selectedLangData.no_contests_found
        setupNaviagationbar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         setupTableView()
    }
    
    func setupNaviagationbar(){
        
        let leftBarButton = UIButton()
        leftBarButton.setImage(#imageLiteral(resourceName: "img_MenuBarBtn"), for: .normal)
        leftBarButton.addTarget(self, action: #selector(didTapMenu), for: .touchUpInside)
        let item1 = UIBarButtonItem()
        item1.customView = leftBarButton
        self.navigationItem.leftBarButtonItems = [item1]
        
      //  Utilities.setNavigationBarWithTitle(viewController: self, title: "Monthly Contest")
    }
    
    override func viewDidLayoutSubviews() {
       Utilities.setNavigationBarWithTitle(viewController: self, title: MySingleton.shared.selectedLangData.contest)
    }
    
    @objc func didTapMenu(_ sender: UIBarButtonItem) {
        if self.menuContainerViewController != nil {
            self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
        }
    }
    
    func setupTableView(){

        contestListTable.register(UINib(nibName: ContestListTVC.reuseIdentifier, bundle: nil), forCellReuseIdentifier: ContestListTVC.reuseIdentifier)
        
        getAllCOntestList()
    }
    
    // MARK: Fetch list of monthly contest
    func getAllCOntestList()
    {
        let params = ["SortColumn":"","SortDirection":"","Skip":"0","Take":"0"]
        WebServices().callUserService1(service: .getAllContest, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post) { (serviceResponse, responseData) in
            print("Get All Contest --- \(serviceResponse)")
            if(serviceResponse["Status"] as! Bool == true)
            {
                DispatchQueue.main.async {
                self.contestList.data.removeAll()
                self.contestList = WebServices().decodeDataToClass(data: responseData, decodeClass: ContestDataModel.self)!
                if self.contestList.data.count == 0
                {
                    self.lblEmpty.isHidden = false
                    self.contestListTable.delegate = self
                    self.contestListTable.dataSource = self
                    self.contestListTable.reloadData()
                    return
                }
                self.contestListTable.delegate = self
                self.contestListTable.dataSource = self
                self.contestListTable.reloadData()
                }
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
            
        }
    }
}

extension MonthlyContestVC:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.contestList.data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ContestListTVC.reuseIdentifier, for: indexPath) as! ContestListTVC
        if self.contestList.data[indexPath.row].Images.count != 0
        {
            cell.contestImage.sd_setImage(with: URL(string: self.contestList.data[indexPath.row].Images[0].ImageUrl), completed: nil)
        }
        else
        {
            cell.contestImage.image = UIImage(named: "contest_img_1")
        }
        cell.contestNameLbl.text = self.contestList.data[indexPath.row].Title
        cell.contestVenueLbl.text = self.contestList.data[indexPath.row].MonthlyContestClosingDisplay
        cell.trendCountLbl.text = String(self.contestList.data[indexPath.row].NoOfPostedTrends!)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "MonthlyContestSB", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "ContestTrendsVC") as! ContestTrendsVC
        destination.contestId = self.contestList.data[indexPath.row].Id
        destination.titleNavigation = self.contestList.data[indexPath.row].Title!
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
}
