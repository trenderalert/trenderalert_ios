//
//  ContestDetailVC.swift
//  TrenderAlert
//
//  Created by HPL on 16/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import Stripe

class ContestDetailVC: UIViewController,UIScrollViewDelegate {

    @IBOutlet weak var lblPrizeTitle: UILabel!
    @IBOutlet weak var lblEntryTitle: UILabel!
    @IBOutlet weak var lblPaymentInfo: UILabel!
    @IBOutlet var lblPrize: UILabel!
    @IBOutlet var lblEntryFee: UILabel!
    @IBOutlet var lblDiscussion: UILabel!
    @IBOutlet var lblDeadline: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet weak var contestListImagesScrollView: UIScrollView!
    @IBOutlet weak var contestListImagesPageControl: UIPageControl!
    @IBOutlet weak var rulesNRegulationsTable: UITableView!
    @IBOutlet weak var contestPrizeNFeeView: UIView!
    var contestId = Int()
    var contestDetailData = ContestDetailModel()
    
    var contestRulesNRegulations = [ImagesList]()
//
//     let contestRulesNRegulations = ["is simply dummy text of the printing and types"]
    
//    let timer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(nextImage), userInfo: nil, repeats: true)
//
//    @objc func nextImage(){
//        print("Timer called")
//    }
    
    @IBAction func btn_BackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblPrizeTitle.text = MySingleton.shared.selectedLangData.prize
        self.lblEntryTitle.text = MySingleton.shared.selectedLangData.entry_fee
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupTableview()
        
        contestPrizeNFeeView.layer.shadowOpacity = 0.5
        contestPrizeNFeeView.layer.shadowColor = UIColor.gray.cgColor
        contestPrizeNFeeView.layer.shadowRadius = 4
        contestPrizeNFeeView.layer.shadowOffset = CGSize(width: 2, height: 2)
        contestPrizeNFeeView.layer.shadowOffset = CGSize(width: -2, height: -2)
    }
    
    func setupScrollView(){
        contestListImagesScrollView.delegate = self
        contestListImagesScrollView.isPagingEnabled = true
        contestListImagesScrollView.contentSize.width = self.view.bounds.width * CGFloat(self.contestDetailData.data[0].Images.count)
        contestListImagesScrollView.showsHorizontalScrollIndicator = false
        
    }
    
    override func viewDidLayoutSubviews() {
        Utilities.setNavigationBarWithTitle(viewController: self, title: MySingleton.shared.selectedLangData.contest_details)
    }
    
    func setupTableview(){
//        rulesNRegulationsTable.dataSource = self
//        rulesNRegulationsTable.delegate = self
        rulesNRegulationsTable.register(UINib(nibName: ContestRulesNRegulationTVC.reuseIdentifier, bundle: nil), forCellReuseIdentifier: ContestRulesNRegulationTVC.reuseIdentifier)
        rulesNRegulationsTable.register(UINib(nibName: ContestRulesNRegulationTableSectionHeaderView.reuseIdentifier, bundle: nil), forHeaderFooterViewReuseIdentifier: ContestRulesNRegulationTableSectionHeaderView.reuseIdentifier)
        rulesNRegulationsTable.tableFooterView = UIView()
        getContestDetails()
        
    }
    
    func loadFeature(){
        for (index,image) in self.contestDetailData.data[0].Images.enumerated(){
            if let imageView = Bundle.main.loadNibNamed(ContestImageUIView.identifier, owner: self, options: nil)?.first as? ContestImageUIView{
                imageView.contestImage.sd_setImage(with: URL(string: self.contestDetailData.data[0].Images[index].ImageUrl), completed: nil)
                imageView.contestImage.tag = index
                contestListImagesScrollView.addSubview(imageView)
                imageView.frame.size.width = self.view.bounds.width
                imageView.frame.origin.x = CGFloat(index) * self.view.bounds.width
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let page = scrollView.contentOffset.x / scrollView.bounds.width
        contestListImagesPageControl.currentPage = Int(page)
    }
    
    // MARK: Fetch details monthly contest
    func getContestDetails()
    {
        WebServices().callUserService1(service: .getContestDetails, urlParameter: "?Id=\(self.contestId)", parameters: nil, isLazyLoading: false, isHeader: true, CallMethod: .get) { (serviceResponse, responseData) in
            print("Get Contest details --- \(serviceResponse)")
            if(serviceResponse["Status"] as! Bool == true)
            {
                DispatchQueue.main.async {
                self.contestDetailData.data.removeAll()
                self.contestDetailData = WebServices().decodeDataToClass(data: responseData, decodeClass: ContestDetailModel.self)!
                if self.contestDetailData.data[0].IsTrendPosted == false && self.contestDetailData.data[0].IsPaid == true
                {
                    self.lblPaymentInfo.text = MySingleton.shared.selectedLangData.create_trend.uppercased()
                }
                else
                {
                    self.lblPaymentInfo.text = MySingleton.shared.selectedLangData.entering_contest
                }
                self.lblTitle.text = self.contestDetailData.data[0].Title
                self.lblDiscussion.text = self.contestDetailData.data[0].Description
                self.lblEntryFee.text = "$\(self.contestDetailData.data[0].EntryFee)"
                    self.lblDeadline.text = "\(MySingleton.shared.selectedLangData.entry_deadline):  \(self.contestDetailData.data[0].EntryDeadlineDisplay!)"
                self.lblPrize.text = "\(self.contestDetailData.data[0].ContestPrize)"
                if self.contestDetailData.data[0].RulesRegulation.count != 0
                {
                    self.rulesNRegulationsTable.delegate = self
                    self.rulesNRegulationsTable.dataSource = self
                    self.rulesNRegulationsTable.reloadData()
                }
                if self.contestDetailData.data[0].Images.count != 0
                {
                    self.contestListImagesPageControl.numberOfPages = self.contestDetailData.data[0].Images.count
                    self.setupScrollView()
                    self.loadFeature()
                }
                else
                {
                    
                }
            }
//                self.contestList.data.removeAll()
//                self.contestList = WebServices().decodeDataToClass(data: responseData, decodeClass: ContestDataModel.self)!
//                self.contestListTable.delegate = self
//                self.contestListTable.dataSource = self
//                self.contestListTable.reloadData()
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
            
        }
    }
    
    @IBAction func didTap_StripePayment(_ sender: UITapGestureRecognizer) {
        if self.contestDetailData.data[0].IsPaid == true && self.contestDetailData.data[0].IsTrendPosted == false
        {
            let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
            let destination = storyboard.instantiateViewController(withIdentifier: "SetTrendViewController") as! SetTrendViewController
            destination.contestId = self.contestId
            destination.isForContest = true
            self.navigationController?.pushViewController(destination, animated: true)

        }
         else if self.contestDetailData.data[0].IsPaid == false && self.contestDetailData.data[0].IsTrendPosted == false
            {
                stripe()
            }
        
    }
    
    func stripe()
    {
        let addCardViewController = STPAddCardViewController()
        addCardViewController.delegate = self
        navigationController?.pushViewController(addCardViewController, animated: true)
    }
}

extension ContestDetailVC: STPAddCardViewControllerDelegate {
    
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
        navigationController?.popViewController(animated: true)
    }
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController,
                               didCreateToken token: STPToken,
                               completion: @escaping STPErrorBlock) {
        let entryFee = self.contestDetailData.data[0].EntryFee.replacingOccurrences(of: "$", with: "")
        print("Entry fees\(entryFee)")
        StripeClient.shared.completeCharge(with: token, amount: Double(entryFee)!) { result in
            switch result {
            // 1
            case .success:
                completion(nil)
                print("Payment Token \(token)")
                let alertController = UIAlertController(title: "Congrats",
                                                        message: "Your payment was successful!",
                                                        preferredStyle: .alert)
                let alertAction = UIAlertAction(title: "OK", style: .default, handler: { _ in
                   self.proceedToCreateTrend(token :token)
                  //  self.navigationController?.popViewController(animated: true)
                })
                alertController.addAction(alertAction)
                self.present(alertController, animated: true)
            // 2
            case .failure(let error):
                completion(error)
            }
        }

    }
    
    func proceedToCreateTrend(token :STPToken)
    {
        let params = ["Id":"",
                       "ContestId":self.contestDetailData.data[0].Id,
                       "PaymentResponseKey":token,
                       "IsUserPaid":"true"] as [String : Any]
        WebServices().callUserService1(service: .participationContest, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post) { (serviceResponse, responseData) in
            print("Get Payment details --- \(serviceResponse)")
            if(serviceResponse["Status"] as! Bool == true)
            {
                self.navigationController?.popViewController(animated: true)
//                let storyboard = UIStoryboard(name: "MonthlyContestSB", bundle: nil)
//                let destination = storyboard.instantiateViewController(withIdentifier: "ContestTrendsVC") as! ContestTrendsVC
//                destination.contestId = self.contestDetailData.data[0].Id
//            //    destination.isForContest = true
//                self.navigationController?.pushViewController(destination, animated: true)
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
            
        }
    }
}


extension ContestDetailVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.contestDetailData.data[0].RulesRegulation.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ContestRulesNRegulationTVC.reuseIdentifier, for: indexPath) as! ContestRulesNRegulationTVC
       // cell.rulesNRegulationLbl.text =  "\(indexPath.row+1).\(self.contestDetailData.data[0].RulesRegulation[indexPath.row].RuleRegulation)"
        let attributedString = try? NSAttributedString(data: ("\(self.contestDetailData.data[0].RulesRegulation[indexPath.row].RuleRegulation)").data(using: .unicode) ?? Data(), options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
        cell.rulesNRegulationLbl.attributedText = attributedString
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: ContestRulesNRegulationTableSectionHeaderView.reuseIdentifier) as! ContestRulesNRegulationTableSectionHeaderView
        return header
    }
    
}
