//
//  ContestDetailModel.swift
//  TrenderAlert
//
//  Created by Admin on 30/04/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
class ContestDetailModel: NSObject, Codable {
    var data = [ContestDetail]()
    
    enum CodingKeys: String, CodingKey {
        case data
    }
}

class ContestDetailData: NSObject, Codable {
    
    var Id = Int()
    var Title : String?
    var Description : String?
    var NoOfPostedTrends : Int?
    var EntryFee = String()
    var ContestPrize = String()
    var EntryDeadline = String()
    var MonthlyContestClosing = String()
    var EntryDeadlineDisplay : String?
    var RulesRegulation = [RulesRegulationDetails]()
    var Images = [ImagesList]()
    var IsPaid = Bool()
    var IsTrendPosted = Bool()
    
    enum CodingKeys: String, CodingKey {
        case Id
        case Title
        case Description
        case NoOfPostedTrends
        case EntryFee
        case ContestPrize
        case EntryDeadline
        case MonthlyContestClosing
        case EntryDeadlineDisplay
        case RulesRegulation
        case Images
        case IsPaid
        case IsTrendPosted
    }
}

class RulesRegulationDetails: NSObject, Codable {
    var Id = Int()
    var ContestId = Int()
    var RuleRegulation = String()
    
    enum CodingKeys: String, CodingKey {
        case Id
        case ContestId
        case RuleRegulation
        
    }
}

class ImagesList: NSObject, Codable {
    var Id = Int()
    var ContestId = Int()
    var ImageUrl = String()
    
    enum CodingKeys: String, CodingKey {
        case Id
        case ContestId
        case ImageUrl
        
    }
}

class ContestDetail: NSObject, Codable {
    var Id = Int()
    var Title : String?
    var Description : String?
    var NoOfPostedTrends : Int?
    var EntryFee = String()
    var ContestPrize = String()
    var EntryDeadline = String()
    var MonthlyContestClosing = String()
    var EntryDeadlineDisplay : String?
    var RulesRegulation = [RulesRegulationDetails]()
    var Images = [ImagesList]()
    var IsPaid = Bool()
    var IsTrendPosted = Bool()
    
    enum CodingKeys: String, CodingKey {
        case Id
        case Title
        case Description
        case NoOfPostedTrends
        case EntryFee
        case ContestPrize
        case EntryDeadline
        case MonthlyContestClosing
        case EntryDeadlineDisplay
        case RulesRegulation
         case Images
        case IsPaid
        case IsTrendPosted
    }
}
