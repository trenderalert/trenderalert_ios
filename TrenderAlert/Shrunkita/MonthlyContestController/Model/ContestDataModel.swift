//
//  ContestDataModel.swift
//  TrenderAlert
//
//  Created by Admin on 29/04/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import UIKit

class ContestDataModel: NSObject, Codable {
    var data = [ContestList]()
    
    enum CodingKeys: String, CodingKey {
        case data
    }
}

class ContestListData: NSObject, Codable {

    var Id = Int()
    var Title : String?
    var Description : String?
    var NoOfPostedTrends : Int?
    var EntryFee = String()
    var ContestPrize = String()
    var EntryDeadline = String()
    var MonthlyContestClosing = String()
    var MonthlyContestClosingDisplay : String?
    var RulesRegulation = [RulesRegulationData]()
    var Images = [ImagesData]()
    var IsPaid = Bool()
    var IsTrendPosted = Bool()
    
    enum CodingKeys: String, CodingKey {
        case Id
        case Title
        case Description
        case NoOfPostedTrends
        case EntryFee
        case ContestPrize
        case EntryDeadline
        case MonthlyContestClosing
        case MonthlyContestClosingDisplay
        case RulesRegulation
        case Images
        case IsPaid
        case IsTrendPosted
    }
}

class RulesRegulationData: NSObject, Codable {
    var Id = Int()
    var ContestId = Int()
    var RuleRegulation : String?
    
    enum CodingKeys: String, CodingKey {
        case Id
        case ContestId
        case RuleRegulation
      
    }
}

class ImagesData: NSObject, Codable {
    var Id = Int()
    var ContestId = Int()
    var ImageUrl = String()
    
    enum CodingKeys: String, CodingKey {
        case Id
        case ContestId
        case ImageUrl
        
    }
}

class ContestList: NSObject, Codable {
    var Id = Int()
    var Title : String?
    var Description : String?
    var NoOfPostedTrends : Int?
    var EntryFee = String()
    var ContestPrize = String()
    var EntryDeadline = String()
    var MonthlyContestClosing = String()
    var MonthlyContestClosingDisplay : String?
    var RulesRegulation = [RulesRegulationData]()
    var Images = [ImagesData]()
    var IsPaid = Bool()
    var IsTrendPosted = Bool()
    
    enum CodingKeys: String, CodingKey {
        case Id
        case Title
        case Description
        case NoOfPostedTrends
        case EntryFee
        case ContestPrize
        case EntryDeadline
        case MonthlyContestClosing
        case MonthlyContestClosingDisplay
        case RulesRegulation
        case Images
        case IsPaid
        case IsTrendPosted
    }
}
