//
//  WinnerDataModel.swift
//  TrenderAlert
//
//  Created by Riken Shah on 09/05/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import Foundation
class WinnerDataModel: NSObject, Codable {
    var data = [WinnerData]()
    
    enum CodingKeys: String, CodingKey {
        case data
    }
}

class WinnerData: NSObject, Codable {
 
    var UserProfileId = Int()
    var UserProfileFirstName : String?
    var UserProfileLastName : String?
    var UserProfileImage : String?
    var ContestTitle : String?
    var ContestPrize = Double()
    var ContestClosingDate = String()
    var ContestClosingDateDisplay = String()
    
    enum CodingKeys: String, CodingKey {
        case UserProfileId
        case UserProfileFirstName
        case UserProfileLastName
        case UserProfileImage
        case ContestTitle
        case ContestPrize
        case ContestClosingDate
        case ContestClosingDateDisplay
    }
}
