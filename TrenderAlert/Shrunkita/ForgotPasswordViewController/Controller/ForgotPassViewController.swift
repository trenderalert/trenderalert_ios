//
//  ForgotPassViewController.swift
//  TrenderAlert
//
//  Created by Admin on 04/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import AFViewShaker

class ForgotPassViewController: UIViewController {

    @IBOutlet var txtEmailOrNo: FormFields!
    
    
    var OTPCode = String()
    var userID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtEmailOrNo.txtFld_UserValue.addTarget(self, action: #selector(self.textFieldTextChanged(_:)), for: UIControl.Event.editingChanged)

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func btn_SendMailToUser(_ sender: UIButton)
    {
       ProceedToSendPassword()
    }
    
    func ProceedToSendPassword(){
        let isValid = validation()
        if (isValid == true) {
            let param = ["Username":txtEmailOrNo.txtFld_UserValue.text!]
            WebServices().callUserService(service: UserServices.ForgotPassword, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: false, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
                print(serviceResponse)
                if(serviceResponse["Status"] as! Bool == true)
                {
                    self.OTPCode = ((serviceResponse["Data"] as! NSArray)[0] as! NSDictionary)["OTP"] as! String
                    self.userID = "\((((serviceResponse["Data"] as! NSArray)[0] as! NSDictionary)["UserID"] as! NSNumber))"
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: {
                        self.performSegue(withIdentifier: "ForgotPasswordToVerifyOTP", sender: self)
                    })
                }
                else
                {
                     TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            })
        }
    }
    
    func validation() -> Bool
    {
        let whitespaceSet = CharacterSet.whitespaces
        if(txtEmailOrNo.txtFld_UserValue.text!.isEmpty)
        {
            txtEmailOrNo.txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Email ID or Mobile No.",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            let viewShaker = AFViewShaker(view: self.txtEmailOrNo)
            viewShaker?.shake()
            return false
        }
        else if (txtEmailOrNo.txtFld_UserValue.text?.trimmingCharacters(in: whitespaceSet).isEmpty)!
        {
            txtEmailOrNo.txtFld_UserValue.text = ""
            txtEmailOrNo.txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Email ID or Mobile No.",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            let viewShaker = AFViewShaker(view: self.txtEmailOrNo)
            viewShaker?.shake()
        }
        return true
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ForgotPasswordToVerifyOTP" {
            let verifyOTPVC = segue.destination as! VerifyOTPViewController
            verifyOTPVC.OTPCode = OTPCode
            verifyOTPVC.userID = userID
            MySingleton.shared.isForgotPassword = true
        }
    }
    
}

extension ForgotPassViewController : UITextFieldDelegate
{
    @objc func textFieldTextChanged(_ sender : UITextField)
    {
        sender.text = sender.text?.lowercased()
    }
}
