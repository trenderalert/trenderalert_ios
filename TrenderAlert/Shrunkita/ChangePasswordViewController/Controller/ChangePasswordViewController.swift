//
//  ChangePasswordViewController.swift
//  TrenderAlert
//
//  Created by OSP LABS on 17/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import AFViewShaker

class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var txtConfirmPass: FormFields!
    @IBOutlet weak var txtNewPass: FormFields!
    @IBOutlet weak var txtOldPass: FormFields!
    var userId = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSettings()
    }
    
    func initialSettings() {
        if MySingleton.shared.isForgotPassword == true {
            self.txtOldPass.isHidden = true
        }
        else {
            self.txtOldPass.isHidden = false
        }
    }
    
    @IBAction func btn_SubmitPasswordAction(_ sender: UIButton)
    {
        
            ProceedToSendPassword()
    }
    
    
    

    func ProceedToSendPassword(){
        var viewArray = [UIView]()
         if MySingleton.shared.isForgotPassword == true
         {
            viewArray = [txtNewPass,txtConfirmPass]
        }
        else
         {
         viewArray = [txtOldPass,txtNewPass,txtConfirmPass]
         }
        let obj_Validation = Validation()
        let viewsToShake = obj_Validation.validationForResetPassword(viewList: viewArray)
        if(viewsToShake.count == 0)
        {
            let param = ["UserID":self.userId,"OldPassword":txtOldPass.txtFld_UserValue.text!,"NewPassword":txtNewPass.txtFld_UserValue.text!,"ConfirmPassword":txtConfirmPass.txtFld_UserValue.text!, "isForgot": "true"] as [String : Any]
            
            if(self.txtNewPass.txtFld_UserValue.text != self.txtConfirmPass.txtFld_UserValue.text)
            {
                if(self.txtConfirmPass.txtFld_UserValue.text?.isEmpty)!
                {
                    self.txtConfirmPass.txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Enter Confirm Password",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
                }
                else
                {
                    self.txtConfirmPass.txtFld_UserValue.text! = ""
                    self.txtConfirmPass.txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Passwords do not match",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
                }
                return
            }
            
            WebServices().callUserService(service: UserServices.changePasssword, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
                print(serviceResponse)
                if(serviceResponse["Status"] as! Bool == true)
                {
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: {
                        self.performSegue(withIdentifier: "ChangePasswordToLogin", sender: self)
                    })
                }
                else
                {
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            })
        }
        else
        {
            let viewShaker = AFViewShaker(viewsArray: viewsToShake)
            print(viewsToShake)
            viewShaker?.shake()
        }
    }

}
