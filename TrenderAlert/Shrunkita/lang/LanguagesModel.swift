//
//  LanguagesModel.swift
//  TrenderAlert
//
//  Created by Riken Shah on 01/10/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
import UIKit

//class LanguagesModel: NSObject, Codable {
//    var data = [WordsList]()
//
//    enum CodingKeys: String, CodingKey {
//        case data
//    }
//}

class LanguagesModel: NSObject, Codable {
    var something_went_wrong_please_try_again_later = String()
    var like = String()
    var ago = String()
    var reply = String()
    var write_a_comment = String()
    var Write_a_reply = String()
    var set_username = String()
    var username = String()
    var next = String()
    var settings = String()
    var post_trend = String()
    var about = String()
    var discussion = String()
    var add = String()
    var members = String()
    var friends = String()
    var trender = String()
    var my_trends = String()
    var favorite = String()
    var my_favorite = String()
    var admins = String()
    var what_is_your_links_and_websites = String()
    var interest = String()
    var education = String()
    var male = String()
    var female = String()
    var open = String()
    var close = String()
    var hours = String()
    var days = String()
    var please_enter_valid_email_address = String()
    var password_cannot_be_empty = String()
    var password_cannot_be_less_then_6_characters = String()
    var please_enter_valid_password = String()
    var password_did_not_match = String()
    var first_name_cannot_be_empty = String()
    var last_name_cannot_be_empty = String()
    var birthday_cannot_be_empty = String()
    var location_cannot_be_empty = String()
    var please_enter_valid_location = String()
    var contact_cannot_be_empty = String()
    var please_enter_valid_contact_number = String()
    var please_enter_valid_email_address_mobile_no_username = String()
    var please_enter_valid_email_address_or_mobile_no = String()
    var choose_trend_type = String()
    var permission = String()
    var text = String()
    var audio = String()
    var location = String()
    var tag_trenders = String()
    var Description = String()
    var title = String()
    var check_out_my_latest_trend = String()
    var entering_contest = String()
    var create_trend = String()
    var rules_regulations = String()
    var entry_deadline = String()
    var about_me = String()
    var what_is_your_education = String()
    var what_is_your_interest = String()
    var religion = String()
    var edit = String()
    var gender = String()
    var Public = String()
    var Private = String()
    var folder = String()
    var create_new_folder = String()
    var select_all = String()
    var add_photo = String()
    var take_photo = String()
    var choose_from_library = String()
    var storage_permission_is_required_in_order_to_set_profile_photo = String()
    var please_provide_storage_permission_to_access_images = String()
    var old_password = String()
    var new_password = String()
    var change_password = String()
    var readotp = String()
    var verification = String()
    var enter_the_verification_code = String()
    var resend_verification_code = String()
    var about_cannot_be_empty = String()
    var education_cannot_be_empty = String()
    var interest_cannot_be_empty = String()
    var religion_cannot_be_em = String()
    var link_website_cannot_be_empty = String()
    var logged_in_successfully = String()
    var comment = String()
    var requests = String()
    var request = String()
    var friend_suggestions = String()
    var message = String()
    var unblock = String()
    var tap_to_log_in = String()
    var log_into_another_account = String()
    var create_new_account = String()
    var welcome_to_trender_alert = String()
    var skip = String()
    var reset_password = String()
    var new_password_and_confirm_password_not_match = String()
    var get_premium = String()
    var friend_list = String()
    var friend_request = String()
    var start_typing = String()
    var add_tag = String()
    var groups = String()
    var create_group = String()
    var add_location = String()
    var please_enter_a_tag = String()
    var please_enter_location = String()
    var no = String()
    var are_you_sure_you_want_to_remove_this_trend_alert = String()
    var yes = String()
    var trenders_tagged = String()
    var tag_limit_per_alert_is_3 = String()
    var location_limit_per_alert_is_3 = String()
    var edit_bio = String()
    var add_a_comment = String()
    var trend_alerts = String()
    var status = String()
    var image = String()
    var video = String()
    var are_you_sure_you_want_to_delete = String()
    var please_add_alert_preference = String()
    var take_video = String()
    var please_wait = String()
    var permission_denied = String()
    var session_expired_please_login = String()
    var you_need_to_be_16_to_sign_up = String()
    var can_t_delete_an_empty_trend_alert = String()
    var type_a_status = String()
    var closed = String()
    var secret = String()
    var only_member_can_search_for_group_see_members_and_there_post = String()
    var trend_created_successfully = String()
    var folder_name = String()
    var share = String()
    var share_external = String()
    var unfavorite = String()
    var following = String()
    var turn_off_post_notifications = String()
    var turn_on_post_notifications = String()
    var report = String()
    var save_to_folder = String()
    var delete_trend = String()
    var edit_trend = String()
    var anyone_can_search_the_group_amp_what_they_post = String()
    var expires_in = String()
    var please_add_a_comment = String()
    var please_add_a_reply = String()
    var are_you_sure_you_want_to_delete_comment = String()
    var confirmation = String()
    var are_you_sure_you_want_to_delete_reply = String()
    var update = String()
    var liked = String()
    var all = String()
    var messages = String()
    var notifications = String()
    var no_notifications_found = String()
    var search_gif = String()
    var view = String()
    var are_you_sure_you_want_to_send_this_audio_message = String()
    var cancel = String()
    var sorting = String()
    var ascending = String()
    var descending = String()
    var start_Date = String()
    var have_sent_an_audio_reply = String()
    var block = String()
    var unfriend = String()
    var search_by_name = String()
    var say_something_about_this = String()
    var share_now = String()
    var nudity = String()
    var violence = String()
    var harassment = String()
    var suicide_or_self_injury = String()
    var false_news = String()
    var spam = String()
    var unauthorised_sales = String()
    var hate_speech = String()
    var terrorism = String()
    var incorrect_voting_information = String()
    var reporting_trend = String()
    var use_camera = String()
    var upload_from_gallery = String()
    var remove_photo = String()
    var please_select_folder = String()
    var view_group_info = String()
    var history = String()
    var type_your_message = String()
    var search_name = String()
    var add_friends_to_tag = String()
    var shared = String()
    var s_trend = String()
    var people_who_liked = String()
    var people_who_shared = String()
    var people_who_viewd = String()
    var this_feature_is_for_premium_members_only = String()
    var buy_a_premium_package_if_you_wish_to_add_more_trend_preference = String()
    var closed_group = String()
    var secret_group = String()
    var add_video = String()
    var clear_chat = String()
    var search_people = String()
    var people = String()
    var tags = String()
    var places = String()
    var are_you_sure_you_want_to_block = String()
    var contest_list = String()
    var posted_trends = String()
    var no_contests_found = String()
    var monthly_contest = String()
    var contest = String()
    var prize = String()
    var contest_winner_list = String()
    var contest_winners = String()
    var search_winner = String()
    var no_data_found = String()
    var entry_fee = String()
    var contest_details = String()
    var save_profile = String()
    var quick_tips = String()
    var trend_alert_tag_prefrences_upto_5_tags = String()
    var upload_upto_15_images_status = String()
    var upload_1_video_with_max_size_256mb = String()
    var trend_last_upto_90_days = String()
    var access_to_your_friend_list_and_groups = String()
    var view_profile = String()
    var are_you_sure_you_want_to_clear_this_chat = String()
    var search_tag = String()
    var no_internet_connection = String()
    var search_trend = String()
    var miles_5 = String()
    var miles_20 = String()
    var miles_50 = String()
    var miles_100 = String()
    var trender_travel = String()
    var trend_detail = String()
    var no_trends_found = String()
    var turn_off_to_type_new_location = String()
    var contest_trend_post_list = String()
    var blocklist = String()
    var join_group = String()
    var no_data_available = String()
    var about_this_group = String()
    var member_not_available = String()
    var admin_not_available = String()
    var edit_group_detail = String()
    var when_on_trender_alert = String()
    var get_notification_from_everwhere = String()
    var only_people_you_follow = String()
    var only_people_following_you = String()
    var gets_a_heads_up_notify_when_someone = String()
    var likes_your_trends = String()
    var follow_your_trends = String()
    var comment_on_your_trends = String()
    var send_a_message = String()
    var turn_on = String()
    var text_chat = String()
    var audio_chat = String()
    var save_setting = String()
    var ad_url_will_open_in_external_browser = String()
    var bussines_name = String()
    var bussines_type = String()
    var placement_prefrences = String()
    var pay = String()
    var contest_payment = String()
    var are_you_sure_you_want_to_delete_this_messages = String()
    var active_now = String()
    var online = String()
    var this_message_was_deleted = String()
    var last_seen = String()
    var advertisement_list = String()
    var pending = String()
    var approved = String()
    var rejected = String()
    var business_account_details = String()
    var activity = String()
    var content = String()
    var audience = String()
    var business_account = String()
    var interactions = String()
    var profile_visits = String()
    var discovery = String()
    var reach = String()
    var cities = String()
    var countries = String()
    var Requested = String()
    var follow = String()
    var age = String()
    var men = String()
    var women = String()
    var see_all = String()
    var change_language = String()
    var chinese = String()
    var spanish = String()
    var english = String()
    var hindi = String()
    var arabic = String()
    var portuguese = String()
    var bengali = String()
    var russian = String()
    var japanese = String()
    var german = String()
    var tap_To_stop_recording_audio = String()
    var ok = String()
    var gallery = String()
    var camera = String()
    var please_select_your_option = String()
    var delete = String()
    var Are_you_sure_you_want_to_delete_this_trend = String()
    var pick_username_for_your_account_you_can_always_change_it_later = String()
    var Welcome_to_TrenderAlert = String()
    var welcomeInfo = String()
    var You_can_not_add_more_than_3_locations = String()
    var You_can_not_add_more_than_3_tags = String()
    var edit_profile = String()
    var Add_new = String()
    var hour = String()
    var You_can_select_only_15_images_or_status = String()
    var To_upload_more_get_premium_package = String()
    var You_can_select_only_1_video = String()
    var create_folder = String()
    var save = String()
    var no_friends_found = String()
    var search = String()
    var total_trends = String()
    var Premium_Package = String()
    var Ability_to_enter_monthly_contest = String()
    var Transaction_ID = String()
    var Purchase_Date = String()
    var Payment_Details = String()
    var invite_contact_info = String()
    var invite_friends = String()
    var invite_join = String()
    var Get_Started = String()
    var invite_contacts = String()
    var No_winner_declared_yet = String()
    var No_trend_posted_yet = String()
    var Advertise_List = String()
    var created_on = String()
    var add_imgs_or_video = String()
    var Duration_of_ad = String()
    var no_record_found = String()
    var What_is_discovery = String()
    var discovery_title_info = String()
    var reach_Info = String()
    var What_are_Interactions = String()
    var interaction_title_info = String()
    var trend_views = String()
    var views_info = String()
    var Trend_Interactions = String()
    var Profile_visits_on_your_account = String()
    var top_locations = String()
    var home = String()
    var Go_premium = String()
    var Languages = String()
    var switch_to_business_account = String()
    var create_advertisement = String()
    var create_ad = String()
    var terms_services = String()
    var contact_us = String()
    var about_this_app = String()
    var Likes = String()
    var feed_posts = String()
    var links_and_websites = String()
    var profile = String()
    var read_more = String()
    var read_less = String()
    var trend_setter = String()
    var first_name = String()
    var last_name = String()
    var birth_date = String()
    var mobileNo = String()
    var Unfollow = String()
    var FOLLOW = String()
    var add_Friend = String()
    var remove = String()
    var accept = String()
    var All_Group_Members = String()
    var Unmute_Member = String()
    var mute_member = String()
    var make_admin = String()
    var remove_member = String()
    var Are_you_sure_you_want_to_unmute_this_user = String()
    var Are_you_sure_you_want_to_mute_this_user = String()
    var TrenderAlert = String()
    var Are_you_sure_you_want_to_remove_this_user = String()
    var add_group_members = String()
    var please_select_member = String()
    var send_request = String()
    var Joined = String()
    var Group_Description = String()
    var Group_Name_is_required = String()
    var search_group = String()
    var secret_group_info = String()
    var Please_select_privacy_option = String()
    var Group_details = String()
    var reject = String()
    var accept_reject_request = String()
    var Group_Request_Action = String()
    var leave = String()
    var Are_you_sure_you_want_to_leave_this_group = String()
    var leave_group_info = String()
    var group = String()
    var post = String()
    var logout = String()
    var My_Groups = String()
    var Groups_Suggestion = String()
    var Confirm = String()
    
    enum CodingKeys :String, CodingKey {
        case something_went_wrong_please_try_again_later
        case edit
        case like
        case ago
        case reply
        case write_a_comment
        case Write_a_reply
        case set_username
        case username
        case next
        case settings
        case post_trend
        case about
        case discussion
        case add
        case start_Date
        case members
        case friends
        case trender
        case my_trends
        case favorite
        case my_favorite
        case admins
        case what_is_your_links_and_websites
        case interest
        case education
        case male
        case female
        case ascending
        case descending
        case open
        case close
        case hours
        case days
        case please_enter_valid_email_address
        case password_cannot_be_empty
        case password_cannot_be_less_then_6_characters
        case please_enter_valid_password
        case password_did_not_match
        case first_name_cannot_be_empty
        case last_name_cannot_be_empty
        case birthday_cannot_be_empty
        case location_cannot_be_empty
        case please_enter_valid_location
        case contact_cannot_be_empty
        case turn_on_post_notifications
        case please_enter_valid_contact_number
        case please_enter_valid_email_address_mobile_no_username
        case please_enter_valid_email_address_or_mobile_no
        case choose_trend_type
        case permission
        case text
        case audio
        case location
        case tag_trenders
        case Description
        case title
        case check_out_my_latest_trend
        case entering_contest
        case create_trend
        case rules_regulations
        case entry_deadline
        case about_me
        case what_is_your_education
        case what_is_your_interest
        case religion
        case gender
        case sorting
        case Public
        case Private
        case folder
        case create_new_folder
        case select_all
        case add_photo
        case following
        case Requested
        case follow
        case take_photo
        case choose_from_library
        case storage_permission_is_required_in_order_to_set_profile_photo
        case please_provide_storage_permission_to_access_images
        case old_password
        case new_password
        case change_password
        case readotp
        case verification
        case enter_the_verification_code
        case resend_verification_code
        case about_cannot_be_empty
        case education_cannot_be_empty
        case interest_cannot_be_empty
        case religion_cannot_be_em
        case link_website_cannot_be_empty
        case logged_in_successfully
        case comment
        case requests
        case request
        case friend_suggestions
        case message
        case unblock
        case tap_to_log_in
        case log_into_another_account
        case create_new_account
        case welcome_to_trender_alert
        case skip
        case reset_password
        case new_password_and_confirm_password_not_match
        case get_premium
        case friend_list
        case friend_request
        case start_typing
        case add_tag
        case groups
        case create_group
        case add_location
        case please_enter_a_tag
        case please_enter_location
        case no
        case are_you_sure_you_want_to_remove_this_trend_alert
        case yes
        case trenders_tagged
        case tag_limit_per_alert_is_3
        case location_limit_per_alert_is_3
        case edit_bio
        case add_a_comment
        case trend_alerts
        case status
        case image
        case video
        case are_you_sure_you_want_to_delete
        case please_add_alert_preference
        case take_video
        case please_wait
        case permission_denied
        case session_expired_please_login
        case you_need_to_be_16_to_sign_up
        case can_t_delete_an_empty_trend_alert
        case type_a_status
        case closed
        case secret
        case only_member_can_search_for_group_see_members_and_there_post
        case trend_created_successfully
        case folder_name
        case share
        case share_external
        case unfavorite
        case turn_off_post_notifications
        case report
        case save_to_folder
        case delete_trend
        case edit_trend
        case anyone_can_search_the_group_amp_what_they_post
        case expires_in
        case please_add_a_comment
        case please_add_a_reply
        case are_you_sure_you_want_to_delete_comment
        case confirmation
        case are_you_sure_you_want_to_delete_reply
        case update
        case liked
        case all
        case messages
        case notifications
        case no_notifications_found
        case search_gif
        case are_you_sure_you_want_to_send_this_audio_message
        case cancel
        case have_sent_an_audio_reply
        case block
        case unfriend
        case search_by_name
        case say_something_about_this
        case share_now
        case nudity
        case view
        case violence
        case harassment
        case suicide_or_self_injury
        case false_news
        case spam
        case unauthorised_sales
        case hate_speech
        case terrorism
        case incorrect_voting_information
        case reporting_trend
        case use_camera
        case upload_from_gallery
        case remove_photo
        case please_select_folder
        case view_group_info
        case history
        case type_your_message
        case search_name
        case add_friends_to_tag
        case shared
        case s_trend
        case people_who_liked
        case people_who_shared
        case people_who_viewd
        case this_feature_is_for_premium_members_only
        case buy_a_premium_package_if_you_wish_to_add_more_trend_preference
        case closed_group
        case secret_group
        case add_video
        case clear_chat
        case search_people
        case people
        case tags
        case places
        case are_you_sure_you_want_to_block
        case contest_list
        case posted_trends
        case no_contests_found
        case monthly_contest
        case contest
        case prize
        case contest_winner_list
        case contest_winners
        case search_winner
        case no_data_found
        case entry_fee
        case contest_details
        case save_profile
        case quick_tips
        case trend_alert_tag_prefrences_upto_5_tags
        case upload_upto_15_images_status
        case upload_1_video_with_max_size_256mb
        case trend_last_upto_90_days
        case access_to_your_friend_list_and_groups
        case view_profile
        case are_you_sure_you_want_to_clear_this_chat
        case search_tag
        case no_internet_connection
        case search_trend
        case miles_5
        case miles_20
        case miles_50
        case miles_100
        case trender_travel
        case trend_detail
        case no_trends_found
        case turn_off_to_type_new_location
        case contest_trend_post_list
        case blocklist
        case join_group
        case no_data_available
        case about_this_group
        case member_not_available
        case admin_not_available
        case edit_group_detail
        case when_on_trender_alert
        case get_notification_from_everwhere
        case only_people_you_follow
        case only_people_following_you
        case gets_a_heads_up_notify_when_someone
        case likes_your_trends
        case follow_your_trends
        case comment_on_your_trends
        case send_a_message
        case turn_on
        case text_chat
        case audio_chat
        case save_setting
        case ad_url_will_open_in_external_browser
        case bussines_name
        case bussines_type
        case placement_prefrences
        case pay
        case contest_payment
        case are_you_sure_you_want_to_delete_this_messages
        case active_now
        case online
        case this_message_was_deleted
        case last_seen
        case advertisement_list
        case pending
        case approved
        case rejected
        case business_account_details
        case activity
        case content
        case audience
        case business_account
        case interactions
        case profile_visits
        case discovery
        case reach
        case cities
        case countries
        case age
        case men
        case women
        case see_all
        case change_language
        case chinese
        case spanish
        case english
        case hindi
        case arabic
        case portuguese
        case bengali
        case russian
        case japanese
        case german
        case tap_To_stop_recording_audio
        case ok
        case gallery
        case camera
        case please_select_your_option
        case delete
        case Are_you_sure_you_want_to_delete_this_trend
        case pick_username_for_your_account_you_can_always_change_it_later
        case Welcome_to_TrenderAlert
        case welcomeInfo
        case You_can_not_add_more_than_3_locations
        case You_can_not_add_more_than_3_tags
        case edit_profile
        case Add_new
        case hour
        case You_can_select_only_15_images_or_status
        case To_upload_more_get_premium_package
        case You_can_select_only_1_video
        case create_folder
        case save
        case no_friends_found
        case search
        case total_trends
        case Premium_Package
        case Ability_to_enter_monthly_contest
        case Transaction_ID
        case Purchase_Date
        case Payment_Details
        case invite_contact_info
        case invite_friends
        case invite_join
        case Get_Started
        case invite_contacts
        case No_winner_declared_yet
        case No_trend_posted_yet
        case Advertise_List
        case created_on
        case add_imgs_or_video
        case Duration_of_ad
        case no_record_found
        case What_is_discovery
        case discovery_title_info
        case reach_Info
        case What_are_Interactions
        case interaction_title_info
        case trend_views
        case views_info
        case Trend_Interactions
        case Profile_visits_on_your_account
        case top_locations
        case home
        case Go_premium
        case Languages
        case switch_to_business_account
        case create_advertisement
        case create_ad
        case terms_services
        case contact_us
        case about_this_app
        case Likes
        case feed_posts
        case links_and_websites
        case profile
        case read_more
        case read_less
        case trend_setter
        case first_name
        case last_name
        case birth_date
        case mobileNo
        case Unfollow
        case FOLLOW
        case add_Friend
        case remove
        case accept
        case All_Group_Members
        case Unmute_Member
        case mute_member
        case make_admin
        case remove_member
        case Are_you_sure_you_want_to_unmute_this_user
        case Are_you_sure_you_want_to_mute_this_user
        case TrenderAlert
        case Are_you_sure_you_want_to_remove_this_user
        case add_group_members
        case please_select_member
        case send_request
        case Joined
        case Group_Description
        case Group_Name_is_required
        case search_group
        case secret_group_info
        case Please_select_privacy_option
        case Group_details
        case reject
        case accept_reject_request
        case Group_Request_Action
        case leave
        case Are_you_sure_you_want_to_leave_this_group
        case leave_group_info
        case group
        case post
        case logout
        case My_Groups
        case Groups_Suggestion
        case Confirm
    }
}
