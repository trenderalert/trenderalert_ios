//
//  SetLangViewController.swift
//  TrenderAlert
//
//  Created by Riken Shah on 04/10/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class SetLangViewController: UIViewController {

    @IBOutlet weak var cnstNxtHeight: NSLayoutConstraint!
    @IBOutlet weak var tblLang: UITableView!
    var langListSave = ["english","chinese","spanish","hindi","arabic","portuguese","bengali","russian","japanese","german"]
     var langList = ["English","中文","Española","हिंदी","عربی","Português","বাংলা","русский","日本語","Deutsche"]
    var signUpObject = SignUp()
    var selectedIndex = Int()
    var wordList = LanguagesModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblLang.register(UINib.init(nibName: "LangTableViewCell", bundle: nil), forCellReuseIdentifier: "LangTableViewCell")
        Utilities.setNavigationBarWithTitle(viewController: self, title: "Language")
        if MySingleton.shared.loginObject.IsFirstTime == "True"
        {
           self.cnstNxtHeight.constant = 50
        }
        else
        {
            self.cnstNxtHeight.constant = 0
            self.updateViewConstraints()
            let leftBarButton = UIButton()
            leftBarButton.setImage(#imageLiteral(resourceName: "img_MenuBarBtn"), for: .normal)
            leftBarButton.addTarget(self, action: #selector(didTapMenu), for: .touchUpInside)
            let item1 = UIBarButtonItem()
            item1.customView = leftBarButton
            self.navigationItem.leftBarButtonItems = [item1]
            
        }
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Menu Action
    @IBAction func didTapMenu(_ sender: UIBarButtonItem) {
        if self.menuContainerViewController != nil {
            self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
        }
    }
    
    
    @IBAction func btn_Next(_ sender: Any) {
        self.performSegue(withIdentifier: "selectlangToSetUser", sender: self)
        MySingleton.shared.selectedLang = self.langListSave[selectedIndex]
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "selectlangToSetUser" {
            let setUserVC = segue.destination as! SetUserNameViewController
            setUserVC.signUpObject = signUpObject
        }
    }

}
extension SetLangViewController : UITableViewDelegate, UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.langList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LangTableViewCell", for: indexPath) as! LangTableViewCell
        cell.lblLang.text = self.langList[indexPath.row]
        if  UserDefaults.standard.object(forKey: "SelectedLang") != nil
        {
            if  UserDefaults.standard.object(forKey: "SelectedLang") as? String == self.langListSave[indexPath.row].lowercased()
            {
                cell.accessoryType = .checkmark
            }
            else
            {
                cell.accessoryType = .none
            }
        }
        else
        {
            if indexPath.row == 0
            {
                cell.accessoryType = .checkmark
            }
            else
            {
                cell.accessoryType = .none
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
       for i in 0...self.langList.count-1
       {
        let cell_indexPath = IndexPath(row: i, section: 0)
          if self.tblLang.cellForRow(at: cell_indexPath)?.accessoryType == .checkmark
          {
            self.tblLang.cellForRow(at: cell_indexPath)?.accessoryType = .none
          }
        else
          {
           // self.tblLang.cellForRow(at: cell_indexPath)?.accessoryType = .checkmark
          }
       }
        self.tblLang.cellForRow(at: indexPath)?.accessoryType = .checkmark
        UserDefaults.standard.setValue(self.langListSave[indexPath.row], forKey: "SelectedLang")
      //  MySingleton.shared.selectedLang = self.langList[indexPath.row].lowercased()
        if let path = Bundle.main.path(forResource: UserDefaults.standard.object(forKey: "SelectedLang") as? String , ofType: "json", inDirectory: "trender_alert") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                let jsonData = try JSONSerialization.data(withJSONObject: jsonResult, options: .prettyPrinted)
                print(jsonResult)
                
                self.wordList = WebServices().decodeDataToClass(data: jsonData, decodeClass: LanguagesModel.self)!
                print(self.wordList)
                MySingleton.shared.selectedLangData = self.wordList
                saveLanguage(strLanguage: self.langListSave[indexPath.row])
                
                // do stuff
            } catch {
                // handle error
            }
            if self.menuContainerViewController != nil
            {
                let side_controller = self.menuContainerViewController.leftMenuViewController as! SideMenuViewController
                side_controller.langData()
            }
        }
    }
    
    func saveLanguage(strLanguage: String){
        WebServices().callUserService(service: .saveLanguage, urlParameter: "\(strLanguage)", parameters: nil, isLazyLoading: false, isHeader: true, CallMethod: .post) { (serviceResponse, responseData) in
            
            if serviceResponse["Status"] == nil {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                return
            }
            if(serviceResponse["Status"] as! Bool == true)
            {
                print(serviceResponse)
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
}
