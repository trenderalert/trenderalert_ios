//
//  LangTableViewCell.swift
//  TrenderAlert
//
//  Created by Riken Shah on 04/10/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class LangTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblLang: UILabel!
    @IBOutlet weak var imgCheck: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
