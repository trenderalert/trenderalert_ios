//
//  SetUserNameViewController.swift
//  TrenderAlert
//
//  Created by Admin on 04/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import AFViewShaker

class SetUserNameViewController: UIViewController {

    @IBOutlet weak var lblInstruct: UILabel!
    @IBOutlet weak var lblTitel: UILabel!
    @IBOutlet var txtUserName: FormFields!
    var signUpObject = SignUp()
    
    var userName = String()
    var userID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initialSettings()
    }
    
    @IBAction func btn_SetUserName(_ sender: UIButton)
    {
        ProceedToSendPassword()
    }
    
    func initialSettings() {
        
        self.lblTitel.text = MySingleton.shared.selectedLangData.set_username
        self.lblInstruct.text = MySingleton.shared.selectedLangData.pick_username_for_your_account_you_can_always_change_it_later
        if signUpObject.Data.count > 0 {
            userName = signUpObject.Data[0].Username[0]
            userID = "\(signUpObject.Data[0].UserID)"
        }
        else {
            userName = MySingleton.shared.loginObject.userName
            userID = "\(MySingleton.shared.loginObject.UserID)"
        }
        txtUserName.txtFld_UserValue.text = userName
    }
    
    func ProceedToSendPassword(){
        let isValid = validation()
        if (isValid == true) {
            let param = ["UserName":txtUserName.txtFld_UserValue.text!,"UserID": userID]
            WebServices().callUserService(service: UserServices.setUserName, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
                print(serviceResponse)
                if(serviceResponse["Status"] as! Bool == true)
                {
                    DispatchQueue.main.async {

                    MySingleton.shared.username = self.txtUserName.txtFld_UserValue.text!
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: {
                        self.performSegue(withIdentifier: "SetUsernameToEditBio", sender: self)
                    })
                 }
                }
                else
                {
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            })
        }
    }
    
    func validation() -> Bool
    {
        let whitespaceSet = CharacterSet.whitespaces
        if(txtUserName.txtFld_UserValue.text!.isEmpty)
        {
            txtUserName.txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Please enter user name.",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            let viewShaker = AFViewShaker(view: self.txtUserName)
            viewShaker?.shake()
            return false
        }
        else if (txtUserName.txtFld_UserValue.text?.trimmingCharacters(in: whitespaceSet).isEmpty)!
        {
            txtUserName.txtFld_UserValue.text = ""
            txtUserName.txtFld_UserValue.attributedPlaceholder = NSMutableAttributedString(string: "Please enter user name.",attributes: [NSAttributedString.Key.foregroundColor: UIColor.red])
            let viewShaker = AFViewShaker(view: self.txtUserName)
            viewShaker?.shake()
        }
        return true
    }
   
}
