//
//  DiscoveryDataModel.swift
//  TrenderAlert
//
//  Created by Riken Shah on 21/08/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
class DiscoveryDataModel: NSObject, Codable {
    var data = [DiscoveryList]()
    
    enum CodingKeys: String, CodingKey {
        case data
    }
}

class DiscoveryList: NSObject, Codable {
    var value = Int()
    var unit : String?
    
    enum CodingKeys: String, CodingKey {
        case value
        case unit
    }
}
