//
//  InteractionDataModel.swift
//  TrenderAlert
//
//  Created by Riken Shah on 20/08/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation

class InteractionDataModel: NSObject, Codable {
    var data = [GraphList]()
    
    enum CodingKeys: String, CodingKey {
        case data
    }
}

class GraphList: NSObject, Codable {
    var value = Int()
    var unit : String?
    
    enum CodingKeys: String, CodingKey {
        case value
        case unit
    }
}
