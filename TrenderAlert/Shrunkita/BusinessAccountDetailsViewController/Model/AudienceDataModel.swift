//
//  AudienceDataModel.swift
//  TrenderAlert
//
//  Created by Riken Shah on 26/08/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import Foundation
class AudienceDataModel: NSObject, Codable {
    var data = AudienceList()
    
    enum CodingKeys: String, CodingKey {
        case data
    }
}

class AudienceList: NSObject, Codable {
    
    var Location = LocationList()
    var Age = AgeList()
    
    enum CodingKeys: String, CodingKey {
        case Location
        case Age
    }
}

class LocationList: NSObject, Codable {
    
    var City : [CityList]?
    var Country : [CountryList]?
    
    enum CodingKeys: String, CodingKey {
        case City
        case Country
    }
}

class AgeList: NSObject, Codable {
    
    var ALL = [AllAgeList]()
    var MEN = [MenList]()
    var WOMEN = [WomenList]()
    
    enum CodingKeys: String, CodingKey {
        case ALL
        case MEN
        case WOMEN
    }
}

class CityList: NSObject, Codable {
    
    var value = Int()
    var unit : String?
    
    enum CodingKeys: String, CodingKey {
        case value
        case unit
    }
}

class CountryList: NSObject, Codable {
    
    var value = Int()
    var unit : String?
    
    enum CodingKeys: String, CodingKey {
        case value
        case unit
    }
}

class AllAgeList: NSObject, Codable {
    
    var value = Int()
    var unit : String?
    
    enum CodingKeys: String, CodingKey {
        case value
        case unit
    }
}

class WomenList: NSObject, Codable {
    
    var value = Int()
    var unit : String?
    
    enum CodingKeys: String, CodingKey {
        case value
        case unit
    }
}
class MenList: NSObject, Codable {
    
    var value = Int()
    var unit : String?
    
    enum CodingKeys: String, CodingKey {
        case value
        case unit
    }
}
