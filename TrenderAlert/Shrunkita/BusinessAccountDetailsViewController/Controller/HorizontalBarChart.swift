//
//  HorizontalBarChart.swift
//  BarChart
//
//  Created by HPL on 23/01/19.
//  Copyright © 2019 Nguyen Vu Nhat Minh. All rights reserved.
//

import UIKit

class HorizontalBarChart: UIView {
    
    /// the width of each bar
//    let barWidth: CGFloat = 40.0
    
//    let barHeight: CGFloat = 40.0
    var barHeight: CGFloat = 30.0
    
    /// space between each bar
    let space: CGFloat = 20.0
    
    let spaceForLeftTitle:CGFloat = (UIDevice.current.userInterfaceIdiom == .pad) ? 100 : 70
    /// space at the bottom of the bar to show the title
    private let bottomSpace: CGFloat = 40.0
    
    private let leftSpace: CGFloat = 40
    
    private let rightSpace: CGFloat = 40
    
    /// space at the top of each bar to show the value
    private let topSpace: CGFloat = 40.0
    
    var titleColor = UIColor.black
    var textColor = UIColor(displayP3Red: 104/255, green: 134/255, blue: 234/255, alpha: 1)
    
    let fontSize = (UIDevice.current.userInterfaceIdiom == .pad) ? 18 : 13
    let fontName = "OpenSans"
    
    
    /// contain all layers of the chart
    private let mainLayer: CALayer = CALayer()
    
    /// contain mainLayer to support scrolling
    private let scrollView: UIScrollView = UIScrollView()
    
    var dataEntries: [GraphList]? = nil {
        didSet {
            mainLayer.sublayers?.forEach({$0.removeFromSuperlayer()})
            
            if let dataEntries = dataEntries {
                scrollView.contentSize = CGSize(width: self.frame.size.width, height: (barHeight + space)*CGFloat(dataEntries.count))
                mainLayer.frame = CGRect(x: 0, y: 0, width: scrollView.contentSize.width, height: scrollView.contentSize.height)
                var number = [Int]()
                
                for i in 0..<dataEntries.count {
                    number.append(dataEntries[i].value)
                }
                print(number.max())
//                drawVerticalLines()
                
                for i in 0..<dataEntries.count {
                    showEntry(index: i, entry: dataEntries[i], maxNo: number.max() ?? 0)
                }
            }
        }
    }
    
    var dataEntriesDiscovery: [DiscoveryList]? = nil {
        didSet {
            mainLayer.sublayers?.forEach({$0.removeFromSuperlayer()})
            
            if let dataEntries = dataEntriesDiscovery {
                scrollView.contentSize = CGSize(width: self.frame.size.width, height: (barHeight + space)*CGFloat(dataEntries.count))
                mainLayer.frame = CGRect(x: 0, y: 0, width: scrollView.contentSize.width, height: scrollView.contentSize.height)
                
                //                drawHorizontalLines()
                var number = [Int]()
                
                for i in 0..<dataEntries.count {
                    number.append(dataEntries[i].value)
                }
                print(number.max())
                
                for i in 0..<dataEntries.count {
                    // number.append(dataEntries[i].value)
                    showEntryDiscovery(index: i, entry: dataEntries[i],maxNo: number.max() ?? 0)
                }
            }
        }
    }
    
    var dataEntriesAudienceLocation: [CityList]? = nil {
        didSet {
            mainLayer.sublayers?.forEach({$0.removeFromSuperlayer()})
            
            if let dataEntries = dataEntriesAudienceLocation {
                scrollView.contentSize = CGSize(width: self.frame.size.width, height: (barHeight + space)*CGFloat(dataEntries.count))
                mainLayer.frame = CGRect(x: 0, y: 0, width: scrollView.contentSize.width, height: scrollView.contentSize.height)
                
                    for i in 0..<dataEntries.count {
                    showEntryAudienceCity(index: i, entry: dataEntries[i])
                  }
                }
            }
    }
    
    var dataEntriesAudienceLocationCountry: [CountryList]? = nil {
        didSet {
            mainLayer.sublayers?.forEach({$0.removeFromSuperlayer()})
            
            if let dataEntries = dataEntriesAudienceLocationCountry {
                scrollView.contentSize = CGSize(width: self.frame.size.width, height: (barHeight + space)*CGFloat(dataEntries.count))
                mainLayer.frame = CGRect(x: 0, y: 0, width: scrollView.contentSize.width, height: scrollView.contentSize.height)
                
                for i in 0..<dataEntries.count {
                    showEntryAudienceCountry(index: i, entry: dataEntries[i])
                }
            }
        }
    }
    
    var dataEntriesAudienceAgeAll: [AllAgeList]? = nil {
        didSet {
            mainLayer.sublayers?.forEach({$0.removeFromSuperlayer()})
            
            if let dataEntries = dataEntriesAudienceAgeAll {
                scrollView.contentSize = CGSize(width: self.frame.size.width, height: (barHeight + space)*CGFloat(dataEntries.count))
                mainLayer.frame = CGRect(x: 0, y: 0, width: scrollView.contentSize.width, height: scrollView.contentSize.height)
                
                for i in 0..<dataEntries.count {
                    showEntryAudienceAllAge(index: i, entry: dataEntries[i])
                }
            }
        }
    }
    
    var dataEntriesAudienceAgeMen: [MenList]? = nil {
        didSet {
            mainLayer.sublayers?.forEach({$0.removeFromSuperlayer()})
            
            if let dataEntries = dataEntriesAudienceAgeMen {
                scrollView.contentSize = CGSize(width: self.frame.size.width, height: (barHeight + space)*CGFloat(dataEntries.count))
                mainLayer.frame = CGRect(x: 0, y: 0, width: scrollView.contentSize.width, height: scrollView.contentSize.height)
                
                for i in 0..<dataEntries.count {
                    showEntryAudienceAllMen(index: i, entry: dataEntries[i])
                }
            }
        }
    }
    
    var dataEntriesAudienceAgeWomen: [WomenList]? = nil {
        didSet {
            mainLayer.sublayers?.forEach({$0.removeFromSuperlayer()})
            
            if let dataEntries = dataEntriesAudienceAgeWomen {
                scrollView.contentSize = CGSize(width: self.frame.size.width, height: (barHeight + space)*CGFloat(dataEntries.count))
                mainLayer.frame = CGRect(x: 0, y: 0, width: scrollView.contentSize.width, height: scrollView.contentSize.height)
                
                for i in 0..<dataEntries.count {
                    showEntryAudienceAllWomen(index: i, entry: dataEntries[i])
                }
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        scrollView.layer.addSublayer(mainLayer)
        scrollView.showsHorizontalScrollIndicator = false
        self.addSubview(scrollView)
    }
    
    override func layoutSubviews() {
        scrollView.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
    }
    
    private func showEntry(index: Int, entry: GraphList,maxNo: Int) {
        /// Starting x postion of the bar
//        let xPos: CGFloat = space + CGFloat(index) * (barWidth + space)
        
        let value = (entry.value) //+ 10
        
        let height: Float = Float(value) / Float(maxNo)
        
        let xPos: CGFloat =  translateWidthValueToYPosition(value: height)
        
        /// Starting y postion of the bar
   
         let yPos: CGFloat = space + CGFloat(index) * (barHeight + space)
        
         let title = entry.unit!.prefix(3)
       
        /// Draw text left the bar
        drawTitle(xPos: 5, yPos:yPos + barHeight/4 , title: String(title), color: titleColor)
        
        drawBar(xPos: xPos, yPos: yPos, color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1))
        
        // Draw text right to the bar
//        drawTextValue(xPos: mainLayer.frame.width - leftSpace, yPos: yPos + space/2, textValue: entry.textValue, color: textColor)
        drawTextValue(xPos: ((mainLayer.frame.width - xPos) + spaceForLeftTitle), yPos: yPos + space/2, textValue: String(entry.value), color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1))
    }
    
    private func showEntryDiscovery(index: Int, entry: DiscoveryList,maxNo: Int) {
        let value = (entry.value) //+ 10
        
        let height: Float = Float(value) / Float(maxNo)
        
        let xPos: CGFloat =  translateWidthValueToYPosition(value: height)
        
        /// Starting y postion of the bar
        
        let yPos: CGFloat = space + CGFloat(index) * (barHeight + space)
        
        let title = entry.unit!.prefix(3)
        
        /// Draw text left the bar
        drawTitle(xPos: 5, yPos:yPos + barHeight/4 , title: String(title), color: titleColor)
        
        drawBar(xPos: xPos, yPos: yPos, color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1))
        
        // Draw text right to the bar
        //        drawTextValue(xPos: mainLayer.frame.width - leftSpace, yPos: yPos + space/2, textValue: entry.textValue, color: textColor)
        drawTextValue(xPos: ((mainLayer.frame.width - xPos) + spaceForLeftTitle), yPos: yPos + space/2, textValue: String(entry.value), color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1))
    }
    
    private func showEntryAudienceCity(index: Int, entry: CityList) {
        let value = (entry.value)//+ 10
        
        let height: Float = Float(value) / 100
        
        let xPos: CGFloat =  translateWidthValueToYPosition(value: height)
        
        /// Starting y postion of the bar
        
        let yPos: CGFloat = space + CGFloat(index) * (barHeight + space)
        
        
        /// Draw text left the bar
        drawTitle(xPos: 5, yPos:yPos + barHeight/4 , title: entry.unit!, color: titleColor)
        
        drawBar(xPos: xPos, yPos: yPos, color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1))
        
        // Draw text right to the bar
        //        drawTextValue(xPos: mainLayer.frame.width - leftSpace, yPos: yPos + space/2, textValue: entry.textValue, color: textColor)
        drawTextValue(xPos: ((mainLayer.frame.width - xPos) + spaceForLeftTitle), yPos: yPos + space/2, textValue: "\(entry.value)%", color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1))
    }
    
    private func showEntryAudienceCountry(index: Int, entry: CountryList) {
        let value = (entry.value)//+ 10
        
        let height: Float = Float(value) / 100
        
        let xPos: CGFloat =  translateWidthValueToYPosition(value: height)
        
        /// Starting y postion of the bar
        
        let yPos: CGFloat = space + CGFloat(index) * (barHeight + space)
        
        if entry.unit == "United Kingdom"
        {
            entry.unit = "UK"
        }
        
        /// Draw text left the bar
        drawTitle(xPos: 5, yPos:yPos + barHeight/4 , title: entry.unit!, color: titleColor)
        
        drawBar(xPos: xPos, yPos: yPos, color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1))
        
        // Draw text right to the bar
        //        drawTextValue(xPos: mainLayer.frame.width - leftSpace, yPos: yPos + space/2, textValue: entry.textValue, color: textColor)
        drawTextValue(xPos: ((mainLayer.frame.width - xPos) + spaceForLeftTitle), yPos: yPos + space/2, textValue: "\(entry.value)%", color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1))
    }
    
    private func showEntryAudienceAllAge(index: Int, entry: AllAgeList) {
        let value = (entry.value) //+ 10
        
        let height: Float = Float(value) / 100
        
        let xPos: CGFloat =  translateWidthValueToYPosition(value: height)
        
        /// Starting y postion of the bar
        
        let yPos: CGFloat = space + CGFloat(index) * (barHeight + space)
        
        
        /// Draw text left the bar
        drawTitle(xPos: 5, yPos:yPos + barHeight/4 , title: entry.unit!, color: titleColor)
        
        drawBar(xPos: xPos, yPos: yPos, color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1))
        
        // Draw text right to the bar
        //        drawTextValue(xPos: mainLayer.frame.width - leftSpace, yPos: yPos + space/2, textValue: entry.textValue, color: textColor)
        drawTextValue(xPos: ((mainLayer.frame.width - xPos) + spaceForLeftTitle), yPos: yPos + space/2, textValue: "\(entry.value)%", color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1))
    }
    
    private func showEntryAudienceAllMen(index: Int, entry: MenList) {
        let value = (entry.value) //+ 10
        
        let height: Float = Float(value) / 100
        
        let xPos: CGFloat =  translateWidthValueToYPosition(value: height)
        
        /// Starting y postion of the bar
        
        let yPos: CGFloat = space + CGFloat(index) * (barHeight + space)
        
        
        /// Draw text left the bar
        drawTitle(xPos: 5, yPos:yPos + barHeight/4 , title: entry.unit!, color: titleColor)
        
        drawBar(xPos: xPos, yPos: yPos, color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1))
        
        // Draw text right to the bar
        //        drawTextValue(xPos: mainLayer.frame.width - leftSpace, yPos: yPos + space/2, textValue: entry.textValue, color: textColor)
        drawTextValue(xPos: ((mainLayer.frame.width - xPos) + spaceForLeftTitle), yPos: yPos + space/2, textValue: "\(entry.value)%", color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1))
    }
    
    private func showEntryAudienceAllWomen(index: Int, entry: WomenList) {
        let value = (entry.value) //+ 10
        
        let height: Float = Float(value) / 100
        
        let xPos: CGFloat =  translateWidthValueToYPosition(value: height)
        
        /// Starting y postion of the bar
        
        let yPos: CGFloat = space + CGFloat(index) * (barHeight + space)
        
        
        /// Draw text left the bar
        drawTitle(xPos: 5, yPos:yPos + barHeight/4 , title: entry.unit!, color: titleColor)
        
        drawBar(xPos: xPos, yPos: yPos, color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1))
        
        // Draw text right to the bar
        //        drawTextValue(xPos: mainLayer.frame.width - leftSpace, yPos: yPos + space/2, textValue: entry.textValue, color: textColor)
        drawTextValue(xPos: ((mainLayer.frame.width - xPos) + spaceForLeftTitle), yPos: yPos + space/2, textValue: "\(entry.value)%", color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1))
    }
    
    private func drawBar(xPos: CGFloat, yPos: CGFloat, color: UIColor) {
        let barLayer = CALayer()
        barLayer.frame = CGRect(x: spaceForLeftTitle + space, y: yPos, width: mainLayer.frame.width - bottomSpace - xPos, height: barHeight)
        
        barLayer.backgroundColor = color.cgColor
        mainLayer.addSublayer(barLayer)
    }
    
    private func drawHorizontalLines() {
        self.layer.sublayers?.forEach({
            if $0 is CAShapeLayer {
                $0.removeFromSuperlayer()
            }
        })
        let horizontalLineInfos = [["value": Float(0.0), "dashed": false], ["value": Float(0.5), "dashed": true], ["value": Float(1.0), "dashed": false]]
        for lineInfo in horizontalLineInfos {
            let xPos = CGFloat(0.0)
            let yPos = translateWidthValueToYPosition(value: (lineInfo["value"] as! Float))
            let path = UIBezierPath()
            path.move(to: CGPoint(x: xPos, y: yPos))
            path.addLine(to: CGPoint(x: scrollView.frame.size.width, y: yPos))
            let lineLayer = CAShapeLayer()
            lineLayer.path = path.cgPath
            lineLayer.lineWidth = 0.5
            if lineInfo["dashed"] as! Bool {
                lineLayer.lineDashPattern = [4, 4]
            }
            lineLayer.strokeColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1).cgColor
            self.layer.insertSublayer(lineLayer, at: 0)
        }
    }
    
    func drawVerticalLines(){
        self.layer.sublayers?.forEach({
            if $0 is CAShapeLayer {
                $0.removeFromSuperlayer()
            }
        })
        
        let horizontalLineInfos = [["value": Float(0.0), "dashed": false], ["value": Float(0.5), "dashed": true], ["value": Float(1.0), "dashed": false]]
        
        for lineInfo in horizontalLineInfos {
            let xPos = translateWidthValueToYPosition(value: (lineInfo["value"] as! Float))
            let yPos = CGFloat(0.0)
            
            
            let path = UIBezierPath()
            path.move(to: CGPoint(x: xPos, y: yPos))
            path.addLine(to: CGPoint(x:xPos , y: scrollView.frame.size.width))
            let lineLayer = CAShapeLayer()
            lineLayer.path = path.cgPath
            lineLayer.lineWidth = 0.5
            if lineInfo["dashed"] as! Bool {
                lineLayer.lineDashPattern = [4, 4]
            }
            lineLayer.strokeColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1).cgColor
            self.layer.insertSublayer(lineLayer, at: 0)
        }
    }
    
    
    
    
    private func drawTextValue(xPos: CGFloat, yPos: CGFloat, textValue: String, color: UIColor) {
        let textLayer = CATextLayer()
        textLayer.frame = CGRect(x: xPos, y: yPos, width: barHeight+space, height: 22)
        textLayer.foregroundColor = color.cgColor
        textLayer.backgroundColor = UIColor.clear.cgColor
        textLayer.alignmentMode = CATextLayerAlignmentMode.left
        textLayer.contentsScale = UIScreen.main.scale
        textLayer.font = CTFontCreateWithName(UIFont.systemFont(ofSize: 0).fontName as CFString, 0, nil)
        textLayer.fontSize = CGFloat(fontSize)
        textLayer.string = textValue
        mainLayer.addSublayer(textLayer)
    }
    
    private func drawTitle(xPos: CGFloat, yPos: CGFloat, title: String, color: UIColor) {
        let textLayer = CATextLayer()
        textLayer.frame = CGRect(x: xPos, y: yPos, width: spaceForLeftTitle + space, height: 22)
        textLayer.foregroundColor = color.cgColor
        textLayer.backgroundColor = UIColor.clear.cgColor
        textLayer.alignmentMode =  CATextLayerAlignmentMode.left
        textLayer.contentsScale = UIScreen.main.scale
        textLayer.font = CTFontCreateWithName(UIFont.systemFont(ofSize: 0).fontName as CFString, 0, nil)
        textLayer.fontSize = CGFloat(fontSize)
        textLayer.string = title
        mainLayer.addSublayer(textLayer)
    }
    
    private func translateWidthValueToYPosition(value: Float) -> CGFloat {
//        let height: CGFloat = CGFloat(value) * (mainLayer.frame.height - bottomSpace - topSpace)
//        return mainLayer.frame.height - bottomSpace - height
        
        let width: CGFloat = CGFloat(value) * (mainLayer.frame.width - leftSpace - rightSpace)
        return mainLayer.frame.width - leftSpace - width
        
    }

}
