//
//  FeedPostImagesCVC.swift
//  TrenderAlert
//
//  Created by HPL on 28/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class FeedPostImagesCVC: UICollectionViewCell {

    static let reuseIdentifier = "FeedPostImagesCVC"
    
    @IBOutlet weak var feedImage: UIImageView!
    @IBOutlet weak var feedPostCountLbl: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
