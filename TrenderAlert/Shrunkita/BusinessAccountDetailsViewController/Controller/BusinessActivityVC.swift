//
//  BusinessActivityVC.swift
//  TrenderAlert
//
//  Created by HPL on 25/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class BusinessActivityVC: UIViewController {

    @IBOutlet weak var activityLbl: UILabel!
    @IBOutlet weak var contentLbl: UILabel!
    @IBOutlet weak var audienceLbl: UILabel!
    
    @IBOutlet weak var activityBtn: UIButton!
    @IBOutlet weak var contentBtn: UIButton!
    @IBOutlet weak var audienceBtn: UIButton!
    
    @IBOutlet weak var activitySeperatorView: UIView!
    @IBOutlet weak var contentSeperatorView: UIView!
    @IBOutlet weak var audienceSeperatorView: UIView!
  
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var businessActivityTable: UITableView!
    var collectionTrend : UICollectionView!
    
    var graphModel = InteractionDataModel()
    var discoveryGraph = DiscoveryDataModel()
    var audidenceGraph = AudienceDataModel()
    var totalVisits = Int()
    var totalInteractionCount = String()
    var totalDiscoveryCount = String()
    var totalAudienceCount = String()
    var totalDiscovery = Int()
    var previousDate = String()
    var currentDate = String()
    
    var heightForBarchart = (UIDevice.current.userInterfaceIdiom == .pad) ? (UIScreen.main.bounds.height * 0.3) : (UIScreen.main.bounds.height * 0.25)
    
    var selectedBusinessAccountDetailsOption = 1
    var feedPostImageCollectionHeight = (UIDevice.current.userInterfaceIdiom == .pad) ? 0.25 : 0.25
    
    var trendData = TrendListData()
    
    var activityBtnActivated:Bool?{
         willSet(newValue){
            if newValue != nil{
                self.activityLbl.textColor = UIColor.black
                self.contentLbl.textColor  = UIColor.lightGray
                self.audienceLbl.textColor = UIColor.lightGray
                
                activitySeperatorView.isHidden = false
                contentSeperatorView.isHidden  = true
                audienceSeperatorView.isHidden = true
                
            }
        }
    }
    
    var contentBtnActivated:Bool?{
        willSet(newValue){
            if newValue != nil{
                self.activityLbl.textColor = UIColor.lightGray
                self.contentLbl.textColor  = UIColor.black
                self.audienceLbl.textColor = UIColor.lightGray
                
                activitySeperatorView.isHidden = true
                contentSeperatorView.isHidden  = false
                audienceSeperatorView.isHidden = true
                
            //    getAllTrends()
            }
        }
    }
    
    
    var audienceBtnActivated:Bool?{
        willSet(newValue){
            if newValue != nil{
                self.activityLbl.textColor = UIColor.lightGray
                self.contentLbl.textColor  = UIColor.lightGray
                self.audienceLbl.textColor = UIColor.black
                
                activitySeperatorView.isHidden = true
                contentSeperatorView.isHidden  = true
                audienceSeperatorView.isHidden = false
            }
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableview()
        setupNavigationbar()
        
        self.activityLbl.text = MySingleton.shared.selectedLangData.activity
        self.contentLbl.text  = MySingleton.shared.selectedLangData.content
        self.audienceLbl.text = MySingleton.shared.selectedLangData.audience
        self.lblTitle.text = MySingleton.shared.selectedLangData.business_account_details
        
        activityBtn.tag = 1
        contentBtn.tag = 2
        audienceBtn.tag = 3
        
        getAllInteractions()
        
        let now = Date()
        let sevenDaysAgo = now.addingTimeInterval(-6 * 24 * 60 * 60)
        print("7 days ago: \(sevenDaysAgo)")
        
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd hh:mm:ssZ"
        
        let myString = formatter.string(from: Date()) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "MMM d"
        // again convert your date to string
        self.currentDate = formatter.string(from: yourDate!)
        
        print(currentDate)
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "yyyy-MM-dd hh:mm:ssZ"
        let myString1 = formatter1.string(from: sevenDaysAgo) // string purpose I add here
        // convert your string to date
        let yourDate1 = formatter1.date(from: myString1)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "MMM d"
        // again convert your date to string
        self.previousDate = formatter.string(from: yourDate1!)
        
        print(previousDate)
        
    }
    
    func setupNavigationbar(){
        let leftBarButton = UIButton()
        leftBarButton.setImage(#imageLiteral(resourceName: "img_MenuBarBtn"), for: .normal)
        leftBarButton.addTarget(self, action: #selector(didTapMenu), for: .touchUpInside)
        let item1 = UIBarButtonItem()
        item1.customView = leftBarButton
        self.navigationItem.leftBarButtonItems = [item1]
        Utilities.setNavigationBarWithTitle(viewController: self, title: MySingleton.shared.selectedLangData.business_account)
    }
    
    @objc func didTapMenu(_ sender: UIBarButtonItem) {
        if self.menuContainerViewController != nil {
            self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    func getAllInteractions()
    {
        let now = Date()
        let sevenDaysAgo = now.addingTimeInterval(-6 * 24 * 60 * 60)
        print("7 days ago: \(sevenDaysAgo)")
        
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd hh:mm:ssZ"
        
        let myString = formatter.string(from: Date()) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "yyyy-MM-dd"
        // again convert your date to string
        let currentDate = formatter.string(from: yourDate!)
        
        print(currentDate)
        let formatter1 = DateFormatter()
        formatter1.dateFormat = "yyyy-MM-dd hh:mm:ssZ"
        let myString1 = formatter1.string(from: sevenDaysAgo) // string purpose I add here
        // convert your string to date
        let yourDate1 = formatter1.date(from: myString1)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "yyyy-MM-dd"
        // again convert your date to string
        let previousDate = formatter.string(from: yourDate1!)
        
        print(previousDate)
        
        let params = ["StartDate":previousDate,"EndDate":currentDate] as [String : Any]
        print(params)
//        let params = ["StartDate":"2019-08-16","EndDate":"2019-08-20"] as [String : Any]
        WebServices().callUserService(service: .getInteractionGraph, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post) { (serviceResponse, responseData) in
            print("Get All Interactions --- \(serviceResponse)")
            if(serviceResponse["Status"] as! Bool == true)
            {
                 DispatchQueue.main.async {
                //self.trendImagesArray.removeAll()
                self.graphModel = WebServices().decodeDataToClass(data: responseData, decodeClass: InteractionDataModel.self)!
                self.totalVisits = serviceResponse["TotalRecords"] as! Int
                var numbers = [Int]()
                for i in 0...self.graphModel.data.count-1
                {
                    numbers.append(self.graphModel.data[i].value)
                }
                self.totalInteractionCount = String(numbers.reduce(0, +))
                self.getDiscoveryGraph(startDate: previousDate, endDate: currentDate)
                }
               // self.businessActivityTable.reloadData()
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }

    }
    
    func getDiscoveryGraph(startDate : String , endDate : String)
    {
        let params = ["StartDate":startDate,"EndDate":endDate] as [String : Any]
        //        let params = ["StartDate":"2019-08-16","EndDate":"2019-08-20"] as [String : Any]
        WebServices().callUserService(service: .getDiscoveryGraph, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post) { (serviceResponse, responseData) in
            print("Get All Discovery --- \(serviceResponse)")
            if(serviceResponse["Status"] as! Bool == true)
            {
                //self.trendImagesArray.removeAll()
                DispatchQueue.main.async {
           
                self.discoveryGraph = WebServices().decodeDataToClass(data: responseData, decodeClass: DiscoveryDataModel.self)!
                self.totalDiscovery = serviceResponse["TotalRecords"] as! Int
                var numbers = [Int]()
                for i in 0...self.discoveryGraph.data.count-1
                {
                    numbers.append(self.discoveryGraph.data[i].value)
                }
                self.totalDiscoveryCount = String(numbers.reduce(0, +))
                self.getAudienceGraph(startDate: startDate, endDate: endDate)
                }
              //   self.businessActivityTable.reloadData()
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func getAudienceGraph(startDate : String , endDate : String)
    {
        let params = ["StartDate":startDate,"EndDate":endDate] as [String : Any]
        //        let params = ["StartDate":"2019-08-16","EndDate":"2019-08-20"] as [String : Any]
        WebServices().callUserService(service: .audienceGraph, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post) { (serviceResponse, responseData) in
            print("Get All Audience --- \(serviceResponse)")
            if(serviceResponse["Status"] as! Bool == true)
            {
                //self.trendImagesArray.removeAll()
                 DispatchQueue.main.async {
                self.audidenceGraph = WebServices().decodeDataToClass(data: responseData, decodeClass: AudienceDataModel.self)!
                self.totalAudienceCount = String(serviceResponse["TotalRecords"] as! Int)
//                var numbers = [Int]()
//                for i in 0...self.discoveryGraph.data.count-1
//                {
//                    numbers.append(self.discoveryGraph.data[i].value)
//                }
//                self.totalAudienceCount = String(numbers.reduce(0, +))
                
                self.businessActivityTable.reloadData()
                }

                }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    
    func setupTableview(){
        
        businessActivityTable.delegate = self
        businessActivityTable.dataSource = self
        businessActivityTable.estimatedRowHeight = 100
        businessActivityTable.rowHeight = UITableView.automaticDimension
        businessActivityTable.register(UINib(nibName: BusinessAccountActivityTVC.reuseIdentifier, bundle: nil), forCellReuseIdentifier: BusinessAccountActivityTVC.reuseIdentifier)
        businessActivityTable.register(UINib(nibName: BusinessAccountAudienceTVC.reuseIdentifier, bundle: nil), forCellReuseIdentifier: BusinessAccountAudienceTVC.reuseIdentifier)
        businessActivityTable.register(UINib(nibName: BusinessAccountContentTVC.reuseIdentifier, bundle: nil), forCellReuseIdentifier: BusinessAccountContentTVC.reuseIdentifier)
        getAllTrends()
    }
    
    @IBAction func toggleBusinessAccountDetailsOptions(_ sender: UIButton) {
        self.selectedBusinessAccountDetailsOption = sender.tag
        let indexPath = IndexPath(row: 0, section: 0)
        businessActivityTable.scrollToRow(at: indexPath, at: .top, animated: true)
        self.businessActivityTable.reloadData()
    }
    
    func getAllTrends()
    {
        let params = ["SortColumn":"","SortDirection":"","Search":"","Skip":0,"Take":100] as [String:Any]
//        print("Param : ",params)
//        print("Param totalTrends", totalTrends,"trendData.count",trendData.data.count)
        
        WebServices().callUserService(service: .getBuisnessTrendList, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post) { (serviceResponse, responseData) in
            print("Get All Trends --- \(serviceResponse)")
            if(serviceResponse["Status"] as! Bool == true)
            {
                 DispatchQueue.main.async {
                let temp = WebServices().decodeDataToClass(data: responseData, decodeClass: TrendListData.self)!
                self.trendData.data += temp.data
                
                self.businessActivityTable.reloadData()
                }
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }

}


extension BusinessActivityVC:UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch selectedBusinessAccountDetailsOption {
        case 1:
            // Activity
            activityBtnActivated = true
            return businessAccountActivityCell(tableView,indexPath)
        case 2:
            // Content
            contentBtnActivated = true
             return businessAccountContent(tableView,indexPath)
        case 3:
            //Audience
            audienceBtnActivated = true
             return businessAccountAudienceCell(tableView,indexPath)
        default:
            // Activity
            activityBtnActivated = true
          return businessAccountActivityCell(tableView,indexPath)
        }
    }
    
    func businessAccountActivityCell(_ tableView:UITableView,_ indexPath:IndexPath)->UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: BusinessAccountActivityTVC.reuseIdentifier, for: indexPath) as! BusinessAccountActivityTVC
        cell.interactionBarChartHeight.constant = heightForBarchart
        cell.discoveryBarChartHeight.constant = heightForBarchart
        cell.discoveryBarChartEntiries = self.discoveryGraph.data
        cell.interactionBarChartEntiries = self.graphModel.data
        cell.lblTotalCount.text = self.totalInteractionCount
        cell.btnInteraction.addTarget(self, action: #selector(self.proccedToIntereactionInfo(_:)), for: .touchUpInside)
        cell.btnDiscovery.addTarget(self, action: #selector(self.proccedToDiscoveryInfo(_:)), for: .touchUpInside)
        if Int(self.totalInteractionCount) == 0
        {
            cell.lblNoDataIntereaction.isHidden = false
            cell.lblNoDataIntereaction.text = MySingleton.shared.selectedLangData.no_record_found
        }
        else
        {
             cell.lblNoDataIntereaction.isHidden = true
        }
        if Int(self.totalDiscoveryCount) == 0
        {
            cell.lblNoDataDiscovery.isHidden = false
            cell.lblNoDataDiscovery.text = MySingleton.shared.selectedLangData.no_record_found
        }
        else
        {
            cell.lblNoDataDiscovery.isHidden = true
        }
        cell.lblVisits.text = String(self.totalVisits)
        cell.lblDate.text = "\(self.previousDate) - \(self.currentDate)"
        cell.lblDiscoveryTotalCount.text  = self.totalDiscoveryCount
        cell.lblReach.text = String(self.totalDiscovery)
        cell.setupCell()
        return cell
    }
    
    @objc func proccedToIntereactionInfo(_ sender: UIButton){
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "InteractionViewController") as! InteractionViewController
        let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height)
        self.navigationController?.present(pop, animated: true, completion: nil)
    }
    
    @objc func proccedToDiscoveryInfo(_ sender: UIButton){
        let destination = self.storyboard?.instantiateViewController(withIdentifier: "DiscoveryViewController") as! DiscoveryViewController
        let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height)
        self.navigationController?.present(pop, animated: true, completion: nil)
    }
    
    func businessAccountAudienceCell(_ tableView:UITableView,_ indexPath:IndexPath)->UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: BusinessAccountAudienceTVC.reuseIdentifier, for: indexPath) as! BusinessAccountAudienceTVC
        
        cell.cityCountryBarChartViewHeight.constant = heightForBarchart
        cell.menWomenBarChartViewHeight.constant = heightForBarchart
        cell.rowWidth = tableView.contentSize.width - 32
        
        cell.cityBarChartEntiries = self.audidenceGraph.data.Location.City ?? []
           cell.allAgeBarChartEntiries = self.audidenceGraph.data.Age.ALL

        cell.menBtn.tag = indexPath.row
        cell.menBtn.setTitle(MySingleton.shared.selectedLangData.men, for: .normal)
        cell.menBtn.addTarget(self, action: #selector(self.setMenGraph(_:)), for: .touchUpInside)
        cell.womenBtn.tag = indexPath.row
        cell.womenBtn.setTitle(MySingleton.shared.selectedLangData.women, for: .normal)
         cell.womenBtn.addTarget(self, action: #selector(self.setWomenGraph(_:)), for: .touchUpInside)
        cell.allBtn.tag = indexPath.row
        cell.allBtn.setTitle(MySingleton.shared.selectedLangData.all, for: .normal)
         cell.allBtn.addTarget(self, action: #selector(self.setAllAgeGraph(_:)), for: .touchUpInside)
        cell.countriesBtn.tag = indexPath.row
        cell.countriesBtn.setTitle(MySingleton.shared.selectedLangData.countries, for: .normal)
         cell.countriesBtn.addTarget(self, action: #selector(self.setCountryGraph(_:)), for: .touchUpInside)
        cell.citiesBtn.tag = indexPath.row
        cell.citiesBtn.setTitle(MySingleton.shared.selectedLangData.cities, for: .normal)
        cell.citiesBtn.addTarget(self, action: #selector(self.setCityGraph(_:)), for: .touchUpInside)
        cell.setupCell()
        
    
        return cell
    }
    
    @objc func setMenGraph(_ sender: UIButton){
         let cell = self.businessActivityTable.cellForRow(at: IndexPath(row: sender.tag, section: 0))as? BusinessAccountAudienceTVC
        cell!.menAgeBarChartEntiries = self.audidenceGraph.data.Age.MEN
        sender.setTitleColor(UIColor.baseColor, for: .normal)
        cell?.allBtn.setTitleColor(UIColor.black, for: .normal)
        cell?.womenBtn.setTitleColor(UIColor.black, for: .normal)
        cell!.setupCellMen()
    }
    
    @objc func setWomenGraph(_ sender: UIButton){
        let cell = self.businessActivityTable.cellForRow(at: IndexPath(row: sender.tag, section: 0))as? BusinessAccountAudienceTVC
        cell!.womenAgeBarChartEntiries = self.audidenceGraph.data.Age.WOMEN
        sender.setTitleColor(UIColor.baseColor, for: .normal)
        cell?.menBtn.setTitleColor(UIColor.black, for: .normal)
        cell?.allBtn.setTitleColor(UIColor.black, for: .normal)
        cell!.setupCellWomen()
    }
    
    @objc func setAllAgeGraph(_ sender: UIButton){
        let cell = self.businessActivityTable.cellForRow(at: IndexPath(row: sender.tag, section: 0))as? BusinessAccountAudienceTVC
        cell!.allAgeBarChartEntiries = self.audidenceGraph.data.Age.ALL
        sender.setTitleColor(UIColor.baseColor, for: .normal)
        cell?.womenBtn.setTitleColor(UIColor.black, for: .normal)
        cell?.menBtn.setTitleColor(UIColor.black, for: .normal)
        cell!.setupCellAll()
    }
    
    @objc func setCityGraph(_ sender: UIButton){
        let cell = self.businessActivityTable.cellForRow(at: IndexPath(row: sender.tag, section: 0))as? BusinessAccountAudienceTVC
        cell!.cityBarChartEntiries = self.audidenceGraph.data.Location.City ?? []
        sender.setTitleColor(UIColor.baseColor, for: .normal)
        cell?.countriesBtn.setTitleColor(UIColor.black, for: .normal)
        cell!.setupCellCity()
    }
    
    @objc func setCountryGraph(_ sender: UIButton){
        let cell = self.businessActivityTable.cellForRow(at: IndexPath(row: sender.tag, section: 0))as? BusinessAccountAudienceTVC
        cell!.countryBarChartEntiries = self.audidenceGraph.data.Location.Country ?? []
        sender.setTitleColor(UIColor.baseColor, for: .normal)
        cell?.citiesBtn.setTitleColor(UIColor.black, for: .normal)
        cell!.setupCellCountry()
    }
    
    func businessAccountContent(_ tableView:UITableView,_ indexPath:IndexPath)->UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: BusinessAccountContentTVC.reuseIdentifier, for: indexPath) as! BusinessAccountContentTVC
        cell.btnPostTrend.addTarget(self, action: #selector(proccedToPostTrend(_:)), for: .touchUpInside)
        cell.feedPostImagesCollectionView.delegate = self
        cell.feedPostImagesCollectionView.dataSource = self
        cell.feedPostImagesCollectionView.reloadData()
        self.collectionTrend = cell.feedPostImagesCollectionView
         cell.seeAllBtn.setTitle(MySingleton.shared.selectedLangData.see_all, for: .normal)
        cell.seeAllBtn.addTarget(self, action: #selector(proccedToSeeAllTrends(_:)), for: .touchUpInside)
        return cell
    }
    
    @objc func proccedToPostTrend(_ sender: UIButton)
    {
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "SetTrendViewController") as! SetTrendViewController
        destination.isBusinessAccount = 1
        destination.delegate = self
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @objc func proccedToSeeAllTrends(_ sender: UIButton)
    {
        let storyboard = UIStoryboard(name: "BusinessAccountDetailsSB", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "BATrendsViewController") as! BATrendsViewController
        destination.delegate = self
        self.navigationController?.pushViewController(destination, animated: true)
    }
}

extension BusinessActivityVC : BuisnessAccountDelegate, BATrendProtocolDelegate
{
    func getLikeCount(like: [Int]) {
        let cellIndexPath = IndexPath(row: 0, section: 0)
        let collectionData = collectionTrend.cellForItem(at: cellIndexPath) as? FeedPostImagesCVC
        collectionData?.feedPostCountLbl.text = ""
        collectionData?.feedPostCountLbl.text = String(like[0])
        if like[0] > 1
        {
            collectionData?.feedPostCountLbl.text = "\( collectionData?.feedPostCountLbl.text ?? "0") \(MySingleton.shared.selectedLangData.Likes)"
        }
        else
        {
            collectionData?.feedPostCountLbl.text = "\( collectionData?.feedPostCountLbl.text ?? "0") \(MySingleton.shared.selectedLangData.like)"
        }
        let cellIndexPath1 = IndexPath(row: 1, section: 0)
        let collectionData1 = collectionTrend.cellForItem(at: cellIndexPath1) as? FeedPostImagesCVC
        collectionData1?.feedPostCountLbl.text = ""
        collectionData1?.feedPostCountLbl.text = String(like[1])
        if like[1] > 1
        {
            collectionData1?.feedPostCountLbl.text = "\( collectionData1?.feedPostCountLbl.text ?? "0") \(MySingleton.shared.selectedLangData.Likes)"
        }
        else
        {
            collectionData1?.feedPostCountLbl.text = "\( collectionData1?.feedPostCountLbl.text ?? "0") \(MySingleton.shared.selectedLangData.like)"
        }
        if self.trendData.data[2].IsGoogleAd != true {
            let cellIndexPath2 = IndexPath(row: 2, section: 0)
            let collectionData2 = collectionTrend.cellForItem(at: cellIndexPath2) as? FeedPostImagesCVC
            collectionData2?.feedPostCountLbl.text = ""
             collectionData2?.feedPostCountLbl.text = String(like[2])
            if like[2] > 1
            {
                collectionData2?.feedPostCountLbl.text = "\( collectionData2?.feedPostCountLbl.text ?? "0") \(MySingleton.shared.selectedLangData.Likes)"
            }
            else
            {
                collectionData2?.feedPostCountLbl.text = "\( collectionData2?.feedPostCountLbl.text ?? "0") \(MySingleton.shared.selectedLangData.like)"
            }
        }
    }
    
    func BuisnessAccount(reload: String) {
       // self.collectionTrend.reloadData()
        self.trendData.data.removeAll()
        getAllTrends()
    }
    
}

extension BusinessActivityVC : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.trendData.data.count != 0 && self.trendData.data.count > 1
        {
          return 3
        }
        else if self.trendData.data.count == 1
        {
            return 1
        }
        else
        {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionTrend.dequeueReusableCell(withReuseIdentifier: "FeedPostImagesCVC", for: indexPath) as! FeedPostImagesCVC
        if trendData.data[indexPath.row].ImageList?.count != 0 {
          cell.feedImage.sd_setImage(with: URL(string: trendData.data[indexPath.row].ImageList![0].TrendImage), completed: nil)
        }
        if trendData.data[indexPath.row].IsGoogleAd != true
        {
            if trendData.data[indexPath.row].TrendLikesCount > 1
            {
                cell.feedPostCountLbl.text = "\(String(trendData.data[indexPath.row].TrendLikesCount)) \(MySingleton.shared.selectedLangData.Likes)"
            }
            else
            {
               cell.feedPostCountLbl.text = "\(String(trendData.data[indexPath.row].TrendLikesCount)) \(MySingleton.shared.selectedLangData.like)"
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        print(collectionView.frame.height)
        return CGSize(width:collectionView.frame.height-50, height:collectionView.frame.height)
    }
    
}

