//
//  BusinessAccountActivityTVC.swift
//  TrenderAlert
//
//  Created by HPL on 25/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class BusinessAccountActivityTVC: UITableViewCell {
    
    @IBOutlet weak var btnInteraction: UIButton!
    @IBOutlet weak var btnDiscovery: UIButton!
    @IBOutlet weak var lblReach: UILabel!
    @IBOutlet weak var lblDiscoveryTotalCount: UILabel!
    @IBOutlet weak var lblVisitDate: UILabel!
    @IBOutlet weak var lblVisits: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTotalCount: UILabel!
    @IBOutlet weak var interactionBarChart: VerticalBarChart!
    @IBOutlet weak var discoveryBarChart: VerticalBarChart!
    @IBOutlet weak var interactionBarChartHeight: NSLayoutConstraint!
    @IBOutlet weak var discoveryBarChartHeight: NSLayoutConstraint!
    @IBOutlet weak var lblNoDataIntereaction: UILabel!
    @IBOutlet weak var lblNoDataDiscovery: UILabel!
    @IBOutlet weak var lblInteraction: UILabel!
    @IBOutlet weak var lblDiscovery: UILabel!
    @IBOutlet weak var lblInteractionAccount: UILabel!
    @IBOutlet weak var lblDiscoveryAccount: UILabel!
    @IBOutlet weak var lblTrendInteraction: UILabel!
    @IBOutlet weak var lblTxtReach: UILabel!
    
    
    static let reuseIdentifier = "BusinessAccountActivityTVC"
    
    var interactionBarChartEntiries = [GraphList]()
    var discoveryBarChartEntiries = [DiscoveryList]()
    var barWidth:CGFloat = (UIDevice.current.userInterfaceIdiom == .pad) ? 60 : 34
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblInteraction.text = MySingleton.shared.selectedLangData.interactions
        self.lblDiscovery.text = MySingleton.shared.selectedLangData.discovery
        self.lblInteractionAccount.text = MySingleton.shared.selectedLangData.discovery
        self.lblTrendInteraction.text = MySingleton.shared.selectedLangData.Trend_Interactions
        self.lblTxtReach.text = MySingleton.shared.selectedLangData.reach
        self.lblDiscoveryAccount.text = MySingleton.shared.selectedLangData.Profile_visits_on_your_account
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(){
     
        interactionBarChart.frame.size.height = interactionBarChartHeight.constant
        interactionBarChart.barWidth = barWidth
        
        discoveryBarChart.frame.size.height = discoveryBarChartHeight.constant
        discoveryBarChart.barWidth = barWidth
        
        interactionBarChart.dataEntries = interactionBarChartEntiries
        discoveryBarChart.dataEntriesDiscovery = discoveryBarChartEntiries
     
    }
    
}
