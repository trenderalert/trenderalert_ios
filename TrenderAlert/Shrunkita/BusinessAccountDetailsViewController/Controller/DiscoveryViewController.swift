//
//  DiscoveryViewController.swift
//  TrenderAlert
//
//  Created by Riken Shah on 21/08/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class DiscoveryViewController: UIViewController {

    @IBOutlet weak var lblReachInfo: UILabel!
    @IBOutlet weak var lblReach: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTitleInfo: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblReachInfo.text = MySingleton.shared.selectedLangData.reach_Info
        self.lblReach.text = MySingleton.shared.selectedLangData.reach
        self.lblTitleInfo.text = MySingleton.shared.selectedLangData.discovery_title_info
        self.lblTitle.text = MySingleton.shared.selectedLangData.What_is_discovery
        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
  
}
