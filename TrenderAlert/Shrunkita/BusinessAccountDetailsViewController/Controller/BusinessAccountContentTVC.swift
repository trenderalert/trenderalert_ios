//
//  BusinessAccountContentTVC.swift
//  TrenderAlert
//
//  Created by HPL on 28/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class BusinessAccountContentTVC: UITableViewCell {

    static let reuseIdentifier = "BusinessAccountContentTVC"
    
    @IBOutlet weak var lblFeedPost: UILabel!
    @IBOutlet var lblTotalTrends: UILabel!
    @IBOutlet weak var feedPostImagesCollectionView: UICollectionView!
    @IBOutlet weak var seeAllBtn: UIButton!
   
    @IBOutlet weak var btnPostTrend: UIButton!
    
    let feedImages = [["Content_Image_1","1,458"],["contest_img_1","5,458"]]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        feedPostImagesCollectionView.dataSource = self
        feedPostImagesCollectionView.delegate   = self
        
        feedPostImagesCollectionView.register(UINib(nibName: FeedPostImagesCVC.reuseIdentifier, bundle: nil), forCellWithReuseIdentifier: FeedPostImagesCVC.reuseIdentifier)
        self.lblFeedPost.text = MySingleton.shared.selectedLangData.feed_posts

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension BusinessAccountContentTVC:UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return feedImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FeedPostImagesCVC.reuseIdentifier, for: indexPath) as! FeedPostImagesCVC
        cell.feedImage.image = UIImage(named: feedImages[indexPath.row][0])
        cell.feedPostCountLbl.text = feedImages[indexPath.row][1]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/3, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
