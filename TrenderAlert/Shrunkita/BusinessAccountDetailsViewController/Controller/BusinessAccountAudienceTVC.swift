//
//  BusinessAccountAudienceTVC.swift
//  TrenderAlert
//
//  Created by HPL on 25/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class BusinessAccountAudienceTVC: UITableViewCell {

    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var citiesBtn: UIButton!
    @IBOutlet weak var countriesBtn: UIButton!
    @IBOutlet weak var allBtn: UIButton!
    @IBOutlet weak var menBtn: UIButton!
    @IBOutlet weak var womenBtn: UIButton!
    @IBOutlet weak var cityCountryBarChartViewHeight: NSLayoutConstraint!
    @IBOutlet weak var menWomenBarChartViewHeight: NSLayoutConstraint!
    
    
    @IBOutlet weak var cityCountryBarChartView: HorizontalBarChart!
    @IBOutlet weak var menWomenBarChartView: HorizontalBarChart!
    
   
    static let reuseIdentifier = "BusinessAccountAudienceTVC"
    
    var cityBarChartEntiries:[CityList] = []
    var countryBarChartEntiries:[CountryList] = []
    var allAgeBarChartEntiries:[AllAgeList] = []
    var menAgeBarChartEntiries:[MenList] = []
    var womenAgeBarChartEntiries:[WomenList] = []
        
    var barHeight:CGFloat = (UIDevice.current.userInterfaceIdiom == .pad) ? 50 : 30
    var rowWidth:CGFloat = 100
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblLocation.text = MySingleton.shared.selectedLangData.top_locations
        self.lblGender.text = MySingleton.shared.selectedLangData.gender
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCell(){
        cityCountryBarChartView.frame.size.height = cityCountryBarChartViewHeight.constant
        cityCountryBarChartView.frame.size.width = rowWidth
        cityCountryBarChartView.barHeight = barHeight
        menWomenBarChartView.frame.size.height = menWomenBarChartViewHeight.constant
        menWomenBarChartView.frame.size.width = rowWidth
        menWomenBarChartView.barHeight = barHeight
        
        cityCountryBarChartView.dataEntriesAudienceLocation = cityBarChartEntiries
        menWomenBarChartView.dataEntriesAudienceAgeAll = allAgeBarChartEntiries
    }
    
    func setupCellWomen()
    {
        menWomenBarChartView.frame.size.height = menWomenBarChartViewHeight.constant
        menWomenBarChartView.frame.size.width = rowWidth
        menWomenBarChartView.barHeight = barHeight
        menWomenBarChartView.dataEntriesAudienceAgeWomen = womenAgeBarChartEntiries
    }
   
    func setupCellMen()
    {
        menWomenBarChartView.frame.size.height = menWomenBarChartViewHeight.constant
        menWomenBarChartView.frame.size.width = rowWidth
        menWomenBarChartView.barHeight = barHeight
        menWomenBarChartView.dataEntriesAudienceAgeMen = menAgeBarChartEntiries
    }
    func setupCellAll()
    {
        menWomenBarChartView.frame.size.height = menWomenBarChartViewHeight.constant
        menWomenBarChartView.frame.size.width = rowWidth
        menWomenBarChartView.barHeight = barHeight
        menWomenBarChartView.dataEntriesAudienceAgeAll = allAgeBarChartEntiries
    }
    func setupCellCity()
    {
        cityCountryBarChartView.frame.size.height = cityCountryBarChartViewHeight.constant
        cityCountryBarChartView.frame.size.width = rowWidth
        cityCountryBarChartView.barHeight = barHeight
        cityCountryBarChartView.dataEntriesAudienceLocation = cityBarChartEntiries
    }
    func setupCellCountry()
    {
        cityCountryBarChartView.frame.size.height = cityCountryBarChartViewHeight.constant
        cityCountryBarChartView.frame.size.width = rowWidth
        cityCountryBarChartView.barHeight = barHeight
        cityCountryBarChartView.dataEntriesAudienceLocationCountry = countryBarChartEntiries
    }
}
