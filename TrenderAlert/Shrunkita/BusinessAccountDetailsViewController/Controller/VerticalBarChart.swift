//
//  BasicBarChart.swift
//  BarChart
//
//  Created by Nguyen Vu Nhat Minh on 19/8/17.
//  Copyright © 2017 Nguyen Vu Nhat Minh. All rights reserved.
//

import UIKit

class VerticalBarChart: UIView {
    
    /// the width of each bar
    var barWidth: CGFloat = 40.0
    
    /// space between each bar
//    var space: CGFloat = 20.0
    var space: CGFloat = 20.0
    /// space at the bottom of the bar to show the title
    private let bottomSpace: CGFloat = 40.0
    
    /// space at the top of each bar to show the value
    private let topSpace: CGFloat = 40.0
    
    /// contain all layers of the chart
    private let mainLayer: CALayer = CALayer()
    
    /// contain mainLayer to support scrolling
    private let scrollView: UIScrollView = UIScrollView()
    
    var titleColor = UIColor.black
    var textColor = UIColor(displayP3Red: 104/255, green: 134/255, blue: 234/255, alpha: 1)
    
    let fontSize = (UIDevice.current.userInterfaceIdiom == .pad) ? 18 : 13
    let fontName = "OpenSans"
    
    
    var dataEntries: [GraphList]? = nil {
        didSet {
            mainLayer.sublayers?.forEach({$0.removeFromSuperlayer()})
            
            if let dataEntries = dataEntries {
                scrollView.contentSize = CGSize(width: (barWidth + space)*CGFloat(dataEntries.count), height: self.frame.size.height)
                mainLayer.frame = CGRect(x: 0, y: 0, width: scrollView.contentSize.width, height: scrollView.contentSize.height)
                
//                drawHorizontalLines()
                var number = [Int]()
                
                for i in 0..<dataEntries.count {
                    number.append(dataEntries[i].value)
                }
                print(number.max())
                
                for i in 0..<dataEntries.count {
                   // number.append(dataEntries[i].value)
                    showEntry(index: i, entry: dataEntries[i],maxNo: number.max() ?? 0)
                }
            }
        }
    }
    
    var dataEntriesDiscovery: [DiscoveryList]? = nil {
        didSet {
            mainLayer.sublayers?.forEach({$0.removeFromSuperlayer()})
            
            if let dataEntries = dataEntriesDiscovery {
                scrollView.contentSize = CGSize(width: (barWidth + space)*CGFloat(dataEntries.count), height: self.frame.size.height)
                mainLayer.frame = CGRect(x: 0, y: 0, width: scrollView.contentSize.width, height: scrollView.contentSize.height)
                
                //                drawHorizontalLines()
                var number = [Int]()
                
                for i in 0..<dataEntries.count {
                    number.append(dataEntries[i].value)
                }
                print(number.max())
                
                for i in 0..<dataEntries.count {
                    // number.append(dataEntries[i].value)
                    showEntryDiscovery(index: i, entry: dataEntries[i],maxNo: number.max() ?? 0)
                }
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        scrollView.layer.addSublayer(mainLayer)
        self.addSubview(scrollView)
    }
    
    override func layoutSubviews() {
        scrollView.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
    }
    
    private func showEntry(index: Int, entry: GraphList,maxNo: Int) {
      
            let value = (entry.value)// + 10
            
        let height: Float = Float(value) / Float(maxNo)
            
        /// Starting x postion of the bar
//        let xPos: CGFloat = space + CGFloat(index) * (barWidth + space)
        let xPos: CGFloat = 5 + CGFloat(index) * (barWidth + space)
        /// Starting y postion of the bar
        let yPos: CGFloat = translateHeightValueToYPosition(value: height)
        
        drawBar(xPos: xPos, yPos: yPos, color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1))
        
        /// Draw text above the bar
        drawTextValue(xPos: xPos - space/2, yPos: yPos - 30, textValue: String(entry.value), color: textColor)
        
        let title = entry.unit!.prefix(3)
        
        /// Draw text below the bar
        drawTitle(xPos: xPos - space/2, yPos: mainLayer.frame.height - bottomSpace + 10, title: String(title), color: titleColor)
    }
    
    private func showEntryDiscovery(index: Int, entry: DiscoveryList,maxNo: Int) {
        
        let value = (entry.value)// + 10
        
        let height: Float = Float(value) / Float(maxNo)
        
        /// Starting x postion of the bar
        //        let xPos: CGFloat = space + CGFloat(index) * (barWidth + space)
        let xPos: CGFloat = 5 + CGFloat(index) * (barWidth + space)
        /// Starting y postion of the bar
        let yPos: CGFloat = translateHeightValueToYPosition(value: height)
        
        drawBar(xPos: xPos, yPos: yPos, color: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1))
        
        /// Draw text above the bar
        drawTextValue(xPos: xPos - space/2, yPos: yPos - 30, textValue: String(entry.value), color: textColor)
        
        let title = entry.unit!.prefix(3)
        
        /// Draw text below the bar
        drawTitle(xPos: xPos - space/2, yPos: mainLayer.frame.height - bottomSpace + 10, title: String(title), color: titleColor)
    }
    
    private func drawBar(xPos: CGFloat, yPos: CGFloat, color: UIColor) {
        let barLayer = CALayer()
        if yPos .isNaN
        {
            return
        }
        barLayer.frame = CGRect(x: xPos, y: yPos, width: barWidth, height: mainLayer.frame.height - bottomSpace - yPos)
        barLayer.backgroundColor = color.cgColor
        mainLayer.addSublayer(barLayer)
    }
    
    private func drawHorizontalLines() {
        self.layer.sublayers?.forEach({
            if $0 is CAShapeLayer {
                $0.removeFromSuperlayer()
            }
        })
        let horizontalLineInfos = [["value": Float(0.0), "dashed": false], ["value": Float(0.5), "dashed": true], ["value": Float(1.0), "dashed": false]]
        for lineInfo in horizontalLineInfos {
            let xPos = CGFloat(0.0)
            let yPos = translateHeightValueToYPosition(value: (lineInfo["value"] as! Float))
            let path = UIBezierPath()
            path.move(to: CGPoint(x: xPos, y: yPos))
            path.addLine(to: CGPoint(x: scrollView.frame.size.width, y: yPos))
            let lineLayer = CAShapeLayer()
            lineLayer.path = path.cgPath
            lineLayer.lineWidth = 0.5
            if lineInfo["dashed"] as! Bool {
                lineLayer.lineDashPattern = [4, 4]
            }
            lineLayer.strokeColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1).cgColor
            self.layer.insertSublayer(lineLayer, at: 0)
        }
    }
    
    private func drawTextValue(xPos: CGFloat, yPos: CGFloat, textValue: String, color: UIColor) {
        let textLayer = CATextLayer()
        if yPos.isNaN
        {
            return
        }
        textLayer.frame = CGRect(x: xPos, y: yPos, width: barWidth+space, height: 22)
        textLayer.foregroundColor = color.cgColor
        textLayer.backgroundColor = UIColor.clear.cgColor
        textLayer.alignmentMode = CATextLayerAlignmentMode.center
        textLayer.contentsScale = UIScreen.main.scale
//        textLayer.font = CTFontCreateWithName(UIFont.systemFont(ofSize: 0).fontName as CFString, 0, nil)
        textLayer.font = UIFont(name: fontName, size: CGFloat(fontSize))
        textLayer.fontSize = CGFloat(fontSize)
        textLayer.string = textValue
        mainLayer.addSublayer(textLayer)
    }
    
    private func drawTitle(xPos: CGFloat, yPos: CGFloat, title: String, color: UIColor) {
        let textLayer = CATextLayer()
        if yPos.isNaN
        {
            return
        }
        textLayer.frame = CGRect(x: xPos, y: yPos, width: barWidth + space, height: 22)
        textLayer.foregroundColor = color.cgColor
        textLayer.backgroundColor = UIColor.clear.cgColor
        
        textLayer.alignmentMode = CATextLayerAlignmentMode.center
        textLayer.contentsScale = UIScreen.main.scale
//        textLayer.font = CTFontCreateWithName(UIFont.systemFont(ofSize: 0).fontName as CFString, 0, nil)
        textLayer.font = UIFont(name: fontName, size: CGFloat(fontSize))
        
        textLayer.fontSize = CGFloat(fontSize)
        textLayer.string = title
        mainLayer.addSublayer(textLayer)
    }
    
    private func translateHeightValueToYPosition(value: Float) -> CGFloat {
        let height: CGFloat = CGFloat(value) * (mainLayer.frame.height - bottomSpace - topSpace)
        return mainLayer.frame.height - bottomSpace - height
    }
}
