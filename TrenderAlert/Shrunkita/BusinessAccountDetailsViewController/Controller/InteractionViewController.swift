//
//  InteractionViewController.swift
//  TrenderAlert
//
//  Created by Riken Shah on 21/08/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class InteractionViewController: UIViewController {

    @IBOutlet weak var lblVIewInfo: UILabel!
    @IBOutlet weak var lblTrendView: UILabel!
    @IBOutlet weak var lblTitleInfo: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblVIewInfo.text = MySingleton.shared.selectedLangData.views_info
        self.lblTrendView.text = MySingleton.shared.selectedLangData.trend_views
        self.lblTitleInfo.text = MySingleton.shared.selectedLangData.interaction_title_info
        self.lblTitle.text = MySingleton.shared.selectedLangData.What_are_Interactions
        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
 

}
