//
//  TrenderTravelVC.swift
//  TrenderAlert
//
//  Created by HPL on 23/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import MapKit
import MBProgressHUD
import SDWebImage
import GoogleMaps
import GooglePlaces
import DropDown
import GoogleMobileAds

class TrenderTravelVC: UIViewController,GMSMapViewDelegate,UITextFieldDelegate {

    @IBOutlet var lblNoData: UILabel!
    @IBOutlet weak var lblTurnOff: UILabel!
    @IBOutlet weak var cnstSearchHeight: NSLayoutConstraint!
    @IBOutlet weak var lblNoDataFound: UILabel!
    @IBOutlet weak var btnCUrrentLocation: UIButton!
    @IBOutlet weak var separaterView: UIView!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var btn100Miles: UIButton!
    @IBOutlet weak var btn50Miles: UIButton!
    @IBOutlet weak var btn20Miles: UIButton!
    @IBOutlet weak var btn5Miles: UIButton!
    @IBOutlet weak var searchTrendTextField: UITextField!
    @IBOutlet weak var searchTrendBtn: UIButton!
    @IBOutlet weak var searchTrendSwitch: UISwitch!
    @IBOutlet weak var trendsTable: UITableView!
    @IBOutlet weak var toggleViewBtn: UIButton!
    @IBOutlet weak var trendCollectionView: UICollectionView!
    @IBOutlet var searchMap: GMSMapView!
    // MARK: - Variables
    var selectedShareFriends = [AnyObject]()
    var trendImagesArray = [UIImage]()
    var refreshControl = UIRefreshControl()
    var trendData = TrendListData()
    var trendImagesCollectionView: UICollectionView!
    var testImageArray = [UIImage]()
    var trendImagesPageControl = UIPageControl()
    var trendCurrentImageLabel = UILabel()
    var radius = Double()
    let picker = UIImagePickerController()
    let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: "Please select your option", message: "", preferredStyle: .actionSheet)
    var userInfoObj = UserInfo()
    var saveFolderTrendId = Int()
    var shareTrendId = Int()
    static var options:[String] = ["All"]
    var isMapViewVisible = true
    
    var latitude : Double!
    var longitude : Double!
    var location_arr : [[String:String]]!
    var miles = "5"
    var gmsCircle : GMSCircle!
    var selfMarker = TrenderAlertMarkerTravel()
    var gmsCamera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 10.0)
    let gmsAttributes : [(Float,Double)] = [(12.0,8046.72),(10.0,32186.9),(11.0,80467.2),(10.0,160934)]
    let locationManager = CLLocationManager()
    var markerArray = [TrenderAlertMarkerTravel]()
    var location = UILabel()
    var totalTrends = UILabel()
    var customInfoWindow = UIView()
    var locationDropdown = DropDown()
    var location_arrDrop = [String]()
    var noOfRecordsPerRequest = 10
    var isListView = true
    var currentCustomAdCnt = 0
    var currentGoogleAdCnt = 0
    var totalTrendsCount = 1
    var isGetAllTrendRequestInProgress = false
    var showBottomLoader = true
    
    var googleAdHeight = (UIDevice.current.userInterfaceIdiom == .pad) ? 90.0 : 50.0
    
    var trendHeaderViewHeight = (UIDevice.current.userInterfaceIdiom == .phone) ? 50 : 70
    
    var trendStatusViewHeight = (UIDevice.current.userInterfaceIdiom == .phone) ? 45 : 65
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblNoData.isHidden = true
        self.gmsCircle = GMSCircle()
        self.radius = 1609*5
        self.lblNoDataFound.isHidden = true
        setupNavigationbar()
        self.lblTurnOff.isHidden = true
        searchMap.delegate = self
        locationManager.delegate = self
        self.searchTrendTextField.delegate = self
        self.searchTrendTextField.addTarget(self, action: #selector(self.textFieldTextChanged(_:)), for: .editingChanged)
        self.lblTurnOff.text = MySingleton.shared.selectedLangData.turn_off_to_type_new_location
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse
        {
            locationManager.startUpdatingLocation()
            searchMap.isMyLocationEnabled = true
            
        }
        else
        {
            locationManager.requestWhenInUseAuthorization()
        }

        setupTextfield()
        setupTableView()
        
        locationDropdown.anchorView = self.separaterView
        locationDropdown.bottomOffset = CGPoint(x: 0, y:(locationDropdown.anchorView?.plainView.bounds.height)!/2)
        locationDropdown.topOffset = CGPoint(x: 0, y:-(locationDropdown.anchorView?.plainView.bounds.height)!/2)
        dropdownSelection()
        self.btn100Miles.makeCornerRadius(radius: 6)
        self.btn50Miles.makeCornerRadius(radius: 6)
        self.btn20Miles.makeCornerRadius(radius: 6)
        self.btn5Miles.makeCornerRadius(radius: 6)
        self.btn5Miles.setTitle(MySingleton.shared.selectedLangData.miles_5, for: .normal)
        self.btn20Miles.setTitle(MySingleton.shared.selectedLangData.miles_20, for: .normal)
        self.btn50Miles.setTitle(MySingleton.shared.selectedLangData.miles_50, for: .normal)
        self.btn100Miles.setTitle(MySingleton.shared.selectedLangData.miles_100, for: .normal)
//        getAllTrends()
        // Do any additional setup after loading the view.
        
        customInfoWindow = Bundle.main.loadNibNamed("MapWindow", owner: self, options: nil)?[0] as! UIView
        self.location = customInfoWindow.viewWithTag(1) as! UILabel
        self.totalTrends = customInfoWindow.viewWithTag(2) as! UILabel
       // self.selfMarker.tracksInfoWindowChanges = true
    }
    
    @objc func refreshAllTrends(sender: UIRefreshControl) {
        getAllTrends(SearchBy: "Place", Latitude: latitude, Longitude: longitude, Miles: "5", SortColumn: "", SortDirection: "", Skip: 0, Take: 15, isLoading: true)
        self.refreshControl.endRefreshing()
    }
    
    
    func setupNavigationbar(){
        let leftBarButton = UIButton()
        leftBarButton.setImage(#imageLiteral(resourceName: "img_MenuBarBtn"), for: .normal)
        leftBarButton.addTarget(self, action: #selector(didTapMenu), for: .touchUpInside)
        let item1 = UIBarButtonItem()
        item1.customView = leftBarButton
        self.navigationItem.leftBarButtonItems = [item1]
        Utilities.setNavigationBarWithTitle(viewController: self, title: MySingleton.shared.selectedLangData.trender_travel)
    }
    
    @objc func didTapMenu(_ sender: UIBarButtonItem) {
        locationManager.stopMonitoringSignificantLocationChanges()
        locationManager.stopUpdatingLocation()
        if self.menuContainerViewController != nil {
            self.menuContainerViewController.toggleLeftSideMenuCompletion(nil)
        }
        
    }
    
    func setupTextfield(){
        searchTrendTextField.attributedPlaceholder = NSAttributedString(string: "\(MySingleton.shared.selectedLangData.search) \(MySingleton.shared.selectedLangData.location)", attributes: [NSAttributedString.Key.foregroundColor:UIColor.white])
    }
    
    func setupTableView(){
        trendsTable.register(UINib(nibName: "TrendTableViewCell", bundle: nil), forCellReuseIdentifier: "TrendTableViewCell")
        trendsTable.register(UINib(nibName:"GoogleAdTVC",bundle:nil),forCellReuseIdentifier:"GoogleAdTVC")
        trendsTable.tableFooterView = UIView()
    }
    
    
    @IBAction func btn_CurrentLocationAction(_ sender: Any)
    {
        self.setupTextfield()
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse
        {
            locationManager.startUpdatingLocation()
            searchMap.isMyLocationEnabled = true
            self.selfMarker.isDraggable = true
        }
        else
        {
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    
    @IBAction func btn_MilesAction(_ sender: UIButton)
    {
        sender.backgroundColor = #colorLiteral(red: 0.9960784314, green: 0.7607843137, blue: 0.06274509804, alpha: 1)
        self.btn20Miles.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9254901961, blue: 0.9294117647, alpha: 1)
        self.btn50Miles.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9254901961, blue: 0.9294117647, alpha: 1)
        self.btn100Miles.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9254901961, blue: 0.9294117647, alpha: 1)
            miles = "5"
         gmsCircle.radius = 1609*5
        self.radius = gmsCircle.radius
        customInfoWindow.removeFromSuperview()
        self.trendData.data = []
        self.totalTrendsCount = 1
        self.currentCustomAdCnt = 0
        self.currentGoogleAdCnt = 0
        getAllTrends(SearchBy: "Place", Latitude: latitude, Longitude: longitude, Miles: miles, SortColumn: "", SortDirection: "", Skip: 0, Take: 15, isLoading: false)
    }
    
    @IBAction func btn_20MilesAction(_ sender: UIButton)
    {
        sender.backgroundColor = #colorLiteral(red: 0.9960784314, green: 0.7607843137, blue: 0.06274509804, alpha: 1)
        self.btn5Miles.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9254901961, blue: 0.9294117647, alpha: 1)
        self.btn50Miles.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9254901961, blue: 0.9294117647, alpha: 1)
        self.btn100Miles.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9254901961, blue: 0.9294117647, alpha: 1)
        miles = "20"
        gmsCircle.radius = 1609*20
        self.radius = gmsCircle.radius
        customInfoWindow.removeFromSuperview()
        self.trendData.data = []
        self.totalTrendsCount = 1
        self.currentCustomAdCnt = 0
        self.currentGoogleAdCnt = 0
        getAllTrends(SearchBy: "Place", Latitude: latitude, Longitude: longitude, Miles: miles, SortColumn: "", SortDirection: "", Skip: 0, Take: 15, isLoading: false)
    }
    
    @IBAction func btn_50MilesAction(_ sender: UIButton)
    {
        sender.backgroundColor = #colorLiteral(red: 0.9960784314, green: 0.7607843137, blue: 0.06274509804, alpha: 1)
        self.btn5Miles.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9254901961, blue: 0.9294117647, alpha: 1)
        self.btn20Miles.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9254901961, blue: 0.9294117647, alpha: 1)
        self.btn100Miles.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9254901961, blue: 0.9294117647, alpha: 1)
        miles = "50"
        gmsCircle.radius = 1609*50
        self.radius = gmsCircle.radius
        customInfoWindow.removeFromSuperview()
        self.trendData.data = []
        self.totalTrendsCount = 1
        self.currentCustomAdCnt = 0
        self.currentGoogleAdCnt = 0
        getAllTrends(SearchBy: "Place", Latitude: latitude, Longitude: longitude, Miles: miles, SortColumn: "", SortDirection: "", Skip: 0, Take: 15, isLoading: false)
    }
    
    @IBAction func btn_30MilesAction(_ sender: UIButton)
    {
        sender.backgroundColor = #colorLiteral(red: 0.9960784314, green: 0.7607843137, blue: 0.06274509804, alpha: 1)
        self.btn5Miles.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9254901961, blue: 0.9294117647, alpha: 1)
        self.btn50Miles.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9254901961, blue: 0.9294117647, alpha: 1)
        self.btn20Miles.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.9254901961, blue: 0.9294117647, alpha: 1)
        miles = "100"
        gmsCircle.radius = 1609*100
        self.radius = gmsCircle.radius
        customInfoWindow.removeFromSuperview()
        self.trendData.data = []
        self.totalTrendsCount = 1
        self.currentCustomAdCnt = 0
        self.currentGoogleAdCnt = 0
        getAllTrends(SearchBy: "Place", Latitude: latitude, Longitude: longitude, Miles: miles, SortColumn: "", SortDirection: "", Skip: 0, Take: 15, isLoading: false)
    }
    
    
    func getAllTrends(SearchBy:String,
        Latitude:Double,
        Longitude:Double,
        Miles:String,
        SortColumn: String,
        SortDirection: String,
        Skip:Int,
        Take:Int,isLoading:Bool) {
        
        if ((trendData.data.count + self.currentCustomAdCnt + self.currentGoogleAdCnt) >= totalTrendsCount) || isGetAllTrendRequestInProgress{
            if isGetAllTrendRequestInProgress{
                return
            }
            self.showBottomLoader = false
            self.trendsTable.tableFooterView?.isHidden = true
            self.isGetAllTrendRequestInProgress = false
            return
        }
        
        let params = [  "SearchBy":SearchBy,
                        "Latitude":Latitude,
                        "Longitude":Longitude,
                        "Miles":Miles,
                        "SortColumn": "",
                        "SortDirection": "",
                        "Skip":"",
        "Take":Take] as [String:Any]
        print(params)
        WebServices().callUserService(service: .trenderTravel, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: isLoading, isHeader: true, CallMethod: .post) { (serviceResponse, responseData) in
            print("Get All Trends --- \(serviceResponse)")
            if(serviceResponse["Status"] as! Bool == true)
            {
                DispatchQueue.main.async {

                let totalTrends = String(serviceResponse["TotalRecords"] as! Int)
                self.totalTrends.text = "Total Trends = \(totalTrends)"
         
                for (index,eachPrayer) in self.markerArray.enumerated()
                {
                    eachPrayer.map = nil
                    eachPrayer.tag = nil
                }
                self.markerArray.removeAll()
                self.trendImagesArray.removeAll()
                 self.trendData.data.removeAll()
                
                let temp = WebServices().decodeDataToClass(data: responseData, decodeClass: TrendListData.self)!
                self.trendData.data += temp.data
                    
//                    if self.trendData.data.count == 0
//                    {
//                        if self.isMapViewVisible
//                        {
//                            self.lblNoData.isHidden = true
//                        }
//                        self.lblNoData.isHidden = false
//                    }
//                    else
//                    {
//                        self.lblNoData.isHidden = true
//                    }
                
                self.currentCustomAdCnt += serviceResponse["CurrentCustomAdCount"] as! Int
                self.currentGoogleAdCnt += serviceResponse["CurrentGoogleAdCount"] as! Int
                
                self.totalTrendsCount = (self.currentCustomAdCnt + self.currentGoogleAdCnt) + (serviceResponse["TotalRecords"] as! Int)
                
                self.isGetAllTrendRequestInProgress = false
                
                self.trendsTable.reloadData()

                        for (index,eachPrayer) in self.trendData.data.enumerated()
                        {
                            let trendMarker = TrenderAlertMarkerTravel()
                            trendMarker.isDraggable = false
                            trendMarker.position = CLLocationCoordinate2D(latitude: (eachPrayer.TrendLatitude), longitude: (eachPrayer.TrendLongitude))
                            trendMarker.icon = self.drawImageWithProfilePic(index: index,pp: #imageLiteral(resourceName: "img_Jhon"), image: #imageLiteral(resourceName: "MapWithProfile"))
                           // trendMarker.icon = #imageLiteral(resourceName: "MapWithProfile")
                            trendMarker.map = self.searchMap
                            trendMarker.tag = index
                            self.markerArray.append(trendMarker)
                            
                        }
                
                if self.isMapViewVisible != true
                {
                    if self.trendData.data.count != 0
                    {
                        self.lblNoDataFound.isHidden = true
                        self.trendsTable.delegate = self
                        self.trendsTable.dataSource = self
                        self.trendsTable.reloadData()
                    }
                    else
                    {
                        self.lblNoDataFound.isHidden = false
                        self.trendsTable.reloadData()
                    }
                }
            }
                
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
            
        }
    }
    
    @objc func btnLearnMoreAction(_ sender : UIButton)
    {
        if trendData.data[sender.tag].IsCustomAd{
            //            if let url = URL(string: "https://stackoverflow.com") {
            
            if var url = trendData.data[sender.tag].AdUrl {
                if !(url.contains("http"))
                {
                    url = url.replacingOccurrences(of: "www.", with: "https://")
                }
                
                if UIApplication.shared.canOpenURL(URL(string: url)!)
                {
                    UIApplication.shared.open(URL(string: url)!)
                }
                //                UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
            }
        }
    }
    
    @objc func btnFullScrProfileImgAction(_ sender : UIButton)
    {
        let zoomImageView = ZoomImageVC(nibName: "ZoomImageVC", bundle: nil)
        if trendData.data[sender.tag].UserProfileImage != nil
        {
            zoomImageView.imageUrl = trendData.data[sender.tag].UserProfileImage!
        }
        self.navigationController?.pushViewController(zoomImageView, animated: true)
    }
    
    @objc func tapActionForLabel(_ recognizer : UITapGestureRecognizer)
    {
        if recognizer.state == UIGestureRecognizer.State.ended {
            print(recognizer.view!.tag)
            
            if trendData.data[recognizer.view!.tag].IsTrendShared == false{
                if trendData.data[recognizer.view!.tag].IsCustomAd{
                }
                else{
                    if (trendData.data[recognizer.view!.tag].AreYouFollowingTrender == 0 || trendData.data[recognizer.view!.tag].AreYouFollowingTrender == 1 || trendData.data[recognizer.view!.tag].AreYouFollowingTrender == 2)
                    {
                        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
                        let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
                        profileVC.userId = trendData.data[recognizer.view!.tag].UserProfileId
                        self.navigationController?.pushViewController(profileVC, animated: true)
                    }
                    else
                    {
//                        (self.tabBarController!).selectedIndex = 4
                    }
                }
            }
            else{
                let firstRange = ((recognizer.view! as! UILabel).text as! NSString).range(of: "\(trendData.data[recognizer.view!.tag].UserProfileFirstName!) \(String(describing: trendData.data[recognizer.view!.tag].UserProfileLastName!))")
                
                let secondRange = ((recognizer.view! as! UILabel).text as! NSString).range(of: "\(trendData.data[recognizer.view!.tag].TrendOwner!)")
                
                if recognizer.didTapAttributedTextInLabel(label: (recognizer.view! as! UILabel), inRange: firstRange) {
                    print(firstRange)
                    if Int(MySingleton.shared.loginObject.UserID) == trendData.data[recognizer.view!.tag].UserProfileId
                    {
//                        (self.tabBarController!).selectedIndex = 4
                    }
                    else
                    {
                        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
                        let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
                        profileVC.userId = trendData.data[recognizer.view!.tag].UserProfileId
                        self.navigationController?.pushViewController(profileVC, animated: true)
                    }
                    
                } else if recognizer.didTapAttributedTextInLabel(label: (recognizer.view! as! UILabel), inRange: secondRange) {
                    print(secondRange)
                    if Int(MySingleton.shared.loginObject.UserID) != trendData.data[recognizer.view!.tag].TrendOwnerId!
                    {
                        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
                        let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
                        profileVC.userId = trendData.data[recognizer.view!.tag].TrendOwnerId!
                        self.navigationController?.pushViewController(profileVC, animated: true)
                    }
                    else
                    {
//                        (self.tabBarController!).selectedIndex = 4
                    }
                    
                } else {
                    print("Tapped none")
                }
            }
            
        }
    }
    
    // MARK: - Config Trend Table View Cell
    func configTrendTableViewCell(tableView: UITableView, withIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "TrendTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! TrendTableViewCell
        trendImagesCollectionView = cell.trendImageCollectionView
        
        // Register Collection View Cells
        registerCollectionViewCells()
        cell.trendCurrentImageLabel.layer.cornerRadius = 3.0
        // Testing
        var calculatedWidth = CGFloat()
        var calculatedHeight = CGFloat()
        calculatedWidth = self.view.frame.size.width
        cell.profileImageView.layer.cornerRadius = 0.5 * cell.profileImageView.bounds.size.width
        cell.profileImageView.clipsToBounds = true
        
        calculatedHeight = (CGFloat(trendData.data[indexPath.row].ImageList![0].ImageHeight ?? 250) * calculatedWidth) / (CGFloat(trendData.data[indexPath.row].ImageList![0].ImageWidth ?? 250))
        
        if trendData.data[indexPath.row].IsTrendShared == false{
            
            if trendData.data[indexPath.row].IsCustomAd{ // Custom Ad
                //            tableView.rowHeight = calculatedHeight
                cell.btnLearnMore.isHidden = false
                cell.btnLearnMore.tag = indexPath.row
                cell.btnLearnMore.addTarget(self, action: #selector(btnLearnMoreAction(_:)), for: .touchUpInside)
                cell.profileImageView.isHidden = true
                cell.shareDescLabel.text = ""
                cell.cnstrntShareDescHeight.constant = 0
                cell.cnstrTrendHeaderViewHeight.constant = 0
                cell.cnstrTrendStatusViewHeight.constant = 0
                cell.cnstrTrendViewCommentViewHeight.isActive = true
                cell.trendViewCommentsView.isHidden = true
                cell.trendStatusView.isHidden = true
                cell.clipsToBounds = true
                //
                cell.videoLayer.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: calculatedHeight)
                //
            }else{
                //             tableView.rowHeight = 286 + calculatedHeight - 61
                cell.btnLearnMore.isHidden = true
                cell.profileImageView.isHidden = false
                cell.profileImageView.layer.cornerRadius = 0.5 * cell.profileImageView.bounds.size.width
                cell.profileImageView.clipsToBounds = true
                
                cell.shareDescLabel.text = ""
                cell.cnstrntShareDescHeight.constant = 0
                cell.cnstrTrendViewCommentViewHeight.isActive = false
                cell.trendViewCommentsView.isHidden = false
                cell.trendStatusView.isHidden = false
                cell.profileNameLabel.text = "\(trendData.data[indexPath.row].UserProfileFirstName ?? "") \(trendData.data[indexPath.row].UserProfileLastName ?? "")"
                cell.profileNameLabel.textColor = UIColor.darkText
                cell.profileNameLabel.tag = indexPath.row
                cell.profileNameTap.numberOfTapsRequired = 1
                cell.profileNameLabel.isUserInteractionEnabled = true
                cell.profileNameTap = UITapGestureRecognizer(target: self, action: #selector(tapActionForLabel(_:)))
                cell.profileNameLabel.addGestureRecognizer(cell.profileNameTap)
                cell.btnFullScrProfileImg.tag = indexPath.row
                cell.btnFullScrProfileImg.addTarget(self, action: #selector(btnFullScrProfileImgAction(_:)), for: .touchUpInside)
                
                cell.locationLabel.text = trendData.data[indexPath.row].TrendLocation ?? ""
                cell.cnstrTrendHeaderViewHeight.constant = CGFloat(trendHeaderViewHeight)
                cell.cnstrTrendStatusViewHeight.constant = CGFloat(trendStatusViewHeight)
                
                if trendData.data[indexPath.row].UserProfileImage != nil {
                    cell.profileImageView.sd_setImage(with: URL(string: trendData.data[indexPath.row].UserProfileImage!), placeholderImage: #imageLiteral(resourceName: "img_selectProfilePic"))
                }
                else
                {
                    cell.profileImageView.image = #imageLiteral(resourceName: "img_selectProfilePic")
                }
                
            }
            
        }
        else
        {
            cell.btnLearnMore.isHidden = true
            cell.profileImageView.isHidden = false
            cell.profileImageView.layer.cornerRadius = 0.5 * cell.profileImageView.bounds.size.width
            cell.profileImageView.clipsToBounds = true
            
            tableView.rowHeight = 286 + calculatedHeight - 28
            cell.cnstrntShareDescHeight.constant = 33
            cell.trendViewCommentsView.isHidden = false
            cell.cnstrTrendViewCommentViewHeight.isActive = false
            cell.trendStatusView.isHidden = false
            cell.shareDescLabel.text = trendData.data[indexPath.row].SharedTrendDescription ?? ""
            
            cell.btnFullScrProfileImg.tag = indexPath.row
            cell.btnFullScrProfileImg.addTarget(self, action: #selector(btnFullScrProfileImgAction(_:)), for: .touchUpInside)
            
            cell.profileNameLabel.tag = indexPath.row
            cell.profileNameTap.numberOfTapsRequired = 1
            cell.profileNameLabel.isUserInteractionEnabled = true
            cell.profileNameTap = UITapGestureRecognizer(target: self, action: #selector(tapActionForLabel(_:)))
            cell.profileNameLabel.addGestureRecognizer(cell.profileNameTap)
            
            let color1 = UIColor.darkText
            // create the attributed colour
            let attributedStringColor = [NSAttributedString.Key.foregroundColor : color1];
            // create the attributed string
            let attributedString = NSMutableAttributedString(string: "\(String(describing: trendData.data[indexPath.row].UserProfileFirstName)) \(String(describing: trendData.data[indexPath.row].UserProfileLastName))", attributes: attributedStringColor)
            // Set the label
            
            
            
            // create the attributed colour
            let attrs1 = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 17), NSAttributedString.Key.foregroundColor : UIColor.black]
            //            let attributedStringColor2 = [NSAttributedString.Key.foregroundColor : color2];
            let attributedString2 = NSMutableAttributedString(string: " shared ", attributes: attrs1)
            attributedString.append(attributedString2)
            let color3 = UIColor.darkText
            let attributedStringColor3 = [NSAttributedString.Key.foregroundColor : color3];
            let attributedString3 = NSMutableAttributedString(string: "\(trendData.data[indexPath.row].TrendOwner ?? "")'s post", attributes: attributedStringColor3)
            attributedString.append(attributedString3)
            cell.profileNameLabel.attributedText = attributedString
            
            cell.locationLabel.text = ""
            cell.cnstrTrendHeaderViewHeight.constant = CGFloat(trendHeaderViewHeight)
            cell.cnstrTrendStatusViewHeight.constant = CGFloat(trendStatusViewHeight)
            
            if trendData.data[indexPath.row].UserProfileImage != nil {
                cell.profileImageView.sd_setImage(with: URL(string: trendData.data[indexPath.row].UserProfileImage!), placeholderImage: #imageLiteral(resourceName: "img_selectProfilePic"))
            }
            else
            {
                cell.profileImageView.image = #imageLiteral(resourceName: "img_selectProfilePic")
            }
        }
        
        if !trendData.data[indexPath.row].IsCustomAd{
            cell.videoLayer.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: calculatedHeight+20)
        }
        
        
        // Data
//        cell.trendExpiryLabel.text = "Expires in \(trendData.data[indexPath.row].RemainingExpiryTime ?? "")"
//        cell.trendAddedLabel.text = "\(trendData.data[indexPath.row].TimeAgo ?? "") Ago"
        cell.trendPostTitleLabel.text = trendData.data[indexPath.row].Title
        cell.postCommentLabel.text = trendData.data[indexPath.row].Description
        cell.viewAllCommentsButton.setTitle("View All \(trendData.data[indexPath.row].CommentCount ?? "") Comment", for: .normal)
        cell.commentsCountLabel.text = "\(trendData.data[indexPath.row].CommentCount ?? "")"
        cell.likesCountLabel.text = "\(trendData.data[indexPath.row].TrendLikesCount)"
        cell.shareCountLabel.text = "\(trendData.data[indexPath.row].TrendShareCount)"
        cell.viewsCountLabel.text = "\(trendData.data[indexPath.row].TrendViewCount ?? "")"
        
        
//        cell.txtAddComment.attributedPlaceholder = NSAttributedString(string: "Write a comment...",
//                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.baseColor])
        
        
        if trendData.data[indexPath.row].IsTrenderOnline
        {
            cell.statusImageView.image = #imageLiteral(resourceName: "img_Active")
        }
        else
        {
            cell.statusImageView.image = #imageLiteral(resourceName: "img_inactive")
        }
        
        if trendData.data[indexPath.row].AreYouFollowingTrender == 0{
            cell.followStatusButton.isHidden = false
            cell.followStatusButton.setTitle(MySingleton.shared.selectedLangData.following, for: .normal)
            cell.followStatusButton.setTitleColor(UIColor.baseColor, for: .normal)
            cell.followStatusButton.tag = indexPath.row
            cell.followStatusButton.addTarget(self, action: #selector(FollowButtonAction(_:)), for: .touchUpInside)
            cell.otherUserProfileButton.isHidden = true
            //            cell.otherUserProfileButton.tag = indexPath.row
            //            cell.otherUserProfileButton.addTarget(self, action: #selector(OtherProfileAction(_:)), for: .touchUpInside)
            
        }
        else if trendData.data[indexPath.row].AreYouFollowingTrender == 1{
            cell.followStatusButton.isHidden = false
            cell.followStatusButton.setTitle(MySingleton.shared.selectedLangData.follow, for: .normal)
            cell.followStatusButton.setTitleColor(UIColor.lightGray, for: .normal)
            cell.followStatusButton.tag = indexPath.row
            cell.followStatusButton.addTarget(self, action: #selector(FollowButtonAction(_:)), for: .touchUpInside)
            cell.otherUserProfileButton.isHidden = true
            //            cell.otherUserProfileButton.tag = indexPath.row
            //            cell.otherUserProfileButton.addTarget(self, action: #selector(OtherProfileAction(_:)), for: .touchUpInside)
        }
        else if trendData.data[indexPath.row].AreYouFollowingTrender == 2{
            cell.followStatusButton.isHidden = false
            cell.followStatusButton.setTitle(MySingleton.shared.selectedLangData.Requested, for: .normal)
            cell.followStatusButton.setTitleColor(UIColor.lightGray, for: .normal)
            cell.followStatusButton.tag = indexPath.row
            cell.followStatusButton.addTarget(self, action: #selector(FollowButtonAction(_:)), for: .touchUpInside)
            cell.otherUserProfileButton.isHidden = true
            //            cell.otherUserProfileButton.tag = indexPath.row
            //            cell.otherUserProfileButton.addTarget(self, action: #selector(OtherProfileAction(_:)), for: .touchUpInside)
        }
        else
        {
            cell.followStatusButton.isHidden = true
            cell.otherUserProfileButton.isHidden = true
            //            cell.otherUserProfileButton.tag = indexPath.row
            //            cell.otherUserProfileButton.addTarget(self, action: #selector(OtherProfileAction(_:)), for: .touchUpInside)
        }
        
        if trendData.data[indexPath.row].EnableTrendLike == true {
            cell.likeButton.isHidden = false
        }
        else {
            cell.likeButton.isHidden = true
        }
        if trendData.data[indexPath.row].IsTrendLike == true {
            cell.trendLikesImageView.image = #imageLiteral(resourceName: "Like_clk")
        }
        else {
            cell.trendLikesImageView.image = #imageLiteral(resourceName: "Like")
        }
        cell.likeButton.accessibilityIdentifier = String(indexPath.row)
        cell.likeButton.tag = trendData.data[indexPath.row].TrendId
        cell.likeButton.addTarget(self, action: #selector(didTapLikeButton), for: .touchUpInside)
        cell.trendImageCollectionView.tag = indexPath.row
        cell.trendImageCollectionView.delegate = self
        cell.trendImageCollectionView.dataSource = self
        cell.trendImagesPageControl.numberOfPages = trendData.data[indexPath.row].ImageList!.count //trendImagesCollectionArray[indexPath.row].count //trendData.data[indexPath.row].ImageList.count
        cell.trendImagesPageControl.hidesForSinglePage = true
        trendImagesPageControl = cell.trendImagesPageControl
        trendCurrentImageLabel = cell.trendCurrentImageLabel
        cell.trendImageCollectionView.reloadData()
        cell.trendImageCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: true)
        
        
        cell.trendCurrentImageLabel.text = "1/\(trendData.data[indexPath.row].ImageList!.count)"
        
        cell.addCommentButton.tag = indexPath.row
        cell.addCommentButton.addTarget(self, action: #selector(self.addCommentAction(_:)), for: .touchUpInside)
        
        if(trendData.data[indexPath.row].ImageList!.count > 1)
        {
            cell.trendCurrentImageLabel.isHidden = false
        }
        else
        {
            cell.trendCurrentImageLabel.isHidden = true
        }
        
        if trendData.data[indexPath.row].ImageList![0].Media == "VIDEO"{
            cell.videoURL = trendData.data[indexPath.row].ImageList![0].VideoUrl
            cell.cnstrntVideoWidth.constant = cell.trendStatusView.frame.size.width/4
            cell.trendViewsView.isHidden = false
            cell.imgVideoIcon.isHidden = false
        }
        else
        {
            cell.videoURL = nil
            cell.cnstrntVideoWidth.constant = 0
            cell.trendViewsView.isHidden = true
            cell.imgVideoIcon.isHidden = true
        }
        
        
        cell.trendMoreButton.tag = indexPath.row
        cell.trendMoreButton.addTarget(self, action: #selector(trendMoreButtonAction(_:)), for: .touchUpInside)
        
        cell.shareCountButton.tag = indexPath.row
        cell.shareCountButton.addTarget(self, action: #selector(shareCountButtonAction(_:)), for: .touchUpInside)
        
        cell.btnShareTrend.tag = indexPath.row
        cell.btnShareTrend.addTarget(self, action: #selector(btnShareTrendAction(_:)), for: .touchUpInside)
        
        cell.likeCountButton.tag = indexPath.row
        cell.likeCountButton.addTarget(self, action: #selector(likeCountButtonAction(_:)), for: .touchUpInside)
        cell.viewsCountButton.tag = indexPath.row
        cell.viewsCountButton.addTarget(self, action: #selector(viewsCountButtonAction(_:)), for: .touchUpInside)
        
        cell.txtAddComment.delegate = self
        cell.txtAddComment.returnKeyType = .send
        cell.txtAddComment.tag = indexPath.row
        if MySingleton.shared.loginObject.ProfilePic != ""
        {
            cell.profileButton.layer.cornerRadius = 0.5 * cell.profileButton.bounds.size.width
            cell.profileButton.clipsToBounds = true
            cell.profileButton.sd_setImage(with: URL(string: MySingleton.shared.loginObject.ProfilePic!), for: .normal)
        }
        else
        {
            cell.profileButton.setImage(#imageLiteral(resourceName: "img_selectProfilePic"), for: .normal)
        }
        
        cell.selectedLang(trendData: self.trendData,index:indexPath.row)
        
        return cell
    }
    
    @objc func shareCountButtonAction(_ sender : UIButton)
    {
        let dataToSend = ["TrendId" : self.trendData.data[sender.tag].TrendId, "Type": "ShareCount"] as [String : Any]
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let likeListVC = storyboard.instantiateViewController(withIdentifier:"LikesListViewController") as! LikesListViewController
        
        likeListVC.trendData = dataToSend
        //        likeListVC.trendId = self.trendData.data[sender.tag].TrendId
        self.navigationController?.pushViewController(likeListVC, animated: true)
    }
    
    @objc func btnShareTrendAction(_ sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "ShareInternalViewController") as! ShareInternalViewController
        destination.trendId = self.trendData.data[sender.tag].TrendId
        destination.friendsPopupDelegate = self
        let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height)
        self.navigationController?.present(pop, animated: true, completion: nil)
    }
    
    @IBAction func toggleMapNTrendView(_ sender: UIButton) {
        
        isMapViewVisible = !isMapViewVisible
        
        if isMapViewVisible{
            btnCUrrentLocation.isHidden = false
            searchMap.isHidden = false
            trendsTable.isHidden = true
            trendsTable.delegate = nil
            trendsTable.dataSource = nil
            self.lblNoData.isHidden = true
            toggleViewBtn.setImage(UIImage(named: "img_travelTrendList"), for: .normal)
            self.cnstSearchHeight.constant = 50
        }else{
            btnCUrrentLocation.isHidden = true
            trendsTable.isHidden = false
            searchMap.isHidden = true
            trendsTable.delegate = self
            trendsTable.dataSource = self
            trendsTable.reloadData()
            self.getAllTrends(SearchBy: "Place", Latitude: self.latitude, Longitude: self.longitude, Miles: self.miles, SortColumn: "", SortDirection: "", Skip: 0, Take: 15, isLoading: true)
            toggleViewBtn.setImage(UIImage(named: "img_TravelLocation"), for: .normal)
            self.cnstSearchHeight.constant = 0
        }
    }
   
    @IBAction func btnSwitchAction(_ sender: UISwitch)
    {
        if sender.isOn
        {
           // searchTrendSwitch.isOn = true
            self.viewSearch.isHidden = true
            self.lblTurnOff.isHidden = false
            if CLLocationManager.authorizationStatus() == .authorizedWhenInUse
            {
                locationManager.startUpdatingLocation()
                locationManager.startMonitoringSignificantLocationChanges()
                searchMap.isMyLocationEnabled = true
                
            }
            else
            {
                locationManager.requestWhenInUseAuthorization()
            }
            
            self.selfMarker.isDraggable = false
        }
        else
        {
            self.viewSearch.isHidden = false
            self.lblTurnOff.isHidden = true
          //  searchTrendSwitch.isOn = false
            locationManager.stopMonitoringSignificantLocationChanges()
            locationManager.stopUpdatingLocation()
            self.selfMarker.isDraggable = true
        }
        searchTrendSwitch.isOn = sender.isOn
        print(searchTrendSwitch.isOn)
        print(sender.isOn)
        self.trendData.data = []
        self.totalTrendsCount = 1
        self.currentCustomAdCnt = 0
        self.currentGoogleAdCnt = 0
    }
}

extension TrenderTravelVC:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trendData.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if trendData.data[indexPath.row].IsGoogleAd{
            let cell = tableView.dequeueReusableCell(withIdentifier: "GoogleAdTVC", for: indexPath) as! GoogleAdTVC
            cell.bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
            cell.bannerView.rootViewController = self
            cell.bannerView.load(GADRequest())
            return cell
        }else{
            return configTrendTableViewCell(tableView: tableView, withIndexPath: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if trendData.data[indexPath.row].IsGoogleAd {
            return CGFloat(googleAdHeight)
        }else {
            var calculatedWidth = CGFloat()
            var calculatedHeight = CGFloat()
            calculatedWidth = self.view.frame.size.width
            
            calculatedHeight = ((CGFloat(trendData.data[indexPath.row].ImageList![0].ImageHeight ?? 250)) * calculatedWidth) / (CGFloat(trendData.data[indexPath.row].ImageList![0].ImageWidth ?? 250))
            if trendData.data[indexPath.row].IsTrendShared == false{
                
                if trendData.data[indexPath.row].IsCustomAd{ // Custom Ad
                    return calculatedHeight
                }
                return 286 + calculatedHeight - 61
            }else{
                return 286 + calculatedHeight - 28
            }
        }
    }
    
    func addComment(index:Int,text:String)
    {
        self.view.endEditing(true)
        var param = [String : Any]()
        
        param = ["TrendID":trendData.data[index].TrendId,"CommentId":"0","CommentText":text ,"IsImage":"false",
                 "ImagePath":[],"gifID":"","TaggedUsers":[]] as [String : Any]
        
        WebServices().callUserService1(service: UserServices.addComment, urlParameter: "", parameters: param as [String : AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
            if(serviceResponse["Status"] as! Bool == true)
            {
                 DispatchQueue.main.async {
                let cell = self.trendsTable.cellForRow(at: IndexPath(row: index, section: 0))as? TrendTableViewCell
                self.trendData.data[index].CommentCount = String(Int(self.trendData.data[index].CommentCount ?? "")! + 1)
                    cell!.commentsCountLabel.text = "\(self.trendData.data[index].CommentCount ?? "")"
                cell!.txtAddComment.text = ""
                }
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        })
    }
    
    @objc func addCommentAction(_ sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Comment", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "CommentViewController") as! CommentViewController
        destination.trendId = trendData.data[sender.tag].TrendId
        destination.trendIndex = sender.tag
        destination.delegate = self
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @objc func viewsCountButtonAction(_ sender : UIButton)
    {
        let dataToSend = ["TrendId" : self.trendData.data[sender.tag].TrendId, "Type": "ViewsCount"] as [String : Any]
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let profileVC = storyboard.instantiateViewController(withIdentifier: "LikesListViewController") as! LikesListViewController
        profileVC.trendData = dataToSend
        self.navigationController?.pushViewController(profileVC, animated: true)
        //        self.performSegue(withIdentifier: "DashboardToLikesCount", sender: dataToSend)
    }
    
    @objc func OtherProfileAction(_ sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
        let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
        profileVC.userId = trendData.data[sender.tag].UserProfileId
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    @objc func FollowButtonAction(_ sender : UIButton)
    {
        if sender.currentTitle == MySingleton.shared.selectedLangData.follow{
            //follow api
            callApiForFavouriteUnfavourite(urlParam: String(trendData.data[sender.tag].UserProfileId), service: .followUser, index: sender.tag)
        }
        else if sender.currentTitle == MySingleton.shared.selectedLangData.following
        {
            //unfollow api
            callApiForFavouriteUnfavourite(urlParam: String(trendData.data[sender.tag].UserProfileId), service: .unfollowUser, index: sender.tag)
        }
    }
    
    @objc func likeCountButtonAction(_ sender : UIButton)
    {
        
        
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let likeListVC = storyboard.instantiateViewController(withIdentifier:"LikesListViewController") as! LikesListViewController
        let dataToSend = ["TrendId" : self.trendData.data[sender.tag].TrendId, "Type": "LikesCount"] as [String : Any]
        likeListVC.trendData = dataToSend
        //        likeListVC.trendId = self.trendData.data[sender.tag].TrendId
        self.navigationController?.pushViewController(likeListVC, animated: true)
 
    }
    
 
    
    func callApiForFavouriteUnfavourite(urlParam : String, service : UserServices, index : Int){
        WebServices().callUserService(service: service, urlParameter: urlParam, parameters: nil, isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
            if service == .markTrendFavourite{
                self.trendData.data[index].IsTrendFavourite = true
            }
            else if service == .markTrendUnfavourite
            {
                self.trendData.data[index].IsTrendFavourite = false
            }
            else if service == .followUser{
                self.trendData.data[index].AreYouFollowingTrender = 2
                let cell = self.trendsTable.cellForRow(at: IndexPath(row: index, section: 0))as! TrendTableViewCell
                cell.followStatusButton.setTitle(MySingleton.shared.selectedLangData.Requested, for: .normal)
                cell.followStatusButton.setTitleColor(UIColor.lightGray, for: .normal)
                
            }
            else if service == .unfollowUser{
                self.trendData.data[index].AreYouFollowingTrender = 1
                let cell = self.trendsTable.cellForRow(at: IndexPath(row: index, section: 0))as! TrendTableViewCell
                cell.followStatusButton.setTitle(MySingleton.shared.selectedLangData.follow, for: .normal)
                cell.followStatusButton.setTitleColor(UIColor.lightGray, for: .normal)
            }
            else if service == .deleteTrend{
                //                self.getAllTrends()
                self.trendData.data.remove(at: index)
                // self.trendImagesArray.remove(at: index)
                self.trendsTable.reloadData()
            }
            else if service == .setTrendPushNotificationStatus
            {
                if(serviceResponse["Status"] as! Bool == true)
                {
                    if (serviceResponse["Message"] as! String).contains("disabled")
                    {
                        self.trendData.data[index].IsPushNotificationEnabled = false
                    }
                    else
                    {
                        self.trendData.data[index].IsPushNotificationEnabled = true
                    }
                }
                else
                {
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }
            
        })
    }
    
    @objc func trendMoreButtonAction(_ sender : UIButton)
    {
        let cell = trendsTable.cellForRow(at: IndexPath(row: sender.tag, section: 0))as! TrendTableViewCell
        
        cell.trendDropDown.anchorView = sender
        cell.trendDropDown.bottomOffset = CGPoint(x: 0, y:(cell.trendDropDown.anchorView?.plainView.bounds.height)!)
        cell.trendDropDown.topOffset = CGPoint(x: 0, y:-(cell.trendDropDown.anchorView?.plainView.bounds.height)!)
        
        cell.trendDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            print(sender.tag)
            
            switch index{
            case 0:
                let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
                let destination = storyboard.instantiateViewController(withIdentifier: "ShareInternalViewController") as! ShareInternalViewController
                destination.trendId = self.trendData.data[sender.tag].TrendId
                let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height)
                self.navigationController?.present(pop, animated: true, completion: nil)
                
            case 2:
                if item == "Favourite"{
                    self.callApiForFavouriteUnfavourite(urlParam: String(self.trendData.data[sender.tag].TrendId), service: .markTrendFavourite, index: sender.tag)
                }
                else
                {
                    self.callApiForFavouriteUnfavourite(urlParam: String(self.trendData.data[sender.tag].TrendId), service: .markTrendUnfavourite, index: sender.tag)
                }
                
            case 3:
                self.callApiForFavouriteUnfavourite(urlParam: String(self.trendData.data[sender.tag].TrendId), service: .setTrendPushNotificationStatus, index: sender.tag)
                
            case 4:
                let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
                let destination = storyboard.instantiateViewController(withIdentifier: "ReportTrendViewController") as! ReportTrendViewController
                let pop = PopUpViewController(destination,withHeight: 270.0)
                self.navigationController?.present(pop, animated: true, completion: nil)
                ////                self.navigationController?.present(destination, animated: true, completion: nil)
                //                self.present(destination, animated: true, completion: nil)
                
            case 5:
                let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
                let destination = storyboard.instantiateViewController(withIdentifier: "SelectFolderViewController") as! SelectFolderViewController
                destination.createFolderPopupDelegate = self
                self.saveFolderTrendId = self.trendData.data[sender.tag].TrendId
                destination.trendId = self.trendData.data[sender.tag].TrendId
//                destination.shareTrendId = self.trendData.data[sender.tag].TrendShareId
//
//                self.shareTrendId = self.trendData.data[sender.tag].TrendShareId
                let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height * 0.7)
                self.navigationController?.present(pop, animated: true, completion: nil)
                
            case 6: //break
                let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
                let destination = storyboard.instantiateViewController(withIdentifier: "SetTrendViewController") as! SetTrendViewController
                destination.editTrendParam = self.trendData.data[sender.tag]
                destination.isEdit = true
                //  (self.tabBarController!).selectedIndex = 2
                self.navigationController?.pushViewController(destination, animated: true)
                
            case 7:
                
                let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: MySingleton.shared.selectedLangData.confirmation, message: MySingleton.shared.selectedLangData.Are_you_sure_you_want_to_delete_this_trend, preferredStyle: .alert)
                let deleteActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.delete, style: .default) { void in
                    self.callApiForFavouriteUnfavourite(urlParam: String(self.trendData.data[sender.tag].TrendId), service: .deleteTrend, index: sender.tag)
                }
                let cancelActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.cancel, style: .cancel) { void in
                }
                actionSheetControllerIOS8.addAction(deleteActionButton)
                actionSheetControllerIOS8.addAction(cancelActionButton)
                actionSheetControllerIOS8.popoverPresentationController?.sourceView = self.view
                actionSheetControllerIOS8.popoverPresentationController?.sourceRect = self.view.bounds
                self.present(actionSheetControllerIOS8, animated: true, completion: nil)
                
            default:
                break
            }
        }
        
    cell.trendMoreButtonAction(index: sender.tag, trendData: self.trendData)
    }
    
    
    @objc func didTapLikeButton(button: UIButton) {
        var isTrendLiked = Bool()
//        for trend in self.trendData.data {
//            if trend.TrendId == button.tag {
//                isTrendLiked = trend.IsTrendLike
//            }
//        }
        isTrendLiked = (self.trendData.data[Int(button.accessibilityIdentifier!)!].IsTrendLike)
        if isTrendLiked == true {
            dislikeTrend(trendID: "\(button.tag)", index: button.accessibilityIdentifier!)
        }
        else {
            likeTrend(trendID: "\(button.tag)", index: button.accessibilityIdentifier!)
        }
    }
    
    func likeTrend(trendID: String, index: String) {
        WebServices().callUserService(service: .likeUnlikeTrend, urlParameter: "\(trendID)", parameters: nil, isLazyLoading: false, isHeader: true, CallMethod: .post) { (serviceResponse, responseData) in
            
            print("Like Trend Response --- \(serviceResponse)")
            if serviceResponse["Status"] == nil {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                return
            }
            if(serviceResponse["Status"] as! Bool == true)
            {
                DispatchQueue.main.async {
                    self.trendData.data[Int(index)!].IsTrendLike = true
                    self.trendData.data[Int(index)!].TrendLikesCount = self.trendData.data[Int(index)!].TrendLikesCount + 1
                    let cellIndexPath = IndexPath(row: Int(index)!, section: 0)
                    let cell = self.trendsTable.cellForRow(at: cellIndexPath)as? TrendTableViewCell
                    cell!.trendLikesImageView.image = #imageLiteral(resourceName: "Like_clk")
                    cell!.likesCountLabel.text = "\(self.trendData.data[Int(index)!].TrendLikesCount)"
                }
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func dislikeTrend(trendID: String, index: String) {
        WebServices().callUserService(service: .likeUnlikeTrend, urlParameter: "\(trendID)", parameters: nil, isLazyLoading: false, isHeader: true, CallMethod: .post) { (serviceResponse, responseData) in
            
            print("Dislike Trend Response --- \(serviceResponse)")
            if serviceResponse["Status"] == nil {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                return
            }
            if(serviceResponse["Status"] as! Bool == true)
            {
                
                DispatchQueue.main.async {
                    self.trendData.data[Int(index)!].IsTrendLike = false
                    self.trendData.data[Int(index)!].TrendLikesCount = self.trendData.data[Int(index)!].TrendLikesCount - 1
                    let cellIndexPath = IndexPath(row: Int(index)!, section: 0)
                    let cell = self.trendsTable.cellForRow(at: cellIndexPath)as? TrendTableViewCell
                    cell!.trendLikesImageView.image = #imageLiteral(resourceName: "Like")
                    cell!.likesCountLabel.text = "\(self.trendData.data[Int(index)!].TrendLikesCount)"
                }
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }

    
    
    func registerCollectionViewCells() {
        trendImagesCollectionView.register(UINib(nibName: "ImageTrendCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageTrendCollectionViewCell")
    }
    
    
}



extension TrenderTravelVC:UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == trendCollectionView {
            // GRID
            return testImageArray.count
        }
        else {
            // IMAGEs
            print("Trend Image Collection View Tag --- \(collectionView.tag) --- Count --- \(trendData.data[collectionView.tag].ImageList!.count)")
            return self.trendData.data[collectionView.tag].ImageList!.count//trendImagesCollectionArray[collectionView.tag].count
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == trendCollectionView {
            return configHomeCollectionViewCell(collectionView:collectionView, withIndexPath: indexPath)
        }
        else {
            return configTrendImagesCollectionViewCell(collectionView: collectionView, withIndexPath: indexPath)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if trendData.data[collectionView.tag].IsCustomAd{
            //            if let url = trendData.data[collectionView.tag].AdUrl{
            //                UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
            //            }
            return
        }
        
        if collectionView == trendCollectionView{
            
        }
        else
        {
        
            let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
            let fullScreen = storyboard.instantiateViewController(withIdentifier:"FullScreenViewController") as! FullScreenViewController
            fullScreen.cellIndex = collectionView.tag
            fullScreen.delegate = self
            fullScreen.imageList = trendData.data[collectionView.tag].ImageList!
            self.navigationController?.pushViewController(fullScreen, animated: true)
        }
    }
    
    func configTrendImagesCollectionViewCell(collectionView: UICollectionView, withIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = "ImageTrendCollectionViewCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! ImageTrendCollectionViewCell
        cell.trendImageView.sd_setImage(with: URL(string: trendData.data[collectionView.tag].ImageList![indexPath.item].TrendImage), placeholderImage: #imageLiteral(resourceName: "logo_navigation"))
        return cell
    }
    
    func configHomeCollectionViewCell(collectionView: UICollectionView, withIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = "HomeCollectionViewCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! HomeCollectionViewCell
        
        cell.trendImageView.image = testImageArray[indexPath.item]
        cell.trendImageView.contentMode = .scaleAspectFill
        collectionView.backgroundColor = .red
        cell.backgroundColor = .white
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == trendCollectionView {
            var calculatedWidth = CGFloat()
            var calculatedHeight = CGFloat()
            
            if indexPath.item == 0 {
                calculatedWidth = self.view.frame.size.width / 3
                calculatedHeight = calculatedWidth
            }
            else if indexPath.item == 1 {
                calculatedWidth = self.view.frame.size.width / 3
                calculatedHeight = calculatedWidth
            }
            else if indexPath.item == 2 {
                
                
                calculatedWidth = (self.view.frame.size.width / 3) * 2
                calculatedHeight = calculatedWidth
            }
            else {
                calculatedWidth = self.view.frame.size.width / 3
                calculatedHeight = calculatedWidth
            }
            
            
            return CGSize(width: calculatedWidth, height: calculatedHeight)
        }
        else {
            var calculatedWidth = CGFloat()
            var calculatedHeight = CGFloat()
            calculatedWidth = self.view.frame.size.width
            calculatedHeight = ((CGFloat(trendData.data[collectionView.tag].ImageList![0].ImageHeight ?? 250)) * calculatedWidth) / (CGFloat(trendData.data[collectionView.tag].ImageList![0].ImageWidth ?? 250))
            return CGSize(width: calculatedWidth, height: calculatedHeight)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if collectionView == trendCollectionView {
        }
        else {
            if trendData.data.count > 0 {
                let cell = trendsTable.cellForRow(at: IndexPath(row: collectionView.tag, section: 0)) as? TrendTableViewCell
                if cell != nil {
                    cell!.trendImagesPageControl.currentPage = indexPath.item
                    cell!.trendCurrentImageLabel.text = "\(indexPath.item + 1)/\(trendData.data[collectionView.tag].ImageList!.count)"
                }
                
            }
        }
    }
}
extension TrenderTravelVC : CreateFolderPopupDelegate{
    func createFolderPopup() {
        let storyboard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
        let nextViewController = storyboard.instantiateViewController(withIdentifier: "NewFolderPoupVC") as! NewFolderPoupVC
        nextViewController.delegate = self
        let pop = PopUpViewController(nextViewController,withHeight: self.view.frame.size.height)
        self.navigationController?.present(pop, animated: true, completion: nil)
    }
    
}

extension TrenderTravelVC : CreateFolderDelegate {
    func controller() {
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "SelectFolderViewController") as! SelectFolderViewController
        destination.createFolderPopupDelegate = self
        destination.trendId = self.saveFolderTrendId
        destination.shareTrendId = self.shareTrendId
        
        let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height * 0.7)
        self.navigationController?.present(pop, animated: true, completion: nil)
    }    
}

extension TrenderTravelVC : TrendViewDelegate,friendSelectedDelegate,FriendsPopupDelegate,TrendCommentDelegate {
    func IncreaseTrendViewCount(Index: Int) {
        let cell = trendsTable.cellForRow(at: IndexPath(row: Index, section: 0))as? TrendTableViewCell
        trendData.data[Index].TrendViewCount = String (Int(trendData.data[Index].TrendViewCount ?? "0")! + 1)
        cell!.viewsCountLabel.text = "\(trendData.data[Index].TrendViewCount)"
    }
    
    func friendsPopup(indexShare: Int, trendId: Int, description: String) {
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "FriendListViewController") as! FriendListViewController
        destination.delegate = self
        destination.trendId = trendId
        destination.index = indexShare
        destination.txtdescprition = description
        let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height)
        self.navigationController?.present(pop, animated: true, completion: nil)
    }
    
    func controller(arraySelectedFriends: [TagFriendModel], index: Int, trendId: Int,description: String) {
          let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "ShareInternalViewController") as! ShareInternalViewController
        destination.trendId = trendId
        
        if arraySelectedFriends.count != 0
        {
            for i in 0...arraySelectedFriends.count-1
            {
                if(arraySelectedFriends[i].isSelected)
                {
                    let dict = ["FriendId": arraySelectedFriends[i].id]
                    selectedShareFriends.append(dict as AnyObject)
                }
            }
        }
        destination.selectedShareFriends = selectedShareFriends
        destination.index = index
        destination.txtdiscription = description
        let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height)
        self.navigationController?.present(pop, animated: true, completion: nil)
    }
    
    func commentCount(Index: Int,CommentCount: Int) {
        let cell = trendsTable.cellForRow(at: IndexPath(row: Index, section: 0))as? TrendTableViewCell
        trendData.data[Index].CommentCount = String(CommentCount)
        cell!.commentsCountLabel.text = trendData.data[Index].CommentCount
    }
}



extension TrenderTravelVC : CLLocationManagerDelegate
{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
        else if status == .denied
        {
            let alertController = UIAlertController(title: "TrenderAlert", message: "GPS location is disabled. Please enable it from settings to get current location.", preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: "Settings", style: UIAlertAction.Style.default, handler: { (action) in
                UIApplication.shared.openURL(URL(string: "App-Prefs:root=Privacy&path=LOCATION")!)
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { (action) in
              
                
             //   self.selfMarker.position = CLLocationCoordinate2D(latitude: self.userDetails["UserLocationLatitude"]as! Double, longitude: self.userDetails["UserLocationLongitude"]as! Double)
                self.selfMarker.tag = -1
             //   self.gmsCamera = GMSCameraPosition.camera(withLatitude: self.userDetails["UserLocationLatitude"]as! Double, longitude: self.userDetails["UserLocationLongitude"]as! Double, zoom: 10.0)
                self.searchMap.camera = self.gmsCamera
                self.selfMarker.isDraggable = true
                self.selfMarker.map = self.searchMap
              //  self.gmsCircle = GMSCircle()
                self.gmsCircle.radius = self.radius
                self.gmsCircle.position = self.selfMarker.position
                self.gmsCircle.map = self.searchMap
                self.gmsCircle.fillColor = UIColor(red: 254.0/255.0, green: 194.0/255.0, blue: 16.0/255.0, alpha: 0.5)
                self.gmsCircle.strokeColor = UIColor.clear
//                GetAddress().getAddressFromLocation(locations.first!) { (address) in
//                    self.location = address
//                }
                self.getAllTrends(SearchBy: "Place", Latitude: self.latitude, Longitude: self.longitude, Miles: self.miles, SortColumn: "", SortDirection: "", Skip: 0, Take: 15, isLoading: true)
                
            })
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if(latitude == nil)
        {
            latitude = locations.first!.coordinate.latitude
            longitude = locations.first!.coordinate.longitude
            if searchTrendSwitch.isOn
            {
                locationManager.startMonitoringSignificantLocationChanges()
                locationManager.startUpdatingLocation()
            }
            else
            {
                locationManager.stopMonitoringSignificantLocationChanges()
                locationManager.stopUpdatingLocation()
            }
            
            selfMarker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            selfMarker.tag = -1
//            GetAddress().getAddressFromLocation(locations.first!) { (address) in
//                self.location = address
//            }
            gmsCamera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 10.0)
            searchMap.camera = gmsCamera
            selfMarker.isDraggable = true
            selfMarker.map = searchMap
          //  gmsCircle = GMSCircle()
            gmsCircle.radius = self.radius
            gmsCircle.position = selfMarker.position
            gmsCircle.map = searchMap
            gmsCircle.fillColor = UIColor(red: 0.0/255.0, green: 150.0/255.0, blue: 255.0/255.0, alpha: 0.3)
            gmsCircle.strokeColor = UIColor.clear
           // self.searchTrendTextField.text! = "Place"
            print(latitude)
            print(longitude)
            self.trendData.data = []
            //    self.trendImagesArray = []
            self.totalTrendsCount = 1
            self.currentCustomAdCnt = 0
            self.currentGoogleAdCnt = 0
            getAllTrends(SearchBy: "Place", Latitude: latitude!, Longitude: longitude!, Miles: miles, SortColumn: "", SortDirection: "", Skip: 0, Take: 15, isLoading: true)
        }
       
            else
            {
                latitude = 0.0
                longitude = 0.0
                latitude = locations.first!.coordinate.latitude
                longitude = locations.first!.coordinate.longitude
                selfMarker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                gmsCamera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: searchMap.camera.zoom)
//                GetAddress().getAddressFromLocation(locations.first!) { (address) in
//                    self.location = address
//                }
                
                self.gmsCircle.radius = self.radius
                searchMap.camera = gmsCamera
                selfMarker.map = searchMap
                gmsCircle.position =  CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
                gmsCircle.map = searchMap
                gmsCircle.fillColor = UIColor(red: 0.0/255.0, green: 150.0/255.0, blue: 255.0/255.0, alpha: 0.3)
                gmsCircle.strokeColor = UIColor.clear
             
                 if searchTrendSwitch.isOn
                 {
                    locationManager.startMonitoringSignificantLocationChanges()
                    locationManager.startUpdatingLocation()
                    self.perform(#selector(self.updateLocationTimer), with: nil, afterDelay: 5.0)
                 }
                else
                 {
                    locationManager.stopUpdatingLocation()
                    self.trendData.data = []
                    //    self.trendImagesArray = []
                    self.totalTrendsCount = 1
                    self.currentCustomAdCnt = 0
                    self.currentGoogleAdCnt = 0
                       getAllTrends(SearchBy: "Place", Latitude: latitude, Longitude: longitude, Miles: miles, SortColumn: "", SortDirection: "", Skip: 0, Take: 15, isLoading: true)
                 }
            }
    }
    
    @objc func updateLocationTimer()
    {
        if searchTrendSwitch.isOn
        {
            self.trendData.data = []
            self.totalTrendsCount = 1
            self.currentCustomAdCnt = 0
            self.currentGoogleAdCnt = 0
             getAllTrends(SearchBy: "Place", Latitude: latitude!, Longitude: longitude!, Miles: miles, SortColumn: "", SortDirection: "", Skip: 0, Take: 15, isLoading: true)
          self.perform(#selector(self.updateLocationTimer), with: nil, afterDelay: 60.0)
        }
    }
    
    func updateLocationoordinates(coordinates:CLLocationCoordinate2D) {
        if selfMarker.tag  == nil
        {
            selfMarker = TrenderAlertMarkerTravel()
            selfMarker.position = coordinates
            selfMarker.map = searchMap
            self.trendData.data = []
            self.totalTrendsCount = 1
            self.currentCustomAdCnt = 0
            self.currentGoogleAdCnt = 0
            getAllTrends(SearchBy: "Place", Latitude: latitude, Longitude: longitude, Miles: miles, SortColumn: "", SortDirection: "", Skip: 0, Take: 15, isLoading: true)
        }
        else
        {
            self.trendData.data = []
            //    self.trendImagesArray = []
            self.totalTrendsCount = 1
            self.currentCustomAdCnt = 0
            self.currentGoogleAdCnt = 0
            selfMarker.position =  coordinates
            getAllTrends(SearchBy: "Place", Latitude: latitude, Longitude: longitude, Miles: miles, SortColumn: "", SortDirection: "", Skip: 0, Take: 15, isLoading: true)
        }
    }
    
    // Camera change Position this methods will call every time
    func mapView(mapView: GMSMapView, didChangeCameraPosition position: GMSCameraPosition) {
        
        self.selfMarker.map = self.searchMap
        selfMarker.position = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        selfMarker.tag = -1
        
        gmsCamera = GMSCameraPosition.camera(withLatitude: latitude, longitude: longitude, zoom: 10.0)
        searchMap.camera = gmsCamera
        selfMarker.isDraggable = true
        
        updateLocationoordinates(coordinates: selfMarker.position)
        self.trendData.data = []
        self.totalTrendsCount = 1
        self.currentCustomAdCnt = 0
        self.currentGoogleAdCnt = 0
        getAllTrends(SearchBy:"Place", Latitude: latitude, Longitude: longitude, Miles: miles, SortColumn: "", SortDirection: "", Skip: 0, Take: 15, isLoading: true)
    }
    
    func mapView(_ mapView: GMSMapView, didEndDragging marker: GMSMarker) {
        latitude = selfMarker.position.latitude
        longitude = selfMarker.position.longitude
        gmsCircle.position = selfMarker.position
        self.trendData.data = []
        //    self.trendImagesArray = []
        self.totalTrendsCount = 1
        self.currentCustomAdCnt = 0
        self.currentGoogleAdCnt = 0
        //locationManager.stopUpdatingLocation()
        getAllTrends(SearchBy:"Place", Latitude: latitude, Longitude: longitude, Miles: miles, SortColumn: "", SortDirection: "", Skip: 0, Take: 15, isLoading: true)
         let locationName = CLLocationCoordinate2D(latitude: selfMarker.position.latitude, longitude: selfMarker.position.longitude)
        GetAddress().getAddressFromLocationTravel(CLLocation(latitude: locationName.latitude, longitude: locationName.longitude)) { (address) in
            self.location.text = address
            print(self.location.text!)
        }
       // getNearByTrend()
        
    }
    
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker)
    {
        gmsCircle.position = CLLocationCoordinate2D(latitude: 0, longitude: 0)
    }
    
 
    public func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool
    {
        if((marker as! TrenderAlertMarkerTravel).tag != -1)
        {
            print((marker as! TrenderAlertMarkerTravel).tag)
            let storyboard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
            let destination = storyboard.instantiateViewController(withIdentifier: "TravelTrendViewController") as! TravelTrendViewController
            destination.trendId = self.trendData.data[(marker as! TrenderAlertMarkerTravel).tag!].TrendId
            self.navigationController?.pushViewController(destination, animated: true)
        }
        else
        {
            if self.view.subviews.contains(customInfoWindow)
            {
               customInfoWindow.removeFromSuperview()
            }
            else
            {
                let location = CLLocationCoordinate2D(latitude: selfMarker.position.latitude, longitude: selfMarker.position.longitude)
                customInfoWindow.removeFromSuperview()
                customInfoWindow.center = mapView.projection.point(for: location)
               // customInfoWindow.center.y = CGFloat(selfMarker.position.latitude) + customInfoWindow.center.y - 5
                customInfoWindow.center.y = customInfoWindow.center.y + 40
                print(customInfoWindow.center)
                let locationN = customInfoWindow.viewWithTag(1) as! UILabel
                let viewMain = customInfoWindow.viewWithTag(3) as! UIView
                viewMain.makeCornerRadius(radius: 4)
                let locationName = CLLocationCoordinate2D(latitude: selfMarker.position.latitude, longitude: selfMarker.position.longitude)
                locationN.text = self.location.text
                if self.location.text == ""
                {
                    GetAddress().getAddressFromLocationTravel(CLLocation(latitude: locationName.latitude, longitude: locationName.longitude)) { (address) in
                        locationN.text = address
                        print(locationN.text!)
                    }
                }
//                GetAddress().getAddressFromLocationTravel(CLLocation(latitude: locationName.latitude, longitude: locationName.longitude)) { (address) in
//                    self.location.text = address
//                    print(self.location.text!)
//                    if address != "" {
                       self.view.addSubview(self.customInfoWindow)
//                    }
             //   }
            }
          }
            return true
    }
    
    
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        customInfoWindow.removeFromSuperview()
    }
    
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
         customInfoWindow.removeFromSuperview()
    }
  
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {

        let customInfoWindow = Bundle.main.loadNibNamed("MapWindow", owner: self, options: nil)?[0] as! UIView
        self.location = customInfoWindow.viewWithTag(1) as! UILabel
        
        let locationName = CLLocationCoordinate2D(latitude: selfMarker.position.latitude, longitude: selfMarker.position.longitude)
          DispatchQueue.main.async {
//        GetAddress().getAddressFromLocationTravel(CLLocation(latitude: locationName.latitude, longitude: locationName.longitude)) { (address) in
//            self.location.text = address
//            // location.text! = self.location
//            print(self.location.text!)
//          }
        }
        let totalTrends = customInfoWindow.viewWithTag(2) as! UILabel
        totalTrends.text = "\(MySingleton.shared.selectedLangData.total_trends) : 0"

       // getAllTrends(SearchBy: "Place", Latitude: selfMarker.position.latitude, Longitude: selfMarker.position.longitude, Miles: miles, SortColumn: "", SortDirection: "", Skip: 0, Take: 15, isLoading: true)
    }

    func drawImageWithProfilePic(index: Int,pp: UIImage, image: UIImage) -> UIImage {
        
        let imgView = UIImageView(image: image)
        let picImgView = UIImageView()
        if trendData.data[index].UserProfileImage != nil
        {
           picImgView.sd_setImage(with: URL(string: trendData.data[index].UserProfileImage!), placeholderImage: #imageLiteral(resourceName: "img_selectProfilePic"))
        }
        else
        {
            picImgView.image = #imageLiteral(resourceName: "img_selectProfilePic")
        }
        imgView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        picImgView.frame = CGRect(x: 0, y: 8, width: 24, height: 24)
        imgView.addSubview(picImgView)
        picImgView.center.x = imgView.center.x
        picImgView.center.y = imgView.center.y - 4
        picImgView.layer.cornerRadius = picImgView.frame.width/2
        picImgView.clipsToBounds = true
        imgView.setNeedsLayout()
        picImgView.setNeedsLayout()
        
        let newImage = imageWithView(view: imgView)
        return newImage
    }
    
    func imageWithView(view: UIView) -> UIImage {
        var image: UIImage?
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0.0)
        if let context = UIGraphicsGetCurrentContext() {
            view.layer.render(in: context)
            image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
        }
        return image ?? UIImage()
    }
    
    @objc func textFieldTextChanged(_ sender : UITextField)
    {
        if(searchTrendTextField.isEditing)
        {
            if  searchTrendTextField.text!.isEmpty
            {
                self.locationDropdown.hide()
            }
            else
            {
                let getAddress = GetAddress()
                self.location_arrDrop.removeAll()
                //self.locationDropdown.dataSource = self.location_arr
                getAddress.getAddress(location_str: sender.text!) { (locationData) in
                    // self.location_arr = locationData
                    self.location_arrDrop.removeAll()
                    for location in locationData
                    {
                        self.location_arrDrop.append(location["address"]!)
                    }
                    print(self.location_arrDrop)
                    self.locationDropdown.dataSource = []
                    self.locationDropdown.dataSource = self.location_arrDrop
                    
                    if !sender.text!.isEmpty
                    {
                        self.locationDropdown.show()
                    }
                  
                }
            }
        }
//        else if (txtEmail.txtFld_UserValue.isEditing)
//        {
//            sender.text = sender.text?.lowercased()
//        }
    }
    
    func dropdownSelection()
    {
        //self.gmsCamera = GMSCameraPosition.camera(withLatitude: self.latitude, longitude: self.longitude, zoom: 10.0)
        locationDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.latitude = 0.0
            self.longitude = 0.0
            self.gmsCircle.radius = 0.0
            self.searchTrendTextField.text! = item
            GetAddress().getLocationFromAddressString(addressStr: self.searchTrendTextField.text!) { (clLocationCoordinate2D,zipcode) in
                self.latitude = clLocationCoordinate2D.latitude
                self.longitude = clLocationCoordinate2D.longitude
                self.selfMarker.position = CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
                self.selfMarker.tag = -1
                self.gmsCamera = GMSCameraPosition.camera(withLatitude: self.latitude, longitude: self.longitude, zoom: 10.0)
                self.searchMap.camera = self.gmsCamera
                self.selfMarker.isDraggable = true
                self.selfMarker.map = self.searchMap
                self.gmsCircle = GMSCircle()
                self.gmsCircle.radius = self.radius
                self.gmsCircle.position = self.selfMarker.position
                self.gmsCircle.map = self.searchMap
                self.gmsCircle.fillColor = UIColor(red: 0.0/255.0, green: 150.0/255.0, blue: 255.0/255.0, alpha: 0.3)
                self.gmsCircle.strokeColor = UIColor.clear
                self.searchTrendTextField.text! = ""
                print(self.latitude)
                print(self.longitude)
                self.setupTextfield()
                self.trendData.data = []
                self.totalTrendsCount = 1
                self.currentCustomAdCnt = 0
                self.currentGoogleAdCnt = 0
                self.getAllTrends(SearchBy: "Place", Latitude: self.latitude!, Longitude: self.longitude!, Miles: self.miles, SortColumn: "", SortDirection: "", Skip: 0, Take: 15, isLoading: true)
            }
        }
    }

}

class TrenderAlertMarkerTravel : GMSMarker
{
    var tag : Int!
}
