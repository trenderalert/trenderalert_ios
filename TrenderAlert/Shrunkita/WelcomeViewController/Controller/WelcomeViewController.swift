//
//  WelcomeViewController.swift
//  TrenderAlert
//
//  Created by Admin on 04/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import MFSideMenu

class WelcomeViewController: UIViewController {

    @IBOutlet weak var lblInfo: UILabel!
    @IBOutlet weak var btnCHangeUserName: UIButton!
    @IBOutlet var lblTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSettings()
    }
    
    func initialSettings() {
        self.lblTitle.text = "\(MySingleton.shared.selectedLangData.Welcome_to_TrenderAlert),\n\(MySingleton.shared.username)"
        self.lblInfo.text = MySingleton.shared.selectedLangData.welcomeInfo
        self.navigationController?.isNavigationBarHidden = true
        setIsFirstTime()
    }
    
    
    @IBAction func didTapNextButton(_ sender: UIButton) {
        let appDelegatge = UIApplication.shared.delegate as! AppDelegate
        let storyBoard = UIStoryboard(name: "Dashboard", bundle: nil)
        let centerViewController = storyBoard.instantiateViewController(withIdentifier: "TrendAlertTabBarController") as! UITabBarController
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let leftViewController = mainStoryBoard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
        appDelegatge.container.leftMenuViewController = leftViewController
        appDelegatge.container.centerViewController = centerViewController
        appDelegatge.container.menuState = MFSideMenuStateClosed
        appDelegatge.container.leftMenuWidth = (centerViewController.view.frame.size.width / 4) * 3
        appDelegatge.window!.rootViewController = appDelegatge.container
        appDelegatge.window!.makeKeyAndVisible()
    }
    
    
    func setIsFirstTime()
    {
        WebServices().callUserService(service: UserServices.isFirstTime, urlParameter: "",parameters:nil, isLazyLoading: false, isHeader: true, CallMethod: .get){ (serviceResponse, serviceData) in
            print(serviceResponse)
            if(serviceResponse["Status"] != nil)
            {
                if(serviceResponse["Status"] as! Bool == true)
                {
                    let loginObject = Login()
                    loginObject.IsFirstTime = "False"
                }
                else
                {
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
        
    }
    
    
    
}
