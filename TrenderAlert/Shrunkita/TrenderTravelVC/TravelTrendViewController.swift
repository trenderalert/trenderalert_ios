//
//  TravelTrendViewController.swift
//  TrenderAlert
//
//  Created by Riken Shah on 08/07/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit
import SDWebImage

class TravelTrendViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var tblTrend: UITableView!
    
    var trendImagesArray = [UIImage]()
    var refreshControl : UIRefreshControl!
    var trendData = TrendListData()
    var trendImagesCollectionView: UICollectionView!
    var testImageArray = [UIImage]()
    var trendImagesPageControl : UIPageControl!
    var trendCurrentImageLabel : UILabel!
    var trendId = Int()
    var saveFolderTrendId = Int()
    var shareTrendId = Int()
    var trendHeaderViewHeight = (UIDevice.current.userInterfaceIdiom == .phone) ? 50 : 70
    
    var trendStatusViewHeight = (UIDevice.current.userInterfaceIdiom == .phone) ? 45 : 65
    
    var googleAdHeight = (UIDevice.current.userInterfaceIdiom == .pad) ? 90.0 : 50.0

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        self.tblTrend.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        setupNavigationbar()
    }
    
    func setupNavigationbar(){
        Utilities.setNavigationBarWithTitle(viewController: self, title: MySingleton.shared.selectedLangData.trend_detail)
    }
    
    @IBAction func btn_BackAction(_ sender: Any)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setupTableView(){
        tblTrend.register(UINib(nibName: "TrendTableViewCell", bundle: nil), forCellReuseIdentifier: "TrendTableViewCell")
         getAllTrends(trendId: self.trendId)
    }
    
    @objc func refreshAllTrends(sender: UIRefreshControl) {
        getAllTrends(trendId: self.trendId)
        self.refreshControl.endRefreshing()
    }
    
    
    func getAllTrends(trendId:Int) {
        let params = ["TrendId":self.trendId] as [String:Any]
        print(params)
        WebServices().callUserService(service: .getTrendDetailsByTrendId, urlParameter: "", parameters: params as [String : AnyObject], isLazyLoading: true, isHeader: true, CallMethod: .post) { (serviceResponse, responseData) in
            print("Get All Trends --- \(serviceResponse)")
            if(serviceResponse["Status"] as! Bool == true)
            {
                DispatchQueue.main.async {
                self.trendData.data.removeAll()
                //                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: {})
                self.trendData = WebServices().decodeDataToClass(data: responseData, decodeClass: TrendListData.self)!
                
           
              
                    self.tblTrend.reloadData()
            }
                
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
            
        }
    }
    
    @objc func btnLearnMoreAction(_ sender : UIButton)
    {
        if trendData.data[sender.tag].IsCustomAd{
            //            if let url = URL(string: "https://stackoverflow.com") {
            
            if var url = trendData.data[sender.tag].AdUrl {
                if !(url.contains("http"))
                {
                    url = url.replacingOccurrences(of: "www.", with: "https://")
                }
                
                if UIApplication.shared.canOpenURL(URL(string: url)!)
                {
                    UIApplication.shared.open(URL(string: url)!)
                }
                //                UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
            }
        }
    }
    
    @objc func btnFullScrProfileImgAction(_ sender : UIButton)
    {
        let zoomImageView = ZoomImageVC(nibName: "ZoomImageVC", bundle: nil)
        if trendData.data[sender.tag].UserProfileImage != nil
        {
            zoomImageView.imageUrl = trendData.data[sender.tag].UserProfileImage!
        }
        self.navigationController?.pushViewController(zoomImageView, animated: true)
    }
    
    @objc func tapActionForLabel(_ recognizer : UITapGestureRecognizer)
    {
        if recognizer.state == UIGestureRecognizer.State.ended {
            print(recognizer.view!.tag)
            
            if trendData.data[recognizer.view!.tag].IsTrendShared == false{
                if trendData.data[recognizer.view!.tag].IsCustomAd{
                }
                else{
                    if (trendData.data[recognizer.view!.tag].AreYouFollowingTrender == 0 || trendData.data[recognizer.view!.tag].AreYouFollowingTrender == 1 || trendData.data[recognizer.view!.tag].AreYouFollowingTrender == 2)
                    {
                        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
                        let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
                        profileVC.userId = trendData.data[recognizer.view!.tag].UserProfileId
                        self.navigationController?.pushViewController(profileVC, animated: true)
                    }
                    else
                    {
                        (self.tabBarController!).selectedIndex = 4
                    }
                }
            }
            else{
                let firstRange = ((recognizer.view! as! UILabel).text as! NSString).range(of: "\(trendData.data[recognizer.view!.tag].UserProfileFirstName!) \(String(describing: trendData.data[recognizer.view!.tag].UserProfileLastName!))")
                
                let secondRange = ((recognizer.view! as! UILabel).text as! NSString).range(of: "\(trendData.data[recognizer.view!.tag].TrendOwner!)")
                
                if recognizer.didTapAttributedTextInLabel(label: (recognizer.view! as! UILabel), inRange: firstRange) {
                    print(firstRange)
                    if Int(MySingleton.shared.loginObject.UserID) == trendData.data[recognizer.view!.tag].UserProfileId
                    {
                        (self.tabBarController!).selectedIndex = 4
                    }
                    else
                    {
                        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
                        let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
                        profileVC.userId = trendData.data[recognizer.view!.tag].UserProfileId
                        self.navigationController?.pushViewController(profileVC, animated: true)
                    }
                    
                } else if recognizer.didTapAttributedTextInLabel(label: (recognizer.view! as! UILabel), inRange: secondRange) {
                    print(secondRange)
                    if Int(MySingleton.shared.loginObject.UserID) != trendData.data[recognizer.view!.tag].TrendOwnerId!
                    {
                        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
                        let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
                        profileVC.userId = trendData.data[recognizer.view!.tag].TrendOwnerId!
                        self.navigationController?.pushViewController(profileVC, animated: true)
                    }
                    else
                    {
                        (self.tabBarController!).selectedIndex = 4
                    }
                    
                } else {
                    print("Tapped none")
                }
            }
            
        }
    }
    
    // MARK: - Config Trend Table View Cell
    func configTrendTableViewCell(tableView: UITableView, withIndexPath indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "TrendTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! TrendTableViewCell
        trendImagesCollectionView = cell.trendImageCollectionView
        
        // Register Collection View Cells
        registerCollectionViewCells()
        cell.trendCurrentImageLabel.layer.cornerRadius = 3.0
        // Testing
        var calculatedWidth = CGFloat()
        var calculatedHeight = CGFloat()
        calculatedWidth = self.view.frame.size.width
        cell.profileImageView.layer.cornerRadius = 0.5 * cell.profileImageView.bounds.size.width
        cell.profileImageView.clipsToBounds = true
        
        calculatedHeight = (CGFloat(trendData.data[indexPath.row].ImageList![0].ImageHeight ?? 250) * calculatedWidth) / (CGFloat(trendData.data[indexPath.row].ImageList![0].ImageWidth ?? 250))
        
        if trendData.data[indexPath.row].IsTrendShared == false{
            
            if trendData.data[indexPath.row].IsCustomAd{ // Custom Ad
                cell.btnLearnMore.isHidden = false
                cell.btnLearnMore.tag = indexPath.row
                cell.btnLearnMore.addTarget(self, action: #selector(btnLearnMoreAction(_:)), for: .touchUpInside)
                cell.profileImageView.isHidden = true
                cell.shareDescLabel.text = ""
                cell.cnstrntShareDescHeight.constant = 0
                cell.cnstrTrendHeaderViewHeight.constant = 0
                cell.cnstrTrendStatusViewHeight.constant = 0
                cell.cnstrTrendViewCommentViewHeight.isActive = true
                cell.trendViewCommentsView.isHidden = true
                cell.trendStatusView.isHidden = true
                cell.clipsToBounds = true
                //
                cell.videoLayer.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: calculatedHeight)
                //
            }else{
                //             tableView.rowHeight = 286 + calculatedHeight - 61
                cell.btnLearnMore.isHidden = true
                cell.profileImageView.isHidden = false
                cell.profileImageView.layer.cornerRadius = 0.5 * cell.profileImageView.bounds.size.width
                cell.profileImageView.clipsToBounds = true
                
                cell.shareDescLabel.text = ""
                cell.cnstrntShareDescHeight.constant = 0
                cell.cnstrTrendViewCommentViewHeight.isActive = false
                cell.trendViewCommentsView.isHidden = false
                cell.trendStatusView.isHidden = false
                cell.profileNameLabel.text = "\(trendData.data[indexPath.row].UserProfileFirstName ?? "") \(trendData.data[indexPath.row].UserProfileLastName ?? "")"
                cell.profileNameLabel.textColor = UIColor.darkText
                cell.profileNameLabel.tag = indexPath.row
                cell.profileNameTap.numberOfTapsRequired = 1
                cell.profileNameLabel.isUserInteractionEnabled = true
                cell.profileNameTap = UITapGestureRecognizer(target: self, action: #selector(tapActionForLabel(_:)))
                cell.profileNameLabel.addGestureRecognizer(cell.profileNameTap)
                cell.btnFullScrProfileImg.tag = indexPath.row
                cell.btnFullScrProfileImg.addTarget(self, action: #selector(btnFullScrProfileImgAction(_:)), for: .touchUpInside)
                
                cell.locationLabel.text = trendData.data[indexPath.row].TrendLocation ?? ""
                cell.cnstrTrendHeaderViewHeight.constant = CGFloat(trendHeaderViewHeight)
                cell.cnstrTrendStatusViewHeight.constant = CGFloat(trendStatusViewHeight)
                
                if trendData.data[indexPath.row].UserProfileImage != nil {
                    cell.profileImageView.sd_setImage(with: URL(string: trendData.data[indexPath.row].UserProfileImage!), placeholderImage: #imageLiteral(resourceName: "img_selectProfilePic"))
                }
                else
                {
                    cell.profileImageView.image = #imageLiteral(resourceName: "img_selectProfilePic")
                }
                
            }
            
        }
        else
        {
            cell.btnLearnMore.isHidden = true
            cell.profileImageView.isHidden = false
            cell.profileImageView.layer.cornerRadius = 0.5 * cell.profileImageView.bounds.size.width
            cell.profileImageView.clipsToBounds = true
            
            tableView.rowHeight = 286 + calculatedHeight - 28
            cell.cnstrntShareDescHeight.constant = 33
            cell.trendViewCommentsView.isHidden = false
            cell.cnstrTrendViewCommentViewHeight.isActive = false
            cell.trendStatusView.isHidden = false
            cell.shareDescLabel.text = trendData.data[indexPath.row].SharedTrendDescription ?? ""
            
            cell.btnFullScrProfileImg.tag = indexPath.row
            cell.btnFullScrProfileImg.addTarget(self, action: #selector(btnFullScrProfileImgAction(_:)), for: .touchUpInside)
            
            cell.profileNameLabel.tag = indexPath.row
            cell.profileNameTap.numberOfTapsRequired = 1
            cell.profileNameLabel.isUserInteractionEnabled = true
            cell.profileNameTap = UITapGestureRecognizer(target: self, action: #selector(tapActionForLabel(_:)))
            cell.profileNameLabel.addGestureRecognizer(cell.profileNameTap)
            
            let color1 = UIColor.darkText
            // create the attributed colour
            let attributedStringColor = [NSAttributedString.Key.foregroundColor : color1];
            // create the attributed string
            let attributedString = NSMutableAttributedString(string: "\(trendData.data[indexPath.row].UserProfileFirstName!) \(String(describing: trendData.data[indexPath.row].UserProfileLastName!))", attributes: attributedStringColor)
            // Set the label
            
            
            
            // create the attributed colour
            let attrs1 = [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 17), NSAttributedString.Key.foregroundColor : UIColor.black]
            //            let attributedStringColor2 = [NSAttributedString.Key.foregroundColor : color2];
            let attributedString2 = NSMutableAttributedString(string: " shared ", attributes: attrs1)
            attributedString.append(attributedString2)
            let color3 = UIColor.darkText
            let attributedStringColor3 = [NSAttributedString.Key.foregroundColor : color3];
            let attributedString3 = NSMutableAttributedString(string: "\(trendData.data[indexPath.row].TrendOwner ?? "")'s post", attributes: attributedStringColor3)
            attributedString.append(attributedString3)
            cell.profileNameLabel.attributedText = attributedString
            
            cell.locationLabel.text = ""
            cell.cnstrTrendHeaderViewHeight.constant = CGFloat(trendHeaderViewHeight)
            cell.cnstrTrendStatusViewHeight.constant = CGFloat(trendStatusViewHeight)
            
            if trendData.data[indexPath.row].UserProfileImage != nil {
                cell.profileImageView.sd_setImage(with: URL(string: trendData.data[indexPath.row].UserProfileImage!), placeholderImage: #imageLiteral(resourceName: "img_selectProfilePic"))
            }
            else
            {
                cell.profileImageView.image = #imageLiteral(resourceName: "img_selectProfilePic")
            }
        }
        
        if !trendData.data[indexPath.row].IsCustomAd{
            cell.videoLayer.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: calculatedHeight+20)
        }
        
        
        // Data
//        cell.trendExpiryLabel.text = "Expires in \(trendData.data[indexPath.row].RemainingExpiryTime ?? "")"
//        cell.trendAddedLabel.text = "\(trendData.data[indexPath.row].TimeAgo ?? "") Ago"
        cell.trendPostTitleLabel.text = trendData.data[indexPath.row].Title
        cell.postCommentLabel.text = trendData.data[indexPath.row].Description
        cell.viewAllCommentsButton.setTitle("View All \(trendData.data[indexPath.row].CommentCount ?? "") Comment", for: .normal)
        cell.commentsCountLabel.text = "\(trendData.data[indexPath.row].CommentCount ?? "")"
        cell.likesCountLabel.text = "\(trendData.data[indexPath.row].TrendLikesCount)"
        cell.shareCountLabel.text = "\(trendData.data[indexPath.row].TrendShareCount)"
        cell.viewsCountLabel.text = "\(trendData.data[indexPath.row].TrendViewCount ?? "")"
        
        
//        cell.txtAddComment.attributedPlaceholder = NSAttributedString(string: "Write a comment...",
//                                                                      attributes: [NSAttributedString.Key.foregroundColor: UIColor.baseColor])
        
        
        if trendData.data[indexPath.row].IsTrenderOnline
        {
            cell.statusImageView.image = #imageLiteral(resourceName: "img_Active")
        }
        else
        {
            cell.statusImageView.image = #imageLiteral(resourceName: "img_inactive")
        }
        
        if trendData.data[indexPath.row].AreYouFollowingTrender == 0{
            cell.followStatusButton.isHidden = false
            cell.followStatusButton.setTitle(MySingleton.shared.selectedLangData.following, for: .normal)
            cell.followStatusButton.setTitleColor(UIColor.baseColor, for: .normal)
            cell.followStatusButton.tag = indexPath.row
            cell.followStatusButton.addTarget(self, action: #selector(FollowButtonAction(_:)), for: .touchUpInside)
            cell.otherUserProfileButton.isHidden = true
            //            cell.otherUserProfileButton.tag = indexPath.row
            //            cell.otherUserProfileButton.addTarget(self, action: #selector(OtherProfileAction(_:)), for: .touchUpInside)
            
        }
        else if trendData.data[indexPath.row].AreYouFollowingTrender == 1{
            cell.followStatusButton.isHidden = false
            cell.followStatusButton.setTitle(MySingleton.shared.selectedLangData.follow, for: .normal)
            cell.followStatusButton.setTitleColor(UIColor.lightGray, for: .normal)
            cell.followStatusButton.tag = indexPath.row
            cell.followStatusButton.addTarget(self, action: #selector(FollowButtonAction(_:)), for: .touchUpInside)
            cell.otherUserProfileButton.isHidden = true
            //            cell.otherUserProfileButton.tag = indexPath.row
            //            cell.otherUserProfileButton.addTarget(self, action: #selector(OtherProfileAction(_:)), for: .touchUpInside)
        }
        else if trendData.data[indexPath.row].AreYouFollowingTrender == 2{
            cell.followStatusButton.isHidden = false
            cell.followStatusButton.setTitle(MySingleton.shared.selectedLangData.Requested, for: .normal)
            cell.followStatusButton.setTitleColor(UIColor.lightGray, for: .normal)
            cell.followStatusButton.tag = indexPath.row
            cell.followStatusButton.addTarget(self, action: #selector(FollowButtonAction(_:)), for: .touchUpInside)
            cell.otherUserProfileButton.isHidden = true
            //            cell.otherUserProfileButton.tag = indexPath.row
            //            cell.otherUserProfileButton.addTarget(self, action: #selector(OtherProfileAction(_:)), for: .touchUpInside)
        }
        else
        {
            cell.followStatusButton.isHidden = true
            cell.otherUserProfileButton.isHidden = true
            //            cell.otherUserProfileButton.tag = indexPath.row
            //            cell.otherUserProfileButton.addTarget(self, action: #selector(OtherProfileAction(_:)), for: .touchUpInside)
        }
        
        if trendData.data[indexPath.row].EnableTrendLike == true {
            cell.likeButton.isHidden = false
        }
        else {
            cell.likeButton.isHidden = true
        }
        if trendData.data[indexPath.row].IsTrendLike == true {
            cell.trendLikesImageView.image = #imageLiteral(resourceName: "Like_clk")
        }
        else {
            cell.trendLikesImageView.image = #imageLiteral(resourceName: "Like")
        }
        cell.likeButton.accessibilityIdentifier = String(indexPath.row)
        cell.likeButton.tag = trendData.data[indexPath.row].TrendId
        cell.likeButton.addTarget(self, action: #selector(didTapLikeButton), for: .touchUpInside)
        cell.trendImageCollectionView.tag = indexPath.row
        cell.trendImageCollectionView.delegate = self
        cell.trendImageCollectionView.dataSource = self
        cell.trendImagesPageControl.numberOfPages = trendData.data[indexPath.row].ImageList!.count //trendImagesCollectionArray[indexPath.row].count //trendData.data[indexPath.row].ImageList.count
        cell.trendImagesPageControl.hidesForSinglePage = true
        trendImagesPageControl = cell.trendImagesPageControl
        trendCurrentImageLabel = cell.trendCurrentImageLabel
        cell.trendImageCollectionView.reloadData()
        cell.trendImageCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: true)
        
        
        cell.trendCurrentImageLabel.text = "1/\(trendData.data[indexPath.row].ImageList!.count)"
        
        cell.addCommentButton.tag = indexPath.row
        cell.addCommentButton.addTarget(self, action: #selector(self.addCommentAction(_:)), for: .touchUpInside)
        
        if(trendData.data[indexPath.row].ImageList!.count > 1)
        {
            cell.trendCurrentImageLabel.isHidden = false
        }
        else
        {
            cell.trendCurrentImageLabel.isHidden = true
        }
        
        if trendData.data[indexPath.row].ImageList![0].Media == "VIDEO"{
            cell.videoURL = trendData.data[indexPath.row].ImageList![0].VideoUrl
            cell.cnstrntVideoWidth.constant = cell.trendStatusView.frame.size.width/4
            cell.trendViewsView.isHidden = false
            cell.imgVideoIcon.isHidden = false
        }
        else
        {
            cell.videoURL = nil
            cell.cnstrntVideoWidth.constant = 0
            cell.trendViewsView.isHidden = true
            cell.imgVideoIcon.isHidden = true
        }
        
        
        
        cell.trendMoreButton.tag = indexPath.row
        cell.trendMoreButton.addTarget(self, action: #selector(trendMoreButtonAction(_:)), for: .touchUpInside)
        
        cell.shareCountButton.tag = indexPath.row
        cell.shareCountButton.addTarget(self, action: #selector(shareCountButtonAction(_:)), for: .touchUpInside)
        
        cell.btnShareTrend.tag = indexPath.row
        cell.btnShareTrend.addTarget(self, action: #selector(btnShareTrendAction(_:)), for: .touchUpInside)
        
        cell.likeCountButton.tag = indexPath.row
        cell.likeCountButton.addTarget(self, action: #selector(likeCountButtonAction(_:)), for: .touchUpInside)
        cell.viewsCountButton.tag = indexPath.row
        cell.viewsCountButton.addTarget(self, action: #selector(viewsCountButtonAction(_:)), for: .touchUpInside)
        
        cell.txtAddComment.delegate = self
        cell.txtAddComment.returnKeyType = .send
        cell.txtAddComment.tag = indexPath.row
        if MySingleton.shared.loginObject.ProfilePic != ""
        {
            cell.profileButton.layer.cornerRadius = 0.5 * cell.profileButton.bounds.size.width
            cell.profileButton.clipsToBounds = true
            cell.profileButton.sd_setImage(with: URL(string: MySingleton.shared.loginObject.ProfilePic!), for: .normal)
        }
        else
        {
            //            cell.profileButton.layer.cornerRadius = 0.5 * cell.profileButton.bounds.size.width
            //            cell.profileButton.clipsToBounds = true
            cell.profileButton.setImage(#imageLiteral(resourceName: "img_selectProfilePic"), for: .normal)
        }
        
        cell.selectedLang(trendData: self.trendData,index:indexPath.row)
        
        //        cell.setNeedsLayout()
        //        cell.layoutIfNeeded()
        
        return cell
    }
    
    @objc func shareCountButtonAction(_ sender : UIButton)
    {
        let dataToSend = ["TrendId" : self.trendData.data[sender.tag].TrendId, "Type": "ShareCount"] as [String : Any]
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let likeListVC = storyboard.instantiateViewController(withIdentifier:"LikesListViewController") as! LikesListViewController
        
        likeListVC.trendData = dataToSend
        //        likeListVC.trendId = self.trendData.data[sender.tag].TrendId
        self.navigationController?.pushViewController(likeListVC, animated: true)
    }
    
    @objc func btnShareTrendAction(_ sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "ShareInternalViewController") as! ShareInternalViewController
        destination.trendId = self.trendData.data[sender.tag].TrendId
        let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height)
        self.navigationController?.present(pop, animated: true, completion: nil)
    }
   
}

extension TravelTrendViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return trendData.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        return configTrendTableViewCell(tableView: tableView, withIndexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if trendData.data[indexPath.row].IsGoogleAd {
            return CGFloat(googleAdHeight)
        }else {
            var calculatedWidth = CGFloat()
            var calculatedHeight = CGFloat()
            calculatedWidth = self.view.frame.size.width
            
            calculatedHeight = ((CGFloat(trendData.data[indexPath.row].ImageList![0].ImageHeight ?? 250)) * calculatedWidth) / (CGFloat(trendData.data[indexPath.row].ImageList![0].ImageWidth ?? 250))
            if trendData.data[indexPath.row].IsTrendShared == false{
                
                if trendData.data[indexPath.row].IsCustomAd{ // Custom Ad
                    return calculatedHeight
                }
                return 286 + calculatedHeight - 61
            }else{
                return 286 + calculatedHeight - 28
            }
        }
    }
    
    @objc func addCommentAction(_ sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "Comment", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "CommentViewController") as! CommentViewController
        destination.trendId = trendData.data[sender.tag].TrendId
        self.navigationController?.pushViewController(destination, animated: true)
    }
    
    @objc func viewsCountButtonAction(_ sender : UIButton)
    {
        let dataToSend = ["TrendId" : self.trendData.data[sender.tag].TrendId, "Type": "ViewsCount"] as [String : Any]
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let profileVC = storyboard.instantiateViewController(withIdentifier: "LikesListViewController") as! LikesListViewController
        profileVC.trendData = dataToSend
        self.navigationController?.pushViewController(profileVC, animated: true)
        //        self.performSegue(withIdentifier: "DashboardToLikesCount", sender: dataToSend)
    }
    
    @objc func OtherProfileAction(_ sender : UIButton)
    {
        let storyboard = UIStoryboard(name: "ProfileSB", bundle: nil)
        let profileVC = storyboard.instantiateViewController(withIdentifier: "OtherUserProfileVC") as! OtherUserProfileVC
        profileVC.userId = trendData.data[sender.tag].UserProfileId
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    @objc func FollowButtonAction(_ sender : UIButton)
    {
        if sender.currentTitle == MySingleton.shared.selectedLangData.follow{
            //follow api
            callApiForFavouriteUnfavourite(urlParam: String(trendData.data[sender.tag].UserProfileId), service: .followUser, index: sender.tag)
        }
        else if sender.currentTitle == MySingleton.shared.selectedLangData.following
        {
            //unfollow api
            callApiForFavouriteUnfavourite(urlParam: String(trendData.data[sender.tag].UserProfileId), service: .unfollowUser, index: sender.tag)
        }
    }
    
    @objc func likeCountButtonAction(_ sender : UIButton)
    {
        
        
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let likeListVC = storyboard.instantiateViewController(withIdentifier:"LikesListViewController") as! LikesListViewController
        let dataToSend = ["TrendId" : self.trendData.data[sender.tag].TrendId, "Type": "LikesCount"] as [String : Any]
        likeListVC.trendData = dataToSend
        //        likeListVC.trendId = self.trendData.data[sender.tag].TrendId
        self.navigationController?.pushViewController(likeListVC, animated: true)
        
    }
    
    
    
    func callApiForFavouriteUnfavourite(urlParam : String, service : UserServices, index : Int){
        WebServices().callUserService(service: service, urlParameter: urlParam, parameters: nil, isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
            if service == .markTrendFavourite{
                self.trendData.data[index].IsTrendFavourite = true
            }
            else if service == .markTrendUnfavourite
            {
                self.trendData.data[index].IsTrendFavourite = false
            }
            else if service == .followUser{
                self.trendData.data[index].AreYouFollowingTrender = 2
                let cell = self.tblTrend.cellForRow(at: IndexPath(row: index, section: 0))as! TrendTableViewCell
                cell.followStatusButton.setTitle(MySingleton.shared.selectedLangData.Requested, for: .normal)
                cell.followStatusButton.setTitleColor(UIColor.lightGray, for: .normal)
                
            }
            else if service == .unfollowUser{
                self.trendData.data[index].AreYouFollowingTrender = 1
                let cell = self.tblTrend.cellForRow(at: IndexPath(row: index, section: 0))as! TrendTableViewCell
                cell.followStatusButton.setTitle(MySingleton.shared.selectedLangData.follow, for: .normal)
                cell.followStatusButton.setTitleColor(UIColor.lightGray, for: .normal)
            }
            else if service == .setTrendPushNotificationStatus
            {
                if(serviceResponse["Status"] as! Bool == true)
                {
                    if (serviceResponse["Message"] as! String).contains("disabled")
                    {
                        self.trendData.data[index].IsPushNotificationEnabled = false
                    }
                    else
                    {
                        self.trendData.data[index].IsPushNotificationEnabled = true
                    }
                }
                else
                {
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }
            
        })
    }
    
    @objc func trendMoreButtonAction(_ sender : UIButton)
    {
        let cell = tblTrend.cellForRow(at: IndexPath(row: sender.tag, section: 0))as! TrendTableViewCell
        
        cell.trendDropDown.anchorView = sender
        cell.trendDropDown.bottomOffset = CGPoint(x: 0, y:(cell.trendDropDown.anchorView?.plainView.bounds.height)!)
        cell.trendDropDown.topOffset = CGPoint(x: 0, y:-(cell.trendDropDown.anchorView?.plainView.bounds.height)!)
        
        cell.trendDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            print(sender.tag)
            
            switch index{
            case 0:
                let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
                let destination = storyboard.instantiateViewController(withIdentifier: "ShareInternalViewController") as! ShareInternalViewController
                destination.trendId = self.trendData.data[sender.tag].TrendId
                let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height)
                self.navigationController?.present(pop, animated: true, completion: nil)
                
            case 2:
                if item == "Favourite"{
                    self.callApiForFavouriteUnfavourite(urlParam: String(self.trendData.data[sender.tag].TrendId), service: .markTrendFavourite, index: sender.tag)
                }
                else
                {
                    self.callApiForFavouriteUnfavourite(urlParam: String(self.trendData.data[sender.tag].TrendId), service: .markTrendUnfavourite, index: sender.tag)
                }
                
            case 3:
                self.callApiForFavouriteUnfavourite(urlParam: String(self.trendData.data[sender.tag].TrendId), service: .setTrendPushNotificationStatus, index: sender.tag)
                
            case 4:
                let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
                let destination = storyboard.instantiateViewController(withIdentifier: "ReportTrendViewController") as! ReportTrendViewController
                let pop = PopUpViewController(destination,withHeight: 270.0)
                self.navigationController?.present(pop, animated: true, completion: nil)
                ////                self.navigationController?.present(destination, animated: true, completion: nil)
                //                self.present(destination, animated: true, completion: nil)
                
            case 5:
                let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
                let destination = storyboard.instantiateViewController(withIdentifier: "SelectFolderViewController") as! SelectFolderViewController
                destination.createFolderPopupDelegate = self
                self.saveFolderTrendId = self.trendData.data[sender.tag].TrendId
                destination.trendId = self.trendData.data[sender.tag].TrendId
                destination.shareTrendId = self.trendData.data[sender.tag].TrendShareId
                
                self.shareTrendId = self.trendData.data[sender.tag].TrendShareId
                let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height * 0.7)
                self.navigationController?.present(pop, animated: true, completion: nil)
                
            case 6: //break
                let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
                let destination = storyboard.instantiateViewController(withIdentifier: "SetTrendViewController") as! SetTrendViewController
                destination.editTrendParam = self.trendData.data[sender.tag]
                destination.isEdit = true
                //  (self.tabBarController!).selectedIndex = 2
                self.navigationController?.pushViewController(destination, animated: true)
                
            case 7:
                
                let actionSheetControllerIOS8: UIAlertController = UIAlertController(title: MySingleton.shared.selectedLangData.confirmation, message: MySingleton.shared.selectedLangData.Are_you_sure_you_want_to_delete_this_trend, preferredStyle: .alert)
                let deleteActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.delete, style: .default) { void in
                    self.callApiForFavouriteUnfavourite(urlParam: String(self.trendData.data[sender.tag].TrendId), service: .deleteTrend, index: sender.tag)
                }
                let cancelActionButton: UIAlertAction = UIAlertAction(title: MySingleton.shared.selectedLangData.cancel, style: .cancel) { void in
                }
                actionSheetControllerIOS8.addAction(deleteActionButton)
                actionSheetControllerIOS8.addAction(cancelActionButton)
                actionSheetControllerIOS8.popoverPresentationController?.sourceView = self.view
                actionSheetControllerIOS8.popoverPresentationController?.sourceRect = self.view.bounds
                self.present(actionSheetControllerIOS8, animated: true, completion: nil)
                
            default:
                break
            }
        }
        
       cell.trendMoreButtonAction(index: sender.tag, trendData: self.trendData)
    }
    
    
    @objc func didTapLikeButton(button: UIButton) {
        var isTrendLiked = Bool()
        //        for trend in self.trendData.data {
        //            if trend.TrendId == button.tag {
        //                isTrendLiked = trend.IsTrendLike
        //            }
        //        }
        isTrendLiked = (self.trendData.data[Int(button.accessibilityIdentifier!)!].IsTrendLike)
        if isTrendLiked == true {
            dislikeTrend(trendID: "\(button.tag)", index: button.accessibilityIdentifier!)
        }
        else {
            likeTrend(trendID: "\(button.tag)", index: button.accessibilityIdentifier!)
        }
    }
    
    func likeTrend(trendID: String, index: String) {
        WebServices().callUserService(service: .likeUnlikeTrend, urlParameter: "\(trendID)", parameters: nil, isLazyLoading: false, isHeader: true, CallMethod: .post) { (serviceResponse, responseData) in
            
            print("Like Trend Response --- \(serviceResponse)")
            if serviceResponse["Status"] == nil {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                return
            }
            if(serviceResponse["Status"] as! Bool == true)
            {
                DispatchQueue.main.async {
                    self.trendData.data[Int(index)!].IsTrendLike = true
                    self.trendData.data[Int(index)!].TrendLikesCount = self.trendData.data[Int(index)!].TrendLikesCount + 1
                    let cellIndexPath = IndexPath(row: Int(index)!, section: 0)
                    let cell = self.tblTrend.cellForRow(at: cellIndexPath)as? TrendTableViewCell
                    cell!.trendLikesImageView.image = #imageLiteral(resourceName: "Like_clk")
                    cell!.likesCountLabel.text = "\(self.trendData.data[Int(index)!].TrendLikesCount)"
                }
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    func dislikeTrend(trendID: String, index: String) {
        WebServices().callUserService(service: .likeUnlikeTrend, urlParameter: "\(trendID)", parameters: nil, isLazyLoading: false, isHeader: true, CallMethod: .post) { (serviceResponse, responseData) in
            
            print("Dislike Trend Response --- \(serviceResponse)")
            if serviceResponse["Status"] == nil {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                return
            }
            if(serviceResponse["Status"] as! Bool == true)
            {
                
                DispatchQueue.main.async {
                    self.trendData.data[Int(index)!].IsTrendLike = false
                    self.trendData.data[Int(index)!].TrendLikesCount = self.trendData.data[Int(index)!].TrendLikesCount - 1
                    let cellIndexPath = IndexPath(row: Int(index)!, section: 0)
                    let cell = self.tblTrend.cellForRow(at: cellIndexPath)as? TrendTableViewCell
                    cell!.trendLikesImageView.image = #imageLiteral(resourceName: "Like")
                    cell!.likesCountLabel.text = "\(self.trendData.data[Int(index)!].TrendLikesCount)"
                }
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        }
    }
    
    
    
    func registerCollectionViewCells() {
        trendImagesCollectionView.register(UINib(nibName: "ImageTrendCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageTrendCollectionViewCell")
    }
    
    
}



extension TravelTrendViewController:UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      
            print("Trend Image Collection View Tag --- \(collectionView.tag) --- Count --- \(trendData.data[collectionView.tag].ImageList!.count)")
            return self.trendData.data[collectionView.tag].ImageList!.count//trendImagesCollectionArray[collectionView.tag].count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
     
            return configTrendImagesCollectionViewCell(collectionView: collectionView, withIndexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let fullScreen = storyboard.instantiateViewController(withIdentifier:"FullScreenViewController") as! FullScreenViewController
        fullScreen.cellIndex = collectionView.tag
        fullScreen.delegate = self
        fullScreen.imageList = trendData.data[collectionView.tag].ImageList!
        self.navigationController?.pushViewController(fullScreen, animated: true)
        
    }
    
    func configTrendImagesCollectionViewCell(collectionView: UICollectionView, withIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = "ImageTrendCollectionViewCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! ImageTrendCollectionViewCell
        cell.trendImageView.sd_setImage(with: URL(string: trendData.data[collectionView.tag].ImageList![indexPath.item].TrendImage), placeholderImage: #imageLiteral(resourceName: "logo_navigation"))
        return cell
    }
    
    func configHomeCollectionViewCell(collectionView: UICollectionView, withIndexPath indexPath: IndexPath) -> UICollectionViewCell {
        let cellIdentifier = "HomeCollectionViewCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! HomeCollectionViewCell
        
        cell.trendImageView.image = testImageArray[indexPath.item]
        cell.trendImageView.contentMode = .scaleAspectFill
        collectionView.backgroundColor = .red
        cell.backgroundColor = .white
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
     
            var calculatedWidth = CGFloat()
            var calculatedHeight = CGFloat()
            calculatedWidth = self.view.frame.size.width
            calculatedHeight = ((CGFloat(trendData.data[collectionView.tag].ImageList![0].ImageHeight ?? 250)) * calculatedWidth) / (CGFloat(trendData.data[collectionView.tag].ImageList![0].ImageWidth ?? 250))
            return CGSize(width: calculatedWidth, height: calculatedHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
     
            if trendData.data.count > 0 {
                let cell = tblTrend.cellForRow(at: IndexPath(row: collectionView.tag, section: 0)) as? TrendTableViewCell
                if cell != nil {
                    cell!.trendImagesPageControl.currentPage = indexPath.item
                    cell!.trendCurrentImageLabel.text = "\(indexPath.item + 1)/\(trendData.data[collectionView.tag].ImageList!.count)"
                }
                
            }
        }
}
extension TravelTrendViewController : CreateFolderPopupDelegate{
    func createFolderPopup() {
        let storyboard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
        let nextViewController = storyboard.instantiateViewController(withIdentifier: "NewFolderPoupVC") as! NewFolderPoupVC
        nextViewController.delegate = self
        let pop = PopUpViewController(nextViewController,withHeight: self.view.frame.size.height)
        self.navigationController?.present(pop, animated: true, completion: nil)
    }
    
}

extension TravelTrendViewController : CreateFolderDelegate {
    func controller() {
        let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
        let destination = storyboard.instantiateViewController(withIdentifier: "SelectFolderViewController") as! SelectFolderViewController
        destination.createFolderPopupDelegate = self
        destination.trendId = self.saveFolderTrendId
        destination.shareTrendId = self.shareTrendId
        
        let pop = PopUpViewController(destination,withHeight: self.view.frame.size.height * 0.7)
        self.navigationController?.present(pop, animated: true, completion: nil)
    }
}

extension TravelTrendViewController : TrendViewDelegate{
    func IncreaseTrendViewCount(Index: Int) {
        let cell = tblTrend.cellForRow(at: IndexPath(row: Index, section: 0))as? TrendTableViewCell
        trendData.data[Index].TrendViewCount = String (Int(trendData.data[Index].TrendViewCount!)! + 1)
        cell!.viewsCountLabel.text = "\(trendData.data[Index].TrendViewCount)"
    }
}
