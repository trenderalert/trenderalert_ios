//
//  SideMenuViewController.swift
//  TrenderAlert
//
//  Created by OSP LABS on 27/12/18.
//  Copyright © 2018 Single. All rights reserved.
//

import UIKit
import MFSideMenu

class SideMenuViewController: UIViewController {

    @IBOutlet var btnSettings: UIButton!
    @IBOutlet var btnLogout: UIButton!
    @IBOutlet var lblTrendSetterSt: UILabel!
    @IBOutlet var lblTrenderSt: UILabel!
    @IBOutlet var lblPostSt: UILabel!
    @IBOutlet var tblMenuList: UITableView!
    @IBOutlet var imgProfile: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblTrend: UILabel!
    @IBOutlet var lblTrender: UILabel!
    @IBOutlet var lblSetter: UILabel!
    @IBOutlet weak var sideMenuTableView: UITableView!
    
    var selectedRow = 0
    var userData = UserInfo()
    
    var sidemenuContentArray = [MySingleton.shared.selectedLangData.home, MySingleton.shared.selectedLangData.trend_alerts, MySingleton.shared.selectedLangData.trender_travel, MySingleton.shared.selectedLangData.friends, MySingleton.shared.selectedLangData.groups, MySingleton.shared.selectedLangData.Go_premium, MySingleton.shared.selectedLangData.invite_contacts, MySingleton.shared.selectedLangData.Languages, MySingleton.shared.selectedLangData.switch_to_business_account,MySingleton.shared.selectedLangData.contest,MySingleton.shared.selectedLangData.contest_winners, MySingleton.shared.selectedLangData.create_advertisement,MySingleton.shared.selectedLangData.terms_services, MySingleton.shared.selectedLangData.contact_us,MySingleton.shared.selectedLangData.about_this_app]
    var sidemenuImageArray = [UIImage(named: "Home"), #imageLiteral(resourceName: "News Categories"), #imageLiteral(resourceName: "trender_travel_dft"), UIImage(named: "Friend_dft"), UIImage(named: "Groups_dft"),#imageLiteral(resourceName: "About this App"), UIImage(named: "Invite_Contact_dft"), UIImage(named: "language_dft"), #imageLiteral(resourceName: "Change Password"),#imageLiteral(resourceName: "Monthly Contest"),#imageLiteral(resourceName: "Contest Winners"),#imageLiteral(resourceName: "Change Password"), #imageLiteral(resourceName: "Change Password"), #imageLiteral(resourceName: "Change Password"),UIImage(named: "language_dft")]
    var sidemenuSelectedImageArray = [UIImage(named: "Home_Y"), UIImage(named: "trender_alert_clk"), #imageLiteral(resourceName: "trender_travel_dft"), UIImage(named: "Friend_clk"), UIImage(named: "Groups_clk"), UIImage(named: "About this App_Y"), UIImage(named: "Invite_Contact_clk"), UIImage(named: "language_dft"), #imageLiteral(resourceName: "Change Password"),#imageLiteral(resourceName: "Monthly Contest"),#imageLiteral(resourceName: "Contest Winners Y"),#imageLiteral(resourceName: "Change Password"), #imageLiteral(resourceName: "Change Password"), #imageLiteral(resourceName: "Change Password"), #imageLiteral(resourceName: "About this App")]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imgProfile.makeCornerRadius(radius: self.imgProfile.layer.frame.height/2)
        let footerView = UIView()
        footerView.backgroundColor = .clear
        sideMenuTableView.tableFooterView = footerView
        lblName.text = MySingleton.shared.loginObject.userName
        
        lblTrendSetterSt.text = MySingleton.shared.selectedLangData.post_trend
        lblPostSt.text = MySingleton.shared.selectedLangData.post
        lblTrenderSt.text = MySingleton.shared.selectedLangData.trender
        btnLogout.setTitle(MySingleton.shared.selectedLangData.logout, for: .normal)
        btnSettings.setTitle(MySingleton.shared.selectedLangData.settings, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getUserInfo()
        
        
       // self.imgProfile.sd_setImage(with: URL(string: MySingleton.shared.loginObject.ProfilePic), placeholderImage: #imageLiteral(resourceName: "img_selectProfilePic"))
    }
    
    override func viewDidLayoutSubviews() {
         self.imgProfile.makeCornerRadius(radius: self.imgProfile.layer.frame.height/2)
    }
    
//    override func viewDidDisappear(_ animated: Bool) {
//        self.tblMenuList.reloadData()
//    }
    
    func langData()
    {
        sidemenuContentArray = [MySingleton.shared.selectedLangData.home, MySingleton.shared.selectedLangData.trend_alerts, MySingleton.shared.selectedLangData.trender_travel, MySingleton.shared.selectedLangData.friends, MySingleton.shared.selectedLangData.groups, MySingleton.shared.selectedLangData.Go_premium, MySingleton.shared.selectedLangData.invite_contacts, MySingleton.shared.selectedLangData.Languages, MySingleton.shared.selectedLangData.switch_to_business_account,MySingleton.shared.selectedLangData.contest,MySingleton.shared.selectedLangData.contest_winners, MySingleton.shared.selectedLangData.create_advertisement,MySingleton.shared.selectedLangData.terms_services, MySingleton.shared.selectedLangData.contact_us,MySingleton.shared.selectedLangData.about_this_app]
        lblTrendSetterSt.text = MySingleton.shared.selectedLangData.post_trend
        lblPostSt.text = MySingleton.shared.selectedLangData.post
        lblTrenderSt.text = MySingleton.shared.selectedLangData.trender
        btnLogout.setTitle(MySingleton.shared.selectedLangData.logout, for: .normal)
        btnSettings.setTitle(MySingleton.shared.selectedLangData.settings, for: .normal)
        print(sidemenuContentArray)
        DispatchQueue.main.async {
            self.tblMenuList.delegate = self
            self.tblMenuList.dataSource = self
             self.tblMenuList.reloadData()
        }
       
    }
    
    func getUserInfo(){
        WebServices().callUserService(service: UserServices.getUserInfo, urlParameter: "", parameters: ["UserID":MySingleton.shared.loginObject.UserID as AnyObject], isLazyLoading: false, isHeader: true, CallMethod: .post, actionAfterServiceResponse: { (serviceResponse, serviceData) in
            print(serviceResponse)
            
            if serviceResponse["Status"] != nil{
                if (serviceResponse["Status"] as! Bool == true){
                    self.userData = WebServices().decodeDataToClass(data: serviceData, decodeClass: UserInfo.self)!
                    if self.userData.data.count != 0
                    {
                        DispatchQueue.main.async {
                            self.lblTrend.text = "\(self.userData.data[0].PostedTrends)"
                            self.lblTrender.text = "\(self.userData.data[0].TotalFollower)"
                            self.lblSetter.text = "\(self.userData.data[0].TotalFollowings)"
                            self.lblName.text = "\(self.userData.data[0].FirstName ?? "") \(self.userData.data[0].LastName ?? "")"
                            self.imgProfile.sd_setImage(with: URL(string: "\(String(self.userData.data[0].UserProfileImage ?? ""))"), placeholderImage: #imageLiteral(resourceName: "img_selectProfilePic"))
                            if ((serviceResponse["data"]as! [AnyObject])[0]as! [String:Any])["Language"]is String
                            {
                                UserDefaults.standard.setValue(((serviceResponse["data"]as! [AnyObject])[0]as! [String:Any])["Language"]as! String, forKey: "SelectedLang")
                            }
                            else
                            {
                                UserDefaults.standard.setValue("english", forKey: "SelectedLang")
                            }
                            
                                if let path = Bundle.main.path(forResource: UserDefaults.standard.object(forKey: "SelectedLang") as? String , ofType: "json", inDirectory: "trender_alert") {
                                    do {
                                        let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
                                        let jsonResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                                        let jsonData = try JSONSerialization.data(withJSONObject: jsonResult, options: .prettyPrinted)
                                        print(jsonResult)
                                        var wordList = LanguagesModel()
                                        
                                        wordList = WebServices().decodeDataToClass(data: jsonData, decodeClass: LanguagesModel.self)!
                                        MySingleton.shared.selectedLangData = wordList
                                        
                                        // do stuff
                                    } catch {
                                        // handle error
                                    }
//                                    let side_controller = self.menuContainerViewController.leftMenuViewController as! SideMenuViewController
                                    self.langData()
                                }
                           
                            
                        }
                    }
                    
                }else{
                    TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
                }
            }else{
                TrenderAlertVC.shared.presentAlertController(message: serviceResponse["Message"] as! String, completionHandler: nil)
            }
        })
    }
    
    
    @IBAction func didTapLogoutButton(_ sender: UIButton) {
        UserDefaults.standard.set(nil, forKey: "access_token")
//        self.performSegue(withIdentifier: "SideMenuToSwitchUser", sender: self)
        let appDelegatge = UIApplication.shared.delegate as! AppDelegate
        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let loginNavigationController = mainStoryBoard.instantiateViewController(withIdentifier: "switchUserNavigationController") as! UINavigationController
        appDelegatge.window!.rootViewController = loginNavigationController
        appDelegatge.window!.makeKeyAndVisible()
    }
    
    @IBAction func didTapSettingsButton(_ sender: UIButton) {
        MySingleton.shared.isFirstTimeSideMenu = false
        let storyboard = UIStoryboard(name: "SettingsSB", bundle: nil)
        let settingsVC = storyboard.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        let tabBar = self.menuContainerViewController.centerViewController as! UITabBarController
        let navigationController = tabBar.selectedViewController as! UINavigationController
        navigationController.viewControllers = [settingsVC]
        self.menuContainerViewController.menuState = MFSideMenuStateClosed
    }
    
}

extension SideMenuViewController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sidemenuContentArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "SideMenuTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! SideMenuTableViewCell
        cell.menuTitleLabel.text = sidemenuContentArray[indexPath.row]
        cell.menuImageView.image = sidemenuImageArray[indexPath.row]
        cell.backgroundColor = UIColor.clear
        cell.menuTitleLabel.textColor = UIColor.black
        cell.view_tapChange.backgroundColor = UIColor.clear
        cell.menuCountLabel.isHidden = true
        if self.selectedRow == indexPath.row {
            cell.menuImageView.image = sidemenuSelectedImageArray[indexPath.row]
//            cell.backgroundColor = UIColor(red: 66/255, green: 66/255, blue: 66/255, alpha: 1)
            cell.view_tapChange.backgroundColor = UIColor(red: 254/255, green: 194/255, blue: 16/255, alpha: 1)
            cell.menuTitleLabel.textColor = UIColor.baseColor
        }else{
            cell.menuImageView.image = sidemenuImageArray[indexPath.row]
//            cell.backgroundColor = UIColor.clear
            cell.menuTitleLabel.textColor = UIColor.black
            cell.view_tapChange.backgroundColor = UIColor.clear
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let viewIdentifier = "SideMenuFooterTableViewCell"
        let footerView = tableView.dequeueReusableCell(withIdentifier: viewIdentifier) as! SideMenuFooterTableViewCell
        return footerView.contentView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//                if indexPath.row == 12 || indexPath.row == 13 || indexPath.row == 14{
//                    return
//                }
        
        self.selectedRow = indexPath.row
        let cell = tableView.cellForRow(at: indexPath) as! SideMenuTableViewCell
        //        cell.backgroundColor = UIColor(red: 66/255, green: 66/255, blue: 66/255, alpha: 1)
        cell.view_tapChange.backgroundColor = UIColor(red: 254/255, green: 194/255, blue: 16/255, alpha: 1)
        cell.menuTitleLabel.textColor = UIColor.baseColor
        if(!(selectedRow == indexPath.row))
        {
            tableView.deselectRow(at: IndexPath(row: selectedRow, section: 0), animated: true)
            if let cellToRemove = tableView.cellForRow(at: IndexPath(row: selectedRow, section: 0)) as? SideMenuTableViewCell
            {
                cell.menuImageView.image = sidemenuImageArray[indexPath.row]
                cellToRemove.backgroundColor = UIColor.clear
                //                cellToRemove.menuTitleLabel.textColor = UIColor.black
                cellToRemove.view_tapChange.backgroundColor = UIColor.clear
            }
        }
        switch indexPath.row {
        case 0:
            // Home
            if MySingleton.shared.isFirstTimeSideMenu == true {
                self.menuContainerViewController.menuState = MFSideMenuStateClosed
            }
            else {
                
                let appDelegatge = UIApplication.shared.delegate as! AppDelegate
                let storyBoard = UIStoryboard(name: "Dashboard", bundle: nil)
                let centerViewController = storyBoard.instantiateViewController(withIdentifier: "TrendAlertTabBarController") as! UITabBarController
                let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
                let leftViewController = mainStoryBoard.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
                appDelegatge.container.leftMenuViewController = leftViewController
                appDelegatge.container.centerViewController = centerViewController
                appDelegatge.container.leftMenuWidth = (centerViewController.view.frame.size.width / 4) * 3
                appDelegatge.window!.rootViewController = appDelegatge.container
                appDelegatge.window!.makeKeyAndVisible()
                
            }
            break
        case 1:
            // Trend Alerts
            MySingleton.shared.isFirstTimeSideMenu = false
            let setAlertVC = self.storyboard?.instantiateViewController(withIdentifier: "SetAlertViewController") as! SetAlertViewController
            let tabBar  = self.menuContainerViewController.centerViewController as! UITabBarController
            let navigationController = tabBar.selectedViewController as! UINavigationController
            navigationController.viewControllers = [setAlertVC]
            self.menuContainerViewController.menuState = MFSideMenuStateClosed
            break
        case 2:
            
            MySingleton.shared.isFirstTimeSideMenu = false
            let storyboard = UIStoryboard(name: "MyTrendsSB", bundle: nil)
            let friendsVC = storyboard.instantiateViewController(withIdentifier: "TrenderTravelVC") as! TrenderTravelVC
            //friendsVC.backToSideMenu = true
            let tabBar  = self.menuContainerViewController.centerViewController as! UITabBarController
            let navigationController = tabBar.selectedViewController as! UINavigationController
            navigationController.viewControllers = [friendsVC]
            self.menuContainerViewController.menuState = MFSideMenuStateClosed
            
            break
        case 3:
            
            if MySingleton.shared.loginObject.IsPremiumUser == "True"{
                MySingleton.shared.isFirstTimeSideMenu = false
                let storyboard = UIStoryboard(name: "FriendsSB", bundle: nil)
                let friendsVC = storyboard.instantiateViewController(withIdentifier: "FriendsVC") as! FriendsVC
                friendsVC.backToSideMenu = true
                let tabBar  = self.menuContainerViewController.centerViewController as! UITabBarController
                let navigationController = tabBar.selectedViewController as! UINavigationController
                navigationController.viewControllers = [friendsVC]
                self.menuContainerViewController.menuState = MFSideMenuStateClosed
            }else{
                //  TrenderAlertVC.shared.presentAlertController(message: "This feature is for premium members only.", completionHandler: nil)
                TrenderAlertVC.shared.presentAlertController(message: "This feature is for premium members only.") {
                    MySingleton.shared.isFirstTimeSideMenu = false
                    let storyboard = UIStoryboard(name: "NotificationSB", bundle: nil)
                    let paymentVC = storyboard.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
                    let tabBar = self.menuContainerViewController.centerViewController as! UITabBarController
                    let navigationController = tabBar.selectedViewController as! UINavigationController
                    navigationController.viewControllers = [paymentVC]
                    self.menuContainerViewController.menuState = MFSideMenuStateClosed
                }
            }
            
            break
        case 4:
            print("Group : ",MySingleton.shared.loginObject.IsPremiumUser)
            if MySingleton.shared.loginObject.IsPremiumUser == "True"{
                MySingleton.shared.isFirstTimeSideMenu = false
                let storyboard = UIStoryboard(name: "GroupSB", bundle: nil)
                let groupVC = storyboard.instantiateViewController(withIdentifier: "GroupVC") as! GroupVC
                let tabBar = self.menuContainerViewController.centerViewController as! UITabBarController
                let navigationController = tabBar.selectedViewController as! UINavigationController
                navigationController.viewControllers = [groupVC]
                self.menuContainerViewController.menuState = MFSideMenuStateClosed
            }else{
                TrenderAlertVC.shared.presentAlertController(message: "This feature is for premium members only.") {
                    MySingleton.shared.isFirstTimeSideMenu = false
                    let storyboard = UIStoryboard(name: "NotificationSB", bundle: nil)
                    let paymentVC = storyboard.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
                    let tabBar = self.menuContainerViewController.centerViewController as! UITabBarController
                    let navigationController = tabBar.selectedViewController as! UINavigationController
                    navigationController.viewControllers = [paymentVC]
                    self.menuContainerViewController.menuState = MFSideMenuStateClosed
                }
            }
            break
        case 5:
            MySingleton.shared.isFirstTimeSideMenu = false
            let storyboard = UIStoryboard(name: "NotificationSB", bundle: nil)
            let paymentVC = storyboard.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
            let tabBar = self.menuContainerViewController.centerViewController as! UITabBarController
            let navigationController = tabBar.selectedViewController as! UINavigationController
            navigationController.viewControllers = [paymentVC]
            self.menuContainerViewController.menuState = MFSideMenuStateClosed
            break
            
            
        case 6:
            MySingleton.shared.isFirstTimeSideMenu = false
            let storyboard = UIStoryboard(name: "InviteContactSB", bundle: nil)
            let inviteContactsVC = storyboard.instantiateViewController(withIdentifier: "InviteContactsVC") as! InviteContactsVC
            let tabBar  = self.menuContainerViewController.centerViewController as! UITabBarController
            let navigationController = tabBar.selectedViewController as! UINavigationController
            navigationController.viewControllers = [inviteContactsVC]
            self.menuContainerViewController.menuState = MFSideMenuStateClosed
            break
            
        case 7:
            MySingleton.shared.isFirstTimeSideMenu = false
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let inviteContactsVC = storyboard.instantiateViewController(withIdentifier: "SetLangViewController") as! SetLangViewController
            let tabBar  = self.menuContainerViewController.centerViewController as! UITabBarController
            let navigationController = tabBar.selectedViewController as! UINavigationController
            navigationController.viewControllers = [inviteContactsVC]
            self.menuContainerViewController.menuState = MFSideMenuStateClosed
            break
            
        case 8:
            if MySingleton.shared.loginObject.IsPremiumUser == "True"{
                MySingleton.shared.isFirstTimeSideMenu = false
                let storyboard = UIStoryboard(name: "BusinessAccountDetailsSB", bundle: nil)
                let inviteContactsVC = storyboard.instantiateViewController(withIdentifier: "BusinessActivityVC") as! BusinessActivityVC
                let tabBar  = self.menuContainerViewController.centerViewController as! UITabBarController
                let navigationController = tabBar.selectedViewController as! UINavigationController
                navigationController.viewControllers = [inviteContactsVC]
                self.menuContainerViewController.menuState = MFSideMenuStateClosed
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: "This feature is for premium members only.") {
                    MySingleton.shared.isFirstTimeSideMenu = false
                    let storyboard = UIStoryboard(name: "NotificationSB", bundle: nil)
                    let paymentVC = storyboard.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
                    let tabBar = self.menuContainerViewController.centerViewController as! UITabBarController
                    let navigationController = tabBar.selectedViewController as! UINavigationController
                    navigationController.viewControllers = [paymentVC]
                    self.menuContainerViewController.menuState = MFSideMenuStateClosed
                }
            }
            break
            
        case 9:
            if MySingleton.shared.loginObject.IsPremiumUser == "True"{
                MySingleton.shared.isFirstTimeSideMenu = false
                let storyboard = UIStoryboard(name: "MonthlyContestSB", bundle: nil)
                let paymentVC = storyboard.instantiateViewController(withIdentifier: "MonthlyContestVC") as! MonthlyContestVC
                let tabBar = self.menuContainerViewController.centerViewController as! UITabBarController
                let navigationController = tabBar.selectedViewController as! UINavigationController
                navigationController.viewControllers = [paymentVC]
                self.menuContainerViewController.menuState = MFSideMenuStateClosed
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: "This feature is for premium members only.") {
                    MySingleton.shared.isFirstTimeSideMenu = false
                    let storyboard = UIStoryboard(name: "NotificationSB", bundle: nil)
                    let paymentVC = storyboard.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
                    let tabBar = self.menuContainerViewController.centerViewController as! UITabBarController
                    let navigationController = tabBar.selectedViewController as! UINavigationController
                    navigationController.viewControllers = [paymentVC]
                    self.menuContainerViewController.menuState = MFSideMenuStateClosed
                }
            }
            break
            
        case 10:
            
            if MySingleton.shared.loginObject.IsPremiumUser == "True"{
                MySingleton.shared.isFirstTimeSideMenu = false
                let storyboard = UIStoryboard(name: "MonthlyContestSB", bundle: nil)
                let paymentVC = storyboard.instantiateViewController(withIdentifier: "ContestWinnerListVC") as! ContestWinnerListVC
                let tabBar = self.menuContainerViewController.centerViewController as! UITabBarController
                let navigationController = tabBar.selectedViewController as! UINavigationController
                navigationController.viewControllers = [paymentVC]
                self.menuContainerViewController.menuState = MFSideMenuStateClosed
            }
            else
            {
                TrenderAlertVC.shared.presentAlertController(message: "This feature is for premium members only.") {
                    MySingleton.shared.isFirstTimeSideMenu = false
                    let storyboard = UIStoryboard(name: "NotificationSB", bundle: nil)
                    let paymentVC = storyboard.instantiateViewController(withIdentifier: "PaymentVC") as! PaymentVC
                    let tabBar = self.menuContainerViewController.centerViewController as! UITabBarController
                    let navigationController = tabBar.selectedViewController as! UINavigationController
                    navigationController.viewControllers = [paymentVC]
                    self.menuContainerViewController.menuState = MFSideMenuStateClosed
                }
            }
            
            break
            
        case 11:
            MySingleton.shared.isFirstTimeSideMenu = false
            let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
            let paymentVC = storyboard.instantiateViewController(withIdentifier: "AdListingViewController") as! AdListingViewController
            let tabBar = self.menuContainerViewController.centerViewController as! UITabBarController
            let navigationController = tabBar.selectedViewController as! UINavigationController
            navigationController.viewControllers = [paymentVC]
            self.menuContainerViewController.menuState = MFSideMenuStateClosed
            break
            
        case 12:
            MySingleton.shared.isFirstTimeSideMenu = false
            let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
            let paymentVC = storyboard.instantiateViewController(withIdentifier: "StaticPagesViewController") as! StaticPagesViewController
            paymentVC.index = 1
            paymentVC.titleStatic = "Terms of services"
            let tabBar = self.menuContainerViewController.centerViewController as! UITabBarController
            let navigationController = tabBar.selectedViewController as! UINavigationController
            navigationController.viewControllers = [paymentVC]
            self.menuContainerViewController.menuState = MFSideMenuStateClosed
            break
            
        case 13:
            MySingleton.shared.isFirstTimeSideMenu = false
            let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
            let paymentVC = storyboard.instantiateViewController(withIdentifier: "StaticPagesViewController") as! StaticPagesViewController
            paymentVC.index = 2
            paymentVC.titleStatic = "Contact Us"
            let tabBar = self.menuContainerViewController.centerViewController as! UITabBarController
            let navigationController = tabBar.selectedViewController as! UINavigationController
            navigationController.viewControllers = [paymentVC]
            self.menuContainerViewController.menuState = MFSideMenuStateClosed
            break
            
        case 14:
            MySingleton.shared.isFirstTimeSideMenu = false
            let storyboard = UIStoryboard(name: "Dashboard", bundle: nil)
            let paymentVC = storyboard.instantiateViewController(withIdentifier: "StaticPagesViewController") as! StaticPagesViewController
            paymentVC.index = 3
            paymentVC.titleStatic = "About Us"
            let tabBar = self.menuContainerViewController.centerViewController as! UITabBarController
            let navigationController = tabBar.selectedViewController as! UINavigationController
            navigationController.viewControllers = [paymentVC]
            self.menuContainerViewController.menuState = MFSideMenuStateClosed
            break
            
        default:
            return
        }
        
        tableView.reloadData()
    }
}
