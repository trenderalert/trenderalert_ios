//
//  TrendAlertTag.swift
//  TrenderAlert
//
//  Created by OSP LABS on 30/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class TrendAlertData: NSObject, Codable {
    var data = [TrendAlertTag]()
    
    enum CodingKeys: String, CodingKey {
        case data
    }
}

class TrendAlertTag: NSObject, Codable {

    var TagsId = 0
    var TagName = String()
    var UserProfileTagMappingId = 0
    
    enum CodingKeys: String, CodingKey {
        case TagsId
        case TagName
        case UserProfileTagMappingId
    }
}
