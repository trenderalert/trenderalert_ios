//
//  SideMenuTableViewCell.swift
//  TrenderAlert
//
//  Created by OSP LABS on 25/01/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var menuImageView: UIImageView!
    @IBOutlet weak var menuTitleLabel: UILabel!
    @IBOutlet weak var menuCountLabel: UILabel!
    @IBOutlet weak var view_tapChange: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
