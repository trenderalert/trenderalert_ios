//
//  TrendAlert.swift
//  TrenderAlert
//
//  Created by OSP LABS on 01/02/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class TrendAlertTagLocationData: NSObject, Codable {
    var data = [TrendAlert]()

    enum CodingKeys: String, CodingKey {
        case data
    }
}

class TrendTags: NSObject, Codable {
    var TagsId = 0
    var TagName = String()
    var UserProfileTagMappingId = 0
    
    enum CodingKeys: String, CodingKey {
        case TagsId
        case TagName
        case UserProfileTagMappingId
    }
}

class TrendLocations: NSObject, Codable {
    var Location = String()
    var UserProfileTagMappingId = 0
    
    enum CodingKeys: String, CodingKey {
        case Location
        case UserProfileTagMappingId
    }
}

class TrendAlert: NSObject, Codable {
    var Id = 0
    var Tags = [TrendTags]()
    var Locations = [TrendLocations]()
    
    enum CodingKeys: String, CodingKey {
        case Id
        case Tags
        case Locations
    }
}
